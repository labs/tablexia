/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.checksum;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Comparator;

public class Checksum {

    public static void main(String[] args) throws NoSuchAlgorithmException, IOException {
        System.out.println(getMd5OfDir(new File(args[0])));
    }

    public static String getMd5OfDir(File file) throws NoSuchAlgorithmException, IOException {
        if (file.isDirectory()) {
            StringBuilder checksumBuilder = new StringBuilder();
            File[] files = file.listFiles();
            Arrays.sort(files,
                    new Comparator<File>() {
                        public int compare(File a, File b) {
                            return a.getName().compareTo(b.getName());
                        }
                    });

            for (int i = 0; i < files.length; i++) {
                checksumBuilder.append(getMd5OfDir(files[i]));
            }
            return getMD5OfString(checksumBuilder.toString());
        } else {
            return getMd5OfFile(file);
        }
    }

    public static String getMd5OfFile(File file) throws IOException, NoSuchAlgorithmException {
        StringBuilder returnVal = new StringBuilder();
        InputStream input = new FileInputStream(file);
        byte[] buffer = new byte[1024];
        MessageDigest md5Hash = MessageDigest.getInstance("MD5");
        int numRead = 0;
        while (numRead != -1) {
            numRead = input.read(buffer);
            if (numRead > 0) {
                md5Hash.update(buffer, 0, numRead);
            }
        }
        input.close();

        byte [] md5Bytes = md5Hash.digest();
        for (int i=0; i < md5Bytes.length; i++) {
            returnVal.append(Integer.toString((md5Bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return returnVal.toString();
    }

    public static String getMD5OfString(String str) throws NoSuchAlgorithmException {
        MessageDigest md5;
        StringBuilder hexString = new StringBuilder();
        md5 = MessageDigest.getInstance("md5");
        md5.reset();
        md5.update(str.getBytes());
        byte messageDigest[] = md5.digest();
        for (int i = 0; i < messageDigest.length; i++) {
            hexString.append(Integer.toHexString((0xF0 & messageDigest[i])>>4));
            hexString.append(Integer.toHexString(0x0F & messageDigest[i]));
        }
        return hexString.toString();
    }

}
