![Tablexia logo](https://gitlab.labs.nic.cz/labs/tablexia/raw/0efd78d10092b21472f8749ef673296502df034a/core/assets/common/screen/about/tablexia.png)

<br />
Tablexia is a modern educational application aiming to support the development of cognitive abilities. It is designed primarily for children with dyslexia in secondary schools. It should find its use in schools to supplement the standard teaching, as well as in pedagogical-psychological counseling offices and other counseling facilities for pupils with learning difficulties. 

## <b>Build</b>

Project is working with git submodule [Tablexia-Model](https://gitlab.labs.nic.cz/labs/tablexia-model). Before building the application, it needs to be called:

```
git submodule update --init --recursive
```

<br />
Tablexia can be build by running the command:

```
./gradlew build
```

This will build all versions of application (Android, iOS, desktop). It will work only on OS X systems, as iOS application can be build only there.
  <br />
<br />
For building specific version of application, run following commands:

<br />
<b>Android version</b> 

```
./gradlew android:build
```

<br />
<b>iOS version</b> 

```
./gradlew ios:build
```

<br />
<b>Desktop version</b> 

```
./gradlew desktop:build
```

<br />
Final result of building application for each platform are 2 build types: RELEASE and DEBUG.

DEBUG version will show debug information on the screen.

## Gradle build parameters
File with build parameters should be located in `~/.gradle/gradle.properies`

<br />
<b>TABLEXIA_ASSETS_DOWNLOAD_URL</b>

URL address for downloading assets (textures, sounds...)

<br />

<b>TABLEXIA_ASSETS_UPLOAD_URL</b>

URL address for uploading assets, when application is built

<br />

<b>TABLEXIA_ASSETS_UPLOAD_USER</b>

User used for uploading assets

<br />

<b>TABLEXIA_SERVER_PROTOCOL</b>

Protocol of server

<br />

<b>TABLEXIA_SERVER_HOST</b>

Server host

<br />

<b>TABLEXIA_SERVER_PORT</b>

Server port

<br />

<b>TABLEXIA_SERVER_SECRET</b>

Password for the server

<br />

<b>TABLEXIA_SENTRY_DSN_DEBUG</b>

URL for Sentry server for collecting bugs, debug version

<br />

<b>TABLEXIA_SENTRY_DSN_RELEASE</b>

URL for Sentry server for collecting bugs, release version

<br />

## <b>Assets</b>

All graphic assets in application are connected into several atlas files. These atlases are then packed into .zip files.
<br />
<br />

<b>Location of files:</b>
<br />

`tablexia/build/assets/src`
Original assets files


`tablexia/build/assets/dest`
Atlas files


`tablexia/build/assets/pack`
Packed .zip files

<br />
In order to have these assets in application, .zip files has to be copied into this folder:

`tablexia/android/assets/.tablexia_devel/download`

<br />
This applies only for desktop version.
