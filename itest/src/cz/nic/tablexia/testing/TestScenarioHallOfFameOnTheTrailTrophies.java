/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.halloffame.HallOfFameScreen;
import cz.nic.tablexia.shared.model.User;

public class TestScenarioHallOfFameOnTheTrailTrophies extends AbstractTestScenarioHallOfFameTrophies {
    public TestScenarioHallOfFameOnTheTrailTrophies(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        User user = createUser();
        logIn(user);

        waitForEvent("1. Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt("2. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("3. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton("4. Click at Hall of Fame button in the menu", MainMenuDefinition.HALL_OF_FAME);
        waitForScreen("5. Wait for screen HallOfFameScreen", HallOfFameScreen.class);

        checkAllTrophiesEmpty(6, user);

        writeToLogFile("\nADD GAME ON THE TRAIL EASY 0 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(7, user, GameDifficulty.EASY, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.NO_STAR);       //get _PLAY1
                                                            //  _PLAY1  _PLAY2  _PLAY3  _DIFF1  _DIFF1 _BONUS
        checkGameTrophies(8, user, new boolean[]{    false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   false,  false,  false,  false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL MEDIUM 1 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(9, user, GameDifficulty.MEDIUM, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkGameTrophies(10, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   false,  false,  false,  false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL HARD 2 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(11, user, GameDifficulty.HARD, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkGameTrophies(12, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   false,  false,  false,  false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL EASY 3 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(13, user, GameDifficulty.EASY, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkGameTrophies(14, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   false,  false,  false,  false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL MEDIUM 0 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(15, user, GameDifficulty.MEDIUM, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.NO_STAR);     //get _PLAY2
        checkGameTrophies(16, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   true,   false,  false,  false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL HARD 1 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(17, user, GameDifficulty.HARD, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkGameTrophies(18, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   true,   false,  false,  false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL EASY 2 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(19, user, GameDifficulty.EASY, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkGameTrophies(20, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   true,   false,  false,  false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL MEDIUM 3 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(21, user, GameDifficulty.MEDIUM, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.THREE_STAR);  //get _DIFF1
        checkGameTrophies(22, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   true,   false,  true,   false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL HARD 0 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(23, user, GameDifficulty.HARD, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.NO_STAR);
        checkGameTrophies(24, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   true,   false,  true,   false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL EASY 1 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(25, user, GameDifficulty.EASY, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.ONE_STAR);      //get _PLAY3
        checkGameTrophies(26, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   true,   true,   true,   false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL MEDIUM 2 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(27, user, GameDifficulty.MEDIUM, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkGameTrophies(28, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   true,   true,   true,   false, false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL HARD 3 STARS ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(29, user, GameDifficulty.HARD, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.THREE_STAR);    //get _DIFF2
        checkGameTrophies(30, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   true,   true,   true,   true,  false,      //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME

        writeToLogFile("\nADD GAME ON THE TRAIL BONUS 1 STAR ---> CHECK TROPHIES\n");
        addNewScoreAndGoToHallOfFame(31, user, GameDifficulty.BONUS, GameDefinition.ON_THE_TRAIL, AbstractTablexiaGame.GameResult.THREE_STAR);    //get _BONUS
        checkGameTrophies(32, user, new boolean[]{   false,  false,  false,  false,  false, false,      //ROBBERY
                                                                false,  false,  false,  false,  false, false,      //PURSUIT
                                                                false,  false,  false,  false,  false, false,      //KIDNAPPING
                                                                false,  false,  false,  false,  false, false,      //NIGHT_WATCH
                                                                false,  false,  false,  false,  false, false,      //SHOOTING_RANGE
                                                                false,  false,  false,  false,  false, false,      //IN_THE_DARKNESS
                                                                false,  false,  false,  false,  false, false,      //CRIME_SCENE
                                                                false,  false,  false,  false,  false, false,      //RUNES
                                                                false,  false,  false,  false,  false, false,      //PROTOCOL
                                                                false,  false,  false,  false,  false, false,      //SAFE
                                                                true,   true,   true,   true,   true,  true,       //ON_THE_TRAIL
                                                                false,  false,  false,  false,  false, false,      //MEMORY_GAME
                                                                false,  false,  false,  false,  false, false});    //ATTENTION_GAME
    }
}
