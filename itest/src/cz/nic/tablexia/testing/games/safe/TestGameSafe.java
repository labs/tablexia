/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.testing.games.safe;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.safe.AbstractMechanics;
import cz.nic.tablexia.game.games.safe.SafeGame;
import cz.nic.tablexia.game.games.safe.SafeMechanicsMedium;
import cz.nic.tablexia.game.games.safe.actors.SequenceGenerator;
import cz.nic.tablexia.game.games.safe.gameobject.SafeLightImage;
import cz.nic.tablexia.game.games.safe.model.SafeSequence;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;
import cz.nic.tablexia.shared.model.resolvers.SafeScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;

/**
 * Created by Aneta Steimarová on 6.10.17.
 */

public class TestGameSafe extends AbstractTestGame {
    private int score;

    public TestGameSafe(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.SAFE);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at game safe button in the menu", GameMenuDefinition.SAFE);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);
        takeScreenShotBlocking();
        clickAt(incGetSteps() + ". Click preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForScreen(incGetSteps() + ". Wait for safe screen", SafeGame.class);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        takeScreenShot();

        playGame();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if(expectedResult != AbstractTablexiaGame.GameResult.NO_STAR){
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();
        clickButtonOnVictoryDialog(incGetSteps()+". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,1);

        waitForScreen(incGetSteps() + ". Wait for game page screen",GamePageScreen.class);

        printResult();
        takeScreenShot();

    }
    private void playGame() {
        SafeGame safeGame = (SafeGame) TablexiaApplication.getActualScreen();

        if(expectedResult.equals(AbstractTablexiaGame.GameResult.NO_STAR)){
            playRound(true, safeGame, 1);
            playRound(true, safeGame, 2);
        }
        else if(expectedResult.equals(AbstractTablexiaGame.GameResult.ONE_STAR)){
            playRound(false, safeGame, 1);
            playRound(false, safeGame, 2);
            playRound(true, safeGame, 3);
            playRound(true, safeGame, 4);
        }
        else if(expectedResult.equals(AbstractTablexiaGame.GameResult.TWO_STAR)){
            playRound(false, safeGame, 1);
            playRound(false, safeGame, 2);
            playRound(false, safeGame, 3);
            playRound(true, safeGame, 4);
        }
        else{
            playRound(false, safeGame, 1);
            playRound(false, safeGame, 2);
            playRound(false, safeGame, 3);
            playRound(false, safeGame, 4);
        }
    }

    private void playRound(boolean doMistake, SafeGame safeGame, int round) {
        waitForEvent(incGetSteps() + ". Wait for sounds to be played", SequenceGenerator.SCENARIO_STEP_SEQUENCE_PLAYED);
        SafeSequence safeSequence = safeGame.getSafeSequence();
        if(doMistake) doMistake(safeGame, safeSequence);
        else doItRight(safeGame, safeSequence, round);
        takeScreenShot();
    }

    private void doMistake(SafeGame safeGame, SafeSequence safeSequence) {
        for(int i=0; i<safeSequence.getSoundsToSequence().length; i++){
            SafeLightImage safeLightImage = (SafeLightImage)findActorByName(AbstractMechanics.LIGHT_NAME+i);
            if(!safeLightImage.getSound().equals(safeGame.getMusic(safeSequence.getExpected().getSoundPath()))){
                setLightAndClickDone(i, "wrong");
                return;
            }
        }
        logError("Could not do mistake!!");
    }

    private void doItRight(SafeGame safeGame, SafeSequence safeSequence, int round) {
        for(int i=0; i<safeSequence.getSoundsToSequence().length; i++){
            SafeLightImage safeLightImage = (SafeLightImage)findActorByName(AbstractMechanics.LIGHT_NAME+i);
            if(safeLightImage.getSound().equals(safeGame.getMusic(safeSequence.getExpected().getSoundPath()))){
                setLightAndClickDone(i, "right");
                score+=round;
                return;
            }
        }
        logError("Could not click at right light!");
    }

    private void setLightAndClickDone(int i, String lightOKorFail) {
        if(difficulty.equals(GameDifficulty.MEDIUM)){
            final float duration = 0.5f;
            final int firstFingerPointer  = 0;

            Actor connector = findActorByName(SafeMechanicsMedium.CONNECTOR_NAME+i);
            Vector2 connectorPos = new Vector2(getActorPosition(connector, ActorPosition.MIDDLE));

            Actor finalSocket = findActorByName(SafeMechanicsMedium.FINAL_SOCKET_NAME);
            Vector2 finalSocketPos = new Vector2(getActorPosition(finalSocket, ActorPosition.MIDDLE));

            Stage stage = connector.getStage();

            logInfo("Move connector" + i + " to final socket");
            boolean result = touchDown(connectorPos.x,connectorPos.y,firstFingerPointer, Input.Buttons.LEFT,stage);

            result &= dragOverTime((int)connectorPos.x,(int)connectorPos.y,(int)finalSocketPos.x,(int)finalSocketPos.y,firstFingerPointer,duration,30,stage);
            result &= touchUp((int)finalSocketPos.x,(int)finalSocketPos.y,firstFingerPointer,Input.Buttons.LEFT,stage);

            if(!result){
                logError("Cannot move connector to final socket!");
                stopTheTest();
            }
            logOK();
        }
        else clickAt(incGetSteps() + ". Click at " + lightOKorFail + " light", AbstractMechanics.LIGHT_NAME+i);
        waitAndClickDone();
    }

    private void waitAndClickDone() {
        waitForEvent(incGetSteps() + ". Wait for lightning the light", AbstractMechanics.LIGHT_LIGHTED_UP);
        clickAt(incGetSteps() + ". Click at done button", SafeGame.DONE_BUTTON);
    }


    @Override
    protected void checkVictoryTable(int daoScore, Table table) {
        if(!checkScoreInTable(table,Integer.toString(daoScore))){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE+ " with correct fail count: " + Integer.toString(daoScore));
            stopTheTest();
        }
    }

    @Override
    protected void testVictoryData() {
        Game game = loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(SafeScoreResolver.SCORE_COUNT,"0"));
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.SAFE,game,daoScore);
    }

    @Override
    protected int getCubsCount() {
        GameResultDefinition gameResult = SafeScoreResolver.ResultStars.getGameResultForScore(score);

        if(gameResult.equals(GameResultDefinition.ONE_STAR)) return 1;
        if(gameResult.equals(GameResultDefinition.TWO_STAR)) return 2;
        if(gameResult.equals(GameResultDefinition.THREE_STAR)) return 3;

        return 0;
    }

    @Override
    protected void printResult() {
        Game game = loadGameFromDao(getGameID());
        int daoScore = Integer.valueOf(game.getGameScore(SafeScoreResolver.SCORE_COUNT,"0"));

        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() +  "\n");
        logInfo("Game number: " + GameDefinition.SAFE.getGameNumber()+ "\n");
        logInfo("SCORE: " + daoScore +"\n");
        logInfo("STARS: " + getCubsCount()+"\n");
    }
}
