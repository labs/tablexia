/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games.robbery;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.robbery.GameScreen;
import cz.nic.tablexia.game.games.robbery.RobberyGame;
import cz.nic.tablexia.game.games.robbery.RuleScreen;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot;
import cz.nic.tablexia.game.games.robbery.rules.RobberyDifficultyDefinition;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.resolvers.RobberyScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;
import cz.nic.tablexia.util.ui.TablexiaLabel;


/**
 * Created by lmarik on 17.2.17.
 */

public class TestGameRobbery extends AbstractTestGame {

    private int numberCreature = 1;

    public TestGameRobbery(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.ROBBERY);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at Game robbery button in the menu", GameMenuDefinition.ROBBERY);


        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForScreen(incGetSteps() + ". Wait for rule screen", RuleScreen.class);
        takeScreenShot();
        clickAtInRobberyScreen(incGetSteps() + ". Click on understand button",RuleScreen.BUTTON_OK);

        waitForEvent(incGetSteps() + ". Wait for event ready game", AbstractTablexiaGame.EVENT_GAME_READY);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        play();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if(expectedResult != AbstractTablexiaGame.GameResult.NO_STAR){
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();

        clickButtonOnVictoryDialog(incGetSteps() +". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,1);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        printResult();
        takeScreenShot();
    }

    @Override
    protected void testVictoryData(){
        Game game = loadGameFromDao(getGameID());

        int numOfPerson = getRobberyScore(RobberyScoreResolver.SCORE_KEY_PERSON_NUMBER,game);
        int wrongPerson = getRobberyScore(RobberyScoreResolver.SCORE_KEY_INNOCENCE_PERSON,game);
        int escapeThieves = getRobberyScore(RobberyScoreResolver.SCORE_KEY_ESCAPED_THIEVES,game);

        int daoScore = numOfPerson - wrongPerson - escapeThieves;
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.ROBBERY,game,daoScore);
    }

    @Override
    protected int getCubsCount() {
        if(difficulty == GameDifficulty.BONUS){
            if (gameScore >= RobberyScoreResolver.BONUS_SCORE_THREE_STARS) return 3;
            else if(gameScore >= RobberyScoreResolver.BONUS_SCORE_TWO_STARS) return 2;
            else if(gameScore >= RobberyScoreResolver.BONUS_SCORE_ONE_STAR) return 1;
            else return 0;
        } else{
            if (gameScore >= RobberyScoreResolver.FINAL_SCORE_THREE_STARS) return 3;
            else if(gameScore >= RobberyScoreResolver.FINAL_SCORE_TWO_STARS) return 2;
            else if(gameScore >= RobberyScoreResolver.FINAL_SCORE_ONE_STAR) return 1;
            else return 0;
        }
    }

    @Override
    protected void printResult() {
        Game game = loadGameFromDao(getGameID());

        int numOfPerson = getRobberyScore(RobberyScoreResolver.SCORE_KEY_PERSON_NUMBER,game);
        int wrongPerson = getRobberyScore(RobberyScoreResolver.SCORE_KEY_INNOCENCE_PERSON,game);
        int escapeThieves = getRobberyScore(RobberyScoreResolver.SCORE_KEY_ESCAPED_THIEVES,game);

        int daoScore = numOfPerson - wrongPerson - escapeThieves;
        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed()  + "\n");
        logInfo("Game number: " + GameDefinition.ROBBERY+ "\n");
        logInfo("Number of person: " + numOfPerson +"\n");
        logInfo("Wrong person: " + wrongPerson +"\n");
        logInfo("EscapeThieves: " + escapeThieves + "\n");
        logInfo("Final score: " + daoScore + "\n");
        logInfo("Cubs count: " + getCubsCount());
    }

    @Override
    protected void checkVictoryTable(int daoScore,Table table) {
        TablexiaLabel personCounter = (TablexiaLabel) findActorInRobbery(GameScreen.LABEL_PERSON_COUNTER);
        if(personCounter == null){
            logError("Actor " + GameScreen.LABEL_PERSON_COUNTER + " not found");
            stopTheTest();
        }
        int labelCreatureCnt = Integer.valueOf(personCounter.getText().toString());

        if((numberCreature-1) != labelCreatureCnt){
            logError("TablexiaLabel " + GameScreen.LABEL_PERSON_COUNTER + " have different counter of value (" +labelCreatureCnt + ") than test data (" + (numberCreature-1) +")");
            stopTheTest();
        }

        String score = (difficulty == GameDifficulty.BONUS) ? Integer.toString(daoScore) : Integer.toString(daoScore) + "/" + Integer.toString(RobberyDifficultyDefinition.DEFAULT_CREATURES_COUNT);

        if(!checkScoreInTable(table,score)){
            if(difficulty != GameDifficulty.BONUS)logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE + " with correct dao score: " + Integer.toString(daoScore) + "/" + Integer.toString(RobberyDifficultyDefinition.DEFAULT_CREATURES_COUNT) );
            else logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE + " with correct dao score: " + Integer.toString(daoScore));
            stopTheTest();
        }
    }


    protected void play() {

        int mistakesInRound = 0;

        int creaturesCount = (difficulty == GameDifficulty.BONUS) ? 110 : RobberyDifficultyDefinition.DEFAULT_CREATURES_COUNT;

        while (numberCreature <= creaturesCount) {
            waitForEvent(incGetSteps() + ". Wait for creature middle event",GameScreen.EVENT_CREATURE_MIDDLE+numberCreature);
            CreatureRoot creature = (CreatureRoot) findActorInRobbery(GameScreen.CREATURE + numberCreature);

            if(creature == null){
                logError("Cannot find creature in number " + numberCreature);
                stopTheTest();
            }

            numberCreature++;
            if (creature.isThief()) {
                if (!doMistake()) {
                    clickOnGameObject(incGetSteps() + ". Click, Person is thief " + creature.getName(), creature);
                    gameScore++;
                } else {
                    logInfo(incGetSteps() + ". Do mistake. Not catch thief " + creature.getName());
                    mistakesInRound++;
                    gameScore--;
                    logOK();
                }
            } else {
                if (doMistake()) {
                    clickOnGameObject(incGetSteps() + ". Do mistake.Click on wrong person " + creature.getName(), creature);
                    mistakesInRound++;
                    gameScore--;
                } else {
                    logInfo(incGetSteps() + ". No click. Person is not thief " + creature.getName());
                    gameScore++;
                    logOK();
                }


            }
            takeScreenShot();
            if(mistakesInRound >= GameScreen.MAXIMUM_MISTAKES_COUNT)
                break;
        }
    }

    private int getRobberyScore(String scoreKey,Game game){
        return Integer.valueOf(game.getGameScore(scoreKey,"0"));
    }


    private boolean doMistake(){
        if(difficulty == GameDifficulty.BONUS){
            if (expectedResult == AbstractTablexiaGame.GameResult.THREE_STAR && gameScore >= RobberyScoreResolver.BONUS_SCORE_THREE_STARS + 2) {
                return true;
            } else if (expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR && gameScore >= RobberyScoreResolver.BONUS_SCORE_THREE_STARS - 2) {
                return true;
            } else if (expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR && gameScore >= RobberyScoreResolver.BONUS_SCORE_TWO_STARS - 2) {
                return true;
            } else if (expectedResult == AbstractTablexiaGame.GameResult.NO_STAR && gameScore >= RobberyScoreResolver.BONUS_SCORE_ONE_STAR - 2) {
                return true;
            } else return false;
        } else {
            if (expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR && gameScore >= RobberyScoreResolver.FINAL_SCORE_THREE_STARS - 2) {
                return true;
            } else if (expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR && gameScore >= RobberyScoreResolver.FINAL_SCORE_TWO_STARS - 2) {
                return true;
            } else if (expectedResult == AbstractTablexiaGame.GameResult.NO_STAR && gameScore >= RobberyScoreResolver.FINAL_SCORE_ONE_STAR - 2) {
                return true;
            } else return false;
        }
    }


    private void clickAtInRobberyScreen(String infoMessage,String actorName){
        logInfo(infoMessage);

        Actor actor = findActorInRobbery(actorName);
        if(actor != null){
            if (!clickInTheActor(actor)) {
                logError("Couldn't click in the actor: " + actorName + "!");
                stopTheTest();
            }
            logOK();
        }else{
            logError("Actor " + actorName + " not found");
            stopTheTest();
        }

    }

    private Actor findActorInRobbery(String actorName){
        if(TablexiaApplication.getActualScreen() instanceof RobberyGame){
            RobberyGame robberyGame = (RobberyGame) TablexiaApplication.getActualScreen();
            Actor actor = robberyGame.getActualScreen().getStage().getRoot().findActor(actorName);
            return actor;

        } else{
             logError("Actual screen is not robbery game");
             stopTheTest();
        }

        return null;
    }

}
