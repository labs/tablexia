/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games.shooting_range;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.shooting_range.Properties;
import cz.nic.tablexia.game.games.shooting_range.ShootingRangeGame;
import cz.nic.tablexia.game.games.shooting_range.actors.Carousel;
import cz.nic.tablexia.game.games.shooting_range.actors.Row;
import cz.nic.tablexia.game.games.shooting_range.actors.Scene;
import cz.nic.tablexia.game.games.shooting_range.actors.Target;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;
import cz.nic.tablexia.game.games.shooting_range.model.Direction;
import cz.nic.tablexia.game.games.shooting_range.model.GameState;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.resolvers.ShootingRangeScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by lmarik on 14.2.17.
 */

public class TestGameShootingRange extends AbstractTestGame {
    private final int SHOOT_PLACE_PADDING   = 50;
    private final int GOOD_HIT              = 2;
    private final int WRONG_HIT             = -1;

    private Scene       gameScene;
    private Carousel    carousel;
    private GameState gameState;

    public TestGameShootingRange(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.SHOOTING_RANGE);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at Game shooting range button in the menu", GameMenuDefinition.SHOOTING_RANGE);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event ready game",AbstractTablexiaGame.EVENT_GAME_READY);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        gameScene = (Scene) findGameActor(ShootingRangeGame.GAME_SCENE);
        carousel = (Carousel) findGameActor(ShootingRangeGame.CAROUSEL);
        takeScreenShot();

        play();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if(expectedResult != AbstractTablexiaGame.GameResult.NO_STAR){
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();
        clickButtonOnVictoryDialog(incGetSteps() +". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,1);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        printResult();
        takeScreenShot();
    }

    @Override
    protected void printResult(){
        Game game = loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(ShootingRangeScoreResolver.SCORE_TOTAL,"0"));
        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() +  "\n");
        logInfo("Game number: " + GameDefinition.SHOOTING_RANGE.getGameNumber()+ "\n");
        logInfo("SCORE: " + daoScore + "\n");
        logInfo("STARS: " + getCubsCount() + "\n");
    }

    @Override
    protected void testVictoryData(){
        Game game = loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(ShootingRangeScoreResolver.SCORE_TOTAL,"0"));
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.SHOOTING_RANGE,game,daoScore);
    }

    @Override
    protected int getCubsCount(){

        int gameDifficultyOrdinal = difficulty.ordinal() -1;
        if (gameState.getScore() > ShootingRangeScoreResolver.CUPS[gameDifficultyOrdinal][2])      return 3;
        else if (gameState.getScore() > ShootingRangeScoreResolver.CUPS[gameDifficultyOrdinal][1]) return 2;
        else if (gameState.getScore() > ShootingRangeScoreResolver.CUPS[gameDifficultyOrdinal][0]) return 1;
        else return 0;

    }


    protected void play() {
        gameState = (GameState) getTablexia().getActualScreen().getData();
        while (gameState.getTime() < Properties.GAME_TIME ) {
            TextureType currentTarget = carousel.getCurrentTarget();
            if (currentTarget != null) {
                if (expectedResult == AbstractTablexiaGame.GameResult.THREE_STAR) {
                    shootOnBox(gameScene.getRows(), TextureType.BOX_GOOD);
                }

                if (expectedResult != AbstractTablexiaGame.GameResult.THREE_STAR && wrongShot(gameState)) {
                    //wrong shot
                    Target target = findWrongTarget(gameScene.getRows(), currentTarget);
                    if (target != null && gameScene.isTouchable() && !target.isShot()) {
                        clickOnTarget(incGetSteps() + ". Shoot on wrong target : " + target.getTextureType().getResource(), target);
                        gameScore += WRONG_HIT;
                    }

                } else {
                    //shot on current target
                    Target target = findTargetInRows(gameScene.getRows(), currentTarget);
                    if (target != null && gameScene.isTouchable() && !target.isShot()) {
                        clickOnTarget(incGetSteps() + ". Shoot on current target : " + currentTarget.getResource(), target);
                        gameScore += GOOD_HIT;
                    }
                }

                if (expectedResult == AbstractTablexiaGame.GameResult.NO_STAR) {
                    shootOnBox(gameScene.getRows(), TextureType.BOX_BAD);
                }
            }
            wait((int)(Properties.GAME_SPEED_NORMAL*1000));
        }
    }

    @Override
    protected void checkVictoryTable(int daoScore,Table table) {
        TablexiaLabel scoreLabel = (TablexiaLabel) findGameActor(ShootingRangeGame.LABEL_SCORE);

        int screenScore = Integer.parseInt(scoreLabel.getText().toString());

        logInfo("GAME SCORE: " + gameScore + "\n");
        if(screenScore != daoScore){
            logError("Scores is different: " + " DAO SCORE: " + daoScore + " LABEL IN SCREEN SCORE: " + screenScore);
            stopTheTest();
        }

        if(!checkScoreInTable(table,Integer.toString(daoScore))){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE+ " with correct dao score: " + daoScore );
            stopTheTest();
        }
    }

    private void clickOnTarget(String infoMessage,Target target){

        logInfo(infoMessage);

        float shootX = target.getX() + target.getWidth()/2;
        float shootY = target.getY() + target.getHeight()/2;
        //Every returns false
        touchDown(shootX,shootY,0, Input.Buttons.LEFT,target.getStage());
        touchUp(shootX,shootY,0,Input.Buttons.LEFT,target.getStage());

        logOK();
    }


    private void shootOnBox(Row[] rows,TextureType botTextureType){
        Target box = findTargetInRows(rows, botTextureType);
        if (box != null && gameScene.isTouchable()  && !box.isShot()) {
            clickOnTarget(incGetSteps() + ". Shoot on box: " + botTextureType.getResource(), box);
        }
    }

    private boolean wrongShot(GameState gameState){
        return (gameState.getScore() > (ShootingRangeScoreResolver.CUPS[difficulty.ordinal()-1][expectedResult.ordinal()] - GOOD_HIT));
    }



    private Target findWrongTarget(Row[] rows, TextureType currentType){
        for(Row row : rows){
            for(Target target: row.getTargets()){
                if((!target.getTextureType().equals(currentType)) && (!target.getTextureType().equals(TextureType.BOX_GOOD))
                        && (!target.getTextureType().equals(TextureType.BOX_BAD))  && !target.isShot() &&
                        (target.getX() > (gameScene.getX() + SHOOT_PLACE_PADDING) && target.getX() < (gameScene.getX() + gameScene.getWidth() - SHOOT_PLACE_PADDING))){
                    return target;
                }
            }
        }
        return null;
    }

    private Target findTargetInRows(Row[] rows, TextureType currentType){
        Target target = null;
        for(Row row : rows){
            Target find = findTargetInRow(row,currentType);

            if(find!=null){
                if(target == null){
                    target = find;
                }else{
                    Direction direction = row.getWave().getDirection();

                    if((direction == Direction.LEFT && target.getX() > find.getX()) || (direction == Direction.RIGHT &&target.getX() < find.getX())){
                        target = find;
                    }
                }
            }
        }
        return target;
    }

    private Target findTargetInRow(Row row,TextureType currentType){
        for(Target target: row.getTargets()){
            if(target.getTextureType().equals(currentType) && !target.isShot() &&
                    (target.getX() > (gameScene.getX() + SHOOT_PLACE_PADDING) && target.getX() < (gameScene.getX() + gameScene.getWidth() - SHOOT_PLACE_PADDING))){
                return target;
            }
        }
        return null;
    }

}
