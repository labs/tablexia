package cz.nic.tablexia.testing.games.memory_game;
/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.memory.actors.BonusObjectPositioner;
import cz.nic.tablexia.game.games.memory.actors.MediumHardObjectPositioner;
import cz.nic.tablexia.game.games.memory.game_objects.GameObject;
import cz.nic.tablexia.game.games.memory.game_objects.MemoryImage;
import cz.nic.tablexia.game.games.memory.game_objects.NumberCardGroup;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;
import cz.nic.tablexia.shared.model.resolvers.MemoryGameScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;
import cz.nic.tablexia.util.Log;


public class TestMemoryGame extends AbstractTestGame {
    private int mistakesCount = 0;

    public TestMemoryGame(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.MEMORY_GAME);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at game memory_game button in the menu", GameMenuDefinition.MEMORY_GAME);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        /*clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }*/

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);
        takeScreenShotBlocking();
        clickAt(incGetSteps() + ". Click preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForScreen(incGetSteps() + ". Wait for memory_game screen", MemoryGame.class);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        takeScreenShot();

        playGame();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if(expectedResult != AbstractTablexiaGame.GameResult.NO_STAR){
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();
        clickButtonOnVictoryDialog(incGetSteps()+". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,1);

        waitForScreen(incGetSteps() + ". Wait for game page screen",GamePageScreen.class);

        printResult();
        takeScreenShot();
    }

    private void playGame() {
        MemoryGame memoryGame = (MemoryGame) TablexiaApplication.getActualScreen();

        waitForEvent(incGetSteps() + ". Wait until photography is shown", MemoryGame.SCENARIO_STEP_PHOTO_SHOWN);

        switch (difficulty){
            case EASY: playEasy(memoryGame); break;
            case BONUS: playBonus(memoryGame); break;
            default: playMediumHard(memoryGame); break;
        }

        clickInTheActor(memoryGame.getDoneButton());
    }

    private void playEasy(MemoryGame memoryGame) {
        takeScreenShot();
        List<GameObject> movableGameObjects = memoryGame.getObjectsPositioner().getMovableGameObjects();
        List<MemoryImage> fixedGameObjects = memoryGame.getObjectsPositioner().getFixedGameObjects();

        int allSelectedObjects = movableGameObjects.size();
        int mistakesToDo = MemoryGameScoreResolver.ResultStars.getScoreForGameResult(expectedResult.getGameResultDefinition(), memoryGame.getGameDifficulty().getDifficultyNumber());
        int correctClickToDo = allSelectedObjects - mistakesToDo;

        for(int i = 0; i< mistakesToDo; i++){
            mistakesCount++;
            performClick(incGetSteps() + ". Do mistake - click at actor " + fixedGameObjects.get(i).getName(), fixedGameObjects.get(i).getName());
        }

        for(int i = 0; i< correctClickToDo; i++){
            performClick(incGetSteps() + ". Click at actor " + movableGameObjects.get(i).getName(), movableGameObjects.get(i).getName());
        }
        takeScreenShot();
    }

    private void performClick(String clickInfoMessage, String gameObjectName) {
        clickAt(clickInfoMessage, gameObjectName);
        waitForEvent(incGetSteps() + ". Actor colors turn to selected", GameObject.SCENARIO_STEP_ACTOR_SELECTED);
    }

    private void playMediumHard(MemoryGame memoryGame) {
        takeScreenShot();

        if(difficulty!= GameDifficulty.HARD && difficulty!=GameDifficulty.MEDIUM){
            logError("Cannot play game with " + difficulty.toString() + " difficulty " + " to difficulty hard/medium");
            stopTheTest();
        }
        List<GameObject> cardsInWidget = ((MediumHardObjectPositioner)memoryGame.getObjectsPositioner()).getCardsInWidget();

        List<NumberCardGroup> numberCards = memoryGame.getObjectsPositioner().getNumberCards();
        int allSelectedObjects = memoryGame.getObjectsPositioner().getNumberCards().size();
        int mistakeMovesToDo = MemoryGameScoreResolver.ResultStars.getScoreForGameResult(expectedResult.getGameResultDefinition(), memoryGame.getGameDifficulty().getDifficultyNumber());
        int correctMovesToDo = allSelectedObjects - mistakeMovesToDo;

        for(int i = 0; i< correctMovesToDo; i++){
            for(GameObject gameCard: cardsInWidget){
                if(gameCard.getDefaultCardNumber() > 0){
                    moveCardToCardNumber(memoryGame, gameCard, gameCard.getDefaultCardNumber(), numberCards);
                    break;
                }
            }
        }

        for(NumberCardGroup numberCard: numberCards){
            if(numberCard.isFixedObject()) continue;
            for(GameObject gameCard: cardsInWidget){
                if(gameCard.getDefaultCardNumber() == 0 || (gameCard.getDefaultCardNumber()!=numberCard.getCardNumber())){
                    moveCardToCardNumber(memoryGame, gameCard, numberCard.getCardNumber(), numberCards);
                    break;
                }
            }
            mistakesCount++;
        }

        takeScreenShot();
    }

    private void moveCardToCardNumber(MemoryGame memoryGame, GameObject gameCard, int cardNumber, List<NumberCardGroup> numberCards) {
        logInfo(incGetSteps() + ". Move game object " + gameCard.getName() + " to number card NO: " + cardNumber);

        float NOcardX = 0;
        float NOcardY = 0;

        for (NumberCardGroup numberCard : numberCards) {
            if(numberCard.getCardNumber() == cardNumber){
                NOcardX = numberCard.getX() + numberCard.getWidth();
                NOcardY = numberCard.getY() + numberCard.getHeight();
                break;
            }
        }

        if(NOcardX==0 && NOcardY==0){
            logError("Could not find card number NO " + cardNumber);
            stopTheTest();
        }

        moveGameObjectToCardPosition(memoryGame, gameCard, NOcardX, NOcardY);
    }

    private void moveGameObjectToCardPosition(MemoryGame memoryGame, GameObject gameObject, float xFinish, float yFinish) {
        Vector2 objectPosition = getActorPosition(gameObject, ActorPosition.MIDDLE);

        Stage stage = gameObject.getStage();
        clickAt((int)xFinish, (int)yFinish, stage);
        takeScreenShot();
        boolean result = touchDown(objectPosition.x, objectPosition.y, 0, Input.Buttons.LEFT, stage);
        result &= dragOverTime((int) objectPosition.x, (int) objectPosition.y, (int) xFinish, (int) yFinish, 0, 0.5f, 30, stage);
        result &= touchUp(xFinish, yFinish, 0, Input.Buttons.LEFT, stage);
        takeScreenShot();
        takeScreenShotBlocking();

        if (!result) {
            logError("Cannot move gameObject " + gameObject.getName() + " to card number.");
            stopTheTest();
        }

        logOK();

        waitForEvent(incGetSteps() + ". Wait for drop event", ((MediumHardObjectPositioner)memoryGame.getObjectsPositioner()).SCENARIO_DROP_EVENT);
    }

    private void playBonus(MemoryGame memoryGame) {
        takeScreenShot();

        if(difficulty!= GameDifficulty.BONUS){
            logError("Cannot play game with " + difficulty.toString() + " difficulty " + " to difficulty bonus");
            stopTheTest();
        }

        List<GameObject> cardsInWidget = ((MediumHardObjectPositioner)memoryGame.getObjectsPositioner()).getCardsInWidget();
        List<GameObject> extraObjects = ((BonusObjectPositioner) memoryGame.getObjectsPositioner()).getListOfExtraObjects();
        List<MemoryImage> fixedObjects = memoryGame.getObjectsPositioner().getFixedGameObjects();

        List<NumberCardGroup> numberCards = memoryGame.getObjectsPositioner().getNumberCards();
        int allSelectedAndExtraObjects = memoryGame.getObjectsPositioner().getNumberCards().size() + extraObjects.size();
        int mistakeMovesAndClicksToDo = MemoryGameScoreResolver.ResultStars.getScoreForGameResult(expectedResult.getGameResultDefinition(), memoryGame.getGameDifficulty().getDifficultyNumber());
        int correctMovesAndClicksToDo = allSelectedAndExtraObjects - mistakeMovesAndClicksToDo;

        Log.info("CLICK and MOVES", "mistakes: " + mistakeMovesAndClicksToDo + ", corrects: " + correctMovesAndClicksToDo + ", all: " + allSelectedAndExtraObjects);

        boolean clickOrMove = true;
        int extraObjectsIndexSelected = 0;
        for(int i=0; i<correctMovesAndClicksToDo; i++){
            if(extraObjectsIndexSelected < extraObjects.size() && clickOrMove){
                performClick(incGetSteps() + ". Click at extra actor " + extraObjects.get(extraObjectsIndexSelected).getName(), extraObjects.get(extraObjectsIndexSelected).getName());
                clickOrMove = false;
                extraObjectsIndexSelected++;
            }
            else {
                for(GameObject gameCard: cardsInWidget){
                    if(gameCard.getDefaultCardNumber() > 0){
                        moveCardToCardNumber(memoryGame, gameCard, gameCard.getDefaultCardNumber(), numberCards);
                        break;
                    }
                }
                clickOrMove = true;
            }
        }

        for(int i=0; i < extraObjects.size()-extraObjectsIndexSelected; i++){
            performClick(incGetSteps() + ". Do mistake - click at actor " + fixedObjects.get(i).getName(), fixedObjects.get(i).getName());
            mistakesCount++;
        }

        for(NumberCardGroup numberCard: numberCards){
            if(numberCard.isFixedObject()) continue;
            for(GameObject gameCard: cardsInWidget){
                if(gameCard.getDefaultCardNumber() == 0 || (gameCard.getDefaultCardNumber()!=numberCard.getCardNumber())){
                    moveCardToCardNumber(memoryGame, gameCard, numberCard.getCardNumber(), numberCards);
                    break;
                }
            }
            mistakesCount++;
        }

    }

    @Override
    protected void checkVictoryTable(int daoScore, Table table) {
        if(!checkScoreInTable(table,Integer.toString(daoScore))){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE+ " with correct fail count: " + Integer.toString(daoScore));
            stopTheTest();
        }
    }

    @Override
    protected void testVictoryData() {
        Game game = loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(MemoryGameScoreResolver.SCORE_COUNT,"0"));
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.MEMORY_GAME,game,daoScore);
    }

    @Override
    protected int getCubsCount() {
        GameResultDefinition gameResult = MemoryGameScoreResolver.ResultStars.getGameResultForScore(mistakesCount, difficulty.getDifficultyNumber());

        if(gameResult.equals(GameResultDefinition.ONE_STAR)) return 1;
        if(gameResult.equals(GameResultDefinition.TWO_STAR)) return 2;
        if(gameResult.equals(GameResultDefinition.THREE_STAR)) return 3;

        return 0;
    }

    @Override
    protected void printResult() {
        Game game = loadGameFromDao(getGameID());
        int daoScore = Integer.valueOf(game.getGameScore(MemoryGameScoreResolver.SCORE_COUNT,"0"));

        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() +  "\n");
        logInfo("Game number: " + GameDefinition.SAFE.getGameNumber()+ "\n");
        logInfo("MISTAKES: " + daoScore +"\n");
        logInfo("STARS: " + getCubsCount()+"\n");
    }
}
