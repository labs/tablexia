/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.testing.games.on_the_trail;

import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.HashMap;
import java.util.Map;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.on_the_trail.BonusRuleGroup;
import cz.nic.tablexia.game.games.on_the_trail.OnTheTrailGame;
import cz.nic.tablexia.game.games.on_the_trail.map.CityMap;
import cz.nic.tablexia.game.games.on_the_trail.map.PositionPoint;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Circle;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Crossroad;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Direction;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.MapController;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Solution;
import cz.nic.tablexia.game.games.on_the_trail.menu.MainMenu;
import cz.nic.tablexia.game.games.on_the_trail.menu.MenuType;
import cz.nic.tablexia.game.games.on_the_trail.model.OnTheTrailGameState;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.resolvers.OnTheTrailScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

public class TestGameOnTheTrail extends AbstractTestGame {
    private OnTheTrailGame game;
    private CityMap cityMap;
    private cz.nic.tablexia.game.games.on_the_trail.menu.AbstractMenu mainMenu;

    private MapController mapController;
    private Map<Direction, TablexiaButton> moveButtons;
    private PositionPoint detective;

    private int moveCounter = 0;
    private int totalScore = OnTheTrailGameState.CROSSROADS_CNT;

    private float dW = 0;
    private float dH = 0;

    public TestGameOnTheTrail(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.ON_THE_TRAIL);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at game on the trial button in the menu", GameMenuDefinition.ON_THE_TRAIL);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        /*clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }*/

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);

        takeScreenShotBlocking();
        clickAt(incGetSteps() + ". Click preloader button", TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForScreen(incGetSteps() + ". Wait for protocol screen", OnTheTrailGame.class);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        waitForEvent(incGetSteps() + ". Wait for event ready game", AbstractTablexiaGame.EVENT_GAME_READY);

        play();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if (expectedResult != AbstractTablexiaGame.GameResult.NO_STAR) {
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();
        clickButtonOnVictoryDialog(incGetSteps() + ". Click finish button in victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, 1);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);

        printResult();
        takeScreenShot();
    }

    private void play(){
        if(difficulty == GameDifficulty.BONUS){
            waitForEvent(incGetSteps() + ". Wait for rule screen", BonusRuleGroup.VISIBLE_EVENT);
            clickAt(incGetSteps() + ". Click on the undestand button", BonusRuleGroup.RULE_BUTTON);
        }

        waitForEvent(incGetSteps() + ". Wait for robber animation finished", OnTheTrailGame.EVENT_ANIM_ROBBER);

        game = (OnTheTrailGame) TablexiaApplication.getActualScreen();
        cityMap = (CityMap) findGameActor(OnTheTrailGame.CITY);
        mainMenu = (cz.nic.tablexia.game.games.on_the_trail.menu.AbstractMenu) findGameActor(OnTheTrailGame.MENU);
        detective = (PositionPoint) findGameActor(CityMap.DETECTIVE_NAME);

        dW = detective.getWidth() / 2;
        dH = detective.getHeight() / 4;

        testMoveButtons();
        mapController = game.getData().getMapController();
        Solution solution = mapController.getSolution();


        Integer actualCross = null;
        for (int i = 0; i < solution.getIds().size(); i++){
            Integer crossID = solution.getIds().get(i);
            Direction direction = solution.getMoves().get(i);

            if(i == 0){
                actualCross = crossID;
                checkActualPosition(actualCross);
                continue;
            }

            if(!doCorrect() && canDoMistake(actualCross, crossID)){
                doMistake(direction, actualCross);
                totalScore--;
            }

            moveByDirection(crossID, direction);
            actualCross = crossID;

            if(i != solution.getIds().size() - 1) {
                if(direction.ordinal() > Direction.SLOPING_DOWN_RIGHT.ordinal()){
                    checkCirclePosition(crossID);
                }else {
                    checkActualPosition(crossID);
                }
            }
            else checkFinalPosition();

        }

        clickAt(incGetSteps() + ". Confirm way, Click on the final button", OnTheTrailGame.FINISH_BUT);
        takeScreenShot();
        clickAt(incGetSteps() + ". Confirm way, Click on the final button", OnTheTrailGame.FINISH_BUT);

    }

    private void doMistake(Direction correct, Integer actualCross){
        Crossroad crossroad = mapController.getCrossRoads().get(actualCross);

        Integer mistakeCrossroad = null;
        Direction mistakeDirection = null;
        Direction backToRoad = null;

        for(Direction direction: crossroad.getNeighbors().keySet()){
            Integer nextCross = crossroad.getNeighbors().get(direction);
            if(direction != correct && clearWay(actualCross, nextCross, direction)) {
                mistakeCrossroad = crossroad.getNeighbors().get(direction);
                mistakeDirection = direction;
                backToRoad = findBackDirection(mapController.getCrossRoads().get(mistakeCrossroad), actualCross);
            }
        }

        moveByDirection(mistakeCrossroad, mistakeDirection);
        moveByDirection(actualCross, backToRoad);
    }

    private Direction findBackDirection(Crossroad mistakeCross, Integer backId){
        for(Direction direction: mistakeCross.getNeighbors().keySet()){
            Integer crossID = mistakeCross.getNeighbors().get(direction);
            if(backId.equals(crossID))
                return direction;
        }


        logError("Cannot move back to " + backId);
        stopTheTest();
        return null;
    }

    private boolean canDoMistake(Integer actual, Integer correct){
        Map<Direction, Integer> neighbors = mapController.getCrossRoads().get(actual).getNeighbors();

        if(neighbors.size() <= 1)
            return false;

        for(Direction direction: neighbors.keySet()){
            Integer next = neighbors.get(direction);
            if(!next.equals(correct) && clearWay(actual, next, direction))
                return true;
        }

        return false;
    }

    //The final position does not lie on this path.
    private boolean clearWay(Integer from, Integer to, Direction direction){
        CityMap.FinalWay finalWay = cityMap.getFinalWay();
        Direction opposite = Direction.getOppositeDirection(direction);
        return !finalWay.getFinish().equals(from) && !finalWay.getStart().equals(to) && opposite.ordinal() != finalWay.getDirection().ordinal();
    }


    private boolean doCorrect(){
        int[] scoreResults = OnTheTrailScoreResolver.scoreResults;

        if (expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR && totalScore >= scoreResults[2]) return false;
        else if(expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR && totalScore >= scoreResults[1]) return false;
        else return expectedResult != AbstractTablexiaGame.GameResult.NO_STAR || totalScore < scoreResults[0];
    }


    private void testMoveButtons(){
        moveButtons = new HashMap<>();
        MenuType menuType = MenuType.getMenuType(difficulty);

        for(Direction direction: menuType.getDirections()){
            writeToLogFile(incGetSteps() + ". Test move button for direction: " + direction);
            TablexiaButton button = (TablexiaButton) findGameActor(direction.name() + MainMenu.BUTTON_NAME_SUFFIX);
            moveButtons.put(direction, button);
            logOK();
        }
    }

    private void moveByDirection(Integer cId, Direction direction){
        moveCounter++;
        writeToLogFile(incGetSteps() + ". Move to crossroad (id: " + cId + ") by move" + direction);
        clickInTheActor(moveButtons.get(direction));
        logOK();

        waitForEvent(incGetSteps() + ". Wait for move("+moveCounter+") by direction" + direction,CityMap.MOVE_FINISHED + moveCounter);
    }

    private void checkFinalPosition(){
        writeToLogFile(incGetSteps() + ". Check final position");

        Vector2 expectedPosition = new Vector2(cityMap.getDiversionPosition());
        Vector2 actualPosition = new Vector2(
                detective.getX() + dW,
                detective.getY() + dH
        );

        checkExpectedPositionAndLog(expectedPosition, actualPosition);
    }

    private void checkActualPosition(Integer expected){
        writeToLogFile(incGetSteps() + ". Check position. Excepted position is on the crossroad (id: "+ expected +")");

        Crossroad crossroad = mapController.getCrossRoads().get(expected);
        Vector2 expectedPosition = cityMap.middleCameraPosition(crossroad.getCenter());
        Vector2 actualPosition = new Vector2(
                detective.getX() + dW,
                detective.getY() + dH
        );

        if(detective.getActualPosition().equals(expected)){
            checkExpectedPositionAndLog(expectedPosition, actualPosition);
        }else {
            logError("Wrong detective actual cross!. Actual: "+ detective.getActualPosition() +" Expected: " + expected);
            takeScreenShot();
            stopTheTest();
        }

    }

    private void checkCirclePosition(Integer expected){
        Crossroad crossroad = mapController.getCrossRoads().get(expected);
        Vector2 expectedPosition = cityMap.middleCameraPosition(crossroad.getCenter());

        Circle circle = mapController.getCircles().get(crossroad.getcID());
        Vector2 center = new Vector2(
                circle.getCenterX() + (cityMap.useMenuBorder() ? OnTheTrailGame.PANEL_WIDTH / 2 : 0),
                circle.getCenterY()
        );

        float rx = circle.getRX();
        float ry = circle.getRY();

        double angle = new Vector2(expectedPosition.x, expectedPosition.y ).sub(center).angleRad();

        Vector2 expectedCirclePosition = new Vector2(
                (center.x + rx*(float)Math.cos(angle)) - dW,
                (center.y + ry*(float)Math.sin(angle)) - dH);

        Vector2 actualPosition = new Vector2(
                detective.getX(),
                detective.getY()
        );

        if(detective.getActualPosition().equals(expected)){
            checkExpectedPositionAndLog(expectedCirclePosition, actualPosition);
        }else {
            logError("Wrong detective actual cross!. Actual: "+ detective.getActualPosition() +" Expected: " + expected);
            takeScreenShot();
            stopTheTest();
        }
    }

    private void checkExpectedPositionAndLog(Vector2 expectedPosition, Vector2 actualPosition){
        int eX = (int) expectedPosition.x;
        int eY = (int) expectedPosition.y;

        int aX = (int) actualPosition.x;
        int aY = (int) actualPosition.y;

        if(eX == aX && eY == aY){
            logOK();
        }else {
            logError("Wrong detective position!. Actual: "+ actualPosition.toString() +" Expected: " + expectedPosition.toString());
            takeScreenShot();
            stopTheTest();
        }
    }


    @Override
    protected void checkVictoryTable(int daoScore, Table table) {
        if(!checkScoreInTable(table, daoScore + "/" + OnTheTrailGameState.CROSSROADS_CNT)){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE + " with correct dao score: " + Integer.toString(daoScore) + "/" +  OnTheTrailGameState.CROSSROADS_CNT);
            stopTheTest();
        }
    }

    @Override
    protected void testVictoryData() {
        Game game = loadGameFromDao(getGameID());
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.ON_THE_TRAIL, game, Integer.valueOf(game.getGameScore(OnTheTrailScoreResolver.SCORE, "0")));
    }

    @Override
    protected int getCubsCount() {
        int[] scoreResults = OnTheTrailScoreResolver.scoreResults;
        if(totalScore >= scoreResults[2]) return 3;
        else if(totalScore >= scoreResults[1]) return 2;
        else if(totalScore >= scoreResults[0]) return 1;

        return 0;
    }

    @Override
    protected void printResult() {
        Game game = loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(OnTheTrailScoreResolver.SCORE, "0"));
        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n");
        logInfo("Game number: " + GameDefinition.ON_THE_TRAIL.getGameNumber() + "\n");
        logInfo("SCORE: " + daoScore + "\n");
        logInfo("STARS: " + getCubsCount() + "\n");
    }

}
