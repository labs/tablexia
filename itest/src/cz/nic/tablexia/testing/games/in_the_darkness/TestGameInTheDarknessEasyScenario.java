/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games.in_the_darkness;

import java.util.ArrayList;
import java.util.Arrays;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.in_the_darkness.action.InTheDarknessActionType;

/**
 * Created by lmarik on 24.2.17.
 * This test is for testing obstacles
 */

public class TestGameInTheDarknessEasyScenario extends TestGameInTheDarkness {


    public TestGameInTheDarknessEasyScenario(Tablexia tablexia) {
        super(tablexia);
        TablexiaSettings.GAMES_RANDOM_SEED = 1487856376706L;
        difficulty = GameDifficulty.EASY;
        expectedResult = AbstractTablexiaGame.GameResult.TWO_STAR;
        exceptedFail = 3;
        haveScenario = true;
        initSteps();
    }

    private void initSteps(){

        scenarioSteps.put(0, Arrays.asList(new InTheDarknessActionType[]{
                InTheDarknessActionType.GO,
                InTheDarknessActionType.LEFT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.LEFT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.RIGHT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
        }));

        scenarioSteps.put(1, Arrays.asList(new InTheDarknessActionType[]{
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.DOG,
                InTheDarknessActionType.GO,
        }));

        scenarioSteps.put(2,Arrays.asList(new InTheDarknessActionType[]{
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.LEFT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.RIGHT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.LEFT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
        }));

        scenarioSteps.put(3,Arrays.asList(new InTheDarknessActionType[]{
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.LEFT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.RIGHT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.LEFT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.DOOR,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.LEFT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.LEFT,
                InTheDarknessActionType.GO,
                InTheDarknessActionType.RIGHT,
                InTheDarknessActionType.GO,
        }));


    }
}
