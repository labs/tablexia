/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games.in_the_darkness.util;



import java.util.ArrayList;
import java.util.Comparator;

import java.util.PriorityQueue;
import java.util.Stack;

import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarkness;

/**
 * Created by lmarik on 22.2.17.
 */

public class AStar {
    private int[][] matrixMap;

    private Node[][] gridNodes;

    private PriorityQueue<Node> openNodes;

    private int startX;
    private int startY;

    private int endX;
    private int endY;

    private int maxStep;

    private int mapWidth,mapHeight;

    public AStar(int[][] matrixMap,int mapWidth,int mapHeight, int startX,int startY,int endX,int endY){
        this.startX = startX;
        this.startY = startY;

        this.endX = endX;
        this.endY = endY;

        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;

        this.matrixMap = matrixMap;

        maxStep = mapWidth*mapHeight;
        gridNodes = new Node[mapWidth][mapHeight];
        openNodes = new PriorityQueue<>(maxStep,new NodeComparator());

        for(int x = 0;x<mapWidth;x++){
            for(int y = 0;y<mapHeight;y++){
                Node newNode = new Node(x,y);
                newNode.setTypeStep(matrixMap[x][y]);
                newNode.setH( Math.abs(x-endX)+Math.abs(y-endY));
                gridNodes[x][y] = newNode;

            }
        }

        gridNodes[startX][startY].setF(0);

        System.out.println("ASTAR [ ------------ ready --------------]");
    }



    public Stack<Node> findPath(){
        Stack<Node> paths = new Stack<>();
        System.out.println("ASTAR [ ------------ start --------------]");
        openNodes.add(gridNodes[startX][startY]);

        Node current ;
        int steps=0;
        while (steps < maxStep || openNodes.isEmpty() ){
            steps++;
            current = openNodes.poll();


            if( current==null || current.getTypeStep() == TestGameInTheDarkness.TypeStep.FINISH.ordinal()){
                break;
            }

            current.setClosed(true);
            Node target;

            //check and update down node
            if (current.getY() + 1 < gridNodes[0].length) {
                target = gridNodes[current.getX()][current.getY()+1];
                checkAndUpdate(current,target);
            }

            //check and update up node
            if (current.getY() -1 >= 0){
                target = gridNodes[current.getX()][current.getY()-1];
                checkAndUpdate(current,target);
            }

            //check and update right node
            if (current.getX() +1 < gridNodes.length){
                target = gridNodes[current.getX()+1][current.getY()];
                checkAndUpdate(current,target);
            }

            //check and update left node
            if (current.getX() -1>= 0){
                target = gridNodes[current.getX() -1][current.getY()];
                checkAndUpdate(current,target);
            }

        }


        //prepare path form START to FINISH

        current = gridNodes[endX][endY];
        paths.push(current);
        while (current.getParent() !=null){
            paths.push(current.getParent());
            current = current.getParent();
            current.setInPath(true);
        }

        System.out.println("ASTAR [ ------------ finish--------------]");
        printGrid();
        return paths;
    }

    private void printGrid(){
        System.out.println("ASTAR [ ------------ grid map --------------]");

        for(int y =0;y<gridNodes[0].length;y++){
            for(int i = 0;i<gridNodes.length;i++){
                if(gridNodes[i][y].getTypeStep() == TestGameInTheDarkness.TypeStep.WALL.ordinal())  System.out.print("* ");
                else if((gridNodes[i][y].getTypeStep() != TestGameInTheDarkness.TypeStep.WALL.ordinal()) && !gridNodes[i][y].isInPath()) System.out.print(". ");
                else if((gridNodes[i][y].getTypeStep() != TestGameInTheDarkness.TypeStep.WALL.ordinal()) && gridNodes[i][y].isInPath()) System.out.print("+ ");

            }
            System.out.println();
        }
    }


    private void checkAndUpdate(Node current,Node target){
        if(target == null || target.getTypeStep() == TestGameInTheDarkness.TypeStep.WALL.ordinal() || target.isClosed()){
            return;
        }

        int targetNewF = target.getH() + 1;
        if(!openNodes.contains(target) || targetNewF < target.getF()){
            target.setF(targetNewF);
            target.setParent(current);

            if(!openNodes.contains(target)) {
                openNodes.add(target);
            }
        }
    }

    public void resetData(){
        gridNodes = new Node[mapWidth][mapHeight];
        openNodes = new PriorityQueue<>(maxStep,new NodeComparator());

        for(int x = 0;x<mapWidth;x++){
            for(int y = 0;y<mapHeight;y++){
                Node newNode = new Node(x,y);
                newNode.setTypeStep(matrixMap[x][y]);
                newNode.setH( Math.abs(x-endX)+Math.abs(y-endY));
                gridNodes[x][y] = newNode;

            }
        }

        gridNodes[startX][startY].setF(0);
    }

    public int getEndX() {
        return endX;
    }

    public void setEndX(int endX) {
        this.endX = endX;
    }

    public int getEndY() {
        return endY;
    }

    public void setEndY(int endY) {
        this.endY = endY;
    }

    public int getStartX() {
        return startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    public class NodeComparator implements Comparator<Node>{

        @Override
        public int compare(Node node1, Node node2) {
            return node1.getF()<node2.getF()?-1:
                    node1.getF()>node2.getF()?1:0;
        }
    }
}
