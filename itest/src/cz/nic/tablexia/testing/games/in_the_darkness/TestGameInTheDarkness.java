/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games.in_the_darkness;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.action.InTheDarknessActionType;
import cz.nic.tablexia.game.games.in_the_darkness.action.widget.ActionsWidget;
import cz.nic.tablexia.game.games.in_the_darkness.creature.Player;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobject.MapObjectType;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobstacle.MapObstacle;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobstacle.MapObstacleType;
import cz.nic.tablexia.game.games.in_the_darkness.map.tile.Tile;
import cz.nic.tablexia.game.games.in_the_darkness.map.tile.TileType;
import cz.nic.tablexia.game.games.in_the_darkness.map.widget.MapWidget;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.resolvers.InTheDarknessScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;
import cz.nic.tablexia.testing.games.in_the_darkness.util.AStar;
import cz.nic.tablexia.testing.games.in_the_darkness.util.Node;
import cz.nic.tablexia.util.ui.actionwidget.Action;
import cz.nic.tablexia.util.ui.actionwidget.ActionsStripWidget;

import static cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarkness.TypeStep.DOG;
import static cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarkness.TypeStep.DOOR;
import static cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarkness.TypeStep.FINISH;
import static cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarkness.TypeStep.GO;
import static cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarkness.TypeStep.KEY;
import static cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarkness.TypeStep.START;
import static cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarkness.TypeStep.WALL;
import static cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarkness.TypeStep.STAIRS;

/**
 * Created by lmarik on 21.2.17.
 */


public class TestGameInTheDarkness extends AbstractTestGame {

    public enum TypeStep{
        GO,
        WALL,
        START,
        FINISH,
        DOOR,
        DOG,
        KEY,
        STAIRS
    }

    private int startX = -1;
    private int startY = -1;

    private int finishX = -1;
    private int finishY = -1;

    private int keyX = -1;
    private int keyY = -1;

    private int stairsFirstFloorX = -1;
    private int stairsFirstFloorY = -1;
    private int stairsSecondFloorX = -1;
    private int stairsSecondFloorY = -1;
    private int stairsRevert = -1;
    private int stepCount= 0;

    private List<Node> openedDoors = new ArrayList<>();

    protected boolean                                       haveScenario   = false;
    protected Map<Integer,List<InTheDarknessActionType>>    scenarioSteps  = new HashMap<>();
    protected int                                           exceptedFail    = 0;

    private int xMoveFinish = 1000;
    private int yMoveFinish = 1000;
    private int mistakesCounter = 0;

    public TestGameInTheDarkness(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.IN_THE_DARKNESS);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at Game in the darkness button in the menu", GameMenuDefinition.IN_THE_DARKNESS);


        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);
        takeScreenShot();


        clickAt(incGetSteps() + ". Click preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);
        waitForEvent(incGetSteps() + ". Wait for event ready game",AbstractTablexiaGame.EVENT_GAME_READY);
        waitForEvent(incGetSteps() + ". Wait for event show animation",InTheDarknessGame.EVENT_SHOW_ANIMATION_FINISHED);

        xMoveFinish = (int) (getTablexia().getActualScreen().getSceneWidth() - InTheDarknessGame.ACTION_SIZE_BIGGER/2);
        yMoveFinish = (int) (getTablexia().getActualScreen().getViewportTopY() - InTheDarknessGame.ACTION_SIZE_BIGGER/2);

        //PLAY TUTORIAL
        setTutorialAction();
        clickAt(incGetSteps() + ". Click on start button",InTheDarknessGame.START_BUTTON);

        playTheGame(InTheDarknessGame.TUTORIAL_STEPS.size()*3,0,1);

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        takeScreenShot();

        clickButtonOnVictoryDialog(incGetSteps() +". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,2);
        waitForEvent(incGetSteps() + ". Wait for event ready game",AbstractTablexiaGame.EVENT_GAME_READY);
        waitForEvent(incGetSteps() + ". Wait for event show animation",InTheDarknessGame.EVENT_SHOW_ANIMATION_FINISHED);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        //PlAY ROUND
        takeScreenShot();
        if(!haveScenario) {
            if(difficulty == GameDifficulty.BONUS){
                int[][] matrixMapFirstFloor = parseMap(0);

                clickAt(incGetSteps() +  "Click at second floor button", "2 " + InTheDarknessGame.BOOKMARK_FLOOR_TEXT_KEY);
                waitForEvent(incGetSteps() +  "Wait for second floor", InTheDarknessGame.EVENT_SHOW_OTHER_FLOOR);

                int[][] matrixMapSecondFloor = parseMap(1);

                testStartAndFinishXAndY();

                //test stairs in first floor exists
               if (stairsFirstFloorX == -1 || stairsFirstFloorY == -1) {
                    logError("Test not found stairs in first floor map");
                    stopTheTest();
                }

                //test stairs in second floor exists
                if (stairsSecondFloorX == -1 || stairsSecondFloorY == -1) {
                    logError("Test not found stairs in second floor map");
                    stopTheTest();
                }

                clickAt(incGetSteps() +  "Click at first floor button", "1 " + InTheDarknessGame.BOOKMARK_FLOOR_TEXT_KEY);
                waitForEvent(incGetSteps() +  "Wait for first floor", InTheDarknessGame.EVENT_SHOW_OTHER_FLOOR);


                exceptedFail = getMistakesForStars(expectedResult);
                while(needMistake()){
                    setActionByAStarBonus(matrixMapFirstFloor, matrixMapSecondFloor, true);
                    startGame();
                    resetMenu();
                    mistakesCounter++;
                }
                setActionByAStarBonus(matrixMapFirstFloor, matrixMapSecondFloor);

                startGame();

            } else {
                int[][] matrixMap = parseMap(0);

                testStartAndFinishXAndY();

                exceptedFail = getMistakesForStars(expectedResult);
                while(needMistake()){
                    setActionByAStar(matrixMap, true);
                    startGame();
                    resetMenu();
                    mistakesCounter++;
                }

                setActionByAStar(matrixMap);

                startGame();
            }

        }else{
            playGameByScenario();
        }

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if(expectedResult != AbstractTablexiaGame.GameResult.NO_STAR){
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();

        clickButtonOnVictoryDialog(incGetSteps() +". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,1);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        printResult();
        takeScreenShot();

    }

    private boolean needMistake() {
        if(mistakesCounter < getMistakesForStars(expectedResult)) return true;
        else return false;
    }

    private int getMistakesForStars(AbstractTablexiaGame.GameResult expectedResult) {
        if(expectedResult == AbstractTablexiaGame.GameResult.NO_STAR) return InTheDarknessScoreResolver.ERROR_COUNT_ONE_STAR;
        else if (expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR) return InTheDarknessScoreResolver.ERROR_COUNT_TWO_STARS;
        else if (expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR) return InTheDarknessScoreResolver.ERROR_COUNT_THREE_STARS;
        else return 0; //three stars
    }

    private void testStartAndFinishXAndY() {
        //test start exists
        if (startX == -1 || startY == -1) {
            logError("Test not found start in map");
            stopTheTest();
        }

        //test safe exists
        if (finishX == -1 || finishY == -1) {
            logError("Test not found finish in map");
            stopTheTest();
        }
    }

    private void startGame(){
        clickAt(incGetSteps() + ". Click on start button",InTheDarknessGame.START_BUTTON);
        playTheGame(stepCount*4,0,1);
    }

    private void playGameByScenario(){
        ActionsWidget actionsWidget = (ActionsWidget) findGameActor(InTheDarknessGame.ACTIONS_WIDGET);

        if(scenarioSteps.isEmpty())
            return;

        List<Integer> keys = new ArrayList<>(scenarioSteps.keySet());

        for(int i = 0;i<keys.size();i++) {

            setActionByScenario(i, actionsWidget);
            startGame();

            if(i != keys.size() -1) {
                resetMenu();
                stepCount = 0;
            }
        }

    }

    private void setActionByScenario(Integer key, ActionsWidget actionsWidget){
        List<InTheDarknessActionType> types = scenarioSteps.get(key);
        for(InTheDarknessActionType type:types){

            Action action = findActionByActionType(type,actionsWidget.getAction());
            if(action!=null){
                dragActionToMenu(action);
                stepCount++;
            }else {
                logError("Action with number " + type.getActionNumber() + " not found");
                stopTheTest();
            }
        }
    }

    private void setActionByAStarBonus(int[][] matrixMapFirstFloor, int[][] matrixMapSecondFloor){
        setActionByAStarBonus(matrixMapFirstFloor, matrixMapSecondFloor, false);
    }

    private void setActionByAStarBonus(int[][] matrixMapFirstFloor, int[][] matrixMapSecondFloor, boolean makeMistake) {
        ActionsWidget actionsWidget = (ActionsWidget) findGameActor(InTheDarknessGame.ACTIONS_WIDGET);

        Player player = (Player) findGameActor(InTheDarknessGame.PLAYER);

        AStar aStar = new AStar(matrixMapFirstFloor,matrixMapFirstFloor.length,matrixMapFirstFloor[0].length,startX,startY,stairsFirstFloorX,stairsFirstFloorY);

        boolean findKey = false;
        if(keyX != -1 && keyY !=-1){
            findKey = true;
        }

        if (findKey){
            //first path for key first floor
            setEnd(aStar, keyX, keyY);
            aStar.resetData();
        }

        int playerPosition = player.getActualPlayerOrientation().ordinal();
        if(makeMistake)playerPosition = findPathAndSetActions(aStar, actionsWidget, playerPosition, true);
        else playerPosition = findPathAndSetActions(aStar, actionsWidget, playerPosition);

        if (playerPosition == -1){
            logError("A star find path failed!");
            stopTheTest();
        }
        if(makeMistake) return;

        if(findKey){
            //second path from key to stairs first floor
            addActionsByTime(InTheDarknessActionType.KEY,actionsWidget.getAction(),1);

            setStartAndEnd(aStar, keyX, keyY, stairsFirstFloorX, stairsFirstFloorY);
            findPathAndSetActions(aStar, actionsWidget, playerPosition);
        }

        addActionsByTime(InTheDarknessActionType.STAIRS,actionsWidget.getAction(),1);
        clickAt(incGetSteps() +  "Click at second floor button", "2 " + InTheDarknessGame.BOOKMARK_FLOOR_TEXT_KEY);
        waitForEvent(incGetSteps() +  "Wait for second floor", InTheDarknessGame.EVENT_SHOW_OTHER_FLOOR);

        AStar a2Star = new AStar(matrixMapSecondFloor,matrixMapSecondFloor.length,matrixMapSecondFloor[0].length,stairsSecondFloorX,stairsSecondFloorY,finishX,finishY);

        playerPosition = stairsRevert;

        boolean findFinish = false;
        if(finishX != -1 && finishY !=-1){
            findFinish = true;
        }

        if (findFinish){
            //path from stairs to finish second floor
            setEnd(a2Star, finishX, finishY);
            a2Star.resetData();
        }

        findPathAndSetActions(a2Star, actionsWidget, playerPosition);

        clickAt(incGetSteps() +  "Click at first floor button", "1 " + InTheDarknessGame.BOOKMARK_FLOOR_TEXT_KEY);
        waitForEvent(incGetSteps() +  "Wait for first floor", InTheDarknessGame.EVENT_SHOW_OTHER_FLOOR);

    }

    private int findPathAndSetActions(AStar aStar, ActionsWidget actionsWidget, int playerPosition){
        return findPathAndSetActions(aStar, actionsWidget, playerPosition, false);
    }

    private int findPathAndSetActions(AStar aStar, ActionsWidget actionsWidget, int playerPosition, boolean makeMistake) {
        Stack<Node> path = aStar.findPath();
        if (path.isEmpty()){
            logError("A star cannot find path");
            stopTheTest();
        }
        //pop start node
        path.pop();

        return setActionByPath(path,actionsWidget,playerPosition, makeMistake);
    }

    private void setStartAndEnd(AStar aStar, int startX, int startY, int endX, int endY) {
        setStart(aStar, startX, startY);
        setEnd(aStar, endX, endY);
        aStar.resetData();
    }

    private void setEnd(AStar aStar, int endX, int endY) {
        aStar.setEndX(endX);
        aStar.setEndY(endY);
    }

    private void setStart(AStar aStar, int startX, int startY) {
        aStar.setStartX(startX);
        aStar.setStartY(startY);
    }

    private void setActionByAStar(int[][] matrixMap){
        setActionByAStar(matrixMap, false);
    }

    private void setActionByAStar(int[][] matrixMap, boolean makeMistake){
        ActionsWidget actionsWidget = (ActionsWidget) findGameActor(InTheDarknessGame.ACTIONS_WIDGET);

        Player player = (Player) findGameActor(InTheDarknessGame.PLAYER);

        AStar aStar = new AStar(matrixMap,matrixMap.length,matrixMap[0].length,startX,startY,finishX,finishY);

        boolean findKey = false;
        if(keyX != -1 && keyY !=-1){
            findKey = true;
        }

        if (findKey){
            //first path for key
            setEnd(aStar, keyX, keyY);
            aStar.resetData();
        }

        int playerPosition = player.getActualPlayerOrientation().ordinal();
        playerPosition = findPathAndSetActions(aStar, actionsWidget, playerPosition, makeMistake);

        if (playerPosition == -1){
            logError("A start find path failed!");
            stopTheTest();
        }

        if(makeMistake) return;

        if(findKey){
            //second path from key to safe
            addActionsByTime(InTheDarknessActionType.KEY,actionsWidget.getAction(),1);

            setStartAndEnd(aStar, keyX, keyY, finishX, finishY);
            findPathAndSetActions(aStar, actionsWidget, playerPosition);
        }
    }

    private int setActionByPath(Stack<Node> path, ActionsWidget actionsWidget,int playerPosition, boolean makeMistake ){
        Node current;
        Node next;

        int needOrientation = -1;
        while (!path.isEmpty()){
            current = path.pop();
            next = path.pop();

            //check player position
            needOrientation = getNeedPlayerOrientation(current,next);

            if (needOrientation == -1){
                logError("Cannot set orientation in path");
                stopTheTest();
                break;
            }

            //Set action for change orientation
            if(playerPosition != needOrientation){
                int diff = needOrientation - playerPosition;
                int times = Math.abs(diff);
                if(times >= 3){
                    //Edit to change the orientation of a smaller number of Step
                    times = 4-times;
                    diff*=-1;
                }


                stepCount+=times;
                if(diff < 0){
                    if(makeMistake){
                        addActionsByTime(InTheDarknessActionType.RIGHT,actionsWidget.getAction(),times);
                        addActionsByTime(InTheDarknessActionType.GO, actionsWidget.getAction(), 1);
                        stepCount++;
                        return needOrientation;
                    }
                    else addActionsByTime(InTheDarknessActionType.LEFT,actionsWidget.getAction(),times);
                }else{
                    if(makeMistake){
                        addActionsByTime(InTheDarknessActionType.LEFT,actionsWidget.getAction(),times);
                        addActionsByTime(InTheDarknessActionType.GO, actionsWidget.getAction(), 1);
                        stepCount++;
                        return needOrientation;
                    }
                    else addActionsByTime(InTheDarknessActionType.RIGHT,actionsWidget.getAction(),times);
                }
            }

            //Check door
            if(current.getTypeStep() == DOOR.ordinal() && !isOpenedDoor(current)){
                if(makeMistake){
                    addActionsByTime(InTheDarknessActionType.GO, actionsWidget.getAction(), 1);
                    stepCount++;
                    return needOrientation;
                }
                else{
                    addActionsByTime(InTheDarknessActionType.DOOR,actionsWidget.getAction(),1);
                    openedDoors.add(current);
                }
                stepCount++;
            }

            //Check dog - true set action for jump dog - false - set action to go
            if(current.getTypeStep() == DOG.ordinal()){
                addActionsByTime(InTheDarknessActionType.DOG, actionsWidget.getAction(), 1);
                stepCount++;
            }else {
                addActionsByTime(InTheDarknessActionType.GO, actionsWidget.getAction(), 1);
                stepCount++;
            }
            playerPosition = needOrientation;

        }

        return needOrientation;
    }

    private boolean isOpenedDoor(Node current){
        for(Node node:openedDoors){
            if(node.getX() == current.getX() && node.getY()==current.getY())
                return true;
        }

        return false;
    }

    private int getNeedPlayerOrientation(Node current,Node next){

        if(current.getX() == next.getX() && (current.getY() - next.getY()) > 0)  return Player.PlayerOrientation.TOP.ordinal();
        else if (current.getX() == next.getX() && (current.getY() - next.getY()) < 0) return  Player.PlayerOrientation.BOTTOM.ordinal();
        else if(current.getY() == next.getY() && (current.getX() - next.getX()) > 0)  return Player.PlayerOrientation.LEFT.ordinal();
        else  if(current.getY() == next.getY() && (current.getX() - next.getX()) < 0)  return Player.PlayerOrientation.RIGHT.ordinal();

        return -1;
    }


    private int[][] parseMap(int floor){
        MapWidget mapWidget = (MapWidget) findGameActor(InTheDarknessGame.MAP_WIDGET + floor);

        Player player = (Player) findGameActor(InTheDarknessGame.PLAYER);

        TileMap tileMap = mapWidget.getTileMap();

        int xSize = tileMap.getMapXSize()*2 + 1;
        int ySize = tileMap.getMapYSize()*2 + 1;
        int[][] matrixMap = new int[xSize][ySize];

        // set all vaule on Wall
        for(int i = 0; i<ySize;i++){
            for(int k = 0; k<xSize;k++){
                matrixMap[k][i] = WALL.ordinal();
            }
        }

        //parse map to matrix with free path and wall
        for(int i = 0; i<tileMap.getMapYSize();i++){
            for(int k = 0; k<tileMap.getMapXSize();k++){
                Tile tile = tileMap.getMap()[k][i];

                int xInMatrix = k == 0 ? 1 : (k*3 -(k-1));
                int yInMatrix = i == 0 ? 1 : (i*3 -(i-1));

                TileType tileType = tile.getTileType();
                int placeValue = getPlaceValue(tile);
                if(placeValue == FINISH.ordinal()){
                    finishX = xInMatrix;
                    finishY = yInMatrix;
                    if(difficulty == GameDifficulty.BONUS && floor!=1){
                        logError("Test not found finish in second floor");
                        stopTheTest();
                    }
                }

                if(placeValue == KEY.ordinal()){
                    keyX = xInMatrix;
                    keyY = yInMatrix;
                    if(difficulty == GameDifficulty.BONUS && floor!=0){
                        logError("Test not found key in first floor");
                        stopTheTest();
                    }
                }

                if(placeValue == STAIRS.ordinal() && floor == 0){
                    stairsFirstFloorX = xInMatrix;
                    stairsFirstFloorY = yInMatrix;
                }

                if(placeValue == STAIRS.ordinal() && floor == 1){
                    stairsSecondFloorX = xInMatrix;
                    stairsSecondFloorY = yInMatrix;
                    if (tileType.isLeftDoor()) {
                        stairsRevert = 3;
                    } else if (tileType.isTopDoor()) {
                        stairsRevert = 0;
                    } else if (tileType.isBottomDoor()) {
                        stairsRevert = 2;
                    } else if (tileType.isRightDoor()) {
                        stairsRevert = 1;
                    }
                }

                setDataToMatrixMap(matrixMap,xInMatrix,yInMatrix, tile.getMapObstacles() ,getPlaceValue(tile),tileType.isTopDoor(),tileType.isBottomDoor(),tileType.isLeftDoor(),tileType.isRightDoor());
            }
        }

        //set start in matrix
        startX = player.getActualTileMapPosition().getPositionX() == 0 ? 1 : (player.getActualTileMapPosition().getPositionX()*3 -(player.getActualTileMapPosition().getPositionX()-1));
        startY = player.getActualTileMapPosition().getPositionY() == 0 ? 1 : (player.getActualTileMapPosition().getPositionY()*3 -(player.getActualTileMapPosition().getPositionY()-1));
        matrixMap[startX][startY] = START.ordinal();

        return matrixMap;
    }


    private int getPlaceValue(Tile tile){
        if(tile.getTileType() == TileType.TILE_0)
            return WALL.ordinal();

        if(tile.getMapObject() != null && tile.getMapObject().getMapObjectType() == MapObjectType.KEY){
            return KEY.ordinal();
        }

        if(tile.getMapObject() != null && tile.getMapObject().getMapObjectType() == MapObjectType.SAFE){
            return FINISH.ordinal();
        }

        if(tile.getMapObject() != null && tile.getMapObject().getMapObjectType() == MapObjectType.STAIRS){
            return STAIRS.ordinal();
        }

        return GO.ordinal();
    }

    private void setDataToMatrixMap(int[][] matrix, int xInMatrix, int yInMatrix, MapObstacle[] mapObstacles, int place, boolean upWall, boolean downHall, boolean leftHall, boolean rightHall){
        matrix[xInMatrix][yInMatrix] = place;
        matrix[xInMatrix][yInMatrix-1] = matrix[xInMatrix][yInMatrix-1] != WALL.ordinal() ? matrix[xInMatrix][yInMatrix-1] : boolToInt(!upWall);
        matrix[xInMatrix][yInMatrix+1] = matrix[xInMatrix][yInMatrix+1] != WALL.ordinal() ? matrix[xInMatrix][yInMatrix+1] : boolToInt(!downHall);
        matrix[xInMatrix-1][yInMatrix] = matrix[xInMatrix-1][yInMatrix] != WALL.ordinal() ? matrix[xInMatrix-1][yInMatrix] : boolToInt(!leftHall);
        matrix[xInMatrix+1][yInMatrix] = matrix[xInMatrix+1][yInMatrix] != WALL.ordinal() ? matrix[xInMatrix+1][yInMatrix] : boolToInt(!rightHall);

        //Add map Obstacles to matrix
        if(mapObstacles != null){
            for(MapObstacle obstacle:mapObstacles){
                addObstacleToMatrixMap(matrix,xInMatrix,yInMatrix,obstacle);
            }
        }
    }

    private void addObstacleToMatrixMap(int[][] matrix,int xInMatrix, int yInMatrix,MapObstacle obstacle){
        if(obstacle == null)
            return;

        int obstacleType = 0;
        if(obstacle.getMapObstacleType() == MapObstacleType.DOG_H || obstacle.getMapObstacleType() == MapObstacleType.DOG_V){
            obstacleType = DOG.ordinal();
        }else if(obstacle.getMapObstacleType() == MapObstacleType.DOOR_H || obstacle.getMapObstacleType()==MapObstacleType.DOOR_V){
            obstacleType = DOOR.ordinal();
        }

        for(MapObstacleType.MapObstaclePosition position : obstacle.getMapObstacleType().getObstacleAvailablePositions()){
            if(position == MapObstacleType.MapObstaclePosition.LEFT_POSITION){
                matrix[xInMatrix-1][yInMatrix] = obstacleType;
            }else if(position == MapObstacleType.MapObstaclePosition.BOTTOM_POSITION){
                matrix[xInMatrix][yInMatrix+1] = obstacleType;
            }
        }

    }

    private int boolToInt(boolean bool){
        return bool == true ? 1 : 0;
    }

    private void addActionsByTime(InTheDarknessActionType type,List<Action> actions,int times){
        int i = 0;
        while (i != times){
            i++;
            Action action = findActionByActionType(type,actions);
            if(action!=null){
                dragActionToMenu(action);
            }else {
                logError("Action with number " + InTheDarknessGame.TUTORIAL_STEPS.get(i).getActionNumber() + " not found");
                stopTheTest();
            }
        }
    }

    private void setTutorialAction(){
        ActionsWidget actionsWidget = (ActionsWidget) findGameActor(InTheDarknessGame.ACTIONS_WIDGET);

        for(int i = 0;i < InTheDarknessGame.TUTORIAL_STEPS.size();i++){
            Action action = findActionByActionType(InTheDarknessGame.TUTORIAL_STEPS.get(i), actionsWidget.getAction());
            if(action!=null){
                dragActionToMenu(action);
            }else {
                logError("Action with number " + InTheDarknessGame.TUTORIAL_STEPS.get(i).getActionNumber() + " not found");
                stopTheTest();
            }
        }
    }

    private void resetMenu(){

        final Stage stage = getTablexia().getActualScreen().getStage();

        final float duration = 0.5f;

        ActionsStripWidget menu = (ActionsStripWidget)findGameActor(InTheDarknessGame.ACTIONS_STRIP_MENU);

        waitForEvent(incGetSteps() + ". Wait for move scroll top event", ActionsStripWidget.EVENT_SCROLL_FINISHED);

        while (menu.getSelectedActions().size() > 0){
            logInfo(incGetSteps() + ". Remove action: " + (menu.getSelectedActions().size()));
            Action removed = menu.getSelectedActions().get(menu.getSelectedActions().size()-1).getAction();

            int xStart = (int) (menu.getX() + removed.getX() + removed.getWidth()/2);
            int yStart = (int) (menu.getY() + menu.getScrollPaneActualPosition() - removed.getHeight()/2+ (menu.getSelectedActions().size()*(removed.getHeight() + menu.getActionOffset())));

            boolean result = touchDown(xStart,yStart,0,Input.Buttons.LEFT,stage);

            result &= dragOverTime(xStart,yStart,(int)(menu.getX() -removed.getWidth()),yStart,0,duration,30,stage);
            result &= touchUp((int)(menu.getX() -removed.getWidth()),yStart,0,Input.Buttons.LEFT,stage);

            if(!result){
                logError("Cannot remove action " + removed.getActionNumber());
                stopTheTest();
            }

            logOK();

        }

    }

    private void dragActionToMenu(Action action){
        ActionsStripWidget menu = (ActionsStripWidget)findGameActor(InTheDarknessGame.ACTIONS_STRIP_MENU);

        Rectangle rectangleAction = action.getReactionArea();

        int xStart =(int) (rectangleAction.getX() + rectangleAction.getWidth()/2);
        int yStart =(int) (rectangleAction.getY() + rectangleAction.getHeight()/2);

        moveActionToMenu(incGetSteps() + ". Move action with action number: " + action.getActionNumber() + " to menu",xStart,yStart,xMoveFinish,yMoveFinish,action,menu);

        waitForEvent(incGetSteps() + ". Wait for event card drop down" , ActionsWidget.EVENT_DROP_DOWN);
    }

    private Action findActionByActionType(InTheDarknessActionType type, List<Action> actions){

        for(Action action: actions){
            if(action.getActionNumber() == type.getActionNumber()) {
                return action;
            }
        }

        return null;
    }


    @Override
    protected void checkVictoryTable(int daoScore, Table table) {
        if(!checkScoreInTable(table,Integer.toString(daoScore))){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE+ " with correct fail count: " + Integer.toString(daoScore));
            stopTheTest();
        }
    }

    @Override
    protected void testVictoryData() {
        int id = (difficulty == GameDifficulty.BONUS) ? GameDifficulty.BONUS_DIFFICULTY_UNLOCK_THRESHOLD + 2 : 2;
        Game game = loadGameFromDao(id);

        int daoScore = Integer.valueOf(game.getGameScore(InTheDarknessScoreResolver.SCORE_KEY_ERRORS_COUNT,"0"));
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.IN_THE_DARKNESS,game,daoScore);

    }

    @Override
    protected int getCubsCount() {
        if(exceptedFail >= InTheDarknessScoreResolver.ERROR_COUNT_ONE_STAR) return 0;
        else if(exceptedFail >= InTheDarknessScoreResolver.ERROR_COUNT_TWO_STARS) return 1;
        else if (exceptedFail >= InTheDarknessScoreResolver.ERROR_COUNT_THREE_STARS) return 2;

        return 3;
    }

    @Override
    protected void printResult() {

        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n");
        logInfo("GAME NUMBER: " + GameDefinition.IN_THE_DARKNESS.getGameNumber()+ "\n");
        logInfo("FAIL: " + exceptedFail+ "\n");
        logInfo("STAR: " + getCubsCount()+ "\n");

    }


}
