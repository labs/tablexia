/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Timer;

import net.engio.mbassy.listener.Handler;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.testing.AbstractTestScenario;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.actionwidget.Action;
import cz.nic.tablexia.util.ui.actionwidget.ActionsStripWidget;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TwoButtonContentDialogComponent;

/**
 * Created by lmarik on 14.2.17.
 */

public abstract class AbstractTestGame extends AbstractTestScenario {
    protected final String  SCENARIO_GAME_ACTION            = "game action";

    protected GameDifficulty                    difficulty      = GameDifficulty.EASY;
    protected AbstractTablexiaGame.GameResult   expectedResult  = AbstractTablexiaGame.GameResult.THREE_STAR;

    protected   int gameScore = 0;
    private     int testSteps = 0;



    protected abstract void checkVictoryTable(int daoScore,Table table);
    protected abstract void testVictoryData();
    protected abstract int getCubsCount();
    protected abstract void printResult();

    protected AtomicBoolean gameFinished = new AtomicBoolean(false);

    @Handler
    public void onGameFinished(AbstractTablexiaGame.GameFinishedEvent event) {
        gameFinished.set(true);
        wakeUp(WAIT_LOCK);
    }

    public AbstractTestGame(Tablexia tablexia) {
        super(tablexia);
    }

    protected void logGameInfo(){
        logInfo("Game difficulty: " + difficulty + "\n"+
                "Expected result: " + expectedResult + "\n");
    }



    protected void checkVictoryDialog(String infoMessage, String dialogName, GameDefinition definition,Game game,int daoScore){
        logInfo(infoMessage);
        TablexiaComponentDialog victoryDialog = getDialog(dialogName);
        int cubsCnt = getCubsCount();

        if(victoryDialog != null){
            Table resultTable = (Table) findGameActor(AbstractTablexiaGame.RESULT_TABLE);

            int visibleCubsCnt = getCntVisibleCubs();
            if(cubsCnt != visibleCubsCnt){
                logError("Visible cubs on dialog (" + visibleCubsCnt+") is different than rank start (" + cubsCnt + ")");
                stopTheTest();
            }

            checkVictoryTable(daoScore,resultTable);

            if(game.getGameNumber() != definition.getGameNumber()){
                logError("Dao game number (" + game.getGameNumber() + ") is different than test game number (" + definition.getGameNumber() + ")" );
                stopTheTest();
            }


            logOK();
        }else{
            logError("Dialog " + dialogName + "not found!");
            stopTheTest();
        }
    }

    protected boolean checkScoreInTable(Table table,String score){

        for(Cell cell: table.getCells()){
            if(cell.getActor() instanceof TablexiaLabel){
                TablexiaLabel findLabel = (TablexiaLabel) cell.getActor();
                if(findLabel.getText().toString().contains(score)){
                    return  true;
                }
            }
        }

        return false;

    }

    protected void moveActionToMenu(String infoMessage,int xStart, int yStart, int xFinish, int yFinish, Action action, ActionsStripWidget menu){
        logInfo(infoMessage);

        final Stage stage = action.getStage();
        final int firstFingerPointer  = 0;
        final float duration = 0.5f;

        int sizeBefore = menu.getSelectedActions().size();


        boolean result = touchDown(xStart,yStart,firstFingerPointer,Input.Buttons.LEFT,stage);

        result &= dragOverTime(xStart,yStart,xFinish,yFinish,firstFingerPointer,duration,30,stage); Log.info(getClass(),"TOUCH  " + xFinish + " - " + yFinish);
        result &= touchUp(xFinish,yFinish,firstFingerPointer,Input.Buttons.LEFT,stage);

        if(!result){
            logError("Cannot move action to menu " + action.getActionNumber());
            stopTheTest();
        }

        int sizeAfter = menu.getSelectedActions().size();

        if(sizeAfter == 0){
            logError("Menu is empty after move action");
            stopTheTest();
        }

        Action lastAction = menu.getSelectedActions().get(sizeAfter-1).getAction();

        if(sizeBefore == sizeAfter || action.getActionNumber() != lastAction.getActionNumber()){
            logError("Move action failed. Action number: " + action.getActionNumber() + " Start: " + xStart +  " - " + yStart + " Finish: " + xFinish + " - " + yFinish);
            stopTheTest();
        }

        logOK();
    }

    protected void clickOnGameObject(String infoMessage, Actor target){
        logInfo(infoMessage);

        float shootX = target.getX() + target.getWidth()/2;
        float shootY = target.getY() + target.getHeight()/2;

        boolean result = touchDown(shootX,shootY,0, Input.Buttons.LEFT,target.getStage());
        result &= touchUp(shootX,shootY,0,Input.Buttons.LEFT,target.getStage());

        if(!result){
            logError("Can not click on game object " + target.toString());
            stopTheTest();
        }

        logOK();

    }

    private int getCntVisibleCubs(){
        int cnt = 0;
        for(int i = 0; i< AbstractTablexiaGame.GameResult.values().length -1;i++){
            int index = i +1;
            if(findActorByName(AbstractTablexiaGame.IMAGE_CUBS + index)!=null){
                  cnt++;
            }
        }

        return cnt;
    }



    protected void clickButtonOnVictoryDialog(String infoMessage, String dialogName, int numberOfButton){
        logInfo(infoMessage);
        TablexiaComponentDialog victoryDialog = getDialog(dialogName);

        if(victoryDialog != null){

            TwoButtonContentDialogComponent buttons = findTwoButtonContent(victoryDialog.getDialogComponents());

            if(buttons == null){
                logError("Dialog " + dialogName + " dont have button component");
                stopTheTest();
            }
            if(numberOfButton == 1)
                clickInTheActor(buttons.getFirstButton().getButton());
            else
                clickInTheActor(buttons.getSecondButton().getButton());

            logOK();
        }else{
            logError("Dialog " + dialogName + "not found!");
            stopTheTest();
        }
    }



    private TwoButtonContentDialogComponent findTwoButtonContent(List<TablexiaDialogComponentAdapter> components){
        for(TablexiaDialogComponentAdapter component : components){
            if(component instanceof TwoButtonContentDialogComponent){
                return (TwoButtonContentDialogComponent) component;
            }
        }

        return null;
    }

    protected boolean clickGameOnMenu(String infoMessage, GameMenuDefinition definition){
        return clickMenuButton(infoMessage,definition.toString());
    }

    protected Actor findGameActor(String actorName){
        Actor actor = findActorByName(actorName);
        if(actor ==null){
            logError("Actor " + actorName + " not found!");
            stopTheTest();
        }

        return actor;
    }

    protected Game loadGameFromDao(int id){
        Game game = GameDAO.getGameForId(id);
        if(game == null){
            logError("Game was not save in database");
            stopTheTest();
        }

        return game;
    }

    protected void playTheGame(int gameTime, float delay,float interVal) {

        Timer timer = new Timer();
        timer.scheduleTask(new Timer.Task() {

            @Override
            public void run() {
                if(gameFinished.get()){
                    cancel();
                }else {
                    doGameAction();
                }
            }
        }, delay, interVal, gameTime);
        timer.start();

        wait(gameTime * 1000);
    }

    protected void doGameAction(){

    };

    protected void setGameDificulty(){
        switch (difficulty){
            case MEDIUM:
                clickAt(incGetSteps() + ". Set difficulty on medium", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
                break;
            case HARD:
                clickAt(incGetSteps() + ". Set difficulty on hard", GamePageGroup.DIFFICULTY_BUTTON_HARD);
                break;
            case EASY:
                clickAt(incGetSteps() + ". Set difficulty on easy", GamePageGroup.DIFFICULTY_BUTTON_EASY);
                break;
            case BONUS:
                clickAt(incGetSteps() + ". Set difficulty on bonus", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        }
    }

    protected void setDataForUnlockBonus(User user, GameDefinition gameDefinition){
        logInfo(incGetSteps() + ". Set game score to open bonus level.\n");
        for (int i = 0; i < GameDifficulty.BONUS_DIFFICULTY_UNLOCK_THRESHOLD; i++){
            setGameScore(user,GameDifficulty.HARD, gameDefinition, AbstractTablexiaGame.GameResult.THREE_STAR);
        }

    }

    protected int getGameID(){
        return difficulty == GameDifficulty.BONUS ? GameDifficulty.BONUS_DIFFICULTY_UNLOCK_THRESHOLD + 1 : 1;
    }

    protected void checkBonusButtonEnable(GameDefinition gameDefinition, User user){
        if(difficulty != GameDifficulty.BONUS)
            return;

        if(!GameDAO.isBonusDifficultyUnlocked(gameDefinition.getGameNumber(), user.getId())){
            logError("Bonus difficulty is not unlocked");
            stopTheTest();
        }
    }

    protected int incGetSteps(){
        testSteps++;
        return testSteps;
    }
}
