/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games.runes;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.runes.RunesGame;
import cz.nic.tablexia.game.common.ui.health_bar.HealthBar;
import cz.nic.tablexia.game.games.runes.actors.IRune;
import cz.nic.tablexia.game.games.runes.actors.Rune;
import cz.nic.tablexia.game.games.runes.actors.RunesCluster;
import cz.nic.tablexia.game.games.runes.actors.RunesHolder;
import cz.nic.tablexia.game.games.runes.actors.TargetPlate;
import cz.nic.tablexia.game.games.runes.helper.RuneDescription;
import cz.nic.tablexia.game.games.runes.model.RunesGameProperties;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.resolvers.RunesScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;

/**
 * Created by lmarik on 16.3.17.
 */

public class TestGameRunes extends AbstractTestGame {

    public TestGameRunes(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {

        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.RUNES);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at runes game button in the menu", GameMenuDefinition.RUNES);


        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event ready game", AbstractTablexiaGame.EVENT_GAME_READY);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");
        takeScreenShot();

        playGame();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if(expectedResult != AbstractTablexiaGame.GameResult.NO_STAR){
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();

        clickButtonOnVictoryDialog(incGetSteps() +". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,1);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);

        printResult();
        takeScreenShot();

    }

    private void playGame(){
        TargetPlate targetPlate = (TargetPlate) findGameActor(RunesGame.TARGET_PLATE);
        Group runes = (Group) findGameActor(RunesGame.RUNES_GROUP);
        HealthBar healthBar = (HealthBar) findGameActor(RunesGame.HEALTH_BAR);

        int round = 0;
        int mistakesCounter = 0;

        while (round < RunesGameProperties.GAME_ROUNDS){
            if(round > 0) waitForEvent(incGetSteps() + ". Wait for event round ready", RunesGame.EVENT_ROUND_READY);
            round++;

            boolean mistakeInRound = playRound(targetPlate,runes);
            if(mistakeInRound==true) mistakesCounter++;

            if(!healthBar.hasHealthsLeft() || mistakesCounter >= 2 || round >= RunesGameProperties.GAME_ROUNDS){
                break;
            }

        }
    }

    private boolean playRound(TargetPlate targetPlate, Group runes){
        List<RunesHolder> runesHolders = getRunesHolders(runes);

        int indexForGet = 0;

        boolean mistakeInRound = false;

        takeScreenShot();

        int size = targetPlate.getActiveTargetsDescriptions().size();

        for(int i = 0 ; i < size ; i++){

            RuneDescription targetRune = targetPlate.getActiveTargetsDescriptions().get(indexForGet);
            RunesHolder holderWithTarget = getRuneHolderWithTarget(targetRune,runesHolders);

            if(holderWithTarget == null){
                logError("Cant find rune " + targetRune + " in the holders");
                stopTheTest();
            }

            //Click on holder
            if(TablexiaSettings.getInstance().isRunningOnMobileDevice()) clickOnHolder(incGetSteps() + ". Click on holder with target: " + targetRune,holderWithTarget);

            //Click on rune
            if(doMistake()){
                clickOnRune(incGetSteps() + ". Click on wrong rune: ", findWrongRune(targetRune,holderWithTarget));
                indexForGet++;
            }else{
                clickOnRune(incGetSteps() + ". Click on correct rune: " + targetRune,findTargetInHolder(targetRune,holderWithTarget));
                gameScore++;
            }

            if (i < (size-1)) waitForEvent(incGetSteps() + ". Wait for event correct/wrong rune", IRune.EVENT_ANIMATE_RUNE_DONE);

        }

        if(!targetPlate.getActiveTargetsDescriptions().isEmpty()){
            mistakeInRound = true;
        }

        return mistakeInRound;
    }

    private Rune findWrongRune(RuneDescription targetRune,RunesHolder holder){
        // TODO: 4/26/21 change to interface method
        for(Actor child: holder.getChildren().items){
            if(child instanceof Rune){
                Rune rune = (Rune) child;
                if(rune.getType() != targetRune || (findFlippedRunes() && rune.getType() == targetRune && !rune.isFlipped()))
                    return rune;
            }

            if(child instanceof RunesCluster){
                RunesCluster cluster = (RunesCluster) child;
                if(cluster.getRune1() != null && cluster.getRune2() != null && cluster.getRune1().getType() != targetRune && cluster.getRune2().getType() != targetRune)
                    return cluster.getRune1();
            }
        }

        return null;
    }

    private boolean doMistake(){
        if(expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR && gameScore >= RunesScoreResolver.CUPS[difficulty.getDifficultyNumber()-1][2] -1) return true;
        else if(expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR && gameScore >= RunesScoreResolver.CUPS[difficulty.getDifficultyNumber()-1][1] -1) return true;
        else if(expectedResult == AbstractTablexiaGame.GameResult.NO_STAR && gameScore >= RunesScoreResolver.CUPS[difficulty.getDifficultyNumber()-1][0] - 1) return true;

        return false;
    }

    private void clickOnRune(String infoMessage,Rune rune){

        logInfo(infoMessage);

        if(rune == null){
            logError("Rune " + rune.getRuneType() + " is null");
            stopTheTest();
        }

        if(!clickInTheActor(rune)){
            logError("Cannot click on the" + rune.getRuneType() + " rune");
            stopTheTest();
        }

        logOK();
    }

    private void clickOnHolder(String infoMessage,RunesHolder holder){
        logInfo(infoMessage);

        if(!clickInTheActor(holder)){
            logError("Cannot click on the rune holder");
            stopTheTest();
        }

        logOK();
    }

    private RunesHolder getRuneHolderWithTarget(RuneDescription targetRune,List<RunesHolder> runesHolders){
        for(RunesHolder runesHolder:runesHolders){
            if(findTargetInHolder(targetRune,runesHolder) != null)
                return runesHolder;
        }

        return null;
    }

    private Rune findTargetInHolder(RuneDescription targetRune,RunesHolder holder){
        // TODO: 4/26/21 change to interface method
        for(Actor child: holder.getChildren().items){
            if(child instanceof Rune && ((Rune) child).getRuneType() == targetRune.getRuneDefinition()){
                Rune rune = (Rune) child;
                //Check flipped rune for hard difficulty
                if(!findFlippedRunes()) {
                    return rune;
                }else if(rune.isFlipped()) {
                    return rune;
                }
            }

            if(child instanceof RunesCluster){
                RunesCluster cluster = (RunesCluster) child;
                if(cluster.getRune1() !=null && cluster.getRune1().getRuneType() == targetRune.getRuneDefinition()) {
                    return cluster.getRune1();
                }

                if(cluster.getRune2() !=null && cluster.getRune2().getRuneType() == targetRune.getRuneDefinition()) {
                    return cluster.getRune2();
                }
            }
        }

        return null;
    }

    private boolean findFlippedRunes(){
        return difficulty == GameDifficulty.HARD;
    }

    private List<RunesHolder> getRunesHolders(Group runes){
        List<RunesHolder> runesHolders = new ArrayList<>();
        for(Actor child: runes.getChildren().items){
            if(child instanceof RunesHolder){
                runesHolders.add((RunesHolder) child);
            }
        }

        return runesHolders;
    }

    @Override
    protected void checkVictoryTable(int daoScore, Table table) {
        String textScore = Integer.toString(daoScore) + "/" + Integer.toString(RunesGameProperties.RUNES_TO_FIND_TOTAL);
        if(!checkScoreInTable(table, textScore)){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE+ " with correct fail count: " + textScore);
            stopTheTest();
        }

    }

    @Override
    protected void testVictoryData() {
        Game game = loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(RunesScoreResolver.SCORE_TOTAL,"0"));
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.RUNES,game,daoScore);
    }

    @Override
    protected int getCubsCount() {
        int[] levels = RunesScoreResolver.CUPS[difficulty.getDifficultyNumber()-1];

        if(gameScore > levels[2]) return 3;
        else if (gameScore > levels[1]) return 2;
        else if (gameScore > levels[0]) return 1;

        return 0;
    }

    @Override
    protected void printResult() {
        Game game = loadGameFromDao(getGameID());
        int daoScore = Integer.valueOf(game.getGameScore(RunesScoreResolver.SCORE_TOTAL,"0"));

        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() +  "\n");
        logInfo("Game number: " + GameDefinition.RUNES.getGameNumber()+ "\n");
        logInfo("SCORE: " + daoScore +"\n");
        logInfo("STARS: " + getCubsCount()+"\n");
    }


}
