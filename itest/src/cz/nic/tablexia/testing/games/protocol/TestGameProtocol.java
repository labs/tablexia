/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.testing.games.protocol;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.protocol.ProtocolGame;
import cz.nic.tablexia.game.games.protocol.controller.ObjectsController;
import cz.nic.tablexia.game.games.protocol.controller.PositionCounter;
import cz.nic.tablexia.game.games.protocol.controller.PositionPair;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObject;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.TypeObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.WallObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.Furniture;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.FurnitureType;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;
import cz.nic.tablexia.game.games.protocol.model.CardsWidget;
import cz.nic.tablexia.game.games.protocol.model.LevelDefinition;
import cz.nic.tablexia.game.games.protocol.model.ProtocolGameState;
import cz.nic.tablexia.game.games.protocol.model.RoomGroup;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.ProtocolGenerator;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.resolvers.ProtocolScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;
import cz.nic.tablexia.util.ui.button.GameImageTablexiaButton;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

/**
 * Created by lmarik on 18.10.17.
 */

public class TestGameProtocol extends AbstractTestGame {

    private final int RADIUS = 20;

    private ProtocolGame protocolGame;
    private ProtocolGameState gameState;
    private CardsWidget cardsWidget;
    private RoomGroup roomGroup;
    private TablexiaButton upButton;
    private TablexiaButton downButton;
    private GameObject fixed;

    private int gameScore = 0;
    private int objectsCnt = 0;
    private int mistakeCnt = 0;

    public TestGameProtocol(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {

        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.PROTOCOL);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at game protocol button in the menu", GameMenuDefinition.PROTOCOL);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);

        takeScreenShotBlocking();
        clickAt(incGetSteps() + ". Click preloader button", TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForScreen(incGetSteps() + ". Wait for protocol screen", ProtocolGame.class);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        takeScreenShot();

        playGame();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if (expectedResult != AbstractTablexiaGame.GameResult.NO_STAR) {
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();
        clickButtonOnVictoryDialog(incGetSteps() + ". Click finish button in victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, 1);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);

        printResult();
        takeScreenShot();
    }

    private void playGame() {
        GameImageTablexiaButton roundButton = (GameImageTablexiaButton) findGameActor(ProtocolGame.ROUND_BUTTON);

        protocolGame = (ProtocolGame) getTablexia().getActualScreen();
        gameState = protocolGame.getData();
        cardsWidget = (CardsWidget) findGameActor(ProtocolGame.CARDS_WIDGET_GROUP);
        roomGroup = (RoomGroup) findGameActor(ProtocolGame.ROOM_GROUP);
        upButton = (TablexiaButton) findGameActor(CardsWidget.UP_BUTTON);
        downButton = (TablexiaButton) findGameActor(CardsWidget.DOWN_BUTTON);

        int playedRound = 0;

        while (playedRound < ProtocolGame.MAX_ROUNDS) {

            playRound();

            clickRoundButton(roundButton);
            waitForEvent(incGetSteps() + ". Wait for finish control event", ProtocolGame.FINISH_CONTROL_EVENT);
            clickRoundButton(roundButton);

            if (playedRound < ProtocolGame.MAX_ROUNDS - 1) {
                waitForEvent(incGetSteps() + ". Wait for new round event", ProtocolGame.NEW_ROUND_EVENT);
            }

            playedRound++;
        }

    }

    private void playRound() {
        recountCorrectObjects();
        takeScreenShot();

        ProtocolGenerator generator = gameState.getProtocolGenerator();
        fixed = (GameObject) findActorByName(ProtocolGame.FIXED_OBJECT);

        logInfo(generator.getController().toString());

        //Test All cards in menu by bigger scale action
        testMenu();

        Map<GameObjectType, List<PositionPair>> solution = generator.getController().getSolution();

        List<GameObjectType> menuObjects = new ArrayList<>(solution.keySet());
        List<GameObjectType> inRoom = new ArrayList<>();
        Stack<GameObjectType> typeStack = new Stack<>();

        typeStack.push(fixed.getType());
        inRoom.add(fixed.getType());
        menuObjects.remove(fixed.getType());

        correct();

        while (!typeStack.isEmpty() && cardsWidget.getCountCards() != 0) {
            GameObjectType objectType = typeStack.pop();

            //Add child to room
            moveHangOnObjectToFloor(objectType, solution, typeStack, inRoom, menuObjects);

            //Add parent to room
            moveDependObjectToFloor(objectType, solution, typeStack, inRoom, menuObjects);

            if (typeStack.isEmpty() && !menuObjects.isEmpty() && inRoom.size() != gameState.getProtocolObjectTypes().size()) {

                GameObjectType nextType = menuObjects.get(0);
                moveAloneObjectToRoom(nextType, solution, typeStack, inRoom, menuObjects, generator.getPositionCounter());
            }
        }

    }

    private void recountCorrectObjects() {
        Game game = loadGameFromDao(getGameID());
        int actualGameScore = Integer.valueOf(game.getGameScore(ProtocolScoreResolver.SCORE_COUNT, "0"));
        logInfo("Default correct item position/undefined item position: game score = " + gameScore + " changed to actual score = " + actualGameScore);

        if(gameScore!=actualGameScore) gameScore = actualGameScore;
    }


/////////////////////////////// ROOM ACTION

    /**
     * Move alone object to room. Object does not depend on the fixed object or on the objects, which depend to fixed object.
     *
     * @param aloneType
     * @param solution
     * @param typeStack
     * @param inRoom
     * @param menuObjects
     * @param counter
     */
    private void moveAloneObjectToRoom(GameObjectType aloneType, Map<GameObjectType, List<PositionPair>> solution, Stack<GameObjectType> typeStack, List<GameObjectType> inRoom, List<GameObjectType> menuObjects, PositionCounter counter) {
        List<PositionPair> pairs = solution.get(aloneType);
        GameObject alone = findGameObjectInMenu(aloneType);

        if (pairs == null || pairs.isEmpty() || pairs.get(0).getObjectType() instanceof GameObjectType) {
            // compute position by fixed position

            float[] middleRatio = gameState.getMiddleRatioInMap();

            Vector2 roomPosition = getActorPosition(roomGroup, ActorPosition.BOTTOM_LEFT);

            float xPosition = roomPosition.x + roomGroup.getWidth() * middleRatio[0] - alone.getCenterPosition().x;
            float yPosition = roomPosition.y + roomGroup.getHeight() * middleRatio[1] - alone.getCenterPosition().y;

            float[] middleShift = counter.countMiddleShifts(aloneType);

            xPosition += middleShift[0];
            yPosition += middleShift[1];

            correct();

            moveGameObjectToPosition(alone, xPosition, yPosition);

        } else if (pairs.get(0).getObjectType() instanceof FurnitureType) {

            moveObjectToFurniture(alone, (FurnitureType) pairs.get(0).getObjectType());

        } else if (pairs.get(0).getObjectType() instanceof WallType) {

            moveObjectToWall(alone, (WallType) pairs.get(0).getObjectType());

        }

        typeStack.push(aloneType);
        inRoom.add(aloneType);
        menuObjects.remove(aloneType);
    }

    private void moveObjectToFurniture(GameObject gameObject, FurnitureType furnitureType) {
        if (doCorrect(gameObject.getType())) {
            correct();
        } else {
            furnitureType = getDifferentFurniture(furnitureType);
            mistake();
        }

        Furniture furniture = (Furniture) findGameActor(ProtocolGame.FURNITURE + furnitureType);
        float[] fixRatio = furnitureType.getFixPositionRatio();

        Vector2 furniturePosition = getActorPosition(furniture, ActorPosition.BOTTOM_LEFT);

        float xPosition = furniturePosition.x + (furniture.getFurnitureImage().getWidth() * fixRatio[0]);
        float yPosition = furniturePosition.y + (furniture.getFurnitureImage().getHeight() * fixRatio[1]);

        logInfo(incGetSteps() + ". Move object [" + gameObject.getType() + "] on the furniture [" + furnitureType + "]");
        moveGameObjectToFurniturePosition(gameObject, xPosition, yPosition);
    }

    private void moveObjectToWall(GameObject gameObject, WallType wallType) {
        if (doCorrect(gameObject.getType())) {
            correct();
        } else {
            wallType = getDifferentWallType(wallType);
            mistake();
        }

        WallObjectDescriptor descriptor = WallObjectDescriptor.getDescriptorByType(gameObject.getType());
        float[] fixRatio = descriptor.getFixRatio(wallType);
        Vector2 roomPosition = getActorPosition(roomGroup, ActorPosition.BOTTOM_LEFT);

        float xPosition = roomPosition.x + roomGroup.getWidth() * fixRatio[0];
        float yPosition = roomPosition.y + roomGroup.getHeight() * fixRatio[1];


        logInfo(incGetSteps() + ". Move object [" + gameObject.getType() + "] on the wall [" + wallType + "]");
        moveGameObjectToWallPosition(gameObject, xPosition, yPosition);
    }

    /**
     * Moving objects to room on which the processed object hangs.
     *
     * @param processed
     * @param solution
     * @param typeStack
     */
    private void moveHangOnObjectToFloor(GameObjectType processed, Map<GameObjectType, List<PositionPair>> solution, Stack<GameObjectType> typeStack, List<GameObjectType> inRoom, List<GameObjectType> menuObjects) {
        if (solution.get(processed) == null)
            return;

        for (PositionPair positionPair : solution.get(processed)) {
            if (positionPair.getObjectType() instanceof GameObjectType && !inRoom.contains(positionPair.getObjectType())) {
                GameObjectType objectType = (GameObjectType) positionPair.getObjectType();
                GameObject gameObject = findGameObjectInMenu(objectType);

                boolean needCorrect = doCorrect(objectType);

                //Set move preposition
                Preposition preposition;
                if (positionPair.getPreposition() == Preposition.BETWEEN) {
                    preposition = getOppositePreposition(getPrepositionForBetweenPosition(objectType, solution.get(processed), inRoom, needCorrect));
                } else {
                    preposition = getOppositePreposition(positionPair.getPreposition());
                }

                if (needCorrect) {
                    moveGameObjectByPreposition(gameObject, new PositionPair(preposition, processed));
                } else {
                    moveGameObjectToFailPosition(gameObject, new PositionPair(preposition, processed));
                }

                typeStack.push(objectType);
                inRoom.add(objectType);
                menuObjects.remove(objectType);
                addScore(needCorrect);
            }
        }
    }

    /**
     * Moving objects to a room, which depend on processed object.
     *
     * @param processed
     * @param solution
     * @param typeStack
     */
    private void moveDependObjectToFloor(GameObjectType processed, Map<GameObjectType, List<PositionPair>> solution, Stack<GameObjectType> typeStack, List<GameObjectType> inRoom, List<GameObjectType> menuObjects) {
        for (GameObjectType objectType : solution.keySet()) {
            List<PositionPair> pairList = solution.get(objectType);
            if (pairList != null) {
                for (PositionPair pair : pairList) {
                    TypeObjectDescriptor positionType = pair.getObjectType();
                    if (positionType == processed && !inRoom.contains(objectType)) {

                        GameObject gameObject = findGameObjectInMenu(objectType);

                        boolean needCorrect = doCorrect(objectType);

                        //Set move preposition
                        Preposition preposition;
                        if (pair.getPreposition() == Preposition.BETWEEN) {
                            preposition = getPrepositionForBetweenPosition(processed, pairList, inRoom, needCorrect);
                        } else {
                            preposition = pair.getPreposition();
                        }

                        if (needCorrect) {
                            moveGameObjectByPreposition(gameObject, new PositionPair(preposition, processed));
                        } else {
                            moveGameObjectToFailPosition(gameObject, new PositionPair(preposition, processed));
                        }

                        typeStack.add(objectType);
                        inRoom.add(objectType);
                        menuObjects.remove(objectType);
                        addScore(needCorrect);
                    }
                }
            }
        }
    }


    private void moveGameObjectByPreposition(GameObject gameObject, PositionPair positionPair) {
        GameObjectType secondType = (GameObjectType) positionPair.getObjectType();
        GameObject second = secondType == fixed.getType() ? fixed : (GameObject) findGameActor(ProtocolGame.OBJECT + positionPair.getObjectType());

        Vector2 secondMiddle = second.getCenterPosition();
        Vector2 secondPosition = getActorPosition(second, ActorPosition.BOTTOM_LEFT);

        float xFinish = secondPosition.x + secondMiddle.x;
        float yFinish = secondPosition.y + secondMiddle.y;

        Preposition preposition = positionPair.getPreposition();

        switch (preposition) {
            case LEFT:
                xFinish -= RADIUS / 2;
                break;
            case RIGHT:
                xFinish += RADIUS / 2;
                break;
            case FRONT:
                yFinish -= RADIUS / 2;
                break;
            case BEHIND:
                yFinish += RADIUS / 2;
                break;
        }

        logInfo(incGetSteps() + ". Move the object [" + gameObject.getType() + "] by preposition " + positionPair.getPreposition() + " to object [" + secondType + "]");
        moveGameObjectToPosition(gameObject, xFinish, yFinish);
    }

    private void moveGameObjectToFailPosition(GameObject object, PositionPair correctPair) {
        final int FAIL_SECTOR_ANGLE = 5;

        GameObjectType subjectType = (GameObjectType) correctPair.getObjectType();
        GameObject subject = subjectType == fixed.getType() ? fixed : (GameObject) findGameActor(ProtocolGame.OBJECT + subjectType);

        Preposition correctPreposition = correctPair.getPreposition();

        double orientation = getCircularSectorOrientation(correctPreposition);

        float failedPositionAngle = (float) (calculationAngleToCircularSector(orientation + getCircularSectorAngle(correctPreposition) / 2 + FAIL_SECTOR_ANGLE));

        Vector2 subjectScreenPosition = getActorPosition(subject, ActorPosition.BOTTOM_LEFT);
        Vector2 centerPosition = new Vector2(subjectScreenPosition.x + subject.getCenterPosition().x, subjectScreenPosition.y + subject.getCenterPosition().y);

        float xFailPosition = (float) (centerPosition.x + RADIUS * Math.cos(Math.toRadians(failedPositionAngle)));
        float yFailPosition = (float) (centerPosition.y + RADIUS * Math.sin(Math.toRadians(failedPositionAngle)));

        logInfo(incGetSteps() + ". Move the object [" + object.getType() + "] to fail position to object [" + subject.getType() + "]");
        moveGameObjectToPosition(object, xFailPosition, yFailPosition);
    }

    private int getCircularSectorAngle(Preposition preposition) {
        if (preposition == Preposition.BEHIND || preposition == Preposition.FRONT) {
            return ObjectsController.VERTICAL_CIRCULAR_SECTOR;
        }

        return ObjectsController.HORIZONTAL_CIRCULAR_SECTOR;
    }

    private float getCircularSectorOrientation(Preposition preposition) {
        switch (preposition) {
            case FRONT:
                return 180.0f;
            case LEFT:
                return 270.0f;
            case RIGHT:
                return 90.0f;
            default:
                return 0.0f;
        }
    }

    private double calculationAngleToCircularSector(double angle) {
        angle = (90 - angle);

        if (Math.abs(angle) > 180) {
            angle = angle > 0 ? (angle - 360) : (angle + 360);
        }

        return angle;
    }

    private void moveGameObjectToWallPosition(GameObject object, float xFinish, float yFinish) {
        final int firstFingerPointer = 0;
        final float duration = 0.5f;

        Vector2 objectPosition = getActorPosition(object, ActorPosition.BOTTOM_LEFT);
        Vector2 wallPoint = object.getWallPoint();
        if(wallPoint == null){
            logError("Object " + object.getType() + " do not have wall point for move to wall");
            stopTheTest();
            return;
        }

        float xPosition = objectPosition.x + wallPoint.x;
        float yPosition = objectPosition.y + wallPoint.y;

        xFinish -= ProtocolGame.CARD_SMALL_SIZE / 2 - object.getImageWidth() / 2;
        yFinish -= ProtocolGame.CARD_SMALL_SIZE / 3;

        Stage stage = object.getStage();
        clickAt((int)xFinish, (int)yFinish, stage);
        boolean result = touchDown(xPosition, yPosition, 0, Input.Buttons.LEFT, stage);
        result &= dragOverTime((int) xPosition, (int) yPosition, (int) xFinish, (int) yFinish, firstFingerPointer, duration, 30, stage);
        result &= touchUp(xFinish, yFinish, 0, Input.Buttons.LEFT, stage);
        takeScreenShotBlocking();

        if (!result) {
            logError("Cannot move gameObject " + object.getType());
            stopTheTest();
        }

        logOK();

        waitForEvent(incGetSteps() + ". Wait for drop event", ProtocolGame.DROP_EVENT + object.getType());
        returnToFirstCard();
    }

    private void moveGameObjectToFurniturePosition(GameObject object, float xFinish, float yFinish) {
        final int firstFingerPointer = 0;
        final float duration = 0.5f;

        Vector2 objectPosition = getActorPosition(object, ActorPosition.BOTTOM_LEFT);
        Vector2 putPoint = object.getPutPoint();
        if(putPoint == null){
            logError("Object " + object.getType() + " do not have put point for move to furniture");
            stopTheTest();
            return;
        }

        float xPosition = objectPosition.x + putPoint.x;
        float yPosition = objectPosition.y + putPoint.y;

        xFinish -= ProtocolGame.CARD_SMALL_SIZE / 2 - object.getImageWidth() / 2;
        yFinish -= ProtocolGame.CARD_SMALL_SIZE / 3;

        Stage stage = object.getStage();
        clickAt((int)xFinish, (int)yFinish, stage);
        boolean result = touchDown(xPosition, yPosition, 0, Input.Buttons.LEFT, stage);
        result &= dragOverTime((int) xPosition, (int) yPosition, (int) xFinish, (int) yFinish, firstFingerPointer, duration, 30, stage);
        result &= touchUp(xFinish, yFinish, 0, Input.Buttons.LEFT, stage);
        takeScreenShotBlocking();

        if (!result) {
            logError("Cannot move gameObject " + object.getType());
            stopTheTest();
        }

        logOK();

        waitForEvent(incGetSteps() + ". Wait for drop event", ProtocolGame.DROP_EVENT + object.getType());
        returnToFirstCard();
    }

    private void moveGameObjectToPosition(GameObject object, float xFinish, float yFinish) {
        final int firstFingerPointer = 0;
        final float duration = 0.5f;

        Vector2 objectPosition = getActorPosition(object, ActorPosition.BOTTOM_LEFT);
        Vector2 middle = object.getCenterPosition();

        float xPosition = objectPosition.x + middle.x;
        float yPosition = objectPosition.y + middle.y;

        xFinish -= ProtocolGame.CARD_SMALL_SIZE / 2 - object.getImageWidth() / 2;
        yFinish -= ProtocolGame.CARD_SMALL_SIZE / 3;

        Stage stage = object.getStage();
        boolean result = touchDown(xPosition, yPosition, 0, Input.Buttons.LEFT, stage);
        result &= dragOverTime((int) xPosition, (int) yPosition, (int) xFinish, (int) yFinish, firstFingerPointer, duration, 30, stage);
        result &= touchUp(xFinish, yFinish, 0, Input.Buttons.LEFT, stage);
        takeScreenShotBlocking();

        if (!result) {
            logError("Cannot move gameObject " + object.getType());
            stopTheTest();
        }

        logOK();

        waitForEvent(incGetSteps() + ". Wait for drop event", ProtocolGame.DROP_EVENT + object.getType());
        returnToFirstCard();
    }

/////////////////////////////// TEST MENU

    private void testMenu() {
        int maxIteration = cardsWidget.getCountCards() - CardsWidget.VISIBLE_CARDS;
        int iteration = 0;

        do {
            checkVisibleCardsInMenu(cardsWidget);

            if (iteration != maxIteration)
                clickInTheActor(downButton);

            iteration++;
        } while (iteration <= maxIteration);

        returnToFirstCard();
    }

    private void checkVisibleCardsInMenu(CardsWidget cardsWidget) {
        for (Actor actor : cardsWidget.getVisibleCards()) {
            if (actor != null && actor instanceof GameObject) {
                GameObject object = (GameObject) actor;
                showBigCard(object);
            }
        }
    }

    private void showBigCard(GameObject object) {
        clickGameObject(object);
        waitForEvent(incGetSteps() + ". Wait for bigger scale event", ProtocolGame.BIGGER_SCALE_EVENT + object.getType());
        clickGameObject(object);
        waitForEvent(incGetSteps() + ". Wait for smaller scale event", ProtocolGame.SMALLER_SCALE_EVENT + object.getType());
    }

/////////////////////////////// PROTOCOL ACTION

    private void addScore(boolean needCorrect) {
        if (!needCorrect) mistake();
        else correct();
    }

    private void correct() {
        gameScore++;
        objectsCnt++;
    }

    private void mistake() {
        mistakeCnt++;
        objectsCnt++;
    }

    private boolean doCorrect(GameObjectType objectType) {
        if(!canMistake(objectType))
            return true;

        int sumObjectsCnt = LevelDefinition.DifficultyDefinition.getDefinitionByDifficulty(difficulty).getSumObjectCount();
        int[] scoreResult = ProtocolScoreResolver.ResultStars.values()[difficulty.ordinal() - 1].getScoreResult();
        if (expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR && objectsCnt >= (sumObjectsCnt - scoreResult[1]+ gameScore))
            return true;
        else if (expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR && objectsCnt >= (sumObjectsCnt - scoreResult[0]+ gameScore))
            return true;
        else return expectedResult == AbstractTablexiaGame.GameResult.THREE_STAR;
    }

    private boolean canMistake(GameObjectType objectType){
        Map<GameObjectType, List<PositionPair>> solution = gameState.getProtocolGenerator().getController().getSolution();
        return !(solution.containsKey(objectType) && solution.get(objectType).isEmpty());
    }

    private WallType getDifferentWallType(WallType wallType) {
        for (WallType different : WallType.values()) {
            if (different != wallType)
                return different;
        }

        return WallType.MIDDLE_WALL;
    }

    private FurnitureType getDifferentFurniture(FurnitureType furnitureType) {
        List<FurnitureType> furnitureTypes = FurnitureType.getFurnitureTypesByDifficulty(difficulty);
        for (FurnitureType different : furnitureTypes) {
            if (different != furnitureType) {
                return different;
            }
        }

        return null;
    }

    private Preposition getOppositePreposition(Preposition preposition) {
        if (preposition == Preposition.LEFT) return Preposition.RIGHT;
        else if (preposition == Preposition.RIGHT) return Preposition.LEFT;
        else if (preposition == Preposition.BEHIND) return Preposition.FRONT;
        else if (preposition == Preposition.FRONT) return Preposition.BEHIND;

        return preposition;
    }

    private Preposition getPrepositionForBetweenPosition(GameObjectType objectType, List<PositionPair> positionPairs, List<GameObjectType> inRoom, boolean needCorrect) {
        if(needCorrect) {
            for (PositionPair pair : positionPairs) {
                if (pair.getPreposition() == Preposition.BETWEEN && pair.getObjectType() != objectType) {
                    return inRoom.contains(pair.getObjectType()) ? Preposition.LEFT : Preposition.RIGHT;
                }
            }

            return Preposition.RIGHT;
        }

        else return Preposition.LEFT;
    }

    private void returnToFirstCard() {
        //Return to first Cards
        while (upButton.isEnabled()) {
            clickInTheActor(upButton);
        }
    }

    private void clickRoundButton(GameImageTablexiaButton roundButton) {
        logInfo(incGetSteps() + ". Click in the actor" + roundButton.getName());
        boolean result = clickInTheActor(roundButton);

        if (!result) {
            logError("Cannot click in the actor " + ProtocolGame.ROUND_BUTTON);
            stopTheTest();
        }

        logOK();
    }

    private GameObject findGameObjectInMenu(GameObjectType objectType) {
        //Set object to visible in menu
        while (!cardsWidget.isGameObjectVisible(objectType)) {
            if (downButton.isEnabled()) {
                clickInTheActor(downButton);
            } else {
                logError(objectType + " cannot find in game menu");
                stopTheTest();
            }
        }

        return (GameObject) findGameActor(ProtocolGame.OBJECT + objectType);
    }

    private void clickGameObject(GameObject object) {
        logInfo(incGetSteps() + ". Click on gameObject[" + object.getType() + "]");

        Vector2 objectPosition = getActorPosition(object, ActorPosition.MIDDLE);

        float xPosition = objectPosition.x + object.getImageWidth() / 2;
        float yPosition = objectPosition.y + object.getImageHeight() / 2;

        boolean result = touchDown(xPosition, yPosition, 0, Input.Buttons.LEFT, object.getStage());
        result &= touchUp(xPosition, yPosition, 0, Input.Buttons.LEFT, object.getStage());

        if (!result) {
            logError("Cannot click on game object" + object.getType());
            stopTheTest();
        }

        logOK();
    }

    @Override
    protected void checkVictoryTable(int daoScore, Table table) {
        if (!checkScoreInTable(table, Integer.toString(daoScore) + "/" + objectsCnt)) {
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE + " with correct dao score: " + Integer.toString(daoScore) + "/" + objectsCnt);
            stopTheTest();
        }
    }

    @Override
    protected void testVictoryData() {

        Game game = loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(ProtocolScoreResolver.SCORE_COUNT, "0"));
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.PROTOCOL, game, daoScore);

    }

    @Override
    protected int getCubsCount() {
        int[] scoreResult = ProtocolScoreResolver.ResultStars.values()[difficulty.ordinal() - 1].getScoreResult();
        if (gameScore >= scoreResult[2]) return 3;
        else if (gameScore >= scoreResult[1]) return 2;
        else if (gameScore >= scoreResult[0]) return 1;
        return 0;
    }

    @Override
    protected void printResult() {

        Game game = loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(ProtocolScoreResolver.SCORE_COUNT, "0"));
        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n");
        logInfo("Game number: " + GameDefinition.PROTOCOL.getGameNumber() + "\n");
        logInfo("SCORE: " + daoScore + "\n");
        logInfo("STARS: " + getCubsCount() + "\n");
    }


}
