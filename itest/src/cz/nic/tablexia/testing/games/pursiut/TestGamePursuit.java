/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games.pursiut;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.Random;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.pursuit.PursuitGame;
import cz.nic.tablexia.game.games.pursuit.model.Grid;
import cz.nic.tablexia.game.games.pursuit.model.PuzzlePiece;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.resolvers.PursuitScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;

/**
 * Created by lmarik on 6.3.17.
 */

public class TestGamePursuit extends AbstractTestGame {

    private Long durationInMillis;
    private Long testCountDroppedEvent = 0l;

    public TestGamePursuit(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {

        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.PURSUIT);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at Game pursuit button in the menu", GameMenuDefinition.PURSUIT);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event ready game", AbstractTablexiaGame.EVENT_GAME_READY);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        takeScreenShot();

        playGame();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if(expectedResult != AbstractTablexiaGame.GameResult.NO_STAR){
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();
        clickButtonOnVictoryDialog(incGetSteps() +". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,1);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);

        printResult();
        takeScreenShot();
    }

    private void playGame(){
        Grid grid = (Grid) findGameActor(PursuitGame.GRID);

        if(expectedResult != AbstractTablexiaGame.GameResult.THREE_STAR){

            long start = System.currentTimeMillis();
            long duration = 0;

            while (duration<(getTimeByExceptedResult()*1000)){
                moveRandomPieces(grid);
                duration = System.currentTimeMillis() - start;
            }
        }


        for (PuzzlePiece piece : grid.getPiecesMap().values()) {
            movePiece(grid, piece, piece.getActualPosition(), piece.getCorrectPosition());
        }

        takeScreenShot();

        for(PuzzlePiece piece:grid.getPiecesMap().values()){
            rotationPiece(grid,piece);
        }

        takeScreenShot();
        testPiecesPosition(grid);
    }

    private void rotationPiece(Grid grid,PuzzlePiece piece){
        final Stage stage = piece.getStage();

        float diff =  360-piece.getRotation();
        int rotationTimes = (int)(Math.abs(diff)/90);

        if(rotationTimes == 0 || rotationTimes == 4)
            return;

        int startX = (int)(grid.getX() + piece.getX() + piece.getWidth()/2);
        int startY = (int) (grid.getY() + piece.getY() + piece.getHeight()/2);

        boolean result = true;

        result &= touchDown(startX,startY,0,Input.Buttons.LEFT,stage);

        int times = 0;
        while (times < rotationTimes){
            times++;
            wait(PursuitGame.ROTATE_SOUND_MIN_INTERVAL);

            logInfo(incGetSteps() + ". " + piece.toString() + " rotated about ninety degrees");

            result &= touchDown(startX,startY,0,Input.Buttons.RIGHT,stage);
            result &= touchUp(startX,startY,0,Input.Buttons.RIGHT,stage);


            logOK();
        }

        result &= touchUp(startX,startY,0,Input.Buttons.LEFT,stage);

        if(!result){
            logError("Cannot rotate with piece" + piece.getId());
            stopTheTest();
        }

        waitForEvent(incGetSteps() + ". Wait for piece dropped event",PursuitGame.EVENT_PIECE_DROPPED + testCountDroppedEvent);
        testCountDroppedEvent++;
    }

    private void movePiece(Grid grid, PuzzlePiece piece,int actualPosition,int finishPosition){
        if(actualPosition == finishPosition){
            return;
        }

        logInfo(incGetSteps() + ". " +piece.toString()+ " moved from actual position(" + actualPosition+") to finish position (" + finishPosition+")");
        int actualRow = actualPosition / grid.getNumberOfColumns();
        int actualColumn = actualPosition % grid.getNumberOfColumns();

        int needRow = finishPosition / grid.getNumberOfColumns();
        int needColumn = finishPosition % grid.getNumberOfColumns();


        int startX = (int)(grid.getX() + (actualColumn)*piece.getWidth() + piece.getWidth()/2);
        int startY = (int)(grid.getY() + ((grid.getNumberOfColumns() - actualRow)*piece.getHeight() - piece.getHeight()/2));

        int finishX = (int)(grid.getX() + (needColumn*piece.getWidth() + piece.getWidth()/2));
        int finishY = (int)(grid.getY() + ((grid.getNumberOfColumns() - needRow)*piece.getHeight() - piece.getHeight()/2));

        boolean result = true;

        result &= touchDown(startX,startY,0,Input.Buttons.LEFT,piece.getStage());
        result &= dragOverTime(startX, startY, finishX, finishY, 0, 0.5f, 30, piece.getStage());
        result &= touchUp(finishX,finishY,0,Input.Buttons.LEFT,piece.getStage());

        if(!result){
            logError("Cannot move piece" + piece.getId());
            stopTheTest();
        }

        logOK();
        waitForEvent(incGetSteps() + ". Wait for piece dropped event",PursuitGame.EVENT_PIECE_DROPPED + testCountDroppedEvent);
        testCountDroppedEvent++;

    }

    private void moveRandomPieces(Grid grid){
        Random random = new Random();

        int piecesCnt = grid.getPiecesMap().size()-1;

        int firstRnd = random.nextInt(piecesCnt);
        int secondRnd = random.nextInt(piecesCnt);
        PuzzlePiece first = (PuzzlePiece) grid.getPiecesMap().values().toArray()[firstRnd];
        PuzzlePiece second = (PuzzlePiece) grid.getPiecesMap().values().toArray()[secondRnd];

        if(first == null || second == null){
            return;
        }

        movePiece(grid,first,first.getActualPosition(),second.getActualPosition());
    }

    private int getTimeByExceptedResult(){
        switch (difficulty){
            case EASY:
                if(expectedResult == AbstractTablexiaGame.GameResult.NO_STAR) return PursuitScoreResolver.EASY_ONE_STAR_DURATION;
                else if(expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR) return PursuitScoreResolver.EASY_TWO_STARS_DURATION;
                else if(expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR) return PursuitScoreResolver.EASY_THREE_STARS_DURATION;
            case MEDIUM:
                if(expectedResult == AbstractTablexiaGame.GameResult.NO_STAR) return PursuitScoreResolver.MEDIUM_ONE_STAR_DURATION;
                else if(expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR) return PursuitScoreResolver.MEDIUM_TWO_STARS_DURATION;
                else if(expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR) return PursuitScoreResolver.MEDIUM_THREE_STARS_DURATION;
            case HARD:
                if(expectedResult == AbstractTablexiaGame.GameResult.NO_STAR) return PursuitScoreResolver.HARD_ONE_STAR_DURATION;
                else if(expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR) return PursuitScoreResolver.HARD_TWO_STARS_DURATION;
                else if(expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR) return PursuitScoreResolver.HARD_THREE_STARS_DURATION;
            case BONUS:
                if(expectedResult == AbstractTablexiaGame.GameResult.NO_STAR) return PursuitScoreResolver.HARD_ONE_STAR_DURATION;
                else if(expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR) return PursuitScoreResolver.HARD_TWO_STARS_DURATION;
                else if(expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR) return PursuitScoreResolver.HARD_THREE_STARS_DURATION;

        }
        return 0;
    }

    private void testPiecesPosition(Grid grid) {

        logInfo(incGetSteps() + ". Test puzzle pieces position and rotation");

        for (PuzzlePiece piece : grid.getPiecesMap().values()) {
            if (piece.getActualPosition() != piece.getCorrectPosition()) {
                logError(piece.toString() + "have bad position. Actual position = " + piece.getActualPosition() + ", Correct position = " + piece.getCorrectPosition());
                stopTheTest();
            }

            if (piece.getRotation() != 0) {
                //logError();
                logError(piece.toString() + "have bad rotation. Rotation is " + piece.getRotation());
                stopTheTest();

            }
        }

        logOK();
    }


    @Override
    protected void checkVictoryTable(int daoScore, Table table) {
        if(!checkScoreInTable(table,Integer.toString(daoScore))){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE+ " with correct move count: " + daoScore );
            stopTheTest();
        }

        if(!checkScoreInTable(table,getFormattedTime())){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE+ " with correct time: " + getFormattedTime() );
            stopTheTest();
        }
    }

    private String getFormattedTime(){

        int minutes = (int) ((durationInMillis / 1000) / 60);
        int seconds = (int) ((durationInMillis / 1000) % 60);

        if(minutes == 0) {
            return seconds + "s";
        } else if (seconds == 0){
            return minutes + "min";
        } else {
            return minutes + "min" + seconds + "s";
        }
    }

    @Override
    protected void testVictoryData() {
        Game game= loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(PursuitScoreResolver.SCORE_KEY_MOVE_COUNT,"0"));
        durationInMillis = game.getGameDuration();
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.PURSUIT,game,daoScore);

    }

    @Override
    protected int getCubsCount() {
        float testTime = durationInMillis / 1000f;
        switch (difficulty){
            case EASY:
                if(testTime > PursuitScoreResolver.EASY_ONE_STAR_DURATION) return 0;
                else if(testTime > PursuitScoreResolver.EASY_TWO_STARS_DURATION) return 1;
                else if(testTime > PursuitScoreResolver.EASY_THREE_STARS_DURATION) return 2;
                else return 3;
            case MEDIUM:
                if(testTime > PursuitScoreResolver.MEDIUM_ONE_STAR_DURATION) return 0;
                else if(testTime > PursuitScoreResolver.MEDIUM_TWO_STARS_DURATION) return 1;
                else if(testTime > PursuitScoreResolver.MEDIUM_THREE_STARS_DURATION) return 2;
                else return 3;

            case HARD:
                if(testTime > PursuitScoreResolver.HARD_ONE_STAR_DURATION) return 0;
                else if(testTime > PursuitScoreResolver.HARD_TWO_STARS_DURATION) return 1;
                else if(testTime > PursuitScoreResolver.HARD_THREE_STARS_DURATION) return 2;
                else return 3;

            case BONUS:
                if(testTime > PursuitScoreResolver.HARD_ONE_STAR_DURATION) return 0;
                else if(testTime > PursuitScoreResolver.HARD_TWO_STARS_DURATION) return 1;
                else if(testTime > PursuitScoreResolver.HARD_THREE_STARS_DURATION) return 2;
                else return 3;

        }
        return 0;
    }

    @Override
    protected void printResult() {
        Game game= loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(PursuitScoreResolver.SCORE_KEY_MOVE_COUNT,"0"));

        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() +  "\n");
        logInfo("Game number: " + GameDefinition.PURSUIT.getGameNumber()+ "\n");
        logInfo("MOVE COUNT: " + daoScore + "\n");
        logInfo("TIME: " + getFormattedTime() + "\n");
        logInfo("STARS: " + getCubsCount() +"\n");
    }


}
