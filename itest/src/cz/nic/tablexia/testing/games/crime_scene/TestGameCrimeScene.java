/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games.crime_scene;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.crime_scene.CrimeSceneGame;
import cz.nic.tablexia.game.games.crime_scene.actors.PlaySoundScreen;
import cz.nic.tablexia.game.games.crime_scene.gameobject.GameObject;
import cz.nic.tablexia.game.games.crime_scene.gameobject.GameObjectType;
import cz.nic.tablexia.game.games.crime_scene.model.CrimeSceneGameState;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.resolvers.CrimeSceneScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;
import cz.nic.tablexia.util.ui.actionwidget.ActionsStripWidget;
import cz.nic.tablexia.util.ui.button.GameImageTablexiaButton;

/**
 * Created by lmarik on 8.3.17.
 */

public class TestGameCrimeScene extends AbstractTestGame {


    private int correctObjects  = 0;
    private int failRound       = 0;

    public TestGameCrimeScene(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {

        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user,GameDefinition.CRIME_SCENE);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at game crime scene button in the menu", GameMenuDefinition.CRIME_SCENE);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        //check bonus
        checkBonusButtonEnable(GameDefinition.CRIME_SCENE, user);

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);
        takeScreenShotBlocking();
        clickAt(incGetSteps() + ". Click preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForScreen(incGetSteps() + ". Wait for crime scene screen", CrimeSceneGame.class);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        takeScreenShot();

        playGame();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if(expectedResult != AbstractTablexiaGame.GameResult.NO_STAR){
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();
        clickButtonOnVictoryDialog(incGetSteps()+". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,1);

        waitForScreen(incGetSteps() + ". Wait for game page screen",GamePageScreen.class);

        printResult();
        takeScreenShot();

    }

    private void playGame(){
        PlaySoundScreen soundScreen = (PlaySoundScreen) findGameActor(CrimeSceneGame.SOUND_SCREEN);
        ActionsStripWidget menu = (ActionsStripWidget) findGameActor(CrimeSceneGame.STRIP_WIDGET);
        Group objectsLayer = (Group) findGameActor(CrimeSceneGame.OBJECTS_LAYER);
        Group gameMap = (Group) findGameActor(CrimeSceneGame.GAME_MAP);
        GameImageTablexiaButton replayButton = (GameImageTablexiaButton) findGameActor(CrimeSceneGame.BUTTON_REPLAY);


        int finishX = (int) (getTablexia().getActualScreen().getSceneWidth() - menu.getWidth()/2);
        int finishY = (int) (getTablexia().getActualScreen().getSceneInnerHeight() - CrimeSceneGame.ACTION_CARD_DRAG_SIZE/2);

        int actionX = (int) (getTablexia().getActualScreen().getSceneWidth()/2 - menu.getWidth());
        int actionY = (int) (getTablexia().getActualScreen().getSceneInnerHeight()/2);

        waitForEvent(incGetSteps() + ". Wait for start play event",CrimeSceneGame.EVENT_START_PLAY);

        soundPlayScenario(soundScreen);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click replay button",CrimeSceneGame.BUTTON_REPLAY);
        waitForEvent(incGetSteps() + ". Wait for start play event",CrimeSceneGame.EVENT_START_PLAY);
        soundPlayScenario(soundScreen);



        int rounds = 0;
        while (rounds<CrimeSceneGame.MAX_ROUNDS) {
            rounds++;

            takeScreenShot();

            boolean mistakeInRound = false;
            List<Integer> actionNumberList = new ArrayList<>();
            CrimeSceneGameState state = (CrimeSceneGameState)(getTablexia().getActualScreen()).getData();

            for (int i = 0; i < state.getSolution().size(); i++) {
                GameObject gameObject = null;

                if(doMistake()){
                    gameObject =  findGameObjectByType(state.getSolution().get(i),objectsLayer,false);
                    mistakeInRound = true;
                }else {
                    gameObject = findGameObjectByType(state.getSolution().get(i),objectsLayer,true);
                    if(!mistakeInRound) correctObjects++;
                }

                if(gameObject == null){
                    logError("Can not find game object by type" + state.getSolution().get(i));
                    stopTheTest();
                }

                clickOnGameObject(gameObject,gameMap,replayButton,objectsLayer);

                //Move showed game object action to strip widget
                moveActionToMenu(incGetSteps() + ". Move game object["+gameObject.getGameObjectType()+"] to strip widget",actionX,actionY,finishX,finishY,gameObject.getAction(),menu);

                waitForEvent(incGetSteps() + ". Wait for event card drop down", GameObject.EVENT_DROP_DOWN);

                actionNumberList.add(gameObject.getGameObjectType().getActionNumber());
            }
            takeScreenShot();

            clickAt(incGetSteps() + ". Click start button", CrimeSceneGame.BUTTON_START);
            waitControlEvent(actionNumberList);


            if(mistakeInRound)
                failRound++;

            if(rounds != CrimeSceneGame.MAX_ROUNDS && failRound != CrimeSceneGame.MAX_WRONG_ROUNDS) {
                waitForEvent(incGetSteps() + ". Wait for start play event", CrimeSceneGame.EVENT_START_PLAY);
                soundScreen = (PlaySoundScreen) findGameActor(CrimeSceneGame.SOUND_SCREEN);
                soundPlayScenario(soundScreen);
            }else {
                break;
            }
        }

    }

    private GameObject findGameObjectByType(GameObjectType correctType,Group objectLayer,boolean correct){
        for(Actor child:objectLayer.getChildren()){
            if(child instanceof GameObject){
                GameObject gameObject = (GameObject) child;
                if(gameObject.getGameObjectType() == correctType && correct){
                    return gameObject;
                }else if(gameObject.getGameObjectType() != correctType && !correct){
                    return gameObject;
                }
            }
        }

        //last item is correct
        if(!correct && objectLayer.getChildren().size > 0){
            return (GameObject) objectLayer.getChildren().items[0];
        }

        return null;
    }

    private boolean doMistake(){
        if (expectedResult == AbstractTablexiaGame.GameResult.THREE_STAR) return false;
        else if (expectedResult == AbstractTablexiaGame.GameResult.NO_STAR) return true;

        if(expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR && correctObjects >= CrimeSceneScoreResolver.ResultStars.TWO_STAR.getScoreForDifficulty(difficulty.getDifficultyDefinition()))
            return true;
        else if(expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR && correctObjects >= CrimeSceneScoreResolver.ResultStars.ONE_STAR.getScoreForDifficulty(difficulty.getDifficultyDefinition()))
            return true;

        return false;
    }

    private void waitControlEvent(List<Integer> actionNumberList){
        for(int actionNumber: actionNumberList){
            waitForEvent(incGetSteps() + ". Wait for control action[" + actionNumber + "] event",CrimeSceneGame.EVENT_ACTION_CONTROLLED + actionNumber);
        }
    }


    private void clickOnGameObject(GameObject gameObject,Group gameMap,GameImageTablexiaButton replayButton,Group objectLayer) {
        logInfo(incGetSteps() + ". Click on game object["+gameObject.getGameObjectType()+"]");

        Vector2 clickPosition = new Vector2((gameMap.getX() + gameObject.getMapPosition().getX() + gameObject.getAction().getWidth()/2),gameMap.getY() + gameObject.getMapPosition().getY() + gameObject.getAction().getHeight()/2);

        //Check game object not overlapping by replay button
        if(isOverlapWithButton(gameObject,replayButton,gameMap)){
            clickPosition.y = (int) (gameMap.getY() + gameObject.getMapPosition().getY() + gameObject.getAction().getHeight());
        }

        //Control collision object with another objects
        clickPosition = checkCollisionWithAnotherObjects(clickPosition,gameObject,objectLayer,gameMap);

        //Click on game object
        boolean result = clickAt((int)clickPosition.x, (int)clickPosition.y, gameObject.getStage());

        if(!result){
            logError("Cannot click on game object" + gameObject.getGameObjectType());
            stopTheTest();
        }

        logOK();
        waitForEvent(incGetSteps() + ". Wait for event higlighted object shown", CrimeSceneGame.EVENT_HIGHLIGHTED_GO);

    }

    private Vector2 checkCollisionWithAnotherObjects(Vector2 clickPosition, GameObject gameObject, Group objectLayer, Group gameMap){

        for (Actor child:objectLayer.getChildren()){
            if(child instanceof GameObject && ((GameObject) child).getGameObjectType() != gameObject.getGameObjectType()){

                GameObject object = (GameObject) child;
                float xMin = gameMap.getX() + object.getMapPosition().getX();
                float xMax = xMin + object.getAction().getWidth();

                float yMin = gameMap.getY() + object.getMapPosition().getY();
                float yMax = yMin + object.getAction().getHeight();

                if((xMin < clickPosition.x && xMax > clickPosition.x) && (yMin < clickPosition.y && yMax > clickPosition.y)){
                    float distanceMin = clickPosition.dst(new Vector2(xMax,yMin));
                    float distanceMax = clickPosition.dst(new Vector2(xMin,yMax));

                    //Change click position by minimal difference
                    if(distanceMin < distanceMax){
                        clickPosition.x += (xMax - clickPosition.x);
                        clickPosition.y += (yMin - clickPosition.y);
                    }else {
                        clickPosition.x += (xMin - clickPosition.x);
                        clickPosition.y += (yMax - clickPosition.y);
                    }
                }
            }
        }

        return clickPosition;
    }

    private boolean isOverlapWithButton(GameObject object,GameImageTablexiaButton replayButton,Group gameMap){
        int xObject = (int) (gameMap.getX() + object.getMapPosition().getX());
        int yObject = (int) (gameMap.getY() + object.getMapPosition().getY());

        return xObject < replayButton.getX() + replayButton.getWidth()
                && xObject+ object.getAction().getWidth() > replayButton.getX()
                && yObject < replayButton.getY() + replayButton.getHeight()
                && yObject + object.getAction().getHeight() > replayButton.getY();
    }

    private void soundPlayScenario(PlaySoundScreen soundScreen){

        for(int i = 0;i<soundScreen.getMusicList().size();i++){
            int indexSound = i+1;
            waitForEvent(incGetSteps() + ". Wait for event finish sound index: " + (indexSound),CrimeSceneGame.EVENT_FINISH_SOUND +indexSound);
        }
        waitForEvent(incGetSteps() + ". Wait for stop played event",CrimeSceneGame.EVENT_STOP_PLAYED);
    }

    private int getMaxScoreByDifficulty(){
        int max = 0;
        int numberOfObjects = CrimeSceneScoreResolver.CrimeSceneDifficulty.getCrimeSceneDifficultyForGameDifficulty(difficulty.getDifficultyDefinition()).getNumberOfObjects();
        for(int i = 0;i<CrimeSceneGame.MAX_ROUNDS;i++){
            max += numberOfObjects + i;
        }
        return max;
    }

    @Override
    protected void checkVictoryTable(int daoScore, Table table) {
        if(!checkScoreInTable(table,Integer.toString(daoScore) + " / " + getMaxScoreByDifficulty())){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE+ " with correct fail count: " + Integer.toString(daoScore)+" / " + getMaxScoreByDifficulty());
            stopTheTest();
        }
    }

    @Override
    protected void testVictoryData() {
        Game game = loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(CrimeSceneScoreResolver.SCORE_KEY_COUNT,"0"));
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.CRIME_SCENE,game,daoScore);
    }

    @Override
    protected int getCubsCount() {
        return CrimeSceneScoreResolver.ResultStars.getStarCountForDifficultyAndErrors(difficulty.getDifficultyDefinition(), correctObjects).getCupsCount();
    }

    @Override
    protected void printResult() {
        Game game = loadGameFromDao(1);

        int daoScore = Integer.valueOf(game.getGameScore(CrimeSceneScoreResolver.SCORE_KEY_COUNT,"0"));

        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() +  "\n");
        logInfo("Game number: " + GameDefinition.CRIME_SCENE.getGameNumber()+ "\n");
        logInfo("SCORE: " +daoScore + "\n");
        logInfo("STARS: " +getCubsCount() + "\n");
    }

}
