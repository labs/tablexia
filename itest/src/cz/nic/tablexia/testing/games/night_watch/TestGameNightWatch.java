/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games.night_watch;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import net.dermetfan.utils.Pair;

import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.night_watch.NightWatchGame;
import cz.nic.tablexia.game.games.night_watch.helper.GameRulesHelper;
import cz.nic.tablexia.game.games.night_watch.helper.TextureHelper;
import cz.nic.tablexia.game.games.night_watch.solution.Solution;
import cz.nic.tablexia.game.games.night_watch.subscene.Watch;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.resolvers.NightWatchScoreResolver_V3_4;
import cz.nic.tablexia.testing.games.AbstractTestGame;
import cz.nic.tablexia.util.ui.button.GameImageTablexiaButton;

/**
 * Created by lmarik on 27.2.17.
 */

public class TestGameNightWatch extends AbstractTestGame {
    private static final int Y_CLICK_MINUS = 30;

    private int playedRound =   0;
    private int roundErrors =   0;

    public TestGameNightWatch(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.NIGHT_WATCH);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at Game night watch button in the menu", GameMenuDefinition.NIGHT_WATCH);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event ready game", AbstractTablexiaGame.EVENT_GAME_READY);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        playRounds();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if(expectedResult != AbstractTablexiaGame.GameResult.NO_STAR){
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }

        testVictoryData();
        takeScreenShot();
        clickButtonOnVictoryDialog(incGetSteps() +". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,1);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        printResult();
        takeScreenShot();
    }

    private void playRounds(){
        takeScreenShot();

        Stage stage = getTablexia().getActualScreen().getStage();

        GameImageTablexiaButton completeButton = (GameImageTablexiaButton) findGameActor(NightWatchGame.COMPLETE_BUTTON);
        Watch watch = (Watch) findGameActor(NightWatchGame.WATCH);

        List<Pair<Integer, Integer>> windowPositions = getWindowsPositionsByDif(difficulty);

        int numberOfRounds = (difficulty == GameDifficulty.BONUS) ? GameRulesHelper.BONUS_NUMBER_OF_ROUNDS : GameRulesHelper.NUMBER_OF_ROUNDS;

        for(int i=0;i< numberOfRounds;i++){
            waitForEvent(incGetSteps() + ". Wait for new day event", NightWatchGame.EVENT_NEW_DAY +i);
            //play round
            Solution solution = NightWatchGame.lastSolution;
            boolean mistake = doMistake();
            if(mistake)
                roundErrors++;

            int index = 0;
            setTimeout(DEFAULT_TASK_TIMEOUT*solution.getWindowSequence().size()*3,SCENARIO_GAME_ACTION);
            while (!completeButton.isVisible()){

                int windowsIndex = solution.getWindowSequence().get(index);
                if(mistake){
                    //mistake - wrong windows
                    clickWindowsByIndex(findWrongWindow(solution.getWindowSequence(),windowPositions.size(),stage), stage,windowPositions);
                }else {
                    clickWindowsByIndex(windowsIndex,stage, windowPositions);
                }

                index++;

                if(index >= solution.getWindowSequence().size()){
                    index = 0;
                }
            }

            AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_GAME_ACTION);
            clearTimeout();


            if(mistake){
                //mistake
                setTimeOnWatch(findWrongTime(solution.getTime()) , watch,watch.getStage());
            }else {
                setTimeOnWatch(solution.getTime(), watch,watch.getStage());
            }

            playedRound++;


            clickAt(incGetSteps() + ". Click on complete button",NightWatchGame.COMPLETE_BUTTON);

            if(roundErrors >= NightWatchGame.MAX_GAME_ERRORS){
                break;
            }

            if(i !=  numberOfRounds-1) {
                waitForEvent(incGetSteps() + ". Wait for evaluate round event",NightWatchGame.EVENT_EVALUATE_ROUND+i);
                waitForEvent(incGetSteps() + ". Wait for new night event", NightWatchGame.EVENT_NEW_NIGHT + i);
            }

        }
    }

    private int findWrongWindow(List<Integer> correctWindows,int windowsSize,Stage stage){
        int i = 0;
        while (i < windowsSize){
            if(!correctWindows.contains(i) && !isSelectedWindowVisible(i,stage)){
                break;
            }
            i++;
        }

        return i;
    }

    private int findWrongTime(int correctTime){
        final int TIME = 12;
        int i = 0;

        while (i < TIME){
            if(i != correctTime){
                break;
            }
            i++;
        }

        return i;
    }

    private boolean doMistake(){
        if(difficulty == GameDifficulty.BONUS){
            if(expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR && playedRound >= NightWatchScoreResolver_V3_4.BONUS_SCORE_THREE_STARS_V3_4 -NightWatchGame.MAX_GAME_ERRORS -1){   return true;}
            else if(expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR && playedRound >= NightWatchScoreResolver_V3_4.BONUS_SCORE_TWO_STARS_V3_4 -NightWatchGame.MAX_GAME_ERRORS-1){   return true;}
            else if(expectedResult == AbstractTablexiaGame.GameResult.NO_STAR && playedRound >= NightWatchScoreResolver_V3_4.BONUS_SCORE_ONE_STAR_V3_4-NightWatchGame.MAX_GAME_ERRORS-1) {   return true;}

            return false;
        } else {
            if(expectedResult == AbstractTablexiaGame.GameResult.TWO_STAR && playedRound >= NightWatchScoreResolver_V3_4.SCORE_THREE_STARS_V3_4 -NightWatchGame.MAX_GAME_ERRORS -1){   return true;}
            else if(expectedResult == AbstractTablexiaGame.GameResult.ONE_STAR && playedRound >= NightWatchScoreResolver_V3_4.SCORE_TWO_STARS_V3_4 -NightWatchGame.MAX_GAME_ERRORS-1){   return true;}
            else if(expectedResult == AbstractTablexiaGame.GameResult.NO_STAR && playedRound >= NightWatchScoreResolver_V3_4.SCORE_ONE_STAR_V3_4-NightWatchGame.MAX_GAME_ERRORS-1) {   return true;}

            return false;
        }
    }

    private void clickWindowsByIndex(int index, Stage stage, List<Pair<Integer, Integer>> windowPositions){

        if(isSelectedWindowVisible(index,stage)) return;

        final float xWindow = windowPositions.get(index).getKey();
        final float yWindow = windowPositions.get(index).getValue()-Y_CLICK_MINUS;


        boolean result = touchDown(xWindow, yWindow, 0, Input.Buttons.LEFT, stage);
        result &= touchUp(xWindow, yWindow, 0, Input.Buttons.LEFT, stage);

        if(!result){
            logError("Cannot click on window " + index);
            stopTheTest();
        }

        if(isSelectedWindowVisible(index,stage)){
            waitForEvent(incGetSteps() + ". Wait for window selected event",NightWatchGame.EVENT_WINDOW_SELECTED+index);
        }else{
            writeToLogFile("Window not selected [TRY NEXT]\n");
        }

    }

    private boolean isSelectedWindowVisible(int index,Stage stage){
        Actor actor = stage.getRoot().findActor(NightWatchGame.WINDOW+index);
        if(actor==null){
            return false;
        }

        return true;
    }

    private void setTimeOnWatch(int time,Watch watch,Stage stage){
        Image hourHand = watch.getHourHandImage();
        if(hourHand == null){
            logError("Image hour hand not found in watch");
            stopTheTest();
        }

        Image onlyWatch = watch.getWatchOnlyImage();
        if(onlyWatch == null){
            logError("Image only watch not found in watch");
            stopTheTest();
        }

        double angle = 360 - (time - 3) * 30;

        float xMiddle = watch.getX() + onlyWatch.getX() + onlyWatch.getWidth()/2;
        float yMiddle = watch.getY() + onlyWatch.getY() + onlyWatch.getWidth()/2;

        int xStart = (int)(watch.getX() + watch.getWidth()/2 - hourHand.getWidth()/2);
        int yStart = (int)(watch.getY() + watch.getHeight() - hourHand.getHeight());

        int xFinish = (int)(xMiddle + (hourHand.getHeight()) * Math.cos(Math.toRadians(angle)));
        int yFinish = (int)(yMiddle + (hourHand.getHeight()) * Math.sin(Math.toRadians(angle)));


        boolean result = touchDown(xStart,yStart,0, Input.Buttons.LEFT,stage);

        result &= dragOverTime(xStart,yStart,xFinish,yFinish,0,0.5f,30,stage);
        result &= touchUp(xFinish,yFinish,0,Input.Buttons.LEFT,stage);

        if(!result){
            logError("Set time on watch failed");
            stopTheTest();
        }

        logOK();
    }

    private  List<Pair<Integer, Integer>> getWindowsPositionsByDif(GameDifficulty difficulty){
        switch (difficulty){
            case BONUS:
                return TextureHelper.testThirtyTwoWindowsPositions;
            case HARD:
                return TextureHelper.testThirtyTwoWindowsPositions;
            case MEDIUM:
                return TextureHelper.testTwentyFourWindowsPositions;
            case EASY:
                return TextureHelper.testSixteenWindowsProperties;
        }
        return null;
    }

    @Override
    protected void checkVictoryTable(int daoScore, Table table) {
        int numberOfRounds = (difficulty == GameDifficulty.BONUS) ? GameRulesHelper.BONUS_NUMBER_OF_ROUNDS : GameRulesHelper.NUMBER_OF_ROUNDS;
        if(!checkScoreInTable(table,Integer.toString(daoScore) + " / " +  numberOfRounds)){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE+ " with correct dao score: " + Integer.toString(daoScore) + " / " + numberOfRounds);
            stopTheTest();
        }
    }

    @Override
    protected void testVictoryData() {
        Game game = loadGameFromDao(getGameID());

        int daoScore = Integer.valueOf(game.getGameScore(NightWatchScoreResolver_V3_4.SCORE_ROUND_COUNT,"0"));
        checkVictoryDialog(incGetSteps()+". Check victory dialog ",AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.NIGHT_WATCH,game,daoScore);
    }

    @Override
    protected int getCubsCount() {

        if(difficulty == GameDifficulty.BONUS){
            if (playedRound > NightWatchScoreResolver_V3_4.BONUS_SCORE_THREE_STARS_V3_4) return 3;
            else if (playedRound > NightWatchScoreResolver_V3_4.BONUS_SCORE_TWO_STARS_V3_4) return 2;
            else if (playedRound > NightWatchScoreResolver_V3_4.BONUS_SCORE_ONE_STAR_V3_4) return 1;
            return 0;
        } else {
            if (playedRound > NightWatchScoreResolver_V3_4.SCORE_THREE_STARS_V3_4) return 3;
            else if (playedRound > NightWatchScoreResolver_V3_4.SCORE_TWO_STARS_V3_4) return 2;
            else if (playedRound > NightWatchScoreResolver_V3_4.SCORE_ONE_STAR_V3_4) return 1;
            return 0;
        }
    }

    @Override
    protected void printResult() {
        Game game = loadGameFromDao(getGameID());

        int score = Integer.valueOf(game.getGameScore(NightWatchScoreResolver_V3_4.SCORE_ROUND_COUNT,"0"));

        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() +  "\n");
        logInfo("Game number: " + GameDefinition.NIGHT_WATCH.getGameNumber()+ "\n");
        logInfo("SCORE: " + score + "\n");
        logInfo("STARS: " + getCubsCount() + "\n");
    }


}
