/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing.games.kidnapping;


import com.badlogic.gdx.scenes.scene2d.ui.Table;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.kidnapping.KidnappingGame;
import cz.nic.tablexia.game.games.kidnapping.actors.Arrow;
import cz.nic.tablexia.game.games.kidnapping.actors.KidnappingMap;
import cz.nic.tablexia.game.games.kidnapping.actors.KidnappingTile;
import cz.nic.tablexia.game.games.kidnapping.model.Direction;
import cz.nic.tablexia.game.games.kidnapping.model.GameState;
import cz.nic.tablexia.game.games.kidnapping.util.DirectionsHelper;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.resolvers.KidnappingScoreResolver;
import cz.nic.tablexia.testing.games.AbstractTestGame;
import cz.nic.tablexia.util.Log;

/**
 * Created by lmarik on 14.3.17.
 */

public class TestGameKidnapping extends AbstractTestGame {

    private static final int MAX_MISTAKES_IN_ROUND = 2;

    private int roundsPlayedCorrect = 0;

    private GameState       state;
    private KidnappingMap   map;
    private int action = 0;
    private int mistakesDone = 0;

    public TestGameKidnapping(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        logGameInfo();

        User user = createUser();
        logIn(user);

        if(difficulty == GameDifficulty.BONUS){
            setDataForUnlockBonus(user, GameDefinition.KIDNAPPING);
        }

        waitForEvent(incGetSteps() + ". Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(incGetSteps() + ". Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);

        waitForEvent(incGetSteps() + ". Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(incGetSteps() + ". Click at Game button in main menu", MainMenuDefinition.GAMES);

        waitForEvent(incGetSteps() + ". Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickGameOnMenu(incGetSteps() + ". Click at kidnapping game button in the menu", GameMenuDefinition.KIDNAPPING);


        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);
        clickAt(incGetSteps() + ". Click at hard difficulty button", GamePageGroup.DIFFICULTY_BUTTON_HARD);
        clickAt(incGetSteps() + ". Click at easy difficulty button", GamePageGroup.DIFFICULTY_BUTTON_EASY);
        clickAt(incGetSteps() + ". Click at bonus difficulty button", GamePageGroup.DIFFICULTY_BUTTON_BONUS);
        if(difficulty != GameDifficulty.BONUS){
            waitForDialog(incGetSteps() + ". Wait for bonus locked dialog", GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
            clickDialogButton(incGetSteps() + ". Click at OK at bonus locked dialog.",GamePageGroup.BONUS_DIFFICULTY_LOCKED_DIALOG_NAME, 0);
        }

        //setDifficulty
        setGameDificulty();

        clickAt(incGetSteps() + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(incGetSteps() + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);
        takeScreenShot();

        clickAt(incGetSteps() + ". Click at preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);


        waitForEvent(incGetSteps() + ". Wait for event ready game", AbstractTablexiaGame.EVENT_GAME_READY);

        waitForEvent(incGetSteps() + ". Wait for rule showed event", KidnappingGame.EVENT_RULE_SHOWED);
        clickAt(incGetSteps() + ". Click at rule button",KidnappingGame.RULE_BUTTON);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        takeScreenShot();

        playGame();

        waitForEvent(incGetSteps() + ". Wait for event ready victory dialog", AbstractTablexiaGame.EVENT_VICTORY_DIALOG_READY);

        if(expectedResult != AbstractTablexiaGame.GameResult.NO_STAR){
            waitForEvent(incGetSteps() + ". Wait for event animation victory dialog finished", AbstractTablexiaGame.EVENT_ANIMATION_FINISHED);
        }


        testVictoryData();
        takeScreenShot();

        clickButtonOnVictoryDialog(incGetSteps() +". Click finish button in victory dialog",AbstractTablexiaGame.VICTORY_DIALOG,1);

        waitForScreen(incGetSteps() + ". Wait for game page screen", GamePageScreen.class);
        printResult();
        takeScreenShot();
    }

    private void playGame(){
        map = (KidnappingMap) findGameActor(KidnappingGame.MAP);
        state = ((KidnappingGame)getTablexia().getActualScreen()).getData();
        final int minRoundsNeeded = getMinRoundsNeeded();

        int step = 0;
        while (step <= state.getPath().size()-1 && mistakesDone<2) {
            playRound(step,minRoundsNeeded);
            step++;
        }

    }

    private void playRound(int step,int minRoundsNeeded){
        waitForEvent(incGetSteps() + ". Wait for start round event",KidnappingGame.EVENT_START_ROUND);
        waitForEvent(incGetSteps() + ". Wait for example played event", KidnappingGame.EVENT_EXAMPLE_PLAYED);
        waitForEvent(incGetSteps() + ". Wait for sounds played event",KidnappingGame.EVENT_SOUNDS_PLAYED);

        if(step == 0){
            replaySounds(incGetSteps() + ". Click replay button",map);
        }


        final Direction[] directions = DirectionsHelper.getNextDirections(state.getLastDirectionFrom());
        final Direction correct = directions[DirectionsHelper.getNextDirectionIndex(directions, state.getNextDirection())];

        checkArrowsByDirection(incGetSteps() + ". Check arrows diplayed on the screen by directions",directions);

        Log.info(getClass(),"TIMEOUT " + action);

        if(step > minRoundsNeeded){
            boolean lastMistake = doMistake(correct, mistakesDone);
            mistakesDone++;
            if(!lastMistake) playCorrect(correct, step);
        }
        else{
            playCorrect(correct, step);
            roundsPlayedCorrect++;
        }

        action++;

    }

    private void playCorrect(Direction correct, int step) {
        clickOnDirection(correct,getArrowByDirection(correct));
        takeScreenShot();
        if(step !=  state.getPath().size() - 1) waitForEvent(incGetSteps() + ". Wait for correct animation direction event",KidnappingGame.EVENT_CORRECT);
    }


    private boolean doMistake(Direction correct, int mistakesDone){
        int doMissCnt = 0;
        while (doMissCnt < MAX_MISTAKES_IN_ROUND){
            doMissCnt++;

            Direction fail = getFailDirection(correct);

            if(fail == null){
                logError("Cannot find bad direction");
                stopTheTest();
            }

            clickOnDirection(fail,getArrowByDirection(fail));
            if(mistakesDone == 0) waitForEvent(incGetSteps() + ". Wait for wrong animation finished event",KidnappingGame.EVENT_ANIMATION_WRONG_FINISHED);
            else return true;
        }
        return false;
    }

    private Direction getFailDirection(Direction correct){
        for(Direction direction: Direction.values()){
            if(direction != correct){
                Arrow arrow = getArrowByDirection(direction);
                if(arrow != null && !arrow.isDisabled())
                return direction;
            }
        }

        return null;
    }


    private Arrow getArrowByDirection(Direction direction){
        KidnappingTile northTile = map.getTile(state.getCurrentPosition());
        KidnappingTile southEastTile = map.getTile(state.getCurrentPosition().getSouthEast());
        KidnappingTile southWestTile = map.getTile(state.getCurrentPosition().getSouthWest());
        switch (direction){
            case NW:
                return northTile.getNorthWestOut();
            case NE:
                return northTile.getNorthEastOut();
            case SE:
                return southEastTile.getNorthWestIn();
            case SW:
                return southWestTile.getNorthEastIn();
        }

        return null;
    }


    private int getMinRoundsNeeded(){
        switch (expectedResult){
            case NO_STAR:
                return KidnappingScoreResolver.GAME_RULE_CUPS[difficulty.getDifficultyNumber()-1][3];
            case ONE_STAR:
                return KidnappingScoreResolver.GAME_RULE_CUPS[difficulty.getDifficultyNumber()-1][2];
            case TWO_STAR:
                return KidnappingScoreResolver.GAME_RULE_CUPS[difficulty.getDifficultyNumber()-1][1];
            case THREE_STAR:
                return KidnappingScoreResolver.GAME_RULE_CUPS[difficulty.getDifficultyNumber()-1][0];
        }

        return 0;
    }

    private void replaySounds(String infoMessage,KidnappingMap map){
        logInfo(infoMessage);

        if(!(clickInTheActor(map.getReplayButton()))){
            logError("Cannot click on replay button");
            stopTheTest();
        }

        logOK();

        waitForEvent(incGetSteps() + ". Wait for example played event", KidnappingGame.EVENT_EXAMPLE_PLAYED);
        waitForEvent(incGetSteps() + ". Wait for sounds played event",KidnappingGame.EVENT_SOUNDS_PLAYED);

    }

    private void checkArrowsByDirection(String infoMessage,Direction[] directions){

        logInfo(infoMessage);

        KidnappingTile northTile = map.getTile(state.getCurrentPosition());
        KidnappingTile southEastTile = map.getTile(state.getCurrentPosition().getSouthEast());
        KidnappingTile southWestTile = map.getTile(state.getCurrentPosition().getSouthWest());

        if(isArrowTouchable(northTile.getNorthWestOut()) != checkDirection(Direction.NW,directions)){
            logError("Northern west arrow bad displayed by directions");
            stopTheTest();
        }

        if(isArrowTouchable(northTile.getNorthEastOut()) != checkDirection(Direction.NE,directions)){
            logError("Northern east arrow bad displayed by directions");
            stopTheTest();
        }

        if(isArrowTouchable(southEastTile.getNorthWestIn()) != checkDirection(Direction.SE,directions)){
            logError("Southern east arrow bad displayed by directions");
            stopTheTest();
        }

        if( isArrowTouchable(southWestTile.getNorthEastIn()) != checkDirection(Direction.SW,directions)){
            logError("Southern west arrow bad displayed by directions");
            stopTheTest();
        }


        logOK();
    }

    private boolean isArrowTouchable(Arrow arrow){
        return arrow != null && !arrow.isDisabled() && arrow.isVisible();
    }

    private boolean checkDirection(Direction direction,Direction[] actualDirections){

        for(Direction actualDirection:actualDirections){
            if(direction == actualDirection) {
                return true;
            }
        }
        return false;
    }

    private void clickOnDirection(Direction direction ,Arrow arrow){


        if(arrow == null){
            logError("Arrow " + direction + " is null");
            stopTheTest();
        }

        setTimeout(DEFAULT_TASK_TIMEOUT,SCENARIO_GAME_ACTION+action);
        while (!arrow.isDisabled()) {
            logInfo(incGetSteps() + ". Click on " + direction + " arrow");
            if (!clickInTheActor(arrow)) {
                writeToLogFile("[TRY NEXT]\n");
            }else {
                logOK();
            }
        }

        clearTimeout();


    }

    @Override
    protected void checkVictoryTable(int daoScore, Table table) {
        if(!checkScoreInTable(table,Integer.toString(daoScore))){
            logError("Not found label in " + AbstractTablexiaGame.RESULT_TABLE+ " with correct fail count: " + Integer.toString(daoScore));
            stopTheTest();
        }
    }

    @Override
    protected void testVictoryData() {
        Game game = loadGameFromDao(getGameID());
        int daoScore = Integer.valueOf(game.getGameScore(KidnappingScoreResolver.GAME_ROUNDS_CORRECT,"0"));
        checkVictoryDialog(incGetSteps() + ". Check victory dialog", AbstractTablexiaGame.VICTORY_DIALOG, GameDefinition.KIDNAPPING,game,daoScore);
    }

    @Override
    protected int getCubsCount() {
        int[] levels = KidnappingScoreResolver.GAME_RULE_CUPS[difficulty.getDifficultyNumber()-1];

        if(roundsPlayedCorrect == levels[0])return 3;
        else if(roundsPlayedCorrect >= levels[1])return 2;
        else if(roundsPlayedCorrect >= levels[2]) return 1;

        return 0;
    }

    @Override
    protected void printResult() {
        Game game = loadGameFromDao(getGameID());
        int daoScore = Integer.valueOf(game.getGameScore(KidnappingScoreResolver.GAME_MISSES,"0"));

        logInfo("Test game finish\n");
        logInfo("GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() +  "\n");
        logInfo("Game number: " + GameDefinition.KIDNAPPING.getGameNumber()+ "\n");
        logInfo("MISSED: " + daoScore + "\n");
        logInfo("STARS: " + getCubsCount() +"\n");
    }


}
