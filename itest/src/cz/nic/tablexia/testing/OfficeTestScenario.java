/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaScreen;
import cz.nic.tablexia.screen.gamemenu.OfficeMenuScreen;
import cz.nic.tablexia.screen.gamemenu.actors.WallOfGames;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.screen.halloffame.HallOfFameScreen;
import cz.nic.tablexia.screen.profile.ProfileScreen;
import cz.nic.tablexia.screen.statistics.StatisticsScreen;

/**
 * Created by aneta on 19.4.17.
 */

public class OfficeTestScenario extends AbstractTestScenario{
    public OfficeTestScenario(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        logIn(createUser());

        waitForEvent("1. Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt("2. Click at door to hall of fame", OfficeMenuScreen.DOOR_TO_HALL_OF_FAME_NAME);
        waitForScreen("3. Wait for screen HallOfFameScreen", HallOfFameScreen.class);

        clickAt("4. Click at door", HallOfFameScreen.ACTOR_DOOR_NAME);
        waitForScreen("5. Wait for screen OfficeMenuScreen", OfficeMenuScreen.class);

        clickAt("6. Click at statistics", OfficeMenuScreen.STATISTICS_IMAGE_NAME);
        waitForScreen("7. Wait for screen StatisticsScreen", StatisticsScreen.class);

        clickAt("8. Click at back button", Tablexia.BACK_BUTTON_NAME);
        waitForScreen("9. Wait for screen OfficeMenuScreen", OfficeMenuScreen.class);

        clickAt("10. Click at encyclopedia", OfficeMenuScreen.ENCYCLOPEDIA_IMAGE_NAME);
        waitForScreen("11. Wait for screen EncyclopediaScreen", EncyclopediaScreen.class);

        clickAt("12. Click at back button", Tablexia.BACK_BUTTON_NAME);
        waitForScreen("13. Wait for screen OfficeMenuScreen", OfficeMenuScreen.class);

        clickAt("14. Click at profile table", OfficeMenuScreen.PROFILE_TABLE_NAME);
        waitForScreen("15. Wait for screen ProfileScreen", ProfileScreen.class);

        clickAt("16. Click at back button", Tablexia.BACK_BUTTON_NAME);
        waitForScreen("17. Wait for screen OfficeMenuScreen", OfficeMenuScreen.class);

        tryWallOfGame(18, WallOfGames.GameIconDefinition.ROBBERY);
        goBackToOffice(19);

        tryWallOfGame(20, WallOfGames.GameIconDefinition.PURSUIT);
        goBackToOffice(21);

        tryWallOfGame(22, WallOfGames.GameIconDefinition.KIDNAPPING);
        goBackToOffice(23);

        tryWallOfGame(24, WallOfGames.GameIconDefinition.NIGHTWATCH);
        goBackToOffice(25);

        tryWallOfGame(26, WallOfGames.GameIconDefinition.SHOOTINGRANGE);
        goBackToOffice(27);

        tryWallOfGame(28, WallOfGames.GameIconDefinition.INTHEDARKNESS);
        goBackToOffice(29);

        tryWallOfGame(30, WallOfGames.GameIconDefinition.CRIMESCENE);
        goBackToOffice(31);

        tryWallOfGame(32, WallOfGames.GameIconDefinition.RUNES);
        goBackToOffice(33);

        tryWallOfGame(34, WallOfGames.GameIconDefinition.PROTOCOL);
        goBackToOffice(35);

        tryWallOfGame(36, WallOfGames.GameIconDefinition.SAFE);
        goBackToOffice(37);

        tryWallOfGame(38, WallOfGames.GameIconDefinition.ON_THE_TRAIL);
        goBackToOffice(39);

        tryWallOfGame(40, WallOfGames.GameIconDefinition.MEMORY_GAME);
        goBackToOffice(41);
    }

    private void tryWallOfGame(int stepNum, WallOfGames.GameIconDefinition nameOfGame){
        clickAt(stepNum + "a. Click at wall of game", OfficeMenuScreen.WALL_OF_GAME_IMAGE_NAME);
        waitForEvent(stepNum + "b. Wait for wall of game ready", WallOfGames.WALL_OF_GAMES_READY);
        clickAt(stepNum + "c. Click at " + nameOfGame + " game", nameOfGame.name());
        waitForScreen(stepNum + "d. Wait for screen GamePageScreen", GamePageScreen.class);

        GamePageScreen pageScreen;
        if(getTablexia().getActualScreen() instanceof GamePageScreen)
        pageScreen = (GamePageScreen) getTablexia().getActualScreen();
        else{
            logError(getTablexia().getActualScreen().toString() + " isn't instance of GamePageScreen!");
            stopTheTest();
            return;
        }

        writeToLogFile(stepNum + "e. Check if game page is screen for " + nameOfGame);
        if(pageScreen.getGameDefinition() != nameOfGame.getGameDefinition()){
            logError("Screen game menu definition (" + pageScreen.getGameDefinition() + ") is different than " + nameOfGame.getGameDefinition());
            stopTheTest();
        }
        logOK();

    }

    private void goBackToOffice(int stepNum){
        clickAt(stepNum + "a. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent(stepNum + "b. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);
        clickMainMenuButton(stepNum + "c. Click at Office button in the menu", MainMenuDefinition.OFFICE);
        waitForScreen(stepNum + "d. Wait for screen OfficeMenuScreen", OfficeMenuScreen.class);
    }

    protected void waitForSubmenu(String infoMessage) {
        logInfo(infoMessage);
        wait(200);
        logOK();
    }

    protected boolean clickGameOnMenu(String infoMessage, GameMenuDefinition definition){
        return clickMenuButton(infoMessage,definition.toString());
    }

}
