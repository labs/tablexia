/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.testing;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.trophy.GameTrophyDefinition;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.gamemenu.OfficeMenuScreen;
import cz.nic.tablexia.screen.halloffame.HallOfFameScreen;
import cz.nic.tablexia.screen.halloffame.trophy.TrophyHelper;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.ui.TablexiaImage;

/**
 * Created by aneta on 15.11.16.
 */
public abstract class AbstractTestScenarioHallOfFameTrophies extends AbstractTestScenario {
    public static final int BONUS_TROPHY_MULTIPLE = 6;

    public AbstractTestScenarioHallOfFameTrophies(Tablexia tablexia) {
        super(tablexia);
    }

    private void checkTrophy(GameTrophyDefinition gameTrophyDefinition, User user, boolean [] gotTrophies, int i){
        writeToLogFile("\tCheck trophy: " + gameTrophyDefinition.toString() + "\n");
        swipeToActor("\t\ta. Swipe to trophy " + gameTrophyDefinition.toString(), gameTrophyDefinition.getTrophyName());
        if (gotTrophies[i]) checkTrophyFull(user, gameTrophyDefinition);
        else checkTrophyEmpty(user, gameTrophyDefinition);
    }

    protected void checkGameTrophies(int stepNumber, User user, boolean[] gotTrophies) {
        writeToLogFile(stepNumber + ". Check all trophies:\n");
        int i = 0;
        List<GameTrophyDefinition> bonusTrophyDefinitions = new ArrayList<>();
        for (GameTrophyDefinition gameTrophyDefinition : GameTrophyDefinition.values()) {
            if((i+1)%BONUS_TROPHY_MULTIPLE == 0) bonusTrophyDefinitions.add(gameTrophyDefinition);
            else checkTrophy(gameTrophyDefinition, user, gotTrophies, i);
            i++;
        }
        i=6;
        for(GameTrophyDefinition bonusTrophyDefinition: bonusTrophyDefinitions){
            checkTrophy(bonusTrophyDefinition, user, gotTrophies, i-1);
            i+=BONUS_TROPHY_MULTIPLE;
        }
    }

    private void checkTrophyEmpty(GameTrophyDefinition gameTrophyDefinition, User user){
        writeToLogFile("\tCheck empty trophy: " + gameTrophyDefinition.toString() + "\n");
        swipeToActor("\t\ta. Swipe to trophy " + gameTrophyDefinition.toString(), gameTrophyDefinition.getTrophyName());
        checkTrophyEmpty(user, gameTrophyDefinition);
    }


    protected void checkAllTrophiesEmpty(int stepNumber, User user) {
        writeToLogFile(stepNumber + ". Check if all trophies are empty:\n");
        List<GameTrophyDefinition> bonusTrophyDefinitions = new ArrayList<>();
        int i=0;
        for (GameTrophyDefinition gameTrophyDefinition : GameTrophyDefinition.values()) {
            if((i+1)%BONUS_TROPHY_MULTIPLE == 0) bonusTrophyDefinitions.add(gameTrophyDefinition);
            else checkTrophyEmpty(gameTrophyDefinition, user);
            i++;
        }
        for(GameTrophyDefinition bonusTrophyDefinition: bonusTrophyDefinitions) checkTrophyEmpty(bonusTrophyDefinition, user);
    }

    protected void addNewScoreAndGoToHallOfFame(int stepNumber, User user, GameDifficulty gameDifficulty, GameDefinition gameDefinition, AbstractTablexiaGame.GameResult gameResult) {
        clickAt(stepNumber + "a. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent(stepNumber + "b. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(stepNumber + "c. Click at Office button in the menu", MainMenuDefinition.OFFICE);
        waitForScreen(stepNumber + "d. Wait for screen OfficeMenuScreen", OfficeMenuScreen.class);

        waitForEvent(stepNumber + "e. Wait for opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(stepNumber + "f. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent(stepNumber + "g. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        setGameScore(user, gameDifficulty, gameDefinition, gameResult);

        clickMainMenuButton(stepNumber + "g. Click at Hall of Fame button in the menu", MainMenuDefinition.HALL_OF_FAME);
        waitForScreen(stepNumber + "h. Wait for screen HallOfFameScreen", HallOfFameScreen.class);
    }

    protected void checkTrophyFull(User user, GameTrophyDefinition gameTrophyDefinition){
        logInfo("\t\tb. User has trophy/ Trophy is visible");
        if(!gameTrophyDefinition.hasTrophy(user) || !isTrophyPictureVisible(gameTrophyDefinition)){
            logError("User should have trophy " + gameTrophyDefinition.getTrophyName() + "!");
            stopTheTest();
        }
        else logOK();
    }

    protected void checkTrophyEmpty(User user, GameTrophyDefinition gameTrophyDefinition){
        logInfo("\t\tb. User don't have trophy/ Trophy isn't visible");
        if(gameTrophyDefinition.hasTrophy(user) || isTrophyPictureVisible(gameTrophyDefinition)){
            logError("User should not have trophy " + gameTrophyDefinition.getTrophyName() + "!");
            stopTheTest();
        }
        else logOK();
    }

    protected boolean isTrophyPictureVisible(GameTrophyDefinition gameTrophyDefinition) {
        TablexiaImage trophy = (TablexiaImage) findActorByName(gameTrophyDefinition.getTrophyName());
        if(trophy==null) return false;
        if(trophy.getId().equals(TrophyHelper.TROPHY_SUBFOLDER_NAME + TrophyHelper.TROPHY_PREFIX + gameTrophyDefinition.getTrophyNameKey() + "_" + TrophyHelper.TrophyType.EMPTY.toString().toLowerCase())){
            return false;
        }
        return true;
    }
}
