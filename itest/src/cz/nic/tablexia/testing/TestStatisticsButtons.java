/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.testing;


import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.statistics.StatisticsScreen;
import cz.nic.tablexia.shared.model.User;

/**
 * Created by Maril on 02.02.2017.
 */

public class TestStatisticsButtons extends AbstractTestStatistics {


    public TestStatisticsButtons(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        //log user
        User user = createUser();
        logIn(user);


        //open statistics
        waitForEvent("1. Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt("2. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("3. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton("4. Click at Statistics button in the menu", MainMenuDefinition.STATISTICS);

        waitForScreen("5. Wait for screen Statistics", StatisticsScreen.class);


        //statistics screen - test buttons
        clickAt("6. Switch graph type to average", StatisticsScreen.SWITCH_GRAPH_AVERAGE);
        takeScreenShotBlocking();
        for (int i = 0; i < GameMenuDefinition.values().length; i++) {
            clickAt((7 + i) + ". Click at up button in the screen", StatisticsScreen.UP__BUTTON);
        }

        for (int i = 0; i < GameMenuDefinition.values().length; i++) {
            clickAt((17 + i) + ". Click at down button in the screen", StatisticsScreen.DOWN_BUTTON);
        }


        clickAt("27. Switch graph type to game", StatisticsScreen.SWITCH_GRAPH_GAME);
        takeScreenShotBlocking();
        //test all game button on click and check them
        clickButtonGameAndTest(28, GameMenuDefinition.NIGHT_WATCH, GameMenuDefinition.NIGHT_WATCH.ordinal());
        takeScreenShotBlocking();
        clickButtonGameAndTest(30, GameMenuDefinition.IN_THE_DARKNESS, GameMenuDefinition.IN_THE_DARKNESS.ordinal());
        takeScreenShotBlocking();
        clickButtonGameAndTest(32, GameMenuDefinition.KIDNAPPING, GameMenuDefinition.KIDNAPPING.ordinal());
        takeScreenShotBlocking();
        clickButtonGameAndTest(34, GameMenuDefinition.PURSUIT, GameMenuDefinition.PURSUIT.ordinal());
        takeScreenShotBlocking();
        clickButtonGameAndTest(36, GameMenuDefinition.RUNES, GameMenuDefinition.RUNES.ordinal());
        takeScreenShotBlocking();
        clickButtonGameAndTest(38, GameMenuDefinition.SHOOTING_RANGE, GameMenuDefinition.SHOOTING_RANGE.ordinal());
        takeScreenShotBlocking();
        clickButtonGameAndTest(40, GameMenuDefinition.ROBBERY, GameMenuDefinition.ROBBERY.ordinal());
        takeScreenShotBlocking();
        clickButtonGameAndTest(42, GameMenuDefinition.PROTOCOL, GameMenuDefinition.PROTOCOL.ordinal());
        takeScreenShotBlocking();
        clickButtonGameAndTest(44, GameMenuDefinition.SAFE, GameMenuDefinition.SAFE.ordinal());
        takeScreenShotBlocking();
        clickButtonGameAndTest(46, GameMenuDefinition.ON_THE_TRAIL, GameMenuDefinition.ON_THE_TRAIL.ordinal());
        takeScreenShotBlocking();
        clickButtonGameAndTest(48, GameMenuDefinition.MEMORY_GAME, GameMenuDefinition.MEMORY_GAME.ordinal());
        takeScreenShotBlocking();

        //test up/down buttons with test checked game button
        clickUpDownButtonAndCheckGameButton(50, ". Click at down button in the screen", 2, StatisticsScreen.DOWN_BUTTON);
        clickUpDownButtonAndCheckGameButton(53, ". Click at up button in the screen", 4, StatisticsScreen.UP__BUTTON);
        clickUpDownButtonAndCheckGameButton(58, ". Click at up button in the screen", 8, StatisticsScreen.UP__BUTTON);
        takeScreenShotBlocking();
        clickUpDownButtonAndCheckGameButton(67, ". Click at up button in the screen", 10, StatisticsScreen.UP__BUTTON);
        clickUpDownButtonAndCheckGameButton(78, ". Click at down button in the screen", 11, StatisticsScreen.DOWN_BUTTON);
        clickUpDownButtonAndCheckGameButton(90, ". Click at down button in the screen", 4, StatisticsScreen.DOWN_BUTTON);
        takeScreenShotBlocking();

    }

}
