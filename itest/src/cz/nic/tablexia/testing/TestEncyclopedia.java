/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaScreen;
import cz.nic.tablexia.screen.encyclopedia.content.actors.PlaybackStateButton;
import cz.nic.tablexia.screen.encyclopedia.content.model.Content;
import cz.nic.tablexia.screen.encyclopedia.menu.MenuItem;
import cz.nic.tablexia.screen.encyclopedia.menu.MenuWidget;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

/**
 * Created by lmarik on 9.2.17.
 */

public class TestEncyclopedia extends AbstractTestScenario {

    private final String EVENT_SCROLL_TOP = "scroll top";

    public TestEncyclopedia(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {

        User user = createUser();
        logIn(user);

        waitForEvent("1. Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt("2. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("3. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton("4. Click at Encyclopedia button in the menu", MainMenuDefinition.ENCYCLOPEDIA);
        waitForScreen("5. Wait for screen Encyclopedia", EncyclopediaScreen.class);

        clickAt("6. Click plus font button", EncyclopediaScreen.BUTTON_PLUS_NAME);
        clickAt("7. Click plus font button", EncyclopediaScreen.BUTTON_PLUS_NAME);
        takeScreenShotBlocking();
        clickAt("8. Click minus font button", EncyclopediaScreen.BUTTON_MINUS_NAME);
        clickAt("9. Click minus font button", EncyclopediaScreen.BUTTON_MINUS_NAME);
        takeScreenShotBlocking();

        clickOnMenuItem("10. Click on menu item. Index menu item " + 1,EncyclopediaScreen.MENU,1);
        waitForEvent("11. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.SPECIFIC_LEARNING_DISABILITIES.getResourcePageName());
        waitForFirstHeaderVisible("12. Wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,13);

        clickOnMenuItem("15. Click on menu item. Index menu item " + 2,EncyclopediaScreen.MENU,2);
        waitForEvent("16. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.DYSGRAPHIA.getResourcePageName());
        waitForFirstHeaderVisible("17. wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,18);

        clickOnMenuItem("20. Click on menu item. Index menu item " + 3,EncyclopediaScreen.MENU,3);
        waitForEvent("21. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.DYSLEXIA.getResourcePageName());
        waitForFirstHeaderVisible("22. Wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,23);

        clickOnMenuItem("25. Click on menu item. Index menu item " + 4,EncyclopediaScreen.MENU,4);
        waitForEvent("26. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.DYSORTHOGRAPHIA.getResourcePageName());
        waitForFirstHeaderVisible("27. Wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,28);

        clickOnMenuItem("30. Click on menu item. Index menu item " + 5,EncyclopediaScreen.MENU,5);
        waitForEvent("31. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.MEMORY.getResourcePageName());
        waitForFirstHeaderVisible("32. Wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,33);

        clickOnMenuItem("43. Click on menu item. Index menu item " + 6,EncyclopediaScreen.MENU,6);
        waitForEvent("44. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.SPATIAL_ORIENTATION.getResourcePageName());
        waitForFirstHeaderVisible("45. Wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,46);

        clickOnMenuItem("48. Click on menu item. Index menu item " + 7,EncyclopediaScreen.MENU,7);
        waitForEvent("49. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.SERIALITY.getResourcePageName());
        waitForFirstHeaderVisible("50. Wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,51);

        clickOnMenuItem("53. Click on menu item. Index menu item " + 8,EncyclopediaScreen.MENU,8);
        waitForEvent("54. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.AUDITORY_DISTINCTION.getResourcePageName());
        waitForFirstHeaderVisible("55. Wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,56);

        clickOnMenuItem("58. Click on menu item. Index menu item " + 9,EncyclopediaScreen.MENU,9);
        waitForEvent("59. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.VISUAL_DISTINCTION.getResourcePageName());
        waitForFirstHeaderVisible("60. Wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,61);

        clickOnMenuItem("63. Click on menu item. Index menu item " + 10,EncyclopediaScreen.MENU,10);
        waitForEvent("64. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.VERBAL_SKILLS.getResourcePageName());
        waitForFirstHeaderVisible("65. Wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,66);


        clickOnMenuItem("68. Click on menu item. Index menu item " + 11,EncyclopediaScreen.MENU,11);
        waitForEvent("69. Wait for page showed",EncyclopediaScreen.EVENT_PAGE_SHOWED + MenuItem.FAMOUS_DYSLECTICS.getResourcePageName());
        waitForFirstHeaderVisible("70. Wait for scroll top",EncyclopediaScreen.CONTENT_TABLE);
        moveInPageAndCheckPlayButtons(EncyclopediaScreen.CONTENT_TABLE,71);
    }

    private void waitForFirstHeaderVisible(String infoMessage,String actorName){
        final int DELAY = 200;

        logInfo(infoMessage);
        Table content = (Table) findActorByName(actorName);

        Table header = getFirstHeader(content);
        if(header == null)
            return;

        setTimeout(DEFAULT_TASK_TIMEOUT,EVENT_SCROLL_TOP);

        while (true){
            float scene = (getTablexia().getActualScreen().getSceneInnerHeight()-content.getHeight()) > 0 ? (getTablexia().getActualScreen().getSceneInnerHeight()-content.getHeight()) : 0;
            if(content.getY() < scene){
                AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_SCROLL_TOP);
                clearTimeout();
                break;
            }
            wait(DELAY);

        }

        logOK();
    }


    private Table getFirstHeader(Table content){
        for(Cell cell : content.getCells()){
            if(cell.getActor() instanceof Table){
                return (Table) cell.getActor();
            }
        }

        return null;
    }


    private void moveInPageAndCheckPlayButtons(String actorName,int steps){

        //wait for content reset position
        wait(2000);

        Table content = (Table) findActorByName(actorName);

        if(content != null){

            int moveY = 0;

            for(Cell cell : content.getCells()){

                if(cell.getActor() instanceof Table){
                    Table table = (Table) cell.getActor();

                    PlaybackStateButton playbackStateButton = findPlayButtonInTable(table);
                    if(playbackStateButton!=null){
                        //table have play button
                        //move to start table
                        if(moveY > 0) {
                            moveInTable(steps + ". Move to next header in encyclopedia page",table,moveY);
                            steps++;
                        }

                        logInfo(steps + ". Click music buttons on screen" );
                        steps++;

                        int clickX = (int) (table.getX() + table.getWidth() - playbackStateButton.getWidth()/2);
                        int clickY = (int) (table.getY() + content.getY()+ playbackStateButton.getHeight()/2);

                        touchDown(clickX,clickY,0,Input.Buttons.LEFT,playbackStateButton.getStage());
                        touchUp(clickX,clickY,0,Input.Buttons.LEFT,playbackStateButton.getStage());

                        logOK();

                        takeScreenShotBlocking();
                        moveY = 0;
                    }

                }

                moveY += cell.getActor().getHeight() + Content.PAD_RIGHT + Content.PAD_BOTTOM;

            }

            //move to end of page
            moveInTable(steps + ". Move to end in encyclopedia page",content,moveY);
            steps++;
            takeScreenShotBlocking();


        }else{
            logError("Actor "+actorName + " not found! " );
            stopTheTest();
        }
    }

    private void printActorPosition(String name,Actor actor){
        Log.info(getClass(),name + " " + actor.getX() +" - " +actor.getY() + " W " + actor.getWidth() + " H " + actor.getHeight());
    }

    private void moveInTable(String infoMessage,Table table,int moveY){
        logInfo(infoMessage);
        final int firstFingerPointer  = 0;

        final float duration = 0.5f;

        int startX = (int) (table.getX() + table.getWidth() / 2);
        int startY = 50;

        int finishX = startX;
        int finishY = moveY;

        boolean result = true;

        result &= touchDown(startX, startY, firstFingerPointer, Input.Buttons.LEFT, table.getStage());
        result &= dragOverTime(startX, startY, finishX, finishY, firstFingerPointer, duration, 30, table.getStage());
        result &= touchUp(finishX, finishY,firstFingerPointer, Input.Buttons.LEFT, table.getStage());

        if(!result){
            logError("Can not move in table group");
            stopTheTest();
        }

        logOK();
    }

    private PlaybackStateButton findPlayButtonInTable(Table table){
        for(Cell cell: table.getCells()){
            if(cell.getActor() instanceof PlaybackStateButton){

                return (PlaybackStateButton) cell.getActor();
            }
        }
        return null;
    }

    private void clickOnMenuItem(String infoMessage,String actorName,int indexItem){
        logInfo(infoMessage);

        MenuWidget menu = (MenuWidget) findActorByName(actorName);

        if(menu != null){

            if(indexItem > menu.getMenuItemCount() -1){
                logError("Index menu item: " + indexItem + " too large");
                stopTheTest();
            }

            TablexiaButton button = (TablexiaButton) findActorByName(menu.getMenuItemTitle(indexItem));
            if(button == null){
                logError("Actor " + menu.getMenuItemTitle(indexItem) + " not found!");
                stopTheTest();
            }

            clickInTheActor(button);

            logOK();
        }else{
            logError("Actor "+actorName + " not found! " );
            stopTheTest();
        }

    }

}
