/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.testing;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.statistics.CustomGroup;
import cz.nic.tablexia.screen.statistics.StatisticsGameScoreResolver;
import cz.nic.tablexia.screen.statistics.StatisticsScreen;
import cz.nic.tablexia.screen.statistics.views.GraphPane;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.ui.button.TablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;

/**
 * Created by Maril on 06.02.2017.
 */

public abstract class AbstractTestStatistics extends AbstractTestScenario {
    public static final String SCENARIO_MOVE_GRAPH = "actor visible";

    private int gameChecked = 1;

    protected int eventReadyCount = 0;

    private final int firstFingerPointer = 0;
    private final float duration = 1f;

    public AbstractTestStatistics(Tablexia tablexia) {
        super(tablexia);
    }

    protected void runStatistics(GameDefinition gameDefinition, GameMenuDefinition gameMenuDefinition) {
        User user = createUser();
        logIn(user);

        //set data to databases
        setGameData(user, gameDefinition);

        //open statistics
        waitForEvent("1. Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt("2. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("3. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton("4. Click at Statistics button in the menu", MainMenuDefinition.STATISTICS);

        waitForScreen("5. Wait for screen Statistics", StatisticsScreen.class);
        waitForEvent("6. Wait for graph ready event", StatisticsScreen.EVENT_GRAPH_READY + eventReadyCount);
        eventReadyCount++;
        unSelectAllDifficulties(7);

        selectDifficultyAndCheckGraph(10, true, false, false);

        //test easy game data
        checkDaoDataWithGraphData(15, StatisticsScreen.GRAPH_PANE, user, gameMenuDefinition, GameDifficulty.EASY);
        checkPositionOfGraphPoints(16, StatisticsScreen.GRAPH_PANE, user, gameMenuDefinition, GameDifficulty.EASY);

        takeScreenShotBlocking();

        clickPointOnGraph("17. Click point on graph. Index point = " + 3, StatisticsScreen.GRAPH_PANE, GameDifficulty.EASY, 3);
        waitForDialog("18. Wait for dialog", StatisticsScreen.DIALOG);
        checkDialogPointData("19. Check dialog data", StatisticsScreen.DIALOG, user, gameMenuDefinition, GameDifficulty.EASY, 3, true);
        //select medium difficulty
        selectDifficultyAndCheckGraph(20, false, true, false);

        checkDaoDataWithGraphData(25, StatisticsScreen.GRAPH_PANE, user, gameMenuDefinition, GameDifficulty.MEDIUM);
        checkPositionOfGraphPoints(26, StatisticsScreen.GRAPH_PANE, user, gameMenuDefinition, GameDifficulty.MEDIUM);

        takeScreenShotBlocking();

        clickPointOnGraph("27. Click point on graph. Index point = " + 0, StatisticsScreen.GRAPH_PANE, GameDifficulty.MEDIUM, 0);
        waitForDialog("28. Wait for dialog", StatisticsScreen.DIALOG);
        checkDialogPointData("29. Check dialog data", StatisticsScreen.DIALOG, user, gameMenuDefinition, GameDifficulty.MEDIUM, 0, true);

        //select hard difficulty
        selectDifficultyAndCheckGraph(30, false, false, true);

        checkDaoDataWithGraphData(35, StatisticsScreen.GRAPH_PANE, user, gameMenuDefinition, GameDifficulty.HARD);
        checkPositionOfGraphPoints(36, StatisticsScreen.GRAPH_PANE, user, gameMenuDefinition, GameDifficulty.HARD);

        takeScreenShotBlocking();

        clickPointOnGraph("37. Click point on graph. Index point = " + 0, StatisticsScreen.GRAPH_PANE, GameDifficulty.HARD, 0);
        waitForDialog("38. Wait for dialog", StatisticsScreen.DIALOG);
        checkDialogPointData("39. Check dialog data", StatisticsScreen.DIALOG, user, gameMenuDefinition, GameDifficulty.HARD, 0, true);


        unSelectAllDifficulties(40);

        selectDifficultyAndCheckGraph(43, true, false, false);

        //Test statistic move graph

        moveWithGraph("48. Move with graph to left", StatisticsScreen.GRAPH_PANE, true);

        takeScreenShotBlocking();

        moveWithGraph("49. Move with graph to right", StatisticsScreen.GRAPH_PANE, false);

        //control average
        clickAt("50. Switch graph type to average", StatisticsScreen.SWITCH_GRAPH_AVERAGE);

        waitForEvent("51. Wait for graph ready event", StatisticsScreen.EVENT_GRAPH_READY + eventReadyCount);
        eventReadyCount++;

        checkAverageScore("52. Check average score", StatisticsScreen.GRAPH_PANE, user, gameMenuDefinition, GameDifficulty.EASY);
        takeScreenShotBlocking();
    }

    private void checkAverageScore(String infoMessage, String actorName, User user, GameMenuDefinition definition, GameDifficulty difficulty) {
        logInfo(infoMessage);

        GraphPane graphPane = (GraphPane) findActorByName(actorName);
        StatisticsGameScoreResolver statisticsGameScoreResolver = new StatisticsGameScoreResolver(definition.getGameDefinition());
        if (graphPane != null) {
            List<Game> daoGames = getDaoDataByGameDifficulty(GameDAO.getGamesForUserAndDefinition(user.getId(), definition.getGameDefinition()), difficulty);
            List<Float> averageScoreDao = statisticsGameScoreResolver.getDailyAverageScores(daoGames);

            if (averageScoreDao.isEmpty()) {
                logError("Database is empty!");
                stopTheTest();
            }

            List<Float> averageScoreGraph = getAverageScoreByDifficulty(graphPane, difficulty);

            if (averageScoreDao.size() != averageScoreGraph.size()) {
                logError("Size average score in database is different than graph average score");
                stopTheTest();
            }

            for (int i = 0; i < averageScoreDao.size(); i++) {

                if (averageScoreDao.get(i).floatValue() != averageScoreGraph.get(i).floatValue()) {
                    logError("Data in index (" + i + ") is different in database than graph data");
                    stopTheTest();
                }
            }

            logOK();

        } else {
            logError("Actor  " + actorName + " not found!");
            stopTheTest();
        }

    }

    private void clickPointOnGraph(String infoMessage, String actorName, GameDifficulty difficulty, int indexPoint) {
        logInfo(infoMessage);

        GraphPane graphPane = (GraphPane) findActorByName(actorName);

        if (graphPane != null) {

            Container<Group> notebook = (Container<Group>) findActorByName(StatisticsScreen.NOTEBOOK);
            Stack graphs = (Stack) findActorByName(StatisticsScreen.GRAPHS);

            if (notebook != null && graphs != null) {
                List<Image> clickAreas = getGraphPointsByDifficulty(graphPane, difficulty);
                if (indexPoint > clickAreas.size() - 1) {
                    logError("Point on index (" + indexPoint + ") not exists!");
                    stopTheTest();
                }

                Actor point = graphPane.getClickAreas().get(indexPoint);

                if (clickInTheActor(point)) {
                    logOK();
                } else {
                    logError("Cannot click on point " + indexPoint);
                    stopTheTest();
                }

            } else {
                logError("Some actors not found:  " +
                        StatisticsScreen.NOTEBOOK + " - " + (notebook != null) + " " +
                        StatisticsScreen.GRAPHS + " - " + (graphs != null) + " ");
                stopTheTest();
            }


        } else {
            logError("Actor  " + actorName + " not found!");
            stopTheTest();
        }
    }

    private void checkDialogPointData(String infoMessage, String dialogKey, User user, GameMenuDefinition definition, GameDifficulty difficulty, int gameIndex, boolean isGamePoint) {
        logInfo(infoMessage);

        TablexiaComponentDialog dialog = getDialog(dialogKey);
        if (dialog == null) {
            logError("Couldn't find text dialog with key: " + dialogKey);
            stopTheTest();
            return;
        }

        if (!dialog.isVisible()) {
            logError("Dialog with key: " + dialogKey + " not showed");
            stopTheTest();
            return;
        }


        List<Game> daoGames = getDaoDataByGameDifficulty(GameDAO.getGamesForUserAndDefinition(user.getId(), definition.getGameDefinition()), difficulty);
        Game game = daoGames.get(gameIndex);

        String gameTitle = definition.getTitle();
        String difText = String.format("%s %s", ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.GAME_DIFFICULTY_NAME), difficulty.getTextDescription(), false);
        String date = game.getGameStartDate(isGamePoint);


        List<TextContentDialogComponent> components = getTextComponentFromDialog(dialog.getDialogComponents());
        boolean findTitle = findTextInDialog(gameTitle, components);
        boolean findDifText = findTextInDialog(difText, components);
        boolean findDate = findTextInDialog(date, components);

        if (!findTitle || !findDifText || !findDate) {
            logError("Dialog data is different Title: " + findTitle + " Difficulty: " + findDifText + " Date: " + findDate);
            stopTheTest();
        }

        logOK();


    }

    private boolean findTextInDialog(String findText, List<TextContentDialogComponent> components) {
        for (TextContentDialogComponent component : components) {
            if (component.getText().equals(findText)) {
                return true;
            }
        }
        return false;
    }

    private List<TextContentDialogComponent> getTextComponentFromDialog(List<TablexiaDialogComponentAdapter> components) {
        List<TextContentDialogComponent> textContentDialogComponents = new ArrayList<>();
        for (TablexiaDialogComponentAdapter componentDialog : components) {
            if (componentDialog instanceof TextContentDialogComponent) {
                TextContentDialogComponent textContentDialogComponent = (TextContentDialogComponent) componentDialog;
                textContentDialogComponents.add(textContentDialogComponent);
            }
        }

        return textContentDialogComponents;
    }


    private void moveWithGraph(String infoMessage, String actorName, boolean toLeft) {
        logInfo(infoMessage);


        GraphPane graphPane = (GraphPane) findActorByName(actorName);
        if (graphPane != null) {

            int startX = (int) (getTablexia().getActualScreen().getSceneWidth() - getTablexia().getActualScreen().getSceneWidth() / 2);
            int startY = (int) (getTablexia().getActualScreen().getSceneInnerHeight() / 2);

            int finishX = toLeft ? startX - 200 : startX + 200;

            setTimeout(DEFAULT_TASK_TIMEOUT, SCENARIO_MOVE_GRAPH);

            Stage stage = graphPane.getStage();

            boolean result = touchDown(startX, startY, firstFingerPointer, Input.Buttons.LEFT, stage);
            result &= dragOverTime(startX, startY, finishX, startY, firstFingerPointer, duration, 40, stage);
            result &= touchUp(finishX, startY, 0, Input.Buttons.LEFT, stage);

            if (!result) {
                logError("Cannot move with graph");
                stopTheTest();
            }

            AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_MOVE_GRAPH);
            clearTimeout();

        } else {
            logError("Actor  " + actorName + " not found!");
            stopTheTest();
        }

        logOK();
    }

    private void checkPositionOfGraphPoints(int step, String actorName, User user, GameMenuDefinition definition, GameDifficulty difficulty) {
        logInfo(step + ". Control position of graph points  - Game Def: " + definition + " Dif: " + difficulty);

        GraphPane graphPane = (GraphPane) findActorByName(actorName);

        if (graphPane != null) {
            List<Game> daoGames = getDaoDataByGameDifficulty(GameDAO.getGamesForUserAndDefinition(user.getId(), definition.getGameDefinition()), difficulty);
            List<Image> graphPoints = getGraphPointsByDifficulty(graphPane, difficulty);

            for (int i = 0; i < graphPoints.size(); i++) {
                Image point = graphPoints.get(i);
                Game game = daoGames.get(i);


                float scoreDao = definition.getGameDefinition().getGameResultResolver(game.getGameScoreResolverVersion()).getComparisonScore(game);

                float xDaoPosition = getXPositionByScore(graphPane, daoGames.size(), i);
                float yDaoPosition = getYPositionByScore(scoreDao, graphPane, isFlipOfGraphNeeded(definition.getGameDefinition()));

                if (point.getX() != xDaoPosition || point.getY() != yDaoPosition) {
                    logError(" Calculated position in index(" + i + ") by score in the database is different than the position of point in a graph!");
                    stopTheTest();
                }
            }


            logOK();
        } else {
            logError("Actor  " + actorName + " not found!");
            stopTheTest();
        }
    }

    private float getXPositionByScore(GraphPane graphPane, int numberOfGames, int index) {
        float graphPadding = 40f;

        if (numberOfGames < 2) {
            return (graphPane.getWidth() / 2) - (GraphPane.POINT_SIZE / 2);
        } else {
            return (graphPadding + ((graphPane.getWidth() - GraphPane.POINT_SIZE - 2 * graphPadding) / (numberOfGames - 1)) * index);
        }
    }

    private float getYPositionByScore(float score, GraphPane graphPane, boolean flippedGraph) {
        float graphPadding = 40f;
        float maxScore = graphPane.getMaxScore();
        float minScore = graphPane.getMinScore();

        if (maxScore == minScore) {
            return (graphPane.getHeight() / 2 - GraphPane.POINT_SIZE / 2);
        } else {
            float differenceScore = (flippedGraph ? maxScore - score : score - minScore);
            return (((((differenceScore) / (maxScore - minScore))) * (graphPane.getHeight() - GraphPane.POINT_SIZE - 2 * graphPadding)) + graphPadding);
        }
    }

    private void checkDaoDataWithGraphData(int step, String actorName, User user, GameMenuDefinition definition, GameDifficulty difficulty) {
        logInfo(step + ". Check database data with graph visual data - Game Def: " + definition + " Dif: " + difficulty);

        GraphPane graphPane = (GraphPane) findActorByName(actorName);

        if (graphPane != null) {
            List<Game> daoGames = getDaoDataByGameDifficulty(GameDAO.getGamesForUserAndDefinition(user.getId(), definition.getGameDefinition()), difficulty);
            List<Game> graphGames = getGraphDataByDifficulty(graphPane, difficulty);

            if (daoGames.size() != graphGames.size()) {
                logError("GameDAO items (" + daoGames.size() + ") is different than Graph items(" + graphGames.size() + ") - Game Def: " + definition + " Dif: " + difficulty);
                stopTheTest();
            }

            List<Image> graphPoints = getGraphPointsByDifficulty(graphPane, difficulty);

            if (daoGames.size() != graphPoints.size()) {
                logError("GameDAO items (" + daoGames.size() + ") is different than Graph points(" + graphPoints.size() + ") - Game Def: " + definition + " Dif: " + difficulty);
                stopTheTest();
            }

            logOK();

        } else {
            logError("Actor  " + actorName + " not found!");
            stopTheTest();
        }

    }

    private List<Game> getDaoDataByGameDifficulty(List<Game> daoGames, GameDifficulty difficulty) {
        List<Game> dataDefinition = new ArrayList<>();
        for (Game game : daoGames) {
            if (game.getGameDifficulty() == difficulty.getDifficultyNumber()) {
                dataDefinition.add(game);
            }
        }
        return dataDefinition;
    }


    private List<Game> getGraphDataByDifficulty(GraphPane graphPane, GameDifficulty difficulty) {
        switch (difficulty) {
            case HARD:
                return graphPane.getHardGames();
            case MEDIUM:
                return graphPane.getMediumGames();
            case EASY:
                return graphPane.getEasyGames();
            default:
                return null;
        }
    }

    private List<Image> getGraphPointsByDifficulty(GraphPane graphPane, GameDifficulty difficulty) {
        switch (difficulty) {
            case HARD:
                return graphPane.getHardPoints();
            case MEDIUM:
                return graphPane.getMediumPoints();
            case EASY:
                return graphPane.getEasyPoints();
            default:
                return null;
        }
    }

    private List<Float> getAverageScoreByDifficulty(GraphPane graphPane, GameDifficulty difficulty) {
        switch (difficulty) {
            case HARD:
                return graphPane.getHardAverageScores();
            case MEDIUM:
                return graphPane.getMediumAverageScores();
            case EASY:
                return graphPane.getEasyAverageScores();
            default:
                return null;
        }
    }

    protected void clickUpDownButtonAndCheckGameButton(int step, String infoMessage, int steps, String buttonKey) {
        //click up/down button

        if (buttonKey.equals(StatisticsScreen.UP__BUTTON)) {
            steps *= -1;
        }

        int gameIndex = (gameChecked + steps) % GameDefinition.values().length;
        if (gameIndex < 0) {
            gameIndex = GameDefinition.values().length + gameIndex;
        }

        for (int i = 0; i < Math.abs(steps); i++) {
            clickAt(step + infoMessage, buttonKey);
            step++;
        }

        testCheckedButton(step, GameDefinition.getSortedGameDefinitionList().get(gameIndex).toString());
        gameChecked = gameIndex;


    }

    protected void clickButtonGameAndTest(int step, GameMenuDefinition definition, int gameIndex) {
        gameChecked = gameIndex;
        CustomGroup gameDefinition = (CustomGroup) findActorByName(definition.toString());
        logInfo(step + ". Make game button " + definition.toString() + " visible and selected.\n");
        int gamesInMenu = GameMenuDefinition.values().length;
        int i=0;
        while(!gameDefinition.isSelected() || !gameDefinition.isVisible()){
            clickAt("\t Actor is not visible - pressing down button to make it visible", StatisticsScreen.DOWN_BUTTON);
            i++;
            if(i == gamesInMenu){
                logError("Cannot find game in menu! All games in menu were visible.");
                stopTheTest();
            }
        }
        logInfo("\t Actor is visible and selected.");
        logOK();
        step++;
        //check button
        testCheckedButton(step, definition.toString());
    }

    private void testCheckedButton(int step, String actorName) {
        logInfo(step + ". Control game button: " + actorName + " is checked.");

        CustomGroup button = (CustomGroup) findActorByName(actorName);
        if (button != null) {
            if (!button.isSelected()) {
                logError("After select actor " + actorName + ". Actor is not checked!");
                stopTheTest();
            } else {
                logOK();
            }
        } else {
            logError("Tablexia button  " + actorName + "not found!");
            stopTheTest();
        }
    }

    private void unSelectAllDifficulties(int step) {
        clickRadioButton(step, StatisticsScreen.DifficultyButtons.EASY.getName(), GameDifficulty.EASY, false);
        step++;
        clickRadioButton(step, StatisticsScreen.DifficultyButtons.MEDIUM.getName(), GameDifficulty.MEDIUM, false);
        step++;
        clickRadioButton(step, StatisticsScreen.DifficultyButtons.HARD.getName(), GameDifficulty.HARD, false);
    }

    private void selectDifficultyAndCheckGraph(int step, boolean easySelect, boolean mediumSelect, boolean hardSelect) {

        boolean easyClick = clickRadioButton(step, StatisticsScreen.DifficultyButtons.EASY.getName(), GameDifficulty.EASY, easySelect);
        boolean mediumClick = clickRadioButton(++step, StatisticsScreen.DifficultyButtons.MEDIUM.getName(), GameDifficulty.MEDIUM, mediumSelect);
        boolean hardClick = clickRadioButton(++step, StatisticsScreen.DifficultyButtons.HARD.getName(), GameDifficulty.HARD, hardSelect);
        step++;
        //Graph had selected difficulties
        if (!easyClick && !mediumClick && !hardClick)
            return;

        waitForEvent(step + ". Wait for graph ready event", StatisticsScreen.EVENT_GRAPH_READY + eventReadyCount);
        step++;
        eventReadyCount++;
        logInfo(step + ". Select difficulty and check with graph data");

        GraphPane graphPane = (GraphPane) findActorByName(StatisticsScreen.GRAPH_PANE);
        if (graphPane != null) {
            Set<GameDifficulty> selectedDifficulties = graphPane.getSelectedDifficulties();


            if (easySelect != selectedDifficulties.contains(GameDifficulty.EASY) || mediumSelect != selectedDifficulties.contains(GameDifficulty.MEDIUM) || hardSelect != selectedDifficulties.contains(GameDifficulty.HARD)) {
                logError("Selected game is different than graph data \n" +
                        "EASY SELECT: " + easySelect + "\n" +
                        "MEDIUM SELECT: " + mediumSelect + "\n" +
                        "HARD SELECT: " + hardSelect + "\n" +
                        "SELECT GRAPH DIFFICULTY:  " + selectedDifficulties.toString() + "\n"

                );
                stopTheTest();
            }

            logOK();
        } else {
            logError("GraphPane  " + StatisticsScreen.GRAPH_PANE + "not found!");
            stopTheTest();
        }
    }

    private boolean clickRadioButton(int numberOperation, String actorName, GameDifficulty difficulty, boolean checked) {
        TablexiaButton button = (TablexiaButton) findActorByName(actorName);

        if (button != null) {
            if (button.isChecked() != checked) {
                clickAt(numberOperation + ". Trigger radio " + difficulty + "  button in the screen", actorName);
                return true;
            } else {
                logInfo(numberOperation + ". Radio " + difficulty + " doest not need trigger. Checked : " + checked);
                logOK();
                return true;
            }
        } else {
            logError("Radio button  " + actorName + "not found!");
            stopTheTest();
        }

        return false;
    }


    private boolean isFlipOfGraphNeeded(GameDefinition definition) {
        switch (definition) {
            case PURSUIT:
            case KIDNAPPING:
            case IN_THE_DARKNESS:
            case MEMORY_GAME:
                return true;
        }
        return false;
    }


    private void setGameData(User user, GameDefinition definition) {
        for (int i = 0; i < 2; i++) {
            setGameScore(user, GameDifficulty.EASY, definition, AbstractTablexiaGame.GameResult.THREE_STAR);
            setGameScore(user, GameDifficulty.EASY, definition, AbstractTablexiaGame.GameResult.ONE_STAR);
            setGameScore(user, GameDifficulty.EASY, definition, AbstractTablexiaGame.GameResult.TWO_STAR);
            setGameScore(user, GameDifficulty.EASY, definition, AbstractTablexiaGame.GameResult.THREE_STAR);
            setGameScore(user, GameDifficulty.EASY, definition, AbstractTablexiaGame.GameResult.TWO_STAR);
            setGameScore(user, GameDifficulty.EASY, definition, AbstractTablexiaGame.GameResult.ONE_STAR);
        }


        setGameScore(user, GameDifficulty.MEDIUM, definition, AbstractTablexiaGame.GameResult.NO_STAR);
        setGameScore(user, GameDifficulty.MEDIUM, definition, AbstractTablexiaGame.GameResult.ONE_STAR);
        setGameScore(user, GameDifficulty.MEDIUM, definition, AbstractTablexiaGame.GameResult.TWO_STAR);

        setGameScore(user, GameDifficulty.HARD, definition, AbstractTablexiaGame.GameResult.NO_STAR);
        setGameScore(user, GameDifficulty.HARD, definition, AbstractTablexiaGame.GameResult.ONE_STAR);
        setGameScore(user, GameDifficulty.HARD, definition, AbstractTablexiaGame.GameResult.TWO_STAR);

    }

}
