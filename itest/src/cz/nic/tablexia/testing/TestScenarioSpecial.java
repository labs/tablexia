/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.testing;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaScreen;
import cz.nic.tablexia.screen.halloffame.HallOfFameScreen;
import cz.nic.tablexia.screen.loader.LoaderScreen;
import cz.nic.tablexia.screen.statistics.StatisticsScreen;

/**
 * Created by drahomir on 6/6/16.
 */
public class TestScenarioSpecial extends AbstractTestScenario {

    public TestScenarioSpecial(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    public void onRunTestScenario() {
        logIn(createUser());

        waitForEvent("1. Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt("2. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("3. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton("4. Click at Hall of Fame button in the menu", MainMenuDefinition.HALL_OF_FAME);
        waitForScreen("5. Wait for screen HallOfFameScreen", HallOfFameScreen.class);

        //Stats
        clickAt("6. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("7. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton("8. Click at Statistics button in the menu", MainMenuDefinition.STATISTICS);
        waitForScreen("9. Wait for screen StatisticsScreen", StatisticsScreen.class);

        //Ency
        clickAt("10. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("11. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton("12. Click at Encyclopedia button in the menu", MainMenuDefinition.ENCYCLOPEDIA);
        waitForScreen("13. Wait for screen EncyclopediaScreen", EncyclopediaScreen.class);

        //Logout
        clickAt("14. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("15. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);


        clickMainMenuButton("16. Click at Logout button in the menu", MainMenuDefinition.LOGOUT);
        waitForDialog("17. Wait for logout dialog", TablexiaSettings.LOGOUT_DIALOG);

        clickDialogButton("18. Click yes in logout dialog", TablexiaSettings.LOGOUT_DIALOG, 1);

        waitForScreen("19. Wait for screen LoaderScreen", LoaderScreen.class);
        waitForEvent("20. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);
    }
}
