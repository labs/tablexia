/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package cz.nic.tablexia.testing;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.safe.AbstractMechanics;
import cz.nic.tablexia.game.games.safe.SafeGame;
import cz.nic.tablexia.game.games.safe.SafeMechanicsMedium;
import cz.nic.tablexia.game.games.safe.actors.SequenceGenerator;
import cz.nic.tablexia.game.games.safe.gameobject.SafeLightImage;
import cz.nic.tablexia.game.games.safe.model.SafeSequence;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.game.trophy.AllTrophiesAnimation;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageGroup;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.screen.halloffame.HallOfFameScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.User;

public class TestScenarioAllTrophies extends AbstractTestScenarioHallOfFameTrophies {
    public TestScenarioAllTrophies(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        User user = createUser();
        logIn(user);

        waitForEvent("1. Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt("2. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("3. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        addTrophies("4. Add first game played trophies to all of the games", user, 1, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);   // first game played trophies + all game played in one day

        addTrophiesWithoutSafeGame("5. Add 3 stars medium trophies to all of the games", user, 1, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);  //3 stars medium trophies + all game played medium for 3 stars

        addTrophies("6. Add 3 stars hard trophies to all of the games", user, 1, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);      //3 stars hard trophies + all game played hard for 3 stars

        addTrophies("7. Add 5 time play trophies to all of the games", user, 2, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);       //5 times played game trophies

        addTrophies("8. Add 10 time play trophies to all of the games", user, 5, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);      //10 times played game trophies

        addTrophies("9. Open bonus level of all of the games", user, 2, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);

        addTrophies("10. Add 3 stars bonus trophies to all of the games", user, 1, GameDifficulty.BONUS, AbstractTablexiaGame.GameResult.THREE_STAR);   //3 stars bonus trophies

        setGameScore3Days("11. Add 3 games in 3 days trophy\n", user);
        setGameScoreUpdate5Days("12. Add 3 games in 3 days trophy\n", user);
        setGameScoreUpdate8Days("13. Add 3 games in 3 days trophy\n", user);

        clickMainMenuButton("14. Click at Hall of Fame button in the menu", MainMenuDefinition.HALL_OF_FAME);
        waitForScreen("15. Wait for screen HallOfFameScreen", HallOfFameScreen.class);


                                                           //  _PLAY1 _PLAY2 _PLAY3 _DIFF1 _DIFF1 _BONUS
        checkGameTrophies(16, user, new boolean[]{    true,  true,  true,  true,  true, true,      //ROBBERY
                                                                true,  true,  true,  true,  true, true,      //PURSUIT
                                                                true,  true,  true,  true,  true, true,      //KIDNAPPING
                                                                true,  true,  true,  true,  true, true,      //NIGHT_WATCH
                                                                true,  true,  true,  true,  true, true,      //SHOOTING_RANGE
                                                                true,  true,  true,  true,  true, true,      //IN_THE_DARKNESS
                                                                true,  true,  true,  true,  true, true,      //CRIME_SCENE
                                                                true,  true,  true,  true,  true, true,      //RUNES
                                                                true,  true,  true,  true,  true, true,      //PROTOCOL
                                                                true,  true,  true,  false,  true, true,      //SAFE
                                                                true,  true,  true,  true,  true, true,      //ON_THE_TRAIL
                                                                true,  true,  true,  true,  true, true,      //MEMORY_GAME
                                                                true,  true,  true,  true,  true, true});    //ATTENTION_GAME

        clickAt("18. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("19. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton("20. Click at Games button in the menu", MainMenuDefinition.GAMES);

        waitForEvent("21. Wait for opened game submenu", AbstractMenu.GAME_SUBMENU_OPENED);

        clickMenuButton("22. Click at game safe button in the menu",GameMenuDefinition.SAFE.toString());

        waitForScreen("23. Wait for game page screen", GamePageScreen.class);

        playSafeMediumOn3Stars(24, user);

        waitForEvent("25. Wait for event all trophy animation", AbstractTablexiaGame.ALL_TROPHY_ANIMATION_SHOWN);

        takeScreenShotBlocking();

        waitForEvent("26. Wait for event all trophy animation finished", AllTrophiesAnimation.ALL_TROPHY_ANIMATION_FINISHED);

        takeScreenShotBlocking();
    }

    private void playSafeMediumOn3Stars(int step, User user) {
        clickAt(step++ + ". Click at medium difficulty button", GamePageGroup.DIFFICULTY_BUTTON_MEDIUM);

        clickAt(step++ + ". Click at start button", GamePageGroup.START_BUTTON);

        waitForEvent(step++ + ". Wait for event preloader button enable", TablexiaApplication.EVENT_PRELOADER_BUTTON_ENABLE);

        clickAt(step++ + ". Click preloader button",TablexiaApplication.PRELOADER_CLOSE_BUTTON);

        waitForScreen(step++ + ". Wait for safe screen", SafeGame.class);

        writeToLogFile("\nSTARTING PLAY. GAME SEED: " + TablexiaSettings.getInstance().getLastSeed() + "\n\n");

        for(int i=1; i<5; i++) {
            playRound(step++, i);
        }
    }

    private void playRound(int step, int round) {
        SafeGame safeGame = (SafeGame) TablexiaApplication.getActualScreen();
        waitForEvent(step + "a. Wait for sounds to be played", SequenceGenerator.SCENARIO_STEP_SEQUENCE_PLAYED);
        SafeSequence safeSequence = safeGame.getSafeSequence();
        for(int i=0; i<safeSequence.getSoundsToSequence().length; i++){
            SafeLightImage safeLightImage = (SafeLightImage)findActorByName(AbstractMechanics.LIGHT_NAME+i);
            if(safeLightImage.getSound().equals(safeGame.getMusic(safeSequence.getExpected().getSoundPath()))){
                dragCableToRightPlug(step, i);
                return;
            }
        }
        logError("Could not click at right light!");
        takeScreenShot();
    }

    private void dragCableToRightPlug(int step, int i){
        final float duration = 0.5f;
        final int firstFingerPointer  = 0;

        Actor connector = findActorByName(SafeMechanicsMedium.CONNECTOR_NAME+i);
        Vector2 connectorPos = new Vector2(getActorPosition(connector, ActorPosition.MIDDLE));

        Actor finalSocket = findActorByName(SafeMechanicsMedium.FINAL_SOCKET_NAME);
        Vector2 finalSocketPos = new Vector2(getActorPosition(finalSocket, ActorPosition.MIDDLE));

        Stage stage = connector.getStage();

        logInfo(step + "b. Move connector" + i + " to final socket");
        boolean result = touchDown(connectorPos.x,connectorPos.y,firstFingerPointer, Input.Buttons.LEFT,stage);

        result &= dragOverTime((int)connectorPos.x,(int)connectorPos.y,(int)finalSocketPos.x,(int)finalSocketPos.y,firstFingerPointer,duration,30,stage);
        result &= touchUp((int)finalSocketPos.x,(int)finalSocketPos.y,firstFingerPointer,Input.Buttons.LEFT,stage);

        if(!result){
            logError("Cannot move connector to final socket!");
            stopTheTest();
        }
        logOK();

        waitForEvent(step + "c. Wait for lightning the light", AbstractMechanics.LIGHT_LIGHTED_UP);
        clickAt(step + "d. Click at done button", SafeGame.DONE_BUTTON);
    }

    private void addTrophies(String infoMessage, User user, int gamesCount, GameDifficulty gameDifficulty, AbstractTablexiaGame.GameResult starCount){
        logInfo(infoMessage + "\n");
        for(int i = 0;i<gamesCount;i++){
            setGameScore(user, gameDifficulty, GameDefinition.ROBBERY, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.CRIME_SCENE, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.IN_THE_DARKNESS, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.NIGHT_WATCH, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.KIDNAPPING, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.PURSUIT, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.RUNES, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.SHOOTING_RANGE, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.PROTOCOL, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.SAFE, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.ON_THE_TRAIL, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.MEMORY_GAME, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.ATTENTION_GAME, starCount);
        }
        logOK();
    }

    private void addTrophiesWithoutSafeGame(String infoMessage, User user, int gamesCount, GameDifficulty gameDifficulty, AbstractTablexiaGame.GameResult starCount){
        logInfo(infoMessage + "\n");
        for(int i = 0;i<gamesCount;i++){
            setGameScore(user, gameDifficulty, GameDefinition.ROBBERY, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.CRIME_SCENE, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.IN_THE_DARKNESS, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.NIGHT_WATCH, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.KIDNAPPING, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.PURSUIT, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.RUNES, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.SHOOTING_RANGE, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.PROTOCOL, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.ON_THE_TRAIL, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.MEMORY_GAME, starCount);
            setGameScore(user, gameDifficulty, GameDefinition.ATTENTION_GAME, starCount);
        }
        logOK();
    }


    private void setGameScore3Days(String infoMessage, User user){
        logInfo(infoMessage);
        setGameScorePlay(user, 1593591315000L);   //1.7.2020 10:15:15
        setGameScorePlay(user, 1593677775000L);   //2.7.2020 10:16:15
        setGameScorePlay(user, 1593764235000L);   //3.7.2020 10:17:15
        logOK();
    }

    private void setGameScoreUpdate5Days(String infoMessage, User user) {
        logInfo(infoMessage);
        setGameScorePlay(user, 1593850695000L);   //4.7.2020 10:18:15
        setGameScorePlay(user, 1593937155000L);   //5.7.2020 10:19:15
        logOK();
    }

    private void setGameScoreUpdate8Days(String infoMessage, User user) {
        logInfo(infoMessage);
        setGameScorePlay(user, 1594023615000L);   //6.7.2020 10:20:15
        setGameScorePlay(user, 1594110075000L);   //7.7.2020 10:21:15
        setGameScorePlay(user, 1594196535000L);   //8.7.2020 10:22:15
        logOK();
    }

    private void setGameScorePlay(User user, long startTime){
        GameDefinition gameDefinition = GameDefinition.PROTOCOL;
        GameDifficulty gameDifficulty = GameDifficulty.EASY;
        AbstractTablexiaGame.GameResult gameResult = AbstractTablexiaGame.GameResult.THREE_STAR;
        writeToLogFile("\tGame score of game " + gameDefinition.toString() + " has been set to result: difficulty " + gameDifficulty.toString() + ", " + gameResult.getStarCount() + " stars" + "\n");
        Game game = GameDAO.createGame(user, gameDifficulty, gameDefinition, new TablexiaRandom(), UserRankManager.UserRank.getInitialRank());
        GameDAO.startGame(game, startTime);
        GameDAO.endGame(game, startTime);
        List<GameScore> gameScoreList = gameDefinition.getGameResultResolver(game.getGameScoreResolverVersion()).getExampleScoreForGameResult(gameDifficulty.getDifficultyDefinition(), gameResult.getGameResultDefinition());
        GameDAO.setGameScores(game, gameScoreList);
    }
}
