/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.testing;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGame;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameBonusNoneStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameBonusOneStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameBonusThreeStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameBonusTwoStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameEasyNoneStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameEasyOneStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameEasyThreeStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameEasyTwoStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameHardNoneStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameHardOneStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameHardThreeStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameHardTwoStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameMediumNoneStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameMediumOneStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameMediumThreeStar;
import cz.nic.tablexia.testing.games.attention_game.TestGameAttentionGameMediumTwoStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneBonusNoneStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneBonusOneStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneBonusThreeStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneBonusTwoStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneEasyNoneStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneEasyOneStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneEasyThreeStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneEasyTwoStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneHardNoneStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneHardOneStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneHardThreeStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneHardTwoStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneMediumNoneStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneMediumOneStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneMediumThreeStar;
import cz.nic.tablexia.testing.games.crime_scene.TestGameCrimeSceneMediumTwoStar;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessBonusNoneStar;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessBonusOneStar;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessBonusTwoStars;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessEasyNoneStar;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessEasyOneStar;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessEasyThreeStars;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessBonusThreeStars;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessBonusScenario;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessEasyScenario;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessHardNoneStar;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessHardOneStar;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessHardThreeStars;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessHardScenario;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessHardTwoStars;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessMediumThreeStars;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessMediumScenario;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessEasyTwoStars;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessMediumNoneStar;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessMediumOneStar;
import cz.nic.tablexia.testing.games.in_the_darkness.TestGameInTheDarknessMediumTwoStars;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingBonusNoneStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingBonusOneStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingBonusThreeStars;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingBonusTwoStars;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingEasyNoneStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingEasyOneStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingEasyThreeStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingEasyTwoStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingHardNoneStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingHardOneStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingHardThreeStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingHardTwoStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingMediumNoneStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingMediumOneStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingMediumThreeStar;
import cz.nic.tablexia.testing.games.kidnapping.TestGameKidnappingMediumTwoStar;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameBonusNoneStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameBonusOneStar;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameBonusThreeStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameBonusTwoStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameEasyNoneStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameEasyOneStar;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameEasyThreeStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameEasyTwoStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameHardNoneStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameHardOneStar;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameHardThreeStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameHardTwoStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameMediumNoneStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameMediumOneStar;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameMediumThreeStars;
import cz.nic.tablexia.testing.games.memory_game.TestMemoryGameMediumTwoStars;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchBonusNoneStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchBonusOneStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchBonusThreeStars;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchBonusTwoStars;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchEasyNoneStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchEasyOneStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchEasyThreeStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchEasyTwoStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchHardNoneStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchHardOneStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchHardThreeStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchHardTwoStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchMediumNoneStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchMediumOneStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchMediumThreeStar;
import cz.nic.tablexia.testing.games.night_watch.TestGameNightWatchMediumTwoStar;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailBonusNoneStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailBonusOneStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailBonusThreeStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailBonusTwoStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailEasyNoneStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailEasyOneStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailEasyThreeStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailEasyTwoStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailHardNoneStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailHardOneStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailHardThreeStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailHardTwoStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailMediumNoneStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailMediumOneStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailMediumThreeStars;
import cz.nic.tablexia.testing.games.on_the_trail.TestGameOnTheTrailMediumTwoStars;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolBonusNoneStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolBonusOneStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolBonusThreeStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolBonusTwoStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolEasyNoneStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolEasyOneStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolEasyThreeStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolEasyTwoStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolHardNoneStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolHardOneStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolHardThreeStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolHardTwoStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolMediumNoneStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolMediumOneStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolMediumThreeStar;
import cz.nic.tablexia.testing.games.protocol.TestGameProtocolMediumTwoStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitBonusNoneStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitBonusOneStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitEasyNoneStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitEasyOneStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitEasyThreeStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitEasyTwoStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitHardNoneStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitHardOneStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitHardThreeStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitHardTwoStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitMediumNoneStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitMediumOneStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitMediumThreeStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitMediumTwoStar;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitBonusThreeStars;
import cz.nic.tablexia.testing.games.pursiut.TestGamePursuitBonusTwoStars;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyBonusNoneStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyBonusOneStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyBonusThreeStars;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyBonusTwoStars;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyEasyNoneStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyEasyOneStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyEasyThreeStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyEasyTwoStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyHardNoneStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyHardOneStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyHardThreeStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyHardTwoStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyMediumNoneStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyMediumOneStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyMediumThreeStar;
import cz.nic.tablexia.testing.games.robbery.TestGameRobberyMediumTwoStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesBonusNoneStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesBonusOneStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesBonusThreeStars;
import cz.nic.tablexia.testing.games.runes.TestGameRunesBonusTwoStars;
import cz.nic.tablexia.testing.games.runes.TestGameRunesEasyNoneStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesEasyOneStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesEasyThreeStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesEasyTwoStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesHardNoneStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesHardOneStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesHardThreeStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesHardTwoStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesMediumNoneStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesMediumOneStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesMediumThreeStar;
import cz.nic.tablexia.testing.games.runes.TestGameRunesMediumTwoStar;
import cz.nic.tablexia.testing.games.safe.TestGameSafeEasyNoneStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeEasyOneStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeEasyThreeStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeEasyTwoStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeHardNoneStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeHardOneStar;
import cz.nic.tablexia.testing.games.safe.TestGameSafeHardThreeStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeHardTwoStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeMediumNoneStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeMediumOneStar;
import cz.nic.tablexia.testing.games.safe.TestGameSafeMediumThreeStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeMediumTwoStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeBonusNoneStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeBonusOneStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeBonusThreeStars;
import cz.nic.tablexia.testing.games.safe.TestGameSafeBonusTwoStars;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeBonusNoneStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeBonusOneStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeBonusThreeStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeBonusTwoStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeEasyNoneStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeEasyOneStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeEasyThreeStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeEasyTwoStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeHardNoneStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeHardOneStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeHardThreeStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeHardTwoStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeMediumNoneStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeMediumOneStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeMediumThreeStar;
import cz.nic.tablexia.testing.games.shooting_range.TestGameShootingRangeMediumTwoStar;
import cz.nic.tablexia.util.Log;

/**
 * Created by aneta on 22.9.16.
 */
public class TestRunner {
    private final String TEST_AVAILABLE_PATH        = "/itest/listOfAvailableITests.txt";

    private ArrayList<Class<? extends AbstractTestScenario>> listOfTests = new ArrayList<Class<? extends AbstractTestScenario>>();
    private Tablexia tablexia;

    public TestRunner(Tablexia tablexia) {
        this.tablexia = tablexia;
        createListOfTests();
    }

    private void createListOfTests(){
        listOfTests.add(NewUserScenario.class);
        listOfTests.add(TestScenarioSpecial.class);
        listOfTests.add(TestScenarioHallOfFameRobberyTrophies.class);
        listOfTests.add(TestScenarioHallOfFamePursuitTrophies.class);
        listOfTests.add(TestScenarioHallOfFameKidnappingTrophies.class);
        listOfTests.add(TestScenarioHallOfFameNightWatchTrophies.class);
        listOfTests.add(TestScenarioHallOfFameShootingRangeTrophies.class);
        listOfTests.add(TestScenarioHallOfFameInTheDarknessTrophies.class);
        listOfTests.add(TestScenarioHallOfFameCrimeSceneTrophies.class);
        listOfTests.add(TestScenarioHallOfFameRunesTrophies.class);
        listOfTests.add(TestScenarioHallOfFameProtocolTrophies.class);
        listOfTests.add(TestScenarioHallOfFameSafeTrophies.class);
        listOfTests.add(TestScenarioHallOfFameOnTheTrailTrophies.class);
        listOfTests.add(TestScenarioHallOfFameMemoryGameTrophies.class);
        listOfTests.add(TestScenarioHallOfFameAttentionGameTrophies.class);
        listOfTests.add(TestScenarioHeapOfTrophies.class);
        listOfTests.add(TestScenarioAllTrophies.class);
        listOfTests.add(TestGameShootingRangeEasyNoneStar.class);
        listOfTests.add(TestGameShootingRangeEasyOneStar.class);
        listOfTests.add(TestGameShootingRangeEasyTwoStar.class);
        listOfTests.add(TestGameShootingRangeEasyThreeStar.class);
        listOfTests.add(TestGameShootingRangeMediumNoneStar.class);
        listOfTests.add(TestGameShootingRangeMediumOneStar.class);
        listOfTests.add(TestGameShootingRangeMediumTwoStar.class);
        listOfTests.add(TestGameShootingRangeMediumThreeStar.class);
        listOfTests.add(TestGameShootingRangeHardNoneStar.class);
        listOfTests.add(TestGameShootingRangeHardOneStar.class);
        listOfTests.add(TestGameShootingRangeHardTwoStar.class);
        listOfTests.add(TestGameShootingRangeHardThreeStar.class);
        listOfTests.add(TestGameShootingRangeBonusNoneStar.class);
        listOfTests.add(TestGameShootingRangeBonusOneStar.class);
        listOfTests.add(TestGameShootingRangeBonusTwoStar.class);
        listOfTests.add(TestGameShootingRangeBonusThreeStar.class);
        listOfTests.add(TestGameRobberyEasyNoneStar.class);
        listOfTests.add(TestGameRobberyEasyOneStar.class);
        listOfTests.add(TestGameRobberyEasyTwoStar.class);
        listOfTests.add(TestGameRobberyEasyThreeStar.class);
        listOfTests.add(TestGameRobberyMediumNoneStar.class);
        listOfTests.add(TestGameRobberyMediumOneStar.class);
        listOfTests.add(TestGameRobberyMediumTwoStar.class);
        listOfTests.add(TestGameRobberyMediumThreeStar.class);
        listOfTests.add(TestGameRobberyHardNoneStar.class);
        listOfTests.add(TestGameRobberyHardOneStar.class);
        listOfTests.add(TestGameRobberyHardTwoStar.class);
        listOfTests.add(TestGameRobberyHardThreeStar.class);
        listOfTests.add(TestGameRobberyBonusNoneStar.class);
        listOfTests.add(TestGameRobberyBonusOneStar.class);
        listOfTests.add(TestGameRobberyBonusTwoStars.class);
        listOfTests.add(TestGameRobberyBonusThreeStars.class);
        listOfTests.add(TestGameInTheDarknessEasyNoneStar.class);
        listOfTests.add(TestGameInTheDarknessEasyOneStar.class);
        listOfTests.add(TestGameInTheDarknessEasyTwoStars.class);
        listOfTests.add(TestGameInTheDarknessEasyThreeStars.class);
        listOfTests.add(TestGameInTheDarknessMediumNoneStar.class);
        listOfTests.add(TestGameInTheDarknessMediumOneStar.class);
        listOfTests.add(TestGameInTheDarknessMediumTwoStars.class);
        listOfTests.add(TestGameInTheDarknessMediumThreeStars.class);
        listOfTests.add(TestGameInTheDarknessHardNoneStar.class);
        listOfTests.add(TestGameInTheDarknessHardOneStar.class);
        listOfTests.add(TestGameInTheDarknessHardTwoStars.class);
        listOfTests.add(TestGameInTheDarknessHardThreeStars.class);
        listOfTests.add(TestGameInTheDarknessBonusNoneStar.class);
        listOfTests.add(TestGameInTheDarknessBonusOneStar.class);
        listOfTests.add(TestGameInTheDarknessBonusTwoStars.class);
        listOfTests.add(TestGameInTheDarknessBonusThreeStars.class);
        listOfTests.add(TestGameInTheDarknessEasyScenario.class);
        listOfTests.add(TestGameInTheDarknessMediumScenario.class);
        listOfTests.add(TestGameInTheDarknessHardScenario.class);
        listOfTests.add(TestGameInTheDarknessBonusScenario.class);
        listOfTests.add(TestGameNightWatchEasyNoneStar.class);
        listOfTests.add(TestGameNightWatchEasyOneStar.class);
        listOfTests.add(TestGameNightWatchEasyTwoStar.class);
        listOfTests.add(TestGameNightWatchEasyThreeStar.class);
        listOfTests.add(TestGameNightWatchMediumNoneStar.class);
        listOfTests.add(TestGameNightWatchMediumOneStar.class);
        listOfTests.add(TestGameNightWatchMediumTwoStar.class);
        listOfTests.add(TestGameNightWatchMediumThreeStar.class);
        listOfTests.add(TestGameNightWatchHardNoneStar.class);
        listOfTests.add(TestGameNightWatchHardOneStar.class);
        listOfTests.add(TestGameNightWatchHardTwoStar.class);
        listOfTests.add(TestGameNightWatchHardThreeStar.class);
        listOfTests.add(TestGameNightWatchBonusNoneStar.class);
        listOfTests.add(TestGameNightWatchBonusOneStar.class);
        listOfTests.add(TestGameNightWatchBonusTwoStars.class);
        listOfTests.add(TestGameNightWatchBonusThreeStars.class);
        listOfTests.add(TestGamePursuitEasyNoneStar.class);
        listOfTests.add(TestGamePursuitEasyOneStar.class);
        listOfTests.add(TestGamePursuitEasyTwoStar.class);
        listOfTests.add(TestGamePursuitEasyThreeStar.class);
        listOfTests.add(TestGamePursuitMediumNoneStar.class);
        listOfTests.add(TestGamePursuitMediumOneStar.class);
        listOfTests.add(TestGamePursuitMediumTwoStar.class);
        listOfTests.add(TestGamePursuitMediumThreeStar.class);
        listOfTests.add(TestGamePursuitHardNoneStar.class);
        listOfTests.add(TestGamePursuitHardOneStar.class);
        listOfTests.add(TestGamePursuitHardTwoStar.class);
        listOfTests.add(TestGamePursuitHardThreeStar.class);
        listOfTests.add(TestGamePursuitBonusNoneStar.class);
        listOfTests.add(TestGamePursuitBonusOneStar.class);
        listOfTests.add(TestGamePursuitBonusTwoStars.class);
        listOfTests.add(TestGamePursuitBonusThreeStars.class);
        listOfTests.add(TestGameCrimeSceneEasyNoneStar.class);
        listOfTests.add(TestGameCrimeSceneEasyOneStar.class);
        listOfTests.add(TestGameCrimeSceneEasyTwoStar.class);
        listOfTests.add(TestGameCrimeSceneEasyThreeStar.class);
        listOfTests.add(TestGameCrimeSceneMediumNoneStar.class);
        listOfTests.add(TestGameCrimeSceneMediumOneStar.class);
        listOfTests.add(TestGameCrimeSceneMediumTwoStar.class);
        listOfTests.add(TestGameCrimeSceneMediumThreeStar.class);
        listOfTests.add(TestGameCrimeSceneHardNoneStar.class);
        listOfTests.add(TestGameCrimeSceneHardOneStar.class);
        listOfTests.add(TestGameCrimeSceneHardTwoStar.class);
        listOfTests.add(TestGameCrimeSceneHardThreeStar.class);
        listOfTests.add(TestGameCrimeSceneBonusNoneStar.class);
        listOfTests.add(TestGameCrimeSceneBonusOneStar.class);
        listOfTests.add(TestGameCrimeSceneBonusTwoStar.class);
        listOfTests.add(TestGameCrimeSceneBonusThreeStar.class);
        listOfTests.add(TestGameKidnappingEasyNoneStar.class);
        listOfTests.add(TestGameKidnappingEasyOneStar.class);
        listOfTests.add(TestGameKidnappingEasyTwoStar.class);
        listOfTests.add(TestGameKidnappingEasyThreeStar.class);
        listOfTests.add(TestGameKidnappingMediumNoneStar.class);
        listOfTests.add(TestGameKidnappingMediumOneStar.class);
        listOfTests.add(TestGameKidnappingMediumTwoStar.class);
        listOfTests.add(TestGameKidnappingMediumThreeStar.class);
        listOfTests.add(TestGameKidnappingHardNoneStar.class);
        listOfTests.add(TestGameKidnappingHardOneStar.class);
        listOfTests.add(TestGameKidnappingHardTwoStar.class);
        listOfTests.add(TestGameKidnappingHardThreeStar.class);
        listOfTests.add(TestGameKidnappingBonusNoneStar.class);
        listOfTests.add(TestGameKidnappingBonusOneStar.class);
        listOfTests.add(TestGameKidnappingBonusTwoStars.class);
        listOfTests.add(TestGameKidnappingBonusThreeStars.class);
        listOfTests.add(TestGameRunesEasyNoneStar.class);
        listOfTests.add(TestGameRunesEasyOneStar.class);
        listOfTests.add(TestGameRunesEasyTwoStar.class);
        listOfTests.add(TestGameRunesEasyThreeStar.class);
        listOfTests.add(TestGameRunesMediumNoneStar.class);
        listOfTests.add(TestGameRunesMediumOneStar.class);
        listOfTests.add(TestGameRunesMediumTwoStar.class);
        listOfTests.add(TestGameRunesMediumThreeStar.class);
        listOfTests.add(TestGameRunesHardNoneStar.class);
        listOfTests.add(TestGameRunesHardOneStar.class);
        listOfTests.add(TestGameRunesHardTwoStar.class);
        listOfTests.add(TestGameRunesHardThreeStar.class);
        listOfTests.add(TestGameRunesBonusNoneStar.class);
        listOfTests.add(TestGameRunesBonusOneStar.class);
        listOfTests.add(TestGameRunesBonusTwoStars.class);
        listOfTests.add(TestGameRunesBonusThreeStars.class);
        listOfTests.add(TestGameProtocolEasyNoneStar.class);
        listOfTests.add(TestGameProtocolEasyOneStar.class);
        listOfTests.add(TestGameProtocolEasyTwoStar.class);
        listOfTests.add(TestGameProtocolEasyThreeStar.class);
        listOfTests.add(TestGameProtocolMediumNoneStar.class);
        listOfTests.add(TestGameProtocolMediumOneStar.class);
        listOfTests.add(TestGameProtocolMediumTwoStar.class);
        listOfTests.add(TestGameProtocolMediumThreeStar.class);
        listOfTests.add(TestGameProtocolHardNoneStar.class);
        listOfTests.add(TestGameProtocolHardOneStar.class);
        listOfTests.add(TestGameProtocolHardTwoStar.class);
        listOfTests.add(TestGameProtocolHardThreeStar.class);
        listOfTests.add(TestGameProtocolBonusNoneStar.class);
        listOfTests.add(TestGameProtocolBonusOneStar.class);
        listOfTests.add(TestGameProtocolBonusTwoStar.class);
        listOfTests.add(TestGameProtocolBonusThreeStar.class);
        listOfTests.add(TestGameSafeEasyNoneStars.class);
        listOfTests.add(TestGameSafeEasyOneStars.class);
        listOfTests.add(TestGameSafeEasyTwoStars.class);
        listOfTests.add(TestGameSafeEasyThreeStars.class);
        listOfTests.add(TestGameSafeMediumNoneStars.class);
        listOfTests.add(TestGameSafeMediumOneStar.class);
        listOfTests.add(TestGameSafeMediumTwoStars.class);
        listOfTests.add(TestGameSafeMediumThreeStars.class);
        listOfTests.add(TestGameSafeHardNoneStars.class);
        listOfTests.add(TestGameSafeHardOneStar.class);
        listOfTests.add(TestGameSafeHardTwoStars.class);
        listOfTests.add(TestGameSafeHardThreeStars.class);
        listOfTests.add(TestGameSafeBonusNoneStars.class);
        listOfTests.add(TestGameSafeBonusOneStars.class);
        listOfTests.add(TestGameSafeBonusTwoStars.class);
        listOfTests.add(TestGameSafeBonusThreeStars.class);
        listOfTests.add(TestMemoryGameEasyNoneStars.class);
        listOfTests.add(TestMemoryGameEasyOneStar.class);
        listOfTests.add(TestMemoryGameEasyTwoStars.class);
        listOfTests.add(TestMemoryGameEasyThreeStars.class);
        listOfTests.add(TestMemoryGameMediumNoneStars.class);
        listOfTests.add(TestMemoryGameMediumOneStar.class);
        listOfTests.add(TestMemoryGameMediumTwoStars.class);
        listOfTests.add(TestMemoryGameMediumThreeStars.class);
        listOfTests.add(TestMemoryGameHardNoneStars.class);
        listOfTests.add(TestMemoryGameHardOneStar.class);
        listOfTests.add(TestMemoryGameHardTwoStars.class);
        listOfTests.add(TestMemoryGameHardThreeStars.class);
        listOfTests.add(TestMemoryGameBonusNoneStars.class);
        listOfTests.add(TestMemoryGameBonusOneStar.class);
        listOfTests.add(TestMemoryGameBonusTwoStars.class);
        listOfTests.add(TestMemoryGameBonusThreeStars.class);
        listOfTests.add(TestGameOnTheTrailEasyNoneStars.class);
        listOfTests.add(TestGameOnTheTrailEasyOneStars.class);
        listOfTests.add(TestGameOnTheTrailEasyTwoStars.class);
        listOfTests.add(TestGameOnTheTrailEasyThreeStars.class);
        listOfTests.add(TestGameOnTheTrailMediumNoneStars.class);
        listOfTests.add(TestGameOnTheTrailMediumOneStars.class);
        listOfTests.add(TestGameOnTheTrailMediumTwoStars.class);
        listOfTests.add(TestGameOnTheTrailMediumThreeStars.class);
        listOfTests.add(TestGameOnTheTrailHardNoneStars.class);
        listOfTests.add(TestGameOnTheTrailHardOneStars.class);
        listOfTests.add(TestGameOnTheTrailHardTwoStars.class);
        listOfTests.add(TestGameOnTheTrailHardThreeStars.class);
        listOfTests.add(TestGameOnTheTrailBonusNoneStars.class);
        listOfTests.add(TestGameOnTheTrailBonusOneStars.class);
        listOfTests.add(TestGameOnTheTrailBonusTwoStars.class);
        listOfTests.add(TestGameOnTheTrailBonusThreeStars.class);
        listOfTests.add(TestGameAttentionGameEasyNoneStar.class);
        listOfTests.add(TestGameAttentionGameEasyOneStar.class);
        listOfTests.add(TestGameAttentionGameEasyTwoStar.class);
        listOfTests.add(TestGameAttentionGameEasyThreeStar.class);
        listOfTests.add(TestGameAttentionGameMediumNoneStar.class);
        listOfTests.add(TestGameAttentionGameMediumOneStar.class);
        listOfTests.add(TestGameAttentionGameMediumTwoStar.class);
        listOfTests.add(TestGameAttentionGameMediumThreeStar.class);
        listOfTests.add(TestGameAttentionGameHardNoneStar.class);
        listOfTests.add(TestGameAttentionGameHardOneStar.class);
        listOfTests.add(TestGameAttentionGameHardTwoStar.class);
        listOfTests.add(TestGameAttentionGameHardThreeStar.class);
        listOfTests.add(TestGameAttentionGameBonusNoneStar.class);
        listOfTests.add(TestGameAttentionGameBonusOneStar.class);
        listOfTests.add(TestGameAttentionGameBonusTwoStar.class);
        listOfTests.add(TestGameAttentionGameBonusThreeStar.class);
        listOfTests.add(TestStatisticsButtons.class);
        listOfTests.add(TestStatisticsRobbery.class);
        listOfTests.add(TestStatisticsCrimeScene.class);
        listOfTests.add(TestStatisticsInTheDarkness.class);
        listOfTests.add(TestStatisticsKidnapping.class);
        listOfTests.add(TestStatisticsNightWatch.class);
        listOfTests.add(TestStatisticsPursuit.class);
        listOfTests.add(TestStatisticsRunes.class);
        listOfTests.add(TestStatisticsShootingRange.class);
        listOfTests.add(TestStatisticsSafe.class);
        listOfTests.add(TestStatisticsProtocol.class);
        listOfTests.add(TestStatisticsOnTheTrail.class);
        listOfTests.add(TestStatisticsMemoryGame.class);
        listOfTests.add(TestStatisticsAttentionGame.class);
        listOfTests.add(TestEncyclopedia.class);
        listOfTests.add(TestProfile.class);
        listOfTests.add(OfficeTestScenario.class);
    }

    public TestRunner(){
        System.out.println("No parameter has been given! Please, launch the application with parameter\n\tdesktop: <TEST_CLASS_NAME>\n\tAndroid: -e testClassName <TEST_CLASS_NAME>");
        createListOfTests();
        String tests = testsToString();
        saveTests(tests);
        System.out.println("Registred tests (<TEST_CLASS_NAME>):" + "\n" +tests);
    }

    private String testsToString(){
        String tests = null;
        for(Class<? extends AbstractTestScenario> testClass : listOfTests){
            if(tests==null) tests = "\t" + testClass.getSimpleName();
            else tests += "\n\t" + testClass.getSimpleName();
        }
        return tests;
    }

    private void saveTests(String tests){

        try {
            File file = new File("../../.." + TEST_AVAILABLE_PATH);
            if (!file.exists()) {
                file.createNewFile();
            }

            FileOutputStream writer = new FileOutputStream(file);
            writer.write((tests).getBytes());
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void runTest(String testClassName){
        if(testClassName==null){
            Log.err(getClass(), "No parameter has been given! Please, launch the application with parameter\n\tdesktop: <TEST_CLASS_NAME>\n\tAndroid: -e testClassName <TEST_CLASS_NAME>");
            String tests = testsToString();
            Log.info(getClass(),"Registred tests (<TEST_CLASS_NAME>):" + tests);
        }
        else {
            for (Class<? extends AbstractTestScenario> testClass : listOfTests) {
                if (testClass.getSimpleName().equals(testClassName)) {
                    try {
                        AbstractTestScenario test = (AbstractTestScenario) ClassReflection.getConstructor(testClass, Tablexia.class).newInstance(tablexia);
                        Log.info(getClass(), "[ RUNNING TEST : " + testClass.getSimpleName() + " ]");
                        test.runTest();
                    } catch (ReflectionException e) {
                        Log.err(getClass(), "Cannot create instance of test " + testClass.getSimpleName() + "! " + e.getMessage());
                    }
                    return;
                }
            }
            Log.err(getClass(), "Test class \"" + testClassName + "\" does not exist!");
        }
        Gdx.app.exit();
    }
}
