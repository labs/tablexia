/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.testing;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.SnapshotArray;

import java.util.Map;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.model.game.UserTrophy;
import cz.nic.tablexia.screen.gamemenu.OfficeMenuScreen;
import cz.nic.tablexia.screen.halloffame.HallOfFameScreen;
import cz.nic.tablexia.screen.halloffame.trophy.TrophyHelper;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.ui.TablexiaImage;

/**
 * Created by aneta on 8.11.16.
 */
public class TestScenarioHeapOfTrophies extends AbstractTestScenario {

    public TestScenarioHeapOfTrophies(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        User user = createUser();
        logIn(user);

        waitForEvent("1. Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt("2. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent("3. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton("4. Click at Hall of Fame button in the menu", MainMenuDefinition.HALL_OF_FAME);
        waitForScreen("5. Wait for screen HallOfFameScreen", HallOfFameScreen.class);

        checkHeapImage(6, 0);
        openAndCheckScoreTable(7, user, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME ROBBERY EASY 0 STARS ---> CHECK TABLE SCORE [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(8, user, GameDefinition.ROBBERY, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(9, 0);
        openAndCheckScoreTable(10, user, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME ROBBERY EASY 1 STARS ---> CHECK TABLE SCORE [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(11, user, GameDefinition.ROBBERY, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(12, 1);
        openAndCheckScoreTable(13, user, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME PURSUIT MEDIUM 0 STARS ---> CHECK TABLE SCORE [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(14, user, GameDefinition.PURSUIT, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(15, 1);
        openAndCheckScoreTable(16, user, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME PURSUIT MEDIUM 1 STARS ---> CHECK TABLE SCORE [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(17, user, GameDefinition.PURSUIT, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(18, 2);
        openAndCheckScoreTable(19, user, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME KIDNAPPING HARD 0 STARS ---> CHECK TABLE SCORE [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(20, user, GameDefinition.KIDNAPPING, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(21, 2);
        openAndCheckScoreTable(22, user, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME KIDNAPPING HARD 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(23, user, GameDefinition.KIDNAPPING, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(24, 3);
        openAndCheckScoreTable(25, user, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME NIGHT_WATCH EASY 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(26, user, GameDefinition.NIGHT_WATCH, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(27, 3);
        openAndCheckScoreTable(28, user, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME NIGHT_WATCH EASY 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(29, user, GameDefinition.NIGHT_WATCH, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(30, 4);
        openAndCheckScoreTable(31, user, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME SHOOTING_RANGE MEDIUM 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(32, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(33, 4);
        openAndCheckScoreTable(34, user, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME SHOOTING_RANGE MEDIUM 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(35, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(36, 5);
        openAndCheckScoreTable(37, user, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS HARD 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(38, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(39, 5);
        openAndCheckScoreTable(40, user, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS HARD 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(41, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(42, 6);
        openAndCheckScoreTable(43, user, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME RUNES EASY 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(44, user, GameDefinition.RUNES, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(45, 6);
        openAndCheckScoreTable(46, user, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME RUNES EASY 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(47, user, GameDefinition.RUNES, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(48, 7);
        openAndCheckScoreTable(49, user, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME CRIME_SCENE MEDIUM 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(50, user, GameDefinition.CRIME_SCENE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(51, 7);
        openAndCheckScoreTable(52, user, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME CRIME_SCENE MEDIUM 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(53, user, GameDefinition.CRIME_SCENE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(54, 8);
        openAndCheckScoreTable(55, user, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME PROTOCOL HARD 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(56, user, GameDefinition.PROTOCOL, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(57, 8);
        openAndCheckScoreTable(58, user, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME PROTOCOL HARD 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(59, user, GameDefinition.PROTOCOL, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(60, 9);
        openAndCheckScoreTable(61, user, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME SAFE EASY 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(62, user, GameDefinition.SAFE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(63, 9);
        openAndCheckScoreTable(64, user, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0);

        writeToLogFile("\nADD GAME SAFE EASY 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(65, user, GameDefinition.SAFE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(66, 10);
        openAndCheckScoreTable(67, user, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0);

        writeToLogFile("\nADD GAME ON_THE_TRAIL MEDIUM 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(68, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(69, 10);
        openAndCheckScoreTable(70, user, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0);

        writeToLogFile("\nADD GAME ON_THE_TRAIL MEDIUM 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(71, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(72, 11);
        openAndCheckScoreTable(73, user, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0);

        writeToLogFile("\nADD GAME MEMORY_GAME HARD 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0]\n");
        addNewScoreAndGoToHallOfFame(74, user, GameDefinition.MEMORY_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(75, 11);
        openAndCheckScoreTable(76, user, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0);

        writeToLogFile("\nADD GAME MEMORY_GAME HARD 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0]\n");
        addNewScoreAndGoToHallOfFame(77, user, GameDefinition.MEMORY_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(78, 12);
        openAndCheckScoreTable(79, user, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0);

        writeToLogFile("\nADD GAME ATTENTION_GAME MEDIUM 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0]\n");
        addNewScoreAndGoToHallOfFame(80, user, GameDefinition.ATTENTION_GAME, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(81, 12);
        openAndCheckScoreTable(82, user, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0);

        writeToLogFile("\nADD GAME ATTENTION_GAME MEDIUM 1 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(83, user, GameDefinition.ATTENTION_GAME, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(84, 13);
        openAndCheckScoreTable(85, user, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME ROBBERY HARD 0 STARS ---> CHECK TABLE SCORE [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(86, user, GameDefinition.ROBBERY, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(87, 13);
        openAndCheckScoreTable(88, user, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME ROBBERY HARD 1 STARS ---> CHECK TABLE SCORE [2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(89, user, GameDefinition.ROBBERY, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(90, 14);
        openAndCheckScoreTable(91, user, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME PURSUIT EASY 0 STARS ---> CHECK TABLE SCORE [2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(92, user, GameDefinition.PURSUIT, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(93, 14);
        openAndCheckScoreTable(94, user, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME PURSUIT EASY 2 STARS ---> CHECK TABLE SCORE [2, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(95, user, GameDefinition.PURSUIT, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(96, 16);
        openAndCheckScoreTable(97, user, 2, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME KIDNAPPING MEDIUM 0 STARS ---> CHECK TABLE SCORE [2, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(98, user, GameDefinition.KIDNAPPING, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(99, 16);
        openAndCheckScoreTable(100, user, 2, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME KIDNAPPING MEDIUM 3 STARS ---> CHECK TABLE SCORE [2, 3, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(101, user, GameDefinition.KIDNAPPING, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(102, 19);
        openAndCheckScoreTable(103, user, 2, 3, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME NIGHT_WATCH HARD 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(104, user, GameDefinition.NIGHT_WATCH, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(105, 19);
        openAndCheckScoreTable(106, user, 2, 3, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME NIGHT_WATCH HARD 2 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(107, user, GameDefinition.NIGHT_WATCH, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(108, 21);
        openAndCheckScoreTable(109, user, 2, 3, 4, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME SHOOTING_RANGE EASY 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(110, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(111, 21);
        openAndCheckScoreTable(112, user, 2, 3, 4, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME SHOOTING_RANGE EASY 1 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(113, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(114, 22);
        openAndCheckScoreTable(115, user, 2, 3, 4, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS MEDIUM 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(116, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(117, 22);
        openAndCheckScoreTable(118, user, 2, 3, 4, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS MEDIUM 2 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(119, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(120, 24);
        openAndCheckScoreTable(121, user, 2, 3, 4, 3, 2, 3, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME RUNES HARD 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 1, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(122, user, GameDefinition.RUNES, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(123, 24);
        openAndCheckScoreTable(124, user, 2, 3, 4, 3, 2, 3, 1, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME RUNES HARD 2 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(125, user, GameDefinition.RUNES, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(126, 26);
        openAndCheckScoreTable(127, user, 2, 3, 4, 3, 2, 3, 3, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME CRIME_SCENE EASY 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 1, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(128, user, GameDefinition.CRIME_SCENE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(129, 26);
        openAndCheckScoreTable(130, user, 2, 3, 4, 3, 2, 3, 3, 1, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME CRIME_SCENE EASY 3 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(131, user, GameDefinition.CRIME_SCENE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(132, 29);
        openAndCheckScoreTable(133, user, 2, 3, 4, 3, 2, 3, 3, 4, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME PROTOCOL MEDIUM 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 1, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(134, user, GameDefinition.PROTOCOL, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(135, 29);
        openAndCheckScoreTable(136, user, 2, 3, 4, 3, 2, 3, 3, 4, 1, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME PROTOCOL MEDIUM 3 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 4, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(137, user, GameDefinition.PROTOCOL, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(138, 32);
        openAndCheckScoreTable(139, user, 2, 3, 4, 3, 2, 3, 3, 4, 4, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME SAFE HARD 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 4, 1, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(140, user, GameDefinition.SAFE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(141, 32);
        openAndCheckScoreTable(142, user, 2, 3, 4, 3, 2, 3, 3, 4, 4, 1, 1, 1, 1);

        writeToLogFile("\nADD GAME SAFE HARD 2 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(143, user, GameDefinition.SAFE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(144, 34);
        openAndCheckScoreTable(145, user, 2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 1, 1, 1);

        writeToLogFile("\nADD GAME ON_THE_TRAIL EASY 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 1, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(146, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(147, 34);
        openAndCheckScoreTable(148, user, 2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 1, 1, 1);

        writeToLogFile("\nADD GAME ON_THE_TRAIL EASY 2 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(149, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(150, 36);
        openAndCheckScoreTable(151, user, 2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 1, 1);

        writeToLogFile("\nADD GAME MEMORY_GAME HARD 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 1, 1]\n");
        addNewScoreAndGoToHallOfFame(152, user, GameDefinition.MEMORY_GAME, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(153, 36);
        openAndCheckScoreTable(154, user, 2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 1, 1);

        writeToLogFile("\nADD GAME MEMORY_GAME HARD 2 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 1]\n");
        addNewScoreAndGoToHallOfFame(155, user, GameDefinition.MEMORY_GAME, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(156, 38);
        openAndCheckScoreTable(157, user, 2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 1);

        writeToLogFile("\nADD GAME ATTENTION_GAME EASY 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 1]\n");
        addNewScoreAndGoToHallOfFame(158, user, GameDefinition.ATTENTION_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(159, 38);
        openAndCheckScoreTable(160, user, 2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 1);

        writeToLogFile("\nADD GAME ATTENTION_GAME EASY 3 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(161, user, GameDefinition.ATTENTION_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(162, 41);
        openAndCheckScoreTable(163, user, 2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME ROBBERY MEDIUM 0 STARS ---> CHECK TABLE SCORE [2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(164, user, GameDefinition.ROBBERY, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(165, 41);
        openAndCheckScoreTable(166, user, 2, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME ROBBERY MEDIUM 3 STARS ---> CHECK TABLE SCORE [5, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(167, user, GameDefinition.ROBBERY, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(168, 44);
        openAndCheckScoreTable(169, user, 5, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME PURSUIT HARD 0 STARS ---> CHECK TABLE SCORE [5, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(170, user, GameDefinition.PURSUIT, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(171, 44);
        openAndCheckScoreTable(172, user, 5, 3, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME PURSUIT HARD 2 STARS ---> CHECK TABLE SCORE [5, 5, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(173, user, GameDefinition.PURSUIT, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(174, 46);
        openAndCheckScoreTable(175, user, 5, 5, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME KIDNAPPING EASY 0 STARS ---> CHECK TABLE SCORE [5, 5, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(176, user, GameDefinition.KIDNAPPING, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(177, 46);
        openAndCheckScoreTable(178, user, 5, 5, 4, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME KIDNAPPING EASY 3 STARS ---> CHECK TABLE SCORE [5, 5, 7, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(179, user, GameDefinition.KIDNAPPING, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(180, 49);
        openAndCheckScoreTable(181, user, 5, 5, 7, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME NIGHT_WATCH MEDIUM 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(182, user, GameDefinition.NIGHT_WATCH, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(183, 49);
        openAndCheckScoreTable(184, user, 5, 5, 7, 3, 2, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME NIGHT_WATCH MEDIUM 3 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 2, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(185, user, GameDefinition.NIGHT_WATCH, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(186, 52);
        openAndCheckScoreTable(187, user, 5, 5, 7, 6, 2, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME SHOOTING_RANGE HARD 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 2, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(188, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(189, 52);
        openAndCheckScoreTable(190, user, 5, 5, 7, 6, 2, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME SHOOTING_RANGE HARD 3 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(191, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(192, 55);
        openAndCheckScoreTable(193, user, 5, 5, 7, 6, 5, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS EASY 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 3, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(194, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(195, 55);
        openAndCheckScoreTable(196, user, 5, 5, 7, 6, 5, 3, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS EASY 3 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(197, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(198, 58);
        openAndCheckScoreTable(199, user, 5, 5, 7, 6, 5, 6, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME RUNES MEDIUM 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 3, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(200, user, GameDefinition.RUNES, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(201, 58);
        openAndCheckScoreTable(202, user, 5, 5, 7, 6, 5, 6, 3, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME RUNES MEDIUM 3 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(203, user, GameDefinition.RUNES, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(204, 61);
        openAndCheckScoreTable(205, user, 5, 5, 7, 6, 5, 6, 6, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME CRIME_SCENE HARD 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 4, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(206, user, GameDefinition.CRIME_SCENE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(207, 61);
        openAndCheckScoreTable(208, user, 5, 5, 7, 6, 5, 6, 6, 4, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME CRIME_SCENE HARD 3 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(209, user, GameDefinition.CRIME_SCENE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(210, 64);
        openAndCheckScoreTable(211, user, 5, 5, 7, 6, 5, 6, 6, 7, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME PROTOCOL EASY 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 4, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(212, user, GameDefinition.PROTOCOL, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(213, 64);
        openAndCheckScoreTable(214, user, 5, 5, 7, 6, 5, 6, 6, 7, 4, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME PROTOCOL EASY 3 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 7, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(215, user, GameDefinition.PROTOCOL, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(216, 67);
        openAndCheckScoreTable(217, user, 5, 5, 7, 6, 5, 6, 6, 7, 7, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME SAFE MEDIUM 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 7, 3, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(218, user, GameDefinition.SAFE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(219, 67);
        openAndCheckScoreTable(220, user, 5, 5, 7, 6, 5, 6, 6, 7, 7, 3, 3, 3, 4);

        writeToLogFile("\nADD GAME SAFE MEDIUM 3 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(221, user, GameDefinition.SAFE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(222, 70);
        openAndCheckScoreTable(223, user, 5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 3, 3, 4);

        writeToLogFile("\nADD GAME ON_THE_TRAIL EASY 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 3, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(224, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(225, 70);
        openAndCheckScoreTable(226, user, 5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 3, 3, 4);

        writeToLogFile("\nADD GAME ON_THE_TRAIL EASY 2 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(227, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(228, 72);
        openAndCheckScoreTable(229, user, 5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 3, 4);

        writeToLogFile("\nADD GAME MEMORY_GAME MEDIUM 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 3, 4]\n");
        addNewScoreAndGoToHallOfFame(230, user, GameDefinition.MEMORY_GAME, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(231, 72);
        openAndCheckScoreTable(232, user, 5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 3, 4);

        writeToLogFile("\nADD GAME MEMORY_GAME MEDIUM 1 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 4]\n");
        addNewScoreAndGoToHallOfFame(233, user, GameDefinition.MEMORY_GAME, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(234, 73);
        openAndCheckScoreTable(235, user, 5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 4);

        writeToLogFile("\nADD GAME ATTENTION_GAME HARD 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 4]\n");
        addNewScoreAndGoToHallOfFame(236, user, GameDefinition.ATTENTION_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(237, 73);
        openAndCheckScoreTable(238, user, 5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 4);

        writeToLogFile("\nADD GAME ATTENTION_GAME HARD 2 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(239, user, GameDefinition.ATTENTION_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(240, 75);
        openAndCheckScoreTable(241, user, 5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME ROBBERY EASY 0 STARS ---> CHECK TABLE SCORE [5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(242, user, GameDefinition.ROBBERY, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(243, 75);
        openAndCheckScoreTable(244, user, 5, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME ROBBERY EASY 3 STARS ---> CHECK TABLE SCORE [8, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(245, user, GameDefinition.ROBBERY, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(246, 78);
        openAndCheckScoreTable(247, user, 8, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME PURSUIT MEDIUM 0 STARS ---> CHECK TABLE SCORE [8, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(248, user, GameDefinition.PURSUIT, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(249, 78);
        openAndCheckScoreTable(250, user, 8, 5, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME PURSUIT MEDIUM 1 STARS ---> CHECK TABLE SCORE [8, 6, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(251, user, GameDefinition.PURSUIT, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(252, 79);
        openAndCheckScoreTable(253, user, 8, 6, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME KIDNAPPING HARD 0 STARS ---> CHECK TABLE SCORE [8, 6, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(254, user, GameDefinition.KIDNAPPING, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(255, 79);
        openAndCheckScoreTable(256, user, 8, 6, 7, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME KIDNAPPING HARD 1 STARS ---> CHECK TABLE SCORE [8, 6, 8, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(257, user, GameDefinition.KIDNAPPING, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(258, 80);
        openAndCheckScoreTable(259, user, 8, 6, 8, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME NIGHT_WATCH EASY 0 STARS ---> CHECK TABLE SCORE [8, 6, 8, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(260, user, GameDefinition.NIGHT_WATCH, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(261, 80);
        openAndCheckScoreTable(262, user, 8, 6, 8, 6, 5, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME NIGHT_WATCH EASY 3 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 5, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(263, user, GameDefinition.NIGHT_WATCH, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(264, 83);
        openAndCheckScoreTable(265, user, 8, 6, 8, 9, 5, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME SHOOTING_RANGE MEDIUM 0 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 5, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(266, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(267, 83);
        openAndCheckScoreTable(268, user, 8, 6, 8, 9, 5, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME SHOOTING_RANGE MEDIUM 3 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(269, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(270, 86);
        openAndCheckScoreTable(271, user, 8, 6, 8, 9, 8, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS HARD 0 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 6, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(272, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(273, 86);
        openAndCheckScoreTable(274, user, 8, 6, 8, 9, 8, 6, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS HARD 3 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(275, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(276, 89);
        openAndCheckScoreTable(277, user, 8, 6, 8, 9, 8, 9, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME RUNES EASY 0 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 6, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(278, user, GameDefinition.RUNES, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(279, 89);
        openAndCheckScoreTable(280, user, 8, 6, 8, 9, 8, 9, 6, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME RUNES EASY 3 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 9, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(281, user, GameDefinition.RUNES, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(282, 92);
        openAndCheckScoreTable(283, user, 8, 6, 8, 9, 8, 9, 9, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME CRIME_SCENE MEDIUM 0 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 9, 7, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(284, user, GameDefinition.CRIME_SCENE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(285, 92);
        openAndCheckScoreTable(286, user, 8, 6, 8, 9, 8, 9, 9, 7, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME CRIME_SCENE MEDIUM 3 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 9, 10, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(287, user, GameDefinition.CRIME_SCENE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(288, 95);
        openAndCheckScoreTable(289, user, 8, 6, 8, 9, 8, 9, 9, 10, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME PROTOCOL HARD 0 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 9, 10, 7, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(290, user, GameDefinition.PROTOCOL, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(291, 95);
        openAndCheckScoreTable(292, user, 8, 6, 8, 9, 8, 9, 9, 10, 7, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME PROTOCOL HARD 3 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 9, 10, 8, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(293, user, GameDefinition.PROTOCOL, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(294, 96);
        openAndCheckScoreTable(295, user, 8, 6, 8, 9, 8, 9, 9, 10, 8, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME SAFE EASY 0 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 9, 10, 8, 6, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(296, user, GameDefinition.SAFE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(297, 96);
        openAndCheckScoreTable(298, user, 8, 6, 8, 9, 8, 9, 9, 10, 8, 6, 5, 4, 6);

        writeToLogFile("\nADD GAME SAFE EASY 1 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 9, 10, 8, 7, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(299, user, GameDefinition.SAFE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(300, 97);
        openAndCheckScoreTable(301, user, 8, 6, 8, 9, 8, 9, 9, 10, 8, 7, 5, 4, 6);

        writeToLogFile("\nADD GAME ON_THE_TRAIL MEDIUM 0 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 9, 10, 8, 7, 5, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(302, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(303, 97);
        openAndCheckScoreTable(304, user, 8, 6, 8, 9, 8, 9, 9, 10, 8, 7, 5, 4, 6);

        writeToLogFile("\nADD GAME ON_THE_TRAIL MEDIUM 1 STARS ---> CHECK TABLE SCORE [8, 6, 8, 9, 8, 9, 9, 10, 8, 7, 8, 4, 6]\n");
        addNewScoreAndGoToHallOfFame(305, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(306, 100);
        openAndCheckScoreTable(307, user, 8, 6, 8, 9, 8, 9, 9, 10, 8, 7, 8, 4, 6);

        writeToLogFile("\nADD TO ALL GAMES 1x MEDIUM 2 STARS ---> CHECK TABLE SCORE [10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 6, 8]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(308, user, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.TWO_STAR, 1);
        checkHeapImage(309, 126);
        openAndCheckScoreTable(310, user, 10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 6, 8);

        writeToLogFile("\nADD GAME MEMORY_GAME HARD 0 STARS ---> CHECK TABLE SCORE [10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 6, 8]\n");
        addNewScoreAndGoToHallOfFame(311, user, GameDefinition.MEMORY_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(312, 126);
        openAndCheckScoreTable(313, user, 10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 6, 8);

        writeToLogFile("\nADD GAME MEMORY_GAME HARD 1 STAR ---> CHECK TABLE SCORE [10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 7, 8]\n");
        addNewScoreAndGoToHallOfFame(314, user, GameDefinition.MEMORY_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(315, 127);
        openAndCheckScoreTable(316, user, 10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 7, 8);

        writeToLogFile("\nADD GAME ATTENTION_GAME EASY 0 STARS ---> CHECK TABLE SCORE [10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 7, 8]\n");
        addNewScoreAndGoToHallOfFame(317, user, GameDefinition.ATTENTION_GAME, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(318, 127);
        openAndCheckScoreTable(319, user, 10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 7, 8);

        writeToLogFile("\nADD GAME ATTENTION_GAME EASY 2 STAR ---> CHECK TABLE SCORE [10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 7, 10]\n");
        addNewScoreAndGoToHallOfFame(420, user, GameDefinition.ATTENTION_GAME, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(421, 129);
        openAndCheckScoreTable(422, user, 10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 7, 10);

        writeToLogFile("\nADD GAME ROBBERY MEDIUM 0 STARS ---> CHECK TABLE SCORE [10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 7, 10]\n");
        addNewScoreAndGoToHallOfFame(423, user, GameDefinition.ROBBERY, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(424, 129);
        openAndCheckScoreTable(425, user, 10, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 7, 10);

        writeToLogFile("\nADD GAME ROBBERY MEDIUM 1 STAR ---> CHECK TABLE SCORE [11, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 7, 10]\n");
        addNewScoreAndGoToHallOfFame(426, user, GameDefinition.ROBBERY, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(427, 130);
        openAndCheckScoreTable(428, user, 11, 8, 10, 11, 10, 11, 11, 12, 10, 9, 10, 7, 10);

        writeToLogFile("\nADD TO ALL GAMES 1x HARD 2 STARS ---> CHECK TABLE SCORE [13, 10, 12, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(429, user, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.TWO_STAR, 1);
        checkHeapImage(430, 156);
        openAndCheckScoreTable(431, user, 13, 10, 12, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12);

        writeToLogFile("\nADD GAME PURSUIT HARD 0 STARS ---> CHECK TABLE SCORE [13, 10, 12, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12]\n");
        addNewScoreAndGoToHallOfFame(432, user, GameDefinition.PURSUIT, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(433, 156);
        openAndCheckScoreTable(434, user, 13, 10, 12, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12);

        writeToLogFile("\nADD GAME PURSUIT HARD 3 STAR ---> CHECK TABLE SCORE [13, 11, 12, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12]\n");
        addNewScoreAndGoToHallOfFame(435, user, GameDefinition.PURSUIT, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(436, 157);
        openAndCheckScoreTable(437, user, 13, 11, 12, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12);

        writeToLogFile("\nADD GAME KIDNAPPING EASY 0 STARS ---> CHECK TABLE SCORE [13, 11, 12, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12]\n");
        addNewScoreAndGoToHallOfFame(438, user, GameDefinition.KIDNAPPING, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(439, 157);
        openAndCheckScoreTable(440, user, 13, 11, 12, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12);

        writeToLogFile("\nADD GAME KIDNAPPING EASY 2 STAR ---> CHECK TABLE SCORE [13, 11, 14, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12]\n");
        addNewScoreAndGoToHallOfFame(441, user, GameDefinition.KIDNAPPING, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(442, 159);
        openAndCheckScoreTable(443, user, 13, 11, 14, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12);

        writeToLogFile("\nADD GAME SHOOTING_RANGE MEDIUM 0 STARS ---> CHECK TABLE SCORE [13, 11, 14, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12]\n");
        addNewScoreAndGoToHallOfFame(444, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(445, 159);
        openAndCheckScoreTable(446, user, 13, 11, 14, 13, 12, 13, 13, 14, 12, 11, 12, 9, 12);

        writeToLogFile("\nADD GAME SHOOTING_RANGE MEDIUM 1 STAR ---> CHECK TABLE SCORE [13, 11, 14, 13, 13, 13, 13, 14, 12, 11, 12, 9, 12]\n");
        addNewScoreAndGoToHallOfFame(447, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(448, 160);
        openAndCheckScoreTable(449, user, 13, 11, 14, 13, 13, 13, 13, 14, 12, 11, 12, 9, 12);

        writeToLogFile("\nADD TO ALL GAMES 1x EASY 3 STARS ---> CHECK TABLE SCORE [16, 14, 17, 16, 16, 16, 16, 17, 15, 14, 15, 12, 15]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(450, user, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR, 1);
        checkHeapImage(451, 199);
        openAndCheckScoreTable(452, user, 16, 14, 17, 16, 16, 16, 16, 17, 15, 14, 15, 12, 15);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS HARD 0 STARS ---> CHECK TABLE SCORE [16, 14, 17, 16, 16, 16, 16, 17, 15, 14, 15, 12, 15]\n");
        addNewScoreAndGoToHallOfFame(453, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(454, 199);
        openAndCheckScoreTable(455, user, 16, 14, 17, 16, 16, 16, 16, 17, 15, 14, 15, 12, 15);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS HARD 1 STAR ---> CHECK TABLE SCORE [16, 14, 17, 16, 16, 17, 16, 17, 15, 14, 15, 12, 15]\n");
        addNewScoreAndGoToHallOfFame(456, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(457, 200);
        openAndCheckScoreTable(458, user, 16, 14, 17, 16, 16, 17, 16, 17, 15, 14, 15, 12, 15);

        writeToLogFile("\nADD GAME RUNES EASY 0 STARS ---> CHECK TABLE SCORE [16, 14, 17, 16, 16, 17, 16, 17, 15, 14, 15, 12, 15]\n");
        addNewScoreAndGoToHallOfFame(459, user, GameDefinition.RUNES, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(460, 200);
        openAndCheckScoreTable(461, user, 16, 14, 17, 16, 16, 17, 16, 17, 15, 14, 15, 12, 15);

        writeToLogFile("\nADD GAME RUNES EASY 1 STAR ---> CHECK TABLE SCORE [16, 14, 17, 16, 16, 17, 17, 17, 15, 14, 15, 12, 15]\n");
        addNewScoreAndGoToHallOfFame(462, user, GameDefinition.RUNES, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(463, 201);
        openAndCheckScoreTable(464, user, 16, 14, 17, 16, 16, 17, 17, 17, 15, 14, 15, 12, 15);

        writeToLogFile("\nADD TO ALL GAMES 1x EASY 3 STARS ---> CHECK TABLE SCORE [19, 17, 20, 19, 19, 20, 20, 20, 18, 17, 18, 15, 18]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(465, user, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR, 1);
        checkHeapImage(466, 240);
        openAndCheckScoreTable(467, user, 19, 17, 20, 19, 19, 20, 20, 20, 18, 17, 18, 15, 18);

        writeToLogFile("\nADD GAME CRIME_SCENE MEDIUM 0 STARS ---> CHECK TABLE SCORE [19, 17, 20, 20, 18, 20, 20, 20, 18, 17, 18, 15, 18]\n");
        addNewScoreAndGoToHallOfFame(468, user, GameDefinition.CRIME_SCENE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(469, 240);
        openAndCheckScoreTable(470, user, 19, 17, 20, 19, 19, 20, 20, 20, 18, 17, 18, 15, 18);

        writeToLogFile("\nADD GAME CRIME_SCENE MEDIUM 3 STARS ---> CHECK TABLE SCORE [19, 17, 20, 19, 19, 20, 20, 23, 18, 17, 18, 15, 18]\n");
        addNewScoreAndGoToHallOfFame(471, user, GameDefinition.CRIME_SCENE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(472, 243);
        openAndCheckScoreTable(473, user, 19, 17, 20, 19, 19, 20, 20, 23, 18, 17, 18, 15, 18);

        writeToLogFile("\nADD GAME PROTOCOL HARD 0 STARS ---> CHECK TABLE SCORE [19, 17, 20, 19, 19, 20, 20, 23, 18, 17, 18, 15, 18]\n");
        addNewScoreAndGoToHallOfFame(474, user, GameDefinition.PROTOCOL, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(475, 243);
        openAndCheckScoreTable(476, user, 19, 17, 20, 19, 19, 20, 20, 23, 18, 17, 18, 15, 18);

        writeToLogFile("\nADD GAME PROTOCOL HARD 3 STARS ---> CHECK TABLE SCORE [19, 17, 20, 19, 19, 20, 20, 23, 21, 17, 18, 15, 18]\n");
        addNewScoreAndGoToHallOfFame(477, user, GameDefinition.PROTOCOL, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(478, 246);
        openAndCheckScoreTable(479, user, 19, 17, 20, 19, 19, 20, 20, 23, 21, 17, 18, 15, 18);

        writeToLogFile("\nADD GAME SAFE EASY 0 STARS ---> CHECK TABLE SCORE [19, 17, 20, 20, 18, 20, 20, 23, 21, 17, 18, 15, 18]\n");
        addNewScoreAndGoToHallOfFame(480, user, GameDefinition.SAFE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(481, 246);
        openAndCheckScoreTable(482, user, 19, 17, 20, 19, 19, 20, 20, 23, 21, 17, 18, 15, 18);

        writeToLogFile("\nADD GAME SAFE EASY 3 STARS ---> CHECK TABLE SCORE [19, 17, 20, 19, 19, 20, 20, 23, 21, 20, 18, 15, 18]\n");
        addNewScoreAndGoToHallOfFame(483, user, GameDefinition.SAFE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(484, 249);
        openAndCheckScoreTable(485, user, 19, 17, 20, 19, 19, 20, 20, 23, 21, 20, 18, 15, 18);

        writeToLogFile("\nADD GAME ON_THE_TRAIL MEDIUM 0 STARS ---> CHECK TABLE SCORE [19, 17, 20, 19, 19, 20, 20, 23, 21, 20, 18, 15, 18]\n");
        addNewScoreAndGoToHallOfFame(486, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(487, 249);
        openAndCheckScoreTable(488, user, 19, 17, 20, 19, 19, 20, 20, 23, 21, 20, 18, 15, 18);

        writeToLogFile("\nADD GAME ON_THE_TRAIL MEDIUM 1 STAR ---> CHECK TABLE SCORE [19, 17, 20, 19, 19, 20, 20, 23, 21, 20, 19, 15, 18]\n");
        addNewScoreAndGoToHallOfFame(489, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(490, 250);
        openAndCheckScoreTable(491, user, 19, 17, 20, 19, 19, 20, 20, 23, 21, 20, 19, 15, 18);

        writeToLogFile("\nADD TO ALL GAMES 1x MEDIUM 3 STARS ---> CHECK TABLE SCORE [22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 18, 21]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(492, user, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR, 1);
        checkHeapImage(493, 289);
        openAndCheckScoreTable(494, user, 22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 18, 21);

        writeToLogFile("\nADD GAME MEMORY_GAME HARD 0 STARS ---> CHECK TABLE SCORE [22, 20, 23, 23, 21, 23, 23, 26, 24, 23, 22, 18, 21]\n");
        addNewScoreAndGoToHallOfFame(495, user, GameDefinition.MEMORY_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(496, 289);
        openAndCheckScoreTable(497, user, 22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 18, 21);

        writeToLogFile("\nADD GAME MEMORY_GAME HARD 3 STARS ---> CHECK TABLE SCORE [22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 21]\n");
        addNewScoreAndGoToHallOfFame(498, user, GameDefinition.MEMORY_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(499, 292);
        openAndCheckScoreTable(500, user, 22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 21);

        writeToLogFile("\nADD GAME ATTENTION_GAME EASY 0 STARS ---> CHECK TABLE SCORE [22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 21]\n");
        addNewScoreAndGoToHallOfFame(501, user, GameDefinition.ATTENTION_GAME, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(502, 292);
        openAndCheckScoreTable(503, user, 22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 21);

        writeToLogFile("\nADD GAME ATTENTION_GAME EASY 3 STARS ---> CHECK TABLE SCORE [22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24]\n");
        addNewScoreAndGoToHallOfFame(504, user, GameDefinition.ATTENTION_GAME, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(505, 295);
        openAndCheckScoreTable(506, user, 22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24);

        writeToLogFile("\nADD GAME ROBBERY MEDIUM 0 STARS ---> CHECK TABLE SCORE [22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24]\n");
        addNewScoreAndGoToHallOfFame(507, user, GameDefinition.ROBBERY, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(508, 295);
        openAndCheckScoreTable(509, user, 22, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24);

        writeToLogFile("\nADD GAME ROBBERY MEDIUM 2 STARS ---> CHECK TABLE SCORE [24, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24]\n");
        addNewScoreAndGoToHallOfFame(510, user, GameDefinition.ROBBERY, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(511, 297);
        openAndCheckScoreTable(512, user, 24, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24);

        writeToLogFile("\nADD GAME PURSUIT HARD 0 STARS ---> CHECK TABLE SCORE [24, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24]\n");
        addNewScoreAndGoToHallOfFame(513, user, GameDefinition.PURSUIT, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(514, 297);
        openAndCheckScoreTable(515, user, 24, 20, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24);

        writeToLogFile("\nADD GAME PURSUIT HARD 2 STARS ---> CHECK TABLE SCORE [24, 22, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24]\n");
        addNewScoreAndGoToHallOfFame(516, user, GameDefinition.PURSUIT, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(517, 299);
        openAndCheckScoreTable(518, user, 24, 22, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24);

        writeToLogFile("\nADD GAME KIDNAPPING EASY 0 STARS ---> CHECK TABLE SCORE [24, 22, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24]\n");
        addNewScoreAndGoToHallOfFame(519, user, GameDefinition.KIDNAPPING, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(520, 299);
        openAndCheckScoreTable(521, user, 24, 22, 23, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24);

        writeToLogFile("\nADD GAME KIDNAPPING EASY 1 STAR ---> CHECK TABLE SCORE [24, 22, 24, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24]\n");
        addNewScoreAndGoToHallOfFame(522, user, GameDefinition.KIDNAPPING, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(523, 300);
        openAndCheckScoreTable(524, user, 24, 22, 24, 22, 22, 23, 23, 26, 24, 23, 22, 21, 24);

        writeToLogFile("\nADD TO ALL GAMES 3x HARD 1 STARS ---> CHECK TABLE SCORE [27, 25, 27, 25, 25, 26, 26, 29, 27, 26, 25, 24, 27]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(525, user, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR, 3);
        checkHeapImage(526, 339);
        openAndCheckScoreTable(527, user, 27, 25, 27, 25, 25, 26, 26, 29, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD GAME NIGHT_WATCH MEDIUM 0 STARS ---> CHECK TABLE SCORE [27, 25, 27, 25, 25, 26, 26, 29, 27, 26, 25, 24, 27]\n");
        addNewScoreAndGoToHallOfFame(528, user, GameDefinition.NIGHT_WATCH, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(529, 339);
        openAndCheckScoreTable(530, user, 27, 25, 27, 25, 25, 26, 26, 29, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD GAME NIGHT_WATCH MEDIUM 3 STARS ---> CHECK TABLE SCORE [27, 25, 27, 28, 25, 26, 26, 29, 27, 26, 25, 24, 27]\n");
        addNewScoreAndGoToHallOfFame(531, user, GameDefinition.NIGHT_WATCH, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(532, 342);
        openAndCheckScoreTable(533, user, 27, 25, 27, 28, 25, 26, 26, 29, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD GAME SHOOTING_RANGE HARD 0 STARS ---> CHECK TABLE SCORE [27, 25, 27, 28, 25, 26, 26, 29, 27, 26, 25, 24, 27]\n");
        addNewScoreAndGoToHallOfFame(534, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(535, 342);
        openAndCheckScoreTable(536, user, 27, 25, 27, 28, 25, 26, 26, 29, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD GAME SHOOTING_RANGE HARD 3 STARS ---> CHECK TABLE SCORE [27, 25, 27, 28, 28, 26, 26, 29, 27, 26, 25, 24, 27]\n");
        addNewScoreAndGoToHallOfFame(537, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(538, 345);
        openAndCheckScoreTable(539, user, 27, 25, 27, 28, 28, 26, 26, 29, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS EASY 0 STARS ---> CHECK TABLE SCORE [27, 25, 27, 28, 28, 26, 26, 29, 27, 26, 25, 24, 27]\n");
        addNewScoreAndGoToHallOfFame(540, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(541, 345);
        openAndCheckScoreTable(542, user, 27, 25, 27, 28, 28, 26, 26, 29, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS EASY 3 STARS ---> CHECK TABLE SCORE [27, 25, 27, 28, 28, 29, 26, 29, 27, 26, 25, 24, 27]\n");
        addNewScoreAndGoToHallOfFame(543, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(544, 348);
        openAndCheckScoreTable(545, user, 27, 25, 27, 28, 28, 29, 26, 29, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD GAME RUNES MEDIUM 0 STARS ---> CHECK TABLE SCORE [27, 25, 27, 28, 28, 29, 26, 29, 27, 26, 25, 24, 27]\n");
        addNewScoreAndGoToHallOfFame(546, user, GameDefinition.RUNES, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(547, 348);
        openAndCheckScoreTable(548, user, 27, 25, 27, 28, 28, 29, 26, 29, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD GAME RUNES MEDIUM 1 STAR ---> CHECK TABLE SCORE [27, 25, 27, 28, 28, 29, 27, 29, 27, 26, 25, 24, 27]\n");
        addNewScoreAndGoToHallOfFame(549, user, GameDefinition.RUNES, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(550, 349);
        openAndCheckScoreTable(551, user, 27, 25, 27, 28, 28, 29, 27, 29, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD GAME CRIME_SCENE HARD 0 STARS ---> CHECK TABLE SCORE [27, 25, 27, 28, 28, 29, 27, 29, 27, 26, 25, 24, 27]\n");
        addNewScoreAndGoToHallOfFame(552, user, GameDefinition.CRIME_SCENE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(553, 349);
        openAndCheckScoreTable(554, user, 27, 25, 27, 28, 28, 29, 27, 29, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD GAME CRIME_SCENE HARD 1 STAR ---> CHECK TABLE SCORE [27, 25, 27, 28, 28, 29, 27, 30, 27, 26, 25, 24, 27]\n");
        addNewScoreAndGoToHallOfFame(555, user, GameDefinition.CRIME_SCENE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(556, 350);
        openAndCheckScoreTable(557, user, 27, 25, 27, 28, 28, 29, 27, 30, 27, 26, 25, 24, 27);

        writeToLogFile("\nADD TO ALL GAMES 1x EASY 3 STARS ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 30, 29, 28, 27, 30]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(558, user, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR, 1);
        checkHeapImage(559, 389);
        openAndCheckScoreTable(560, user, 30, 28, 30, 31, 31, 32, 30, 33, 30, 29, 28, 27, 30);

        writeToLogFile("\nADD GAME PROTOCOL EASY 0 STARS ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 30, 29, 28, 27, 30]\n");
        addNewScoreAndGoToHallOfFame(561, user, GameDefinition.PROTOCOL, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(562, 389);
        openAndCheckScoreTable(563, user, 30, 28, 30, 31, 31, 32, 30, 33, 30, 29, 28, 27, 30);

        writeToLogFile("\nADD GAME PROTOCOL EASY 3 STARS ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 33, 29, 28, 27, 30]\n");
        addNewScoreAndGoToHallOfFame(564, user, GameDefinition.PROTOCOL, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(565, 392);
        openAndCheckScoreTable(566, user, 30, 28, 30, 31, 31, 32, 30, 33, 33, 29, 28, 27, 30);

        writeToLogFile("\nADD GAME SAFE MEDIUM 0 STARS ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 33, 29, 28, 27, 30]\n");
        addNewScoreAndGoToHallOfFame(567, user, GameDefinition.SAFE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(568, 392);
        openAndCheckScoreTable(569, user, 30, 28, 30, 31, 31, 32, 30, 33, 33, 29, 28, 27, 30);

        writeToLogFile("\nADD GAME SAFE MEDIUM 3 STARS ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 28, 27, 30]\n");
        addNewScoreAndGoToHallOfFame(570, user, GameDefinition.SAFE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(571, 395);
        openAndCheckScoreTable(572, user, 30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 28, 27, 30);

        writeToLogFile("\nADD GAME ON_THE_TRAIL HARD 0 STARS ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 28, 27, 30]\n");
        addNewScoreAndGoToHallOfFame(573, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(574, 395);
        openAndCheckScoreTable(575, user, 30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 28, 27, 30);

        writeToLogFile("\nADD GAME ON_THE_TRAIL HARD 3 STARS ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 31, 27, 30]\n");
        addNewScoreAndGoToHallOfFame(576, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(577, 398);
        openAndCheckScoreTable(578, user, 30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 31, 27, 30);

        writeToLogFile("\nADD GAME MEMORY_GAME HARD 0 STARS ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 31, 27, 30]\n");
        addNewScoreAndGoToHallOfFame(579, user, GameDefinition.MEMORY_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(580, 398);
        openAndCheckScoreTable(581, user, 30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 31, 27, 30);

        writeToLogFile("\nADD GAME MEMORY_GAME HARD 1 STAR ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 31, 28, 30]\n");
        addNewScoreAndGoToHallOfFame(582, user, GameDefinition.MEMORY_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(583, 399);
        openAndCheckScoreTable(584, user, 30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 31, 28, 30);

        writeToLogFile("\nADD GAME ATTENTION_GAME EASY 0 STARS ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 31, 28, 30]\n");
        addNewScoreAndGoToHallOfFame(585, user, GameDefinition.ATTENTION_GAME, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.NO_STAR);
        checkHeapImage(586, 399);
        openAndCheckScoreTable(587, user, 30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 31, 28, 30);

        writeToLogFile("\nADD GAME ATTENTION_GAME EASY 1 STAR ---> CHECK TABLE SCORE [30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 31, 28, 31]\n");
        addNewScoreAndGoToHallOfFame(588, user, GameDefinition.ATTENTION_GAME, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(589, 400);
        openAndCheckScoreTable(590, user, 30, 28, 30, 31, 31, 32, 30, 33, 33, 32, 31, 28, 31);

        writeToLogFile("\nADD TO ALL GAMES 3x MEDIUM 1 STARS ---> CHECK TABLE SCORE [33, 31, 33, 34, 34, 35, 33, 36, 36, 35, 34, 31, 34]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(591, user, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR, 3);
        checkHeapImage(592, 439);
        openAndCheckScoreTable(593, user, 33, 31, 33, 34, 34, 35, 33, 36, 36, 35, 34, 31, 34);

        writeToLogFile("\nADD GAME ROBBERY MEDIUM 2 STARS ---> CHECK TABLE SCORE [35, 31, 33, 34, 34, 35, 33, 36, 36, 35, 34, 31, 34]\n");
        addNewScoreAndGoToHallOfFame(594, user, GameDefinition.ROBBERY, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(595, 441);
        openAndCheckScoreTable(596, user, 35, 31, 33, 34, 34, 35, 33, 36, 36, 35, 34, 31, 34);

        writeToLogFile("\nADD GAME PURSUIT HARD 3 STARS ---> CHECK TABLE SCORE [35, 34, 33, 34, 34, 35, 33, 36, 36, 35, 34, 31, 34]\n");
        addNewScoreAndGoToHallOfFame(597, user, GameDefinition.PURSUIT, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(598, 444);
        openAndCheckScoreTable(599, user, 35, 34, 33, 34, 34, 35, 33, 36, 36, 35, 34, 31, 34);

        writeToLogFile("\nADD GAME KIDNAPPING EASY 3 STAR ---> CHECK TABLE SCORE [35, 34, 36, 34, 34, 35, 33, 36, 36, 35, 34, 31, 34]\n");
        addNewScoreAndGoToHallOfFame(600, user, GameDefinition.KIDNAPPING, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(601, 447);
        openAndCheckScoreTable(602, user, 35, 34, 36, 34, 34, 35, 33, 36, 36, 35, 34, 31, 34);

        writeToLogFile("\nADD GAME NIGHT_WATCH MEDIUM 2 STAR ---> CHECK TABLE SCORE [35, 34, 36, 36, 34, 35, 33, 36, 36, 35, 34, 31, 34]\n");
        addNewScoreAndGoToHallOfFame(603, user, GameDefinition.NIGHT_WATCH, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(604, 449);
        openAndCheckScoreTable(605, user, 35, 34, 36, 36, 34, 35, 33, 36, 36, 35, 34, 31, 34);

        writeToLogFile("\nADD GAME SHOOTING_RANGE HARD 1 STAR ---> CHECK TABLE SCORE [35, 34, 36, 36, 35, 35, 33, 36, 36, 35, 34, 31, 34]\n");
        addNewScoreAndGoToHallOfFame(606, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(607, 450);
        openAndCheckScoreTable(608, user, 35, 34, 36, 36, 35, 35, 33, 36, 36, 35, 34, 31, 34);

        writeToLogFile("\nADD TO ALL GAMES 1x HARD 3 STARS ---> CHECK TABLE SCORE [38, 37, 39, 39, 38, 38, 36, 39, 39, 38, 37, 34, 37]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(609, user, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR, 1);
        checkHeapImage(610, 489);
        openAndCheckScoreTable(611, user, 38, 37, 39, 39, 38, 38, 36, 39, 39, 38, 37, 34, 37);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS EASY 1 STAR ---> CHECK TABLE SCORE [38, 37, 39, 39, 38, 39, 36, 39, 39, 38, 37, 34, 37]\n");
        addNewScoreAndGoToHallOfFame(612, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(613, 490);
        openAndCheckScoreTable(614, user, 38, 37, 39, 39, 38, 39, 36, 39, 39, 38, 37, 34, 37);

        writeToLogFile("\nADD GAME RUNES MEDIUM 3 STARS ---> CHECK TABLE SCORE [38, 37, 39, 39, 38, 39, 39, 39, 39, 38, 37, 34, 37]\n");
        addNewScoreAndGoToHallOfFame(615, user, GameDefinition.RUNES, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(616, 493);
        openAndCheckScoreTable(617, user, 38, 37, 39, 39, 38, 39, 39, 39, 39, 38, 37, 34, 37);

        writeToLogFile("\nADD GAME CRIME_SCENE HARD 3 STARS ---> CHECK TABLE SCORE [38, 37, 39, 39, 38, 39, 39, 42, 39, 38, 37, 34, 37]\n");
        addNewScoreAndGoToHallOfFame(618, user, GameDefinition.CRIME_SCENE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(619, 496);
        openAndCheckScoreTable(620, user, 38, 37, 39, 39, 38, 39, 39, 42, 39, 38, 37, 34, 37);

        writeToLogFile("\nADD GAME PROTOCOL EASY 3 STARS ---> CHECK TABLE SCORE [38, 37, 39, 39, 38, 39, 39, 42, 42, 38, 37, 34, 37]\n");
        addNewScoreAndGoToHallOfFame(621, user, GameDefinition.PROTOCOL, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(622, 499);
        openAndCheckScoreTable(623, user, 38, 37, 39, 39, 38, 39, 39, 42, 42, 38, 37, 34, 37);

        writeToLogFile("\nADD GAME SAFE MEDIUM 1 STAR ---> CHECK TABLE SCORE [38, 37, 39, 39, 38, 39, 39, 42, 42, 39, 37, 34, 37]\n");
        addNewScoreAndGoToHallOfFame(624, user, GameDefinition.SAFE, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(625, 500);
        openAndCheckScoreTable(626, user, 38, 37, 39, 39, 38, 39, 39, 42, 42, 39, 37, 34, 37);

        writeToLogFile("\nADD TO ALL GAMES 1x HARD 3 STARS ---> CHECK TABLE SCORE [41, 40, 42, 42, 41, 42, 42, 45, 45, 42, 40, 37, 40]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(627, user, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR, 1);
        checkHeapImage(628, 539);
        openAndCheckScoreTable(629, user, 41, 40, 42, 42, 41, 42, 42, 45, 45, 42, 40, 37, 40);

        writeToLogFile("\nADD GAME ON_THE_TRAIL HARD 1 STAR ---> CHECK TABLE SCORE [41, 40, 42, 42, 41, 42, 42, 45, 45, 42, 41, 37, 40]\n");
        addNewScoreAndGoToHallOfFame(630, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(631, 540);
        openAndCheckScoreTable(632, user, 41, 40, 42, 42, 41, 42, 42, 45, 45, 42, 41, 37, 40);

        writeToLogFile("\nADD GAME MEMORY_GAME EASY 3 STARS ---> CHECK TABLE SCORE [41, 40, 42, 42, 41, 42, 42, 45, 45, 42, 41, 40, 40]\n");
        addNewScoreAndGoToHallOfFame(633, user, GameDefinition.MEMORY_GAME, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(634, 543);
        openAndCheckScoreTable(635, user, 41, 40, 42, 42, 41, 42, 42, 45, 45, 42, 41, 40, 40);

        writeToLogFile("\nADD GAME ATTENTION_GAME MEDIUM 3 STARS ---> CHECK TABLE SCORE [41, 40, 42, 42, 41, 42, 42, 45, 45, 42, 41, 40, 43]\n");
        addNewScoreAndGoToHallOfFame(636, user, GameDefinition.ATTENTION_GAME, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(637, 546);
        openAndCheckScoreTable(638, user, 41, 40, 42, 42, 41, 42, 42, 45, 45, 42, 41, 40, 43);

        writeToLogFile("\nADD GAME ROBBERY HARD 3 STARS ---> CHECK TABLE SCORE [44, 40, 42, 42, 41, 42, 42, 45, 45, 42, 41, 40, 43]\n");
        addNewScoreAndGoToHallOfFame(639, user, GameDefinition.ROBBERY, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(640, 549);
        openAndCheckScoreTable(641, user, 44, 40, 42, 42, 41, 42, 42, 45, 45, 42, 41, 40, 43);

        writeToLogFile("\nADD GAME PURSUIT EASY 1 STAR ---> CHECK TABLE SCORE [44, 41, 42, 42, 41, 42, 42, 45, 45, 42, 41, 40, 43]\n");
        addNewScoreAndGoToHallOfFame(642, user, GameDefinition.PURSUIT, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(643, 550);
        openAndCheckScoreTable(644, user, 44, 41, 42, 42, 41, 42, 42, 45, 45, 42, 41, 40, 43);

        writeToLogFile("\nADD TO ALL GAMES 3x EASY 1 STARS ---> CHECK TABLE SCORE [47, 44, 45, 45, 44, 45, 45, 48, 48, 45, 44, 43, 46]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(645, user, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR, 3);
        checkHeapImage(646, 589);
        openAndCheckScoreTable(647, user, 47, 44, 45, 45, 44, 45, 45, 48, 48, 45, 44, 43, 46);

        writeToLogFile("\nADD GAME KIDNAPPING MEDIUM 2 STARS ---> CHECK TABLE SCORE [47, 44, 47, 45, 44, 45, 45, 48, 48, 45, 44, 43, 46]\n");
        addNewScoreAndGoToHallOfFame(648, user, GameDefinition.KIDNAPPING, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(649, 591);
        openAndCheckScoreTable(650, user, 47, 44, 47, 45, 44, 45, 45, 48, 48, 45, 44, 43, 46);

        writeToLogFile("\nADD GAME NIGHT_WATCH HARD 2 STARS ---> CHECK TABLE SCORE [47, 44, 47, 47, 44, 45, 45, 48, 48, 45, 44, 43, 46]\n");
        addNewScoreAndGoToHallOfFame(651, user, GameDefinition.NIGHT_WATCH, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(652, 593);
        openAndCheckScoreTable(653, user, 47, 44, 47, 47, 44, 45, 45, 48, 48, 45, 44, 43, 46);

        writeToLogFile("\nADD GAME SHOOTING_RANGE EASY 3 STARS ---> CHECK TABLE SCORE [47, 44, 47, 47, 47, 45, 45, 48, 48, 45, 44, 43, 46]\n");
        addNewScoreAndGoToHallOfFame(654, user, GameDefinition.SHOOTING_RANGE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(655, 596);
        openAndCheckScoreTable(656, user, 47, 44, 47, 47, 47, 45, 45, 48, 48, 45, 44, 43, 46);

        writeToLogFile("\nADD GAME IN_THE_DARKNESS MEDIUM 3 STARS ---> CHECK TABLE SCORE [47, 44, 47, 47, 47, 48, 45, 48, 48, 45, 44, 43, 46]\n");
        addNewScoreAndGoToHallOfFame(657, user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(658, 599);
        openAndCheckScoreTable(659, user, 47, 44, 47, 47, 47, 48, 45, 48, 48, 45, 44, 43, 46);

        writeToLogFile("\nADD GAME RUNES HARD 1 STAR ---> CHECK TABLE SCORE [47, 44, 47, 47, 47, 48, 46, 48, 48, 45, 44, 43, 46]\n");
        addNewScoreAndGoToHallOfFame(660, user, GameDefinition.RUNES, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(661, 600);
        openAndCheckScoreTable(662, user, 47, 44, 47, 47, 47, 48, 46, 48, 48, 45, 44, 43, 46);

        writeToLogFile("\nADD TO ALL GAMES 1x MEDIUM 3 STARS ---> CHECK TABLE SCORE [50, 47, 50, 50, 50, 51, 49, 51, 51, 48, 47, 46, 49]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(663, user, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR, 1);
        checkHeapImage(664, 639);
        openAndCheckScoreTable(665, user, 50, 47, 50, 50, 50, 51, 49, 51, 51, 48, 47, 46, 49);

        writeToLogFile("\nADD GAME CRIME_SCENE EASY 3 STARS ---> CHECK TABLE SCORE [50, 47, 50, 50, 50, 51, 49, 54, 51, 48, 47, 46, 49]\n");
        addNewScoreAndGoToHallOfFame(666, user, GameDefinition.CRIME_SCENE, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(667, 642);
        openAndCheckScoreTable(668, user, 50, 47, 50, 50, 50, 51, 49, 54, 51, 48, 47, 46, 49);

        writeToLogFile("\nADD GAME PROTOCOL MEDIUM 3 STARS ---> CHECK TABLE SCORE [50, 47, 50, 50, 50, 51, 49, 54, 54, 48, 47, 46, 49]\n");
        addNewScoreAndGoToHallOfFame(669, user, GameDefinition.PROTOCOL, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(670, 645);
        openAndCheckScoreTable(671, user, 50, 47, 50, 50, 50, 51, 49, 54, 54, 48, 47, 46, 49);

        writeToLogFile("\nADD GAME SAFE HARD 3 STARS ---> CHECK TABLE SCORE [50, 47, 50, 50, 50, 51, 49, 54, 54, 51, 47, 46, 49]\n");
        addNewScoreAndGoToHallOfFame(672, user, GameDefinition.SAFE, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(673, 648);
        openAndCheckScoreTable(674, user, 50, 47, 50, 50, 50, 51, 49, 54, 54, 51, 47, 46, 49);

        writeToLogFile("\nADD GAME ON_THE_TRAIL EASY 1 STAR ---> CHECK TABLE SCORE [50, 47, 50, 50, 50, 51, 49, 54, 54, 51, 48, 46, 49]\n");
        addNewScoreAndGoToHallOfFame(675, user, GameDefinition.ON_THE_TRAIL, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(676, 649);
        openAndCheckScoreTable(677, user, 50, 47, 50, 50, 50, 51, 49, 54, 54, 51, 48, 46, 49);

        writeToLogFile("\nADD GAME MEMORY_GAME MEDIUM 1 STAR ---> CHECK TABLE SCORE [50, 47, 50, 50, 50, 51, 49, 54, 54, 51, 48, 47, 49]\n");
        addNewScoreAndGoToHallOfFame(678, user, GameDefinition.MEMORY_GAME, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(679, 650);
        openAndCheckScoreTable(680, user, 50, 47, 50, 50, 50, 51, 49, 54, 54, 51, 48, 47, 49);

        writeToLogFile("\nADD TO ALL GAMES 3x HARD 1 STARS ---> CHECK TABLE SCORE [53, 50, 53, 53, 53, 54, 52, 57, 57, 54, 51, 50, 52]\n");
        addNewScoresToAllGamesAndGoToHallOfFame(681, user, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR, 3);
        checkHeapImage(682, 689);
        openAndCheckScoreTable(683, user, 53, 50, 53, 53, 53, 54, 52, 57, 57, 54, 51, 50, 52);

        writeToLogFile("\nADD GAME ATTENTION_GAME HARD 3 STARS ---> CHECK TABLE SCORE [53, 50, 53, 53, 53, 54, 52, 57, 57, 54, 51, 50, 55]\n");
        addNewScoreAndGoToHallOfFame(684, user, GameDefinition.ATTENTION_GAME, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(685, 692);
        openAndCheckScoreTable(686, user, 53, 50, 53, 53, 53, 54, 52, 57, 57, 54, 51, 50, 55);

        writeToLogFile("\nADD GAME ROBBERY EASY 2 STARS ---> CHECK TABLE SCORE [55, 50, 53, 53, 53, 54, 52, 57, 57, 54, 51, 50, 55]\n");
        addNewScoreAndGoToHallOfFame(687, user, GameDefinition.ROBBERY, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(688, 694);
        openAndCheckScoreTable(689, user, 55, 50, 53, 53, 53, 54, 52, 57, 57, 54, 51, 50, 55);

        writeToLogFile("\nADD GAME PURSUIT MEDIUM 2 STARS ---> CHECK TABLE SCORE [55, 52, 53, 53, 53, 54, 52, 57, 57, 54, 51, 50, 55]\n");
        addNewScoreAndGoToHallOfFame(690, user, GameDefinition.PURSUIT, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.TWO_STAR);
        checkHeapImage(691, 696);
        openAndCheckScoreTable(692, user, 55, 52, 53, 53, 53, 54, 52, 57, 57, 54, 51, 50, 55);

        writeToLogFile("\nADD GAME KIDNAPPING HARD 3 STAR ---> CHECK TABLE SCORE [55, 52, 56, 53, 53, 54, 52, 57, 57, 54, 51, 50, 55]\n");
        addNewScoreAndGoToHallOfFame(693, user, GameDefinition.KIDNAPPING, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR);
        checkHeapImage(694, 699);
        openAndCheckScoreTable(695, user, 55, 52, 56, 53, 53, 54, 52, 57, 57, 54, 51, 50, 55);

        writeToLogFile("\nADD GAME NIGHT_WATCH EASY 1 STAR ---> CHECK TABLE SCORE [55, 52, 56, 54, 53, 54, 52, 57, 57, 54, 51, 50, 55]\n");
        addNewScoreAndGoToHallOfFame(696, user, GameDefinition.NIGHT_WATCH, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.ONE_STAR);
        checkHeapImage(697, 700);
        openAndCheckScoreTable(698, user, 55, 52, 56, 54, 53, 54, 52, 57, 57, 54, 51, 50, 55);
    }

    private void addNewScoreAndGoToHallOfFame(int stepNumber, User user, GameDefinition game, GameDifficulty gameDifficulty, AbstractTablexiaGame.GameResult gameResult){
        clickAt(stepNumber + "a. Click at door", HallOfFameScreen.ACTOR_DOOR_NAME);
        waitForScreen(stepNumber + "b. Wait for screen OfficeMenuScreen", OfficeMenuScreen.class);

        waitForEvent(stepNumber + "c. Wait for opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(stepNumber + "d. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent(stepNumber + "e. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        setGameScore(user, gameDifficulty, game, gameResult);

        clickMainMenuButton(stepNumber + "f. Click at Hall of Fame button in the menu", MainMenuDefinition.HALL_OF_FAME);
        waitForScreen(stepNumber + "g. Wait for screen HallOfFameScreen", HallOfFameScreen.class);
    }

    private void addNewScoresToAllGamesAndGoToHallOfFame(int stepNumber, User user, GameDifficulty gameDifficulty, AbstractTablexiaGame.GameResult gameResult, int times){
        clickAt(stepNumber + "a. Click at door", HallOfFameScreen.ACTOR_DOOR_NAME);
        waitForScreen(stepNumber + "b. Wait for screen OfficeMenuScreen", OfficeMenuScreen.class);

        waitForEvent(stepNumber + "c. Wait for opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        clickAt(stepNumber + "d. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent(stepNumber + "e. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        setGameScores(user, gameDifficulty, gameResult, times);

        clickMainMenuButton(stepNumber + "f. Click at Hall of Fame button in the menu", MainMenuDefinition.HALL_OF_FAME);
        waitForScreen(stepNumber + "g. Wait for screen HallOfFameScreen", HallOfFameScreen.class);
    }

    private void openAndCheckScoreTable(int stepNumber, User user, int... score) {
        clickAt(stepNumber + "a. Click at heap of trophies", HallOfFameScreen.TROPHY_HEAP_NAME);
        waitForDialog(stepNumber + "b. Wait for score table dialog", HallOfFameScreen.SCORE_TABLE_DIALOG_NAME);
        checkScoreTable(stepNumber, user, score);
        writeVisibleActorsToLogFile();
        waitForActorOnStage(HallOfFameScreen.SCORE_TABLE_DIALOG_NAME);
        writeVisibleActorsToLogFile();
        clickAt(stepNumber + "d. Click at score table", HallOfFameScreen.SCORE_TABLE_DIALOG_NAME);
    }

    private void writeVisibleActorsToLogFile() {
        SnapshotArray<Actor> stageActors = TablexiaApplication.getStage().getRoot().getChildren();
        writeToLogFile("\t Stage actors: ");
        for (Actor stageActor : stageActors){
            writeToLogFile(stageActor.getName() + " ");
        }
        writeToLogFile("\n");
    }

    private void checkScoreTable(int number, User user, int... score) {
        final Map<GameDefinition, Integer> starResults = UserTrophy.AllStarsGame.getNumberOfStarsResults(user);
        logInfo(number + "c. Check score table");
        for (GameDefinition gameDefinition : GameDefinition.getSortedGameDefinitionList()) {
            if (starResults.containsKey(gameDefinition)) {
                if(starResults.get(gameDefinition) != score[gameDefinition.ordinal()]){
                    logError("Score of game " + gameDefinition.getTitle() + " should be " + score[gameDefinition.ordinal()] + "! Score table shows " + starResults.get(gameDefinition) + "");
                    stopTheTest();
                }
            }
        }
        logOK();
    }

    private void checkHeapImage(int stepNumber, int starCount) {
        logInfo(stepNumber + ". Check heap of trophies");
        TablexiaImage heapImage = (TablexiaImage) findActorByName(HallOfFameScreen.TROPHY_HEAP_NAME);
        if(heapImage==null) {
            logError("Cannot find image of heap of trophies!");
            stopTheTest();
        }
        String assetNameForStarCount = TrophyHelper.TrophyHeapAssets.getTrophyHeapAssetNameForStarCount(starCount);
        if(!assetNameForStarCount.equals(heapImage.getId())){
            logError("Image of heap isn't set to" + assetNameForStarCount + "! Shown image of heap: " + heapImage.getId());
            stopTheTest();
        }
        logOK();
    }
}
