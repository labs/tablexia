/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.ranksystem.UserRankManager;

import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.main.MainMenuDefinition;

import cz.nic.tablexia.screen.createuser.FormScreen;
import cz.nic.tablexia.screen.gamemenu.OfficeMenuScreen;
import cz.nic.tablexia.screen.profile.ProfileScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaProgressBar;

/**
 * Created by lmarik on 13.2.17.
 */

public class TestProfile extends AbstractTestScenario {
    private long gameIdCnt = 0;

    public TestProfile(Tablexia tablexia) {
        super(tablexia);
    }

    @Override
    protected void onRunTestScenario() {
        User user = createUser();
        logIn(user);
        UserRankManager.getInstance().onNewUser(user);

        waitForEvent( "1 . Wait for event opened ready", AbstractMenu.SCENARIO_STEP_READY_MENU);

        goToProfileSceen(2);
        checkProfileData("3. Check user data",user);
        checkThereIsNoRank("4. Check badge and progress rank",user);

        backToOffice(5);
        addGames("6. Add games to user " + user.getName(), user,1);

        //RANK_I
        goToProfileSceen(7);
        checkBadgeByRankUser("8. Check badge and progress rank", user, "RANK_I");

        backToOffice(9);
        addGames("10. Add games to user " + user.getName(), user,1);

        //RANK_II
        goToProfileSceen(11);
        checkBadgeByRankUser("12. Check badge and progress rank", user, "RANK_II");

        backToOffice(13);
        addGames("14. Add games to user " + user.getName(), user,1);

        //RANK_III
        goToProfileSceen(15);
        checkBadgeByRankUser("16. Check badge and progress rank", user, "RANK_III");

        backToOffice(17);
        addGames("18. Add games to user " + user.getName(), user,3);

        //RANK_IV
        goToProfileSceen(19);
        checkBadgeByRankUser("20. Check badge and progress rank", user, "RANK_IV");

        backToOffice(21);
        addGames("22. Add games to user " + user.getName(), user,3);

        //RANK_V
        goToProfileSceen(23);
        checkBadgeByRankUser("24. Check badge and progress rank", user, "RANK_V");

        backToOffice(25);
        addGames("26. Add games to user " + user.getName(), user,3);

        //RANK_VI
        goToProfileSceen(27);
        checkBadgeByRankUser("28. Check badge and progress rank", user, "RANK_VI");

        backToOffice(29);
        addGames("30. Add games to user " + user.getName(), user,3);

        //RANK_VII
        goToProfileSceen(31);
        checkBadgeByRankUser("32. Check badge and progress rank", user, "RANK_VII");

        backToOffice(33);
        addGames("34. Add games to user " + user.getName(), user,6);

        //RANK_VIII
        goToProfileSceen(35);
        checkBadgeByRankUser("36. Check badge and progress rank", user, "RANK_VIII");

        backToOffice(37);
        addGames("38. Add games to user " + user.getName(), user,4);

        //RANK_IX
        goToProfileSceen(39);
        checkBadgeByRankUser("40. Check badge and progress rank", user, "RANK_IX");

        backToOffice(41);
        addGames("42. Add games to user " + user.getName(), user,10);

        //RANK_X
        goToProfileSceen(43);
        checkBadgeByRankUser("44. Check badge and progress rank", user, "RANK_X");

        backToOffice(45);
        addGames("46. Add games to user " + user.getName(), user,6);

        //RANK_XI
        goToProfileSceen(47);
        checkBadgeByRankUser("48. Check badge and progress rank", user, "RANK_XI");
    }

    private void checkThereIsNoRank(String infoMessage,User user){
        logInfo(infoMessage);

        TablexiaLabel rankLabel = (TablexiaLabel) findActorByName(ProfileScreen.RANK_LABEL);

        if(rankLabel!=null) {
            logError("User should not have rank yet. ");
            stopTheTest();
        }

        logOK();
        takeScreenShotBlocking();
    }

    private void checkBadgeByRankUser(String infoMessage, User user, String rank){
        logInfo(infoMessage);

        TablexiaLabel rankLabel = (TablexiaLabel) findActorByName(ProfileScreen.RANK_LABEL);
        TablexiaProgressBar progressBar = (TablexiaProgressBar) findActorByName(ProfileScreen.PROGRESS_BAR);

        if(rankLabel != null && progressBar != null ){
            UserRankManager.UserRank userRank = UserRankManager.getInstance().getRank(user);
            UserRankManager.RankProgress rankProgress = UserRankManager.getInstance().getTotalUserRankProgress(user);

            String rankName = ApplicationTextManager.getInstance().getText(userRank.getNameKey());

            //test rank name
            if(!(rankLabel.getText().toString().equals(rankName))){
                logError("User rank name is different than text in the" + ProfileScreen.RANK_LABEL);
                stopTheTest();
            }

            //test progress bar
            float percentRank = MathUtils.clamp(rankProgress.getPercentDone(),0,1);
            if(percentRank != progressBar.getPercent()){
                logError("User progress rank is different than rank in the" + ProfileScreen.PROGRESS_BAR);
                stopTheTest();
            }

            //test badge image
            Image badgeImage = (Image) findActorByName(userRank.toString());
            if(badgeImage==null){
                logError("Badge image with rank " + userRank.toString() + " not exists");
                stopTheTest();
            }
            if(userRank.toString()!=rank){
                logError("User rank " + userRank.toString() + " should be " + rank + "!");
                stopTheTest();
            }
            logOK();
        }else {
            logError("Same actor not found: "+ProfileScreen.PROGRESS_BAR+" = " + (rankLabel!=null) +  " "
                    + ProfileScreen.RANK_LABEL+ " = " + (progressBar != null) + " ");
            stopTheTest();
        }
        takeScreenShotBlocking();
    }

    private void checkProfileData(String infoMessage, User user){
        logInfo(infoMessage);
        TextField name = (TextField) findActorByName(FormScreen.FIELD_FORM_NAME);
        Image femaleImage = (Image) findActorByName(ProfileScreen.IMAGE_FEMALE);
        Image maleImage = (Image) findActorByName(ProfileScreen.IMAGE_MALE);
        TablexiaLabel ageLabel = (TablexiaLabel) findActorByName(ProfileScreen.AGE_LABEL);

        if(name != null && femaleImage!=null && maleImage != null && ageLabel != null){

            //test user name
            if(!(user.getName().equals(name.getText()))){
                logError("Username is different than text in the " + FormScreen.FIELD_FORM_NAME);
                stopTheTest();
            }

            //test gender image
            if(user.getGender().equals(GenderDefinition.FEMALE) != femaleImage.isVisible() && user.getGender().equals(GenderDefinition.MALE) != maleImage.isVisible()){
                logError("User gender is different than visible gender image");
                stopTheTest();
            }

            //test user age
            try {
                if(user.getAge() != Integer.parseInt(ageLabel.getText().toString())){
                    logError("User age is different than " + ProfileScreen.AGE_LABEL);
                    stopTheTest();
                }
            } catch (NumberFormatException e) {
                logError("NumberFormatException in the " + ProfileScreen.AGE_LABEL);
                stopTheTest();
            }
            logOK();
        }else{
            logError("Same actor not found: "+ FormScreen.FIELD_FORM_NAME+ " = " + (name!=null)+ " "
                    + ProfileScreen.IMAGE_FEMALE +" = " + (femaleImage!=null)+ " "
                    + ProfileScreen.IMAGE_MALE + " =" + (maleImage != null)+ " "
                    + ProfileScreen.AGE_LABEL +" = " + (ageLabel != null));
            stopTheTest();
        }
        takeScreenShotBlocking();
    }

    private void goToProfileSceen(int stepNum){
        clickAt(stepNum + "a. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent(stepNum + "b. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(stepNum + "c. Click at Profile button in the menu", MainMenuDefinition.PROFILE);
        waitForScreen(stepNum + "d. Wait for screen Profile", ProfileScreen.class);
    }

    private void backToOffice(int stepNum){
        clickAt(stepNum + "a. Click at open/close menu button", AbstractMenu.OPEN_CLOSE_MENU_BUTTON_NAME);
        waitForEvent(stepNum + "b. Wait for event opened menu", AbstractMenu.SCENARIO_STEP_OPENED_MENU);

        clickMainMenuButton(stepNum + "c. Click at Office button in the menu", MainMenuDefinition.OFFICE);
        waitForScreen(stepNum + "d. Wait for screen Office", OfficeMenuScreen.class);
    }


    private void addGames(String infoMessage ,User user,int gamesCount){
        logInfo(infoMessage);
        UserRankManager rankManager = UserRankManager.getInstance();
        for(int i = 0;i<gamesCount;i++){
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.ROBBERY,GameDifficulty.HARD,AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.CRIME_SCENE,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.IN_THE_DARKNESS,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.NIGHT_WATCH,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.KIDNAPPING,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.PURSUIT,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.RUNES,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.SHOOTING_RANGE,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.PROTOCOL,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.SAFE,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.ON_THE_TRAIL,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.MEMORY_GAME,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
            rankManager.addGameToRankData(user,createGame(user,GameDefinition.ATTENTION_GAME,GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR), false);
        }
        logOK();
    }

    private Game createGame(User user, GameDefinition gameDefinition, GameDifficulty gameDifficulty, AbstractTablexiaGame.GameResult gameResult) {
        Game game = new Game(gameIdCnt++, user, gameDifficulty.getDifficultyNumber(), gameDefinition.getGameNumber(), 0L, 1L, 2L, 0, null, 0, null, 1, TablexiaSettings.BuildType.DEBUG.getId(), TablexiaSettings.Platform.DESKTOP.getId(), "-- NONE --", 0, 1, UserRankManager.UserRank.getInitialRank().getId(), 8);
        game.setGameScoreMap(getGameScoreList(gameDefinition, gameDifficulty, gameResult, game.getGameScoreResolverVersion()));
        return game;
    }

    private List<GameScore> getGameScoreList(GameDefinition gameDefinition, GameDifficulty gameDifficulty, AbstractTablexiaGame.GameResult gameResult, Integer gameScoreResolverVersion) {
        return gameDefinition.getGameResultResolver(gameScoreResolverVersion).getExampleScoreForGameResult(gameDifficulty.getDifficultyDefinition(), gameResult.getGameResultDefinition());
    }
}
