/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.testing;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.async.AsyncExecutor;
import com.badlogic.gdx.utils.async.AsyncResult;
import com.badlogic.gdx.utils.async.AsyncTask;

import net.engio.mbassy.bus.error.IPublicationErrorHandler;
import net.engio.mbassy.bus.error.PublicationError;
import net.engio.mbassy.listener.Handler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicBoolean;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.event.MenuControlEvent;
import cz.nic.tablexia.debug.DebugActor;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.pursuit.helper.GameRulesHelper;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.main.MainMenuDefinition;
import cz.nic.tablexia.menu.user.UserMenu;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.gamemenu.OfficeMenuScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.button.TablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.components.SingleButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TwoButtonContentDialogComponent;

public abstract class AbstractTestScenario implements Runnable {

	private static final String     OUTPUT_DIRECTORY_NAME           = "iTest_results/";
    private static final FileHandle OUTPUT_DIRECTORY_FILE_HANDLE    = (Gdx.app.getType() == Application.ApplicationType.Android || Gdx.app.getType() == Application.ApplicationType.iOS) ? Gdx.files.external(OUTPUT_DIRECTORY_NAME) : Gdx.files.local(OUTPUT_DIRECTORY_NAME);
	private static final String     LOG_FILE_EXT   			        = ".log";
    private static final String     SCREENSHOT_EXTENSION            = ".png";

	public static final String      		NAME_FOR_TESTS      = "TestDetektiv";
	public static final int         		AGE_FOR_TESTS       = 12;
	public static final GenderDefinition 	GENDER_FOR_TESTS    = GenderDefinition.FEMALE;
	public static final String      		AVATAR_FOR_TESTS    =  "3";
	public static final String      		SIGNATURE_FOR_TESTS = "[[{class:cz.nic.tablexia.util.Point,x:0.7213922,y:0.889313},{class:cz.nic.tablexia.util.Point,x:0.7241445,y:0.8880705},{class:cz.nic.tablexia.util.Point,x:0.7296492,y:0.88558567},{class:cz.nic.tablexia.util.Point,x:0.7379061,y:0.8818583},{class:cz.nic.tablexia.util.Point,x:0.7489154,y:0.8768885},{class:cz.nic.tablexia.util.Point,x:0.7598737,y:0.87191874},{class:cz.nic.tablexia.util.Point,x:0.770781,y:0.86694896},{class:cz.nic.tablexia.util.Point,x:0.7816374,y:0.8619792},{class:cz.nic.tablexia.util.Point,x:0.79244274,y:0.8570094},{class:cz.nic.tablexia.util.Point,x:0.80569464,y:0.8545245},{class:cz.nic.tablexia.util.Point,x:0.821393,y:0.8545245},{class:cz.nic.tablexia.util.Point,x:0.8395379,y:0.8570094},{class:cz.nic.tablexia.util.Point,x:0.86012936,y:0.8619792},{class:cz.nic.tablexia.util.Point,x:0.87113863,y:0.86409134},{class:cz.nic.tablexia.util.Point,x:0.87256575,y:0.86334586},{class:cz.nic.tablexia.util.Point,x:0.8644107,y:0.85974276},{class:cz.nic.tablexia.util.Point,x:0.84667355,y:0.853282},{class:cz.nic.tablexia.util.Point,x:0.8303126,y:0.84620005},{class:cz.nic.tablexia.util.Point,x:0.81532776,y:0.83849686},{class:cz.nic.tablexia.util.Point,x:0.80171907,y:0.8301725},{class:cz.nic.tablexia.util.Point,x:0.7894866,y:0.82122695},{class:cz.nic.tablexia.util.Point,x:0.77654046,y:0.8126541},{class:cz.nic.tablexia.util.Point,x:0.76288086,y:0.8044539},{class:cz.nic.tablexia.util.Point,x:0.7485076,y:0.7966265},{class:cz.nic.tablexia.util.Point,x:0.73342085,y:0.78917176},{class:cz.nic.tablexia.util.Point,x:0.7183341,y:0.78159285},{class:cz.nic.tablexia.util.Point,x:0.7032473,y:0.77388966},{class:cz.nic.tablexia.util.Point,x:0.68816054,y:0.76606226},{class:cz.nic.tablexia.util.Point,x:0.67307377,y:0.7581107},{class:cz.nic.tablexia.util.Point,x:0.65803796,y:0.7502833},{class:cz.nic.tablexia.util.Point,x:0.6430532,y:0.7425801},{class:cz.nic.tablexia.util.Point,x:0.6281193,y:0.7350012},{class:cz.nic.tablexia.util.Point,x:0.61323637,y:0.7275465},{class:cz.nic.tablexia.util.Point,x:0.59830254,y:0.7199676},{class:cz.nic.tablexia.util.Point,x:0.5833177,y:0.7122644},{class:cz.nic.tablexia.util.Point,x:0.5682819,y:0.704437},{class:cz.nic.tablexia.util.Point,x:0.5531951,y:0.69648534},{class:cz.nic.tablexia.util.Point,x:0.5381593,y:0.68865794},{class:cz.nic.tablexia.util.Point,x:0.5231745,y:0.6809548},{class:cz.nic.tablexia.util.Point,x:0.50824064,y:0.67337584},{class:cz.nic.tablexia.util.Point,x:0.49335775,y:0.6659212},{class:cz.nic.tablexia.util.Point,x:0.4784239,y:0.6583423},{class:cz.nic.tablexia.util.Point,x:0.46343905,y:0.6506391},{class:cz.nic.tablexia.util.Point,x:0.44840324,y:0.6428117},{class:cz.nic.tablexia.util.Point,x:0.43331647,y:0.63486},{class:cz.nic.tablexia.util.Point,x:0.4182297,y:0.6270326},{class:cz.nic.tablexia.util.Point,x:0.40314293,y:0.6193294},{class:cz.nic.tablexia.util.Point,x:0.38805616,y:0.6117505},{class:cz.nic.tablexia.util.Point,x:0.3729694,y:0.6042959},{class:cz.nic.tablexia.util.Point,x:0.35171542,y:0.59696543},{class:cz.nic.tablexia.util.Point,x:0.32429418,y:0.5897593},{class:cz.nic.tablexia.util.Point,x:0.29070574,y:0.5826773},{class:cz.nic.tablexia.util.Point,x:0.25095007,y:0.57571954},{class:cz.nic.tablexia.util.Point,x:0.2273515,y:0.5702528},{class:cz.nic.tablexia.util.Point,x:0.21991006,y:0.56627697},{class:cz.nic.tablexia.util.Point,x:0.22862571,y:0.56379205},{class:cz.nic.tablexia.util.Point,x:0.2534985,y:0.5627981},{class:cz.nic.tablexia.util.Point,x:0.2683814,y:0.56018895},{class:cz.nic.tablexia.util.Point,x:0.2732744,y:0.55596465},{class:cz.nic.tablexia.util.Point,x:0.2681775,y:0.5501252},{class:cz.nic.tablexia.util.Point,x:0.25309074,y:0.54267055},{class:cz.nic.tablexia.util.Point,x:0.24427314,y:0.53484315},{class:cz.nic.tablexia.util.Point,x:0.2417247,y:0.52664304},{class:cz.nic.tablexia.util.Point,x:0.24544543,y:0.51807016},{class:cz.nic.tablexia.util.Point,x:0.25543532,y:0.50912446},{class:cz.nic.tablexia.util.Point,x:0.2654252,y:0.5001788},{class:cz.nic.tablexia.util.Point,x:0.2754151,y:0.49123323},{class:cz.nic.tablexia.util.Point,x:0.28540498,y:0.48228762},{class:cz.nic.tablexia.util.Point,x:0.29539487,y:0.473342},{class:cz.nic.tablexia.util.Point,x:0.3053338,y:0.4643964},{class:cz.nic.tablexia.util.Point,x:0.31522173,y:0.45545077},{class:cz.nic.tablexia.util.Point,x:0.32505873,y:0.44650516},{class:cz.nic.tablexia.util.Point,x:0.33484474,y:0.43755955},{class:cz.nic.tablexia.util.Point,x:0.3446817,y:0.42861396},{class:cz.nic.tablexia.util.Point,x:0.35456967,y:0.41966835},{class:cz.nic.tablexia.util.Point,x:0.36450857,y:0.41072273},{class:cz.nic.tablexia.util.Point,x:0.37449846,y:0.40177712},{class:cz.nic.tablexia.util.Point,x:0.38443738,y:0.39295575},{class:cz.nic.tablexia.util.Point,x:0.39432535,y:0.38425863},{class:cz.nic.tablexia.util.Point,x:0.40416232,y:0.37568575},{class:cz.nic.tablexia.util.Point,x:0.41394833,y:0.36723712},{class:cz.nic.tablexia.util.Point,x:0.4237853,y:0.35866424},{class:cz.nic.tablexia.util.Point,x:0.43367326,y:0.34996712},{class:cz.nic.tablexia.util.Point,x:0.4436122,y:0.34114575},{class:cz.nic.tablexia.util.Point,x:0.45360208,y:0.33220014},{class:cz.nic.tablexia.util.Point,x:0.46359196,y:0.32325453},{class:cz.nic.tablexia.util.Point,x:0.47358185,y:0.3143089},{class:cz.nic.tablexia.util.Point,x:0.48357174,y:0.30536333},{class:cz.nic.tablexia.util.Point,x:0.49356163,y:0.2964177},{class:cz.nic.tablexia.util.Point,x:0.5035005,y:0.2874721},{class:cz.nic.tablexia.util.Point,x:0.5133885,y:0.27852648},{class:cz.nic.tablexia.util.Point,x:0.5232255,y:0.26958087},{class:cz.nic.tablexia.util.Point,x:0.5330115,y:0.26063523},{class:cz.nic.tablexia.util.Point,x:0.54728276,y:0.24796227},{class:cz.nic.tablexia.util.Point,x:0.56603926,y:0.231562},{class:cz.nic.tablexia.util.Point,x:0.5892811,y:0.21143436},{class:cz.nic.tablexia.util.Point,x:0.6170081,y:0.18757936},{class:cz.nic.tablexia.util.Point,x:0.6333691,y:0.17353973},{class:cz.nic.tablexia.util.Point,x:0.638364,y:0.16931543},{class:cz.nic.tablexia.util.Point,x:0.63199294,y:0.17490645},{class:cz.nic.tablexia.util.Point,x:0.6142558,y:0.19031279},{class:cz.nic.tablexia.util.Point,x:0.60345036,y:0.19963114},{class:cz.nic.tablexia.util.Point,x:0.5995768,y:0.20286152},{class:cz.nic.tablexia.util.Point,x:0.6026349,y:0.20000389},{class:cz.nic.tablexia.util.Point,x:0.61262476,y:0.19105825},{class:cz.nic.tablexia.util.Point,x:0.61624354,y:0.1875794},{class:cz.nic.tablexia.util.Point,x:0.61349124,y:0.18956731},{class:cz.nic.tablexia.util.Point,x:0.60436785,y:0.19702199},{class:cz.nic.tablexia.util.Point,x:0.5888733,y:0.20994346},{class:cz.nic.tablexia.util.Point,x:0.5733788,y:0.2228649},{class:cz.nic.tablexia.util.Point,x:0.5578843,y:0.23578633},{class:cz.nic.tablexia.util.Point,x:0.54238975,y:0.24870777},{class:cz.nic.tablexia.util.Point,x:0.5268952,y:0.26162922},{class:cz.nic.tablexia.util.Point,x:0.5114007,y:0.27455065},{class:cz.nic.tablexia.util.Point,x:0.49590617,y:0.2874721},{class:cz.nic.tablexia.util.Point,x:0.48041168,y:0.30039352},{class:cz.nic.tablexia.util.Point,x:0.46491715,y:0.31331497},{class:cz.nic.tablexia.util.Point,x:0.4494736,y:0.3262364},{class:cz.nic.tablexia.util.Point,x:0.43408102,y:0.33915785},{class:cz.nic.tablexia.util.Point,x:0.4187394,y:0.35207927},{class:cz.nic.tablexia.util.Point,x:0.40344876,y:0.36500072},{class:cz.nic.tablexia.util.Point,x:0.38831103,y:0.37518877},{class:cz.nic.tablexia.util.Point,x:0.37332618,y:0.38264346},{class:cz.nic.tablexia.util.Point,x:0.35849425,y:0.38736475},{class:cz.nic.tablexia.util.Point,x:0.34381524,y:0.38935265},{class:cz.nic.tablexia.util.Point,x:0.33239824,y:0.3964346},{class:cz.nic.tablexia.util.Point,x:0.32424322,y:0.40861058},{class:cz.nic.tablexia.util.Point,x:0.3193502,y:0.42588058},{class:cz.nic.tablexia.util.Point,x:0.31771922,y:0.44824466},{class:cz.nic.tablexia.util.Point,x:0.31603724,y:0.47048447},{class:cz.nic.tablexia.util.Point,x:0.3143043,y:0.4926},{class:cz.nic.tablexia.util.Point,x:0.31252038,y:0.5145913},{class:cz.nic.tablexia.util.Point,x:0.31068552,y:0.53645825},{class:cz.nic.tablexia.util.Point,x:0.3089016,y:0.55844957},{class:cz.nic.tablexia.util.Point,x:0.30716866,y:0.5805651},{class:cz.nic.tablexia.util.Point,x:0.30548668,y:0.60280484},{class:cz.nic.tablexia.util.Point,x:0.3038557,y:0.625169},{class:cz.nic.tablexia.util.Point,x:0.3021737,y:0.647533},{class:cz.nic.tablexia.util.Point,x:0.3004408,y:0.669897},{class:cz.nic.tablexia.util.Point,x:0.29865688,y:0.69226104},{class:cz.nic.tablexia.util.Point,x:0.29682198,y:0.714625},{class:cz.nic.tablexia.util.Point,x:0.29503807,y:0.736989},{class:cz.nic.tablexia.util.Point,x:0.29330516,y:0.75935304},{class:cz.nic.tablexia.util.Point,x:0.29162318,y:0.78171706},{class:cz.nic.tablexia.util.Point,x:0.28999218,y:0.8040812},{class:cz.nic.tablexia.util.Point,x:0.2863224,y:0.8273149},{class:cz.nic.tablexia.util.Point,x:0.2806139,y:0.8514184},{class:cz.nic.tablexia.util.Point,x:0.27286664,y:0.87639153},{class:cz.nic.tablexia.util.Point,x:0.26308063,y:0.90223444},{class:cz.nic.tablexia.util.Point,x:0.25798374,y:0.91788924},{class:cz.nic.tablexia.util.Point,x:0.25757602,y:0.923356},{class:cz.nic.tablexia.util.Point,x:0.2618574,y:0.9186347},{class:cz.nic.tablexia.util.Point,x:0.2708279,y:0.9037253},{class:cz.nic.tablexia.util.Point,x:0.27974743,y:0.8889402},{class:cz.nic.tablexia.util.Point,x:0.288616,y:0.8742794},{class:cz.nic.tablexia.util.Point,x:0.2974336,y:0.85974276},{class:cz.nic.tablexia.util.Point,x:0.30620027,y:0.8453304},{class:cz.nic.tablexia.util.Point,x:0.3149669,y:0.8307938},{class:cz.nic.tablexia.util.Point,x:0.32373354,y:0.8161329},{class:cz.nic.tablexia.util.Point,x:0.33250016,y:0.8013478},{class:cz.nic.tablexia.util.Point,x:0.3412668,y:0.7864384},{class:cz.nic.tablexia.util.Point,x:0.3500844,y:0.7716533},{class:cz.nic.tablexia.util.Point,x:0.35895297,y:0.7569924},{class:cz.nic.tablexia.util.Point,x:0.3678725,y:0.7424558},{class:cz.nic.tablexia.util.Point,x:0.37684304,y:0.72804344},{class:cz.nic.tablexia.util.Point,x:0.38576257,y:0.7135068},{class:cz.nic.tablexia.util.Point,x:0.39463115,y:0.6988459},{class:cz.nic.tablexia.util.Point,x:0.40344876,y:0.6840608},{class:cz.nic.tablexia.util.Point,x:0.41221538,y:0.66915154},{class:cz.nic.tablexia.util.Point,x:0.421033,y:0.6542422},{class:cz.nic.tablexia.util.Point,x:0.42990157,y:0.63933283},{class:cz.nic.tablexia.util.Point,x:0.4388211,y:0.6244235},{class:cz.nic.tablexia.util.Point,x:0.44779164,y:0.60951406},{class:cz.nic.tablexia.util.Point,x:0.45671117,y:0.59472895},{class:cz.nic.tablexia.util.Point,x:0.46557975,y:0.5800681},{class:cz.nic.tablexia.util.Point,x:0.47439733,y:0.5655315},{class:cz.nic.tablexia.util.Point,x:0.48316398,y:0.5511191},{class:cz.nic.tablexia.util.Point,x:0.4919306,y:0.5365825},{class:cz.nic.tablexia.util.Point,x:0.50069726,y:0.52192163},{class:cz.nic.tablexia.util.Point,x:0.5094639,y:0.5071365},{class:cz.nic.tablexia.util.Point,x:0.5182305,y:0.49222717},{class:cz.nic.tablexia.util.Point,x:0.5270481,y:0.4774421},{class:cz.nic.tablexia.util.Point,x:0.5359167,y:0.46278122},{class:cz.nic.tablexia.util.Point,x:0.5448362,y:0.4482446},{class:cz.nic.tablexia.util.Point,x:0.5538068,y:0.43383223},{class:cz.nic.tablexia.util.Point,x:0.5627263,y:0.4192956},{class:cz.nic.tablexia.util.Point,x:0.5715949,y:0.40463474},{class:cz.nic.tablexia.util.Point,x:0.5804125,y:0.38984963},{class:cz.nic.tablexia.util.Point,x:0.5891791,y:0.37494028},{class:cz.nic.tablexia.util.Point,x:0.59794575,y:0.36015517},{class:cz.nic.tablexia.util.Point,x:0.6067124,y:0.34549433},{class:cz.nic.tablexia.util.Point,x:0.61547905,y:0.3309577},{class:cz.nic.tablexia.util.Point,x:0.62424564,y:0.3165453},{class:cz.nic.tablexia.util.Point,x:0.63326716,y:0.3015117},{class:cz.nic.tablexia.util.Point,x:0.6425435,y:0.2858569},{class:cz.nic.tablexia.util.Point,x:0.65207464,y:0.26958084},{class:cz.nic.tablexia.util.Point,x:0.66186064,y:0.2526836},{class:cz.nic.tablexia.util.Point,x:0.67164665,y:0.23591058},{class:cz.nic.tablexia.util.Point,x:0.68143266,y:0.21926181},{class:cz.nic.tablexia.util.Point,x:0.6912187,y:0.20273727},{class:cz.nic.tablexia.util.Point,x:0.7010047,y:0.186337},{class:cz.nic.tablexia.util.Point,x:0.71216685,y:0.16981246},{class:cz.nic.tablexia.util.Point,x:0.7247052,y:0.15316367},{class:cz.nic.tablexia.util.Point,x:0.7386197,y:0.13639064},{class:cz.nic.tablexia.util.Point,x:0.7539103,y:0.11949338},{class:cz.nic.tablexia.util.Point,x:0.7621673,y:0.11005078},{class:cz.nic.tablexia.util.Point,x:0.76339054,y:0.10806286},{class:cz.nic.tablexia.util.Point,x:0.7575801,y:0.11352962},{class:cz.nic.tablexia.util.Point,x:0.74473596,y:0.12645106},{class:cz.nic.tablexia.util.Point,x:0.7304137,y:0.1393725},{class:cz.nic.tablexia.util.Point,x:0.7146134,y:0.15229394},{class:cz.nic.tablexia.util.Point,x:0.69733495,y:0.16521537},{class:cz.nic.tablexia.util.Point,x:0.67857844,y:0.17813681},{class:cz.nic.tablexia.util.Point,x:0.66186064,y:0.1900643},{class:cz.nic.tablexia.util.Point,x:0.64718163,y:0.20099781},{class:cz.nic.tablexia.util.Point,x:0.63454133,y:0.21093738},{class:cz.nic.tablexia.util.Point,x:0.6239399,y:0.219883},{class:cz.nic.tablexia.util.Point,x:0.61333835,y:0.22882862},{class:cz.nic.tablexia.util.Point,x:0.60273683,y:0.23777421},{class:cz.nic.tablexia.util.Point,x:0.5921353,y:0.24671985},{class:cz.nic.tablexia.util.Point,x:0.5815338,y:0.25566548},{class:cz.nic.tablexia.util.Point,x:0.57093227,y:0.26448685},{class:cz.nic.tablexia.util.Point,x:0.56033075,y:0.27318397},{class:cz.nic.tablexia.util.Point,x:0.5497292,y:0.28175685},{class:cz.nic.tablexia.util.Point,x:0.53912777,y:0.29020548},{class:cz.nic.tablexia.util.Point,x:0.52852625,y:0.29877836},{class:cz.nic.tablexia.util.Point,x:0.5179247,y:0.30747548},{class:cz.nic.tablexia.util.Point,x:0.5073232,y:0.31629685},{class:cz.nic.tablexia.util.Point,x:0.49672168,y:0.32524246},{class:cz.nic.tablexia.util.Point,x:0.48617113,y:0.33406383},{class:cz.nic.tablexia.util.Point,x:0.47567156,y:0.34276092},{class:cz.nic.tablexia.util.Point,x:0.46522295,y:0.35133383},{class:cz.nic.tablexia.util.Point,x:0.4548253,y:0.35978246},{class:cz.nic.tablexia.util.Point,x:0.4443767,y:0.36835533},{class:cz.nic.tablexia.util.Point,x:0.43387714,y:0.37705246},{class:cz.nic.tablexia.util.Point,x:0.42332658,y:0.3858738},{class:cz.nic.tablexia.util.Point,x:0.4127251,y:0.3948194},{class:cz.nic.tablexia.util.Point,x:0.39982995,y:0.40761662},{class:cz.nic.tablexia.util.Point,x:0.38464126,y:0.42426538},{class:cz.nic.tablexia.util.Point,x:0.36715895,y:0.44476575},{class:cz.nic.tablexia.util.Point,x:0.34738305,y:0.4691177},{class:cz.nic.tablexia.util.Point,x:0.33484474,y:0.48340583},{class:cz.nic.tablexia.util.Point,x:0.32954398,y:0.48763013},{class:cz.nic.tablexia.util.Point,x:0.3314808,y:0.48179063},{class:cz.nic.tablexia.util.Point,x:0.34065518,y:0.46588734},{class:cz.nic.tablexia.util.Point,x:0.3448856,y:0.45619625},{class:cz.nic.tablexia.util.Point,x:0.34417203,y:0.4527174},{class:cz.nic.tablexia.util.Point,x:0.33851448,y:0.45545077},{class:cz.nic.tablexia.util.Point,x:0.32791296,y:0.46439648},{class:cz.nic.tablexia.util.Point,x:0.31899342,y:0.46576315},{class:cz.nic.tablexia.util.Point,x:0.31175587,y:0.45955092},{class:cz.nic.tablexia.util.Point,x:0.30620027,y:0.44575977},{class:cz.nic.tablexia.util.Point,x:0.30232662,y:0.42438963},{class:cz.nic.tablexia.util.Point,x:0.298402,y:0.4031438},{class:cz.nic.tablexia.util.Point,x:0.29442647,y:0.38202223},{class:cz.nic.tablexia.util.Point,x:0.2903999,y:0.3610249},{class:cz.nic.tablexia.util.Point,x:0.2863224,y:0.3401518},{class:cz.nic.tablexia.util.Point,x:0.2796455,y:0.3145574},{class:cz.nic.tablexia.util.Point,x:0.27036917,y:0.28424174},{class:cz.nic.tablexia.util.Point,x:0.25849345,y:0.24920475},{class:cz.nic.tablexia.util.Point,x:0.2440183,y:0.20944645},{class:cz.nic.tablexia.util.Point,x:0.23576134,y:0.1842248},{class:cz.nic.tablexia.util.Point,x:0.2337226,y:0.17353976},{class:cz.nic.tablexia.util.Point,x:0.23790205,y:0.17739135},{class:cz.nic.tablexia.util.Point,x:0.24829967,y:0.19577958},{class:cz.nic.tablexia.util.Point,x:0.26037928,y:0.21342231},{class:cz.nic.tablexia.util.Point,x:0.27414086,y:0.23031957},{class:cz.nic.tablexia.util.Point,x:0.28958443,y:0.24647138},{class:cz.nic.tablexia.util.Point,x:0.30670995,y:0.26187772},{class:cz.nic.tablexia.util.Point,x:0.32184768,y:0.2762901},{class:cz.nic.tablexia.util.Point,x:0.33499762,y:0.2897085},{class:cz.nic.tablexia.util.Point,x:0.34615982,y:0.30213296},{class:cz.nic.tablexia.util.Point,x:0.3553342,y:0.31356347},{class:cz.nic.tablexia.util.Point,x:0.36450857,y:0.32486972},{class:cz.nic.tablexia.util.Point,x:0.37368298,y:0.33605173},{class:cz.nic.tablexia.util.Point,x:0.38285735,y:0.3471095},{class:cz.nic.tablexia.util.Point,x:0.39203176,y:0.35804302},{class:cz.nic.tablexia.util.Point,x:0.4012062,y:0.36910078},{class:cz.nic.tablexia.util.Point,x:0.41038057,y:0.3802828},{class:cz.nic.tablexia.util.Point,x:0.41955495,y:0.39158905},{class:cz.nic.tablexia.util.Point,x:0.42872936,y:0.40301958},{class:cz.nic.tablexia.util.Point,x:0.43790376,y:0.41445008},{class:cz.nic.tablexia.util.Point,x:0.4470781,y:0.42588058},{class:cz.nic.tablexia.util.Point,x:0.45625255,y:0.43731108},{class:cz.nic.tablexia.util.Point,x:0.46542695,y:0.44874159},{class:cz.nic.tablexia.util.Point,x:0.47460136,y:0.46004784},{class:cz.nic.tablexia.util.Point,x:0.48377576,y:0.47122985},{class:cz.nic.tablexia.util.Point,x:0.49295017,y:0.48228762},{class:cz.nic.tablexia.util.Point,x:0.5021246,y:0.49322113},{class:cz.nic.tablexia.util.Point,x:0.5112991,y:0.5042789},{class:cz.nic.tablexia.util.Point,x:0.52047354,y:0.5154609},{class:cz.nic.tablexia.util.Point,x:0.529648,y:0.5267672},{class:cz.nic.tablexia.util.Point,x:0.5388225,y:0.5381977},{class:cz.nic.tablexia.util.Point,x:0.54799706,y:0.5496282},{class:cz.nic.tablexia.util.Point,x:0.5571717,y:0.5610587},{class:cz.nic.tablexia.util.Point,x:0.56634647,y:0.5724892},{class:cz.nic.tablexia.util.Point,x:0.5755214,y:0.58391976},{class:cz.nic.tablexia.util.Point,x:0.552343,y:0.6082717},{class:cz.nic.tablexia.util.Point,x:0.4968114,y:0.64554507},{class:cz.nic.tablexia.util.Point,x:0.4089265,y:0.6957399},{class:cz.nic.tablexia.util.Point,x:0.2886884,y:0.7588562},{class:cz.nic.tablexia.util.Point,x:0.19850977,y:0.80619335},{class:cz.nic.tablexia.util.Point,x:0.13839069,y:0.83775145},{class:cz.nic.tablexia.util.Point,x:0.10833116,y:0.8535305}]]";

	private static final float 	DEFAULT_ACTOR_POSITION_PADDING	= 10f;

	private static final float 	DEBUG_ACTOR_SQUARE_SIZE		  	= 50f;
	protected static final int 	DEFAULT_TASK_TIMEOUT		  	= 60;
	protected static final int 	TASK_DOWNLOAD_ASSETS_TIMEOUT	= 400;

	private static final String DATE_FORMAT_FOR_LOGFILE_NAME  	= "yyyyMMddHHmmSS";

	private static final int 	POINTER_SWIPE					= 0;
	private static final float 	TIME_SWIPE						= 1f;
	private static final int 	STEPS_SWIPE						= 30;

	private static final Object EVENT_LOCK						= new Object();
	private static final Object LAST_AWAITING_EVENT_LOCK		= new Object();
	private static final Object DIALOG_LOCK						= new Object();
	private static final Object LAST_AWAITING_DIALOG_LOCK		= new Object();
	private static final Object SCREEN_LOCK						= new Object();
	private static final Object SCREENSHOT_LOCK					= new Object();
	private static final Object END_APP_LOCK					= new Object();
	protected static final Object WAIT_LOCK						= new Object();

	private static final String TEST_STATUS_OK 					= "OK";
	private static final String TEST_STATUS_FAIL				= "FAIL";
	private static final String TEST_STATUS_PROCESSING			= "PROCESSING";

	public static final String SCENARIO_STEP_ACTOR_VISIBLE   	= "actor visible";
	public static final String SCENARIO_STEP_MMD_VISIBLE		= "main menu item visible";
	protected static 	String SCREENSHOT_ERROR            		= "";

	private Thread	testThread;
	private File    currentLogFile = null;

	/**
	 * Screenshot counter
	 */
	private static int screenNum = 1;

	private List<String> logMessages = new ArrayList<>();

	private ExecutionTimeout timeout;
	private boolean inDebugMode = false;
	private DebugActor debugActor;

	private Class<? extends AbstractTablexiaScreen> awaitingScreen = null;
	private Class<? extends AbstractTablexiaScreen> lastScreen = null;
	private static String awaitingEvent = null;
	private static String lastEvent = null;
	private String awaitingDialog = null;
	private String lastDialog = null;
	private AtomicBoolean waitingEvent = new AtomicBoolean(false);
	private AtomicBoolean waitingDialog = new AtomicBoolean(false);

	private final Tablexia tablexia;

	protected String 	status = TEST_STATUS_PROCESSING;
	private String 		nameOfTheTest = null;
	private String      currentTestName = null;

	private boolean startTest = false;


	private AsyncExecutor asyncExecutor = new AsyncExecutor(1);

	private AsyncResult<Void> task;

	public enum ActorPosition {
		MIDDLE,
		TOP_LEFT,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_RIGHT
	}

	private Actor findApplicationScopeActor(String actorKey){
		Actor actor = TablexiaApplication.getStage().getRoot().findActor(actorKey);
		return actor;
	}

	public Actor findActorByName(String actorKey) {
		if(actorKey==null){
			logError("Actor key is null!");
			stopTheTest();
		}
		Actor actor = TablexiaApplication.getActualScreen().getStage().getRoot().findActor(actorKey);
		if(actor==null){
			Log.info(getClass(), "Actor " + actorKey + " was not found in screen. Continue searching on application scope.");
			actor = findApplicationScopeActor(actorKey);
			if(actor==null)Log.info(getClass(), "Actor " + actorKey + " was not found.");
		}
		return actor;
	}

	public AbstractTestScenario(Tablexia tablexia) {
		this.tablexia = tablexia;
		setInDebugMode(true);
		ApplicationBus.getInstance().subscribe(this);
		TablexiaStorage.DATABASE_DIRECTORY.emptyDirectory();
		ApplicationBus.getInstance().addErrorHandler(new IPublicationErrorHandler() {
			@Override
			public void handleError(PublicationError error) {
				logError(error.getMessage());
			}
		});
	}

	@Override
	public final void run(){
		screenNum=1;
		if(!createLogFile()){
			Gdx.app.exit();
			putToSleep(END_APP_LOCK);
		}
		runTestScenario();
		finishTest();

	}

	private void runTestScenario() {
		startTest = true;
		onRunTestScenario();
	}

	protected abstract void onRunTestScenario();

	public void runTest() {
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread t, Throwable e) {

				logError("UncaughtException: " + e.getMessage());
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				writeToLogFile(errors.toString());
				stopTheTest();

			}
		});

		testThread = new Thread(this);
		testThread.start();
	}

	/**
	 * Creates a log file
	 * @return true if file was created
	 **/
	private boolean createLogFile() {
		if(currentLogFile == null) {
			GregorianCalendar calendar = new GregorianCalendar();
			nameOfTheTest = getClass().getSimpleName();
			FileHandle outputDirectory = prepareOutputDirectory();
			if (outputDirectory != null) {
				currentTestName = nameOfTheTest + "_" + new SimpleDateFormat(DATE_FORMAT_FOR_LOGFILE_NAME, Locale.US).format(calendar.getTime());
				currentLogFile = prepareLogFileForStatus(TEST_STATUS_PROCESSING);
				if (currentLogFile == null) {
					Log.err(getClass(), "Cannot create iTest log file!");
					return false;
				} else {
					writeToLogFile("Test: " + nameOfTheTest);					//adding name of the test to log file
					writeToLogFile("\t[" + status + "]\n");                   	//adding status of the test
				}
			} else {
				Log.err(getClass(), "Cannot create iTest log directory!");
				return false;
			}
		}
		return true;
	}

	private FileHandle prepareOutputDirectory() {
		FileHandle outputDirectory = OUTPUT_DIRECTORY_FILE_HANDLE;
		if (outputDirectory != null && (outputDirectory.exists() || outputDirectory.file().mkdir())) {
			return outputDirectory;
		}
		return null;
	}

	private void finishTest() {
		writeFinalLogFile();
		System.exit(0); //TODO Check on iOS test run
		putToSleep(END_APP_LOCK);
	}


	/**
	 * Click at Actor or Shape
	 */
	protected void clickAt(String infoMessage, final String componentKey) {
		logInfo(infoMessage);
		Actor actor = findActorByName(componentKey);
		if (actor != null) {
			if (!clickInTheActor(actor)) {
				logError("Couldn't click in the actor: " + componentKey + "!");
				stopTheTest();
			}
			logOK();
		} else {
			logError("Couldn't find actor with key: " + componentKey + "!");
			stopTheTest();
		}
	}

	protected void clickRepeat(int scenarioStepNum, String infoMessage, String actorKey, int count) {
		for(int i = 0; i < count; i++) {
			clickAt((scenarioStepNum+i) + ". " + infoMessage, actorKey);
			wait(200);
		}
	}

	/**
	 * Simulated click in the middle of given actor
	 * @param actor
	 * @return
	 */
	protected boolean clickInTheActor(final Actor actor) {
		return clickInTheActor(actor, ActorPosition.MIDDLE);
	}

	protected boolean clickInTheActor(final Actor actor, ActorPosition actorPosition) {

		Vector2[] click = new Vector2[1];

		CountDownLatch countDownLatch = new CountDownLatch(1);
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				click[0] = getActorPosition(actor, actorPosition);
				countDownLatch.countDown();
			}
		});

		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			logError("Cannot get actor position " + actor.getName());
			stopTheTest();
		}

		return clickAt((int) click[0].x, (int) click[0].y, actor.getStage());
	}

	/**
	 * Simulates click inside the middle of given rectangle (used for example when we have some area on screen which is part of bigger actor)
	 *
	 * @param rect
	 * @param stage
	 * @return
	 */
	protected boolean clickInTheRectangle(final Rectangle rect, final Stage stage) {
		if (rect == null) {
			logError("Rectangle is null");
			stopTheTest();
			return false;
		}

		if (stage == null) {
			logError("Stage is null");
			stopTheTest();
			return false;
		}

		final float x = rect.getX() + rect.getWidth()  / 2;
		final float y = rect.getY() + rect.getHeight() / 2;

		if (isInDebugMode()) {
			drawDebugSquare((int)x, (int)y, stage);
		}

		Vector2 click = stage.stageToScreenCoordinates(new Vector2(x, y));
		Log.debug("Testing", "Clicked at the middle of rectangle X: " + click.x + ", Y: " + click.y);

		return clickAt((int)x, (int)y, stage);
	}

	/**
	 * Simulates click on device screen. It expects coordinates related to stage (from actor for example)
	 *
	 * @param x
	 * @param y
	 * @param stage
	 * @return true when successful
	 */
	//TODO - Add support for different fingers (pointers) and buttons
	public boolean clickAt(final int x, final int y, final Stage stage) {
		if (stage == null) {
			logError("Stage is null");
			stopTheTest();
			return false;
		}

		if (isInDebugMode()) {
			drawDebugSquare(x, y, stage);
		}

		boolean triggeredDown = touchDown(x, y, 0, Input.Buttons.LEFT, stage);
		boolean triggeredUp = touchUp(x, y, 0, Input.Buttons.LEFT, stage);

		return triggeredDown && triggeredUp;
	}

	/**
	 * Fires touch down event
	 * @param touchDownX X Position in stages coord system
	 * @param touchDownY Y position in stages coord System
	 * @param pointer Pointer (Which finger...)
	 * @param button Button (Which button was pressed)
	 * @param stage Stage
	 * @return true when successful
	 */
	protected boolean touchDown(float touchDownX, float touchDownY, final int pointer, final int button, final Stage stage) {
		final boolean[] result = {false};
		final CountDownLatch latchDown = new CountDownLatch(1);

		Gdx.app.postRunnable(() -> {
			final Vector2 screenTouchDown = stage.stageToScreenCoordinates(new Vector2(touchDownX, touchDownY));
			result[0] = stage.touchDown((int)screenTouchDown.x, (int)screenTouchDown.y, pointer, button);
			latchDown.countDown();
		});

		try {
			latchDown.await();
		} catch (InterruptedException e) {
			logError("Cannot wait to touch down!" +  e.getMessage());
			stopTheTest();
		}

		if (isInDebugMode()) {
			drawDebugSquare((int)touchDownX, (int)touchDownY, stage);
		}

		return result[0];
	}

	/**
	 * Fires touch up event
	 * @param touchUpX X Position in stages coord system
	 * @param touchUpY Y position in stages coord System
	 * @param pointer Pointer (Which finger...)
	 * @param button Button (Which button was released)
	 * @param stage Stage
     * @return true when successful
     */
	protected boolean touchUp(float touchUpX, float touchUpY, final int pointer, final int button, final Stage stage) {
		final boolean[] result = {false};
		final CountDownLatch latchUp = new CountDownLatch(1);

		Gdx.app.postRunnable(()->{
			final Vector2 screenTouchUp = stage.stageToScreenCoordinates(new Vector2(touchUpX, touchUpY));
			result[0] = stage.touchUp((int) screenTouchUp.x, (int) screenTouchUp.y, pointer, button);
			latchUp.countDown();
		});

		try {
			latchUp.await();
		} catch (InterruptedException e) {
			logError("Cannot wait to touch down!" +  e.getMessage());
			stopTheTest();
		}
		return result[0];
	}

	/**
	 * Drags from start position to destination position over time in steps (linear interpolation is used for steps)
	 * Don't forget to call touchDown for the same pointer before dragging and touch Up after dragging (finish callback is great for calling touchUp method)
	 * @param startX Start Position X
	 * @param startY Start Position Y
	 * @param destX Destination Position X
	 * @param destY	Destination Position Y
	 * @param pointer Pointer (Which finger...)
	 * @param time Duration of drag
	 * @param steps Number of steps
	 * @param stage Stage
     */
	protected boolean dragOverTime(int startX, int startY, int destX, int destY, final int pointer, final float time, final int steps, final Stage stage) {
		return dragOverTime(startX, startY, destX, destY, pointer, 0, time, steps, stage);
	}

	/**
	 * Drags from start position to destination position over time in steps (linear interpolation is used for steps)
	 * Don't forget to call touchDown for the same pointer before dragging and touch Up after dragging (finish callback is great for calling touchUp method)
	 * @param startX Start Position X
	 * @param startY Start Position Y
	 * @param destX Destination Position X
	 * @param destY	Destination Position Y
	 * @param pointer Pointer (Which finger...)
	 * @param delay Delay before drag
	 * @param time Duration of drag
	 * @param steps Number of steps
     * @param stage Stage
     */
	protected boolean dragOverTime(int startX, int startY, int destX, int destY, final int pointer, final float delay, final float time, final int steps, final Stage stage) {
		final Vector2[] startPos = new Vector2[1];
		final Vector2[] destPos = new Vector2[1];

		CountDownLatch countDownLatchDrag = new CountDownLatch(1);

		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				startPos[0] = stage.stageToScreenCoordinates(new Vector2(startX, startY));
				destPos[0] = stage.stageToScreenCoordinates(new Vector2(destX, destY));
				countDownLatchDrag.countDown();
			}
		});

		try {
			countDownLatchDrag.await();
		} catch (InterruptedException e) {
			logError("Cannot wait to dragOverTime!" +  e.getMessage());
			stopTheTest();
		}

		final Vector2 singleStep = new Vector2((destPos[0].x - startPos[0].x) / steps, (destPos[0].y - startPos[0].y) / steps);

		final float interval = time / steps;

		boolean result = true;
		int i = 0;
		while(i <= steps) {
			long startTime = System.currentTimeMillis();
			long intervalMs = (long) (interval * 1000);
			result &= touchDragged((int)(startPos[0].x + i * singleStep.x), (int)(startPos[0].y + i * singleStep.y), pointer, stage);

			long actualWait = intervalMs - (System.currentTimeMillis() - startTime);
			if(actualWait > 0) wait((int) (interval*1000));
			i++;
		}

		return result;
	}

	private boolean touchDragged(final int dragX,final int dragY,final int pointer,final Stage stage) {
		final boolean[] result = new boolean[] {false};

		final CountDownLatch countDownLatch = new CountDownLatch(1);

		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				result[0] = stage.touchDragged(dragX, dragY, pointer);
				countDownLatch.countDown();
			}
		});

		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			logError(e.getMessage());
			stopTheTest();
		}

		return result[0];
	}

	/**
	 * Pause script execution for given time in milliseconds
	 * WARNING it is not working inside other threads
	 * like if you call Gdx.app.postRunnable(new Runnable()...or such
	 *
	 * @param timeInMilliseconds
	 */
	protected void pauseExecution(int timeInMilliseconds) {
		try {
			Thread.sleep(timeInMilliseconds);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * Please see getActorPosition(Actor actor, ActorPosition position, float padding) method...
	 * padding parameter is DEFAULT_ACTOR_POSITION_PADDING constant...
	 * @return Actors position
	 */
	protected Vector2 getActorPosition(final Actor actor, ActorPosition position) {
		return getActorPosition(actor, position, DEFAULT_ACTOR_POSITION_PADDING);
	}

	/**
	 * Returns actor position related to application / device screen
	 *
	 * @param actor
	 * @param position position inside actor (middle, left or right, top or bottom with padding)
	 * @param padding padding for the position (no effect when position parameter equals ActorPosition.MIDDLE)
	 * @return Actors position
	 */
	protected Vector2 getActorPosition(final Actor actor, ActorPosition position, float padding) {
		if (actor.getStage() == null) {
			logError("Actor '" + actor + "' isn't part of any stage.");
			stopTheTest();
			return null;
		}

		Vector2 clickPosition;

		switch (position) {
			case MIDDLE:
				clickPosition = actor.localToStageCoordinates(new Vector2(actor.getWidth() / 2, actor.getHeight() / 2));
				break;
			case BOTTOM_LEFT:
				clickPosition = actor.localToStageCoordinates(new Vector2(padding, padding));
				break;
			case TOP_RIGHT:
				clickPosition = actor.localToStageCoordinates(new Vector2(actor.getWidth() - padding, actor.getHeight() - padding));
				break;
			case BOTTOM_RIGHT:
				clickPosition = actor.localToStageCoordinates(new Vector2(actor.getWidth() - padding, padding));
				break;
			case TOP_LEFT:
				clickPosition = actor.localToStageCoordinates(new Vector2(padding, actor.getHeight() - padding));
				break;
			default:
				clickPosition = actor.localToStageCoordinates(new Vector2(actor.getWidth() / 2, actor.getHeight() / 2));
		}

		if (isInDebugMode()) {
			drawDebugSquare((int)clickPosition.x, (int)clickPosition.y, actor.getStage());
		}

		return clickPosition;
	}

	/**
	 * Return all buttons in the dialog. Order depends on a dialog itself, but stays the same across launches.
	 * @param dialog Dialog you want to get all the buttons from.
	 * @return Array of TablexiaButton in provided dialog. Null if dialog doesnt contain any buttons.
     */
	protected TablexiaButton[] getDialogButtons(TablexiaComponentDialog dialog) {
		ArrayList<TablexiaButton> buttons = new ArrayList<TablexiaButton>();
		TablexiaButton[] resultArray;

		for(TablexiaDialogComponentAdapter component : dialog.getDialogComponents()) {
			if (component instanceof TwoButtonContentDialogComponent) {
				TwoButtonContentDialogComponent twoButtonComponent = (TwoButtonContentDialogComponent) component;
				buttons.add(twoButtonComponent.getFirstButton().getButton());
				buttons.add(twoButtonComponent.getSecondButton().getButton());
			}
			else if(component instanceof SingleButtonContentDialogComponent) {
				SingleButtonContentDialogComponent singleButtonComponent = (SingleButtonContentDialogComponent) component;
				buttons.add(singleButtonComponent.getButton());
			}
		}

		if(buttons.size() == 0) {
			logError("Couldn't find any buttons in dialog: " + dialog.getName());
			stopTheTest();
			return null;
		}

		resultArray = new TablexiaButton[buttons.size()];
		buttons.toArray(resultArray);
		return resultArray;
	}

	protected boolean clickDialogButton(String infoMessage, String dialogKey, int buttonIndex) {
		logInfo(infoMessage);
		TablexiaComponentDialog dialog = getDialog(dialogKey);
		if(dialog == null){
			logError("Couldn't find text dialog with key: " + dialogKey);
			stopTheTest();
			return false;
		}

		TablexiaButton[] buttons = getDialogButtons(dialog);
		if(buttons == null){
			logError("Cannot find any dialog button!");
			stopTheTest();
			return false;
		}

		if(buttonIndex < 0 || buttonIndex >= buttons.length) {
			logError("There is not button with index: " + buttonIndex);
			stopTheTest();
			return false;
		}

		if(clickInTheActor(buttons[buttonIndex])){
			logOK();
			return true;
		}
		return false;
	}

	protected void takeScreenShot() {
		takeScreenShot(null);
	}

	/**
	 * Take screenshot from application
	 */
	protected void takeScreenShot(final Runnable finishCallback) {
		getTablexia().takeScreenShot(new Tablexia.ScreenshotListener() {
			@Override
			public void screenshotTaken(final Pixmap screen) {
				task = asyncExecutor.submit(new TakeScreenShotAsyncTask(screen, finishCallback));
            }
        });
	}

	/**
	 * Same as takeScreenshot method but this one waits
	 * for the screenshot to get captured and saved
	 */
	protected void takeScreenShotBlocking() {
		//Needs to be object
		final boolean[] result = {false};

		takeScreenShot(new Runnable() {
			@Override
			public void run() {
				result[0] = true;
				wakeUp(SCREENSHOT_LOCK);
			}
		});

		//Don't sleep if not needed!
		if(!result[0]) putToSleep(SCREENSHOT_LOCK);
	}

	/**
	 * Starts waiting for some task to be finished (screen transition, actor animation....)
	 * Error will be triggered if timeout is not cleared
	 *
	 * @param timeInSeconds
	 * @param name
	 * @param <T>
	 */
	protected <T> void setTimeout(long timeInSeconds, T name) {
		this.timeout = new ExecutionTimeout<T>(timeInSeconds, name);
	}

	/**
	 * Clears currently running timeout so error is not triggered
	 */
	protected void clearTimeout() {
		if (timeout != null) {
			this.timeout.cancelWaiting();
			this.timeout = null;
		}
	}

	protected void writeToLogFile(String message){
		logMessages.add(message);
		if(currentLogFile == null) {
			Log.err(getClass(), "Log file was not created! Message \"" + message + "\" could not be written to log file");
			Gdx.app.exit();
			putToSleep(END_APP_LOCK);
		}
		try {
			FileWriter fw = new FileWriter(currentLogFile, true);
			fw.write(message);
			fw.close();
		} catch(IOException e) {
			Log.err(getClass(), "Cannot write to log file!", e);
		}
	}

	/**
	 * Adding information about which scenario step is running to log file
	 * @param info
     */
	protected void logInfo(String info) {
		writeToLogFile(info + "\t");
	}

	/**
	 * Confirms, that no problem appears during the scenario step; writing OK to log file
	 */
	protected void logOK() {
		writeToLogFile("[OK]\n");
	}

	/**
	 * Problem appears during the scenario step; writing FAIL to log file; taking screenshot
	 */
	protected void logFail(){
		SCREENSHOT_ERROR = "_error";
		takeScreenShotBlocking();
		writeToLogFile("[FAIL]\n");
		writeToLogFile("\tScreenshot screen_" + (screenNum-1) + "_" + currentTestName + SCREENSHOT_ERROR + SCREENSHOT_EXTENSION + " has been taken.\n");
		SCREENSHOT_ERROR = "";
	}

	/**
	 * Log testing error with information about where in parent class was logged
	 *
	 * @param error
	 */
	protected void logError(String error) {
		status = TEST_STATUS_FAIL;
		logFail();
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();

		StringBuilder sb = new StringBuilder();
		sb.append(error + "\n");

		//Starting from 2, skipping Thread class and this method call...
		for(int i = 2; i < stackTrace.length; i++) {
			sb.append("	" + stackTrace[i].getClassName() + ":" + stackTrace[i].getLineNumber() + " (" + stackTrace[i].getMethodName() + ")\n");
		}
		sb.append("\n");

		writeToLogFile(sb.toString());
		Log.err(getClass(), sb.toString());
	}

	/**
	 * Draw red debug square on scene with center on x and y location and size of DEBUG_ACTOR_SQUARE_SIZE constant
	 *
	 * @param x
	 * @param y
	 * @param stage
	 */
	private void drawDebugSquare(final int x, final int y, final Stage stage) {
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				getDebugActor().setX(x - DEBUG_ACTOR_SQUARE_SIZE/2f);
				getDebugActor().setY(y - DEBUG_ACTOR_SQUARE_SIZE/2f);
				getDebugActor().setWidth(DEBUG_ACTOR_SQUARE_SIZE);
				getDebugActor().setHeight(DEBUG_ACTOR_SQUARE_SIZE);

				getDebugActor().setTouchable(Touchable.disabled);

				stage.addActor(getDebugActor());
			}
		});
	}

	/**
	 * Returns if test is in debug mode. Red square around clicked locations is drawn when it is on
	 *
	 * @return
	 */
	protected boolean isInDebugMode() {
		return inDebugMode;
	}

	/**
	 * Set debug mode
	 *
	 * @param inDebugMode
	 */
	protected void setInDebugMode(boolean inDebugMode) {
		this.inDebugMode = inDebugMode;
	}

	/**
	 * Return instance of debug actor (red square). We are using only one actor and move him
	 * around scene with each click
	 *
	 * @return
	 */
	private DebugActor getDebugActor() {
		if (debugActor == null) {
			debugActor = new DebugActor();
		}

		return debugActor;
	}

	protected Tablexia getTablexia() {
		return tablexia;
	}

	/**
	 * Helper class for setting timeout of waiting on some task (screen loaded, actor animation finished.....)
	 *
	 * @param <T> object that we receive inside timeout event
	 */
	protected class ExecutionTimeout<T> {

		private Timer timer;

		public ExecutionTimeout(long timeoutInSec, final T task) {
			timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					// we were waiting for some task / event to happen which should cancel this timer.
					// When this not occur we send event about timeout of waiting
					ApplicationBus.getInstance().publishAsync(new TaskWaitingTimeout<T>(task));
				}
			}, timeoutInSec*1000);
		}

		public void cancelWaiting() {
			timer.cancel();
		}
	}

	/**
	 * Event fired when task timeout is reached
	 *
	 * @param <T> object to help identify which task timed out
	 */
	public class TaskWaitingTimeout<T> implements ApplicationBus.ApplicationEvent {
		private T task;

		private TaskWaitingTimeout(T task) {
			this.task = task;
		}

		public T getTask() {
			return task;
		}
	}


	protected void putToSleep(Object lock) {
		synchronized (lock) {
			try {
				lock.wait();
			} catch (InterruptedException e) {
				logError("Cannot wait anymore! InterruptedException: " + e.getMessage());
			}
		}
	}

	protected void wakeUp(Object lock) {
		synchronized (lock) {
			lock.notify();
		}
	}

	protected void wait(int timeMs) {
		synchronized (WAIT_LOCK) {
			try {
				WAIT_LOCK.wait(timeMs);
			} catch (InterruptedException e) {
				logError("InterruptedException: " + e.getMessage());
			}
		}
	}

	@Handler
	private void handleTaskWaitingTimeout(TaskWaitingTimeout waitingTimeout){
		logError("Task waiting for " + waitingTimeout.getTask() + " timed out!");
		stopTheTest();
	}

	@Handler
	private void onScreenVisibleEvent(AbstractTablexiaScreen.ScreenVisibleEvent screenVisibleEvent) {
		if(awaitingScreen != null && awaitingScreen.equals(screenVisibleEvent.getActualScreenClass())) {
			clearTimeout();
			wakeUp(SCREEN_LOCK);
		}
		lastScreen=screenVisibleEvent.getActualScreenClass();
	}

	protected void waitForScreen(String infoMessage, Class<? extends AbstractTablexiaScreen> screen) {
		logInfo(infoMessage);
		if(lastScreen==null || !lastScreen.equals(screen)) {
			awaitingScreen = screen;
			setTimeout(DEFAULT_TASK_TIMEOUT, screen);
			putToSleep(SCREEN_LOCK);
		}
		logOK();
		awaitingScreen = null;
		lastScreen=null;
	}

	@Handler
	public void handleScenarioStepEvent(final AbstractTablexiaScreen.ScenarioStepEvent<String> step){
		synchronized (LAST_AWAITING_EVENT_LOCK) {
			String handledEvent = step.getStep().toString();
			if (step.getStep() != null) {
				if (awaitingEvent != null) {
					if (awaitingEvent.equals(handledEvent)) {
						clearTimeout();
						waitingEvent.set(false);
						wakeUp(EVENT_LOCK);
						return;
					} else lastEvent = handledEvent;
				}
				//lastEvent will be always set to last event (empty if this is first caught event)
				else lastEvent = handledEvent;
			}
		}
	}

	protected void waitForEvent(String infoMessage, String eventName) {
		waitForEvent(infoMessage, eventName, DEFAULT_TASK_TIMEOUT);
	}

	protected void waitForEvent(String infoMessage, String eventName, int timeout) {
		logInfo(infoMessage);
		synchronized (LAST_AWAITING_EVENT_LOCK) {
			if (lastEvent == null || !lastEvent.equals(eventName)) {  //event has been already caught
				awaitingEvent = eventName;
				setTimeout(timeout, eventName);
				waitingEvent.set(true);
			}
		}
		if(waitingEvent.get()) putToSleep(EVENT_LOCK);
		logOK();
		awaitingEvent=null;
		lastEvent=null;
	}

	@Handler
	public void handleShownDialog(final TablexiaComponentDialog.DialogVisibleEvent dialog) {
		synchronized (LAST_AWAITING_DIALOG_LOCK) {
			String dialogName = dialog.getDialogName();
			if (Tablexia.LANGUAGE_REQUEST.equals(dialogName)) {
				if (clickDialogButton("\n\tClick \"Česky\" in language dialog", Tablexia.LANGUAGE_REQUEST, 0)) {
					writeToLogFile("\tSelected language: czech\t");
				}
			} else if (dialogName != null) {
				if (dialogName.equals(awaitingDialog)) {
					clearTimeout();
					waitingDialog.set(false);
					wakeUp(DIALOG_LOCK);
				}
				//lastDialog will be always set to last shown dialog name (null if this is first caught dialog name)
				lastDialog = dialogName;
			}
		}
	}

	protected void waitForDialog(String infoMessage, String dialogName){
		logInfo(infoMessage);
		synchronized (LAST_AWAITING_DIALOG_LOCK) {
			if (lastDialog == null || !lastDialog.equals(dialogName)) {
				awaitingDialog = dialogName;
				setTimeout(DEFAULT_TASK_TIMEOUT, dialogName);
				waitingDialog.set(true);
			}
		}
		if(waitingDialog.get()) putToSleep(DIALOG_LOCK);
		logOK();
		awaitingDialog=null;
		lastDialog=null;
	}

	protected TablexiaComponentDialog getDialog(final String dialogKey) {
		Actor dialog = findActorByName(dialogKey);

		if(dialog == null || !(dialog instanceof TablexiaComponentDialog)) {
			logError("Couldn't find dialog with key: " + dialogKey + "!");
			stopTheTest();
			return null;
		}

		return (TablexiaComponentDialog) dialog;
	}

	protected boolean typeToTextField(String infoMessage, String text, String textFieldKey) {
		logInfo(infoMessage);
		TextField textField = (TextField) findActorByName(textFieldKey);
		if(textField == null) {
			logError("Couldn't find text field with key: " + textFieldKey);
			stopTheTest();
			return false;
		}

		getTablexia().getStage().setKeyboardFocus(textField);
		wait(200);

		boolean result = typeText(text);

		wait(200);
		getTablexia().getStage().setKeyboardFocus(null);
		textField.getOnscreenKeyboard().show(false);

		return result;
	}

	protected boolean typeText(String text) {
		boolean keyTyped = true;

		for(char letter : text.toCharArray()) {
			keyTyped &= getTablexia().getStage().keyTyped(letter);
			pauseExecution(150);
		}

		if (!keyTyped) {
			logError("Failed to type text.");
			stopTheTest();
		}
		logOK();

		return keyTyped;
	}


	protected boolean isVisibleInMenu(Actor actor){
		wait(200);
		Vector2 clickBottom = getActorPosition(actor, ActorPosition.BOTTOM_LEFT);

		if(		(int)(clickBottom.y) <= (AbstractMenu.SELECTBOX_LOCALE_HEIGHT + 2*AbstractMenu.MAINMENU_PADDING) ||
				(int)(TablexiaSettings.getViewportHeight(actor.getStage()) - AbstractMenu.SELECTBOX_LOCALE_HEIGHT) < (int)(clickBottom.y+ actor.getHeight())){
			return false;
		}
		return true;
	}

	protected boolean clickMainMenuButton(String infoMessage, MainMenuDefinition mmd) {
		return clickMenuButton(infoMessage,mmd.getTitle());
	}

	protected boolean clickMenuButton(String infoMessage , String buttonName){
		logInfo(infoMessage);
		Actor actor = findActorByName(buttonName);
		if(actor==null){
			logError("Could not find actor " + buttonName + "!");
			stopTheTest();
		}


		setTimeout(DEFAULT_TASK_TIMEOUT, SCENARIO_STEP_MMD_VISIBLE);

		while(!isVisibleInMenu(actor)){
			swipeToMainMenu(buttonName);
		}
		clearTimeout();

		if(!clickInTheActor(actor, ActorPosition.MIDDLE)){
			logError("Couldn't click in the actor: " + buttonName + "!");
			stopTheTest();
			return false;
		}
		logOK();
		return true;
	}

	private int isActorVisible(Actor actor){
		final Vector2 actorClick = getActorPosition(actor, ActorPosition.MIDDLE);
		if((int)(actorClick.x) <= 0) return -1;
		if((int)TablexiaSettings.getViewportWidth(actor.getStage()) <= (int)(actorClick.x)) return 1;
		return 0;
	}

	protected void swipeToActor(String infoMessage, String actorName) {
		logInfo(infoMessage);
		Actor actor = findActorByName(actorName);
		if (actor == null) {
			logError("Could not find actor " + actorName + "!");
			stopTheTest();
		}
		setTimeout(DEFAULT_TASK_TIMEOUT, SCENARIO_STEP_ACTOR_VISIBLE);
		int move = isActorVisible(actor);
		while(move!=0) {
            final Stage stage = actor.getStage();
            if (stage == null) {
                logError("Stage is null. Cannot scroll to actor " + actorName + "!");
                stopTheTest();
            }
			final int touchDownX = (int) (stage.getWidth() / 2);
			final int touchUpX = move>0 ? 0 : (int)TablexiaSettings.getViewportWidth(actor.getStage());

			touchDown(touchDownX, stage.getHeight() / 2, 0, Input.Buttons.LEFT, stage);
			dragOverTime(
					touchDownX,
					(int) (stage.getHeight() / 2f),
					touchUpX,
					(int) (stage.getHeight() / 2),
					0,
					0.5f,
					30,
					stage
			);
			touchUp(touchUpX,(int) (stage.getHeight() / 2),0,Input.Buttons.LEFT,stage);

			move = isActorVisible(actor);
		}
		AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_STEP_ACTOR_VISIBLE);
		clearTimeout();
		logOK();
	}


	protected boolean swipeToMainMenu(String actorName){
		final Stage stage = Tablexia.getStage();
		final Vector2 itemPosition = getActorPosition(findActorByName(actorName), ActorPosition.MIDDLE);
		if(stage==null){
			logError("Stage is null. Cannot scroll to MainMenuDefinition!");
			stopTheTest();
			return false;
		}
		final int destY = (itemPosition.y < (stage.getHeight()/2)) ? (int)(stage.getHeight()/2 + (stage.getHeight()/2 - itemPosition.y)): (int)(stage.getHeight()/2 - (itemPosition.y - stage.getHeight()/2));

		boolean result= touchDown(itemPosition.x, (int)stage.getHeight()/2, 0, Input.Buttons.LEFT, stage);

		result &= dragOverTime(
				(int) itemPosition.x,
				(int)stage.getHeight()/2,
				(int) itemPosition.x,
				destY,
				POINTER_SWIPE,
				TIME_SWIPE,
				STEPS_SWIPE,
				stage
		);

		result &= touchUp((int) itemPosition.x, destY, 1, Input.Buttons.LEFT, stage);
		return result;
	}

    private void writeFinalLogFile() {
		try {
			// If status is in TEST_STATUS_PROCESSING state, no error appeared during the test
			String finalTestStatus = status.equals(TEST_STATUS_PROCESSING) ? TEST_STATUS_OK : TEST_STATUS_FAIL;
			FileWriter fw = new FileWriter(prepareLogFileForStatus(finalTestStatus), false);
			logMessages.set(1, "\t[" + finalTestStatus + "]\n");
			for(String message : logMessages) {
				fw.write(message);
			}
			fw.close();
			if (currentLogFile == null || !currentLogFile.delete()) {
				Log.err(getClass(), "Cannot delete temporary log file!");
			}
		} catch(IOException e) {
			Log.err(getClass(), "Cannot rewrite log file!", e);
		}
		logMessages.clear();
		currentLogFile = null;
	}

	private File prepareLogFileForStatus(String status) {
		FileHandle outputDirectory = prepareOutputDirectory();
		if (outputDirectory == null) {
			return null;
		}
		return outputDirectory.child("/" + status + "_" + currentTestName + LOG_FILE_EXT).file();
	}

	protected void stopTheTest(){
        logInfo("An error occurred. Test has been stopped.");
        finishTest();
    }

	protected User createUser(){
		return createUser(NAME_FOR_TESTS, AGE_FOR_TESTS , GENDER_FOR_TESTS, AVATAR_FOR_TESTS, SIGNATURE_FOR_TESTS);
	}

	protected User createUser(String name, int age, GenderDefinition gender, String avatar, String signature) {
		writeToLogFile("Creating user in database:\n");
		waitForEvent("\tcreateUser: Wait for loading data", AbstractMenu.SCENARIO_STEP_OPENED_MENU, TASK_DOWNLOAD_ASSETS_TIMEOUT);
		User user = UserDAO.createUser(name, age , gender, avatar, signature);
		writeToLogFile("\tcreateUser: User has been successfully created.\n");

		return user;
	}

	protected void logIn(User user){
		writeToLogFile("Log in as user " + user.getName() + ":\n");
		UserRankManager.getInstance().onNewUser(user);
		TablexiaSettings.getInstance().changeUser(user);
		ApplicationBus.getInstance().post(new MenuControlEvent(UserMenu.class, AbstractMenu.MenuAction.HIDE, true)).asynchronously();
		ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(TablexiaSettings.INITIAL_SCREEN, TablexiaApplication.ScreenTransaction.FADE)).asynchronously();
		waitForScreen("\tlogIn: Wait for office page visible", OfficeMenuScreen.class);
		waitForEvent("\tlogIn: Wait for show help event", OfficeMenuScreen.EVENT_HELP_SHOWN);
		//clickAt("\tlogIn: Click at toggle help overlay button", OfficeMenuScreen.TOGGLE_HELP_OVERLAY_BUTTON_NAME);
		clickMiddleInScreen();
		waitForEvent("\tlogIn: Wait for hidden help event",OfficeMenuScreen.EVENT_HELP_HIDDEN);
	}

	private void clickMiddleInScreen(){
		AbstractTablexiaScreen screen = getTablexia().getActualScreen();

		int middleX = (int) (screen.getViewportWidth()/2);
		int middleY = (int) (screen.getSceneInnerHeight()/2);
		Log.info(getClass(),"CL " + middleX + " - " + middleY);
		boolean result = touchDown(middleX,middleY,0,Input.Buttons.LEFT,screen.getStage());
		Log.info(getClass(),"RESULT: " + result);
		result &= touchUp(middleX,middleY,0,Input.Buttons.LEFT,screen.getStage());
		Log.info(getClass(),"RESULT: " + result);
	}

	protected void setGameScore(User user, GameDifficulty gameDifficulty, GameDefinition gameDefinition, AbstractTablexiaGame.GameResult gameResult){
		writeToLogFile("\tGame score of game " + gameDefinition.toString() + " has been set to result: difficulty " + gameDifficulty.toString() + ", " + gameResult.getStarCount() + " stars" + "\n");
		Game game = GameDAO.createGame(user, gameDifficulty, gameDefinition, new TablexiaRandom(), UserRankManager.UserRank.getInitialRank());
		if(gameDefinition.equals(GameDefinition.PURSUIT)){
			long duration = GameRulesHelper.getDurationForStars(gameDifficulty, gameResult.getStarCount());
			GameDAO.startGame(game, TimeUtils.millis() - duration);
		}
		else GameDAO.startGame(game);
		GameDAO.endGame(game);
		List<GameScore> gameScoreList = gameDefinition.getGameResultResolver(game.getGameScoreResolverVersion()).getExampleScoreForGameResult(gameDifficulty.getDifficultyDefinition(), gameResult.getGameResultDefinition());
		GameDAO.setGameScores(game, gameScoreList);
	}

	protected void setGameScores(User user, GameDifficulty gameDifficulty, AbstractTablexiaGame.GameResult gameResult, int times) {
		for(int i=0; i< times; i++){
			for (GameDefinition gameDefinition : GameDefinition.values()) {
				writeToLogFile("\tGame score of game " + gameDefinition.toString() + " has been set to result: difficulty " + gameDifficulty.toString() + ", " + gameResult.getStarCount() + " stars" + "\n");
				Game game = GameDAO.createGame(user, gameDifficulty, gameDefinition, new TablexiaRandom(), UserRankManager.UserRank.getInitialRank());
				if(gameDefinition.equals(GameDefinition.PURSUIT)){
					long duration = GameRulesHelper.getDurationForStars(gameDifficulty, gameResult.getStarCount());
					GameDAO.startGame(game, TimeUtils.millis() - duration);
				}
				else GameDAO.startGame(game);
				GameDAO.endGame(game);
				List<GameScore> gameScoreList = gameDefinition.getGameResultResolver(game.getGameScoreResolverVersion()).getExampleScoreForGameResult(gameDifficulty.getDifficultyDefinition(), gameResult.getGameResultDefinition());
				GameDAO.setGameScores(game, gameScoreList);
			}
		}
	}


	private class TakeScreenShotAsyncTask implements AsyncTask<Void> {

		private Pixmap screen;
		private Runnable finishCallback;

		public TakeScreenShotAsyncTask(final Pixmap screen, final Runnable finishCallback) {
			this.screen = screen;
			this.finishCallback = finishCallback;

		}

		@Override
		public Void call() throws Exception {
			Log.info(getClass(), "Screenshot task called");

			FileHandle outputDirectory = prepareOutputDirectory();
			if (outputDirectory == null) {
				Log.err(getClass(), "Cannot create output directory for screenshots!");
				stopTheTest();
				return null;
			}

			saveScreenshot(outputDirectory.child("/screen_" + screenNum++ + "_" + currentTestName + SCREENSHOT_ERROR +  SCREENSHOT_EXTENSION), screen, true);
			screen.dispose();

			if(finishCallback!=null){
				finishCallback.run();
			}

			return null;
		}
	}

	private static void saveScreenshot(FileHandle file, Pixmap pixmap, boolean flipY) {
		try {
			PixmapIO.PNG writer = new PixmapIO.PNG((int)(pixmap.getWidth() * pixmap.getHeight() * 1.5f)); // Guess at deflated size.
			try {
				writer.setFlipY(flipY);
				writer.write(file, pixmap);
			} finally {
				writer.dispose();
			}
		} catch (IOException ex) {
			throw new GdxRuntimeException("Error writing PNG: " + file, ex);
		}
	}

	protected void waitForActorOnStage(String actorName) {
		setTimeout(DEFAULT_TASK_TIMEOUT, actorName);
		Actor scoreTable = findActorByName(actorName);
		while(scoreTable == null){
			scoreTable = findActorByName(actorName);
		}
		clearTimeout();
	}
}