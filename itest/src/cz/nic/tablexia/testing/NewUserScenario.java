/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.testing;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.user.UserAvatarDefinition;
import cz.nic.tablexia.menu.user.UserMenu;
import cz.nic.tablexia.screen.createuser.FormScreen;
import cz.nic.tablexia.screen.createuser.PanoramaScreen;
import cz.nic.tablexia.screen.createuser.form.FormActorsLayout;
import cz.nic.tablexia.screen.gamemenu.OfficeMenuScreen;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;

/**
 * Created by drahomir on 6/6/16.
 */
public class NewUserScenario extends AbstractTestScenario {
    public NewUserScenario(Tablexia tablexia) {
        super(tablexia);
    }

    public void onRunTestScenario() {
        waitForEvent( "1. Wait for loading data", AbstractMenu.SCENARIO_STEP_OPENED_MENU, TASK_DOWNLOAD_ASSETS_TIMEOUT);

        clickAt("2. Click new user button", UserMenu.NEW_USER_BUTTON);

        waitForEvent("3. Wait for event newspapers screen visible", PanoramaScreen.SCENARIO_STEP_NEWSPAPERS_SCENE_VISIBLE);

        clickAt("4. Click at newspapers", PanoramaScreen.NEWSPAPERS);

        waitForDialog("5. Wait for newspapers dialog", PanoramaScreen.NEWSPAPERS_CONTINUE_DIALOG);

        clickAt("6. Click at newspapers continue dialog", PanoramaScreen.NEWSPAPERS_CONTINUE_DIALOG);
        waitForEvent("7. Wait for event street screen visible", PanoramaScreen.SCENARIO_STEP_STREET_SCENE_VISIBLE);

        waitForActorOnStage(PanoramaScreen.STREET_DOOR);
        swipeToActor("8. Swipe through street to actor " + PanoramaScreen.STREET_DOOR, PanoramaScreen.STREET_DOOR);

        clickRepeat(9, "Click detective office's door", PanoramaScreen.STREET_DOOR, 3);
        clickDetectiveDialogs(12);

        waitForEvent("20. Wait for event form scene visible",FormScreen.SCENARIO_STEP_FORM_SCENE_VISIBLE);

        //Choosing avatar
        clickAt("21. Click at avatar image", FormScreen.FORM_AVATAR_IMAGE);
        waitForEvent("22. Wait for event mugshot images visible", FormScreen.SCENARIO_STEP_MUGSHOTS_VISIBLE);

        clickAt("23. Click at avatar" + String.valueOf(UserAvatarDefinition.AVATAR_6.number()), String.valueOf(UserAvatarDefinition.AVATAR_6.number()));
        waitForEvent("24. Wait for event image chosen", FormScreen.SCENARIO_STEP_IMAGE_CHOSEN);

        //Text field
        clickAt("25. Click at text field", FormScreen.FIELD_FORM_NAME);
        if(TablexiaSettings.getInstance().isRunningOnMobileDevice()){
            clickAt("26. a) Click at alternative text field", FormScreen.ALTERNATIVE_TEXT_FIELD);
            waitForEvent("26. b) Wait for event alternative text field shown", FormScreen.ALTERNATIVE_TEXT_FIELD_SHOWN);
            typeToTextField("26. c) Type text to text field", "Detektiv", FormScreen.ALTERNATIVE_TEXT_FIELD);
            clickAt("26. d) Click at yes", FormScreen.YES_ICON);
            waitForEvent("27. Wait for alternative name field hidden", FormScreen.ALTERNATIVE_NAME_FIELD_HIDDEN);
        }
        else {
            typeToTextField("26. Type text to text field", "Detektiv", FormScreen.FIELD_FORM_NAME);
            waitForEvent("27. Wait for text field timeout", FormScreen.SCENARIO_STEP_TEXT_FIELD_TIMEOUT);
        }

        //Setting up age
        clickAt("28. Click at age plus button", FormScreen.BUTTON_FORM_AGE_PLUS);
        clickAt("29. Click at age plus button", FormScreen.BUTTON_FORM_AGE_PLUS);
        clickAt("30. Click at age minus button", FormScreen.BUTTON_FORM_AGE_MINUS);

        clickAt("31. Click at switch button female", FormScreen.SWITCH_BUTTON_FORM_GENDER_FEMALE);

        waitForEvent("32. Wait for event shaking pen", FormScreen.PEN_SHAKED);
        if(movePenOverSignature("33. Move pen over signature"))logOK();
        else logFail();
        waitForDialog("34. Wait for signature dialog", FormScreen.FORM_SIGNATURE_DIALOG);

        signTheForm("35. Signing the form");

        clickDialogButton("36. Click yes in signature dialog", FormScreen.FORM_SIGNATURE_DIALOG, 1);
        waitForEvent("37. Wait for event form scene visible",FormScreen.SCENARIO_STEP_FORM_SCENE_VISIBLE);

        if(stampTheForm("38. Stamping the form"))logOK();
        else logFail();

        waitForScreen("39. Wait for office page visible", OfficeMenuScreen.class);
        clickAt("40. Click at toggle help overlay button", OfficeMenuScreen.TOGGLE_HELP_OVERLAY_BUTTON_NAME);
    }

    private void clickDetectiveDialogs(int numberSteps){
        int dialogCnt = 0;
        int logCnt = numberSteps;

        while (dialogCnt < PanoramaScreen.NUMBER_OF_OFFICE_DIALOGS){
            dialogCnt++;
            waitForEvent((logCnt++)+". Wait for  detective's dialog ready event " + dialogCnt,PanoramaScreen.DETECTIVES_DIALOG_READY + dialogCnt);
            clickAt((logCnt++) +". Click detective's dialog", PanoramaScreen.DETECTIVES_DIALOG + dialogCnt);
        }
    }

    protected boolean movePenOverSignature(String infoMessage) {
        logInfo(infoMessage);
        final Actor pen = findActorByName(FormScreen.FORM_PEN);
        final Actor signature = findActorByName(FormScreen.FORM_SIGNATURE);

        // this one was fun...we need to drag pen with its top left corner, over middle of signature area and drop it there
        Vector2 penMiddle = getActorPosition(pen, ActorPosition.TOP_LEFT);
        final Vector2 signatureMiddle = getActorPosition(signature, ActorPosition.MIDDLE);

        boolean result = true;

        result &= touchDown(penMiddle.x, penMiddle.y, 0, Input.Buttons.LEFT, pen.getStage());
        result &= dragOverTime((int) penMiddle.x, (int) penMiddle.y, (int) signatureMiddle.x, (int) signatureMiddle.y, 0, 0.5f, 20, pen.getStage());
        result &= touchUp(signatureMiddle.x, signatureMiddle.y, 0, Input.Buttons.LEFT, pen.getStage());

        return result;
    }

    protected boolean signTheForm(String infoMessage) {
        logInfo(infoMessage);
        TablexiaComponentDialog dialog = getDialog(FormScreen.FORM_SIGNATURE_DIALOG);

        boolean result = true;

        Vector2 touchDown = getRandomPositionInsideSignatureDialog(dialog);
        // drawing of random signature by touching down on random position inside opened dialog
        result &= touchDown((int)touchDown.x, (int)touchDown.y, 0, Input.Buttons.LEFT, dialog.getStage());
        wait(100);

        // drag "finger" around dialog to draw random signature (I hope rhombus with line inside will not appear on some random seed :-D)
        // I know you secretly hope it will... :P
        Vector2 start = getRandomPositionInsideSignatureDialog(dialog);
        Vector2 drag = null;
        for (int i = 0; i < 10; i++) {
            drag = getRandomPositionInsideSignatureDialog(dialog);
            dragOverTime((int) start.x, (int) start.y, (int) drag.x, (int) drag.y, 0, 0.2f, 10, dialog.getStage());
            start = drag;
        }

        //Should never be null
        if(drag != null) {
            result &= touchUp((int) drag.x, (int) drag.y, 0, Input.Buttons.LEFT, dialog.getStage());
        }
        else {
            logError("Drag position for signature is null. This should never happen...");
            result = false;
        }
        logOK();
        return result;
    }

    private Vector2 getRandomPositionInsideSignatureDialog(Actor dialog) {
        int dialogX = (int)dialog.getX();
        int dialogY = (int)(dialog.getY() + dialog.getHeight() / 5);
        int dialogWidth = (int)dialog.getWidth();
        int dialogHeight = (int)dialog.getHeight() * 4 / 5;

        return new Vector2(MathUtils.random(dialogX, dialogWidth), MathUtils.random(dialogY, dialogHeight));
    }

    protected boolean stampTheForm(String infoMessage) {
        // drag stamp and drop it on designated place
        logInfo(infoMessage);
        final Actor stamp = findActorByName(FormScreen.FORM_STAMP);

        boolean result = touchDown(stamp.getX() + stamp.getWidth() / 2, stamp.getY() + stamp.getHeight() / 2, 0, Input.Buttons.LEFT, stamp.getStage());

        final int x = FormActorsLayout.STAMP_DROP_AREA_LEFT_X + ((FormActorsLayout.STAMP_DROP_AREA_RIGHT_X - FormActorsLayout.STAMP_DROP_AREA_LEFT_X) / 2) + (int)stamp.getWidth() / 2;
        final int y = FormActorsLayout.STAMP_DROP_AREA_BOTTOM_Y + ((FormActorsLayout.STAMP_DROP_AREA_TOP_Y - FormActorsLayout.STAMP_DROP_AREA_BOTTOM_Y) / 2) + (int)stamp.getHeight() / 2;

        result &= dragOverTime((int) (stamp.getX() + stamp.getWidth() / 2), (int) (stamp.getY() + stamp.getHeight() / 2), x, y, 0, 0.5f, 20, stamp.getStage());
        result &= touchUp(x, y, 0, Input.Buttons.LEFT, stamp.getStage());
        return result;
    }
}
