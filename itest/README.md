# <b>How to run intergation tests</b>

## <b>Run selected test</b>
Use This option to run only one selected test on one device.
Available tests are in tablexia/itest/listOfAvailableITests.txt. 

<b>Desktop version</b>

Run selected test (jar file is in tablexia/desktop/build/libs):
```
java -jar Tablexia\ \(feature-gitlab-ci\)-iTest-<VERSION>.jar <TEST_CLASS_NAME>
```
Log file will be saved into tablexia/desktop/build/libs/iTest_results.

<b>Android version</b>

```
adb shell am start -a android.intent.action.MAIN -n cz.nic.tablexia.itest/cz.nic.tablexia.android.AndroidITestLauncher -e testClassName <TEST_CLASS_NAME>
```
You can run the test from Android Studio using Edit Configurations -> Launch Flags: 
```
-e testClassName <TEST_CLASS_NAME> 
```
and build variant iTest.
Log file will be saved into external memory of connected device in to a folder iTest_results.

<b>iOS version</b>

For running tests on iOS devices, you need a utility "ideviceinstaller"(http://macappstore.org/ideviceinstaller/)

Command ```ideviceinstaller -l``` shows installed applications on your connected iOS device.
For example :
```"cz.nic.tablexia.devel - Tablexia 14714714714"```

For running application on iOS device use command:
```
idevicedebug run "<APPLICATION_ID>" "<PARAMETER_1>" "<PARAMETER_2>"
```
For example:
```
idevicedebug run "cz.nic.tablexia.devel" "myParameter"
```
If there are more than one connected iOS devices, you need to specify what device you want to use using parameter:
```
-u "<FORTY_LOCAL_UDID_OF_DEVICE>"
```

<br />
### <b>Run set of tests using gradle</b>
Put tests, that you want to run in tablexia/itest/iTestBundle.txt (each one to one line)
You can use file listOfAvailableITests.txt to see which tests are available to use or you can use prepared sets in tablexia/itest/testSets.

<b>Desktop version</b>

Command for run desktop tests:
```
./gradlew desktop:runITestBundle
```

Tests will be run one by one as they are written in tablexia/itest/iTestBundle.txt and log files will be saved into tablexia/desktop/build/libs/iTest_results.

<b>Android version</b>

You can run set of tests on one or more connected devices at the same time.
For running tests, you must have set all connected devices to developer mode and have permition to USB debugging.

Command for run android tests:
```
./gradlew runAndroidITestBundle
```
Tests will be run one by one as they are written in tablexia/itest/iTestBundle.txt and log files will be saved into tablexia/android/build/outputs/apk/iTest_results-<DEVICENAME>/iTest_results.
