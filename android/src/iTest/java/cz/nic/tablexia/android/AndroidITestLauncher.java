/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.android;

import android.os.Bundle;

import cz.nic.tablexia.testing.TestRunner;

/**
 * Created by frantisek on 13.4.16.
 */
public class AndroidITestLauncher extends AndroidLauncher {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		String testClassName = null;
		Bundle extras = this.getIntent().getExtras();
		if ( extras != null ) {
			if (extras.containsKey("testClassName")) {
				testClassName = (String) extras.get("testClassName");
			}
		}
		super.onCreate(savedInstanceState);
		runTest(testClassName);
	}

	private void runTest(String testClassName){
		TestRunner testRunner = new TestRunner(tablexia);
		testRunner.runTest(testClassName);
	}

}
