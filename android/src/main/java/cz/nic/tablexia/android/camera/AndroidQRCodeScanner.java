/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.android.camera;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.nic.tablexia.android.AndroidLauncher;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.QRCodeScanner;

/**
 * Created by Vitaliy Vashchenko on 6.10.16.
 */
public class AndroidQRCodeScanner extends QRCodeScanner implements Camera.PreviewCallback {

    private static final List<String> PREFERABLE_FOCUS_MODES = new ArrayList<String>();

    static {
        PREFERABLE_FOCUS_MODES.add(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        PREFERABLE_FOCUS_MODES.add(Camera.Parameters.FOCUS_MODE_INFINITY);
    }

    private static final int MAX_PREVIEW_WIDTH  = 1920;
    private static final int MAX_PREVIEW_HEIGHT = 1080;

    private final AndroidLauncher           activity;
    private       CameraSurface             cameraSurface;
    private       ScreenOrientationListener screenOrientationListener;
    private       MultiFormatReader         reader;

    private boolean previewCalled = false;
    private boolean cameraReady   = false;

    public AndroidQRCodeScanner(AndroidLauncher activity) {
        this.activity = activity;
        screenOrientationListener = new ScreenOrientationListener(activity, SensorManager.SENSOR_DELAY_NORMAL);
        reader = new MultiFormatReader();
    }

    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera) {
        if (isScanningEnabled()) {
            int width = camera.getParameters().getPreviewSize().width;
            int height = camera.getParameters().getPreviewSize().height;
            LuminanceSource source = new PlanarYUVLuminanceSource(bytes, width, height, width / 4, height / 4, width / 2, height / 2, false);  //set searching area in the middle of the screen
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            Result result;
            try {
                result = reader.decode(bitmap);
                onCodeScanned(result.getText());
                Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1f);
                Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
            } catch (NotFoundException e) {
                Log.err(getClass(), "Cannot decode bitmap!", e);
            } finally {
                reader.reset();
            }
        }
    }

    public void onCameraReady() {
        cameraReady = true;
        if (previewCalled) startPreviewAsync();
    }

    @Override
    public void onCameraPreviewStarted() {
        if (isCameraPreviewActive()) stopPreview();
        prepareCameraAsync();

        if (!cameraReady) {
            previewCalled = true;
        } else {
            startPreviewAsync();
        }
    }

    @Override
    public void onCameraPreviewStopped() {
        stopPreviewAsync();
    }

    @Override
    protected CameraPermissionStatus refreshCameraPermission() {
        //TODO
        return CameraPermissionStatus.Allowed;
    }

    @Override
    protected void requestPermissionSettingChange() {
        //TODO
    }

    private synchronized void startPreviewAsync() {
        Runnable r = new Runnable() {
            public void run() {
                startPreview();
            }
        };
        activity.post(r);
    }

    private synchronized void stopPreviewAsync() {
        Runnable r = new Runnable() {
            public void run() {
                stopPreview();
            }
        };
        activity.post(r);
    }

    private synchronized void prepareCameraAsync() {
        Runnable r = new Runnable() {
            public void run() {
                prepareCamera();
            }
        };
        activity.post(r);
    }

    private void prepareCamera() {
        Log.debug(getClass().getSimpleName(), "prepareCamera");
        if (cameraSurface == null) {
            Log.debug(getClass(), "surface is null");
            cameraSurface = new CameraSurface(activity);
        }
        if (cameraSurface.getParent() == null) {
            Log.debug(getClass(), "parent==null - adding view to parent");
            cameraSurface.setZOrderOnTop(true);
            activity.addContentView(cameraSurface, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
    }

    private void startPreview() {
        if (cameraSurface != null && cameraSurface.getCamera() != null) {
            Log.debug(getClass().getSimpleName(), "startPreview");
            cameraSurface.getCamera().setPreviewCallback(this);
            cameraSurface.getCamera().startPreview();
            screenOrientationListener.enable();
        } else {
            prepareCamera();
        }
    }

    private void stopPreview() {
        Log.debug(getClass().getSimpleName(), "stopPreview");
        screenOrientationListener.disable();
        if (cameraSurface != null) {
            Log.debug(getClass().getSimpleName(), "deleting parent");
            ViewParent parentView = cameraSurface.getParent();
            if (parentView instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) parentView;
                viewGroup.removeView(cameraSurface);
            }
            if (cameraSurface.getCamera() != null) {
                cameraSurface.getCamera().setPreviewCallback(null);
                cameraSurface.getCamera().stopPreview();
                cameraSurface.getCamera().release();
            }
            cameraSurface = null;
        }
    }

    private class ScreenOrientationListener extends OrientationEventListener {

        public ScreenOrientationListener(Context context, int rate) {
            super(context, rate);
        }

        @Override
        public void onOrientationChanged(int i) {
            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            if (cameraSurface != null) {
                cameraSurface.orientationChanged(rotation);
            }
        }
    }

    class CameraSurface extends SurfaceView implements SurfaceHolder.Callback {

        private Camera camera;

        public CameraSurface(Context context) {
            super(context);
            getHolder().addCallback(this);
        }

        public void surfaceCreated(SurfaceHolder holder) {
            camera = Camera.open();

        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            try {
                prepareCameraParameters();
                camera.setPreviewDisplay(holder);
                onCameraReady();
            } catch (IOException e) {
                Log.err(getClass(), "Cannot change the surface!", e);
            }
        }

        public void orientationChanged(int i) {
            int orientation;
            if (camera != null) {
                if (i == Surface.ROTATION_180 || i == Surface.ROTATION_270) {
                    orientation = 180;
                } else {
                    orientation = 0;
                }
                camera.setDisplayOrientation(orientation);
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            holder.removeCallback(this);
            holder.getSurface().release();
            camera.release();
            camera = null;
        }

        public Camera getCamera() {
            return camera;
        }

        private void prepareCameraParameters() {
            Camera.Parameters parameters = camera.getParameters();
            List<String> supportedFocusModes = camera.getParameters().getSupportedFocusModes();

            for (String focusMode : PREFERABLE_FOCUS_MODES) {
                if (supportedFocusModes.contains(focusMode)) {
                    parameters.setFocusMode(focusMode);
                    break;
                }
            }

            Camera.Size bestSize = getBestSize(parameters.getSupportedPreviewSizes());

            parameters.setPictureSize(bestSize.width, bestSize.height);
            camera.setParameters(parameters);
        }

        private Camera.Size getBestSize(List<Camera.Size> previewSizes) {

            // Collect the supported resolutions that are at least as big as the preview Surface
            List<Camera.Size> bigEnough = new ArrayList<Camera.Size>();
            // Collect the supported resolutions that are smaller than the preview Surface
            List<Camera.Size> notBigEnough = new ArrayList<Camera.Size>();

            int applicationWidth = Gdx.graphics.getWidth();
            int applicationHeight = Gdx.graphics.getHeight();

            Point screenSize = new Point();
            activity.getWindowManager().getDefaultDisplay().getSize(screenSize);
            screenSize.x = screenSize.x > MAX_PREVIEW_WIDTH ? MAX_PREVIEW_WIDTH : screenSize.x;
            screenSize.y = screenSize.y > MAX_PREVIEW_HEIGHT ? MAX_PREVIEW_HEIGHT : screenSize.y;

            for (Camera.Size option : previewSizes) {
                if (option.width <= screenSize.x && option.height <= screenSize.y &&
                        option.height == option.width * screenSize.x / screenSize.y) {
                    if (option.width >= applicationWidth &&
                            option.height >= applicationHeight) {
                        bigEnough.add(option);
                    } else {
                        notBigEnough.add(option);
                    }
                }
            }

            // Pick the smallest of those big enough. If there is no one big enough, pick the
            // largest of those not big enough.
            if (bigEnough.size() > 0) {
                return Collections.min(bigEnough, new CompareSizesByArea());
            } else if (notBigEnough.size() > 0) {
                return Collections.max(notBigEnough, new CompareSizesByArea());
            } else {
                return previewSizes.get(0);
            }
        }

    }

    static class CompareSizesByArea implements Comparator<Camera.Size> {

        @Override
        public int compare(Camera.Size lhs, Camera.Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.width * lhs.height -
                    (long) rhs.width * rhs.height);
        }

    }
}
