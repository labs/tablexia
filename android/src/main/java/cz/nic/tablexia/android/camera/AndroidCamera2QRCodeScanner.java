/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.android.camera;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.nic.tablexia.android.AndroidLauncher;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.QRCodeScanner;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class AndroidCamera2QRCodeScanner extends QRCodeScanner {

    private static final int            IMAGE_FORMAT              = ImageFormat.YUV_420_888;
    private final static String         BACKGROUND_THREAD_NAME    = "BACKGROUND_THREAD";

    private final        int            IMAGE_READER_BUFFER_LIMIT = 4;
    private static final int            MAX_PREVIEW_WIDTH         = 1280;
    private static final int            MAX_PREVIEW_HEIGHT        = 720;

    private static final long           QR_CODE_SCAN_DELAY        = 333; //milliseconds

    private AndroidLauncher        androidLauncher;
    private TextureView            textureView;
    private String                 cameraId;
    private CameraDevice           cameraDevice;
    private CaptureRequest.Builder captureRequestBuilder;
    private CameraCaptureSession   cameraCaptureSessions;
    private Handler                backgroundHandler;
    private HandlerThread          backgroundThread;
    private ImageReader            imageReader;
    private MultiFormatReader      qrReader;
    private Size                   previewSize;
    private CameraManager          cameraManager;
    private Integer                viewRotation;

    private boolean cameraAccessible = false;


    public AndroidCamera2QRCodeScanner(AndroidLauncher androidLauncher) {
        this.androidLauncher = androidLauncher;
        cameraAccessible = androidLauncher.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        qrReader = new MultiFormatReader();
    }

    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener =
            new ImageReader.OnImageAvailableListener() {
                private byte[] tempImageData = null;
                private Long lastTime;

                @Override
                public void onImageAvailable(ImageReader reader) {
                    backgroundHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Image img = reader.acquireLatestImage();
                            if(img == null) return;

                            //Initializing lastTime here, because we want to skip first frame. (It crashes otherwise)
                            if(lastTime == null) lastTime = System.currentTimeMillis();
                            if(System.currentTimeMillis() - lastTime > QR_CODE_SCAN_DELAY) { //Skip all frames which are not QR_CODE_SCAN_DELAY apart
                                if(img != null) img.close();
                                lastTime = System.currentTimeMillis();
                                return;
                            }

                            try {
                                int width = img.getWidth();
                                int height = img.getHeight();

                                //Each line of pixels (except last one) has a padding at the end, which we need to crop.
                                int xPadding = img.getPlanes()[0].getRowStride() - width;

                                //Copy yBuffer data to tempImageData
                                ByteBuffer yBuffer = img.getPlanes()[0].getBuffer();
                                if(tempImageData == null || tempImageData.length < yBuffer.capacity()) {
                                    tempImageData = new byte[yBuffer.capacity()];
                                }
                                yBuffer.get(tempImageData);

                                //Get the cropped source and scan QR code
                                LuminanceSource source = new PlanarYUVLuminanceSource(tempImageData, width + xPadding, height, 0, 0, width, height, false);
                                BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
                                Result rawResult = qrReader.decode(bitmap);
                                onCodeScanned(rawResult.getText());
                            } catch (ReaderException e) {
                                Log.err(getClass(), "ReaderException thrown!", e);
                            } catch (NullPointerException e) {
                                Log.err(getClass(), "NPE while reading QR Code!", e);
                            } finally {
                                qrReader.reset();
                                if (img != null) img.close();
                            }
                        }
                    });
                }

            };

    public void setCameraDevice(CameraDevice cameraDevice) {
        this.cameraDevice = cameraDevice;
    }

    public CameraDevice getCameraDevice() {
        return cameraDevice;
    }

    @Override
    public void onCameraPreviewStarted() {
        textureView = new TextureView(androidLauncher);
        textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                prepareCamera(width, height);
                startBackgroundThread();
                configureTransform(width, height);
                openCamera();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                configureTransform(width, height);
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                if (viewRotation != null && viewRotation != getOrientation())
                    configureTransform(textureView.getWidth(), textureView.getHeight());
            }
        });
        textureView.bringToFront();
        androidLauncher.post(new Runnable() {
            @Override
            public void run() {
                androidLauncher.addContentView(textureView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
        });
    }

    @Override
    public void onCameraPreviewStopped() {
        androidLauncher.post(new Runnable() {
            @Override
            public void run() {
                if (textureView != null) {
                    ViewParent viewParent = textureView.getParent();
                    if (viewParent instanceof ViewGroup) {
                        ViewGroup viewGroup = (ViewGroup) viewParent;
                        viewGroup.removeView(textureView);
                    }
                }
                closeCamera();
                if (backgroundThread != null) stopBackgroundThread();
            }
        });
    }

    @Override
    protected CameraPermissionStatus refreshCameraPermission() {
        //TODO implement this properly
        if(cameraAccessible) return CameraPermissionStatus.Allowed;
        else return CameraPermissionStatus.Denied;
    }

    @Override
    protected void requestPermissionSettingChange() {
        //TODO - implement this!
    }

    private int getOrientation(){
        return androidLauncher.getWindowManager().getDefaultDisplay().getRotation();
    }

    private void prepareCamera(int width, int height) {
        cameraManager = (CameraManager) androidLauncher.getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String id : cameraManager.getCameraIdList()) {

                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(id);
                Integer lens = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (lens != CameraCharacteristics.LENS_FACING_BACK) continue;

                cameraId = id;
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                Point displaySize = new Point();
                androidLauncher.getWindowManager().getDefaultDisplay().getSize(displaySize);
                int maxPreviewWidth = displaySize.x;
                int maxPreviewHeight = displaySize.y;

                if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                    maxPreviewWidth = MAX_PREVIEW_WIDTH;
                }

                if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                    maxPreviewHeight = MAX_PREVIEW_HEIGHT;
                }


                Size largest = Collections.max(
                        Arrays.asList(map.getOutputSizes(IMAGE_FORMAT)),
                        new CompareSizesByArea());

                previewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                        width, height, maxPreviewWidth,
                        maxPreviewHeight, largest);
                imageReader = ImageReader.newInstance(previewSize.getWidth(), previewSize.getHeight(),
                        IMAGE_FORMAT, IMAGE_READER_BUFFER_LIMIT);
                imageReader.setOnImageAvailableListener(mOnImageAvailableListener, backgroundHandler);

            }
            // if none of cameras doesn't fit -> go back to loader screen
            if (cameraId == null) androidLauncher.onBackPressed();
        } catch (CameraAccessException e) {
            Log.err(getClass(), "Error while opening the camera", e);
        }
    }

    @SuppressLint("MissingPermission")
    private void openCamera() {
        // TODO: 22.11.16 add permission requst for API > 23 
        Log.debug(getClass(), "Opening the camera");
        try {
            cameraManager.openCamera(cameraId, new CameraDevice.StateCallback() {
                @Override
                public void onOpened(CameraDevice cameraDevice) {
                    setCameraDevice(cameraDevice);
                    createCameraPreview();
                }

                @Override
                public void onDisconnected(CameraDevice cameraDevice) {
                    cameraDevice.close();
                }

                @Override
                public void onError(CameraDevice cameraDevice, int i) {
                    cameraDevice.close();
                    setCameraDevice(null);
                }
            }, backgroundHandler);
        } catch (CameraAccessException e) {
            Log.err(getClass(), "Error while opening the camera", e);
        }
        Log.debug(getClass(), "Camera is opened");
    }

    private void createCameraPreview() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            texture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
            Surface surface = new Surface(texture);
            Surface imageSurface = imageReader.getSurface();
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(imageSurface);
            captureRequestBuilder.addTarget(surface);
            cameraDevice.createCaptureSession(Arrays.asList(imageSurface, surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                    if (null == cameraDevice) {
                        return;
                    }
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                    Log.err(getClass(), "ConfigureFailed");
                }
            }, backgroundHandler);
        } catch (CameraAccessException e) {
            Log.err(getClass(), "Can't access the camera", e);
        }
    }

    private void updatePreview() {
        if (null == cameraDevice) {
            Log.err(getClass(), "Camera device = null");
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, backgroundHandler);
        } catch (CameraAccessException e) {
            Log.err(getClass(), "Can't access the camera", e);
        }
    }

    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
        if (textureView != null) {
            textureView = null;
        }
    }

    protected void startBackgroundThread() {
        backgroundThread = new HandlerThread(BACKGROUND_THREAD_NAME);
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }

    protected void stopBackgroundThread() {
        backgroundThread.quitSafely();
        try {
            backgroundThread.join();
            backgroundThread = null;
            backgroundHandler = null;
        } catch (InterruptedException e) {
            Log.err(getClass(), "Error while stopping background thread", e);
        }
    }

    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth, 
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        List<Size> bigEnough = new ArrayList<Size>();
        List<Size> notBigEnough = new ArrayList<Size>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            return choices[0];
        }
    }

    private void configureTransform(int viewWidth, int viewHeight) {
        Log.debug(getClass(), "Transforming the view");

        if (textureView == null || previewSize == null) return;

        int rotation = getOrientation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, previewSize.getHeight(), previewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / previewSize.getHeight(),
                    (float) viewWidth / previewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        viewRotation = rotation;
        textureView.setTransform(matrix);
    }

    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }

}
