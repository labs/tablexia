/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.android;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;

import java.io.File;
import java.util.ArrayList;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.android.camera.AndroidCamera2QRCodeScanner;
import cz.nic.tablexia.android.camera.AndroidCameraOpener;
import cz.nic.tablexia.android.camera.AndroidQRCodeScanner;
import cz.nic.tablexia.debug.BuildConfig;
import cz.nic.tablexia.debug.R;
import cz.nic.tablexia.loader.application.ApplicationAvatarManager;
import cz.nic.tablexia.screen.loader.IConnectionManager;
import cz.nic.tablexia.screen.loader.ILegacyManager;
import cz.nic.tablexia.util.IFileSystemManager;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.QRCodeScanner;

public class AndroidLauncher extends AndroidApplication {

    public static final Tablexia.SQLConnectionType SQL_CONNECTION_TYPE = new Tablexia.SQLConnectionType("org.sqldroid.SQLDroidDriver", "jdbc:sqldroid:");

    private static final int PERMISSIONS_REQUEST_CODE = 1;

    private static final int MULTI_SAMPLING_2X = 2;
    private static final boolean HAS_SOFT_BACK_BUTTON = false;
    private static final boolean HAS_ALTERNATIVE_CONTROLS = false;

    protected Tablexia tablexia;
    private AndroidCameraOpener cameraOpener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.cameraOpener = new AndroidCameraOpener(this);

        // workaround for launching app from market or installation dialog
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(intent.getAction())) {
                finish();
                return;
            }
        }

        ArrayList<String> permissions = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CAMERA);
        }

        if(permissions.size()>0){
            ActivityCompat.requestPermissions(this, permissions.toArray(new String[0]), AndroidLauncher.PERMISSIONS_REQUEST_CODE);
        }


        QRCodeScanner qrCodeScanner = android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? new AndroidCamera2QRCodeScanner(this) : new AndroidQRCodeScanner(this);
        TablexiaAndroidNet tablexiaNet = new TablexiaAndroidNet(this);
        AndroidLegacyManager androidLegacyManager = new AndroidLegacyManager();

        if (tablexiaNet.isLegacyFailed()){
            androidLegacyManager.setSupported(false);
        }


        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useWakelock = true;
        config.numSamples = MULTI_SAMPLING_2X;

        initialize(tablexia = new Tablexia(BuildConfig.BUILD_TYPE,
                getResources().getConfiguration().locale,
                SQL_CONNECTION_TYPE,
                new AndroidConnectionManager(getContext()),
                androidLegacyManager,
                cameraOpener,
                new AndroidFileSystemManager(),
                qrCodeScanner,
                getResources().getString(R.string.sentry_dsn),
                HAS_SOFT_BACK_BUTTON,
                HAS_ALTERNATIVE_CONTROLS,
                savedInstanceState == null,
                Build.SERIAL,
                false,
                tablexiaNet), config);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean exitOnEnd = false;
        for (int i = 0; i < permissions.length; i++) {
            Log.info(getClass().getSimpleName(), "Permission: " + permissions[i] + "was: " + grantResults[i]);
            if(permissions[i] == Manifest.permission.WRITE_EXTERNAL_STORAGE && grantResults[i] == PackageManager.PERMISSION_DENIED){
                exitOnEnd = true;
            }
        }

        if (exitOnEnd) exit();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AndroidCameraOpener.TAKE_PHOTO_CODE) {
            Log.info(getClass(), "Take Photo Request Code. ResultCode == " + resultCode);
            if (resultCode == RESULT_OK && cameraOpener.getPhotoPath() != null) {
                String imagePath = getPath(cameraOpener.getPhotoPath());
                Log.info(getClass(), "ImagePath: " + imagePath);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    imagePath = getExternalFilesDir("") + imagePath.replace("files/", "");
                Pixmap resultPixmap = ApplicationAvatarManager.getInstance().createAvatarPixmap(new FileHandle(imagePath));
                cameraOpener.deleteTemporaryFile();
                cameraOpener.onPhotoTaken(resultPixmap, true);
            } else {
                cameraOpener.onPhotoTaken(null, false);
            }
        }
    }

    private String getPath(Uri uri) {
        String uriType = Build.VERSION.SDK_INT < Build.VERSION_CODES.N ? "file" : "content";
        if (uri.getScheme().equalsIgnoreCase(uriType)) {
            return uri.getPath();
        }

        Cursor cursor = getContentResolver().query(uri, new String[]{MediaStore.Images.Media.DATA}, null, null, null);

        if (cursor == null) {
            return null;
        }

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        String filePath = cursor.getString(column_index);

        cursor.close();

        return filePath;
    }

    public void post(Runnable r) {
        handler.post(r);
    }

    private static class AndroidConnectionManager implements IConnectionManager {
        private Context androidContext;

        public AndroidConnectionManager(Context androidContext) {
            this.androidContext = androidContext;
        }

        @Override
        public boolean isUsingMobileData() {
            return getConnectionType() == ConnectionType.Mobile;
        }

        @Override
        public ConnectionType getConnectionType() {
            ConnectivityManager conMan = (ConnectivityManager) androidContext.getSystemService(Context.CONNECTIVITY_SERVICE);

            //We are not connected to the internet at all...
            if (conMan == null || conMan.getActiveNetworkInfo() == null)
                return ConnectionType.Unknown;

            //Is user using any of mobile internet types ? Hope I didn't miss any...
            switch (conMan.getActiveNetworkInfo().getType()) {
                case ConnectivityManager.TYPE_MOBILE:
                case ConnectivityManager.TYPE_MOBILE_DUN:
                case ConnectivityManager.TYPE_MOBILE_HIPRI:
                case ConnectivityManager.TYPE_MOBILE_MMS:
                case ConnectivityManager.TYPE_MOBILE_SUPL:
                    return ConnectionType.Mobile;

                case ConnectivityManager.TYPE_ETHERNET:
                    return ConnectionType.Ethernet;

                case ConnectivityManager.TYPE_WIFI:
                    return ConnectionType.Wifi;

                default:
                    return ConnectionType.Unknown;
            }
        }
    }

    public static class AndroidFileSystemManager implements IFileSystemManager {
        @Override
        public Long getSpaceAvailable(String path) {
            File file = new File(path);
            file.mkdirs();
            return file.getUsableSpace();
        }
    }

    private static class AndroidLegacyManager implements ILegacyManager {

        private boolean isSupported = true;

        public void setSupported(boolean supported) {
            isSupported = supported;
        }

        @Override
        public boolean needLegacyCheck() {
            return android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1;
        }

        @Override
        public boolean isSupportedDevice() {
            return isSupported;
        }

        @Override
        public void checkLegacy() {
        }
    }
}