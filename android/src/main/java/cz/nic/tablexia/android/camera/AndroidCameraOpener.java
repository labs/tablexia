/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.android.camera;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;

import cz.nic.tablexia.android.AndroidLauncher;
import cz.nic.tablexia.util.CameraOpener;
import cz.nic.tablexia.util.Log;

public class AndroidCameraOpener extends CameraOpener {
    public static final int TAKE_PHOTO_CODE = 80085;

    AndroidLauncher androidLauncher;
    Uri             photoPath;

    private static final String EXTERNAL_FILES_DIR_TYPE = "PHOTO";

    private static final String AVATAR_FILE_NAME_PREFIX = "avatar";
    private static final String AVATAR_FILE_NAME_SUFFIX = null;
    
    
    public AndroidCameraOpener(AndroidLauncher androidLauncher) {
        this.androidLauncher = androidLauncher;
    }

    public Uri getPhotoPath() {
        return photoPath;
    }


    @Override
    public void onCameraOpen() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(androidLauncher.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = File.createTempFile(AVATAR_FILE_NAME_PREFIX, AVATAR_FILE_NAME_SUFFIX, androidLauncher.getExternalFilesDir(EXTERNAL_FILES_DIR_TYPE));
            } catch (IOException ex) {
                Log.err(AndroidCameraOpener.class, "Cant create temporary file to save avatar.");
            }
            if (photoFile != null) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    photoPath = Uri.fromFile(photoFile);
                } else {
                    photoPath = FileProvider.getUriForFile(androidLauncher.getContext(), androidLauncher.getContext().getPackageName() + ".provider", photoFile);
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoPath);
                androidLauncher.startActivityForResult(takePictureIntent, TAKE_PHOTO_CODE);
            }
        }

    }

    @Override
    public void onCameraClose() {
        
    }

    @Override
    public boolean cameraAccessible() {
        return androidLauncher.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA) &&
                ContextCompat.checkSelfPermission(androidLauncher, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

    }

    @Override
    public void deleteTemporaryFile() {
        if (photoPath != null){
            File temp = new File(photoPath.getPath());
            if (temp.exists()) temp.delete();
            photoPath = null;
        }
    }
}
