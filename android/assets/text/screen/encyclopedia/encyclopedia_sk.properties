#
# Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

play=Prehrať
pause=Pauza

hint_header=Rada pre teba
#    <!-- Dysgraphia -->
dysgraphia=Dysgrafia
dysgraphia_definition=Jednou z tzv. špecifických porúch učenia, ktorá označuje výrazne problémy v písomnom prejave, najmä v jeho grafickej podobe.

dysgraphia_paragraph_1=Písmo človeka s dysgrafiou je spravidla veľmi nečitateľné. Takýmto ľuďom na začiatku školskej dochádzky veľmi dlho trvalo, kým sa naučili písať. Všetky kľučky a oblúčiky v písmenách pre nich znamenali problém. Aj neskôr, keď potrebujú písmom rýchle zaznamenať informácie, ľahko sa prihodí, že je až príliš kostrbaté, neurovnané a nečitateľné (niekedy len pre okolie, ale občas po sebe neprečíta ani sám pisateľ).

dysgraphia_paragraph_2=V dospievaní a dospelosti je písanie predovšetkým prostriedok na zachytenie informácií a sprostredkovanie informácií iným ľuďom. Ak je písmo nečitateľné a túto funkciu neplní, je zbytočné ho využívať. V takom prípade je výhodnejšie buď zmeniť typ písma - niekedy stačí, keď namiesto písaného začne dotyčný používať tlačené písmo, niektorým tiež pomohol prechod na nové písmo Comenia Script - viď http://www.lencova.eu/cs/uvod/comenia_script, alebo sa naučiť písať rýchlo a efektívne všetkými desiatimi prstami na počítači. Na internete sa dajú stiahnuť voľne dostupné programy pre osvojenie písania na klávesnici všetkými desiatimi prstami.

dysgraphia_hint_text=Ak nemôžeš prečítať svoje poznámky a neprečítajú ich po tebe ani ostatní, skús písať tlačeným písmom, alebo sa nauč písať všetkými desiatimi na počítači.

#    <!-- Dyslexia -->
dyslexia=Dyslexia
dyslexia_definition=Tzv. špecifická porucha učenia, ktorá spôsobuje problémy v čítanom prejave, a to aj napriek kvalitnej výučbe, domácemu nácviku a primeraným (niekedy dokonca nadpriemerným!) rozumovým schopnostiam. Hovorí sa, že ľudia s dyslexiou sú tí, ktorí sa učia inak.

dyslexia_paragraph_1=Súčasný výskum naznačuje, že ide o ťažkosti, ktoré sú spôsobené drobným poškodením centrálnej nervovej sústavy a zrejme sú dedičné. Súvisia s niekoľkými ďalšími typmi oslabení a v bežnom živote sa prejavujú práve problémami v čítaní. Tieto problémy pretrvávajú celý život a nesúvisia iba so školou. Záleží na ich intenzite a tiež na rozsahu terapeutickej práce (tzv. reedukácie). Vo vyššom veku môžu byť ťažkosti niekedy úplne zminimalizované, niekedy ich človek vníma ako veľmi intenzívne. Okrem toho je tiež dôležité si uvedomiť, za akých podmienok ide človeku čítanie lepšie. Ak je to iba trochu možné, oplatí sa vytvárať si pre čítanie také prostredie, aby človeka čo najmenej zaťažovalo. Svoju úlohu môže hrať denná doba, ale tiež pokoj a dostatok času na čítanie, využívanie rôznych moderných technológií (napr. elektronické čítačky, hlasové výstupy z počítačov, audioknihy alebo nastavenie počítača tak, aby boli texty čo najprehľadnejšie).

dyslexia_paragraph_2=V súčasnej dobe žijeme v tzv. informačnej spoločnosti v ktorej si využívaním písania a čítania neustále vymieňame informácie a pre ľudí, ktorí sa naučili ľahko a rýchlo čítať v prvej triede základnej školy môže byť niekedy nepochopiteľné, že to niekomu nejde. Preto sa z dyslexie stáva väčší problém než v minulosti. Súčasne sa začína hovoriť, že ľudia s dyslexiou sú tí, ktorí sa učia inak. To znamená, že sa dokážu naučiť (niektorí môžu byť významnými expertmi vo svojom obore), ale potrebujú nájsť trochu odlišné spôsoby učenia a mať vytvorené také podmienky, aby im čítanie a písanie nebránilo v prejave ich schopností. Čím je človek starší, tým viac by mal sám vedieť, za akých okolností sa mu lepšie číta a pracuje. Vo všetkých typoch škôl majú žiaci s dyslexiou nárok na prispôsobenie podmienok štúdia, ale je dobré vedieť, čo presne potrebujú, aby boli prípadné úpravy skutočne efektívne.

dyslexia_hint_text=Keď si si nie istý ako sa čo píše, používaj prehľady gramatických pravidiel. Môžeš si ich vytvoriť na počítači, aby si sa v nich vyznal.

#    <!-- Dysortografia -->
dysorthographia=Dysortografia

dysorthographia_definition=Problémy v písomnom prejave. Text človeka s dysortografiou je plný rôznych chýb a nepresností, ktorých sa nedopúšťa opakovane a nijak nesúvisia s jeho rozumovými schopnosťami.

dysorthographia_paragraph_1=Tzv. špecifická porucha učenia, ktorá sa prejavuje problémami v písomnom prejave, predovšetkým v jeho gramatickej zložke. Medzi typické chyby ľudí s dysortografiou patrí: vynechávanie hlások v slovách, chýbajúce mäkčene a čiarky (t.j. diakritika), zámeny mäkkých a tvrdých slabík, zámeny znelých a neznelých hlások (napr. c/s/z, b/p, g/k, m/n, h/ch), prehadzovanie hlások v slovách do takej miery, že sú celé skomolené alebo nesprávna aplikácia gramatických pravidiel. Niekedy ide o to, že človek s dysortografiou správnu hlásku "ako keby nepočul" alebo si neuvedomuje, z ktorých všetkých hlások sa slovo skladá napriek tomu, že slovo ako celok počuje a chápe ho (to je práve jeden z prejavov dysortografie). Inokedy ide o situácie, kedy nefunguje rýchla automatická aplikácia gramatických pravidiel. Človek s dysortografiou často pravidlo pozná aj ho vie presne vysvetliť, ale keď musí napísať vetu, v ktorej sa kombinuje mnoho rôznych pravidiel gramatiky, v tom istom pravidle, ktoré práve bezchybne zdôvodnil, urobí chybu.

dysorthographia_paragraph_2=Rovnako ako pri dyslexii je dôležité si uvedomiť, za akých podmienok je pre človeka najvýhodnejšie písať tak, aby množstvo chýb v texte bolo čo najnižšie. Rovnako ako pri dyslexii a dysgrafii tiež platí, že žiaci a študenti s dysortografiou majú vo všetkých typoch škôl nárok na prispôsobenie podmienok štúdia. Niekedy pomôže dostatok času a pokoj na prácu, niekedy písanie na počítači s možnosťou automatických opráv. Takisto je nesmierne dôležité vedieť texty po sebe veľmi dôkladne kontrolovať a vedieť, čomu je potrebné venovať zvýšenú pozornosť. V prípade, keď je obsah textu veľmi dôležitý sa vyplatí, keď po pisateľovi prečíta text niekto iný a chyby opraví, pretože dotyčný ich väčšinou nevidí. Iný trik, ktorý niekedy zaberá, je čítať text odzadu tak, že čítame slová ako sú napísané, ale od konca vety alebo odseku, takže nie je možné sa sústrediť na obsah textu, ale iba na každé slovo zvlášť.

dysorthographia_hint_text=Dôležité texty, ktoré si napísal si vždy nechaj niekým skontrolovať!

#    <!-- Memory -->
memory=Pamäť
memory_definition=Niektoré zložky pamäte môžu byť u človeka s dyslexiou oslabené a tým sa mu komplikuje učenie, pre ktoré je pamäť nesmierne dôležitá. Neplatí to však vždy.

memory_paragraph_1=Pamäť má mnoho rôznych foriem - inak si pamätáme zážitky z dovolenky, inú pamäť využívame, keď sa učíme bicyklovať a inú keď sa učíme vzorce z chémie alebo slovíčka v jazyku a pod. Práve preto, že máme mnoho rôznych typov pamäte, je možné nahradiť slabý druh pamäte za iný, ktorý máme rozvinutý dobre. V neposlednom rade je vhodné vedieť, že pokiaľ máme oslabenú niektorú zložku pamäte, je potrebné prijať také opatrenia, aby dôsledky boli minimálne (napríklad všetko, čo je potrebné si pamätať si dotyčný zapisuje, pri riešení úloh je vhodné priebežné písanie poznámok, zapisovanie si čiastkových výsledkov a podobne).

memory_short_term=Krátkodobá pamäť

memory_short_term_definition=V krátkodobej pamäti si ukladáme informácie, ktoré sa k nám práve dostali.

memory_short_term_paragraph_1=Používame ju napríklad pri inštrukcii učiteľa, čo majú žiaci robiť, na ktorej strane si majú otvoriť učebnice alebo ktoré cvičenie vypracovať. Pomocou nej si zapamätáme telefónneho čísla, ktoré potrebujeme vytočiť alebo si zapísať, oboznámujeme sa s menami osôb s ktorými sme sa práve zoznámili, pamätáme si časy a miesta, kde sa koná nejaké stretnutie, ktoré si potrebujeme zapísať do diára a podobne. Niektorí ľudia (nielen) s dyslexiou môžu mať krátkodobú pamäť oslabenú. Informácie v nej uložené rýchlo zabudnú, spravidla ešte skôr, ako ich stihnú využiť (kým otvoria učebnicu nevedia, ktoré cvičenie bolo zadané, skôr ako vstúpia do miestnosti, zabudnú, čo z nej mali priniesť atď.). Dôležitú úlohu hrá fakt, že obidva údaje, ktoré si uložíme v krátkodobej pamäti sa nakoniec dostanú aj do dlhodobej. Tá slúži pre uchovávanie údajov na dlhý čas, niekedy na celý život.

memory_short_term_paragraph_2=Dobrá správa je, že pamäť sa dá trénovať (preto sú tú zaradené niektoré aktivity, ktorým sa v hre venujete). Navyše je dobré vedieť, že pokiaľ mám túto pamäť oslabenú, potrebujem si dôležité údaje hneď zapisovať alebo si ich viac krát prečítať a častejšie opakovať. Tiež je dobré vedieť, kde sú informácie uvedené, aby sme sa k nim mohli vrátiť, keď ich potrebujeme a vidieť ich viackrát, kým si ich zapamätáme.

memory_short_term_hint_text=Čo si nepamätáš, to si ihneď zapíš. Používaj "externú pamäť". 

memory_working=Pracovná pamäť

memory_working_definition=Pracovnú pamäť potrebujeme, keď vykonávame nejakú činnosť čiže prácu, ako to vyplýva aj z jej názvu.

memory_working_paragraph_1=V nej sa kombinujú údaje, ktoré si vybavíme z dlhodobej pamäte a súčasne informácie uložené v krátkodobej pamäti, ktoré súvisia s tým, čo práve robíme. Vezmime si napríklad výpočet zložitejšieho matematického príkladu (35x7) alebo zapisovanie poznámok pri výklade učiteľa. V prvom prípade musíme rozložiť výpočet na niekoľko častí, stále si však musíme pamätať pôvodné zadanie príkladu a súčasne si musíme pamätať, čo sme už vypočítali a čo ešte nie, musíme si vybaviť z pamäti násobilku aj postup práce, ktorý sme sa kedysi naučili. Človek s oslabenou pracovnou pamäťou môže naraziť na to, že kým vypočíta 5x7 zabudne, aký bol predchádzajúci výsledok alebo začne s počítaním 30x7 a medzitým zabudne, ako znelo zadanie celého príkladu, takže napíše výsledok výpočtu a neuvedomí si, že toto nie je definitívny výsledok. Pri zapisovaní poznámok si zase musíme pamätať, čo učiteľ povedal a zároveň registrovať, čo práve hovorí. Okrem toho pri zápise musíme aplikovať rôzne gramatické pravidlá, premýšľať o pravidlách slovosledu alebo o rôznych značkách a skratkách, ktoré vystihnú to, čo bolo povedané.

memory_working_paragraph_2=Niektorí ľudia s dyslexiou majú s týmto spôsobom pamäťovej práce problémy. Z tohto dôvodu je pre nich dôležité pracovnú pamäť zámerne trénovať (viď aktivity v hre), ale tiež vedieť, ako sa vyhnúť tomu, aby sa nemuseli spoliehať iba na pamäť. Napríklad namiesto písania poznámok môžeme používať diktafón alebo si pamätať, kde sú podklady k preberanému učivu zapísané v učebnici, aby sme si mohli zapisovať iba niektoré dôležité údaje. Keď pracujeme na niečom zložitejšom, je výhodnejšie si priebežne zapisovať načo sme už prišli, čo sme urobili a podobne.

memory_working_hint_text=Trénuj si postupy práce! Predtým ako začneš niečo robiť, povedz si pre seba, čo všetko musíš splniť a načo nesmieš zabudnúť. Používaj slová "po prvé", "po druhé", "po tretie" atď.

memory_auditory=Sluchová pamäť

memory_auditory_definition=Sluchová pamäť slúži pre počuté informácie. Ide o typ krátkodobej pamäte, ktorá sa využíva na špecifický druh informácií, teda tých, ktoré si dotyčný musí zapamätať sluchom.

memory_auditory_paragraph_1=Niekedy sa stáva, že ľudia s dyslexiou majú práve tento druh krátkodobej pamäte oslabený. Opäť je užitočné jej posilňovanie, ale súčasne je výhodné vedieť, že aby sa človek nemusel spoliehať len na sluchovú pamäť, čokoľvek dôležité si môžeme zapísať. Situácia je horšia, keď sa ide o zapamätanie počutých informácií, ktoré si musíme pamätať mechanicky, teda bez logických súvislosti (napr. zoznam vecí na nákup, slovíčka v jazyku, vybrané slová alebo chemický či fyzikálny vzorec). Ak si môžeme vytvoriť medzi údajmi logické súvislosti, utriediť si ich tak, aby dávali väčší zmysel, rozdeliť ich do logických skupín podľa určitej vlastnosti, podľa nadradeného pojmu, podľa podobného znenia atď., tak je zapamätanie omnoho účinnejšie. Inou formou opory môže byť vizualizácia počutého. Vtedy si predstavujeme čo počujeme, alebo si to iba symbolicky zobrazujeme vo fantázii, napríklad pomocou farieb, tvarov a pod.

memory_auditory_hint_text=Keď si nepamätáš slová, skús si informácie predstavovať ako obrázky, značky, farby a podobne - možno potom pôjde zapamätávanie ľahšie.

memory_visual=Zraková pamäť

memory_visual_definition=Zraková pamäť je určená pre videné informácie. Ide o druh krátkodobej pamäte, ktorá spracováva údaje, ktoré prijímame zrakom.

memory_visual_paragraph_1=Niekedy býva u ľudí s dyslexiou oslabená, ale často sa práve naopak stáva, že na rozdiel od sluchovej pamäte, ktorá pri dyslexii trpí častejšie, táto pamäť funguje veľmi dobre. Je preto nanajvýš žiaduce ju pri práci využívať a všetko, čo si potrebujeme zapamätať, musíme nielen počuť, ale tiež vidieť v nejakej podobe znázornené (obrázky, súvislý text, grafy, tabuľky, nákresy, symboly a podobne).

memory_visual_hint_text=Zoznam informácií si vždy skús usporiadať do nejakých logických skupín - zapamätanie tak pôjde ľahšie.

#    <!-- Spatial orientation -->
spatial_orientation=Priestorová orientácia

spatial_orientation_definition=Orientácia v priestore znamená uvedomenie si pozície vľavo, vpravo, vpredu, vzadu, hore, dole a správne určenie smeru.

spatial_orientation_paragraph_1=Niektorí ľudia s dyslexiou (neplatí to samozrejme všeobecne pre všetkých) čelia v tejto oblasti určitým oslabeniam. Je pre nich ťažšie rýchlo určiť, kde je napravo a naľavo. Keď sa ocitnú v novom prostredí, nepamätajú si trasu akou prišli, nedokázali by bez pomoci znovu nájsť cestu a podobne. Často sa im stáva, že sa niekde stratia, prichádzajú neskôr, pretože si neuvedomili, že majú odbočiť, nedokážu nájsť rovnakú cestu späť, nepamätajú si, kde zaparkovali auto a podobne. Orientáciu v priestore však potrebujeme tiež preto, aby sme sa dokázali správne orientovať na stránke v texte, aby sme si uvedomili, že čítame vždy zľava doprava, odhora dole či akým smerom vedieme čiaru pri písaní.

spatial_orientation_paragraph_2=Priestorovú orientáciu je tiež možné veľmi dobre trénovať. Aj ľudia, ktorí nemajú žiadny orientačný zmysel sa v priestore môžu naučiť orientovať, ale vyžaduje to viac úsilia, driny, tréningu a cieleného premýšľania o priestore okolo nás. Ak rezignujeme a budeme iba hlásať, že priestorovú orientáciu nemáme, pravdepodobne to tak aj ostane. Medzi najjednoduchšie postupy na začiatku tréningu môžu patriť zámerné vyhľadávania nápovedí (bodov a miest v okolí), ktoré nám umožnia cielene sledovať, kam ideme a čo bolo v okolí. Tomu cieľu je však potrebné venovať pozornosť! Inou užitočnou nápoveďou môže byť uvedomenie si, kde majú ľudia srdce, keď sa nich dívame. Skoro každý z nás to dokáže určiť a spoznať, jednoducho to na ľuďoch vidíme. Je to ich ľavá strana, od ktorej môžeme odvodiť, ktorá je tá line 83pravá a tým pádom čo je naľavo, napravo, pred a za dotyčnou osobou.

spatial_orientation_hint_text=Ak niekam ideš, vždy si všímaj orientačné body.

#    <!-- Seriality -->
seriality=Serialita

seriality_definition=Postupnosť, alebo schopnosť stanoviť presné kroky ako vykonávať určitú činnosť tak, aby sme na nič nezabudli.

seriality_paragraph_1=Prejaví sa napríklad pri stanovení postupu práce alebo tiež pri spôsobe riešenia určitej úlohy. Ak máme niekomu popísať, ako niečo robíme, musíme využiť serialitu. Serialita sa premietne do našej práce vtedy keď nič nezabudneme, vsetko dokončíme do finále, nevynecháme nič podstatné a podobne. Môžeme tiež hovoriť o zrakovej serialite, kedy musíme poznať poradie či popísať sekvenciu po sebe idúcich informácií, ktoré vidíme. Alebo sa niekedy hovorí o sluchovej serialite, kedy zachytávame postup toho, o čom sa hovorí a čo počujeme. Často však potrebujeme obidve seriality. Zložité procesy tiež vyžadujú serialitu, aby sme dokázali popísať naše myšlienkové procesy, ktoré nevidíme a nepočujeme.

seriality_paragraph_2=Dyslexia je niekedy sprevádzaná oslabením schopnosti seriality. Zaujímavé však je, že to býva často nahradené veľmi dokonalou schopnosťou tzv. celostného myslenia, kedy naopak jedinec dokáže spracovávať informácie v širších súvislostiach a dokáže nájsť riešenie bez toho, aby vedel, ako k nemu prišiel. Keď má informácie prezentované vo väčších celkoch (ako napr. príbeh, film, obrázok s mnohými detailami a podobne) pracuje sa mu s nimi lepšie, ako keď sú prezentované v presných zoznamoch a v presnej sekvencii deja. Niekedy sa hovorí, že veľmi rozvinuté celostné myslenie je jednou z "výhod" dyslexie. Takáto schopnosť je určite veľmi cenná a je potrebné ju pestovať a využívať vždy keď je to vhodné. Niekedy je ale užitočné (v škole, v práci či doma) vedieť používať postupy a odovzdávať ich druhým (keď vieme, ako sme na niečo prišli, ale nedokážeme to okoliu vysvetliť, je to niekedy nepoužiteľné.) Dobrá správa spočíva opäť v tom, že svoje schopnosti seriality môžeme zdokonaľovať tréningom.

seriality_hint_text=Vždy si na prácu vyčleň dosť času! Najmä keď vieš, že hrozí, že budeš robiť veľa chýb.

#    <!-- Auditory distinction -->
auditory_distinction=Sluchové rozlišovanie

auditory_distinction_definition=Schopnosť uvedomiť si odlišnosť podobne znejúcich hlások a slov, prípadne ďalších zvukov.

auditory_distinction_paragraph_1=Sluchové rozlišovanie sa postupne rozvíja v predškolskom veku a schopnosť určiť správne hlásky v slove je nevyhnutným predpokladom schopnosti naučiť sa čítať. Deti na začiatku prvej triedy dokážu slabikovať slová, určiť prvé a posledné hlásky v slovách (veľmi rýchlo sa potom naučia vyhláskovať celé slová), uvedomiť si dĺžku hlások a rozlíšiť podobne znejúce, ale pritom odlišné slová. To všetko patrí do kategórie tzv. fonetického uvedomenia (fonéma = hláska). Práve táto schopnosť sa u ľudí s dyslexiou ukazuje ako tradične oslabená a takmer u všetkých z nich spôsobuje problémy s čítaním, rovnako ako pri písaní u ľudí s dysortografiou (v konečnom dôsledku tieto dva druhy problémov idú spravidla ruka v ruke, iba v určitom veku možno jeden trochu preváži nad druhým - v slovenčine je rozhodne náročnejšie naučiť sa písať bezchybne). Napriek tomu, že dotyční počujú úplne bezchybne, keď dôjde na presné určenie podobne znejúcich zvukov a tónov, akoby rozdiel "nepočuli" alebo sa musia nad ním aj vo vyššom veku dôkladne zamyslieť, aby ho dokázali určiť a neuvedomujú si ho automaticky. To isté sa deje tiež s uvedomením hlások v slovách - slovo ako celok perfektne vnímajú a chápu, ale presné určenie každej jednej hlásky je náročnejšie. Fonetické uvedomenie potrebujeme nielen keď slovo píšeme, ale zloženie hlások nám napovie aj to, ako sa asi bude vyslovovať. Preto sa deti na prvom stupni základnej školy, pokiaľ im bola diagnostikovaná dyslexia, učia slová hláskovať a rôzne s hláskami pracovať (určovať dĺžku hlások, odlišovať mäkké a tvrdé slabiky a pod.). Niekedy sa veľmi dobre vyvinie táto schopnosť pri nácviku, ale stále nie je úplne zautomatizovaná pre rýchle čítanie a písanie, preto sa pri ňom stále môžu objavovať chyby. Jedným z najdôležitejších opatrení, ktoré môžu uľahčiť prácu je dostatok času na obidve spomenuté činnosti, aby sa jedinec mohol v pokoji zamyslieť, ako slovo prečítať (najmä ak sa ide o cudzie alebo menej obvyklé slová) či ako slovo napísať. Veľmi často sa potom tieto problémy objavia vo väčšej miere pri učení cudzích jazykov alebo pri stretnutí s neznámymi slovami (napr. s odbornými pojmami, ktoré sme doteraz nepočuli).

auditory_distinction_hint_text=Pozoruj sám seba! Všímaj si, kedy sa učíš najlepšie (podľa dennej doby, druhu učiva, prítomnosti iných ľudí, množstva obrázkov, spôsobu výkladu a podobne). Takýto postup potom pri príprave do školy využívaj čo najviac.

#    <!--Specific learning disabilities-->
specific_learning_disabilities=Špecifické poruchy učenia

specific_learning_disabilities_definition=Slovné spojenie, ktoré zahŕňa dyslexiu, dysgrafiu, dysortografiu, ale tiež dyskalkúliu (ťažkosti v matematických schopnostiach) a niektoré ďalšie menej časté formy problémov v učení (napr. dyspraxiu alebo pohybovú neobratnosť).

specific_learning_disabilities_paragraph_1=Výhodou tohto označenia je, že spája všetky typy ťažkostí do jednej kategórie. Veľmi často sa totiž stáva, že človek, ktorý má dyslexiu, má tiež dysortografiu a dysgrafiu. Preto, keď v bežnej reči hovoríme o človeku s dyslexiou či o dyslektikovi, obvykle máme na mysli človeka, ktorý má problémy s čítaním a písaním.

specific_learning_disabilities_paragraph_2=Nevýhodou tohto pojmu však je, že slovo učenie až príliš často asociuje školské prostredie a niekedy si preto neuvedomíme, že ťažkosti sa premietajú aj do mimoškolských, každodenných činností a pretrvávajú aj v dospelosti, keď už do žiadnej školy nechodíme. Okrem toho, označenie porucha neznie príliš pekne. O mnoho dôležitejšie je pristupovať k ľuďom s dyslexiou, dysgrafiou či dysortografiou ako k tým, ktorým sa učia sa pracujú inak. Nie sú "porušení", iba potrebujú odlišné podmienky na prácu. A je predsa normálne, že každý sme iný.

specific_learning_disabilities_hint_text=Máš v slovenčine povinnú literatúru? Prečítaj si povinnú knihu ako e-knihu s použitím čítačky (ľahko si v nej nastavíš veľkosť písma aj množstvo textu na stránku) alebo si ju zožeň ako audioknihu a "čítaj ušami" pomocou MP3.

#    <!--Introduction-->
introduction=O encyklopedii
introduction_paragraph_1=Nasledujúce heslá stručne vysvetľujú, čo znamená dyslexia a ďalšie príbuzné pojmy, a v čom môžu spočívať ďalšie ťažkosti ľudí s dyslexiou. Pri každom hesle nájdete tučným písmom zvýraznenú jednu až dve vety. Pokiaľ sa Vám nechce čítať celý dlhý text, pozrite sa iba na tieto stručné definície. Ak máte záujem dozvedieť sa viac, prejdite si postupne aj ďalšie odseky pri každom hesle. Text si tiež môžete nechať od niekoho prečítať a spoločne sa porozprávať o tom, čo vlastne znamená.

#    <!--Famous dyslectics-->
famous_dyslectics=Slávni dyslektici
famous_dyslectics_paragraph_1=Napriek tomu, že dylexia znamená problémy, ľudia, ktorí ňou trpia, môžu mať súčasne rôznorodé nadanie, ktoré musí byť podporené. Preto je dobré vedieť si uvedomiť, čo potrebujú, aby sa napriek dyslexii mohli prejaviť a venovať tomu, čo ich baví, zaujíma, v čom vynikajú. Pre inšpiráciu uvádzame výpočet niektorých slávnych ľudí s dyslexiou:

famous_dyslectics_person_1=Orlando Bloom (herec)
famous_dyslectics_person_2=Tom Cruise (herec)
famous_dyslectics_person_3=Whoopi Goldberg (herečka)
famous_dyslectics_person_4=Keanu Reeves (herec)
famous_dyslectics_person_5=Robin Williams (herec)
famous_dyslectics_person_6=Keira Knightley (herečka)
famous_dyslectics_person_7=Pablo Picasso (maliar)
famous_dyslectics_person_8=Auguste Rodin (sochár)
famous_dyslectics_person_9=Andy Warhol (maliar)
famous_dyslectics_person_10=Muhammad Ali (boxer)
famous_dyslectics_person_11=Magic Johnson (basketbalista)
famous_dyslectics_person_12=Henry Ford (zakladateľ automobilky Ford)
famous_dyslectics_person_13=William Hewlett (zakladateľ počítačovej spoločnosti)
famous_dyslectics_person_14=Winston Churchill (politik)
famous_dyslectics_person_15=John F. Kennedy (politik)
famous_dyslectics_person_16=Nelson Rockefeller (podnikateľ)
famous_dyslectics_person_17=Woodrow Wilson (politik)
famous_dyslectics_person_18=George Washington (politik)
famous_dyslectics_person_19=Hans Christian Andersen (spisovateľ)
famous_dyslectics_person_20=Agatha Christie (spisovateľka)
famous_dyslectics_person_21=F. Scott Fitzgerald (spisovateľ)
famous_dyslectics_person_22=Alexander Graham Bell (vynálezca)
famous_dyslectics_person_23=Thomas Edison (vynálezca)
famous_dyslectics_person_24=John Lennon (hudobník)
famous_dyslectics_person_25=Švédská kráľovská rodina

#    <!--Visual distinction-->
visual_distinction=Zrakové rozlišovanie

visual_distinction_definition=Schopnosť rozlišovať podobne vyzerajúce symboly a znaky.

visual_distinction_paragraph_1=Ide o obdobu sluchového rozlišovania a u ľudí s dyslexiou býva táto schopnosť niekedy dlhodobo oslabená. Inak sa obvykle rozvinie v predškolskom veku a deti ju dokážu dobre využívať po nástupe do školy, kedy sa učia čítať, písať a počítať.Oslabené zrakové rozlišovanie môže viesť k zámenám podobných znakov či obrázkov a teda tiež k zámenám podobných písmen ako sú b/d/p, u/n, a/e/o, s/z a iné. Vždy ide iba o veľmi nepatrný detail, ktorý je znázornený inak alebo umiestnený na inej strane, ale vytvára podstatný rozdiel vo význame.Podobné problémy sa potom môžu objaviť pri určení smeru jazdy, kedy ide o rovnakú šípku, ktorá iba mieri iným smerom, alebo pri určovaní podobných značiek produktov v obchode a pod. Rovnako ako sluchové rozlišovanie aj zrakové rozlišovanie sa môže trénovať a zdokonaľovať. To sa robí najmä na prvom stupni základnej školy, kedy sa ukáže, že žiak má nejaké problémy.

visual_distinction_hint_text=Využívaj pri práci čo najviac zmyslov! O čom sa hovorí, to si nájdi ako obrázok. Ak to pôjde, skús si to chytiť, pohybuj s tým, alebo si to sám/sama zapíš či nakresli.

#    <!--Verbal skills-->
verbal_skills=Verbálne schopnosti

verbal_skills_definition=Vyjadrovanie myšlienok, hovorenie v celých súvislých vetách, schopnosť vysvetliť iným problém či postup práce, prednášanie pred druhými ľuďmi (aj na verejnosti) - to všetko patrí medzi verbálne schopnosti. 

verbal_skills_paragraph_1=Verbálne znamená rečové, takže všetko, čo súvisí s týmito schopnosťami, súvisí s našou rečou. Ľudia sa svojími verbálnymi schopnosťami líšia - niektorí majú veľmi bohatú slovnú zásobu a dokážu hovoriť kvetnato, dlho a ich prejavy sa dobre počúvajú. Často preto, že je ich reč plynulá, príjemne intonujú, dokážu rýchlo zareagovať na niečo, čo povedal niekto iný, používajú rôzne slovné obraty, vtipy a humor. Iní môžu mať veľmi dobrú slovnú zásobu a môžu vedieť dávať slová na papier dobre, ale pri prejave pred druhými ľuďmi sa zadrhávajú, ich reč je nudná a nezáživná. A ďalší ľudia môžu byť v prejavoch veľmi neskúsení a skladanie slov do súvislých viet im ide veľmi ťažko. Dôležitým východiskom kvalitných verbálnych schopností je bohatá slovná zásoba - tu získame čítaním (napríklad aj audiokníh a čítačiek), chodením do divadla, pozeráním filmov, rozprávanim sa s druhými ľudmi a podobne.

verbal_skills_paragraph_2=Ľudia s dyslexiou majú niekedy verbálne schopnosti trochu oslabené a to hlavne vtedy, keď sa ich problémy s čítaním spoja aj s problémami s hovorením (logopedické problémy). Ale! - naopak veľká skupina ľudí s problémami s dyslexiou dokáže verbálne schopnosti veľmi dobre využívať. Pod heslom "keď nedokážeme písať a čítať, musíme to obkecať" sa ich verbálne schopnosti stali ich silnou stránkou. Môžu byť vynikajúcimi rečníkmi, dokážu sa vyjadrovať veľmi presne a ich okolie im túto schopnosť často závidí a obdivuje ich. Majme pritom na pamäti, že rovnako ako všetky ostatné schopnosti, ktoré sú v Encyklopédií popísané, aj tieto verbálne sa dajú trénovať.

verbal_skills_hint_text=Keď si si nie istý svojím prejavom (napríklad referát v triede) povedz ho na verejnosti svojím kamarátom, rodičom, súrodencom alebo domácim zvieratkám. Keď sa učíš, vysvetľuj si učenie polohlasne, alebo to potom niekomu celé prerozprávaj.
