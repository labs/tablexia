/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.jcabi.manifests.Manifests;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaBuildConfig;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.screen.loader.IConnectionManager;
import cz.nic.tablexia.screen.loader.ILegacyManager;
import cz.nic.tablexia.util.IFileSystemManager;

public class DesktopLauncher {
    private static final String     BUILD_VARIANT_MANIFEST_ATTRIBUTE    = "Build-Type";
    private static final String     SENTRY_DSN_KEY_MANIFEST_ATTRIBUTE   = "Sentry-DSN";
    private static final boolean    HAS_SOFT_BACK_BUTTON                = true;
    private static final boolean    HAS_ALTERNATIVE_CONTROLS            = true;


    private static final String DESKTOP_ICON_PATH                    = "icon/";
    private static final String DESKTOP_ICON_16                      = DESKTOP_ICON_PATH + "desktop_icon_16.png";
    private static final String DESKTOP_ICON_32                      = DESKTOP_ICON_PATH + "desktop_icon_32.png";
    private static final String DESKTOP_ICON_128                     = DESKTOP_ICON_PATH + "desktop_icon_128.png";
    private static final String DESKTOP_ICON_DEBUG_16                = DESKTOP_ICON_PATH + "desktop_icon_debug_16.png";
    private static final String DESKTOP_ICON_DEBUG_32                = DESKTOP_ICON_PATH + "desktop_icon_debug_32.png";
    private static final String DESKTOP_ICON_DEBUG_128               = DESKTOP_ICON_PATH + "desktop_icon_debug_128.png";
    private static final String DESKTOP_ICON_DEVEL_16                = DESKTOP_ICON_PATH + "desktop_icon_devel_16.png";
    private static final String DESKTOP_ICON_DEVEL_32                = DESKTOP_ICON_PATH + "desktop_icon_devel_32.png";
    private static final String DESKTOP_ICON_DEVEL_128               = DESKTOP_ICON_PATH + "desktop_icon_devel_128.png";
    private static final String DESKTOP_ICON_ITEST_16                = DESKTOP_ICON_PATH + "desktop_icon_itest_16.png";
    private static final String DESKTOP_ICON_ITEST_32                = DESKTOP_ICON_PATH + "desktop_icon_itest_32.png";
    private static final String DESKTOP_ICON_ITEST_128               = DESKTOP_ICON_PATH + "desktop_icon_itest_128.png";
    private static final int 	MULTI_SAMPLING_2X					 = 2;

    private static final Tablexia.SQLConnectionType SQL_CONNECTION_TYPE = new Tablexia.SQLConnectionType("org.sqlite.JDBC", "jdbc:sqlite:");

    public static void main(String[] arg) {
            initTablexia();
    }

    public static Tablexia initTablexia(){
        String buildType         = loadAttributeFromManifest(BUILD_VARIANT_MANIFEST_ATTRIBUTE, TablexiaSettings.BuildType.DEVEL.getKey());
        String sentryDSN         = loadAttributeFromManifest(SENTRY_DSN_KEY_MANIFEST_ATTRIBUTE, null);
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();


        config.resizable = false;
        config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
        config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
        config.samples = MULTI_SAMPLING_2X;
        config.title = TablexiaBuildConfig.APP_NAME;
        config.fullscreen = true;

        if (buildType == null || buildType.equals(TablexiaSettings.BuildType.DEVEL.getKey())) {
            config.addIcon(DESKTOP_ICON_DEVEL_16, Files.FileType.Internal);
            config.addIcon(DESKTOP_ICON_DEVEL_32, Files.FileType.Internal);
            config.addIcon(DESKTOP_ICON_DEVEL_128, Files.FileType.Internal);
        } else if (buildType.equals(TablexiaSettings.BuildType.ITEST.getKey())) {
            config.addIcon(DESKTOP_ICON_ITEST_16, Files.FileType.Internal);
            config.addIcon(DESKTOP_ICON_ITEST_32, Files.FileType.Internal);
            config.addIcon(DESKTOP_ICON_ITEST_128, Files.FileType.Internal);
        } else if (buildType.equals(TablexiaSettings.BuildType.DEBUG.getKey())) {
            config.addIcon(DESKTOP_ICON_DEBUG_16, Files.FileType.Internal);
            config.addIcon(DESKTOP_ICON_DEBUG_32, Files.FileType.Internal);
            config.addIcon(DESKTOP_ICON_DEBUG_128, Files.FileType.Internal);
        } else if (buildType.equals(TablexiaSettings.BuildType.RELEASE.getKey())) {
            config.addIcon(DESKTOP_ICON_16, Files.FileType.Internal);
            config.addIcon(DESKTOP_ICON_32, Files.FileType.Internal);
            config.addIcon(DESKTOP_ICON_128, Files.FileType.Internal);
        }

        Tablexia tablexia = new Tablexia(
                buildType,
                Locale.getDefault(),
                SQL_CONNECTION_TYPE,
                new DesktopConnectionManager(),
                new DesktopLegacyManager(),
                null, //CameraOpener
                new DesktopFileSystemManager(),
                null,
                sentryDSN,
                HAS_SOFT_BACK_BUTTON,
                HAS_ALTERNATIVE_CONTROLS,
                true,
                null,
                true);

	    new LwjglApplication(tablexia, config);
        return tablexia;
    }

    private static String loadAttributeFromManifest(String attributeName, String defaultValue) {
        if (Manifests.exists(attributeName)) {
            return Manifests.read(attributeName);
        }
        return defaultValue;
    }

    private static class DesktopConnectionManager implements IConnectionManager {
        @Override
        public boolean isUsingMobileData() {
        	return false;
		}

        @Override
        public ConnectionType getConnectionType() {
            //TODO - Wifi || Ethernet in Java?
            return ConnectionType.Ethernet;
        }
    }

    public static class DesktopFileSystemManager implements IFileSystemManager {
        @Override
        public Long getSpaceAvailable(String path) {
            File file = new File(path);
            if(!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            long space = file.getUsableSpace();
            file.delete();
            return space;
        }
    }

    private static class DesktopLegacyManager implements ILegacyManager {

        @Override
        public boolean needLegacyCheck() {
            return false;
        }

        @Override
        public boolean isSupportedDevice() {
            return true;
        }

        @Override
        public void checkLegacy() {

        }
    }

}
