/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

apply plugin: "java"

final String GEN_DIRECTORY = "gen/"
final String TABLEXIA_PACKAGE_PATH = "${GEN_DIRECTORY}cz/nic/tablexia/"
final String BUILD_CONFIG_FILE = "${TABLEXIA_PACKAGE_PATH}TablexiaBuildConfig.java"
final String BUILD_CONFIG_FILE_ENCODING = "UTF-8"

targetCompatibility = 1.8
sourceCompatibility = 1.8
[compileJava, compileTestJava]*.options*.encoding = 'UTF-8'

sourceSets.main.java.srcDirs = ["src/", "gen/", "external/tablexia-shared"]
sourceSets.test.java.srcDirs = ["test/", "external/copyright-test"]


eclipse.project {
    name = appName + "-core"
}

clean {
    delete GEN_DIRECTORY
}

task writeTablexiaBuildConfig {
    dependsOn([':util:checksum:runChecksum', ':util:checksum:runHdChecksum'])
    doLast {
        File folder = file(TABLEXIA_PACKAGE_PATH)
        if (!folder.exists()) {
            folder.mkdirs()
        }
        File buildConfigFile = file(BUILD_CONFIG_FILE)
        buildConfigFile.write("package cz.nic.tablexia;\n" +
                "\n" +
                "/**\n" +
                " * Automatically generated file. Do not modify or add to repository!\n" +
                " */\n" +
                "public class TablexiaBuildConfig {\n" +
                "\n" +
                "    public final static String  APP_NAME = \"${tablexiaAppName}\";\n" +
                "    public final static String  APPLICATION_VERSION_NAME = \"${tablexiaVersionName}\";\n" +
                "    public final static Integer APPLICATION_VERSION_CODE = ${tablexiaVersionCode};\n" +
                "    public final static String  MODEL_VERSION_NAME = \"${tablexiaModelVersionName}\";\n" +
                "    public final static Integer MODEL_VERSION_CODE = ${tablexiaModelVersionCode};\n" +
                "    public final static String  ASSETS_CHECKSUM = \"${getMapConvertedToString(rootProject.ext.assetsChecksum)}\";\n" +
                "    public final static String  ASSETS_HD_CHECKSUM = \"${getMapConvertedToString(rootProject.ext.assetsHdChecksum)}\";\n" +
                "    public final static String  TABLEXIA_ASSETS_URL = \"${project.assetsDownloadUrl}\";\n" +
                "    public final static String  TABLEXIA_SERVER_PROTOCOL = ${project.hasProperty('TABLEXIA_SERVER_PROTOCOL') ? "\"$TABLEXIA_SERVER_PROTOCOL\"" : "null"};\n" +
                "    public final static String  TABLEXIA_SERVER_HOST = \"${project.serverHost}\";\n" +
                "    public final static Integer TABLEXIA_SERVER_PORT = ${project.hasProperty('TABLEXIA_SERVER_PORT') ? "$TABLEXIA_SERVER_PORT" : "null"};\n" +
                "    public final static String  TABLEXIA_SERVER_SECRET = \"${project.serverSecret}\";\n" +
                "    public final static String  SENTRY_DSN_FALLBACK = \"${project.sentryDSNFallbackValue}\";\n" +
                "\n" +
                "}", BUILD_CONFIG_FILE_ENCODING)
    }
}

compileJava.dependsOn writeTablexiaBuildConfig
