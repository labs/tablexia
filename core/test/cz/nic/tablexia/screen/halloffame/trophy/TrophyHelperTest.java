/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.halloffame.trophy;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.trophy.GameTrophyDefinition;
import cz.nic.tablexia.game.trophy.ITrophyDefinition;
import cz.nic.tablexia.game.trophy.UserTrophyDefinition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TrophyHelperTest {

    @Test
    public void testGetPossibleTrophiesForGame() {
        List<ITrophyDefinition> expectedTrophies;
        
        expectedTrophies = new ArrayList<>();
        Collections.addAll(expectedTrophies, UserTrophyDefinition.values());
        expectedTrophies.add(GameTrophyDefinition.ROBBERY_PLAY1);
        expectedTrophies.add(GameTrophyDefinition.ROBBERY_PLAY2);
        expectedTrophies.add(GameTrophyDefinition.ROBBERY_PLAY3);
        expectedTrophies.add(GameTrophyDefinition.ROBBERY_DIFF1);
        expectedTrophies.add(GameTrophyDefinition.ROBBERY_DIFF2);
        expectedTrophies.add(GameTrophyDefinition.ROBBERY_BONUS);
        
        checkPossibleTrophies(expectedTrophies, TrophyHelper.getPossibleTrophiesForGame(GameDefinition.ROBBERY));
        
        expectedTrophies = new ArrayList<>();
        Collections.addAll(expectedTrophies, UserTrophyDefinition.values());
        expectedTrophies.add(GameTrophyDefinition.PURSUIT_PLAY1);
        expectedTrophies.add(GameTrophyDefinition.PURSUIT_PLAY2);
        expectedTrophies.add(GameTrophyDefinition.PURSUIT_PLAY3);
        expectedTrophies.add(GameTrophyDefinition.PURSUIT_DIFF1);
        expectedTrophies.add(GameTrophyDefinition.PURSUIT_DIFF2);
        expectedTrophies.add(GameTrophyDefinition.PURSUIT_BONUS);
        
        checkPossibleTrophies(expectedTrophies, TrophyHelper.getPossibleTrophiesForGame(GameDefinition.PURSUIT));

        expectedTrophies = new ArrayList<>();
        Collections.addAll(expectedTrophies, UserTrophyDefinition.values());
        expectedTrophies.add(GameTrophyDefinition.KIDNAPPING_PLAY1);
        expectedTrophies.add(GameTrophyDefinition.KIDNAPPING_PLAY2);
        expectedTrophies.add(GameTrophyDefinition.KIDNAPPING_PLAY3);
        expectedTrophies.add(GameTrophyDefinition.KIDNAPPING_DIFF1);
        expectedTrophies.add(GameTrophyDefinition.KIDNAPPING_DIFF2);
        expectedTrophies.add(GameTrophyDefinition.KIDNAPPING_BONUS);
        
        checkPossibleTrophies(expectedTrophies, TrophyHelper.getPossibleTrophiesForGame(GameDefinition.KIDNAPPING));

        expectedTrophies = new ArrayList<>();
        Collections.addAll(expectedTrophies, UserTrophyDefinition.values());
        expectedTrophies.add(GameTrophyDefinition.NIGHT_WATCH_PLAY1);
        expectedTrophies.add(GameTrophyDefinition.NIGHT_WATCH_PLAY2);
        expectedTrophies.add(GameTrophyDefinition.NIGHT_WATCH_PLAY3);
        expectedTrophies.add(GameTrophyDefinition.NIGHT_WATCH_DIFF1);
        expectedTrophies.add(GameTrophyDefinition.NIGHT_WATCH_DIFF2);
        expectedTrophies.add(GameTrophyDefinition.NIGHT_WATCH_BONUS);
        
        checkPossibleTrophies(expectedTrophies, TrophyHelper.getPossibleTrophiesForGame(GameDefinition.NIGHT_WATCH));

        expectedTrophies = new ArrayList<>();
        Collections.addAll(expectedTrophies, UserTrophyDefinition.values());
        expectedTrophies.add(GameTrophyDefinition.SHOOTING_RANGE_PLAY1);
        expectedTrophies.add(GameTrophyDefinition.SHOOTING_RANGE_PLAY2);
        expectedTrophies.add(GameTrophyDefinition.SHOOTING_RANGE_PLAY3);
        expectedTrophies.add(GameTrophyDefinition.SHOOTING_RANGE_DIFF1);
        expectedTrophies.add(GameTrophyDefinition.SHOOTING_RANGE_DIFF2);
        expectedTrophies.add(GameTrophyDefinition.SHOOTING_RANGE_BONUS);

        checkPossibleTrophies(expectedTrophies, TrophyHelper.getPossibleTrophiesForGame(GameDefinition.SHOOTING_RANGE));

        expectedTrophies = new ArrayList<>();
        Collections.addAll(expectedTrophies, UserTrophyDefinition.values());
        expectedTrophies.add(GameTrophyDefinition.IN_THE_DARKNESS_PLAY1);
        expectedTrophies.add(GameTrophyDefinition.IN_THE_DARKNESS_PLAY2);
        expectedTrophies.add(GameTrophyDefinition.IN_THE_DARKNESS_PLAY3);
        expectedTrophies.add(GameTrophyDefinition.IN_THE_DARKNESS_DIFF1);
        expectedTrophies.add(GameTrophyDefinition.IN_THE_DARKNESS_DIFF2);
        expectedTrophies.add(GameTrophyDefinition.IN_THE_DARKNESS_BONUS);

        checkPossibleTrophies(expectedTrophies, TrophyHelper.getPossibleTrophiesForGame(GameDefinition.IN_THE_DARKNESS));
        expectedTrophies = new ArrayList<>();
        Collections.addAll(expectedTrophies, UserTrophyDefinition.values());
        expectedTrophies.add(GameTrophyDefinition.RUNES_PLAY1);
        expectedTrophies.add(GameTrophyDefinition.RUNES_PLAY2);
        expectedTrophies.add(GameTrophyDefinition.RUNES_PLAY3);
        expectedTrophies.add(GameTrophyDefinition.RUNES_DIFF1);
        expectedTrophies.add(GameTrophyDefinition.RUNES_DIFF2);
        expectedTrophies.add(GameTrophyDefinition.RUNES_BONUS);

        checkPossibleTrophies(expectedTrophies, TrophyHelper.getPossibleTrophiesForGame(GameDefinition.RUNES));

        expectedTrophies = new ArrayList<>();
        Collections.addAll(expectedTrophies, UserTrophyDefinition.values());
        expectedTrophies.add(GameTrophyDefinition.CRIME_SCENE_PLAY1);
        expectedTrophies.add(GameTrophyDefinition.CRIME_SCENE_PLAY2);
        expectedTrophies.add(GameTrophyDefinition.CRIME_SCENE_PLAY3);
        expectedTrophies.add(GameTrophyDefinition.CRIME_SCENE_DIFF1);
        expectedTrophies.add(GameTrophyDefinition.CRIME_SCENE_DIFF2);
        expectedTrophies.add(GameTrophyDefinition.CRIME_SCENE_BONUS);

        checkPossibleTrophies(expectedTrophies, TrophyHelper.getPossibleTrophiesForGame(GameDefinition.CRIME_SCENE));

        expectedTrophies = new ArrayList<>();
        Collections.addAll(expectedTrophies, UserTrophyDefinition.values());
        expectedTrophies.add(GameTrophyDefinition.PROTOCOL_PLAY1);
        expectedTrophies.add(GameTrophyDefinition.PROTOCOL_PLAY2);
        expectedTrophies.add(GameTrophyDefinition.PROTOCOL_PLAY3);
        expectedTrophies.add(GameTrophyDefinition.PROTOCOL_DIFF1);
        expectedTrophies.add(GameTrophyDefinition.PROTOCOL_DIFF2);
        expectedTrophies.add(GameTrophyDefinition.PROTOCOL_BONUS);

        checkPossibleTrophies(expectedTrophies, TrophyHelper.getPossibleTrophiesForGame(GameDefinition.PROTOCOL));

        expectedTrophies = new ArrayList<>();
        Collections.addAll(expectedTrophies, UserTrophyDefinition.values());
        expectedTrophies.add(GameTrophyDefinition.SAFE_PLAY1);
        expectedTrophies.add(GameTrophyDefinition.SAFE_PLAY2);
        expectedTrophies.add(GameTrophyDefinition.SAFE_PLAY3);
        expectedTrophies.add(GameTrophyDefinition.SAFE_DIFF1);
        expectedTrophies.add(GameTrophyDefinition.SAFE_DIFF2);
        expectedTrophies.add(GameTrophyDefinition.SAFE_BONUS);

        checkPossibleTrophies(expectedTrophies, TrophyHelper.getPossibleTrophiesForGame(GameDefinition.SAFE));
        
    }

    @Test
    public void testGetTrophyDefinitionByName() {
        for (ITrophyDefinition userTrophyDefinition : UserTrophyDefinition.values()){
            assertEquals("Should return other user trophy definition", userTrophyDefinition, TrophyHelper.getTrophyDefinitionByName(userTrophyDefinition.name()));
        }
        for (GameTrophyDefinition gameTrophyDefinition : GameTrophyDefinition.values()){
            assertEquals("Should return other game trophy definition", gameTrophyDefinition, TrophyHelper.getTrophyDefinitionByName(gameTrophyDefinition.name()));
        }

    }
    
    @Test
    public void trophyHelperHasAllTrophies(){
        List<ITrophyDefinition> allTrophies = new ArrayList<>();
        Collections.addAll(allTrophies, GameTrophyDefinition.values());
        Collections.addAll(allTrophies, UserTrophyDefinition.values());
        
        for (ITrophyDefinition iTrophyDefinition: allTrophies){
            assertTrue("Trophy helper is missing " + iTrophyDefinition.name(), TrophyHelper.trophiesProperties.containsKey(iTrophyDefinition));
        }
    }

    public static void checkPossibleTrophies(List<ITrophyDefinition> expectedTrophies, List<ITrophyDefinition> actualTrophies) {
        assertEquals("Expected " + expectedTrophies.size() + " trophies, but got " + actualTrophies.size(), expectedTrophies.size(), actualTrophies.size());
        assertEquals("Actual trophies doesn't include some expected trophy/ies", expectedTrophies, actualTrophies);
    }
}