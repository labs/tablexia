/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.test.structure;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lmarik on 10.1.18.
 */

public class TablexiaDebugTest {

    private static final String CORE_PATH       = "../tablexia/core/src/cz/nic/tablexia/";
    private static final String JAVA_EXTENSION  = ".java";

    private static final String DEBUG_REGEX     = "(setDebug\\s*\\(\\s*true\\s*\\)|setDebug\\s*\\(\\s*true,(\\s*(true|false))\\s*\\))";
    private static final String COMMENT_REGEX   = "(\\/\\*([^*]|(\\*+([^*\\/])))*\\*+\\/)|(\\/\\/.*)";

    private List<String> javaPathList = new ArrayList<>();


    @Before
    public void tearUp() {
        File coreDirectory = new File(CORE_PATH);
        loadJavaFiles(coreDirectory.listFiles());
    }

    @Test
    public void debugTest() {
        boolean testResult = true;

        for (String path : javaPathList) {
            testResult &= checkFile(path);
        }

        Assert.assertTrue("Detected bad debug setting in files (" + javaPathList.size() + ")", testResult);
    }

    private boolean checkFile(String filePath) {
        boolean fileIsCorrect = true;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            int lineCnt = 0;
            boolean lineIsInComment = false;

            while ((line = reader.readLine()) != null) {
                lineCnt++;

                //remove comment in line
                line = line.replaceAll(COMMENT_REGEX, "");

                int startComment = line.indexOf("/*");
                int finalComment = line.indexOf("*/");

                if (startComment != -1) {
                    lineIsInComment = true;
                }

                if (finalComment != -1) {
                    lineIsInComment = false;
                }

                Matcher matcher = Pattern.compile(DEBUG_REGEX).matcher(line);
                while (matcher.find()) {
                    int startIndex = matcher.start();

                    if (!lineIsInComment || startIndex < startComment) {
                        fileIsCorrect = false;
                        System.out.println("Detect wrong debug set in file: " + filePath + " on line: " + lineCnt);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return fileIsCorrect;
    }

    private void loadJavaFiles(File[] files) {
        if (files == null || files.length == 0)
            return;

        for (File file : files) {
            if (file.isDirectory()) loadJavaFiles(file.listFiles());
            else if (file.getName().endsWith(JAVA_EXTENSION))
                javaPathList.add(file.getAbsolutePath());
        }
    }
}
