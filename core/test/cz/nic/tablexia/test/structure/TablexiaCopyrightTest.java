/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.test.structure;

import java.util.ArrayList;
import java.util.List;

import cz.nic.util.test.structure.AbstractCopyrightTest;

public class TablexiaCopyrightTest extends AbstractCopyrightTest {

    private static final List<String>   EXCLUDED_FILES      = new ArrayList<String>() {{
                                                                add("R.java");
                                                                add("BuildConfig.java");
                                                                add("TablexiaBuildConfig.java");
                                                                add("cache.properties");
                                                                add("robovm.properties");
                                                                add("gradle-wrapper.properties");
                                                                add("project.properties");
                                                                add("gradle.properties");
                                                                add("local.properties");
                                                                add("build");
                                                                add("CameraSurface.java");
                                                                add(".idea");
                                                                }};

    private static final String[]   PATHS             = new String[]{"../", "../android/assets/text/", "../android", "../", "../"};
    private static final String[]   EXTENSIONS        = new String[]{JAVA_EXTENSION, PROPERTIES_EXTENSION, XML_EXTENSION, GRADLE_EXTENSION, YML_EXTENSION};

    public TablexiaCopyrightTest() {
        super(PATHS, EXTENSIONS, EXCLUDED_FILES);
    }
}
