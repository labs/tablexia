/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.test.games;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.robbery.RobberyGame;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.screen.statistics.StatisticsGameScoreResolver;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;


/**
 * Created by danilov on 11/11/15.
 */
public class AverageScoreTest {

    private List<Float>     averageScores;

    @Before
    public void tearUp() {
        User user = new User(1, "name", 15, GenderDefinition.FEMALE, "avatar", "signature", false, false, false);
        List<Game> games = new ArrayList<Game>();
        String key = RobberyGame.SCORE_KEY_PERSON_NUMBER;

        for (int i = 0; i < 3; i++) {
            List<GameScore> scores = new CopyOnWriteArrayList<GameScore>();
            scores.add(new GameScore(key, (i * 10) + ""));
            TablexiaRandom random = new TablexiaRandom();
            Game game = new Game((long)i,
                                 user,
                                 GameDifficulty.EASY.getDifficultyNumber(),
                                 GameDefinition.ROBBERY.getGameNumber(),
                                 random.getSeed(),
                                 0l,
                                 1l,
                                 TablexiaSettings.LocaleDefinition.cs_CZ.getLocaleNumber(),
                                 "Tablexia AverageScoreTest",
                                 1,
                                 "Tablexia AverageScoreTest",
                                 1,
                                 TablexiaSettings.BuildType.DEBUG.getId(),
                                 TablexiaSettings.Platform.DESKTOP.getId(),
                                 "Serial",
                                 Game.BACK_COMPATIBILITY_321_RANK_SYSTEM_VERSION_CODE,
                                 1,
                                 UserRankManager.UserRank.getInitialRank().getId(),
                                 15
            );
            game.setGameScoreMap(scores);
            games.add(game);
        }

       StatisticsGameScoreResolver statisticsGameScoreResolver = new StatisticsGameScoreResolver(GameDefinition.ROBBERY);
       averageScores = statisticsGameScoreResolver.getDailyAverageScores(games);
    }

    @Test
    public void getAverageScoreTest() {
        Assert.assertEquals(10, averageScores.get(0), 0);
    }

    @Test
    public void getPointsSize() {
        Assert.assertEquals(1, averageScores.size(), 0);
    }

    @After
    public void tearDown() {
        averageScores = null;
    }
}
