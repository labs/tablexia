/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.test.games;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.ranksystem.RankProcessingScript;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;

/**
 * Created by drahomir on 8/17/16.
 */
public class RankSystemTest {

    private static long gameIdCounter = 0;

    public static List<GameScore> getGameScoreList(GameDefinition gameDefinition, GameDifficulty gameDifficulty, AbstractTablexiaGame.GameResult gameResult, Integer gameScoreResolverVersion) {
        return gameDefinition.getGameResultResolver(gameScoreResolverVersion).getExampleScoreForGameResult(gameDifficulty.getDifficultyDefinition(), gameResult.getGameResultDefinition());
    }

    public static Game createGame(User user, GameDefinition gameDefinition, GameDifficulty gameDifficulty, AbstractTablexiaGame.GameResult gameResult) {
        //TODO - rank system version code
        Game game = new Game(gameIdCounter++, user, gameDifficulty.getDifficultyNumber(), gameDefinition.getGameNumber(), 0L, 1L, 2L, 0, null, 0, null, 1, TablexiaSettings.BuildType.DEBUG.getId(), TablexiaSettings.Platform.DESKTOP.getId(), "-- NONE --", 0, 1, UserRankManager.UserRank.getInitialRank().getId(), 15);
        game.setGameScoreMap(getGameScoreList(gameDefinition, gameDifficulty, gameResult, game.getGameScoreResolverVersion()));
        return game;
    }

    @Test
    public void test() {
        User user = new User(0, "RankSystemTest", 21, GenderDefinition.FEMALE, null, null, false, false, false);

        UserRankManager.initialize();
        UserRankManager.RankComputeData data = RankProcessingScript.calculateUsersRank(user, new RankProcessingScript.RankGamesRetriever() {
            @Override
            public List<Game> getGames(User user, RankProcessingScript script) {
                List<Game> games = new ArrayList<Game>();
                /* NO RANK */

                //PRE 3.3 GAMES
                if(script == RankProcessingScript.BEFORE_3_3) {

                    games.add(createGame(user, GameDefinition.ROBBERY,         GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.PURSUIT,         GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.KIDNAPPING,      GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.NIGHT_WATCH,     GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SHOOTING_RANGE,  GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.EASY, AbstractTablexiaGame.GameResult.THREE_STAR));
                    /* RANK I */

                    games.add(createGame(user, GameDefinition.ROBBERY,         GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.PURSUIT,         GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.KIDNAPPING,      GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.NIGHT_WATCH,     GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SHOOTING_RANGE,  GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    /* RANK II */

                    games.add(createGame(user, GameDefinition.ROBBERY,         GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.PURSUIT,         GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.KIDNAPPING,      GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.NIGHT_WATCH,     GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SHOOTING_RANGE,  GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    //Two games of 'in the darkness' to make sure that experience points stay at 0 after rank up
                    games.add(createGame(user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.ONE_STAR));
                    games.add(createGame(user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    /* RANK III - Ranked up, all progress is at 0... hopefully...*/

                    //This games is not possible. Crime Scene wasn't available in the version described by RankProcessingScript, so rank calculating scripts should skip that.
                    games.add(createGame(user, GameDefinition.CRIME_SCENE,     GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.TWO_STAR));
                }
                if(script == RankProcessingScript.V3_3) {
                    //These are V3.3 games

                    //Extra games for newly added games (crime scene and runes)
                    games.add(createGame(user, GameDefinition.RUNES,           GameDifficulty.HARD,   AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.CRIME_SCENE,     GameDifficulty.HARD,   AbstractTablexiaGame.GameResult.THREE_STAR));

                    games.add(createGame(user, GameDefinition.ROBBERY,         GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.PURSUIT,         GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.KIDNAPPING,      GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.NIGHT_WATCH,     GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SHOOTING_RANGE,  GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.RUNES,           GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.CRIME_SCENE,     GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));

                    games.add(createGame(user, GameDefinition.ROBBERY,         GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.PURSUIT,         GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    //Two games of 'kidnapping' to make sure that experience points stay at 0 after rank up
                    games.add(createGame(user, GameDefinition.KIDNAPPING,      GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.KIDNAPPING,      GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.NIGHT_WATCH,     GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SHOOTING_RANGE,  GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.RUNES,           GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.CRIME_SCENE,     GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    /* RANK IV */
                }

                if(script == RankProcessingScript.V3_5) {
                    //These are V3.5 games

                    //Extra games for newly added games (protocol, safe, on the trail, memory game, attention game)
                    games.add(createGame(user, GameDefinition.PROTOCOL,        GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SAFE,            GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.ON_THE_TRAIL,    GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.MEMORY_GAME,     GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.ATTENTION_GAME,  GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));

                    games.add(createGame(user, GameDefinition.PROTOCOL,        GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SAFE,            GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.ON_THE_TRAIL,    GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.MEMORY_GAME,     GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.ATTENTION_GAME,  GameDifficulty.MEDIUM, AbstractTablexiaGame.GameResult.THREE_STAR));

                    games.add(createGame(user, GameDefinition.ROBBERY,         GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.PURSUIT,         GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.KIDNAPPING,      GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.NIGHT_WATCH,     GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SHOOTING_RANGE,  GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.RUNES,           GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.CRIME_SCENE,     GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.PROTOCOL,        GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SAFE,            GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.ON_THE_TRAIL,    GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.MEMORY_GAME,     GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.ATTENTION_GAME,  GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));

                    games.add(createGame(user, GameDefinition.ROBBERY,         GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.PURSUIT,         GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.KIDNAPPING,      GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.NIGHT_WATCH,     GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SHOOTING_RANGE,  GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.IN_THE_DARKNESS, GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    //Two games of 'runes' to make sure that experience points stay at 0 after rank up
                    games.add(createGame(user, GameDefinition.RUNES,           GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.RUNES,           GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.CRIME_SCENE,     GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.PROTOCOL,        GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.SAFE,            GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.ON_THE_TRAIL,    GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.MEMORY_GAME,     GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));
                    games.add(createGame(user, GameDefinition.ATTENTION_GAME,  GameDifficulty.HARD, AbstractTablexiaGame.GameResult.THREE_STAR));

                    /* RANK V */
                }

                return games;
            }
        });

        UserRankManager.UserRank currRank = data.getCurrentRank();
        UserRankManager.UserRank nextRank = data.getNextRank();

        //RANK CHECK
        Assert.assertEquals(UserRankManager.UserRank.RANK_V, currRank);
        Assert.assertEquals(UserRankManager.UserRank.RANK_VI, nextRank);

        System.out.println("Checking Rank Progress For all Games! All games should be at 0 progress!");

        //Robbery
        UserRankManager.RankProgress robberyRankProgress = data.getRankProgress(GameDefinition.ROBBERY.getGameNumber());
        System.out.println("Robbery: " + robberyRankProgress);
        checkRankProgress(robberyRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //Pursuit
        UserRankManager.RankProgress pursuitRankProgress = data.getRankProgress(GameDefinition.PURSUIT.getGameNumber());
        System.out.println("Pursuit: " + pursuitRankProgress);
        checkRankProgress(pursuitRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //Kidnapping
        UserRankManager.RankProgress kidnappingRankProgress = data.getRankProgress(GameDefinition.KIDNAPPING.getGameNumber());
        System.out.println("Kidnapping: " + kidnappingRankProgress);
        checkRankProgress(kidnappingRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //NightWatch
        UserRankManager.RankProgress nightWatchRankProgress    = data.getRankProgress(GameDefinition.NIGHT_WATCH.getGameNumber());
        System.out.println("Night Watch: " + nightWatchRankProgress);
        checkRankProgress(nightWatchRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //ShootingRange
        UserRankManager.RankProgress shootingRangeRankProgress = data.getRankProgress(GameDefinition.SHOOTING_RANGE.getGameNumber());
        System.out.println("Shooting Range: " + shootingRangeRankProgress);
        checkRankProgress(shootingRangeRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //InTheDarkness
        UserRankManager.RankProgress inTheDarknessRankProgress = data.getRankProgress(GameDefinition.IN_THE_DARKNESS.getGameNumber());
        System.out.println("In The Darkness: " + inTheDarknessRankProgress);
        checkRankProgress(inTheDarknessRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //Runes
        UserRankManager.RankProgress runesRankProgress = data.getRankProgress(GameDefinition.RUNES.getGameNumber());
        System.out.println("Runes: " + runesRankProgress);
        checkRankProgress(runesRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //Crime Scene
        UserRankManager.RankProgress crimeSceneRankProgress = data.getRankProgress(GameDefinition.CRIME_SCENE.getGameNumber());
        System.out.println("CrimeScene: " + crimeSceneRankProgress);
        checkRankProgress(crimeSceneRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //Protocol
        UserRankManager.RankProgress protocolRankProgress = data.getRankProgress(GameDefinition.PROTOCOL.getGameNumber());
        System.out.println("Protocol: " + protocolRankProgress);
        checkRankProgress(protocolRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //Safe
        UserRankManager.RankProgress safeRankProgress = data.getRankProgress(GameDefinition.SAFE.getGameNumber());
        System.out.println("Safe: " + safeRankProgress);
        checkRankProgress(safeRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //On The Trail
        UserRankManager.RankProgress onTheTrailRankProgress = data.getRankProgress(GameDefinition.ON_THE_TRAIL.getGameNumber());
        System.out.println("On The Trail: " + onTheTrailRankProgress);
        checkRankProgress(onTheTrailRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //Memory Game
        UserRankManager.RankProgress memoryGameRankProgress = data.getRankProgress(GameDefinition.MEMORY_GAME.getGameNumber());
        System.out.println("Memory Game: " + memoryGameRankProgress);
        checkRankProgress(memoryGameRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());

        //Attention Game
        UserRankManager.RankProgress attentionGameRankProgress = data.getRankProgress(GameDefinition.ATTENTION_GAME.getGameNumber());
        System.out.println("Attention Game: " + attentionGameRankProgress);
        checkRankProgress(attentionGameRankProgress, currRank.getExperiencePointsNeeded(), currRank.getExperiencePointsNeeded(), nextRank.getExperiencePointsNeeded());
    }

    public static void checkRankProgress(UserRankManager.RankProgress rankProgress, int min, int curr, int next) {
        Assert.assertEquals(rankProgress.getMinXP(), min);
        Assert.assertEquals(rankProgress.getCurrXP(), curr);
        Assert.assertEquals(rankProgress.getNextXP(), next);
    }
}