/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.test.games.runes;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.games.runes.actors.RuneInstance;
import cz.nic.tablexia.game.games.runes.actors.TargetPlate;
import cz.nic.tablexia.game.games.runes.assets.RuneDefinition;
import cz.nic.tablexia.game.games.runes.helper.RuneDescription;
import cz.nic.tablexia.game.games.runes.model.RunesDifficultyDefinition;
import cz.nic.tablexia.game.games.runes.model.RunesGameProperties;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RunesGeneratorTest {

    private static final int TIMEOUT_LIMIT = 100;

    private TablexiaRandom tablexiaRandom;

    @Rule
    public Timeout generatorTimeout = new Timeout(TIMEOUT_LIMIT);

    @Before
    public void tearUp() {
        tablexiaRandom = new TablexiaRandom();
    }

    @Test
    public void testTargetIsPresentOnce() {
        for (RunesDifficultyDefinition runesDifficultyDefinition : RunesDifficultyDefinition.values()) {
            for (int i = 0; i < RunesGameProperties.GAME_ROUNDS; i++) {
                List<RuneDescription> runeDescriptions = TargetPlate.generateNewTargets(i, runesDifficultyDefinition, tablexiaRandom);
                Set<RuneDescription> runeDescriptionSet = new LinkedHashSet<>();
                for (RuneDescription runeDescription : runeDescriptions) {
                    assertTrue("Element is present more than once", runeDescriptionSet.add(runeDescription));
                }
            }
        }
    }

    @Test
    public void testNoCorrectRuneInWrongOnes() {
        for (RunesDifficultyDefinition runesDifficultyDefinition : RunesDifficultyDefinition.values()) {
            for (int round = 0; round < RunesGameProperties.GAME_ROUNDS; round++) {
                List<RuneDescription> targets = TargetPlate.generateNewTargets(round, runesDifficultyDefinition, tablexiaRandom);
                List<RuneDescription> wrongRuneDescriptions = RuneInstance.getSimilarRuneInstances(targets, targets, tablexiaRandom, RunesGameProperties.RUNES_COUNT[round] - RunesGameProperties.RUNES_TO_FIND[round], runesDifficultyDefinition.isFlipRunes());
                for (RuneDescription targetRuneDescription : targets) {
                    assertFalse("Target rune description is in wrong runes", wrongRuneDescriptions.contains(targetRuneDescription));
                }
            }
        }
    }

    @Test
    public void testRandomRuneDefinitionGenerator() {
        LinkedHashSet<RuneDefinition> runeDefinitions = new LinkedHashSet<>();
        while (runeDefinitions.size() < RuneDefinition.ALL_RUNES.length) {
            assertTrue("Set already contains newly generated rune definition", runeDefinitions.add(RuneDefinition.getRandomRuneDefinition(RuneDefinition.ALL_RUNES, runeDefinitions, tablexiaRandom)));
        }
    }

}
