/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.test.games.runes;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.games.runes.actors.HorizontalRunesHolder;
import cz.nic.tablexia.game.games.runes.actors.RuneInstance;
import cz.nic.tablexia.game.games.runes.actors.RunesHolder;
import cz.nic.tablexia.game.games.runes.actors.TargetPlate;
import cz.nic.tablexia.game.games.runes.actors.VerticalRunesHolder;
import cz.nic.tablexia.game.games.runes.helper.HolderManager;
import cz.nic.tablexia.game.games.runes.helper.PositionDefinition;
import cz.nic.tablexia.game.games.runes.helper.PositionManager;
import cz.nic.tablexia.game.games.runes.helper.RuneDescription;
import cz.nic.tablexia.game.games.runes.model.RunesDifficultyDefinition;
import cz.nic.tablexia.game.games.runes.model.RunesGameProperties;

import static org.junit.Assert.assertTrue;

public class RunesGameFlowTest {


    private TablexiaRandom tablexiaRandom;


    @Before
    public void tearUp() {
        tablexiaRandom = new TablexiaRandom();
    }

    @Test
    public void testGameFlow() {
        for (RunesDifficultyDefinition runesDifficultyDefinition : RunesDifficultyDefinition.values()) {
            HolderManager holderManager = new HolderManager(runesDifficultyDefinition, tablexiaRandom);
            PositionManager positionManager = new PositionManager(runesDifficultyDefinition);
            for (int round = 0; round < RunesGameProperties.GAME_ROUNDS; round++) {
                List<PositionDefinition> newPositions = positionManager.getRoundPositions(round, tablexiaRandom);
                List<RuneDescription> targets = TargetPlate.generateNewTargets(round, runesDifficultyDefinition, tablexiaRandom);
                assertTrue("Wrong targets count! Should be " + RunesGameProperties.RUNES_TO_FIND[round] + ", but there are " + targets.size(), targets.size() == RunesGameProperties.RUNES_TO_FIND[round]);

                int groupsCount = runesDifficultyDefinition.getGroupsCount()[round];
                for (int i = 0; i < groupsCount; i++) {
                    holderManager.getHolders().add(newPositions.get(i).isVertical() ? new VerticalRunesHolder(targets, runesDifficultyDefinition) : new HorizontalRunesHolder(targets, runesDifficultyDefinition));
                }

                for (int i = 0; i < holderManager.getHolders().size(); i++) {
                    RunesHolder holder = holderManager.getHolders().get(i);
                    PositionDefinition positionDefinition = newPositions.get(i);
                    holder.setMaxEntities(PositionDefinition.getMaxChildrenCount(runesDifficultyDefinition, positionDefinition));
                }

                holderManager.updateHolders(targets, round);
                for (RunesHolder runesHolder : holderManager.getHolders()) {
                    runesHolder.setWrongRunes(RuneInstance.getSimilarRuneInstances(runesHolder.getHolderTargets(), runesHolder.getRoundTargets(), tablexiaRandom, runesHolder.getWrongRunesCount(), runesDifficultyDefinition.isFlipRunes()));
                }

                boolean temp = holderManager.getTotalRunesCount() == RunesGameProperties.RUNES_COUNT[round];
                assertTrue("Wrong runes count on stage! Should be: " + RunesGameProperties.RUNES_COUNT[round] + ", but there are:" + holderManager.getTotalRunesCount() + ". Diff:" + runesDifficultyDefinition.getGameDifficulty().name() + " Round: " + round, temp);
                ArrayList<RuneDescription> allRunesOnStage = new ArrayList();
                for (RunesHolder holder : holderManager.getHolders()) {
                    allRunesOnStage.addAll(holder.getAllHolderRunes());
                }
                for (RuneDescription targetDescription : targets) {
                    int cnt = 0;
                    for (RuneDescription runeDescription : allRunesOnStage) {
                        if (runeDescription.getRuneDefinition().equals(targetDescription.getRuneDefinition()) && runesDifficultyDefinition.isFlipRunes() == runeDescription.isFlipped())
                            cnt++;
                    }
                    assertTrue("Target is present " + cnt + " times. Should be 1" + ". Diff:" + runesDifficultyDefinition.getGameDifficulty().name() + " Round: " + round, cnt == 1);
                }
                holderManager.clearHolders();
            }
        }
    }

}
