/*
 *
 *  * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.test.games;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.crime_scene.gameobject.GameObjectType;
import cz.nic.tablexia.game.games.crime_scene.model.LevelDefinition;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;

public class CrimeScenePositionsTest {

    @Test
    public void testPositions() {
        for (LevelDefinition.Level level : LevelDefinition.Level.values()){
            List<GameObjectType> objects = LevelDefinition.getGameObjectsByLevel(level);
            for (GameObjectType gameObjectType : objects){
                Position position = LevelDefinition.getGameObjectsPosition(level, gameObjectType);
                Assert.assertNotNull(position);
            }
        }
    }

}
