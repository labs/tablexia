/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.test.games.kidnapping;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.kidnapping.Properties;
import cz.nic.tablexia.game.games.kidnapping.generator.PathGenerator;
import cz.nic.tablexia.game.games.kidnapping.model.Direction;
import cz.nic.tablexia.game.games.kidnapping.model.GameState;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;
import cz.nic.tablexia.game.games.kidnapping.util.DirectionsHelper;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Created by lhoracek on 11/25/15.
 */
public class PathGeneratorTest {

    private static final int ROUNDS = 100;

    private GameState getGameState(Long seed, GameDifficulty difficulty) {
        TablexiaRandom random = new TablexiaRandom(seed);
        GameState gs = GameState.GameStateFactory.createInstance(null, random, difficulty, TablexiaSettings.LocaleDefinition.cs_CZ.getLocale(), Properties.MAP_WIDTH, Properties.MAP_HEIGHT, Properties.GAME_STOPS, Properties.MAP_WIDTH / 2, Properties.MAP_HEIGHT / 2);
        return gs;
    }

    /**
     * Check next directions based on "from direction" - generated direction should not offer returning direction
     */
    @Test
    public void testGetNextDirections() {
        for (Direction from : Direction.values()) {
            Direction[] directions = DirectionsHelper.getNextDirections(from);
            assertFalse(Arrays.asList(directions).contains(from));
        }
    }

    /**
     * Random direction should not go back to "from direction"
     */
    @Test
    public void testGetRandomDirection(){
        Random randomSeed = new Random();
        TablexiaRandom random = new TablexiaRandom(randomSeed.nextLong());
        PathGenerator pathGenerator = new PathGenerator(random);
        for(Direction direction : Direction.values()){
            for (int i = 0; i < ROUNDS; i++) {
                Direction randomDirection = pathGenerator.getRandomDirection(direction);
                assertFalse("Direction " + direction.name() + " -> randomDirection " + randomDirection.name() + " seed " + random.getSeed(), randomDirection.equals(direction) );
            }
        }
    }

    /**
     * Path must never go back to previous direction "from direction "
     */
    @Test
    public void testPathIsNotGoingBack() {
        Random randomSeed = new Random();
        for (GameDifficulty difficulty : GameDifficulty.getVisibleGameDifficultyList()) {
            for (int i = 0; i < ROUNDS; i++) {
                Long seed = randomSeed.nextLong();
                GameState gs = getGameState(seed, difficulty);
                List<Position> path = gs.getPath();
                for (int s = 0; s < path.size() - 1; s++) {
                    Direction dir = DirectionsHelper.getNextDirection(path, s);
                    Direction dirOpossite = dir.getOppositeDirection();
                    Direction nextDir = DirectionsHelper.getNextDirection(path, s + 1);
                    assertTrue("Checking seed " + seed + " step " + s + " -> " + gs.toString(), !dirOpossite.equals(nextDir));
                }
            }
        }
    }

    /**
     * Check if i ask for last direction that it is opposite to direction of n-1 step
     */
    @Test
    public void testLastDirectionIsCorrect() {
        Random randomSeed = new Random();
        for (GameDifficulty difficulty : GameDifficulty.getVisibleGameDifficultyList()) {
            for (int i = 0; i < ROUNDS; i++) {
                Long seed = randomSeed.nextLong();
                GameState gs = getGameState(seed, difficulty);
                List<Position> path = gs.getPath();
                for (int s = 0; s < path.size() - 1; s++) {
                    Direction dir = DirectionsHelper.getNextDirection(path, s);
                    Direction lastDirFrom = DirectionsHelper.getLastDirectionFrom(path, s + 1);
                    assertTrue("Checking seed " + seed + " step " + s + " -> " + gs.toString(), dir.getOppositeDirection().equals(lastDirFrom));
                }
            }
        }
    }


    /**
     * Test that generated last direction is  not going back
     */
    @Test
    public void testStartDirection() {
        Random randomSeed = new Random();
        for (GameDifficulty difficulty : GameDifficulty.getVisibleGameDifficultyList()) {
            for (int i = 0; i < ROUNDS; i++) {
                Long seed = randomSeed.nextLong();
                GameState gs = getGameState(seed, difficulty);
                gs.setStep(0);
                Direction lastFromDir = gs.getLastDirectionFrom();
                Direction nextDirectionDir = gs.getNextDirection();
                assertTrue("Checking seed " + seed + " start step -> " + gs.toString(), !lastFromDir.equals(nextDirectionDir));
            }
        }
    }

    /**
     * Check that start direction is not going back
     */
    @Test
    public void testEndDirection() {
        Random randomSeed = new Random();
        for (GameDifficulty difficulty : GameDifficulty.getVisibleGameDifficultyList()) {
            for (int i = 0; i < ROUNDS; i++) {
                Long seed = randomSeed.nextLong();
                GameState gs = getGameState(seed, difficulty);
                gs.setStep(gs.getPath().size() - 1);
                Direction lastDir = gs.getNextDirection();
                gs.setStep(gs.getPath().size() - 2);
                Direction beforeLastDir = gs.getNextDirection();
                assertTrue("Checking seed " + seed + " last step -> " + gs.toString(), !lastDir.getOppositeDirection().equals(beforeLastDir));
            }
        }
    }
}
