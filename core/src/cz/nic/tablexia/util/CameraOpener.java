/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util;

import com.badlogic.gdx.graphics.Pixmap;

import cz.nic.tablexia.loader.application.ApplicationAvatarManager;

public abstract class CameraOpener {
    
    private OnPhotoTakenListener onPhotoTakenListener;
    
    public interface OnPhotoTakenListener{
        void onPhotoTaken(Pixmap pixmap, boolean success);
    }
    
    public void openCamera(){
        onCameraOpen();
    }
    
    
    public void closeCamera(){
        onCameraClose();
    }

    public void setOnPhotoTakenListener(OnPhotoTakenListener onPhotoTakenListener) {
        this.onPhotoTakenListener = onPhotoTakenListener;
    }

    abstract public void onCameraOpen();
    
    abstract public void onCameraClose();
    
    abstract public boolean cameraAccessible();
    
    abstract public void deleteTemporaryFile();
    
    public void onPhotoTaken(Pixmap pixmap, boolean success){
        if (onPhotoTakenListener != null){
            onPhotoTakenListener.onPhotoTaken(
                    success ? ApplicationAvatarManager.getInstance().createAvatarPixmap(pixmap) : null,
                    success
            );
        }
    }
}
