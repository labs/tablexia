/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util;

import com.badlogic.gdx.audio.Sound;

import cz.nic.tablexia.TablexiaSettings;

/**
 * Created by drahomir on 5/16/16.
 */
public class TablexiaSound implements Sound {
    private static final float DEFAULT_VOLUME = 1f;
    private static final float DEFAULT_PITCH  = 1f;
    private static final float DEFAULT_PAN    = 0f;

    private Sound sound;

    public TablexiaSound(Sound sound) {
        this.sound = sound;
    }

    @Override
    public long play() {
        return play(DEFAULT_VOLUME);
    }

    @Override
    public long play(float volume) {
        return play(volume, DEFAULT_PITCH, DEFAULT_PAN);
    }

    @Override
    public long play(float volume, float pitch, float pan) {
        if(TablexiaSettings.getInstance().isSoundMuted())
            return -1;
        else
            return sound.play(volume * TablexiaSettings.getInstance().getSoundLevel(), pitch, pan);
    }

    @Override
    public long loop() {
        return loop(DEFAULT_VOLUME, DEFAULT_PITCH, DEFAULT_PAN);
    }

    @Override
    public long loop(float volume) {
        return loop(volume, DEFAULT_PITCH, DEFAULT_PAN);
    }

    @Override
    public long loop(float volume, float pitch, float pan) {
        if(!TablexiaSettings.getInstance().isSoundMuted())
            return sound.loop(volume, pitch, pan);
        else
            return -1;
    }

    @Override
    public void stop() {
        sound.stop();
    }

    @Override
    public void pause() {
        sound.pause();
    }

    @Override
    public void resume() {
        sound.resume();
    }

    @Override
    public void dispose() {
        sound.dispose();
    }

    @Override
    public void stop(long soundId) {
        sound.stop(soundId);
    }

    @Override
    public void pause(long soundId) {
        sound.pause(soundId);
    }

    @Override
    public void resume(long soundId) {
        if(!TablexiaSettings.getInstance().isSoundMuted())
            sound.resume(soundId);
    }

    @Override
    public void setLooping(long soundId, boolean looping) {
        sound.setLooping(soundId, looping);
    }

    @Override
    public void setPitch(long soundId, float pitch) {
        sound.setPitch(soundId, pitch);
    }

    @Override
    public void setVolume(long soundId, float volume) {
        sound.setVolume(soundId, volume);
    }

    @Override
    public void setPan(long soundId, float pan, float volume) {
        sound.setPan(soundId, pan, volume);
    }
}