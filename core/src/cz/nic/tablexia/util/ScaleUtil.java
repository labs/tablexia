/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.util.ui.TablexiaGreyScaleImage;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;

/**
 * Created by lhoracek on 4/7/15.
 */
public class ScaleUtil {

    public static Image createImageSizePosition(Texture texture, float width, float height, float x, float y) {
        Image image = createImageSize(texture, width, height);
        image.setPosition(x, y);
        return image;
    }

    public static Image createImageSizePosition(TextureRegion textureRegion, float width, float height, float x, float y) {
        Image image = createImageSize(textureRegion, width, height);
        image.setPosition(x, y);
        return image;
    }

    public static Image createImageWidthPosition(Texture texture, float width, float x, float y) {
        Image image = createImageToWidth(texture, width);
        image.setPosition(x, y);
        return image;
    }

    public static Image createImageWidthPosition(TextureRegion textureRegion, float width, float x, float y) {
        Image image = createImageToWidth(textureRegion, width);
        image.setPosition(x, y);
        return image;
    }

    public static Image createImageHeightPosition(Texture texture, float height, float x, float y) {
        Image image = createImageToHeight(texture, height);
        image.setPosition(x, y);
        return image;
    }

    public static Image createImageHeightPosition(TextureRegion textureRegion, float height, float x, float y) {
        Image image = createImageToHeight(textureRegion, height);
        image.setPosition(x, y);
        return image;
    }

    public static Image createImageFitInto(Texture texture, float width, float height) {
        Image image = new Image(texture);
        if (getWidth(texture.getWidth(), texture.getHeight(), height) > width) {
            image.setSize(width, getHeight(texture.getWidth(), texture.getHeight(), width));
        } else {
            image.setSize(getWidth(texture.getWidth(), texture.getHeight(), height), height);
        }
        return image;
    }

    public static Image createImageFitInto(TextureRegion textureRegion, float width, float height) {
        Image image = new Image(textureRegion);
        if (getWidth(textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), height) > width) {
            image.setSize(width, getHeight(textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), width));
        } else {
            image.setSize(getWidth(textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), height), height);
        }
        return image;
    }

    /** Function for scaling images to fit desired size
     *
     * If the width or height of the image is larger then target size, it is set to this size
     * and the other side is modified by rules:
     *
     * If the width of the image is larger than it's height, the other side is divided by ratio
     * If the width of the image is smaller than it's height, the other side is multiplied by ratio
     * */
    public static Image createImageScaledToFitInto(TextureRegion textureRegion, float widthToFit, float heightToFit) {
        Image image = new Image(textureRegion);
        float ratio = (float)textureRegion.getRegionWidth() / (float)textureRegion.getRegionHeight();

        if (widthToFit < textureRegion.getRegionWidth()) {
            float newWidth = widthToFit;
            float newHeight = ratio > 1 ? widthToFit / ratio : widthToFit * ratio;
            image.setSize(newWidth, newHeight);
        }

        if (heightToFit < image.getHeight()) {
            float newWidth = ratio > 1 ? heightToFit / ratio : heightToFit * ratio;
            float newHeight = heightToFit;
            image.setSize(newWidth, newHeight);
        }
        return image;
    }

    public static Image createImageSize(Texture texture, float width, float height) {
        Image image = new Image(texture);
        image.setSize(width, height);
        return image;
    }

    public static Image createImageSize(TextureRegion texture, float width, float height) {
        Image image = new Image(texture);
        image.setSize(width, height);
        return image;
    }

    public static Image createImageToWidth(Texture texture, float width) {
        Image image = new Image(texture);
        image.setSize(width, getHeight(texture.getWidth(), texture.getHeight(), width));
        return image;
    }

    public static Image createImageToWidth(TextureRegion textureRegion, float width) {
        Image image = new Image(textureRegion);
        image.setSize(width, getHeight(textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), width));
        return image;
    }

    public static TablexiaGreyScaleImage createGreyScaleImageToWidth(TextureRegion textureRegion, float width) {
        TablexiaGreyScaleImage image = new TablexiaGreyScaleImage(textureRegion);
        image.setSize(width, getHeight(textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), width));
        return image;
    }

    public static Image createImageToWidthWithPosition(TextureRegion textureRegion, float width, float positionX, float positionY) {
        Image image = createImageToWidth(textureRegion, width);
        image.setPosition(positionX, positionY);
        return image;
    }

    public static Image createImageToHeight(Texture texture, float height) {
        Image image = new Image(texture);
        image.setSize(getWidth(texture.getWidth(), texture.getHeight(), height), height);
        return image;
    }

    public static Image createImageToHeight(TextureRegion textureRegion, float height) {
        Image image = new Image(textureRegion);
        image.setSize(getWidth(textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), height), height);
        return image;
    }

    public static TablexiaGreyScaleImage createGreyScaleImageToHeight(TextureRegion textureRegion, float height) {
        TablexiaGreyScaleImage image = new TablexiaGreyScaleImage(textureRegion);
        image.setSize(getWidth(textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), height), height);
        return image;
    }

    public static TablexiaNoBlendingImage createNoBlendingImageToHeight(TextureRegion textureRegion, float height) {
        TablexiaNoBlendingImage image = new TablexiaNoBlendingImage(textureRegion);
        setImageHeight(image, height);
        return image;
    }

    public static TablexiaNoBlendingImage createNoBlendingImageToWidth(TextureRegion textureRegion, float width) {
        TablexiaNoBlendingImage image = new TablexiaNoBlendingImage(textureRegion);
        image.setSize(width, getHeight(textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), width));
        return image;
    }

    public static TablexiaNoBlendingImage createNoBlendingImageToWidthWithPosition(TextureRegion textureRegion, float width, float positionX, float positionY) {
        TablexiaNoBlendingImage image = createNoBlendingImageToWidth(textureRegion, width);
        image.setPosition(positionX, positionY);
        return image;
    }

    public static float getHeight(float origWidth, float origHeight, float targetWidth) {
        return targetWidth / (origWidth / origHeight);
    }

    public static float getWidth(float origWidth, float origHeight, float targetHeight) {
        return targetHeight * (origWidth / origHeight);
    }

    public static Actor setFullScreen(Actor actor, Stage stage) {
        actor.setPosition(0, 0);
        actor.setSize(stage.getWidth(), stage.getHeight());
        return actor;
    }

    public static Actor setFullScene(Actor actor, Stage stage) {
        actor.setBounds(TablexiaSettings.getSceneLeftX(stage), TablexiaSettings.getSceneOuterBottomY(stage), TablexiaSettings.getSceneWidth(stage), TablexiaSettings.getSceneOuterHeight(stage));
        return actor;
    }

    public static Actor setFullInnerScene(Actor actor, Stage stage) {
        actor.setBounds(TablexiaSettings.getSceneLeftX(stage), TablexiaSettings.getSceneInnerBottomY(stage), TablexiaSettings.getSceneWidth(stage), TablexiaSettings.getSceneInnerHeight(stage));
        return actor;
    }

    public static void setImageHeight(Image image, float height) {
        image.setSize(getWidth(image.getWidth(), image.getHeight(), height), height);
    }

    public static void setImageDrawableMinSizeForHeight(Image image, float height) {
        image.getDrawable().setMinHeight(height);
        image.getDrawable().setMinWidth(getWidth(image.getWidth(), image.getHeight(), height));
    }

    public static void setImageDrawableMinSizeForWidth(Image image, float width) {
        image.getDrawable().setMinWidth(width);
        image.getDrawable().setMinHeight(getHeight(image.getWidth(), image.getHeight(), width));
    }

    public static Actor setBackgroundBounds(Actor actor) {
        actor.setPosition(0, -TablexiaSettings.getMinWorldHeight() / 2);
        actor.setSize(TablexiaSettings.getWorldSize(), TablexiaSettings.getMinWorldHeight() * 2);
        return actor;
    }

    public static Actor setFullscreenBounds(Actor actor, Stage stage) {
        actor.setBounds(TablexiaSettings.getViewportLeftX(stage), TablexiaSettings.getViewportBottomY(stage), stage.getWidth(), stage.getHeight()); // scaling viewport camera y-position adjustment
        return actor;
    }
}
