/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.listener;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import cz.nic.tablexia.util.Log;

/**
 * Created by lhoracek on 4/10/15.
 */
public class DragActorListener extends InputListener {
    protected float grabX, grabY, startX, startY;
    protected final Actor             actor;
    private       boolean             bounceBack;
    private       DragActorCallback dragListener;
    private       boolean             logging;

    public DragActorListener(Actor actor) {
        this.actor = actor;
    }

    public DragActorListener(Actor actor, boolean bounceBack) {
        this.actor = actor;
        this.bounceBack = bounceBack;
    }

    public DragActorListener(Actor actor, boolean bounceBack, DragActorCallback dragDropListener) {
        this(actor, bounceBack);
        this.dragListener = dragDropListener;
    }


    public void setDragListener(DragActorCallback dragListener) {
        this.dragListener = dragListener;
    }

    private void moveBack() {
        if (dragListener != null) {
            dragListener.onMoveBackStart(startX, startY);
        }

        actor.addAction(Actions.sequence(Actions.moveTo(startX, startY, 0.5f, Interpolation.elasticOut), Actions.run(new Runnable() {
            @Override
            public void run() {
                if (dragListener != null) {
                    dragListener.onMoveBackEnded(startX, startY);
                }
            }
        })));
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if(button == Input.Buttons.RIGHT) return false;
        super.touchDown(event, x, y, pointer, button);
        if (pointer == 0 && button == 0) {
            grabX = x;
            grabY = y;
            startX = actor.getX();
            startY = actor.getY();
            event.stop();

            if (dragListener != null) {
                dragListener.onStarted(startX, startY);
            }
            return true;
        }
        return false;
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        super.touchDragged(event, x, y, pointer);
        if (pointer == 0) {
            float bx = actor.getX() + (x - grabX) * actor.getScaleX();
            float by = actor.getY() + (y - grabY) * actor.getScaleY();
            actor.setPosition(bx, by);
            if (actor.getDebug()) { // TODO enable logging switch
//                Log.info(((Object) this).getClass().getName(), "Position " + bx + "x" + by);
            }
            event.stop();
        }
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (pointer == 0 && button == 0) {
            super.touchUp(event, x, y, pointer, button);
			if (dragListener != null) {
				dragListener.onDropped(actor.getX(), actor.getY());
			}

			Log.debug(this.getClass(), "Actor x: " + actor.getX() + ", Actor y: " + actor.getY());

            if (bounceBack) {
                moveBack();
            }
            event.stop();
        }
    }

    public interface DragActorCallback {
        void onDropped(float x, float y);
        void onStarted(float x, float y);
        void onMoveBackStart(float x, float y);
        void onMoveBackEnded(float x, float y);
    }

    public static class DragListenerAdapter implements DragActorCallback {

        @Override
        public void onDropped(float x, float y) {

        }

        @Override
        public void onStarted(float x, float y) {

        }

        @Override
        public void onMoveBackStart(float x, float y) {

        }

        @Override
        public void onMoveBackEnded(float x, float y) {

        }
    }

    public boolean isLogging() {
        return logging;
    }

    public void setLogging(boolean logging) {
        this.logging = logging;
    }
}
