/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.listener;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.LinkedHashMap;

import cz.nic.tablexia.game.games.pursuit.action.RotateAndMovePieceInPosition;
import cz.nic.tablexia.game.games.pursuit.action.RotatePieceToClosestAngle;
import cz.nic.tablexia.game.games.pursuit.helper.ArithmeticsHelper;
import cz.nic.tablexia.game.games.pursuit.model.Grid;
import cz.nic.tablexia.game.games.pursuit.model.PuzzlePiece;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.util.entity.Touch;

/**
 * Created by Vaclav Tarantik on 6/18/15.
 */
public class DragAndRotateActorListener extends InputListener {
    private static final int DRAGGED_PIECE_ZINDEX = 25;
    public static final float ROTATION_ANIMATION_DURATION = 0.3f;
    public static final float MOVETO_ANIMATION_DURATION = 0.2f;
    private float grabX, grabY;
    private float               initialFingersAngle;
    private Actor               draggedActor;
    private Actor               parentActor;
    private IOnRotationFinished iOnRotationFinished;
    private float               actorOriginalRotation;
    private boolean             performingAction;
    private boolean             pieceAnimationDone;
    private Touch               lastTouch, newTouchDown;
    private boolean             rotatedWithDoubleTap;

    private RotateAndMovePieceInPosition pieceTranslationAction;

    private LinkedHashMap<Integer, Point> activePointers;
    
    public DragAndRotateActorListener(IOnRotationFinished iOnRotationFinished) {
        this.iOnRotationFinished = iOnRotationFinished;
        activePointers = new LinkedHashMap<Integer, Point>();
        pieceAnimationDone = true;
    }

    public void selected(float x, float y) {
    }

    public void moved(float x, float y, Actor draggedPiece) {
    }

    public void dropped(float x, float y, Actor draggedPiece) {
    }

	public void rotated(float angle, Actor rotatedPiece) {
	}

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if (pointer==0) { //one finger
            newTouchDown = new Touch(((Grid) parentActor).pieceAtPoint(x, y), TimeUtils.millis()); //save touched piece and time
            if (lastTouch != null && newTouchDown.getTime() - lastTouch.getTime() > Touch.TIME_BETWEEN_TAPS) //check if new touch is out of the time boundaries
                lastTouch = null;
        }
        if (button != 1) {
            activePointers.put(pointer, new Point(x, y));
        }
        if (activePointers.size() == 1) {
            selected(x, y);
        }
        if (draggedActor != null && !performingAction) {
            //mouse right button clicked
            if (button == 1) {
                draggedActor.setRotation((draggedActor.getRotation() + 90) % 360);
				rotated((draggedActor.getRotation() + 90), draggedActor);
            }
            if (activePointers.size() == 1) {
                draggedActor.setZIndex(DRAGGED_PIECE_ZINDEX);
                draggedActor.setScale(1.2f);
                Vector2 recalculatedCoords = new Vector2(x, y);
                draggedActor.stageToLocalCoordinates(recalculatedCoords);
                grabX = recalculatedCoords.x;
                grabY = recalculatedCoords.y;

            } else if (activePointers.size() == 2) {
                initialFingersAngle = (float) ArithmeticsHelper.getAngleBetweenTwoPoints(activePointers.get(activePointers.keySet().toArray()[0]), activePointers.get(activePointers.keySet().toArray()[1]));
                actorOriginalRotation = draggedActor.getRotation();
                Log.info(getClass(), "Actors initial angle: " + actorOriginalRotation);
            }
        }
        return true;
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        super.touchDragged(event, x, y, pointer);
        if (draggedActor != null && !performingAction) {
            if (activePointers.size() == 1) {
                //updating current pointer position
                activePointers.put(pointer, new Point(x, y));
                Vector2 recalculatedCoords = new Vector2(x, y);
                draggedActor.stageToLocalCoordinates(recalculatedCoords);
                float bx = draggedActor.getX() + (recalculatedCoords.x - grabX);
                float by = draggedActor.getY() + (recalculatedCoords.y - grabY);
                draggedActor.setPosition(bx, by);
                moved(bx, by, draggedActor);
            } else if (activePointers.size() > 1) {
                //not updating the first finger coordinates when rotating so the piece does not move
                activePointers.put(pointer, new Point(x, y));
                Point firstFingerCoordinates = activePointers.get(activePointers.keySet().toArray()[0]);
                Point secondFingerCoordinates = activePointers.get(activePointers.keySet().toArray()[1]);
                double angle = ArithmeticsHelper.getAngleBetweenTwoPoints(firstFingerCoordinates, secondFingerCoordinates);
				float actorRotation = draggedActor.getRotation() + (float) (angle - initialFingersAngle);
                draggedActor.setRotation(actorRotation);
                initialFingersAngle = (float) angle;

				rotated(actorRotation - actorOriginalRotation, draggedActor);
            }
        }
    }

    @Override
    public void touchUp(InputEvent event, final float x, final float y, int pointer, int button) {
        if (pointer == 0 && button != 1) { //don't react on right mouse button double click
            Touch newTouchUp = new Touch(((Grid) parentActor).pieceAtPoint(x, y), TimeUtils.millis()); 
            if (newTouchDown != null) { // check if there is record of last touchDown event
                if (isCorrectDoubleTap(newTouchUp, newTouchDown)) { //check if those touches can be doubleTap gesture
                    setRotatedWithDoubleTap(true); // prevent animation canceling on drop event 
                    final Actor actorToRotate = lastTouch.getTouchedActor(); 
                    final float newRotationAngle = ArithmeticsHelper.getClosestRightAngle(actorToRotate.getRotation() - 90);
                    actorToRotate.addAction(Actions.sequence(Actions.rotateTo(newRotationAngle, ROTATION_ANIMATION_DURATION), Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            rotated(newRotationAngle, actorToRotate);
                            dropped(x, y, actorToRotate); // force check if rotated piece isn't completing the map 
                        }
                    })));
                } else {
                    resetLastTouch(newTouchUp, newTouchDown);
                }
            }
        }
        if (button != 1) {
            if (draggedActor != null && !performingAction && activePointers.size()>=1) {
                //storing original fingers positions and deciding which finger was released
                boolean draggingFingerLifted = ((Integer) (activePointers.keySet().toArray()[0]) == pointer);
                boolean rotatingFingerLifted = false;
                if (activePointers.size() > 1) {
                    rotatingFingerLifted = (Integer) activePointers.keySet().toArray()[1] == pointer;
                }
                //removing lifted finger pointer after storing its position
                activePointers.remove(pointer);
                if (activePointers.size() == 0) {
                    //if user removed all fingers before animation finished, piece is stopped and animated back to its original position
                    if (!isPieceAnimationDone()) {
                        if (pieceTranslationAction != null) {
                            pieceTranslationAction.interrupt();
                        }
                        Point pieceOriginalPosition = ((Grid) parentActor).getPieceGridPositionInPixels(((PuzzlePiece) draggedActor).getActualPosition());
                        parentActor.addAction(new RotateAndMovePieceInPosition((PuzzlePiece) draggedActor, actorOriginalRotation, pieceOriginalPosition, iOnRotationFinished));
                    }
                    parentActor.addAction(Actions.after(new Action() {
                        @Override
                        public boolean act(float delta) {
                            dropped(x, y, draggedActor);
                            return true;
                        }
                    }));

                } else if (activePointers.size() > 0) {
                    //if user raised the dragging finger, animate piece under second finger
                    Point currentFirstPointer = activePointers.get(activePointers.keySet().toArray()[0]);
                    if (draggingFingerLifted) {
                        setPieceAnimationDone(false);
                        Vector2 recalculatedCoords = new Vector2(currentFirstPointer.x, currentFirstPointer.y);
                        draggedActor.stageToLocalCoordinates(recalculatedCoords);
                        float bx = draggedActor.getX() + (recalculatedCoords.x - (draggedActor.getWidth() / 2));
                        float by = draggedActor.getY() + (recalculatedCoords.y - (draggedActor.getHeight() / 2));
                        pieceTranslationAction = new RotateAndMovePieceInPosition((PuzzlePiece) draggedActor, actorOriginalRotation, new Point(bx, by), iOnRotationFinished);
						rotated(ArithmeticsHelper.getAngleInRotationDirection(actorOriginalRotation, draggedActor.getRotation()), draggedActor);
                        parentActor.addAction(pieceTranslationAction);
                    } else if (rotatingFingerLifted) {
                        parentActor.addAction(new RotatePieceToClosestAngle((PuzzlePiece) draggedActor, actorOriginalRotation, iOnRotationFinished));
						rotated(ArithmeticsHelper.getAngleInRotationDirection(actorOriginalRotation, draggedActor.getRotation()), draggedActor);
                    }
                }
            } else {
                activePointers.remove(pointer);
            }
        }

    }

    /**
     * Check is new touchDown+touchUp is tap and check if this new tap can be double tap gesture with last recorded tap
     */
    private boolean isCorrectDoubleTap(Touch newTouchUp, Touch newTouchDown) {
        return Touch.isTap(newTouchUp, newTouchDown) && lastTouch != null && Touch.isDoubleTap(newTouchUp, lastTouch);
    }

    /**
     * Resets last correct tap
     */
    private void resetLastTouch(Touch newTouchUp, Touch newTouchDown) {
        setRotatedWithDoubleTap(false);
        lastTouch = new Touch(draggedActor, (newTouchUp.getTime() + newTouchDown.getTime()) / 2);
    }

    public boolean isRotatedWithDoubleTap() {
        return rotatedWithDoubleTap;
    }

    public void setRotatedWithDoubleTap(boolean rotatedWithDoubleTap) {
        this.rotatedWithDoubleTap = rotatedWithDoubleTap;
    }
    
    public interface IOnRotationFinished {
        void onDragAndRotationFinished();
    }

    public void setDraggedActor(Actor actor) {
        this.draggedActor = actor;
    }

    public Actor getDraggedActor() {
        return draggedActor;
    }

    public void setPerformingAction(boolean performingAction) {
        this.performingAction = performingAction;
    }

    public void setParentActor(Actor parentActor) {
        this.parentActor = parentActor;
    }

    public void setPieceAnimationDone(boolean pieceAnimationDone) {
        this.pieceAnimationDone = pieceAnimationDone;
    }

    public boolean isPieceAnimationDone() {
        return pieceAnimationDone;
    }
}
