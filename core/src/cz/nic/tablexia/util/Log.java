/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

import cz.nic.tablexia.TablexiaSentry;

public class Log {
	
	public enum TablexiaLogLevel {
		
		NONE	(Application.LOG_NONE),
		DEBUG	(Application.LOG_DEBUG),
		INFO	(Application.LOG_INFO),
		ERROR	(Application.LOG_ERROR);
		
		private int logLevelValue;

		TablexiaLogLevel(int logLevelValue) {
			this.logLevelValue = logLevelValue;
		}
		
		public int getLogLevelValue() {
			return logLevelValue;
		}
	}
	
	public static void setLoglevel(TablexiaLogLevel logLevel) {
		Gdx.app.setLogLevel(logLevel.getLogLevelValue());
	}
	
	
	/////////////////// DEBUG
	
	public static void debug(Class<?> clazz, String message, Throwable exception) {
		Gdx.app.debug(clazz.getSimpleName(), message, exception);
	}
	
	public static void debug(String tag, String message, Throwable exception) {
		Gdx.app.debug(tag, message, exception);
	}
	
	public static void debug(Class<?> clazz, String message) {
		Gdx.app.debug(clazz.getSimpleName(), message);
	}
	
	public static void debug(String tag, String message) {
		Gdx.app.debug(tag, message);
	}
	
	
	
	/////////////////// INFO
	
	public static void info(Class<?> clazz, String message, Throwable exception) {
		Gdx.app.log(clazz.getSimpleName(), message, exception);
	}
	
	public static void info(String tag, String message, Throwable exception) {
		Gdx.app.log(tag, message, exception);
	}
	
	public static void info(Class<?> clazz, String message) {
		Gdx.app.log(clazz.getSimpleName(), message);
	}
	
	public static void info(String tag, String message) {
		Gdx.app.log(tag, message);
	}
	
	
	
	/////////////////// ERROR
	
	public static void err(Class<?> clazz, String message, Throwable exception) {
		Gdx.app.error(clazz.getSimpleName(), message, exception);
		TablexiaSentry.log(TablexiaLogLevel.ERROR, clazz, message, exception);
	}
	
	public static void err(String tag, String message, Throwable exception) {
		Gdx.app.error(tag, message, exception);
		TablexiaSentry.log(TablexiaLogLevel.ERROR, tag, message, exception);
	}
	
	public static void err(Class<?> clazz, String message) {
		Gdx.app.error(clazz.getSimpleName(), message);
		TablexiaSentry.log(TablexiaLogLevel.ERROR, clazz, message);
	}
	
	public static void err(String tag, String message) {
		Gdx.app.error(tag, message);
		TablexiaSentry.log(TablexiaLogLevel.ERROR, tag, message);
	}

}
