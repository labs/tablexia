/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.qrcode;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import cz.nic.tablexia.util.Log;

/**
 * Created by Vitaliy Vashchenko on 26.1.16.
 */
public class QrCodeEncoder {
    private static final Color DEFAULT_BACKGROUND = Color.WHITE;
    private static final Color DEFAULT_FOREGROUND = Color.BLACK;

    private static final int   DEFAULT_WIDTH      = 256;
    private static final int   DEFAULT_HEIGHT     = DEFAULT_WIDTH;

    public static Texture createQrCode(String data) {
        return createQrCode(data, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_BACKGROUND, DEFAULT_FOREGROUND);
    }

    public static Texture createQrCode(String data, Color background, Color foreground) {
        return createQrCode(data, DEFAULT_WIDTH, DEFAULT_HEIGHT, background, foreground);
    }

    public static Texture createQrCode(String data, int width, int height, Color background, Color foreground) {
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix matrix;
        Texture texture = null;

        try {
            matrix = writer.encode(data, BarcodeFormat.QR_CODE, width, height);
            Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
            pixmap.setColor(background);
            pixmap.fill();
            pixmap.setColor(foreground);
            for(int i = 0; i < matrix.getWidth(); i++){
                for (int j = 0; j< matrix.getHeight(); j++){
                    if(matrix.get(i,j)){
                        pixmap.drawPixel(i,j);
                    }
                }
            }

            texture = new Texture(pixmap);
            pixmap.dispose();
        } catch (WriterException e) {
            Log.err(QrCodeEncoder.class, "Cannot encode QR code!", e);
        }
        
        return texture;
    }
}
