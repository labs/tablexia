/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by drahomir on 5/13/16.
 */
public class TablexiaGreyScaleImage extends Image {
    //Default vertex shader which can be found in SpriteBatch.createDefaultShader() method
    private static String vertexShader =
            "attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
            + "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
            + "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
            + "uniform mat4 u_projTrans;\n" //
            + "varying vec4 v_color;\n" //
            + "varying vec2 v_texCoords;\n" //
            + "\n" //
            + "void main()\n" //
            + "{\n" //
            + "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
            + "   v_color.a = v_color.a * (255.0/254.0);\n" //
            + "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
            + "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
            + "}\n";

    //Edited fragment shader from SpriteBatch.createDefaultShader() method
    private static String fragmentShader =
            "#ifdef GL_ES\n" //
            + "#define LOWP lowp\n" //
            + "precision mediump float;\n" //
            + "#else\n" //
            + "#define LOWP \n" //
            + "#endif\n" //
            + "varying LOWP vec4 v_color;\n" //
            + "varying vec2 v_texCoords;\n" //
            + "uniform sampler2D u_texture;\n" //
            + "void main()\n"//
            + "{\n" //
            + "  vec4 color = v_color * texture2D(u_texture, v_texCoords);\n" //gets actual color
            + "  float grey = dot(color.rgb, vec3(0.299, 0.587, 0.114));\n"   //computes grey value
            + "  gl_FragColor = vec4(grey, grey, grey, color.w);\n"           //outputs grey color (color.w is original transparency)
            + "}";

    private static ShaderProgram greyScaleShader;

    public TablexiaGreyScaleImage(TextureRegion textureRegion) {
        super(textureRegion);

        if(greyScaleShader == null)
            initializeShader();
    }

    private static void initializeShader() {
        greyScaleShader = new ShaderProgram(vertexShader, fragmentShader);
        if(!greyScaleShader.isCompiled()) throw new RuntimeException("Couldn't compile GreyScale shader! Compile log: " + greyScaleShader.getLog());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setShader(greyScaleShader);
            super.draw(batch, parentAlpha);
        batch.setShader(null); //use default shader
    }
}
