/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by lhoracek on 5/20/15.
 */
public class PlayMusicAction extends Action {
    private boolean finished = false;
	private boolean played = false;
    private Music music;

    @Override
    public void reset() {
        super.reset();
        finished = false;
    }

    @Override
    public void restart() {
        super.restart();
        finished = false;
    }

    public Music getMusic() {
        return music;
    }

    public void setMusic(Music music) {
        this.music = music;
        this.music.setOnCompletionListener(new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music music) {
                finished = true;
				played = false;
            }
        });
    }

    @Override
    public boolean act(float delta) {
        Pool pool = getPool();
        setPool(null); // Ensure this action can't be returned to the pool while executing.
        if (!finished && !music.isPlaying() && !played) {
			music.play();
			played = true;
        }
        setPool(pool);
        return finished;
    }

    public static PlayMusicAction getInstance(Music music) {
        final PlayMusicAction action = Actions.action(PlayMusicAction.class);
        action.setMusic(music);
        return action;
    }
}
