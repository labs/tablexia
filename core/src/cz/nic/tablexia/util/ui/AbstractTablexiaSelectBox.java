/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.ArraySelection;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Cullable;
import com.badlogic.gdx.scenes.scene2d.utils.Disableable;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectSet;

import cz.nic.tablexia.loader.application.ApplicationInternalSoundManager;
import cz.nic.tablexia.util.Log;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.removeActor;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Matyáš Latner.
 */
public abstract class AbstractTablexiaSelectBox<T> extends Widget implements Disableable {
	private static final int MIN_SCROLL_DISTANCE_TO_HIDE_OPTIONS	=	24;

    static final Vector2 temp = new Vector2();

    final Array<T> items = new Array();
    final ArraySelection<T> selection = new ArraySelection(items);
    TablexiaSelectBoxList selectBoxList;
    private float prefWidth, prefHeight;
    private ClickListener clickListener;
    boolean disabled;
    boolean checkValidity = true;

    public AbstractTablexiaSelectBox(float itemHeight) {
        setSize(getPrefWidth(), itemHeight);

        selection.setActor(this);
        selection.setRequired(true);

        selectBoxList = new TablexiaSelectBoxList(this, itemHeight);

        addListener(clickListener = new ClickListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                if (pointer == 0 && button != 0) return false;
                if (disabled) return false;
                if (selectBoxList.hasParent()) {
                    hideList();
                } else {
                    ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.BUTTON_CLICKED).play();
                    showList();
                }
                return true;
            }
        });
    }

    /** Set the max number of items to display when the select box is opened. Set to 0 (the default) to display as many as fit in
     * the stage height. */
    public void setMaxListCount (int maxListCount) {
        selectBoxList.maxListCount = maxListCount;
    }

    /** @return Max number of items to display when the box is opened, or <= 0 to display them all. */
    public int getMaxListCount () {
        return selectBoxList.maxListCount;
    }

    protected void setStage (Stage stage) {
        if (stage == null) selectBoxList.hide();
        super.setStage(stage);
    }

    /** Set the backing Array that makes up the choices available in the SelectBox */
    public void setItems (T... newItems) {
        if (newItems == null) throw new IllegalArgumentException("newItems cannot be null.");
        float oldPrefWidth = getPrefWidth();

        items.clear();
        items.addAll(newItems);
        selection.validate();
        selectBoxList.list.setItems(items);

        invalidate();
        if (oldPrefWidth != getPrefWidth()) invalidateHierarchy();
    }

    /** Sets the items visible in the select box. */
    public void setItems (Array<T> newItems) {
        if (newItems == null) throw new IllegalArgumentException("newItems cannot be null.");
        float oldPrefWidth = getPrefWidth();

        items.clear();
        items.addAll(newItems);
        selection.validate();
        selectBoxList.list.setItems(items);

        invalidate();
        if (oldPrefWidth != getPrefWidth()) invalidateHierarchy();
    }

    public TablexiaSelectBoxList getSelectBoxList() {
        return selectBoxList;
    }

    public void clearItems () {
        if (items.size == 0) return;
        items.clear();
        selection.clear();
        invalidateHierarchy();
    }

    /** Returns the internal items array. If modified, {@link #setItems(Array)} must be called to reflect the changes. */
    public Array<T> getItems () {
        return items;
    }

    @Override
    public void layout () {
        prefHeight = getHeight();
        float maxItemWidth = 0;
        prefWidth = maxItemWidth;
        prefWidth = Math.max(prefWidth, maxItemWidth);
    }

    @Override
    public void draw (Batch batch, float parentAlpha) {
        validate();

        Color color = getColor();
        float width = getWidth();
        float height = getHeight();

        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

        T selected = selection.first();
        if (selected != null) {
            drawSelectedItem(batch, parentAlpha, selected, width, height);
        }
    }

    protected abstract void drawSelectedItem(Batch batch, float parentAlpha, T selected, float width, float height);

    protected abstract void drawListItem(Batch batch, float parentAlpha, T item, float x, float y, float width, float height);

    /** Get the set of selected items, useful when multiple items are selected
     * @return a Selection object containing the selected elements */
    public ArraySelection<T> getSelection () {
        return selection;
    }

    /** Returns the first selected item, or null. For multiple selections use {@link AbstractTablexiaSelectBox#getSelection()}. */
    public T getSelected () {
        return selection.first();
    }

    /** Sets the selection to only the passed item, if it is a possible choice, else selects the first item. */
    public void setSelected (T item) {
        if (!isCheckValidity()) {
            selection.set(item);
        }
        else {
            if (items.contains(item, false))
                selection.set(item);
            else if (items.size > 0)
                selection.set(items.first());
            else
                selection.clear();
        }
    }

    /** @return The index of the first selected item. The top item has an index of 0. Nothing selected has an index of -1. */
    public int getSelectedIndex () {
        ObjectSet<T> selected = selection.items();
        return selected.size == 0 ? -1 : items.indexOf(selected.first(), false);
    }

    /** Sets the selection to only the selected index. */
    public void setSelectedIndex (int index) {
        selection.set(items.get(index));
    }

    public void setDisabled (boolean disabled) {
        if (disabled && !this.disabled) hideList();
        this.disabled = disabled;
    }

    public void setCheckValidity(boolean checkValidity) {
        this.checkValidity = checkValidity;
    }

    public boolean isCheckValidity() {
        return checkValidity;
    }

    public boolean isDisabled () {
        return disabled;
    }

    public float getPrefWidth () {
        validate();
        return prefWidth;
    }

    public float getPrefHeight () {
        validate();
        return prefHeight;
    }

    public void showList () {
        if (items.size == 0) return;
        selectBoxList.show(getStage());
    }

    public void hideList () {
        selectBoxList.hide();
    }

    /** Returns the list shown when the select box is open. */
    public TablexiaList getList () {
        return selectBoxList.list;
    }

    /** Returns the scroll pane containing the list that is shown when the select box is open. */
    public ScrollPane getScrollPane () {
        return selectBoxList;
    }

    protected void onShow (Actor selectBoxList, boolean below) {
        selectBoxList.getColor().a = 0;
        selectBoxList.addAction(fadeIn(0.3f, Interpolation.fade));
    }

    protected void onHide (Actor selectBoxList) {
        selectBoxList.getColor().a = 1;
        selectBoxList.addAction(sequence(fadeOut(0.15f, Interpolation.fade), removeActor()));
    }

    /**
     * Override this method if u want to handle list select click on your own
     * @return true if u don't want default behaviour
     */
    public boolean onSelectBoxItemSelected(T item) {
        return false;
    }

    /** @author Nathan Sweet */
    public class TablexiaSelectBoxList extends ScrollPane {
        private final AbstractTablexiaSelectBox<T> selectBox;
        int maxListCount;
        private final Vector2 screenPosition = new Vector2();
        final TablexiaList list;
        private InputListener hideListener;
        private Actor previousScrollFocus;

        public TablexiaSelectBoxList (final AbstractTablexiaSelectBox<T> selectBox, float itemHeight) {
            super(null, new ScrollPaneStyle());
            this.selectBox = selectBox;

            setOverscroll(false, false);
            setFadeScrollBars(false);
            setScrollingDisabled(true, false);

            list = new TablexiaList(itemHeight);
            list.setTouchable(Touchable.disabled);
            setActor(list);

            list.addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    if(!selectBox.onSelectBoxItemSelected(list.getSelected())) {
                        changeToSelected();
                    }
                    hide();
                }

                public boolean mouseMoved (InputEvent event, float x, float y) {
                    list.setSelectedIndex(Math.min(selectBox.items.size - 1, (int)((list.getHeight() - y) / list.getItemHeight())));
                    return true;
                }
            });

            addListener(new InputListener() {
                public void exit (InputEvent event, float x, float y, int pointer, Actor toActor) {
                    if (toActor == null || !isAscendantOf(toActor)) list.selection.set(selectBox.getSelected());
                }
            });

            hideListener = new InputListener() {
                public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                    ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.BUTTON_CLICKED).play();
                    Actor target = event.getTarget();
                    if (isAscendantOf(target)) return false;
                    list.selection.set(selectBox.getSelected());
                    hide();
                    return false;
                }

                public boolean keyDown (InputEvent event, int keycode) {
                    if (keycode == Input.Keys.ESCAPE) hide();
                    return false;
                }
            };
        }

        public void changeToSelected() {
            selectBox.selection.choose(list.getSelected());
        }

        public TablexiaList getList() {
            return list;
        }

        public void show (Stage stage) {
            if (list.isTouchable()) return;

            stage.removeCaptureListener(hideListener);
            stage.addCaptureListener(hideListener);
            stage.addActor(this);

            selectBox.localToStageCoordinates(screenPosition.set(0, 0));

            // Show the list above or below the select box, limited to a number of items and the available height in the stage.
            float itemHeight = list.getItemHeight();
            float height = itemHeight * (maxListCount <= 0 ? selectBox.items.size : Math.min(maxListCount, selectBox.items.size));
            Drawable scrollPaneBackground = getStyle().background;
            if (scrollPaneBackground != null)
                height += scrollPaneBackground.getTopHeight() + scrollPaneBackground.getBottomHeight();

            float heightBelow = screenPosition.y;
            float heightAbove = stage.getCamera().viewportHeight - screenPosition.y - selectBox.getHeight();
            boolean below = true;
            if (height > heightBelow) {
                if (heightAbove > heightBelow) {
                    below = false;
                    height = Math.min(height, heightAbove);
                } else
                    height = heightBelow;
            }

            if (below)
                setY(screenPosition.y - height);
            else
                setY(screenPosition.y + selectBox.getHeight());
            setX(screenPosition.x);
            setHeight(height);
            validate();
            float width = Math.max(getPrefWidth(), selectBox.getWidth());
            if (getPrefHeight() > height) width += getScrollBarWidth();
            if (scrollPaneBackground != null) {
                // Assume left and right padding are the same, so right padding can include a shadow.
                width += Math.max(0, scrollPaneBackground.getRightWidth() - scrollPaneBackground.getLeftWidth());
            }
            setWidth(width);

            validate();
            scrollTo(0, list.getHeight() - selectBox.getSelectedIndex() * itemHeight - itemHeight / 2, 0, 0, true, true);
            updateVisualScroll();

            previousScrollFocus = null;
            Actor actor = stage.getScrollFocus();
            if (actor != null && !actor.isDescendantOf(this)) previousScrollFocus = actor;
            stage.setScrollFocus(this);

            list.selection.set(selectBox.getSelected());
            list.setTouchable(Touchable.enabled);
            clearActions();
            selectBox.onShow(this, below);
        }

        public void hide () {
            if (!list.isTouchable() || !hasParent()) return;
            list.setTouchable(Touchable.disabled);

            Stage stage = getStage();
            if (stage != null) {
                stage.removeCaptureListener(hideListener);
                if (previousScrollFocus != null && previousScrollFocus.getStage() == null) previousScrollFocus = null;
                Actor actor = stage.getScrollFocus();
                if (actor == null || isAscendantOf(actor)) stage.setScrollFocus(previousScrollFocus);
            }

            clearActions();
            selectBox.onHide(this);
        }

        public void draw (Batch batch, float parentAlpha) {
            selectBox.localToStageCoordinates(temp.set(0, 0));
            if (getX() != temp.x) setX(temp.x);
            
            super.draw(batch, parentAlpha);
        }

        public void act (float delta) {
            super.act(delta);
            toFront();
        }
    }

    public class TablexiaList extends Widget implements Cullable {
        private final Array<T> items = new Array();
        final ArraySelection<T> selection = new ArraySelection(items);
        private Rectangle cullingArea;
        private float prefWidth, prefHeight;
        private float itemHeight;

        public TablexiaList (float itemHeight) {
            this.itemHeight = itemHeight;
            selection.setActor(this);
            selection.setRequired(true);

            setSize(getPrefWidth(), getPrefHeight());

            addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (pointer == 0 && button != 0) return false;
                    if (selection.isDisabled()) return false;
                    TablexiaList.this.touchDown(y);
                    return true;
                }
            });
        }

        void touchDown (float y) {
            if (items.size == 0) return;
            float height = getHeight();
            int index = (int)((height - y) / itemHeight);
            index = Math.max(0, index);
            index = Math.min(items.size - 1, index);
            selection.choose(items.get(index));
        }

        public void layout () {
            prefWidth = 0;
            prefHeight = items.size * itemHeight;
        }

        @Override
        public void draw (Batch batch, float parentAlpha) {
            validate();
            float itemY = getHeight();

            for (int i = 0; i < items.size; i++) {
                if (cullingArea == null || (itemY - itemHeight <= cullingArea.y + cullingArea.height && itemY >= cullingArea.y)) {
                    drawListItem(batch, parentAlpha, items.get(i), 0, getY() + itemY - itemHeight, getWidth(), itemHeight);
                } else if (itemY < cullingArea.y) {
                    break;
                }
                itemY -= itemHeight;
            }
        }

        public ArraySelection<T> getSelection () {
            return selection;
        }

        /** Returns the first selected item, or null. */
        public T getSelected () {
            return selection.first();
        }

        /** Sets the selection to only the passed item, if it is a possible choice. */
        public void setSelected (T item) {
            if (items.contains(item, false))
                selection.set(item);
            else if (selection.getRequired() && items.size > 0)
                selection.set(items.first());
            else
                selection.clear();
        }

        /** @return The index of the first selected item. The top item has an index of 0. Nothing selected has an index of -1. */
        public int getSelectedIndex () {
            ObjectSet<T> selected = selection.items();
            return selected.size == 0 ? -1 : items.indexOf(selected.first(), false);
        }

        /** Sets the selection to only the selected index. */
        public void setSelectedIndex (int index) {
            if (index < -1 || index >= items.size)
                throw new IllegalArgumentException("index must be >= -1 and < " + items.size + ": " + index);
            if (index == -1) {
                selection.clear();
            } else {
                selection.set(items.get(index));
            }
        }

        public void setItems (T... newItems) {
            if (newItems == null) throw new IllegalArgumentException("newItems cannot be null.");
            float oldPrefWidth = getPrefWidth(), oldPrefHeight = getPrefHeight();

            items.clear();
            items.addAll(newItems);
            selection.validate();

            invalidate();
            if (oldPrefWidth != getPrefWidth() || oldPrefHeight != getPrefHeight()) invalidateHierarchy();
        }

        /** Sets the items visible in the list, clearing the selection if it is no longer valid. If a selection is
         * {@link ArraySelection#getRequired()}, the first item is selected. */
        public void setItems (Array newItems) {
            if (newItems == null) throw new IllegalArgumentException("newItems cannot be null.");
            float oldPrefWidth = getPrefWidth(), oldPrefHeight = getPrefHeight();

            items.clear();
            items.addAll(newItems);
            selection.validate();

            invalidate();
            if (oldPrefWidth != getPrefWidth() || oldPrefHeight != getPrefHeight()) invalidateHierarchy();
        }

        public void clearItems () {
            if (items.size == 0) return;
            items.clear();
            selection.clear();
            invalidateHierarchy();
        }

        /** Returns the internal items array. If modified, {@link #setItems(Array)} must be called to reflect the changes. */
        public Array<T> getItems () {
            return items;
        }

        public float getItemHeight () {
            return itemHeight;
        }

        public float getPrefWidth () {
            validate();
            return prefWidth;
        }

        public float getPrefHeight () {
            validate();
            return prefHeight;
        }

        public void setCullingArea (Rectangle cullingArea) {
            this.cullingArea = cullingArea;
        }
    }
}
