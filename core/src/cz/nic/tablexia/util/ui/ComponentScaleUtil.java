/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

/**
 * Created by frantisek on 19.11.15.
 */
public class ComponentScaleUtil {

    private static final float REFERENCE_DENSITY = 0.6f;
    private static final float REFERENCE_WIDTH = 900 / REFERENCE_DENSITY;
    private static final float SCALE_DIFFERENCE_LIMIT = 3f;
    private static final float BIGGER_SCALE = 1.3f;
    private static final float FONT_BIGGER_SCALE = 1.5f;

    public static float scaleSize(float size, ScaleProvider provider) {
        return size * provider.getScale();
    }

    public static float scaleFontSize(float size, ScaleProvider provider) {
        return size * provider.getFontScale();
    }

    public static boolean isUnderThreshold() {
        float actualWidth = Gdx.app.getType() == Application.ApplicationType.iOS ? Gdx.app.getGraphics().getBackBufferHeight() / Gdx.app.getGraphics().getDensity() : Gdx.app.getGraphics().getHeight() / Gdx.app.getGraphics().getDensity();
        float actualScale = REFERENCE_WIDTH / actualWidth;

        if (actualScale >= SCALE_DIFFERENCE_LIMIT) {
            return true;
        }

        return false;
    }

    public static void scaleActor(Actor actor, ScaleProvider provider) {
        float oldWidth = actor.getWidth();
        float oldHeight = actor.getHeight();

        float newWidth = scaleSize(oldWidth, provider);
        float newHeight = scaleSize(oldHeight, provider);

        actor.setWidth(newWidth);
        actor.setHeight(newHeight);

        // we move actor to be in same center position as before scaling
        actor.moveBy(((oldWidth - newWidth) / 2), ((oldHeight - newHeight) / 2));
    }

    public static void scaleCell(Cell cell, ScaleProvider provider) {
        float oldWidth = cell.getMinWidth();
        float oldHeight = cell.getMinHeight();

        float newWidth = scaleSize(oldWidth, provider);
        float newHeight = scaleSize(oldHeight, provider);

        cell.width(newWidth);
        cell.height(newHeight);
    }

    public static void scaleButton(TextButton button, ScaleProvider provider) {
        scaleActor(button, provider);
        scaleLabel(button.getLabel(), provider);
        if (button.getParent() instanceof Table) {
            ((Table) button.getParent()).getCell(button).width(button.getWidth()).height(button.getHeight());
        }
    }

    public static void scaleLabel(Label label, ScaleProvider provider) {
        label.setFontScale(scaleFontSize(label.getFontScaleX(), provider));
    }

    public static void scaleTable(Table table, ScaleProvider provider) {
        for (Cell cell : table.getCells()) {
            if (cell.getActor() instanceof Label) {
                scaleLabel((Label) cell.getActor(), provider);
            } else if (cell.getActor() instanceof Group) {
                scaleGroup((Group) cell.getActor(), provider);
                cell.height(cell.getActor().getHeight());
                cell.width(cell.getActor().getWidth());
            } else {
                scaleActor(cell.getActor(), provider);
                cell.height(cell.getActor().getHeight());
                cell.width(cell.getActor().getWidth());
            }
        }
    }

    public static void scaleGroup(Group group, ScaleProvider provider) {
        float oldWidth = group.getWidth();
        float oldHeight = group.getHeight();

        for (Actor actor : group.getChildren()) {
            if (actor instanceof Label) {
                scaleLabel((Label) actor, provider);
            } else if (actor instanceof Group) {
                scaleGroup((Group) actor, provider);
            } else {
                scaleActor(actor, provider);
            }
        }

        if (group instanceof WidgetGroup) {
            ((WidgetGroup) group).pack();
        }

        float newWidth = group.getWidth();
        float newHeight = group.getHeight();

        group.moveBy(oldWidth - newWidth, oldHeight - newHeight);
    }

    public static void scaleDialog(Dialog dialog, ScaleProvider provider) {
        scaleActor(dialog, provider);
        scaleTable(dialog.getContentTable(), provider);
    }

//    public static void scaleDialog(TablexiaComponentDialog dialog, ScaleProvider provider) {
//        float dialogOldX = dialog.getX();
//        float dialogOldY = dialog.getY();
//
//        scaleDialog((Dialog)dialog, provider);
//
//        float dialogNewX = dialog.getX();
//        float dialogNewY = dialog.getY();
//
//        for (Dialog child : dialog.getChildDialogs()) {
//            scaleDialog(child, provider);
//
//            float newX;
//            float newY;
//
//            // child dialog is on right side of parent dialog
//            if (dialog.getX() < child.getX()) {
//                newX = dialogOldX - dialogNewX;
//            } else {
//                newX = dialogNewX - dialogOldX;
//            }
//
//            // dialog is below one third of parent dialog
//            if (dialog.getY() + dialog.getHeight() / 3 > child.getY()) {
//                newY = dialogNewY - dialogOldY;
//            } else {
//                newY = dialogOldY - dialogNewY;
//            }
//
//            // we move child dialog to be in same center position as before scaling
//            child.moveBy(newX / 2, newY / 2);
//        }
//
//        if (dialog instanceof DynamicBubbleDialog) {
//            DynamicBubbleDialog dynamicDialog = ((DynamicBubbleDialog) dialog);
//            scaleActor(dynamicDialog.getDialogBottomImage(), provider);
//            dynamicDialog.refreshDialogPosition();
//        }
//    }

    public static class DefaultScale implements ScaleProvider {

        public float getScale() {
            return isUnderThreshold() ? BIGGER_SCALE : 1;
        }

        public float getFontScale() {
            return isUnderThreshold() ? FONT_BIGGER_SCALE : 1;
        }
    }

    public static class DynamicScale implements ScaleProvider {

        public float getScale() {
            return isUnderThreshold() ? BIGGER_SCALE : 1 / BIGGER_SCALE;
        }

        public float getFontScale() {
            return isUnderThreshold() ? FONT_BIGGER_SCALE : 1 / FONT_BIGGER_SCALE;
        }
    }

    public interface ScaleProvider {
        float getScale();
        float getFontScale();
    }
}
