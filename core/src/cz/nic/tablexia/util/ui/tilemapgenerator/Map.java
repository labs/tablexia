/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.tilemapgenerator;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;

import cz.nic.tablexia.util.listener.DragActorListener;

/**
 * Created by lhoracek on 5/19/15.
 */
public class Map extends Group {

    protected Group           tiles;
    private DragActorListener debugListener = new DragActorListener(tiles);

    public Map(Group tiles) {
        this.tiles = tiles;
        addActor(tiles);
    }

    public void draw(Batch batch, float parentAlpha) {
        clipBegin();
        super.draw(batch, parentAlpha);
        clipEnd();
    }

    public void setMapScale(float scaleXY) {
        tiles.setScale(scaleXY);
    }

    public float getMapScale() {
        if (tiles.getScaleY() != tiles.getScaleX()) {
            throw new IllegalStateException("Different scale");
        }
        return tiles.getScaleX();
    }

    public Group getTiles() {
        return tiles;
    }

    @Override
    public void setDebug(boolean enabled) {
        super.setDebug(enabled);
        if (enabled) {
            tiles.addListener(debugListener);
        } else {
            tiles.removeListener(debugListener);
        }
    }

 }