/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.tilemapgenerator;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;

/**
 * Created by lhoracek on 9/2/15.
 */
public class ParentCullingGroup extends Group {

    private int tileBaseWidth;
    private int tileBaseHeight;

    public ParentCullingGroup(int tileBaseWidth, int tileBaseHeight) {
        this.tileBaseWidth = tileBaseWidth;
        this.tileBaseHeight = tileBaseHeight;
    }

    @Override
    protected void positionChanged() {
        super.positionChanged();
        Vector2 start = new Vector2(0, 0);
        parentToLocalCoordinates(start);
        Vector2 end = new Vector2(getParent().getWidth(), getParent().getHeight());
        parentToLocalCoordinates(end);

        setCullingArea(new Rectangle(start.x - (tileBaseWidth * 2), start.y - (tileBaseHeight * 2), end.x - start.x + (tileBaseWidth * 4), end.y - start.y + (tileBaseHeight * 4)));
    }
}
