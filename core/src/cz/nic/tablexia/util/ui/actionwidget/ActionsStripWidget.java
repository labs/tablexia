/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.actionwidget;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.ui.actionwidget.Action.ActionListener;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Widget for showing selected actions list
 * 
 * @author Matyáš Latner
 *
 */
public class ActionsStripWidget extends Group implements ActionListener {
	public enum StartButtonControl {
		IS_NOT_EMPTY, IS_FULL
	}

	public 	static final String						EVENT_SCROLL_FINISHED			= "scroll finished";

	public  static final Interpolation 				ACTION_MOVE_INTERPOLATION 			= Interpolation.swingOut;
	
	private static final float 						ACTION_DETACH_WIDTH_RATIO 				= 0.5f;
	private static final int 						ACTUAL_POINTER_WIDTH 					= 50;
	private static final int 						ACTUAL_POINTER_HEIGHT 					= 30;
	private static final float 						ACTUAL_POINTER_POSITION_X_OFFSET_RATIO 	= 9f / 10;

	protected static final String 					SELECTED_ACTIONS_STRING_SEPARATOR 		= ",";

	public  static final float  					ANIMATION_DURATION      			= 0.4f;

	private static boolean 							actionsEnabled 	= true;
	private boolean 								scrollEnabled  	= true;

	private Group 									scrollPane;
	private Group 									overlay;
	private Group 									nextActionField;
	private Image									actualPointer;

	private TablexiaButton 							startButton;
	private StartButtonControl buttonState;
	
	private CopyOnWriteArrayList<ActionContainer> 	selectedActions;
	private float 									nextActionFieldActualPosition;
	private float 									actionOffsetX;
	private boolean 								startButtonControl;
	private boolean 								enableActionSorting;

	private Action                      			draggedAction;
	private Float									initialScrollPosition;
	private AbstractTablexiaGame 					tablexiaGame;

	private int 									actualCollisionNumber;

	final private int								actionSizeBigger;
	final private int 								actionSizeSmaller;
	final private float								animationDuration;

	final private int 								actionOffset;
	final private int 								leftActionOffset;
	final private String							controlActualPath, controlNextPath;
	private int 									maximumActionsCount;


	public ActionsStripWidget(Float initialScrollPosition, String selectedActionsString, AbstractTablexiaGame tablexiaGame, int actionSizeBigger, int actionSizeSmaller, float animationDuration, String controlActualPath, String controlNextPath, int maximumActionsCount, StartButtonControl buttonState) {
		this.initialScrollPosition = initialScrollPosition;
		this.tablexiaGame = tablexiaGame;
		this.actionSizeBigger = actionSizeBigger;
		this.actionSizeSmaller = actionSizeSmaller;
		this.animationDuration = animationDuration;
		this.controlActualPath = controlActualPath;
		this.controlNextPath = controlNextPath;
		this.maximumActionsCount = maximumActionsCount;
		this.buttonState = buttonState;

		actionOffsetX = actionSizeBigger / 8;
		actionOffset = actionSizeSmaller / 10;
		leftActionOffset = 3 * actionOffset;

		enableActionSorting = true;
		startButtonControl = true;

		selectedActions = new CopyOnWriteArrayList<ActionContainer>();
		
		createScrollPane();
		createNextActionField(tablexiaGame);
		createOverlay(tablexiaGame);

		prepareSelectedActions(selectedActionsString, tablexiaGame);
	}

	@Override
	public void setSize(float width, float height) {
		super.setSize(width, height);
		nextActionField.setSize(actionSizeBigger * 2, getHeight());
		moveScrollPaneBottomToAction(0, false, null);
	}

	protected void prepareSelectedActions(String selectedActionsString, AbstractTablexiaGame tablexiaGame) {
	}

	public String getSelectedActionsString() {
		StringBuffer result = new StringBuffer();
		for (ActionContainer selectedActionContainer: selectedActions) {
			if (result.length() > 0) {
				result.append(SELECTED_ACTIONS_STRING_SEPARATOR);
			}
			result.append(selectedActionContainer.getAction().getActionNumber());
		}
		return result.toString();
	}

	public float getScrollPaneActualPosition() {
		return scrollPaneActualPosition;
	}

	public int getActionOffset() {
		return actionOffset;
	}

	public Image getActualPointer() {
		return actualPointer;
	}

	//////////////////////////////////////////// CONTROL
	
	public void enableControl() {
		actionsEnabled = true;
		scrollEnabled  = true;
		for (ActionContainer actionContainer : selectedActions) {
			actionContainer.getAction().enable();
		}
	}
	
	public void disableControl() {
		actionsEnabled = false;
		scrollEnabled  = false;
		for (ActionContainer actionContainer : selectedActions) {
			actionContainer.getAction().disable();
		}
	}

	private void resetScroll() {
		accel0 = 0;
		accel1 = 0;
		accel2 = 0;
		accel3 = 0;
		accel4 = 0;
		accel5 = 0;
	}
	
	public boolean isNotFull() {
		return selectedActions.size() < maximumActionsCount;
	}
	
	public void enableStartButtonControl(boolean startButtonControl) { 
		this.startButtonControl = startButtonControl; 
	}
	
	public void enableActionSorting(boolean enableActionSorting) {
		this.enableActionSorting = enableActionSorting;
	}

	
/* //////////////////////////////////////////// OVERLAY */
	
	private void createOverlay(AbstractTablexiaGame tablexiaGame) {
		actualPointer = new Image(tablexiaGame.getScreenTextureRegion(controlActualPath));
		actualPointer.setSize(ACTUAL_POINTER_WIDTH, ACTUAL_POINTER_HEIGHT);
		actualPointer.setPosition(-(actualPointer.getHeight() * ACTUAL_POINTER_POSITION_X_OFFSET_RATIO), ((actionSizeBigger + actionOffset) / 2) - (actualPointer.getHeight() / 2));
		
		overlay = new Group();
		overlay.addActor(actualPointer);
		addActor(overlay);
	}
	
	private void showOverlay(boolean visible) {
		overlay.setVisible(visible);
	}
	
	
	
//////////////////////////////////////////// SCROLLPANE

	private static final float 	SCROLL_MAX_ACCEL_LIMIT 	= 5000;
	private static final float  SCROLL_FRICTION 		= 0.96f;
	private static final int 	SCROLL_ACCEL_SPEEDUP 	= 7;

	private boolean			scrollPaneTracked			= false;

	private float 			accel0;
	private float 			accel1;
	private float 			accel2;
	private float			accel3;
	private float			accel4;
	private float			accel5;
	private float 			scrollPaneLastPosition;
	private float 			scrollPaneActualPosition;
	private float 			scrollPaneTrackedPosition;

	public boolean isScrollEnabled() {
		return isScreenFilledWithActions() && scrollEnabled;
	}

	protected synchronized void performScrollPanePositionChange() {
		if (isScrollEnabled()) {
			if (accel5 < 0 && accel5 < -SCROLL_MAX_ACCEL_LIMIT)
				accel0 = accel1 = accel5 = - SCROLL_MAX_ACCEL_LIMIT;
			if (accel5 > 0 && accel5 > SCROLL_MAX_ACCEL_LIMIT)
				accel0 = accel1 = accel5 = SCROLL_MAX_ACCEL_LIMIT;

			if (accel5 >= -1 && accel5 <= 1) {
				accel0 = accel1 = accel2 = accel3 = accel4 = accel5 = 0;
				return;
			}

			if (scrollPaneTracked) {
				scrollPaneActualPosition = scrollPaneTrackedPosition;
			} else {
				if (! (Double.isNaN(accel5) || Double.isInfinite(accel5))) {
					scrollPaneTrackedPosition = scrollPaneActualPosition = scrollPaneActualPosition - accel5;
				}
			}
			accel5 = (accel5 * SCROLL_FRICTION);

			// bottom scroll limit
			if (scrollPaneActualPosition > 0) {
				scrollPaneActualPosition = 0;
				accel0 = accel1 = accel2 = accel3 = accel4 = accel5 = 0;
			}
			// top scroll limit
			int actionSize = isNotFull() ? selectedActions.size() + 1 : selectedActions.size();
			float topLimit = getHeight() - ((actionSize * (actionSizeBigger + actionOffset) + actionOffset) + actionOffset);
			if (scrollPaneActualPosition < topLimit) {
				scrollPaneActualPosition = topLimit;
				accel0 = accel1 = accel2 = accel3 = accel4 = accel5 = 0;
			}

			scrollPane.setPosition(0, scrollPaneActualPosition);
		}
	}
	
	private void createScrollPane() {
		scrollPane = new Group() {

			@Override
			public void act(float delta) {
				super.act(delta);
				showVisibleActionContainers(Math.abs(getY()) - (actionSizeBigger + actionOffset), getStage().getHeight() + Math.abs(getY()));
			}

		};
		addActor(scrollPane);

		addListener(new DragListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if(button == Input.Buttons.RIGHT) return false;
				if (isScrollEnabled() && pointer == 0) {
					scrollPaneLastPosition = y;
				}
				scrollPaneTracked = true;
				return super.touchDown(event, x, y, pointer, button);
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				scrollPaneTracked = false;
				super.touchUp(event, x, y, pointer, button);
			}

			@Override
			public void touchDragged(InputEvent event, float x, float y, int pointer) {
				if (isScrollEnabled()) {
					float offsetY = scrollPaneLastPosition - y;
					scrollPaneLastPosition = y;

					accel5 = (accel0 + accel1 + accel2 + accel3 + accel4 + offsetY) / 6;
					accel0 = accel1;
					accel1 = accel2;
					accel3 = accel4;
					accel4 = accel5;

					accel5 = accel5 * SCROLL_ACCEL_SPEEDUP;
					scrollPaneTrackedPosition = scrollPane.getY() - offsetY;
				}
			}

			public boolean scrolled(InputEvent event, float x, float y, int amount) {
				if(isScrollEnabled()){
					scrollPaneTracked = true;
					float selectedActionHeight = selectedActions.size() * (actionSizeBigger + actionOffset);

					float mouseWheelY = Math.min(getHeight(), Math.max(getHeight() * 0.9f, selectedActionHeight * 0.1f) / 4);

					float offsetY = (mouseWheelY / 2) * (amount * -1);

					scrollPaneLastPosition = y;

					accel5 = (accel0 + accel1 + accel2 + accel3 + accel4 + offsetY) / 6;
					accel0 = accel1;
					accel1 = accel2;
					accel3 = accel4;
					accel4 = accel5;

					accel5 = accel5 * SCROLL_ACCEL_SPEEDUP;
					scrollPaneTrackedPosition = scrollPane.getY() - offsetY;

					return true;
				}

				return false;
			}

			@Override
			public void dragStart(InputEvent event, float x, float y, int pointer) {
				super.dragStart(event, x, y, pointer);
			}
		});

		setScrollFocus();
	}

	public void setScrollFocus(){
		if(scrollPane != null) {
			tablexiaGame.getStage().setScrollFocus(scrollPane);
		}
	}
	
	private void moveScrollOffset(float offset, Runnable finishHandler) {
		moveScrollPane(scrollPane.getY() + offset, false, false, finishHandler);
	}
	
	private void moveScrollPaneTopToAction(int position, Runnable finishHandler) {
		moveScrollPane(getScrollPaneTopPosition(position), true, true, finishHandler);
	}

	private void moveScrollPaneBottomToAction(int position, boolean useEaseFunction, Runnable finishHandler) {
		moveScrollPane(getScrollPaneBottomPosition(position), true, useEaseFunction, finishHandler);
	}
	
	private void moveScrollPane(float positionY, boolean animated, boolean useEaseFunction, Runnable finishHandler) {
		scrollPane.clearActions();
		if (animated) {
			MoveToAction moveToAction = moveTo(scrollPane.getX(), positionY, ANIMATION_DURATION, useEaseFunction ? Interpolation.swingOut : Interpolation.linear);
			if (finishHandler != null) {
				RunnableAction runAction = run(finishHandler);
				scrollPane.addAction(sequence(moveToAction, runAction));
			} else {
				scrollPane.addAction(moveToAction);
			}
		} else {
			scrollPane.setPosition(scrollPane.getX(), positionY);
			if (finishHandler != null) {
				finishHandler.run();
			}
		}
		scrollPaneActualPosition = scrollPaneTrackedPosition = positionY;
	}
	
	private float getScrollPaneTopPosition(int position) {
		return getHeight() - (position * (actionSizeBigger + actionOffset) + 2 * actionOffset);
	}
	
	private int getScrollPaneBottomPosition(int position) {
		return -position * (actionSizeBigger + actionOffset);
	}

	
/* //////////////////////////////////////////// NEXT ACTION FIELD */
	
	private void createNextActionField(AbstractTablexiaGame tablexiaGame) {
		nextActionField = new Group();
		nextActionField.setPosition(leftActionOffset, actionOffsetX);

		Image nextActionFieldImage = new Image(tablexiaGame.getScreenTextureRegion(controlNextPath));
		nextActionFieldImage.setSize(actionSizeBigger, actionSizeBigger);
		nextActionField.addActor(nextActionFieldImage);

		moveNextActionFieldPermanently(0, false);
		scrollPane.addActor(nextActionField);
	}
	
	private void moveNextActionFieldPermanently(int position, boolean animated) {
		nextActionFieldActualPosition = actionOffset + getCurrentActionOffset(position);
		moveNextActionField(nextActionFieldActualPosition, animated);
	}
	
	private void resetNextActionTemporalyPosition() {
		moveNextActionField(nextActionFieldActualPosition, true);
	}
	
	private void moveNextActionTemporalyHalfUp() {
		moveNextActionField(nextActionFieldActualPosition + (actionSizeBigger / 2) + actionOffset, true);
	}
	
	private void moveNextActionField(float positionY, boolean animated) {
		nextActionField.setVisible(isNotFull());
		nextActionField.clearActions();
		if (animated) {
			nextActionField.addAction(moveTo(nextActionField.getX(), positionY, ANIMATION_DURATION, ACTION_MOVE_INTERPOLATION));
		} else {
			nextActionField.setPosition(nextActionField.getX(), positionY);
		}
	}



//////////////////////////////////////////// COLLISIONS

	public void setDraggedAction(Action draggedActionArea) {
		this.draggedAction = draggedActionArea;
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		if (draggedAction != null) {

			// check next action field
			if (nextActionField.isVisible()) {
				if (overlaps(draggedAction, nextActionField, false, selectedActions.size())) {
					draggedAction.onCollision(selectedActions.size(), actionSizeBigger, actionSizeSmaller);
					return;
				}
			}

			for (int i = 0; i < selectedActions.size(); i++) {
				ActionContainer actionContainer = selectedActions.get(i);
				if (actionContainer.isVisible()) {

					// check planned actions
					Action action = actionContainer.getAction();
					if (!draggedAction.equals(action) && overlaps(draggedAction, action, true, i)) {
						draggedAction.onCollision(action.getOrderNumber(), actionSizeBigger, actionSizeSmaller);
						actualCollisionNumber = i;
						return;
					}
				}
			}
			actualCollisionNumber = -1;
			draggedAction.onCollision(Action.NO_COLLISION_NUMBER, actionSizeBigger, actionSizeSmaller);
		}
	}

	private boolean overlaps(Actor actor1, Actor actor2, boolean moveReactionArea, int i) {

		Vector2 actor1Coordinates = actor1.localToStageCoordinates(new Vector2(0, 0));
		Vector2 actor2Coordinates = actor2.localToStageCoordinates(new Vector2(0, 0));

		float actor2Y = actor2Coordinates.y;
		float actor2Height = actor2.getHeight();
		if (moveReactionArea) {
			actor2Y = i == actualCollisionNumber ? actor2Coordinates.y - actor2.getHeight() : actor2Coordinates.y - (actor2.getHeight() * 1f/8);
			actor2Height = i == actualCollisionNumber ?  actor2.getHeight() * 1.2f : actor2.getHeight() / 4;
			Image actor = new Image(ApplicationAtlasManager.getInstance().getColorTextureRegion(Color.BLUE));
			actor.setBounds(actor2Coordinates.x, actor2Y, actor2.getWidth(), actor2Height);
		}

		return actor1Coordinates.x < actor2Coordinates.x + (actor2.getWidth() * actor2.getScaleX())
				&& actor1Coordinates.x + (actor1.getWidth() * actor1.getScaleX()) > actor2Coordinates.x
				&& actor1Coordinates.y < actor2Y + (actor2Height * actor2.getScaleY())
				&& actor1Coordinates.y + (actor1.getHeight() * actor1.getScaleY()) > actor2Y;
	}
	
	public void performCollisionWithNumberStart(int collisionNumber) {
		if (isNotFull() && enableActionSorting) {
			if (collisionNumber > 0 && collisionNumber < selectedActions.size()) {
				moveNextActionTemporalyHalfUp();
				moveActionsFromPositionHalfDown(collisionNumber);
				moveActionsFromPositionHalfUp(collisionNumber);
				selectedActions.get(collisionNumber).setReactionRectangleOffset();
			} else if (collisionNumber == selectedActions.size()) {
				resetNextActionTemporalyPosition();
				resetSelectedActionsPositions();
			}
		}
	}
	
	public void performCollisionWithNumberFinish(int collisionNumber) {
		resetNextActionTemporalyPosition();
		resetSelectedActionsPositions();
		
		if (collisionNumber < selectedActions.size() && collisionNumber > 0) {
			selectedActions.get(collisionNumber).resetReactionRectangle(true);
		}
	}

	
/* //////////////////////////////////////////// START BUTTON */
	
	public void setStartButton(TablexiaButton startButton) {
		this.startButton = startButton;
	}
	
	private void enableStartButton() {
		if (startButton != null && !startButton.isEnabled()) {
			startButton.setEnabled(true);
		}
	}
	
	private void disableStartButton() {
		if (startButton != null && startButton.isEnabled()) {
			startButton.setEnabled(false);
		}
	}
	
	public void setStartButtonState() {
		if (startButtonControl) {
			if(buttonState == StartButtonControl.IS_NOT_EMPTY) {
				if (selectedActions.size() > 0) {
					enableStartButton();
				} else {
					disableStartButton();
				}
			}else if (buttonState == StartButtonControl.IS_FULL){
				if(isNotFull()){
					disableStartButton();
				}else {
					enableStartButton();
				}
			}
		}
	}
	
	
/* //////////////////////////////////////////// ACTIONS */
	
	private void moveActionsFromPositionHalfUp(int actionNumber) {
		for (int i = actionNumber; i < selectedActions.size(); i++) {
			selectedActions.get(i).moveTemporalyHalfUp(true);
		}
	}
	
	private void moveActionsFromPositionHalfDown(int actionNumber) {
		for (int i = actionNumber - 1; i >= 0; i--) {
			selectedActions.get(i).moveTemporalyHalfDown(true);
		}
	}
	
	private void resetSelectedActionsPositions() {
		for (ActionContainer actionContainer : selectedActions) {
			actionContainer.resetPosition(true);
		}
	}

	public void addSelectedAction(String texturePath, int selectedActionPosition, AbstractTablexiaGame tablexiaGame, int actionNumber) {
		if (isNotFull()) {
			// reset collision animation
			if (selectedActionPosition < selectedActions.size()) {
				selectedActions.get(selectedActionPosition).resetReactionRectangle(false);
			}
			
			// add new action
			ActionContainer actionContainer = createActionContainer(setUpAction(createAction(texturePath, selectedActionPosition, tablexiaGame, actionNumber)));
			actionContainer.getAction().setEnbaled(enableActionSorting);
			if (selectedActionPosition > selectedActions.size() || !enableActionSorting) {
				selectedActions.add(actionContainer);
			} else {
				selectedActions.add(selectedActionPosition, actionContainer);
			}
			scrollPane.addActor(actionContainer);
			
			// reset actions position
			for (int i = 0; i < selectedActions.size(); i++) {
				ActionContainer selectedAction = selectedActions.get(i);
				selectedAction.changeActionNumber(i);
				selectedAction.moveToActionPosition(false, true, this);
			}

			
			// move with scroll pane
			boolean animatedNextActionField = false;
			if (selectedActionPosition >= selectedActions.size() - 1) {
				if (isNotFull()) {
					if(getScrollPaneTopPosition(selectedActionPosition + 2) < 0) {
						moveScrollPaneTopToAction(selectedActionPosition + 2, null);
					}
					animatedNextActionField = true;
				}
			} else {
				moveScrollOffset(-(actionSizeBigger / 2) - actionOffset, null);
				if(getScrollPaneTopPosition(selectedActions.size() + 1) > 0) {
					moveScrollPaneBottomToAction(0, true, null);
				}
			}
			
			// move next action field
			moveNextActionFieldPermanently(selectedActions.size(), animatedNextActionField);
			
			setStartButtonState();
		}
	}

	private void showVisibleActionContainers(float bottomPositionY, float topPositionY) {
		for (ActionContainer actionContainer: selectedActions) {
			actionContainer.setVisible(actionContainer.getY() > bottomPositionY && actionContainer.getY() < topPositionY);
		}
	}

	public void removeActionAndUpdateView(int orderNumber){
		removeAction(orderNumber);
		resetSelectedActionsPositions();
	}
	
	public void removeAction(int orderNumber) {
		for(com.badlogic.gdx.scenes.scene2d.Action action : selectedActions.get(orderNumber).getActions()) {
			selectedActions.get(orderNumber).removeAction(action);
		}
		selectedActions.get(orderNumber).remove();
		for (int i = orderNumber + 1; i < selectedActions.size(); i++) {			
			selectedActions.get(i).movePermanentlyDown(true);
		}

		selectedActions.remove(orderNumber);
		moveNextActionFieldPermanently(selectedActions.size(), true);


		if (scrollPane.getY() < getScrollPaneTopPosition(selectedActions.size() + 1)) {
			if (isScreenFilledWithActions()) {
				moveScrollPaneTopToAction(selectedActions.size() + 1, null);
			} else {				
				moveScrollPaneBottomToAction(0, true, null);
			}
		}

		setStartButtonState();
	}


	public void setMaximumActionsCount(int maximumActionsCount) {
		this.maximumActionsCount = maximumActionsCount;
	}

	public boolean containsAction(Action action){
		if(selectedActions == null)
			return false;

		for(ActionContainer container: selectedActions){
			if(container.getAction().equals(action))
				return true;
		}

		return false;
	}
	
	public List<ActionContainer> getSelectedActions() {
		return selectedActions;
	}
	
	public void displaySelectedActions(Integer actionNumber) {
		showOverlay(false);

		//For Test
		Runnable testEventScrollMenuListener = new Runnable() {
			@Override
			public void run() {
				AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_SCROLL_FINISHED);
			}
		};

		if (initialScrollPosition != null) {
			moveScrollPane(initialScrollPosition, false, false, null);
			initialScrollPosition = null;
		} else {
			int position = 0;
			if (actionNumber != null && isScreenFilledWithActions(selectedActions.size())) {
				position = actionNumber.intValue();
				if (!isScreenFilledWithActions(selectedActions.size() - position)) {
					moveScrollPaneTopToAction(selectedActions.size() + 1,testEventScrollMenuListener);
					return;
				}
			}
			moveScrollPaneBottomToAction(position, true, testEventScrollMenuListener);
		}
	}
	
	public void displayProcessedActions(int actionNumber, boolean useEaseFunction, Runnable finishHandler) {
		showOverlay(true);
		moveScrollPaneBottomToAction(actionNumber, useEaseFunction, finishHandler);
	}
	
	private ActionContainer createActionContainer(Action action) {
		return new ActionContainer(leftActionOffset, action, 0, this, actionSizeBigger, animationDuration);
	}

	protected Action createAction(String texturePath, final int position, AbstractTablexiaGame tablexiaGame, int actionNumber) {
		Action newAction = new StripWidgetCustomAction(texturePath, position, actionSizeBigger, 0, 0, true, tablexiaGame, actionNumber);
		return newAction;
	}

	private Action setUpAction(Action action) {
		action.setActionsStripWidget(this);
		action.addActionListener(this);
		action.setTouchable(enableActionSorting ? Touchable.enabled : Touchable.disabled);
		return action;
	}

	private boolean isScreenFilledWithActions() {
		return isScreenFilledWithActions(selectedActions.size() + 1);
	}

	private boolean isScreenFilledWithActions(int actionCount) {
		return (actionCount * (actionSizeBigger + actionOffset)) > getHeight();
	}

	public float getCurrentActionOffset(int position) {
		return position * (actionSizeBigger + actionOffset);
	}

	public class StripWidgetCustomAction extends Action {
		public StripWidgetCustomAction(String texturePath, int orderNumber, int actionSize, float positionX, float positionY, boolean visible, AbstractTablexiaGame tablexiaGame, int actionNumber) {
			super(texturePath, orderNumber, actionSize, positionX, positionY, visible, tablexiaGame, actionNumber);
		}

		private boolean detached	= false;

		private Float lastPositonX  = null;
		private Float lastPositonY  = null;
		private float detachLimit   = -getWidth() * ACTION_DETACH_WIDTH_RATIO;

		@Override
		protected boolean downAction(float positionX, float positionY, boolean center) {
			if (actionsEnabled) {
				return super.downAction(positionX, positionY, false);
			}

			return false;
		}

		@Override
		protected void upAction(boolean hideWithAnimation) {
			if (detached) {
				// dismiss action
				resetScroll();
				enableControl();
				super.upAction(true);
			} else {
				// return back to action position
				for (ActionContainer actionContainer : selectedActions) {
					actionContainer.getAction().resetPositionX(true);
				}
				setDraggedAction(null);
			}
		}

		@Override
		protected void dragAction(float dragPositionX, float dragPositionY) {
			setVisible(true);
			if (lastPositonX != null && lastPositonY != null) {
				if (detached || getX() + (getWidth() / 2) < detachLimit) {
					// detached
					if (!detached) {
						detached = true;

						disableControl();

						//move to ActionStrip layer and proper position
						Vector2 stageCoordinates = localToStageCoordinates(new Vector2(0, 0));
						setPosition(stageCoordinates.x, stageCoordinates.y);
						ActionsStripWidget.this.addActor(this);

						resizeWithAnimation((float) actionSizeSmaller);
						ActionsStripWidget.this.removeAction(getOrderNumber());
					}
					super.dragAction(dragPositionX, dragPositionY);
				} else {
					// fixated
					if (dragPositionX <= getInitialPositionX() + getWidth()) {
						movePositionX(dragPositionX, false);
					} else {
						resetPositionX(true);
					}
				}
			}
			lastPositonX = dragPositionX;
			lastPositonY = dragPositionY;
		}

		@Override
		public void onCollision(int orderNumber, int actionSizeBigger, int actionSizeSmaller) {
			if (detached) {
				super.onCollision(orderNumber, actionSizeBigger, actionSizeSmaller);
			}
		}

	}
	
/* //////////////////////////////////////////// ACTION LISTENER */
	
	@Override
	public void onActionDrag(Action lastAction) {
		// nothing needed
	}

    @Override
    public void onActionDoubleTap(Action action) {
    }

    @Override
	public void onActionDrop(Action action, int collidesWithNumber) {
		if (collidesWithNumber != Action.NO_COLLISION_NUMBER) {
			addSelectedAction(action.getTexturePath(), collidesWithNumber, tablexiaGame, action.getActionNumber());
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		performScrollPanePositionChange();
		super.draw(batch, parentAlpha);
	}
}
