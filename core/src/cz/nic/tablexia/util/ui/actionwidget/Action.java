/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.actionwidget;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.util.entity.Touch;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Action icon
 * 
 * @author Matyáš Latner
 *
 */
public class Action extends Image {
	
	public 	static final int					NO_COLLISION_NUMBER 			= -1;

	private static final float 					DETACH_ANIM_SCALE_FINISH_VALUE 	= 0.1f;
	private static final float 					DETACH_ANIM_SCALE_DURATION 		= 0.4f;
	private static final float 					DETACH_ANIM_ALPHA_DURATION 		= 0.4f;
	private static final Interpolation.ExpOut 	DETACH_ANIM_SCALE_INTERPOLATION = Interpolation.exp10Out;
	
	private static final float 					ATTACH_ANIM_SCALE_DURATION 		= 0.25f;
	private static final float 					ATTACH_ANIM_SCALE_START_VALUE 	= 0.1f;
	private static final float 					ATTACH_ANIM_SCALE_FINISH_VALUE 	= 1f;
	private static final Interpolation.SwingOut ATTACH_ANIM_SCALE_INTERPOLATION = Interpolation.swingOut;
	private static final float 					ATTACH_ANIM_ALPHA_DURATION 		= 0.25f;
	
	private static final float 					RESIZE_ANIM_SCALE_DURATION 		= 0.3f;
	private static final Interpolation.SwingOut RESIZE_ANIM_SCALE_INTERPOLATION = Interpolation.swingOut;

	public interface ActionListener {
		void onActionDrag(Action action);
		void onActionDrop(Action action, int collidesWithNumber);
		void onActionDoubleTap(Action action);
	}

	private List<ActionListener> actionListeners;
	private int orderNumber;
	
	protected 	boolean clickable 			= false;
	protected 	boolean draggable 			= false;
	private 	int		collidesWithNumber	= NO_COLLISION_NUMBER;

	private Float 				initialPositionX;
	private float 				actualSize;
	private ScaleToAction       actualScaleAction;
	private Rectangle			reactionArea;
	protected ActionsStripWidget  actionsStripWidget;
    private String              texturePath;
	private final int			actionNumber;

	private DragListener dragListener = new DragListener() {

		Touch lastRecordedTouchDown;
		boolean lastTapValid;
		Touch lastRecordedTap;
		
		@Override
		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
			if(button == Input.Buttons.RIGHT) return false;
			lastRecordedTouchDown = new Touch(event.getListenerActor(), System.currentTimeMillis());
			return downAction(x, y, true);
		}

		@Override
		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
			Touch touchUp = new Touch(event.getListenerActor(), System.currentTimeMillis());
			lastTapValid = Touch.isTap(lastRecordedTouchDown, touchUp);
			if (lastTapValid) {
				if (lastRecordedTap == null) {
					lastRecordedTap = touchUp;
				} else if (Touch.isDoubleTap(lastRecordedTap, touchUp)) {
					onDoubleTap();
					lastRecordedTap = null;
				} else {
					lastRecordedTap = touchUp;
				}
			}
			upAction(true);
		}

		@Override
		public void touchDragged(InputEvent event, float x, float y, int pointer) {
			dragAction(x, y);
		}
	};

	 public Action(String texturePath, int orderNumber, int actionSize, float positionX, float positionY, boolean visible, AbstractTablexiaGame tablexiaGame, int actionNumber) {
		super(tablexiaGame.getScreenTextureRegion(texturePath));

		this.orderNumber = orderNumber;
		this.actualSize  = actionSize;
		this.texturePath = texturePath;
		this.actionNumber = actionNumber;

		setBounds(positionX, positionY, actionSize, actionSize);
		setOrigin(Align.center);
		if (visible) {
			draggable = true;
			setVisible(true);
		} else {
			draggable = false;
			setVisible(false);
		}

		setDragListener();
		initialPositionX = getX();
		actionListeners  = new ArrayList<ActionListener>();
	}

	public Rectangle getReactionArea() {
		if (reactionArea == null) {
			reactionArea = new Rectangle(0, 0, getWidth(), getHeight());
		}
		return reactionArea.setPosition(localToStageCoordinates(new Vector2(0, 0)));
	}

	public float getActualSize() {
		return actualSize;
	}

    public String getTexturePath() {
        return texturePath;
    }

	public int getActionNumber() {
		return actionNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public int getOrderNumber() {
		return orderNumber;
	}
	
	public void setActionsStripWidget(ActionsStripWidget actionsStripWidget) {
		this.actionsStripWidget = actionsStripWidget;
	}
	
	public void setInitialPositionX(Float initialPositionX) {
		this.initialPositionX = initialPositionX;
	}
	
	public Float getInitialPositionX() {
		return initialPositionX;
	}
	
	public void resetPositionX(boolean animated) {
		movePositionX(initialPositionX, animated);
	}
	
	public void movePositionX(float positionX, boolean animated) {
		clearActions();
		if (animated) {
			addAction(moveTo(positionX, getY()));
		} else {
			moveBy(positionX - getWidth() / 2, 0);
		}
	}
	
	/* //////////////////////////////////////////// ACTION STATE */
	
	public void setEnbaled(boolean enabled) {
		clickable = enabled;
	}
	
	public void disable() {
		setEnbaled(false);
	}
	
	public void enable() {
		setEnbaled(true);
	}
	
	
	/* //////////////////////////////////////////// CLICK LISTENER */
	
	public void addActionListener(ActionListener actionClickListener) {
		actionListeners.add(actionClickListener);
	}
	
	public void removeActionListener(ActionListener actionClickListener) {
		actionListeners.remove(actionClickListener);
	}

	public void setDragListener() {
		addListener(dragListener);
	}

	public void setClickable() {
		setTouchable(Touchable.enabled);
	}
	
	protected boolean downAction(float positionX, float positionY, boolean center) {
		if (center) {
			moveBy(positionX - getWidth() / 2, positionY - getHeight() / 2);
		}
		for (ActionListener actionListener : actionListeners) {
			actionListener.onActionDrag(Action.this);
		}
		actionsStripWidget.setDraggedAction(this);
		this.setZIndex(100);
		draggable = true;
		return true;
	}
	
	protected void dragAction(float dragPositionX, float dragPositionY) {
		moveBy(dragPositionX - getWidth() / 2, dragPositionY - getHeight() / 2);
	}

	protected void upAction(boolean hideWithAnimation) {
		clickable = false;
		draggable = false;

		actionsStripWidget.setDraggedAction(null);

		if (collidesWithNumber != NO_COLLISION_NUMBER) {
			performDropAction();
			detachFromScene();
		} else {
			if(hideWithAnimation) {
				hideWithAnimation();
			} else {
				hide();
			}
		}

		collidesWithNumber = NO_COLLISION_NUMBER;
	}

	protected void onDoubleTap() {
		if (!TablexiaSettings.getInstance().isRunningOnMobileDevice())
			actionsStripWidget.onActionDoubleTap(this);
	}

//////////////////////////////////////////// COLLISION

	public void onCollision(int orderNumber, int actionSizeBigger, int actionSizeSmaller) {
		if (actionsStripWidget.isNotFull()) {
			if (orderNumber != NO_COLLISION_NUMBER) {
				if (collidesWithNumber != orderNumber) {
					collidesWithNumber = orderNumber;
					actionsStripWidget.performCollisionWithNumberStart(collidesWithNumber);
					resizeWithAnimation((float) actionSizeBigger);
				}
			} else if (collidesWithNumber != NO_COLLISION_NUMBER) {
				collidesWithNumber = NO_COLLISION_NUMBER;
				actionsStripWidget.performCollisionWithNumberFinish(collidesWithNumber);
				resizeWithAnimation((float) actionSizeSmaller);
			}
		}
	}
	
	
//////////////////////////////////////////// ANIMATIONS
	
	public void resizeWithAnimation(float newSize) {
		float size = newSize / actualSize;
		if (actualScaleAction != null) {
			actualScaleAction.finish();
		}
		actualScaleAction = scaleTo(size, size, RESIZE_ANIM_SCALE_DURATION, RESIZE_ANIM_SCALE_INTERPOLATION);
		addAction(sequence(actualScaleAction,
						   run(new Runnable() {
							   @Override
							   public void run() {
								   actualScaleAction = null;
							   }
						   })));
	}
	
	public void showWithAnimation() {
		addAction(fadeOut(0));
		addAction(scaleTo(ATTACH_ANIM_SCALE_START_VALUE, ATTACH_ANIM_SCALE_START_VALUE, 0));
		setVisible(true);
		addAction(parallel(fadeIn(ATTACH_ANIM_ALPHA_DURATION), scaleTo(ATTACH_ANIM_SCALE_FINISH_VALUE, ATTACH_ANIM_SCALE_FINISH_VALUE, ATTACH_ANIM_SCALE_DURATION, ATTACH_ANIM_SCALE_INTERPOLATION)));
	}

	private void hideWithAnimation() {
		addAction(sequence(parallel(fadeOut(DETACH_ANIM_ALPHA_DURATION),
						scaleTo(DETACH_ANIM_SCALE_FINISH_VALUE, DETACH_ANIM_SCALE_FINISH_VALUE, DETACH_ANIM_SCALE_DURATION, DETACH_ANIM_SCALE_INTERPOLATION)),
				run(new Runnable() {
					@Override
					public void run() {
						Action.this.setVisible(false);
						performDropAction();
						detachFromScene();
					}
				})));
	}

	private void hide() {
		addAction(run(new Runnable() {
			@Override
			public void run() {
				setVisible(false);
				performDropAction();
				detachFromScene();
			}
		}));
	}
	
	
	/* //////////////////////////////////////////// UTILITY */

	private void detachFromScene() {
		removeListener(dragListener);
		remove();
	}

	private void performDropAction() {
		for (ActionListener actionListener : actionListeners) {
			actionListener.onActionDrop(Action.this, collidesWithNumber);
		}
	}
}
