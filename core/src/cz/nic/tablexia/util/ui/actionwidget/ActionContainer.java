/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.actionwidget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Align;


import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.ui.TablexiaLabel;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;

/**
 * Action container with action number
 *
 * @author Matyáš Latner
 *
 */
public class ActionContainer extends Group {
	private static final Color 								 ACTION_NUMBER_TEXT_COLOR     = Color.BLACK;
	private static final ApplicationFontManager.FontType     ACTION_NUMBER_TEXT_FONT_TYPE = ApplicationFontManager.FontType.REGULAR_16;

	private TablexiaLabel   actionNumberText;
	private Action 			action;
//	private Rectangle 		reactionRectangle;
	private float			permanentPositionY;
	private float 			startPositionY;
//	private MoveModifier 	actualActionContainerModifier;
//	private MoveModifier 	reactionRectangleMoveModifier;

	private int				actionSizeBigger;
	private float			animationDuration;

	private ActionsStripWidget actionsStripWidget;

	public ActionContainer(float x, Action action, float startPositionY, ActionsStripWidget actionsStripWidget, int actionSizeBigger, float animationDuration) {
		setPosition(x, ActionContainer.getPositionYForActionPosition(startPositionY, action.getOrderNumber(), actionsStripWidget, actionSizeBigger), actionSizeBigger);
		this.startPositionY = startPositionY;
		this.actionsStripWidget = actionsStripWidget;
		this.actionSizeBigger = actionSizeBigger;
		this.animationDuration = animationDuration;

		this.actionNumberText = new TablexiaLabel(String.valueOf(action.getOrderNumber() + 1), new TablexiaLabel.TablexiaLabelStyle(ACTION_NUMBER_TEXT_FONT_TYPE, ACTION_NUMBER_TEXT_COLOR));
		this.actionNumberText.setAlignment(Align.topLeft);
		this.actionNumberText.setPosition(actionSizeBigger + actionsStripWidget.getActionOffset(),
										  actionSizeBigger - actionNumberText.getHeight());

		this.action = action;
		addActor(action);
		addActor(actionNumberText);

		permanentPositionY = getY();

		prepareReactionRectangle();
	}



/* //////////////////////////////////////////// ACTION */

	public Action getAction() {
		return action;
	}

	public void changeActionNumber(int actionNumber) {
		action.setOrderNumber(actionNumber);
		actionNumberText.setText(String.valueOf(actionNumber + 1));
	}


/* //////////////////////////////////////////// REACTION RECTANGLE */

	private void prepareReactionRectangle() {
//		reactionRectangle = new Rectangle(0, 0, action.getWidth(), ActionsStripWidget.ACTION_OFFSET, vertexBufferObjectManager);
//		reactionRectangle.setVisible(false);
//		resetReactionRectangle(false);
//		attachChild(reactionRectangle);
	}

//	public Rectangle getReactionRectangle() {
//		return reactionRectangle;
//	}

	public void resetReactionRectangle(boolean animated) {
		moveReactionReactangle(-((action.getHeight() / 2) + (actionsStripWidget.getActionOffset() / 2)), animated);
	}

	public void setReactionRectangleOffset() {
		moveReactionReactangle(-(action.getHeight() + (actionsStripWidget.getActionOffset() / 2)), true);
	}

	private void moveReactionReactangle(float positionY, boolean animated) {
//		if (reactionRectangleMoveModifier != null) {
//			reactionRectangle.unregisterEntityModifier(reactionRectangleMoveModifier);
//			reactionRectangleMoveModifier = null;
//		}
//		if (animated) {
//			reactionRectangleMoveModifier = new MoveModifier(PotmeActivity.ANIMATION_MOVE_PERIOD,
//					0,
//					reactionRectangle.getY(),
//					0,
//					positionY,
//					EaseBackOut.getInstance());
//			reactionRectangleMoveModifier.setAutoUnregisterWhenFinished(true);
//			reactionRectangle.registerEntityModifier(reactionRectangleMoveModifier);
//		} else {
//			reactionRectangle.setPosition(0, positionY);
//		}
	}



/* //////////////////////////////////////////// MOVEMENT */

	public void resetPosition(boolean animated) {
		moveToYPosition(permanentPositionY, animated, true);
	}

	public void moveTemporalyHalfUp(boolean animated) {
		moveToYPosition(permanentPositionY + (action.getActualSize() / 2) + actionsStripWidget.getActionOffset(), animated, false);
	}

	public void moveTemporalyHalfDown(boolean animated) {
		moveToYPosition(permanentPositionY - (action.getActualSize() / 2) - actionsStripWidget.getActionOffset(), animated, false);
	}

	public void movePermanentlyUp(boolean animated) {
		moveToActionPositionOffset(1, animated, true);
		changeActionNumber(action.getOrderNumber() - 1);
	}

	public void movePermanentlyDown(boolean animated) {
		moveToActionPositionOffset(-1, animated, true);
		changeActionNumber(action.getOrderNumber() - 1);
	}

	public void moveToActionPositionOffset(int actionPosition, boolean animated, boolean permament) {
		moveToYPosition(permanentPositionY + (actionPosition * action.getActualSize()) + (Math.signum(actionPosition) * actionsStripWidget.getActionOffset()), animated, permament);
	}

	public void moveToActionPosition(boolean animated, boolean permament, ActionsStripWidget actionsStripWidget) {
		moveToYPosition(ActionContainer.getPositionYForActionPosition(startPositionY, action.getOrderNumber(), actionsStripWidget, actionSizeBigger), animated, permament);
	}

	public void moveToYPosition(float positionY, boolean animated, boolean permanent) {
		clearActions();
		if (animated) {
			addAction(moveTo(getX(), positionY, animationDuration, actionsStripWidget.ACTION_MOVE_INTERPOLATION));
		} else {
			setPosition(getX(), positionY);
		}
		if (permanent) {
			permanentPositionY = positionY;
		}
	}

	private static float getPositionYForActionPosition(float startPositionY, int actionPosition, ActionsStripWidget actionsStripWidget, int actionSizeBigger) {
		return actionsStripWidget.getActionOffset() + (actionPosition * (actionSizeBigger + actionsStripWidget.getActionOffset())) + startPositionY;
	}
	
}
