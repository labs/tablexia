/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by Matyáš Latner.
 */
public class AnimatedImage extends Image {

    protected   Animation<TextureRegion>   animation;
    private     float       stateTime = 0;

    private     boolean     started;
    private     boolean     loop;
    private     Runnable    finishHandler;

    public AnimatedImage(Animation animation, boolean started) {
        super((TextureRegion) animation.getKeyFrame(0));
        this.animation = animation;
        this.started = started;
        this.loop = false;
    }

    public void startAnimation(Runnable finishHandler) {
        this.finishHandler = finishHandler;
        stateTime = 0;
        started = true;
        loop = false;
    }

    public void startAnimationLoop() {
        stateTime = 0;
        started = true;
        loop = true;
    }

    public void stopAnimation() {
        started = false;
        loop = false;
    }

    @Override
    public void act(float delta) {
        if (started) {
            stateTime = stateTime + delta;
            if (loop) {
                ((TextureRegionDrawable)getDrawable()).setRegion((TextureRegion) animation.getKeyFrame(stateTime += delta, true));
            } else {
                ((TextureRegionDrawable)getDrawable()).setRegion((TextureRegion) animation.getKeyFrame(stateTime, false));
                if (finishHandler != null && animation.isAnimationFinished(stateTime)) {
                    finishHandler.run();
                }
            }
        } else {
            ((TextureRegionDrawable)getDrawable()).setRegion((TextureRegion) animation.getKeyFrame(0));
        }
        super.act(delta);
    }
}
