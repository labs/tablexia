/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class TablexiaProgressBar extends Group {
    private float fillOffsetX;
    private float fillOffsetY;

    private float percent;
    private float visualPercent;
    private float speed;
    private boolean smooth;
    
    private boolean patchOffset = false;
    
    private static final int DEFAULT_PATH_WIDTH   = 80;
    private static final int DEFAULT_PATCH_HEIGHT = 8;
    private static final int DEFAULT_OFFSET_X     = 1;
    private static final int DEFAULT_OFFSET_Y     = 1;

    private Image progressbar;
    private TextureRegion fill;
    
    
    public TablexiaProgressBar(NinePatch progressbarPath, TextureRegion fill){
        this(progressbarPath, fill, DEFAULT_PATH_WIDTH, DEFAULT_PATCH_HEIGHT);
    }
    
    public TablexiaProgressBar(NinePatch progressbarPath, TextureRegion fill, int width, int height){
        this.progressbar = new Image(progressbarPath);
        this.progressbar.setSize(width, height);
        this.fill = fill;
        patchOffset = true;
        setWidth(width);
    }

    public TablexiaProgressBar(TextureRegion progressbar, TextureRegion fill) {
        this(new Image(progressbar), fill, progressbar.getRegionWidth());
    }
    
    public TablexiaProgressBar(Image progressbarImage, TextureRegion fill, int width){
        this.progressbar = progressbarImage;
        this.fill = fill;
        setWidth(width);
    }

    @Override
    public void setWidth(float width) {
        float height = width * (progressbar.getHeight() / (float) progressbar.getWidth());
        super.setSize(width, height);
        
        if (!patchOffset){
            float scale_x = getWidth() / progressbar.getWidth();
            float scale_y = getHeight() / progressbar.getHeight();
            fillOffsetX = (this.progressbar.getWidth() - this.fill.getRegionWidth()) / 2 * scale_x;
            fillOffsetY = (this.progressbar.getHeight() - this.fill.getRegionHeight()) / 2 * scale_y;
        }else {
            fillOffsetX = DEFAULT_OFFSET_X;
            fillOffsetY = DEFAULT_OFFSET_Y;
        }
    }

    @Override
    public void setHeight(float height) {}

    @Override
    public void setBounds(float x, float y, float width, float height) {
        setPosition(x, y);
        setWidth(width);
    }

    @Override
    @Deprecated
    public void setSize(float width, float height) {
        setWidth(width);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(smooth) {
            if (visualPercent < percent) {
                visualPercent += 1/speed * delta;

                if (visualPercent > percent) visualPercent = percent;
            } else if (visualPercent > percent) {
                visualPercent -= 1/speed * delta;

                if (visualPercent < percent) visualPercent = percent;
            }
        }
    }

    public void setPercent(float percent) {
        this.percent = MathUtils.clamp(percent, 0, 1);

        if(!smooth)
            visualPercent = this.percent;
    }

    public void setSmooth(boolean smooth) {
        this.smooth = smooth;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        float origAlpha = batch.getColor().a;

        batch.setColor(batch.getColor().r, batch.getColor().g, batch.getColor().b, parentAlpha);

        if(visualPercent != 0) {
            int origRegionWidth = fill.getRegionWidth();
            fill.setRegionWidth((int) (fill.getRegionWidth() * visualPercent));
            batch.draw(
                    fill,
                    getX() + fillOffsetX,
                    getY() + fillOffsetY,
                    (getWidth() - 2 * fillOffsetX) * visualPercent,
                    getHeight() - 2 * fillOffsetY
            );
            fill.setRegionWidth(origRegionWidth);
        }
        
        progressbar.getDrawable().draw(batch, getX(), getY(), getWidth(), getHeight());

        batch.setColor(batch.getColor().r, batch.getColor().g, batch.getColor().b, origAlpha);
    }

    public float getFillOffsetX() {
        return fillOffsetX;
    }

    public float getFillOffsetY() {
        return fillOffsetY;
    }

    //Gets for testing
    public float getPercent() {
        return percent;
    }
}