/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import cz.nic.tablexia.loader.application.ApplicationFontManager;

/**
 * Created by bcx on 2/18/16.
 */
public class TablexiaLabel extends Label {
    public static class TablexiaLabelStyle {
        private ApplicationFontManager.FontType fontType;

        //This is not just a regular color, its canadian one!
        private Color colour;

        public TablexiaLabelStyle(ApplicationFontManager.FontType fontType, Color color) {
            this.fontType = fontType;
            this.colour = color;
        }

        public ApplicationFontManager.FontType getFontType() {
            return fontType;
        }

        public Color getColor() {
            return colour;
        }
    }

    /**
     * Fields...
     */
    private TablexiaLabelStyle  tablexiaLabelStyle;
    private boolean             markupEnabled;
    private float               scale;

    public TablexiaLabel(CharSequence text, TablexiaLabelStyle style) {
        this(text, style, false);
    }

    public TablexiaLabel(CharSequence text, TablexiaLabelStyle style, boolean markupEnabled) {
        super(text, new LabelStyle(ApplicationFontManager.getInstance().getDistanceFieldFont(style.getFontType()), style.getColor()));
        this.markupEnabled = markupEnabled;
        setTablexiaLabelStyle(style);

        //Fix to get precise size of label while using SDF fonts
        setSize(getWidth() * style.getFontType().getScale(), getHeight() * style.getFontType().getScale());
    }

    public void setTablexiaLabelStyle(TablexiaLabelStyle style) {
        if(style != null) {
            tablexiaLabelStyle = style;
        }

        scale = tablexiaLabelStyle.getFontType().getSize() / ApplicationFontManager.DISTANCE_FIELD_FONT_SIZE;
        setFontScale(scale);

        setStyle(new LabelStyle(ApplicationFontManager.getInstance().getDistanceFieldFont(style.getFontType()), getTablexiaLabelStyle().getColor()));
    }

    public TablexiaLabelStyle getTablexiaLabelStyle() {
        return tablexiaLabelStyle;
    }

    private void beforeDraw(Batch batch) {
        batch.setShader(ApplicationFontManager.getInstance().getDistanceFieldShader());
        ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaLabelStyle.getFontType()).setDistanceFieldSmoothing((ApplicationFontManager.DISTANCE_FIELD_FONT_DEFAULT_SMOOTHING * scale));
        ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaLabelStyle.getFontType()).getData().markupEnabled = markupEnabled;

        if(markupEnabled) invalidate();
    }

    private void afterDraw(Batch batch) {
        batch.setShader(null);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        beforeDraw(batch);
        super.draw(batch, parentAlpha);
        afterDraw(batch);
    }
}
