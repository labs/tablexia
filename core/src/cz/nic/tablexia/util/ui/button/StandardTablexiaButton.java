/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.button;

import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;

/**
 * Created by Matyáš Latner.
 */
public class StandardTablexiaButton extends TablexiaButton {

    private static final boolean                            DEFAULT_TEXT_WRAP           = false;

    public enum TablexiaButtonType {

        BLUE    (ApplicationInternalTextureManager.InternalNinePatch.BUTTON_BLUE_UNPRESSED_PATCH,
                ApplicationInternalTextureManager.InternalNinePatch.BUTTON_BLUE_PRESSED_PATCH,
                ApplicationInternalTextureManager.InternalNinePatch.BUTTON_BLUE_SOLARIZED_PATCH,
                ApplicationInternalTextureManager.InternalNinePatch.BUTTON_DISABLED_PATCH),
        GREEN   (ApplicationInternalTextureManager.InternalNinePatch.BUTTON_GREEN_UNPRESSED_PATCH,
                ApplicationInternalTextureManager.InternalNinePatch.BUTTON_GREEN_PRESSED_PATCH,
                ApplicationInternalTextureManager.InternalNinePatch.BUTTON_GREEN_SOLARIZED_PATCH,
                ApplicationInternalTextureManager.InternalNinePatch.BUTTON_DISABLED_PATCH),
        RED     (ApplicationInternalTextureManager.InternalNinePatch.BUTTON_RED_UNPRESSED_PATCH,
                ApplicationInternalTextureManager.InternalNinePatch.BUTTON_RED_PRESSED_PATCH,
                ApplicationInternalTextureManager.InternalNinePatch.BUTTON_RED_SOLARIZED_PATCH,
                ApplicationInternalTextureManager.InternalNinePatch.BUTTON_DISABLED_PATCH);

        private ApplicationInternalTextureManager.InternalNinePatch unpressedPath;
        private ApplicationInternalTextureManager.InternalNinePatch pressedPath;
        private ApplicationInternalTextureManager.InternalNinePatch solarizedPath;
        private ApplicationInternalTextureManager.InternalNinePatch disabledPath;

        TablexiaButtonType(ApplicationInternalTextureManager.InternalNinePatch unpressedPath,
                           ApplicationInternalTextureManager.InternalNinePatch pressedPath,
                           ApplicationInternalTextureManager.InternalNinePatch solarizedPath,
                           ApplicationInternalTextureManager.InternalNinePatch disabledPath) {

            this.unpressedPath  = unpressedPath;
            this.pressedPath    = pressedPath;
            this.solarizedPath  = solarizedPath;
            this.disabledPath   = disabledPath;
        }

        public ApplicationInternalTextureManager.InternalNinePatch getUnpressedPath() {
            return unpressedPath;
        }

        public ApplicationInternalTextureManager.InternalNinePatch getPressedPath() {
            return pressedPath;
        }

        public ApplicationInternalTextureManager.InternalNinePatch getSolarizedPath() {
            return solarizedPath;
        }

        public ApplicationInternalTextureManager.InternalNinePatch getDisabledPath() {
            return disabledPath;
        }
    }

    public StandardTablexiaButton(String text) {
        this(text, TablexiaButtonType.GREEN);
    }

    public StandardTablexiaButton(String text, TablexiaButtonType tablexiaButtonType) {
        this(text, tablexiaButtonType, DEFAULT_TEXT_WRAP);
    }

    public StandardTablexiaButton(String text, TablexiaButtonType tablexiaButtonType, boolean wrapText) {
        super(text, wrapText,
                ApplicationInternalTextureManager.getInstance().getPatch(tablexiaButtonType.getUnpressedPath()),
                ApplicationInternalTextureManager.getInstance().getPatch(tablexiaButtonType.getPressedPath()),
                ApplicationInternalTextureManager.getInstance().getPatch(tablexiaButtonType.getSolarizedPath()),
                ApplicationInternalTextureManager.getInstance().getPatch(tablexiaButtonType.getDisabledPath()));
    }
}
