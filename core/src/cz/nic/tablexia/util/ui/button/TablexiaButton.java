/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.button;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import net.engio.mbassy.listener.Handler;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationInternalSoundManager;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;
import cz.nic.tablexia.util.ui.TablexiaLabel;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Matyáš Latner.
 */
public class TablexiaButton extends Group {

    private static final float                              DEFAULT_WIDTH                       = 170;
    private static final float                              DEFAULT_HEIGHT                      = 80;
    private static final ApplicationFontManager.FontType    DEFAULT_TEXT_FONT                   = ApplicationFontManager.FontType.REGULAR_20;
    private static final Color                              DEFAULT_TEXT_COLOR                  = Color.BLACK;
    private static final int                                DEFAULT_TEXT_ALIGN                  = Align.center;
    private static final boolean                            DEFAULT_SOUND_CONTROL               = true;
    private static final boolean                            DEFAULT_SCALE_TO_TEXT               = true;
    private static final boolean                            DEFAULT_ADJUST_POSITION_X           = true;
    private static final boolean                            DEFAULT_ADJUST_POSITION_Y           = false;
    private static final boolean                            DEFAULT_CLICK_ONCE                  = false;
    private static final boolean                            DEFAULT_ONLY_ON_CLICK               = false;
    private static final boolean                            DEFAULT_ADAPTIVE_SIZE               = true;
    private static final boolean                            DEFAULT_ADAPTIVE_POSITION_FIX       = true;

    private static final float                              DEFAULT_ADAPTIVE_SIZE_RATIO         = 1.3f;
    private static final float                              DEFAULT_BUTTON_HIGHLITE_DURATION    = 0.5f;

    private final Group BACKGROUND_LAYER;
    private final Group EFFECT_LAYER;
    private final Group TEXT_LAYER;

    private NinePatch   unpressedPatch;
    private Image       unpressed;
    private NinePatch   pressedPatch;
    private Image       pressed;
    private NinePatch   solarizedPatch;
    private Image       solarized;
    private NinePatch   disabledPatch;
    private Image       disabled;

    private NinePatch                           actualPatch;
    private TablexiaLabel                       textLabel;
    private ApplicationFontManager.FontType     fontType;

    private float       originalX;
    private float       originalY;
    private float       originalWidth;
    private float       originalHeight;

    private float       adaptiveSizeRatio;

    private boolean     sound;
    private boolean     adjustSize;
    private boolean     adjustPositionX;
    private boolean     adjustPositionY;
    private boolean     useOnce;
    private boolean     isScaled;
    private boolean     sizeChangedCalled;
    private boolean     adaptiveSize;
    private boolean     adaptiveSizePositionFix;
    private boolean     checkable;
    private boolean     enabled;
    private boolean     onlyOnClick;

    private RepeatAction highliteAction;

    public TablexiaButton(String text, boolean wrapText, Texture unpressedPatch, Texture pressedPatch, Texture solarizedPatch, Texture disabledPatch) {
        this(text, wrapText,
                unpressedPatch != null ? new TextureRegion(unpressedPatch) : null,
                pressedPatch != null ? new TextureRegion(pressedPatch) : null,
                solarizedPatch != null ? new TextureRegion(solarizedPatch) : null,
                disabledPatch != null ? new TextureRegion(disabledPatch) : null);
    }

    public TablexiaButton(String text, boolean wrapText, TextureRegion unpressedPatch, TextureRegion pressedPatch, TextureRegion solarizedPatch, TextureRegion disabledPatch) {
        this(text, wrapText,
                prepareClearNinePatch(unpressedPatch),
                prepareClearNinePatch(pressedPatch),
                prepareClearNinePatch(solarizedPatch),
                prepareClearNinePatch(disabledPatch));
    }

    public TablexiaButton(String text, boolean wrapText, NinePatch unpressedPatch, NinePatch pressedPatch, NinePatch solarizedPatch, NinePatch disabledPatch) {
        this.adjustSize                 = DEFAULT_SCALE_TO_TEXT;
        this.adjustPositionX            = DEFAULT_ADJUST_POSITION_X;
        this.adjustPositionY            = DEFAULT_ADJUST_POSITION_Y;
        this.adjustSize                 = DEFAULT_SCALE_TO_TEXT;
        this.sound                      = DEFAULT_SOUND_CONTROL;
        this.useOnce                    = DEFAULT_CLICK_ONCE;
        this.onlyOnClick                = DEFAULT_ONLY_ON_CLICK;
        this.adaptiveSize               = DEFAULT_ADAPTIVE_SIZE;
        this.adaptiveSizePositionFix    = DEFAULT_ADAPTIVE_POSITION_FIX;

        this.fontType                   = DEFAULT_TEXT_FONT;
        this.adaptiveSizeRatio          = DEFAULT_ADAPTIVE_SIZE_RATIO;

        this.isScaled                   = false;
        this.sizeChangedCalled          = false;


        BACKGROUND_LAYER                = new Group();
        EFFECT_LAYER                    = new Group();
        TEXT_LAYER                      = new Group();

        addActor(BACKGROUND_LAYER);
        addActor(EFFECT_LAYER);
        addActor(TEXT_LAYER);

        if (unpressedPatch != null) {
            this.unpressedPatch = unpressedPatch;
            unpressed           = new Image(unpressedPatch);
        }
        if (pressedPatch != null) {
            this.pressedPatch   = pressedPatch;
            pressed             = new Image(pressedPatch);
        }
        if (solarizedPatch != null) {
            this.solarizedPatch = solarizedPatch;
            solarized           = new Image(solarizedPatch);
            solarized.setVisible(false);
            EFFECT_LAYER.addActor(solarized);
        }
        if (disabledPatch != null) {
            this.disabledPatch  = disabledPatch;
            disabled            = new Image(disabledPatch);
        }

        textLabel = new TablexiaLabel(text, new TablexiaLabel.TablexiaLabelStyle(fontType, DEFAULT_TEXT_COLOR));
        textLabel.setAlignment(DEFAULT_TEXT_ALIGN);
        textLabel.setWrap(wrapText);
        textLabel.pack();
        TEXT_LAYER.addActor(textLabel);

        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setEnabled();
    }

    private static NinePatch prepareClearNinePatch(TextureRegion textureRegion) {
        return textureRegion == null ? null : new NinePatch(textureRegion, 0, 0, 0, 0);
    }

    @Override
    protected void setParent(Group parent) {
        super.setParent(parent);
        if (parent != null) {
            adjustBounds();
        }
        processAdaptiveSize(adaptiveSize && parent != null);
    }

	protected NinePatch getActualPatch() {
		return actualPatch;
	}

//////////////////////////// SETTINGS

    public TablexiaButton text(String text) {
        if (textLabel != null) {
            textLabel.setText(text);
            adjustBounds();
        }
        return this;
    }

    public TablexiaButton adaptiveSize(boolean adaptiveSize) {
        this.adaptiveSize = adaptiveSize;
        processAdaptiveSize(adaptiveSize);
        return this;
    }

    public TablexiaButton adaptiveSizeRatio(float adaptiveSizeRatio) {
        this.adaptiveSizeRatio = adaptiveSizeRatio;
        processAdaptiveSize(adaptiveSize);
        return this;
    }

    public TablexiaButton adaptiveSizePositionFix(boolean adaptiveSizePositionFix) {
        this.adaptiveSizePositionFix = adaptiveSizePositionFix;
        processAdaptiveSize(adaptiveSize);
        return this;
    }

    public TablexiaButton sound(boolean sound) {
        this.sound = sound;
        return this;
    }

    public TablexiaButton adjustSize(boolean adjustSize) {
        if (this.adjustSize != adjustSize) {
            this.adjustSize = adjustSize;
            sizeChanged();
        }
        return this;
    }

    public TablexiaButton adjustPositionX(boolean adjustPositionX) {
        if (this.adjustPositionX != adjustPositionX) {
            this.adjustPositionX = adjustPositionX;
            sizeChanged();
        }
        return this;
    }

    public TablexiaButton adjustPositionY(boolean adjustPositionY) {
        if (this.adjustPositionY != adjustPositionY) {
            this.adjustPositionY = adjustPositionY;
            sizeChanged();
        }
        return this;
    }

    public TablexiaButton fontType(ApplicationFontManager.FontType fontType) {
        this.fontType = fontType;
        textLabel.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(fontType, textLabel.getTablexiaLabelStyle().getColor()));
        return this;
    }

    public TablexiaButton fontColor(Color textColor) {
        textLabel.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(textLabel.getTablexiaLabelStyle().getFontType(), textColor));
        return this;
    }


//////////////////////////// INPUT HANDLING

    private final ClickListener buttonListener = new ClickListener() {

        @Override
        public boolean handle(Event e) {
            boolean result = super.handle(e);
            if (inputListener != null) {
                inputListener.handle(e);
            }
            return result;
        }

        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if(button == Input.Buttons.RIGHT) return false;
            if(!onlyOnClick) {
                toggleCheckState();
                playClickSound();
            }
            super.touchDown(event, x, y, pointer, button);
            return true;
        }

        @Override
        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            if (!checkable && !onlyOnClick) {
                setUnchecked();
            }
            super.touchUp(event, x, y, pointer, button);
        }

        @Override
        public void clicked(InputEvent event, float x, float y) {
            if (useOnce) {
                setDisabled();
            }

            if(onlyOnClick) {
                playClickSound();
                if(checkable)
                    toggleCheckState();
                else
                    setChecked();
            }
        }
    };

    private InputListener inputListener;

    private void registerInputListener() {
        super.addListener(buttonListener);
    }

    private void unregisterInputListener() {
        super.removeListener(buttonListener);
    }

    public TablexiaButton setInputListener(InputListener inputListener) {
        this.inputListener = inputListener;
        return this;
    }

    @Override
    public boolean removeListener(EventListener listener) {
        setInputListener(null);
        return true;
    }

    @Deprecated
    @Override
    /**
     * @deprecated use {@link #setInputListener(InputListener)} instead.
     */
    public boolean addListener(EventListener listener) {
        setInputListener(inputListener);
        return true;
    }

    public void removeListener() {
        removeListener(inputListener);
    }


    protected void playClickSound() {
        if (sound) {
            ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.BUTTON_CLICKED).play();
        }
    }


//////////////////////////// BUTTON SIZE

    @Override
    public void setX(float x) {
        originalX = x;
        super.setX(x);
    }

    @Override
    public void setY(float y) {
        originalY = y;
        super.setY(y);
    }

    @Override
    public void setPosition(float x, float y) {
        originalX = x;
        originalY = y;
        super.setPosition(x, y);
    }

    @Override
    public void setWidth(float width) {
        originalWidth = width;
        super.setWidth(width);
    }

    @Override
    public void setHeight(float height) {
        originalHeight = height;
        super.setHeight(height);
    }

    @Override
    public void setSize(float width, float height) {
        originalWidth = width;
        originalHeight = height;
        super.setSize(width, height);
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        originalX = x;
        originalY = y;
        originalWidth = width;
        originalHeight = height;
        super.setBounds(x, y, width, height);
    }

    /**
     * Same as {@link #setBounds(float, float, float, float)} method.
     * @return instance of {@link TablexiaButton} for method chaining.
     */
    public TablexiaButton setButtonBounds(float x, float y, float width, float height) {
        setBounds(x, y, width, height);
        return this;
    }

    /**
     * Same as {@link #setSize(float, float)} method.
     * @return instance of {@link TablexiaButton} for method chaining.
     */
    public TablexiaButton setButtonSize(float width, float height) {
        setSize(width, height);
        return this;
    }

    /**
     * Same as {@link #setPosition(float, float)} method.
     * @return instance of {@link TablexiaButton} for method chaining.
     */
    public TablexiaButton setButtonPosition(float x, float y) {
        setPosition(x, y);
        return this;
    }

    @Override
    protected void positionChanged() {
        adjustBounds();
    }

    @Override
    protected void sizeChanged() {
        adjustBounds();
    }

    protected void adjustBounds() {
        if (!sizeChangedCalled) {
            sizeChangedCalled = true;

            float x = getX();
            float y = getY();
            float width = getWidth();
            float height = getHeight();
            if (adjustSize) {
                if (getInnerWidth() < getContentWidth()) {
                    width = getContentWidth() + getActualPatchHorizontalBorder();
                }
                if (getInnerHeight() < getContentHeight()) {
                    height = getContentHeight() + getActualPatchVerticalBorder();
                }
                if (adjustPositionX) {
                    x = originalX + ((originalWidth - width) / 2);
                }
                if (adjustPositionY) {
                    y = originalY + ((originalHeight - height) / 2);
                }
            }
            BACKGROUND_LAYER.setSize(width, height);
            setSizeForImage(unpressed, unpressedPatch);
            setSizeForImage(pressed, pressedPatch);
            setSizeForImage(solarized, solarizedPatch);
            setSizeForImage(disabled, disabledPatch);
            textLabel.pack();
            setSizeTextLabel(textLabel);
            super.setBounds(x, y, width, height);
            super.sizeChanged();

            sizeChangedCalled = false;
        }
    }

    public float getContentWidth() {
        if(textLabel.getText().toString().isEmpty()) return 0;
        return textLabel.getMinWidth();
    }

    public float getContentHeight() {
        return textLabel.getMinHeight();
    }

    private void setSizeForImage(Image image, NinePatch patch) {
        if (image != null && patch != null) {
            image.setPosition(0, 0);
            image.setSize(getWidth(), getHeight());
        }
    }

    private void setSizeTextLabel(TablexiaLabel label) {
        if (label != null && actualPatch != null) {
            prepareTextSize(label);
            prepareTextPosition(label);
        }
    }

    protected void prepareTextPosition(TablexiaLabel label) {
        label.setPosition(actualPatch.getPadLeft(), actualPatch.getPadBottom());
    }

    protected void prepareTextSize(TablexiaLabel label) {
        label.setSize(getInnerWidth(), getInnerHeight());
    }

    private float getActualPatchHorizontalBorder() {
        float result = 0;
        if (actualPatch != null) {
            result = actualPatch.getPadLeft() + actualPatch.getPadRight();
        }
        return result;
    }

    private float getActualPatchVerticalBorder() {
        float result = 0;
        if (actualPatch != null) {
            result = actualPatch.getPadBottom() + actualPatch.getPadTop();
        }
        return result;
    }

	protected float getInnerWidth() {
        float result = getWidth();
        if (actualPatch != null) {
            result = result - getActualPatchHorizontalBorder();
        }
        return result;
    }

    protected float getInnerHeight() {
        float result = getHeight();
        if (actualPatch != null) {
            result = result - getActualPatchVerticalBorder();
        }
        return result;
    }


//////////////////////////// BUTTON STATE

    public TablexiaButton useOnce(boolean useOnce) {
        this.useOnce = useOnce;
        return this;
    }

    public TablexiaButton onlyOnClick(boolean onlyOnClick) {
        this.onlyOnClick = onlyOnClick;
        return this;
    }

    public TablexiaButton checkable(boolean checkable) {
        this.checkable = checkable;
        return this;
    }

    public boolean isChecked() {
        return actualPatch == pressedPatch;
    }

    public TablexiaButton setEnabled() {
        setEnabled(true);
        return this;
    }

    public TablexiaButton setDisabled() {
        setEnabled(false);
        return this;
    }

    public TablexiaButton setEnabled(boolean enabled) {
        if (this.enabled != enabled) {
            this.enabled = enabled;
            if (enabled) {
                registerInputListener();
                setUnchecked();
            } else {
                unregisterInputListener();
                changeBackground(disabled, disabledPatch);
            }
        }
        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    private void changeBackground(Image image, NinePatch patch) {
        if (image != null && patch != null) {
            BACKGROUND_LAYER.clearChildren();
            BACKGROUND_LAYER.addActor(image);
            actualPatch = patch;
            sizeChanged();
        }
    }

    public void setChecked(boolean checked) {
        if (checked) {
            setChecked();
        } else {
            setUnchecked();
        }
    }

    public void setChecked() {
        changeBackground(pressed, pressedPatch);
    }

    public void setUnchecked() {
        changeBackground(unpressed, unpressedPatch);
    }

    public void toggleCheckState() {
        setChecked(!isChecked());
    }

    public TablexiaLabel getLabel() {
        return textLabel;
    }

//////////////////////////// HIGHLITE

    public void highliteButton(boolean enable) {
        if (solarized != null) {
            if (enable) {
                solarized.addAction(alpha(0));
                highliteAction = forever(sequence(fadeIn(DEFAULT_BUTTON_HIGHLITE_DURATION), fadeOut(DEFAULT_BUTTON_HIGHLITE_DURATION)));
                solarized.addAction(highliteAction);
                solarized.setVisible(true);
            } else {
                solarized.setVisible(false);
                solarized.removeAction(highliteAction);
            }
        }
    }


//////////////////////////// ADAPTIVE SIZE

    private void processAdaptiveSize(boolean adaptiveSize) {
        if (adaptiveSize && hasParent()) {
            ApplicationBus.getInstance().subscribe(this);
        } else {
            ApplicationBus.getInstance().unsubscribe(this);
        }
        adaptiveSizeThresholdChanged();
    }

    public void adaptiveSizeThresholdChanged() {
        float oldWidth  = getWidth();
        float oldHeight = getHeight();
        float width     = getWidth();
        float height    = getHeight();

        if (ComponentScaleUtil.isUnderThreshold()) {
            if (!isScaled) {
                isScaled = true;
                scaleFontBySize(adaptiveSizeRatio);
                width = getWidth() * adaptiveSizeRatio;
                height = getHeight() * adaptiveSizeRatio;
            }
        } else {
            if (isScaled) {
                isScaled = false;
                scaleFontBySize(1);
                width = getWidth() / adaptiveSizeRatio;
                height = getHeight() / adaptiveSizeRatio;
            }
        }

        float positionX = getX();
        float positionY = getY();
        if (adaptiveSizePositionFix) {
            positionX = positionX + (oldWidth - width) / 2;
            positionY = positionY + (oldHeight - height) / 2;
        }

        setBounds(positionX, positionY, width, height);
    }

    private void scaleFontBySize(float sizeRatio) {
        textLabel.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.getProximateFontTypeForSize(Math.round(fontType.getSize() * sizeRatio), fontType.isBold()), textLabel.getTablexiaLabelStyle().getColor()));
    }

    @Handler
    public void handleScreenSizeThresholdChanged(TablexiaApplication.ScreenSizeThresholdChanged screenSizeThresholdChanged) {
        adaptiveSizeThresholdChanged();
    }
}