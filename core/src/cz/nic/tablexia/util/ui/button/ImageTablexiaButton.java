/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.button;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by frantisek on 18.2.16.
 */
public class ImageTablexiaButton extends StandardTablexiaButton {

	private static final int	DEFAULT_ICON_ALIGN			 			= Align.left;
	private static final float  DEFAULT_ICON_HEIGHT_RATIO 				= 1f / 2;
	private static final float  DEFAULT_ICON_HEIGHT_RATIO_ICON_ONLY 	= 3f / 4;

	private static final int	DEFAULT_ICON_PADDING 		 			= 7;
	private static final int 	DEFAULT_IMAGE_LEFT_PADDING_MULTIPLIER 	= 3;
	private static final float  DEFAULT_LABEL_TO_ICON_OFFSET 			= 1 / 2f; //0 means label is centered and icon is next to it

	private int					iconAlign 					 			= DEFAULT_ICON_ALIGN;
	private final Group 		ICON_LAYER;
	private Image				icon;

	public ImageTablexiaButton(String text, Image icon, int align) {
		super(text);
		ICON_LAYER = new Group();
		init(icon, align);
	}

	public ImageTablexiaButton(String text, Image icon) {
		this(text, icon, DEFAULT_ICON_ALIGN);
	}

	public ImageTablexiaButton(String text, Image icon, int align, TablexiaButtonType tablexiaButtonType) {
		this(text, icon, align, tablexiaButtonType, false);
	}

	public ImageTablexiaButton(String text, Image icon, TablexiaButtonType tablexiaButtonType, boolean wrapText) {
		this(text, icon, DEFAULT_ICON_ALIGN, tablexiaButtonType, wrapText);
	}

	public ImageTablexiaButton(String text, Image icon, int align, TablexiaButtonType tablexiaButtonType, boolean wrapText) {
		super(text, tablexiaButtonType, wrapText);
		ICON_LAYER = new Group();
		init(icon, align);
	}

	private void init(Image icon, int align) {
		addActor(ICON_LAYER);
		setIcon(icon, align);
	}


	public TablexiaButton setIcon(Image icon) {
		return setIcon(icon, iconAlign);
	}

	public TablexiaButton setIcon(Image icon, int align) {
		this.icon = icon;
		this.iconAlign = align;

		ICON_LAYER.clear();
		if (icon == null) {
			return this;
		}

		ICON_LAYER.addActor(icon);
		adjustBounds();
		return this;
	}

	private boolean hasLabel() {
		return getLabel() != null && !getLabel().getText().toString().isEmpty();
	}

	private boolean hasIcon() {
		return icon != null;
	}

	@Override
	protected void adjustBounds() {
		if(hasIcon()) {
			prepareIconSizeAndPosition();
		}

		super.adjustBounds();
	}

	@Override
	public float getContentWidth() {
		float labelWidth = super.getContentWidth();

		float iconWidth = 0;
		if (icon != null) {
			iconWidth = icon.getWidth();
			if(hasLabel()) iconWidth += (DEFAULT_IMAGE_LEFT_PADDING_MULTIPLIER * DEFAULT_ICON_PADDING);
		}
		return labelWidth + iconWidth;
	}

	@Override
	protected void prepareTextSize(TablexiaLabel label) {
		//Don't remove this, super.prepareTextSize sets labels size across whole button, which breaks stuff...
	}

	@Override
	protected void prepareTextPosition(TablexiaLabel label) {
		if(!hasLabel()) return;

		label.setPosition(
				getActualPatch().getPadLeft()   + (getInnerWidth() - label.getWidth())/2,
				getActualPatch().getPadBottom() + (getInnerHeight() / 2f) - (label.getHeight() / 2f)
		);

		if(hasIcon()) {
			float iconOffset = icon.getWidth() * DEFAULT_LABEL_TO_ICON_OFFSET;

			switch (iconAlign) {
				case Align.left:
					label.setX(label.getX() + iconOffset);
					break;
				case Align.right:
					label.setX(label.getX() - iconOffset);
					break;
			}
		}
	}

	private void prepareIconSizeAndPosition () {
		if(hasLabel()) { //Button has a label
			ScaleUtil.setImageHeight(icon, getInnerHeight() * DEFAULT_ICON_HEIGHT_RATIO);

			switch (iconAlign) {
				case Align.left:
				case Align.bottomLeft:
				case Align.topLeft:
					//Put icon to the left of label
					icon.setX(getLabel().getX() - icon.getWidth() - DEFAULT_ICON_PADDING);
					break;

				case Align.right:
				case Align.bottomRight:
				case Align.topRight:
					//Put icon to the right of label
					icon.setX(getLabel().getX() + getLabel().getWidth() + DEFAULT_ICON_PADDING);
					break;
			}
		} else { //Button doesn't have a label, center the icon
			ScaleUtil.setImageHeight(icon, getInnerHeight() * DEFAULT_ICON_HEIGHT_RATIO_ICON_ONLY);
			icon.setX(getActualPatch().getPadLeft() + getInnerWidth() / 2f - icon.getWidth() / 2f);
		}

		icon.setY(getActualPatch().getPadBottom() + (getInnerHeight() - icon.getHeight()) / 2f);
	}

	public Image getIcon() {
		return icon;
	}
}
