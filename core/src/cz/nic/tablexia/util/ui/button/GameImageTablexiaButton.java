/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.button;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;

/**
 * Created by drahomir on 8/26/16.
 */
public class GameImageTablexiaButton extends ImageTablexiaButton {
    private static final TablexiaButtonType DEFAULT_TABLEXIA_BUTTON_TYPE    = TablexiaButtonType.GREEN;
    private static final float              DEFAULT_THRESHOLD_WIDTH         = 64;
    private static final float              DEFAULT_THRESHOLD_HEIGHT_RATIO  = 1.3f;

    private boolean underThreshold = false;

    private String text = null;

    public GameImageTablexiaButton(String text, Image icon) {
        this(text, icon, DEFAULT_TABLEXIA_BUTTON_TYPE);
    }

    public GameImageTablexiaButton(String text, Image icon, TablexiaButtonType type) {
        super(text, icon, type, false);
        this.text = text;
    }

    @Override
    public void adaptiveSizeThresholdChanged() {
        if(ComponentScaleUtil.isUnderThreshold()) {
            underThreshold = true;
            getLabel().setText(null);
            getLabel().setVisible(false);
            setDefaultThresholdSize();
        } else {
            underThreshold = false;
            getLabel().setText(text);
            getLabel().setVisible(true);
        }
    }

    @Override
    public TablexiaButton setButtonSize(float width, float height) {
        //Prevents from resizing button when app is under threshold!
        if(!underThreshold) super.setButtonSize(width, height);
        else setDefaultThresholdSize();
        return this;
    }

    @Override
    public void setSize(float width, float height) {
        //Prevents from resizing button when app is under threshold!
        if(!underThreshold) super.setSize(width, height);
        else setDefaultThresholdSize();
    }

    @Override
    public TablexiaButton setButtonBounds(float x, float y, float width, float height) {
        if(!underThreshold) {
            super.setButtonBounds(x, y, width, height);
        } else {
            setButtonPosition(x, y);
            setDefaultThresholdSize();
        }

        return this;
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        super.setPosition(x, y);
        setSize(width, height);
    }

    private void setDefaultThresholdSize() {
        super.setSize(DEFAULT_THRESHOLD_WIDTH, DEFAULT_THRESHOLD_WIDTH * DEFAULT_THRESHOLD_HEIGHT_RATIO);
        adjustBounds();
    }
}