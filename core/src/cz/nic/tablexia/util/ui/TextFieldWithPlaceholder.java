/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.loader.application.ApplicationFontManager;

/**
 * Some older Android keyboards have bug with backspace not working after pressing enter to stop editing
 * and then focusing field again and trying to delete old text https://github.com/libgdx/libgdx/issues/953
 *
 * Created by frantisek on 15.12.15.
 */
public class TextFieldWithPlaceholder extends TextField {

    public static class TablexiaTextFieldStyle {
        private ApplicationFontManager.FontType fontType;
        private Color fontColor;
        private Drawable cursor, selection, background;

        public TablexiaTextFieldStyle(ApplicationFontManager.FontType fontType, Color fontColor, Drawable cursor, Drawable selection, Drawable background) {
            this.fontType = fontType;
            this.fontColor = fontColor;
            this.cursor = cursor;
            this.selection = selection;
            this.background = background;
        }

        public ApplicationFontManager.FontType getFontType() {
            return fontType;
        }

        public Color getFontColor() {
            return fontColor;
        }

        public Drawable getBackground() {
            return background;
        }

        public Drawable getCursor() {
            return cursor;
        }

        public Drawable getSelection() {
            return selection;
        }
    }

    private static final Color  DEFAULT_PLACEHOLDER_COLOR = Color.GRAY;
    private static final int    DEFAULT_PLACEHOLDER_ALIGN = Align.center;

    private TablexiaTextFieldStyle tablexiaTextFieldStyle;
    private TablexiaLabel placeholder;

    private GlyphLayout layout;

    private float tmpTextWidth = 0;
    private float tmpFontDescent = 0;

    public TextFieldWithPlaceholder(String text, TablexiaTextFieldStyle tablexiaTextFieldStyle, String placeholderText) {
        this(text, tablexiaTextFieldStyle, placeholderText, DEFAULT_PLACEHOLDER_ALIGN);
    }

    public TextFieldWithPlaceholder(String text, TablexiaTextFieldStyle tablexiaTextFieldStyle, String placeholderText, int align) {
        this(   text,
                new TextFieldStyle(
                        ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaTextFieldStyle.fontType),
                        tablexiaTextFieldStyle.fontColor,
                        tablexiaTextFieldStyle.cursor,
                        tablexiaTextFieldStyle.selection,
                        tablexiaTextFieldStyle.background
                ),
                placeholderText,
                align
        );

        this.tablexiaTextFieldStyle = tablexiaTextFieldStyle;
        this.layout = new GlyphLayout(ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaTextFieldStyle.fontType), "");

    }

    private TextFieldWithPlaceholder(String text, TextFieldStyle style, String placeholderText) {
        this(text, style, placeholderText, DEFAULT_PLACEHOLDER_ALIGN);
    }

    private TextFieldWithPlaceholder(String text, TextFieldStyle style, String placeholderText, int placeholderAlign) {
        super(text, style);

        placeholder = new TablexiaLabel(placeholderText, preparePlaceholderStyle(style));
        placeholder.setTouchable(Touchable.disabled);
        placeholder.setAlignment(placeholderAlign);
        refreshPlaceholderState();
    }

    public void setTablexiaTextFieldStyle(TablexiaTextFieldStyle style) {
        if(style != null) {
            setStyle(new TextFieldStyle(ApplicationFontManager.getInstance().getDistanceFieldFont(style.getFontType()), style.getFontColor(), style.getCursor(), style.getSelection(), style.getBackground()));
            layout = new GlyphLayout(ApplicationFontManager.getInstance().getDistanceFieldFont(style.getFontType()), "");
        }
    }

    public void setPlaceHolderLabelStyle(TablexiaLabel.TablexiaLabelStyle style) {
        if(placeholder != null) placeholder.setTablexiaLabelStyle(style);
    }

    public void refreshPlaceholderState() {
        if (placeholder != null) {
            placeholder.setVisible(getText() == null || getText().toString().isEmpty());
        }
    }

    @Override
    protected void positionChanged() {
        super.positionChanged();
        if (placeholder != null) {
            placeholder.setPosition(getX(), getY());
        }
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        if (placeholder != null) {
            placeholder.setSize(getWidth(), getHeight());
        }
    }

    @Override
    protected void setParent(Group parent) {
        super.setParent(parent);
        if (parent!= null && placeholder != null) {
            parent.addActor(placeholder);
        }
    }

    /**
     * Use setTablexiaTextFieldStyle instead
     * @param textFieldStyle
     */
    @Deprecated
    @Override
    public void setStyle(TextFieldStyle textFieldStyle) {
        if (placeholder != null) {
            placeholder.setTablexiaLabelStyle(preparePlaceholderStyle(textFieldStyle));
        }
        super.setStyle(textFieldStyle);
    }

    @Override
    public void setText (String str) {
        super.setText(str);
        refreshPlaceholderState();
    }

    protected void initialize () {
        super.initialize();
        addListener(new TextFieldWithPlaceholderClickListener());
    }

    private TablexiaLabel.TablexiaLabelStyle preparePlaceholderStyle(TextFieldStyle textFieldStyle) {
        return new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_18, DEFAULT_PLACEHOLDER_COLOR);
    }

    public class TextFieldWithPlaceholderClickListener extends InputListener {

        @Override
        public boolean keyDown (InputEvent event, int keycode) {
            refreshPlaceholderState();
            return false;
        }

        @Override
        public boolean keyTyped (InputEvent event, char character) {
            // have to check also (int)character because of some bug in iOS keyboard. It returns code Input.Keys.ENTER for every key after "done" button pressed
            if ((event.getKeyCode() == Input.Keys.ENTER && ((int)character == 10 || (int)character == 13)) || event.getKeyCode() == Input.Keys.TAB) {
                TextFieldWithPlaceholder.this.getStage().unfocus(TextFieldWithPlaceholder.this);
                Gdx.input.setOnscreenKeyboardVisible(false);
            }

            //Measures texts Width and fonts descent
            float scale = tablexiaTextFieldStyle.getFontType().getSize() / ApplicationFontManager.DISTANCE_FIELD_FONT_SIZE;
            ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaTextFieldStyle.getFontType()).getData().setScale(scale);
            layout.setText(ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaTextFieldStyle.getFontType()), displayText);
            tmpTextWidth = layout.width;
            tmpFontDescent = ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaTextFieldStyle.getFontType()).getDescent();
            ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaTextFieldStyle.getFontType()).getData().setScale(1);

            refreshPlaceholderState();
            return false;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    @Override
    protected void drawCursor(Drawable cursorPatch, Batch batch, BitmapFont font, float x, float y) {
        float scale = tablexiaTextFieldStyle.getFontType().getSize() / ApplicationFontManager.DISTANCE_FIELD_FONT_SIZE;
        cursorPatch.draw(
				batch,
				(x + (glyphPositions.get(cursor) * scale)),
				y - tablexiaTextFieldStyle.getFontType().getSize() + tmpFontDescent,
				cursorPatch.getMinWidth(),
				tablexiaTextFieldStyle.getFontType().getSize() - tmpFontDescent);
    }

	protected int letterUnderCursor (float x) {
		float scale = tablexiaTextFieldStyle.getFontType().getSize() / ApplicationFontManager.DISTANCE_FIELD_FONT_SIZE;
		x -= textOffset;
		x /= scale;
		int index = glyphPositions.size - 1;
		float[] glyphPositions = this.glyphPositions.items;
		for (int i = 0, n = this.glyphPositions.size; i < n; i++) {
			if (glyphPositions[i] > x) {
				index = i - 1;
				break;
			}
		}
		return Math.max(0, index);
	}

    @Override
    protected void drawText(Batch batch, BitmapFont font, float x, float y) {
        batch.setShader(ApplicationFontManager.getInstance().getDistanceFieldShader());
		float scale = tablexiaTextFieldStyle.getFontType().getSize() / ApplicationFontManager.DISTANCE_FIELD_FONT_SIZE;
		ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaTextFieldStyle.getFontType()).setDistanceFieldSmoothing(ApplicationFontManager.DISTANCE_FIELD_FONT_DEFAULT_SMOOTHING * scale);
		ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaTextFieldStyle.getFontType()).getData().setScale(scale);
		font.draw(
			batch,
			displayText,
			x,
			y + font.getDescent(),
			0,
			displayText.length(),
			getWidth(),
			Align.left,
			false
		);
		ApplicationFontManager.getInstance().getDistanceFieldFont(tablexiaTextFieldStyle.getFontType()).getData().setScale(1);
        batch.setShader(null);
    }
}