/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

import cz.nic.tablexia.TablexiaSettings;

/**
 * Created by lhoracek on 8/25/15.
 */
public class Switch extends Group {
    private static final float STEPS_OFFSET = 10;
    private DragSwitchListener dragSwitchListener;
    private Image[]            stepImages;

    public Switch(TextureRegion background, TextureRegion... steps) {
        Image backgroundImage = new Image(background);
        backgroundImage.setSize(background.getRegionWidth()/getMultiplicator(), background.getRegionHeight()/getMultiplicator());
        this.addActor(backgroundImage); // background
        this.setSize(backgroundImage.getWidth(), backgroundImage.getHeight());
        Group selector = new WidgetGroup();
        this.stepImages = new Image[steps.length];
        for (int i = 0; i < steps.length; i++) {
            TextureRegion step = steps[i];
            Image stepImage = new Image(step);
            stepImage.setSize(step.getRegionWidth()/getMultiplicator(), step.getRegionHeight()/getMultiplicator());
            this.stepImages[i] = stepImage;
            selector.addActor(stepImage);
            stepImage.setY(-stepImage.getHeight() / 2 - STEPS_OFFSET);
        }
        this.addActor(selector);
        selector.setY(backgroundImage.getHeight() / 2);

        float start = -(steps[0].getRegionWidth()/getMultiplicator()) / 2;
        float stop = (background.getRegionWidth()/getMultiplicator()) - ((steps[steps.length - 1].getRegionWidth()/getMultiplicator()) / 2);
        if (steps.length > 3)
            throw new IllegalArgumentException("Switch is not implemented yet for multiple snap points. Add computing of snap point values");
        dragSwitchListener = new DragSwitchListener(selector, start, stop, start + (stop - start / 2) - (steps[1].getRegionWidth()/getMultiplicator()) / 2); // written just for one intermediate snap point
        selector.addListener(dragSwitchListener);
        dragSwitchListener.setSwitchMovedListener(new DragSwitchListener.SwitchMovedListener() {
            @Override
            public void movedToStep(int step) {
                for (Image im : stepImages) {
                    im.setVisible(false);
                }
                stepImages[step].setVisible(true);
            }
        });
    }

    private int getMultiplicator() {
        if(TablexiaSettings.getInstance().isUseHdAssets()) return 3;
        else return 1;
    }

    public void switchToStep(int step) {
        dragSwitchListener.switchToStep(step);
    }

    public void setDisabledStep(int step) {
        dragSwitchListener.setDisabledStep(step);
    }

    public void setSwitchSelectedListener(DragSwitchListener.SwitchSelectedListener switchSelectedListener) {
        dragSwitchListener.setSwitchSelectedListener(switchSelectedListener);
    }

    public void setSwitchMovedListener(DragSwitchListener.SwitchMovedListener switchMovedListener) {
        dragSwitchListener.setSwitchMovedListener(switchMovedListener);
    }

    public static class DragSwitchListener extends ClickListenerWithSound {
        private float grabX, startX;

        private final float maxX, minX;
        private final float[]                snapPoints;
        private final Actor                  actor;
        private       SwitchMovedListener    switchMovedListener;
        private       SwitchSelectedListener switchSelectedListener;
        private       Integer                disabledPoint;

        public DragSwitchListener(Actor actor, float minX, float maxX, float... snapPoints) {
            this.actor = actor;
            this.minX = minX;
            this.maxX = maxX;
            this.snapPoints = snapPoints == null ? new float[0] : snapPoints;
        }

        // TODO add callback method
        private void moveBack() {
            actor.addAction(Actions.moveTo(startX, actor.getY(), 0.5f, Interpolation.pow2));
        }

        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if(button == Input.Buttons.RIGHT) return false;
            super.touchDown(event, x, y, pointer, button);
            grabX = x;
            startX = actor.getX();
            event.stop();
            return true;
        }

        @Override
        public void touchDragged(InputEvent event, float x, float y, int pointer) {
            super.touchDragged(event, x, y, pointer);
            float bx = actor.getX() + (x - grabX);
            if (bx > minX && bx < maxX) {
                actor.setPosition(bx, actor.getY());
                if (switchMovedListener != null) {
                    switchMovedListener.movedToStep(getStep(true));
                }
            }
            event.stop();
        }

        @Override
        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            super.touchUp(event, x, y, pointer, button);
            actor.addAction(Actions.moveTo(getClosestValue(false), actor.getY()));
            if (switchMovedListener != null) {
                switchMovedListener.movedToStep(getStep(false));
            }
            if (switchSelectedListener != null) {
                switchSelectedListener.stepSelected(getStep(false));
            }
            event.stop();
        }

        //Sound effect is played when touching up switch, not on click
        @Override
        public void clicked (InputEvent event, float x, float y) {
        }

        private int getStep(boolean useDisabled) {
            float closest = getClosestValue(useDisabled);

            for (int i = 0; i < snapPoints.length; i++) {
                if (snapPoints[i] == closest) {
                    return i + 1;
                }
            }
            return closest == minX ? 0 : snapPoints.length + 1;
        }

        private float getX(int step) {
            if (step == 0) {
                return minX;
            } else if (step <= snapPoints.length) {
                return snapPoints[step - 1];
            }
            return maxX;
        }


		public void switchToStep(int step, boolean triggerEvent) {
			actor.setX(getX(step));
			if (switchMovedListener != null && triggerEvent) {
				switchMovedListener.movedToStep(getStep(true));
			}
			if (switchSelectedListener != null && triggerEvent) {
				switchSelectedListener.stepSelected(getStep(true));
			}
		}

		public void switchToStep(int step) {
			switchToStep(step, true);
        }

        private float getClosestValue(boolean useDisabled) {
            float left = minX;
            float right = maxX;

            // find closest snap points
            for (int i = 0; i < snapPoints.length; i++) {
                float snap = snapPoints[i];
                if (!useDisabled && disabledPoint != null && i == (disabledPoint.intValue() - 1)) { // snap points are indexed without border values - indexes are -1
                    continue;
                }
                if (snap > actor.getX() && ((snap - actor.getX()) < (right - actor.getX()))) {
                    right = snap;
                } else if (snap <= actor.getX() && ((actor.getX() - snap) < (actor.getX() - left))) {
                    left = snap;
                }
            }
            float middle = left + ((right - left) / 2);
            return actor.getX() > middle ? right : left;
        }

        public SwitchMovedListener getSwitchMovedListener() {
            return switchMovedListener;
        }

        public void setDisabledStep(Integer disabledPoint) {
            this.disabledPoint = disabledPoint;
        }

        public void setSwitchMovedListener(SwitchMovedListener switchMovedListener) {
            this.switchMovedListener = switchMovedListener;
        }

        public SwitchSelectedListener getSwitchSelectedListener() {
            return switchSelectedListener;
        }

        public void setSwitchSelectedListener(SwitchSelectedListener switchSelectedListener) {
            this.switchSelectedListener = switchSelectedListener;
        }

        public interface SwitchMovedListener {
            void movedToStep(int step);
        }

        public interface SwitchSelectedListener {
            void stepSelected(int step);
        }
    }
}