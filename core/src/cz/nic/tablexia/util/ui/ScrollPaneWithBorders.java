/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;

/**
 * Created by drahomir on 4/4/16.
 */
public class ScrollPaneWithBorders extends SpecialScrollPane{
    private static final float DESKTOP_SCROLL_SIZE =    10f;
    private static final float DEFAULT_SCROLL_SIZE =    3f;

    private static final Color SCROLL_COLOR        =    new Color(0.f,0,0,0.1f);
    private static final Color KNOB_COLOR          =    new Color(0,0,0,0.35f);

    private float lastScrollX;
    private Image leftBorder, rightBorder;

    private boolean singleBorder = false;

    public ScrollPaneWithBorders(Actor widget, Image border) {
        this(widget, border, null);
    }

    public ScrollPaneWithBorders(Actor widget, Image leftBorder, Image rightBorder) {
        super(widget);
        lastScrollX = getVisualScrollX();

        this.leftBorder = leftBorder;
        this.rightBorder = rightBorder;

        leftBorder.setVisible(false);

        if(rightBorder != null) {
            rightBorder.setVisible(false);
        }
        else {
            singleBorder = true;
            this.rightBorder = leftBorder;
        }
        this.setStyle(initScrollPaneStyle());
        this.setFadeScrollBars(false);
    }

    private ScrollPaneStyle initScrollPaneStyle() {
        SpecialScrollPane.ScrollPaneStyle style = new SpecialScrollPane.ScrollPaneStyle();
        style.hScrollKnob = getScrollRegionDrawable(KNOB_COLOR);
        style.hScroll = getScrollRegionDrawable(SCROLL_COLOR);
        return style;
    }

    private TextureRegionDrawable getScrollRegionDrawable(Color color){
        TextureRegionDrawable regionDrawable = new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getColorTextureRegion(color));
        regionDrawable.setMinHeight(TablexiaSettings.getInstance().isRunningOnMobileDevice() ? DEFAULT_SCROLL_SIZE : DESKTOP_SCROLL_SIZE);
        return regionDrawable;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(lastScrollX != getVisualScrollX()) {
            lastScrollX = getVisualScrollX();

            if(isRightEdge()) {
                rightBorder.setVisible(true);
                float posX = getX() + getWidth() - (getVisualScrollX() - (getWidget().getWidth() - getWidth()));
                rightBorder.setPosition(MathUtils.floor(posX), getY());
                rightBorder.setSize(rightBorder.getWidth(), getHeight());
            }
            else if(isLeftEdge()) {
                leftBorder.setVisible(true);
                float posX = getX() - leftBorder.getWidth() - getVisualScrollX();
                leftBorder.setPosition(MathUtils.ceil(posX), getY());
                leftBorder.setSize(leftBorder.getWidth(), getHeight());
            }
            else {
                leftBorder.setVisible(false);
                rightBorder.setVisible(false);
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        if(getLeftBorder().isVisible())
            getLeftBorder().draw(batch, parentAlpha);

        if(getRightBorder().isVisible())
            getRightBorder().draw(batch, parentAlpha);
    }

    public boolean isSingleBorder() {
        return singleBorder;
    }

    public Image getLeftBorder() {
        return leftBorder;
    }

    public Image getRightBorder() {
        return rightBorder;
    }
}
