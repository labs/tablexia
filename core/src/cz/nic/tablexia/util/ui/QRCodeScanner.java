/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.Gdx;

import cz.nic.tablexia.util.Log;

public abstract class QRCodeScanner {
    public enum CameraPermissionStatus {
        Allowed(true, false),
        Denied(false, true),
        NotDetermined(false, true);

        private boolean scanningAllowed;
        private boolean requestsPermissionChange;

        CameraPermissionStatus(boolean scanningAllowed, boolean requestsPermissionChange) {
            this.scanningAllowed = scanningAllowed;
            this.requestsPermissionChange = requestsPermissionChange;
        }

        public boolean isScanningAllowed() {
            return scanningAllowed;
        }

        public boolean requestsPermissionChange() {
            return requestsPermissionChange;
        }
    }

    public interface QRCodeListener {
        void onCodeScanned(String data);
        void scannerCanceled();
    }

    private   QRCodeListener         listener;
    private   boolean                scanningEnabled = false;
    protected boolean                cameraPreviewActive = false;
    protected CameraPermissionStatus cameraPermissionStatus = CameraPermissionStatus.NotDetermined;

    public void enableScanning() {
        scanningEnabled = true;
    }
    
    public void disableScanning() {
        scanningEnabled = false;
    }
    
    public boolean isScanningEnabled() {
        return scanningEnabled;
    }
    
    public boolean isCameraPreviewActive() {
        return cameraPreviewActive;
    }
    
    public void setQRCodeListener(QRCodeListener listener) {
        this.listener = listener;
    }
    
    public void removeQRCodeListener() {
        this.listener = null;
    }
    
    public synchronized void onCodeScanned(final String data) {
        if(!isScanningEnabled()) return;
        
        if(listener != null) {
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() { listener.onCodeScanned(data); }
            });
        }
    }

    public void cancelScanner(){
        if(listener != null){
            listener.scannerCanceled();
        }
    }
    
    public void startCameraPreview() {
        if(isCameraPreviewActive()) {
            Log.info(getClass(), "Camera preview is already active. Cannot start it again!");
            return;
        }

        cameraPermissionStatus = refreshCameraPermission();
        if(!cameraPermissionStatus.isScanningAllowed()) {
            if(cameraPermissionStatus.requestsPermissionChange()) {
                Log.info(getClass(), "Requesting permission setting change!");
                requestPermissionSettingChange();
            } else {
                Log.info(getClass(), "Scanning is not allowed, but permission change is not requested!");
            }
            return;
        }

        Log.info(getClass(), "Starting Camera Preview!");
        
        cameraPreviewActive = true;
        onCameraPreviewStarted();
    }
    
    public void stopCameraPreview() {
        if(!isCameraPreviewActive()) {
            Log.info(getClass(), "Camera preview is not running. Cannot stop it!");
            return;
        }
        
        Log.info(getClass(), "Stopping Camera Preview");
        
        onCameraPreviewStopped();
        cameraPreviewActive = false;
    }

    public CameraPermissionStatus getCameraPermissionStatus() {
        return cameraPermissionStatus;
    }

    public abstract void onCameraPreviewStarted();
    public abstract void onCameraPreviewStopped();

    protected abstract CameraPermissionStatus refreshCameraPermission();
    protected abstract void requestPermissionSettingChange();
}