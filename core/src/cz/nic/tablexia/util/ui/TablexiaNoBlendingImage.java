/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;

public class TablexiaNoBlendingImage extends Image {

    public TablexiaNoBlendingImage() {
    }

    public TablexiaNoBlendingImage(NinePatch patch) {
        super(patch);
    }

    public TablexiaNoBlendingImage(TextureRegion region) {
        super(region);
    }

    public TablexiaNoBlendingImage(Texture texture) {
        super(texture);
    }

    public TablexiaNoBlendingImage(Skin skin, String drawableName) {
        super(skin, drawableName);
    }

    public TablexiaNoBlendingImage(Drawable drawable) {
        super(drawable);
    }

    public TablexiaNoBlendingImage(Drawable drawable, Scaling scaling) {
        super(drawable, scaling);
    }

    public TablexiaNoBlendingImage(Drawable drawable, Scaling scaling, int align) {
        super(drawable, scaling, align);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.disableBlending();
        super.draw(batch, parentAlpha);
        batch.enableBlending();
    }

}
