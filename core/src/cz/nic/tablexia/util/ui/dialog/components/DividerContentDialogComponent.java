/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;

import cz.nic.tablexia.loader.application.ApplicationAtlasManager;

/**
 * Created by Matyáš Latner.
 */
public class DividerContentDialogComponent extends ImageContentDialogComponent {

    private static final float  DEFAULT_PADDING     = 1 / 20f;
    private static final float  DEFAULT_CELL_HEIGHT = 1f;
    private static final Color  DEFAULT_COLOR       = Color.BLACK;

    private Float   width;
    private Float   height;

    private Color   color;

    public DividerContentDialogComponent() {
        this(DEFAULT_CELL_HEIGHT);
    }

    public DividerContentDialogComponent(Color color) {
        this(DEFAULT_CELL_HEIGHT, null, color);
    }

    public DividerContentDialogComponent(Float height) {
        this(height, null, DEFAULT_COLOR);
    }

    public DividerContentDialogComponent(Float height, Float width) {
        this(height, width, DEFAULT_COLOR);
    }

    public DividerContentDialogComponent(Float height, Float width, Color color) {
        super(new Image(ApplicationAtlasManager.getInstance().getColorTextureRegion(color)),
                Scaling.stretch,
                1f);

        this.height = height;
        this.width  = width;
    }

    @Override
    public void sizeChanged() {
        Cell<Image> imageCell = getImageCell();
        if (imageCell != null) {
            Float cellWidth = width;
            if (cellWidth == null) {
                cellWidth = getDialog().getWidth();
                cellWidth = cellWidth - cellWidth * DEFAULT_PADDING;
            }
            imageCell.width(cellWidth).height(height).colspan(imageCell.getTable().getColumns());
        }
    }
}
