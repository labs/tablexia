/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class ScrollPaneDialogComponent extends TablexiaDialogComponentAdapter {

    private ScrollPane scrollPane;

    public ScrollPaneDialogComponent(Table table) {
        this(table, true, false);
    }
    
    public ScrollPaneDialogComponent(Table table, boolean disableScrollX, boolean disableScrollY) {
        scrollPane = new ScrollPane(table);
        scrollPane.setScrollingDisabled(disableScrollX, disableScrollY);
    }

    @Override
    public void prepareContent(Cell content) {
        content.setActor(scrollPane);
        super.prepareContent(content);
    }
}
