/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;

/**
 * Created by Matyáš Latner.
 */
public class TablexiaComponentDialog extends Stack {

    public enum TablexiaDialogType {

        BUBBLE_SQUARE(ApplicationAtlasManager.DIALOG_BUBBLE_CLASSIC_SQUARE, false, 0, 0, 0, 0),
        BUBBLE_ROUNDED(ApplicationAtlasManager.DIALOG_BUBBLE_CLASSIC_ROUND, false, 0, 0, 0, 0),
        DIALOG_SQUARE(ApplicationAtlasManager.DIALOG_SQUARE, false, 0, 0, 0, 0),
        DIALOG_SQUARE_BORDERS(Tablexia.ERROR_DIALOG_BACKGROUND_INTERNAL, true, 23, 26, 18, 28),
        DIALOG_RECTANGLE(ApplicationAtlasManager.DIALOG_RECTANGLE, false, 0, 0, 0, 0),
        DIALOG_BADGE_BOARD(ApplicationAtlasManager.DIALOG_BADGE_BOARD, false, 0, 0, 0, 0),
        DIALOG_SQUARE_SHADOW(ApplicationAtlasManager.DIALOG_BUBBLE_CLASSIC_SQUARE_SHADOW, false, 0, 0, 0 ,0);

        private String backgroundPath;
        private boolean internalMemory;

        /*
           Since we are not using texture atlases for internal assets
           we need to store padding for Ninepatch somewhere else ;)
         */
        public int leftPad, rightPad, topPad, botPad;

        TablexiaDialogType(String backgroundPath, boolean internalMemory, int leftPad, int rightPad, int topPad, int botPad) {
            this.backgroundPath = backgroundPath;
            this.internalMemory = internalMemory;

            this.leftPad = leftPad;
            this.rightPad = rightPad;
            this.topPad = topPad;
            this.botPad = botPad;
        }

        public boolean isInternalMemory() {
            return internalMemory;
        }

        public String getBackgroundPath() {
            return backgroundPath;
        }
    }

    private static final float                  DIALOG_HEIGHT_RATIO                 = 1f / 4;
    private static final float                  DIALOG_WIDTH_RATIO                  = 3f / 2;

    private static final int                    DIALOG_CONTENT_ALIGN                = Align.bottomLeft;
    private static final Texture.TextureFilter  INTERNAL_TEXTURE_DEFAULT_FILTER     = Texture.TextureFilter.Linear;

    private static final float                  FADE_IN_DURATION                    = 0.5f;
    private static final Interpolation.ExpOut   FADE_IN_INTERPOLATION               = Interpolation.exp10Out;
    private static final float                  FADE_OUT_DURATION                   = 0.2f;
    private static final Interpolation          FADE_OUT_INTERPOLATION              = Interpolation.linear;
    public  static final boolean                DEFAULT_HIDE_ANIMATION              = true;

    private float               originalX;
    private float               originalY;
    private float               originalWidth;
    private float               originalHeight;

	//Called when size for every component has been computed (happens after first draw)
	private Runnable            componentsSizeComputedCallback;
	private boolean             componentsSizeComputed = false;

    private TablexiaDialogType dialogType;

    protected Group               backgroundLayer;
    protected Group               contentBackgroundLayer;
    protected Container<Table>    contentLayer;
    protected Table               content;

    protected Image               contentBackgroundImage;
    protected NinePatch           contentBackgroundPatch;
	private boolean				  componentsInitiated = false;

    protected List<TablexiaDialogComponentAdapter>    dialogComponents;

    private ShowDialogListener dialogListener;

    protected TablexiaComponentDialog() {
        //
    }

    TablexiaComponentDialog(Stage stage, TablexiaDialogType dialogType, TablexiaDialogComponentAdapter... preparedDialogComponents) {
        setStage(stage);
        dialogComponents = new ArrayList<TablexiaDialogComponentAdapter>();
        for (TablexiaDialogComponentAdapter dialogComponent: preparedDialogComponents) {
            if (dialogComponent != null) {
                dialogComponent.setDialog(this);
                dialogComponents.add(dialogComponent);
            }
        }

        this.dialogType = dialogType;

        backgroundLayer         = new Group();
        contentBackgroundLayer  = new Group();
        contentLayer            = new Container<Table>();
        content                 = new Table();

        contentLayer.setActor(content);

        add(contentBackgroundLayer);
        add(contentLayer);
    }

    public NinePatch getContentBackgroundPatch() {
        return contentBackgroundPatch;
    }

    protected void init() {
        for (TablexiaDialogComponentAdapter tablexiaDialogComponentAdapter: dialogComponents) {
            tablexiaDialogComponentAdapter.prepareBackground(backgroundLayer);
        }


        if(dialogType.isInternalMemory()) {
            Texture texture = new Texture(Gdx.files.internal(dialogType.getBackgroundPath()));
            texture.setFilter(INTERNAL_TEXTURE_DEFAULT_FILTER, INTERNAL_TEXTURE_DEFAULT_FILTER);
            contentBackgroundPatch = new NinePatch(
                    texture,
                    dialogType.leftPad,
                    dialogType.rightPad,
                    dialogType.topPad,
                    dialogType.botPad
            );
        } else {
            contentBackgroundPatch = ApplicationAtlasManager.getInstance().getPatch(dialogType.getBackgroundPath());
        }

        contentBackgroundImage = new Image(contentBackgroundPatch);
        contentBackgroundLayer.addActor(contentBackgroundImage);
        contentLayer.align(DIALOG_CONTENT_ALIGN);
        content.setFillParent(true);

        for (TablexiaDialogComponentAdapter tablexiaDialogComponentAdapter: dialogComponents) {
            tablexiaDialogComponentAdapter.prepareContent(content.add());
            content.row();
        }
    }

    public void show() {
        float height = TablexiaSettings.getViewportHeight(getStage()) * DIALOG_HEIGHT_RATIO;
        float width = height * DIALOG_WIDTH_RATIO;
        show(width, height);
    }

	public void show(float width, float height) {
		show(width, height, true);
	}

    public void show(float width, float height, Stage stage) {
        setStage(stage);
        show(width, height);
    }

    public void show(final float width, final float height, final boolean withFadeAnim) {
        final String dialogName = this.getName();


		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {

				if (!componentsInitiated) {
					componentsInitiated = true;
					init();
				}

				setSize(width, height);

				if (withFadeAnim) {
					addAction(Actions.alpha(0f));
					backgroundLayer.addAction(Actions.alpha(0f));
				}

				getStage().addActor(backgroundLayer);
				getStage().addActor(TablexiaComponentDialog.this);
				for (TablexiaDialogComponentAdapter tablexiaDialogComponentAdapter : dialogComponents) {
					tablexiaDialogComponentAdapter.show();
				}
				registerInputListener();
				dialogShowed();

				if (withFadeAnim) {
                    backgroundLayer.addAction(Actions.fadeIn(FADE_IN_DURATION,FADE_IN_INTERPOLATION));
                    addAction(Actions.sequence(Actions.addAction(Actions.fadeIn(FADE_IN_DURATION, FADE_IN_INTERPOLATION)), Actions.run(new Runnable() {
                                @Override
                                public void run() {
                                    triggerDialogVisibleEvent(dialogName);
                                }
                            }))
                    );
				}
                else { triggerDialogVisibleEvent(dialogName); }
			}
		});
    }

    private void triggerDialogVisibleEvent(String dialogName){
        ApplicationBus.getInstance().post(new DialogVisibleEvent(true, dialogName)).asynchronously();
    }

    public void hide() {
        hide(DEFAULT_HIDE_ANIMATION);
    }

    public void hide(boolean animated) {
        componentsSizeComputed = true; //prevent recalculating the size of newly created dialog if going to hide it right after showing
        dialogHided();

        if (animated) {
            backgroundLayer.addAction(Actions.sequence(Actions.fadeOut(FADE_OUT_DURATION, FADE_OUT_INTERPOLATION)));
            addAction(Actions.sequence(Actions.fadeOut(FADE_OUT_DURATION, FADE_OUT_INTERPOLATION), Actions.run(new Runnable() {
                @Override
                public void run() {
                    performHideActions();
                }
            })));
        } else {
            performHideActions();
        }
    }

    private void performHideActions() {
        for (TablexiaDialogComponentAdapter tablexiaDialogComponentAdapter : dialogComponents) {
            tablexiaDialogComponentAdapter.hide();
        }
        unregisterInputListener();
        backgroundLayer.remove();
        remove();
        ApplicationBus.getInstance().post(new DialogVisibleEvent(false)).asynchronously();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        //Call beforeDraw for every component
        for (TablexiaDialogComponentAdapter tablexiaDialogComponentAdapter : dialogComponents) {
            tablexiaDialogComponentAdapter.beforeDraw();
        }

        //Draw components themselves
        super.draw(batch, parentAlpha);

        //Draw additions for each of the components
        for (TablexiaDialogComponentAdapter tablexiaDialogComponentAdapter : dialogComponents) {
            tablexiaDialogComponentAdapter.drawAdditions(batch, parentAlpha);
        }

        //Call afterDraw for all components
        for (TablexiaDialogComponentAdapter tablexiaDialogComponentAdapter : dialogComponents) {
            tablexiaDialogComponentAdapter.afterDraw();
        }

		if (!componentsSizeComputed && componentsSizeComputedCallback != null) {
			componentsSizeComputed = true;
			componentsSizeComputedCallback.run();
		}
    }

    public void processSizeThresholdChanged() {
        for (TablexiaDialogComponentAdapter tablexiaDialogComponentAdapter : dialogComponents) {
            tablexiaDialogComponentAdapter.adaptiveSizeThresholdChanged();
        }
    }


//////////////////////////// INPUT HANDLING

    private InputListener inputListener = new InputListener() {

        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if(button == Input.Buttons.RIGHT) return false;
            for (TablexiaDialogComponentAdapter tablexiaDialogComponentAdapter : dialogComponents) {
                tablexiaDialogComponentAdapter.dialogTouched(x, y);
            }
            return true;
        }
    };

    private void registerInputListener() {
        getStage().addListener(inputListener);
    }

    private void unregisterInputListener() {
        if(getStage() != null)
            getStage().removeListener(inputListener);
    }


//////////////////////////// SIZE CHANGING

    @Override
    protected void sizeChanged() {
        if (contentBackgroundImage != null) {
            contentBackgroundImage.setPosition(-contentBackgroundPatch.getPadLeft(), -contentBackgroundPatch.getPadBottom());
            contentBackgroundImage.setSize(contentBackgroundPatch.getPadLeft() + getWidth() + contentBackgroundPatch.getPadRight(), contentBackgroundPatch.getPadBottom() + getHeight() + contentBackgroundPatch.getPadTop());
        }
        if (dialogComponents != null) {
            for (TablexiaDialogComponentAdapter tablexiaDialogComponentAdapter : dialogComponents) {
                tablexiaDialogComponentAdapter.sizeChanged();
            }
        }
        super.sizeChanged();
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        originalX = x;
        originalY = y;
        originalWidth = width;
        originalHeight = height;
        super.setBounds(x, y, width, height);
    }

    @Override
    public void setSize(float width, float height) {
        originalWidth = width;
        originalHeight = height;
        super.setSize(width, height);
    }

    @Override
    public void setWidth(float width) {
        originalWidth = width;
        super.setWidth(width);
    }

    @Override
    public void setHeight(float height) {
        originalHeight = height;
        super.setHeight(height);
    }

    @Override
    public void setPosition(float x, float y) {
        originalX = x;
        originalY = y;
        super.setPosition(x, y);
    }

    @Override
    public void setPosition(float x, float y, int alignment) {
        originalX = x;
        originalY = y;
        super.setPosition(x, y, alignment);
    }

    @Override
    public void setX(float x) {
        originalX = x;
        super.setX(x);
    }

    @Override
    public void setY(float y) {
        originalY = y;
        super.setY(y);
    }

    public float getOriginalX() {
        return originalX;
    }

    public float getOriginalY() {
        return originalY;
    }

    public float getOriginalWidth() {
        return originalWidth;
    }

    public float getOriginalHeight() {
        return originalHeight;
    }

    public void setTemporaryBounds(float x, float y, float width, float height) {
        super.setBounds(x, y, width, height);
    }

    public void setTemporarySize(float width, float height) {
        super.setSize(width, height);
    }

    public void setTemporaryPosition(float x, float y) {
        super.setPosition(x, y);
    }

    public void setTemporaryX(float x) {
        super.setX(x);
    }

    public void setTemporaryY(float y) {
        super.setY(y);
    }

    public void setTemporaryWidth(float width) {
        super.setWidth(width);
    }

    public void setTemporaryHeight(float height) {
        super.setHeight(height);
    }

    public float getInnerWidth() {
        return getWidth();
    }

    public float getOutterWidth() {
        return getWidth() + contentBackgroundPatch.getPadLeft() + contentBackgroundPatch.getPadRight();
    }

    public float getInnerHeight() {
        return getHeight();
    }

    public float getOutterHeight() {
        return getHeight() + contentBackgroundPatch.getPadBottom() + content.getPadTop();
    }

    public float getInnerLeftX() {
        return getX();
    }

    public float getOutterLeftX() {
        return getInnerLeftX() - contentBackgroundPatch.getPadLeft();
    }

    public float getInnerRightX() {
        return getInnerLeftX() + getInnerWidth();
    }

    public float getOutterRightX() {
        return getOutterLeftX() + getOutterWidth();
    }

    public float getInnerBottomY() {
        return getY();
    }

    public float getOutterBottomY() {
        return getInnerBottomY() - contentBackgroundPatch.getPadBottom();
    }

    public float getInnerTopY() {
        return getInnerBottomY() + getInnerHeight();
    }

    public float getOutterTopY() {
        return getOutterBottomY() + getOutterHeight();
    }

    public NinePatch getBg() {
        return contentBackgroundPatch;
    }

	public void setComponentsSizeComputedCallback(Runnable componentsSizeComputedCallback) {
		this.componentsSizeComputedCallback = componentsSizeComputedCallback;
	}

	public List<TablexiaDialogComponentAdapter> getDialogComponents() {
		return dialogComponents;
	}

//////////////////////////// SHOW LISTENER


    public interface ShowDialogListener{
        void dialogShowed();
        void dialogHided();
    }

    public void setDialogListener(ShowDialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }

    private void dialogShowed(){
        if(dialogListener != null)
            dialogListener.dialogShowed();
    }

    private void dialogHided(){
        if (dialogListener != null)
            dialogListener.dialogHided();
    }

//////////////////////////// PAUSE EVENT

    public static class DialogVisibleEvent implements ApplicationBus.ApplicationEvent {

        private static int visibleDialogCount = 0;
        String dialogName = null;

        public String getDialogName() { return dialogName; }

        private DialogVisibleEvent(boolean show, String dialogName) {
            this.dialogName=dialogName;
            countDialogs(show);
        }

        private DialogVisibleEvent(boolean show) {
            countDialogs(show);
        }

        private void countDialogs(boolean show){
            if (show) {
                visibleDialogCount++;
            } else {
                if (visibleDialogCount <= 0) {
                    visibleDialogCount = 0;
                } else {
                    visibleDialogCount--;
                }
            }
        }

        public static boolean isDialogVisible() {
            return visibleDialogCount > 0;
        }

        public static void clearVisibleDialogCount(){
            visibleDialogCount = 0;
        }
    }
}
