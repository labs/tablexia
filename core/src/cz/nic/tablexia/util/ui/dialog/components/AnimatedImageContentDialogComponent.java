/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.utils.Scaling;

import cz.nic.tablexia.util.ui.AnimatedImage;

/**
 * Created by Matyáš Latner.
 */
public class AnimatedImageContentDialogComponent extends ImageContentDialogComponent {

    public AnimatedImageContentDialogComponent(AnimatedImage animatedImage) {
        super(animatedImage);
    }

    public AnimatedImageContentDialogComponent(AnimatedImage animatedImage, Scaling scaling) {
        super(animatedImage, scaling);
    }

    public AnimatedImageContentDialogComponent(AnimatedImage animatedImage, Scaling scaling, Float widthRatio) {
        super(animatedImage, scaling, widthRatio);
    }

    public AnimatedImageContentDialogComponent(AnimatedImage animatedImage, Scaling scaling, Float widthRatio, Float heightRatio) {
        super(animatedImage, scaling, widthRatio, heightRatio);
    }
}
