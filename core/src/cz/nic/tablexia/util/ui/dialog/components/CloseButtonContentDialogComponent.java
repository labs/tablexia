/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Matyáš Latner.
 */
public class CloseButtonContentDialogComponent extends SingleButtonContentDialogComponent {

    private InputListener inputListener;

    private InputListener closeListener = new ClickListener() {

        @Override
        public boolean handle(Event e) {
            boolean result = super.handle(e);
            if (inputListener != null) {
                inputListener.handle(e);
            }
            return result;
        }

        @Override
        public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            if (getDialog() != null) {
                getDialog().hide();
            }
        }
    };

    public CloseButtonContentDialogComponent(String buttonText) {
        super(buttonText);
    }

    @Override
    public SingleButtonContentDialogComponent setInputListener(InputListener inputListener) {
        this.inputListener = inputListener;
        return this;
    }

    @Override
    public void prepareContent(Cell content) {
        super.setInputListener(closeListener);
        super.prepareContent(content);
    }
}
