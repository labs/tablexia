/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;

/**
 * Created by Matyáš Latner.
 */
public class FixedSpaceContentDialogComponent extends TablexiaDialogComponentAdapter {

    private static final float  DEFAULT_HEIGHT_RATIO = 1f / 40;

    private     Float           heightRatio;
    protected   Cell            cell;

    public FixedSpaceContentDialogComponent() {
        this(DEFAULT_HEIGHT_RATIO);
    }

    public FixedSpaceContentDialogComponent(Float heightRatio) {
        this.heightRatio = heightRatio;
    }

    @Override
    public void prepareContent(Cell content) {
        cell = content;
    }

    @Override
    public void sizeChanged() {
        resize();
    }

    private void resize() {
        cell.minHeight(getDialog().getHeight() * heightRatio);
    }
}
