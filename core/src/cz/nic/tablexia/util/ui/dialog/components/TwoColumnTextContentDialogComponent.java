/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by Matyáš Latner.
 */
public class TwoColumnTextContentDialogComponent extends TextContentDialogComponent {

    private static final Color                               DEFAULT_FONT_COLOR              = Color.BLACK;
    private static final ApplicationFontManager.FontType     DEFAULT_FONT_TYPE               = ApplicationFontManager.FontType.REGULAR_20;
    private static final int                                 DEFAULT_ALIGN_1                 = Align.center;
    private static final int                                 DEFAULT_ALIGN_2                 = Align.center;

    private final   String                                  text2;
    private int                                             align2;
    private TablexiaLabel                                   textLabel;

    public TwoColumnTextContentDialogComponent(String text1, String text2) {
        this(text1, text2, DEFAULT_ALIGN_1, DEFAULT_ALIGN_2);
    }

    public TwoColumnTextContentDialogComponent(String text1, String text2, Integer align1, Integer align2) {
        this(text1, text2, DEFAULT_FONT_TYPE, DEFAULT_FONT_COLOR, align1, align2);
    }

    public TwoColumnTextContentDialogComponent(String text1, String text2, ApplicationFontManager.FontType fontType, Color fontColor, Integer align1, Integer align2) {
        super(text1, fontType, fontColor, align1, Float.valueOf(0f));
        this.text2 = text2;
        this.align2 = align2.intValue();
    }

    @Override
    public void adaptiveSizeThresholdChanged() {
        super.adaptiveSizeThresholdChanged();
        textLabel.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(getActualFontType(), getFontColor()));
    }

    @Override
    protected void prepareTextLabel(Cell content) {
        super.prepareTextLabel(content);
        textLabel = new TablexiaLabel(text2, new TablexiaLabel.TablexiaLabelStyle(getActualFontType(), getFontColor()));
        textLabel.setWrap(true);
        textLabel.setAlignment(align2);
        content.getTable().add().setActor(textLabel).fillX();
    }
}
