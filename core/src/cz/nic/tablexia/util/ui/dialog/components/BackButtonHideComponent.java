/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import net.engio.mbassy.listener.Handler;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;

/**
 * Created by frantisek on 23. 2. 2016.
 */
public class BackButtonHideComponent extends TablexiaDialogComponentAdapter {

    private boolean animated;

    public BackButtonHideComponent() {
        this(TablexiaComponentDialog.DEFAULT_HIDE_ANIMATION);
    }

    public BackButtonHideComponent(boolean animated) {
        this.animated = animated;
    }

    public void show() {
        super.show();
        ApplicationBus.getInstance().subscribe(this);
    }

    public void hide() {
        super.hide();
        ApplicationBus.getInstance().unsubscribe(this);
    }

    @Handler(priority = TablexiaApplication.BackButtonPressed.DIALOG_PRIORITY)
    public void onBackButtonPress(TablexiaApplication.BackButtonPressed event) {
		if (event.shouldProcess(TablexiaApplication.BackButtonPressed.DIALOG_PRIORITY)) {
			event.setProcessWithPriority(TablexiaApplication.BackButtonPressed.DIALOG_PRIORITY);
            hideDialog();
		}
    }

    protected void hideDialog() {
        getDialog().hide(animated);
    }
}
