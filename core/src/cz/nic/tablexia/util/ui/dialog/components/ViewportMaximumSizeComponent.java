/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

/**
 * Created by Matyáš Latner.
 */
public class ViewportMaximumSizeComponent extends TablexiaDialogComponentAdapter {

    private static final float  MAXIMUM_SIZE_BORDER_PADDING_RATIO   = 1f / 40;

    private float               originalPositionX;
    private float               originalPositionY;
    private float               originalWidth;
    private float               originalHeight;

    private float               lastScreenWidth;
    private float               lastScreenHeight;

    private boolean             isResizing;

    public ViewportMaximumSizeComponent() {
        this.isResizing = false;
    }

    @Override
    public void sizeChanged() {
        if (!isResizing) {
            originalWidth = getDialog().getWidth();
            originalHeight = getDialog().getHeight();
        }

        rememberScreenSize();
    }

    // TODO use event for screen size change handling
    @Override
    public void beforeDraw() {
        if (getStage().getWidth() != lastScreenWidth || getStage().getHeight() != lastScreenHeight) {
            rememberScreenSize();
            processSizeChanged();
        }
    }

    private void processSizeChanged() {
        isResizing = true;
        float viewportLeftX     = getViewportLeftX();
        float viewportBottomY   = getViewportBottomY();
        float viewportRightX    = getViewportRightX();
        float viewportTopY      = getViewportTopY();
        float borderOffset      = getViewportHeight() * MAXIMUM_SIZE_BORDER_PADDING_RATIO;

        originalPositionX = getDialog().getX();
        originalPositionY = getDialog().getY();
        float positionX = originalPositionX < viewportLeftX ? viewportLeftX + getDialog().getContentBackgroundPatch().getPadLeft() + borderOffset : originalPositionX;
        float positionY = originalPositionY < viewportBottomY ? viewportBottomY + getDialog().getContentBackgroundPatch().getPadBottom() + borderOffset : originalPositionY;

        float width = positionX + originalWidth > viewportRightX - getDialog().getContentBackgroundPatch().getPadRight() ? viewportRightX - positionX - getDialog().getContentBackgroundPatch().getPadRight() - borderOffset : originalWidth;
        float height = positionY + originalHeight > viewportTopY - getDialog().getContentBackgroundPatch().getPadTop() ? viewportTopY - positionY - getDialog().getContentBackgroundPatch().getPadTop() - borderOffset : originalHeight;

        getDialog().setTemporaryBounds(positionX, positionY, width, height);
        isResizing = false;
    }

    private void rememberScreenSize() {
        lastScreenWidth = getDialog().getWidth();
        lastScreenHeight = getDialog().getHeight();
    }
}
