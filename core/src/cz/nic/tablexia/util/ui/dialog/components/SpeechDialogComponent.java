/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.util.Point;

/**
 * This DialogComponent turns any TablexiaComponentDialog into a speech dialog (comics bubble like...) by adding
 * a simple arrow of your choice pointing to the point of your choice...
 *
 * I'm not responsible for any injury or damage to any of your personal stuff while
 * reading and/or trying to understand the code bellow... I'm sorry for the mess :(
 *
 * Created by Drahomir Karchnak on 2/4/16.
 */
public class SpeechDialogComponent extends TablexiaDialogComponentAdapter {
    public static final float STAGE_CULLING_PADDING = 8;
    private static final float SCALE_FACTOR          = 1.8f;

    private static final float DIALOG_ADJUST_STEP = 0.05f;

    private static final float DIALOG_ADJUST_OFFSET_MAX = 0.9f;
    private static final float DIALOG_ADJUST_OFFSET_MIN = 0.1f;
    private static final float DIALOG_ADJUST_STEP_END_OFFSET = 0.15f;

    /**
     * Defines Type of the arrow
     * basically the look...
     */
    public enum ArrowType {
        ARROW(ApplicationAtlasManager.DIALOG_BUBBLE_ARROW_LEFT, ApplicationAtlasManager.DIALOG_BUBBLE_ARROW_TOP, false),
        BEND_ARROW(ApplicationAtlasManager.DIALOG_BUBBLE_ARROW_BEND_LEFT_NEW, ApplicationAtlasManager.DIALOG_BUBBLE_ARROW_BEND_RIGHT_NEW, true),
        CLASSIC(ApplicationAtlasManager.DIALOG_BUBBLE_CLASSIC_LEFT_NEW, ApplicationAtlasManager.DIALOG_BUBBLE_CLASSIC_RIGHT_NEW, true),
        CLASSIC_SHADOW(ApplicationAtlasManager.DIALOG_CLASSIC_LEFT_SHADOW, ApplicationAtlasManager.DIALOG_CLASSIC_RIGHT_SHADOW, true);

        //Assets for the arrow
        private String leftAsset, rightAsset;

        //Whether or not the dialog using this arrow should be diagonal
        private boolean diagonal;

        ArrowType(String left, String right, boolean diagonal) {
            this.leftAsset = left;
            this.rightAsset = right;
            this.diagonal = diagonal;
        }
    }

    /**
     * Defines position of the arrow on the dialog
     */
    public enum ArrowPosition {
        //Diagonal position of arrow
        TOP_LEFT( 0.1f, 1f, -0.5f, 0, false, true),
        BOTTOM_LEFT( 0.1f, 0,  -0.5f, -1, false, false),
        TOP_RIGHT(0.9f, 1,  -0.5f, 0, false, true),
        BOTTOM_RIGHT(0.9f, 0,  -0.5f, -1, false, false),

        LEFT(0, 0.5f, -1f, -0.5f, false, false),
        RIGHT(1f, 0.5f, 0, -0.5f, true, false),
        TOP(0.5f, 1f, -0.5f, 0, false, false),
        BOT(0.5f, 0, -0.5f, -1f, false, true);

        /*
            These values get multiplied by the dialogs width or height
            and are used as an offset from the corner of the dialog...
         */
        float preferedDialogOffsetX, preferedDialogOffsetY;

        /*
            These values get multiplied by the arrows width or height
            and are used as an offset for the arrow itself...
         */
        float arrowOffsetX, arrowOffsetY;

        //Whether or not we need to flip the arrow texture, when drawing the arrow...
        boolean flipX, flipY;

        ArrowPosition(float offsetX, float offsetY, float arrowOffsetX, float arrowOffsetY, boolean flipX, boolean flipY) {
            this.preferedDialogOffsetX = offsetX;
            this.preferedDialogOffsetY = offsetY;
            this.arrowOffsetX = arrowOffsetX;
            this.arrowOffsetY = arrowOffsetY;
            this.flipX = flipX;
            this.flipY = flipY;
        }
    }

    //ArrowType and ArrowPosition
    private ArrowType arrowType;
    private ArrowPosition arrowPosition;

    //target point
    private Point target;

    //Original arrow
    private TextureRegion arrowTexture;
    private int originalArrowWidth = 0;
    private int originalArrowHeight = 0;

    //Offsets
    private Vector2 currentOffset;

    private float dialogOffsetX, dialogOffsetY;

    //Whether or not use diagonal type of the dialog
    private boolean useDiagonals;
    private boolean useOppositeDirection;

    /**
     * Constructor...
     * @param target target point
     * @param offset oofset between the dialog and the target point
     */
    public SpeechDialogComponent(Point target, Vector2 offset) {
        this(target, offset, ArrowType.BEND_ARROW);
    }

    /**
     * Constructor... Again...
     * @param target target point
     * @param offset offset between the dialog and the target point
     * @param arrowType arrow type
     */
    public SpeechDialogComponent(Point target, Vector2 offset, ArrowType arrowType) {
        this(target, offset, arrowType, false);
    }

    /**
     * Constructor
     * @param target target point
     * @param arrowType arrow type
     */
    public SpeechDialogComponent(Point target, ArrowType arrowType) {
        this(target, new Vector2(0,0), arrowType, false);
    }

    /**
     * Constructor... Again...
     * @param target target point
     * @param offset offset between the dialog and the target point
     * @param arrowType arrow type
     */
    public SpeechDialogComponent(Point target, Vector2 offset, ArrowType arrowType, boolean oppositeDir) {
        this.target = target;

        this.currentOffset = new Vector2(offset);

        this.arrowType = arrowType;
        this.useDiagonals = arrowType.diagonal;
        this.useOppositeDirection = oppositeDir;
    }

    /**
     * Draws arrows for the dialog
     * @param batch
     * @param parentAlpha
     */
    @Override
    public void drawAdditions(Batch batch, float parentAlpha) {
        String texture;

        if (useDiagonals) { //Using diagonal type of arrow
            if (arrowPosition == ArrowPosition.BOTTOM_LEFT || arrowPosition == ArrowPosition.TOP_LEFT) {
                texture = arrowType.leftAsset;
            } else {
                texture = arrowType.rightAsset;
            }
        }
        else { //Using straight type of arrow
            if(arrowPosition == ArrowPosition.TOP || arrowPosition == ArrowPosition.BOT) {
                texture = arrowType.rightAsset;
            }
            else {
                texture = arrowType.leftAsset;
            }
        }

        TextureRegion arrowTexture = ApplicationAtlasManager.getInstance().getTextureRegion(texture);

        batch.draw(arrowTexture.getTexture(),
                getDialog().getX() + dialogOffsetX * getDialog().getWidth() + (arrowPosition.arrowOffsetX * originalArrowWidth),
                getDialog().getY() + dialogOffsetY * getDialog().getHeight() + (arrowPosition.arrowOffsetY * originalArrowHeight),
                originalArrowWidth,
                originalArrowHeight,
                arrowTexture.getRegionX(),
                arrowTexture.getRegionY(),
                arrowTexture.getRegionWidth(),
                arrowTexture.getRegionHeight(),
                arrowPosition.flipX,
                arrowPosition.flipY
        );
    }

    @Override
    public void show() {
        updateArrowPosition();
        updateArrowTextureAndSize();
        updatePosition();
        super.show();
    }

    private void setArrowPosition(ArrowPosition arrowPosition) {
        if(useOppositeDirection)
            arrowPosition = getOppositeArrowPosition(arrowPosition);

        this.arrowPosition = arrowPosition;
        dialogOffsetX = arrowPosition.preferedDialogOffsetX;
        dialogOffsetY = arrowPosition.preferedDialogOffsetY;
    }

    /**
     * Updates arrows original width and height
     */
    private void updateArrowTextureAndSize() {
        String texture;

        if( arrowPosition == ArrowPosition.BOTTOM_LEFT ||
            arrowPosition == ArrowPosition.TOP_LEFT ||
            arrowPosition == ArrowPosition.LEFT ||
            arrowPosition == ArrowPosition.RIGHT
        ) {
            texture = arrowType.leftAsset;
        }
        else {
            texture = arrowType.rightAsset;
        }

        arrowTexture = ApplicationAtlasManager.getInstance().getTextureRegion(texture);
        originalArrowWidth = arrowTexture.getRegionWidth();
        originalArrowHeight = arrowTexture.getRegionHeight();
    }

    /**
     * Updates arrowPosition according to targetPoint and Stage
     */
    private void updateArrowPosition() {
        //TODO - Add rectangle instead of Stage...
        if(useDiagonals) {
            if (target.x < getStage().getWidth() / 2) { //Show it to the right of the target
                if (target.y < getStage().getHeight() / 2) { //Bottom
                    setArrowPosition(ArrowPosition.BOTTOM_LEFT);
                } else { //Top
                    setArrowPosition(ArrowPosition.TOP_LEFT);
                }
            } else { //Show it to the left of the target
                if (target.y < getStage().getHeight() / 2) { //Bottom
                    setArrowPosition(ArrowPosition.BOTTOM_RIGHT);
                } else { //Top
                    setArrowPosition(ArrowPosition.TOP_RIGHT);
                }
            }
        }
        else {
            if(isDialogOnTheTopHalfOfScreen(target.y)) { //Show it above the point
                setArrowPosition(ArrowPosition.BOT);
            }
            else { //Show it bellow the point
                setArrowPosition(ArrowPosition.TOP);
            }
        }
    }

    /**
     * Places dialog diagonally from the target
     * Use currentOffset vector to control dialogs offset from the target position
     */
    private void calculateDiagonalPosition() {
        switch (arrowPosition) {
            case TOP_LEFT:
                getDialog().setPosition(
                        target.x + currentOffset.x - (dialogOffsetX * getDialog().getWidth()) + originalArrowWidth/2,
                        target.y - getDialog().getHeight() - currentOffset.y - originalArrowHeight);
                break;
            case BOTTOM_LEFT:
                getDialog().setPosition(
                        target.x + currentOffset.x - (dialogOffsetX * getDialog().getWidth()) + originalArrowWidth/2,
                        target.y + currentOffset.y + originalArrowHeight);
                break;
            case TOP_RIGHT:
                getDialog().setPosition(
                        target.x - currentOffset.x - (dialogOffsetX * getDialog().getWidth()) - originalArrowWidth/2,
                        target.y - getDialog().getHeight() - currentOffset.y - originalArrowHeight);
                break;
            case BOTTOM_RIGHT:
                getDialog().setPosition(
                        target.x - currentOffset.x - (dialogOffsetX * getDialog().getWidth()) - originalArrowWidth/2,
                        target.y + currentOffset.y + originalArrowHeight);
                break;
            default:
                break;
        }
    }

    /**
     * Places dialog diagonally from the target
     * Use currentOffset vector to control dialogs offset from the target position
     * if dialog is out of screen when using  ArrowPosition's dialogOffsetValue
     * will try to position the dialog and arrow to fit the screen itself...
     */
    private void calculateDiagonalPositionAdjust() {
        //calculates the diagonal position
        calculateDiagonalPosition();

        //Opps we've reached the screens border...
        if(isDialogOutOfScreenLeftRight()) {
            //Let's move the arrow/dialog to the right/left few times and see if it helps...
            float step = DIALOG_ADJUST_STEP;

            //Arrow is on the right side, lets move it to the left (close to the middle)
            if(arrowPosition == ArrowPosition.BOTTOM_RIGHT || arrowPosition == ArrowPosition.TOP_RIGHT) {
                //Lets start with max x offset value (arrow all the way to the right)
                dialogOffsetX = DIALOG_ADJUST_OFFSET_MAX;

                //While we are getting closer to middle - step end offset
                while((dialogOffsetX-=step) >= 0.5f - DIALOG_ADJUST_STEP_END_OFFSET) {
                    //try to calculate the position again
                    calculateDiagonalPosition();

                    //I bet dialog is not outta screen now!
                    if(!isDialogOutOfScreenLeftRight()) {
                        //Great, escape this method!
                        return;
                    }
                }
            }
            //Arrow is on the left side of the dialog, moving to the right now (getting closer to middle)
            else {
                //Starting from MIN X OFFSET (arrow all the way to the left)
                dialogOffsetX = DIALOG_ADJUST_OFFSET_MIN;

                //As we are reaching the middle + step end offset
                while((dialogOffsetX+=step) <= 0.5f + DIALOG_ADJUST_STEP_END_OFFSET) {
                    //calculate the position again
                    calculateDiagonalPosition();

                    //Is the dialog with new position completely inside of the screen ?
                    if(!isDialogOutOfScreenLeftRight()) {
                        //Yuppp, bye!
                        return;
                    }
                }
            }

            //We couldn't find the proper position :/
            //TODO - Find the best fitting one instead of resetting the position
            dialogOffsetX = 0.5f;
            calculateDiagonalPosition();
        }
    }

    /**
     * Calculates whether top/down or left/right
     * position for the dialog...
     *
     * This method calculates Top or Down positon first
     * if the dialog is out of screen
     * calculates left or right position
     */
    private void calculateStraightPosition() {
        //Want to start by positioning speech dialog on top or bottom
        if(arrowPosition != ArrowPosition.TOP || arrowPosition != ArrowPosition.BOT) {
            //decide whether to use bot or top
            updateArrowPosition();
        }

        //calculate position
        calculateTopOrDown();

        //Opps dialog doesnt fit...
        if(isDialogOutOfScreen()) {
            //TODO - Add rectangle instead of Stage...
            //Should we draw it to the left or right then ?
            if(isDialogOnTheLeftHalfOfScreen(target.x))
                setArrowPosition(ArrowPosition.LEFT);
            else
                setArrowPosition(ArrowPosition.RIGHT);

            //We have to switch textures, get new texture and arrow size...
            updateArrowTextureAndSize();

            //Okey, position it to the left or right then...
            calculateLeftOrRightAdjust();
        }
    }

    public boolean isDialogOnTheLeftHalfOfScreen(float dialogX) {
        return dialogX < getStage().getWidth()/2;
    }

    public boolean isDialogOnTheTopHalfOfScreen(float dialogY) {
        return dialogY < getStage().getHeight()/2;
    }

    /**
     * Calculates Top or Down positon for the dialog
     * according to target position
     */
    private void calculateTopOrDown() {
        //TODO - Add rectangle instead of Stage...
        switch (arrowPosition) {
            case BOT:
                getDialog().setPosition(
                        target.x - (arrowPosition.preferedDialogOffsetX * getDialog().getWidth()),
                        target.y + currentOffset.y + originalArrowHeight);
                break;
            case TOP:
                getDialog().setPosition(
                        target.x - (arrowPosition.preferedDialogOffsetX * getDialog().getWidth()),
                        target.y - getDialog().getHeight() - currentOffset.y - originalArrowHeight);
                break;
            default:
                return;
        }
    }

    /**
     * Calculates Left or Right position for the dialog
     * according to target position
     */
    private void calculateLeftOrRight() {
        //TODO - Add rectangle instead of Stage...
        switch (arrowPosition) {
            case LEFT:
                getDialog().setPosition(
                        target.x - dialogOffsetX + currentOffset.x + originalArrowWidth,
                        target.y - (dialogOffsetY * getDialog().getHeight())
                );
                break;
            case RIGHT:
                getDialog().setPosition(
                        target.x - dialogOffsetX * getDialog().getWidth() - currentOffset.x - originalArrowWidth,
                        target.y - (dialogOffsetY * getDialog().getHeight())
                );
                break;
            default:
                return;
        }
    }

    /**
     * Calculates Left or Right position for the dialog
     * according to target position and adjusts its position (up/down)
     * when out of the screen
     */
    private void calculateLeftOrRightAdjust() {
        calculateLeftOrRight();

        //Oops we are out of the screen
        if(isDialogOutOfScreenTopDown()) {
            //Lets move dialog up or down few times by this step
            float step = DIALOG_ADJUST_STEP;

            //Starting value, middle of the dialog
            dialogOffsetY = 0.5f;

            //Dialog is bellow the screen (Moving arrow lower/raising dialog)
            if(target.y < getStage().getHeight()/2f) {
                //While we haven't reached min offset value (Arrow can be placed even lower)
                while((dialogOffsetY-=step) >= DIALOG_ADJUST_OFFSET_MIN) {
                    //Calculate new position
                    calculateLeftOrRight();

                    //We are not out of screen now.. awesome!
                    if(!isDialogOutOfScreenTopDown()) {
                        //Abort the mission!
                        return;
                    }
                }

                //We haven't managed to place dialog so its in screen completly...
                dialogOffsetY = DIALOG_ADJUST_OFFSET_MIN;
            }
            //Dialog is above the screen (Moving arrow higher/lowering the dialogs position)
            else {
                //While we haven't reached max offset value (Arrow can be placed higher)
                while((dialogOffsetY+=step) <= DIALOG_ADJUST_OFFSET_MAX) {
                    //Calculate new possition
                    calculateLeftOrRight();

                    //We are out of screen now...
                    if(!isDialogOutOfScreenTopDown())
                        //Terminate... :D
                        return;
                }

                //We couldn't find the proper position :(
                dialogOffsetY = DIALOG_ADJUST_OFFSET_MAX;
            }
        }

        //Didnt find the fitting one... Calculate position with lowest/highest possible offset
        calculateLeftOrRight();
    }

    /**
     * Simple AABB collision test to find out whether or not
     * the dialog is out of screen...
     * @return
     */
    private boolean isDialogOutOfScreen() {
        return isDialogOutOfScreenTopDown() || isDialogOutOfScreenLeftRight();
    }

    /**
     * Is dialog either to the left or to the right of the screen
     * @return
     */
    public boolean isDialogOutOfScreenLeftRight() {
        return  getDialog().getOutterRightX() > getStage().getWidth() - STAGE_CULLING_PADDING ||
                getDialog().getOutterLeftX() < STAGE_CULLING_PADDING;
    }

    /**
     * Has dialog reached top or bottom border of the window ?
     * @return
     */
    public boolean isDialogOutOfScreenTopDown() {
        return  getDialog().getOutterBottomY() < STAGE_CULLING_PADDING ||
                getDialog().getOutterTopY() > getStage().getHeight() - STAGE_CULLING_PADDING;
    }

    private ArrowPosition getOppositeArrowPosition(ArrowPosition pos) {
        switch (pos) {
            case TOP_LEFT:
                return ArrowPosition.BOTTOM_RIGHT;
            case BOTTOM_LEFT:
                return ArrowPosition.TOP_RIGHT;
            case TOP_RIGHT:
                return ArrowPosition.BOTTOM_LEFT;
            case BOTTOM_RIGHT:
                return ArrowPosition.TOP_LEFT;
            case LEFT:
                return ArrowPosition.RIGHT;
            case RIGHT:
                return ArrowPosition.LEFT;
            case TOP:
                return ArrowPosition.BOT;
            case BOT:
                return ArrowPosition.TOP;
            default:
                return pos;
        }
    }

    /**
     * Updates dialogs position
     */
    private void updatePosition() {
        if(useDiagonals)
            calculateDiagonalPositionAdjust();
        else
            calculateStraightPosition();
    }
}