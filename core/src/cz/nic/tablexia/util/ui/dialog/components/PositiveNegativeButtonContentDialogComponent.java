/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;

/**
 * Created by Matyáš Latner.
 */
public class PositiveNegativeButtonContentDialogComponent extends TwoButtonContentDialogComponent {

    private static final PositiveNegativeButtonType DEFAULT_POSITIVE_NEGATIVE_BUTTON_TYPE = PositiveNegativeButtonType.YES_NO;

    public enum PositiveNegativeButtonType {
        YES_NO              (ApplicationTextManager.ApplicationTextsAssets.SYSTEM_YES,      ApplicationTextManager.ApplicationTextsAssets.SYSTEM_NO),
        CONFIRM_DECLINE     (ApplicationTextManager.ApplicationTextsAssets.SYSTEM_CONFIRM,  ApplicationTextManager.ApplicationTextsAssets.SYSTEM_DECLINE),
        AGAIN_EXIT          (ApplicationTextManager.ApplicationTextsAssets.SYSTEM_RETRY,    ApplicationTextManager.ApplicationTextsAssets.SYSTEM_EXIT),
		AGAIN_BACK          (ApplicationTextManager.ApplicationTextsAssets.SYSTEM_RETRY,    ApplicationTextManager.ApplicationTextsAssets.SYSTEM_BACK),
        SETTINGS_DECLINE    (ApplicationTextManager.ApplicationTextsAssets.SYSTEM_SETTINGS, ApplicationTextManager.ApplicationTextsAssets.SYSTEM_DECLINE);

        private final String positiveText;
        private final String negativeText;

        PositiveNegativeButtonType(String positiveText, String negativeText) {
            this.positiveText = positiveText;
            this.negativeText = negativeText;
        }

        public String getPositiveText() {
            return positiveText;
        }

        public String getNegativeText() {
            return negativeText;
        }
    }

    public PositiveNegativeButtonContentDialogComponent(InputListener yesInputListener) {
        this(yesInputListener, DEFAULT_POSITIVE_NEGATIVE_BUTTON_TYPE);
    }

    public PositiveNegativeButtonContentDialogComponent(InputListener yesInputListener, PositiveNegativeButtonType positiveNegativeButtonType) {
        this(yesInputListener, null, positiveNegativeButtonType);
    }

    public PositiveNegativeButtonContentDialogComponent(InputListener yesInputListener, InputListener noInputListener) {
        this(yesInputListener, noInputListener, DEFAULT_POSITIVE_NEGATIVE_BUTTON_TYPE);
    }

    public PositiveNegativeButtonContentDialogComponent(InputListener yesInputListener, boolean yesButtonUseOnce, InputListener noInputListener, boolean noButtonUseOnce) {
        this(yesInputListener, yesButtonUseOnce, noInputListener, noButtonUseOnce, DEFAULT_POSITIVE_NEGATIVE_BUTTON_TYPE);
    }    

    public PositiveNegativeButtonContentDialogComponent(InputListener yesInputListener, boolean yesButtonUseOnce, InputListener noInputListener, boolean noButtonUseOnce, PositiveNegativeButtonType positiveNegativeButtonType) {
        this(yesInputListener, yesButtonUseOnce, ApplicationTextManager.getInstance().getText(positiveNegativeButtonType.getNegativeText()), 
                noInputListener, noButtonUseOnce,  ApplicationTextManager.getInstance().getText(positiveNegativeButtonType.getPositiveText()));
    }

    public PositiveNegativeButtonContentDialogComponent(InputListener yesInputListener, InputListener noInputListener, PositiveNegativeButtonType positiveNegativeButtonType) {
        this(yesInputListener, false, ApplicationTextManager.getInstance().getText(positiveNegativeButtonType.getPositiveText()),
                noInputListener, false, ApplicationTextManager.getInstance().getText(positiveNegativeButtonType.getNegativeText()));
    }

    public PositiveNegativeButtonContentDialogComponent(InputListener yesInputListener, boolean yesButtonUseOnce, String yesButtonText, InputListener noInputListener, boolean noButtonUseOnce, String noButtonText) {
        super(  noButtonText,
                yesButtonText,
                new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_NO_ICON)),
                new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_YES_ICON)),
                StandardTablexiaButton.TablexiaButtonType.RED,
                yesButtonUseOnce,
                StandardTablexiaButton.TablexiaButtonType.GREEN,
                noButtonUseOnce,
                noInputListener,
                yesInputListener
                );
    }
    
}