/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Scaling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.dialog.components.AdaptiveSizeDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.AlertOnShowDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.BackButtonHideComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DimmerDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.PositiveNegativeButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.SingleButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TouchCloseDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ViewportMaximumSizeComponent;

public class TablexiaComponentDialogFactory implements Disposable {

    public static final int         WARNING_DIALOG_WIDTH        = 400;
    public static final int         WARNING_DIALOG_HEIGHT       = 300;
    private static final Float      WARNING_DIALOG_TOP_PAD      = 1f/10;
    private static final Float      WARNING_DIALOG_BOT_PAD      = 1f/40;
    private static final float      WARNING_DIALOG_WIDTH_RATIO  = 3f/5;
    private static final float      WARNING_DIALOG_HEIGHT_RATIO = 1f/5;
    private static final Scaling    WARNING_DIALOG_SCALING      = Scaling.fill;

    private static final TablexiaComponentDialog.TablexiaDialogType DEFAULT_DIALOG_TYPE = TablexiaComponentDialog.TablexiaDialogType.DIALOG_SQUARE_BORDERS;

    private static TablexiaComponentDialogFactory instance;

    private Stage stage;

    private TablexiaComponentDialogFactory(Stage stage) {
        this.stage = stage;
    }

    public static TablexiaComponentDialogFactory getInstance() {
        if (instance == null) {
            String exceptionMessage = TablexiaComponentDialogFactory.class.getSimpleName() + " is not initialized!";
            Log.err(TablexiaSettings.class, exceptionMessage);
            throw new IllegalStateException(exceptionMessage);
        }
        return instance;
    }

    public static void init(Stage stage) {
        if (instance != null) {
            Log.info(TablexiaComponentDialogFactory.class, TablexiaComponentDialogFactory.class.getSimpleName() + " already initialized! Skipping initialization...");
            return;
        }

        instance = new TablexiaComponentDialogFactory(stage);
    }

    public static Stage getDialogStage() {
        return getInstance().stage;
    }

    @Override
    public void dispose() {
        instance = null;
    }

    public TablexiaComponentDialog createWarningYesNoDialog(String text, InputListener yesInputListener, boolean hideOnBackButton) {
        return createWarningYesNoDialog(text, yesInputListener, null, hideOnBackButton, true);
    }

    public TablexiaComponentDialog createExitDialog() {
        return TablexiaComponentDialogFactory.getInstance().createWarningYesNoDialog(
                ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.EXIT_MESSAGE),
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        Gdx.app.exit();
                    }
                },
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        Tablexia.getActualScreen().resetExitDialogState();
                    }
                },
                true, true);
    }

    public TablexiaComponentDialog createWarningYesNoDialog(String text, InputListener yesInputListener, InputListener noInputListener, boolean hideOnBackButton, boolean useOnce) {
        ArrayList<TablexiaDialogComponentAdapter> adapters = new ArrayList<TablexiaDialogComponentAdapter>();

        adapters.add(new FixedSpaceContentDialogComponent(WARNING_DIALOG_TOP_PAD));
        adapters.add(new ImageContentDialogComponent(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.DIALOG_WARNING), WARNING_DIALOG_SCALING, WARNING_DIALOG_WIDTH_RATIO, WARNING_DIALOG_HEIGHT_RATIO));

		if (hideOnBackButton) {
			adapters.add(new BackButtonHideComponent());
		}

        return createYesNoDialog(adapters, text, yesInputListener, noInputListener, useOnce);
    }

    public TablexiaComponentDialog createYesNoDialog(String text, InputListener yesInputListener, InputListener noInputListener) {
        return createYesNoDialog(null, text, yesInputListener, noInputListener, true);
    }

    public TablexiaComponentDialog createYesNoDialog(List<TablexiaDialogComponentAdapter> additionalComponents, String text, InputListener yesInputListener, InputListener noInputListener, boolean useOnce){
        return createYesNoDialog(additionalComponents, text, ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_YES),
                ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_NO), yesInputListener, noInputListener, useOnce);
    }

    public TablexiaComponentDialog createYesNoDialog(List<TablexiaDialogComponentAdapter> additionalComponents, String text, String yesButtonText, String noButtonText, InputListener yesInputListener, InputListener noInputListener, boolean useOnce) {
        ArrayList<TablexiaDialogComponentAdapter> adapters = new ArrayList<TablexiaDialogComponentAdapter>();

        if (additionalComponents != null) adapters.addAll(additionalComponents);

        adapters.add(new ResizableSpaceContentDialogComponent());
        adapters.add(new TextContentDialogComponent(text));

        adapters.add(new ResizableSpaceContentDialogComponent());
        adapters.add(new PositiveNegativeButtonContentDialogComponent(yesInputListener, useOnce, yesButtonText, noInputListener, useOnce, noButtonText));
        adapters.add(new FixedSpaceContentDialogComponent(WARNING_DIALOG_BOT_PAD));

        adapters.add(new AlertOnShowDialogComponent());
        adapters.add(new AdaptiveSizeDialogComponent());
        adapters.add(new CenterPositionDialogComponent());
        adapters.add(new DimmerDialogComponent());

        return createDialog(adapters.toArray(new TablexiaDialogComponentAdapter[]{}));
    }

    public TablexiaComponentDialog createSingleButtonNotifyDialog(String text, InputListener buttonInputListener) {
        return createSingleButtonNotifyDialog(text, buttonInputListener, null, false);
    }

    public TablexiaComponentDialog createSingleButtonNotifyDialog(String text, InputListener buttonInputListener, boolean closeOnTouch) {
        return createSingleButtonNotifyDialog(text, buttonInputListener, null, closeOnTouch);
    }

    public TablexiaComponentDialog createSingleButtonNotifyDialog(String text, InputListener buttonInputListener, ArrayList<TablexiaDialogComponentAdapter> additionalAdapters, boolean closeOnTouch) {
        ArrayList<TablexiaDialogComponentAdapter> adapters = new ArrayList<>();

        adapters.add(new ResizableSpaceContentDialogComponent());
        adapters.add(new TextContentDialogComponent(text, true, true));

        adapters.add(new ResizableSpaceContentDialogComponent());
        adapters.add(new SingleButtonContentDialogComponent(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_UNDERSTAND), 
                                                            StandardTablexiaButton.TablexiaButtonType.RED,
                                                            buttonInputListener));
        adapters.add(new FixedSpaceContentDialogComponent(WARNING_DIALOG_BOT_PAD));

        adapters.add(new AlertOnShowDialogComponent());
        adapters.add(new AdaptiveSizeDialogComponent());
        adapters.add(new CenterPositionDialogComponent());
        if (closeOnTouch) adapters.add(new TouchCloseDialogComponent());
        adapters.add(new DimmerDialogComponent());

        if(additionalAdapters != null) adapters.addAll(additionalAdapters);

        return createDialog(adapters.toArray(new TablexiaDialogComponentAdapter[]{}));
    }

    public TablexiaComponentDialog createStandardUntouchableDialog(TablexiaDialogComponentAdapter... componentDefinitions) {

        List<TablexiaDialogComponentAdapter> standardComponents = new ArrayList<TablexiaDialogComponentAdapter>() {
            {
                add(new CenterPositionDialogComponent());
                add(new DimmerDialogComponent());
                add(new ViewportMaximumSizeComponent());
                add(new AdaptiveSizeDialogComponent());
            }
        };

        List<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>(Arrays.asList(componentDefinitions));
        components.addAll(standardComponents);

        return createDialog(components.toArray(new TablexiaDialogComponentAdapter[] {}));
    }

    public TablexiaComponentDialog createStandardDialog(TablexiaDialogComponentAdapter... componentDefinitions) {

        List<TablexiaDialogComponentAdapter> standardComponents = new ArrayList<TablexiaDialogComponentAdapter>() {
            {
                add(new CenterPositionDialogComponent());
                add(new DimmerDialogComponent());
                add(new TouchCloseDialogComponent());
                add(new ViewportMaximumSizeComponent());
                add(new AdaptiveSizeDialogComponent());
            }
        };

        List<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>(Arrays.asList(componentDefinitions));
        components.addAll(standardComponents);

        return createDialog(components.toArray(new TablexiaDialogComponentAdapter[] {}));
    }

    public TablexiaComponentDialog createDialog(TablexiaDialogComponentAdapter... componentDefinitions) {
        return createDialog(DEFAULT_DIALOG_TYPE, componentDefinitions);
    }

    public TablexiaComponentDialog createDialog(TablexiaComponentDialog.TablexiaDialogType dialogType, TablexiaDialogComponentAdapter ... componentDefinitions) {
        return createDialog(stage, dialogType, componentDefinitions);
    }

    public TablexiaComponentDialog createDialog(Stage stage, TablexiaComponentDialog.TablexiaDialogType dialogType, TablexiaDialogComponentAdapter ... componentDefinitions) {
        return new TablexiaComponentDialog(stage, dialogType, componentDefinitions);
    }

    public TablexiaComponentDialog createPermissionDialog(final String message, final Runnable onConfirmRunnable) {
        TablexiaDialogComponentAdapter[] componentAdapters = new TablexiaDialogComponentAdapter[] {
                new CenterPositionDialogComponent(),
                new FixedSpaceContentDialogComponent(),
                new AdaptiveSizeDialogComponent(),
                new ViewportMaximumSizeComponent(),
                new ResizableSpaceContentDialogComponent(),
                new TextContentDialogComponent(message),
                new ResizableSpaceContentDialogComponent(),
                new PositiveNegativeButtonContentDialogComponent(
                    new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            if(onConfirmRunnable != null) onConfirmRunnable.run();
                        }
                    },
                    PositiveNegativeButtonContentDialogComponent.PositiveNegativeButtonType.SETTINGS_DECLINE),
                new FixedSpaceContentDialogComponent()
        };

        return new TablexiaComponentDialog(stage, TablexiaComponentDialog.TablexiaDialogType.DIALOG_SQUARE_BORDERS, componentAdapters) {
            @Override
            public void show() {
                show(WARNING_DIALOG_WIDTH, WARNING_DIALOG_HEIGHT);
            }
        };
    }
}
