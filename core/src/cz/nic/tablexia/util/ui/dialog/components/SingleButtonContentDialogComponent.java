/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.util.ui.button.ImageTablexiaButton;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

/**
 * Created by Matyáš Latner.
 */
public class SingleButtonContentDialogComponent extends TablexiaDialogComponentAdapter {

    private static final float                                      DIALOG_BUTTON_ADAPTIVE_SIZE_RATIO   = 1.3f;
    private static final StandardTablexiaButton.TablexiaButtonType  DEFAULT_BUTTON_TYPE                 = StandardTablexiaButton.TablexiaButtonType.GREEN;

    private final TablexiaButton button;

    public SingleButtonContentDialogComponent(String buttonText) {
        this(buttonText, DEFAULT_BUTTON_TYPE);
    }

    public SingleButtonContentDialogComponent(String buttonText, StandardTablexiaButton.TablexiaButtonType buttonType) {
        this(buttonText, buttonType, null);
    }

    public SingleButtonContentDialogComponent(String buttonText, StandardTablexiaButton.TablexiaButtonType buttonType, InputListener inputListener) {
        this(buttonText, false, buttonType, inputListener);
    }

    public SingleButtonContentDialogComponent(String buttonText, boolean wrapText, StandardTablexiaButton.TablexiaButtonType buttonType, InputListener inputListener) {
        this(new StandardTablexiaButton(buttonText, buttonType, wrapText), inputListener);
    }

    public SingleButtonContentDialogComponent(String buttonText, Image buttonIcon, StandardTablexiaButton.TablexiaButtonType buttonType, InputListener inputListener) {
        this(new ImageTablexiaButton(buttonText, buttonIcon, buttonType, false), inputListener);
    }

    public SingleButtonContentDialogComponent(String buttonText, Image buttonIcon, boolean wrapText, StandardTablexiaButton.TablexiaButtonType buttonType, InputListener inputListener) {
        this(new ImageTablexiaButton(buttonText, buttonIcon, buttonType, wrapText), inputListener);
    }
    
    public SingleButtonContentDialogComponent(StandardTablexiaButton button, InputListener inputListener, boolean enabled){
        this(button, inputListener);
        this.button.setEnabled(enabled);
    }

    public SingleButtonContentDialogComponent(StandardTablexiaButton button, InputListener inputListener) {
        this.button = button;
        this.button.adaptiveSizeRatio(DIALOG_BUTTON_ADAPTIVE_SIZE_RATIO);
        this.button.adaptiveSize(false);

        if(inputListener != null) {
            button.setInputListener(inputListener);
        }
    }

    public TablexiaButton getButton() {
        return button;
    }

    @Override
    public void adaptiveSizeThresholdChanged() {
        super.adaptiveSizeThresholdChanged();
        button.adaptiveSizeThresholdChanged();
    }

    @Override
    public void prepareContent(Cell content) {
        content.row();
        content.setActor(button);
    }

    public boolean isEnabled() {
        return button.isEnabled();
    }

    public SingleButtonContentDialogComponent useOnce(boolean useOnce) {
        button.useOnce(useOnce);
        return this;
    }

    public SingleButtonContentDialogComponent setEnabled(boolean enabled) {
        button.setEnabled(enabled);
        return this;
    }

    public SingleButtonContentDialogComponent setInputListener(InputListener inputListener) {
        button.setInputListener(inputListener);
        return this;
    }
}
