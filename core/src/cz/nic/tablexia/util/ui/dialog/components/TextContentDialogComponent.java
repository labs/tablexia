/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by Matyáš Latner.
 */
public class TextContentDialogComponent extends TablexiaDialogComponentAdapter {

    private static final Color                               DEFAULT_FONT_COLOR              = Color.BLACK;
    private static final ApplicationFontManager.FontType     DEFAULT_FONT_TYPE               = ApplicationFontManager.FontType.REGULAR_18;
    private static final int                                 DEFAULT_ALIGN                   = Align.center;
    private static final Float                               DEFAULT_PADDING                 = 0f;
    public static final int                                  FONT_SIZE_RESIZE_THRESHOLD      = 10;
    public static final float                                PADDING_SCALE_THRESHOLD         = 1.7f;

    private final   String                                  text;
    private ApplicationFontManager.FontType                 fontType;
    private ApplicationFontManager.FontType                 actualFontType;
    private final   Color                                   fontColor;
    private         float                                   origLeftPadding, origRightPadding;
    private         float                                   currLeftPadding, currRightPadding;
    private final   int                                     align;
    private         boolean                                 wrap;
    private final   boolean                                 fullDialogWidth;

    private boolean                                         isScaled;
    private TablexiaLabel                                   textLabel;

    private Cell content;

    public TextContentDialogComponent(String text) {
        this(text, DEFAULT_ALIGN);
    }

    public TextContentDialogComponent(String text, boolean wrap) {
        this(text, DEFAULT_FONT_TYPE, DEFAULT_FONT_COLOR, DEFAULT_ALIGN, DEFAULT_PADDING, wrap, false);
    }

    public TextContentDialogComponent(String text, ApplicationFontManager.FontType fontType, boolean wrap) {
        this(text, fontType, DEFAULT_FONT_COLOR, DEFAULT_ALIGN, DEFAULT_PADDING, wrap, false);
    }

    public TextContentDialogComponent(String text, ApplicationFontManager.FontType fontType, Color color, boolean wrap) {
        this(text, fontType, color, DEFAULT_ALIGN, DEFAULT_PADDING, wrap, false);
    }

    public TextContentDialogComponent(String text, boolean wrap, boolean fullDialogWidth) {
        this(text, DEFAULT_FONT_TYPE, DEFAULT_FONT_COLOR, DEFAULT_ALIGN, DEFAULT_PADDING, wrap, fullDialogWidth);
    }

    public TextContentDialogComponent(String text, Integer align) {
        this(text, align, DEFAULT_PADDING);
    }

    public TextContentDialogComponent(String text, Integer align, Float padding) {
        this(text, DEFAULT_FONT_TYPE, DEFAULT_FONT_COLOR, align, padding, true, false);
    }

    public TextContentDialogComponent(String text, ApplicationFontManager.FontType fontType) {
        this(text, fontType, DEFAULT_FONT_COLOR);
    }

    public TextContentDialogComponent(String text, ApplicationFontManager.FontType fontType, boolean wrap, boolean fullDialogWidth) {
        this(text, fontType, DEFAULT_FONT_COLOR, DEFAULT_ALIGN, DEFAULT_PADDING, wrap, fullDialogWidth);
    }

    public TextContentDialogComponent(String text, ApplicationFontManager.FontType fontType, Color fontColor) {
        this(text, fontType, fontColor, DEFAULT_ALIGN, DEFAULT_PADDING, true, false);
    }

    public TextContentDialogComponent(String text, Color color, int align, float padding) {
        this(text, DEFAULT_FONT_TYPE, color, align, padding);
    }

    public TextContentDialogComponent(String text, ApplicationFontManager.FontType fontType, Color fontColor, Integer align, Float padding) {
        this(text, fontType, fontColor, align, padding, true, false);
    }

    public TextContentDialogComponent(String text, ApplicationFontManager.FontType fontType, Color fontColor, Integer align, Float padding, boolean wrap, boolean fullDialogWidth) {
        this.text           = text;
        this.fontType       = fontType;
        this.actualFontType = fontType;
        this.fontColor      = fontColor;
        this.align          = align.intValue();
        this.origLeftPadding = padding;
        this.origRightPadding = padding;
        this.isScaled       = false;
        this.wrap           = wrap;
        this.fullDialogWidth= fullDialogWidth;
    }

    protected Color getFontColor() {
        return fontColor;
    }

    public ApplicationFontManager.FontType getFontType() {
        return fontType;
    }

    protected ApplicationFontManager.FontType getActualFontType() {
        return actualFontType;
    }

    public void setPadding(float leftPadding, float rightPadding) {
        this.origLeftPadding = leftPadding;
        this.origRightPadding = rightPadding;
    }

    @Override
    public void adaptiveSizeThresholdChanged() {
        if (ComponentScaleUtil.isUnderThreshold()) {
            if (!isScaled) {
                actualFontType = ApplicationFontManager.FontType.getProximateFontTypeForSize(fontType.getSize() + FONT_SIZE_RESIZE_THRESHOLD, fontType.isBold());
                isScaled = true;
            }
        } else {
            if (isScaled) {
                actualFontType = fontType;
                isScaled = false;
                currLeftPadding = origLeftPadding;
                currRightPadding = origRightPadding;
            }
        }
        textLabel.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(actualFontType, fontColor));

        updatePadding();
    }

    private void updatePadding() {
        if (ComponentScaleUtil.isUnderThreshold()) {
            currLeftPadding = origLeftPadding * PADDING_SCALE_THRESHOLD;
            currRightPadding = origRightPadding * PADDING_SCALE_THRESHOLD;
        }
        else {
            currLeftPadding = origLeftPadding;
            currRightPadding = origRightPadding;
        }

        content.padLeft(currLeftPadding).padRight(currRightPadding);

        if(fullDialogWidth)
            content.width(getDialog().getInnerWidth() - currLeftPadding - currRightPadding);
    }

    @Override
    public void show() {
        super.show();
        updatePadding();
    }

    @Override
    public void prepareContent(Cell content) {
        prepareTextLabel(content);
    }

    protected void prepareTextLabel(Cell content) {
        this.content = content;
        textLabel = new TablexiaLabel(text, new TablexiaLabel.TablexiaLabelStyle(actualFontType, getFontColor()), true);
        textLabel.setWrap(wrap);
        textLabel.setAlignment(align);
        content.setActor(textLabel).fillX().padLeft(currLeftPadding).padRight(currRightPadding);
    }

    public static ApplicationFontManager.FontType getDefaultFontType() {
        return DEFAULT_FONT_TYPE;
    }

    //Gets for testing

    public String getText() {
        return text;
    }
}
