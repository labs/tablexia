/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;

/**
 * Created by Matyáš Latner.
 */
public class TwoColumnContentDialogComponent extends TablexiaDialogComponentAdapter {

    private static final Float              DEFAULT_SPACE_RATIO = 0f;

    private TablexiaDialogComponentAdapter dialogComponentAdapter1;
    private TablexiaDialogComponentAdapter dialogComponentAdapter2;

    private Cell                            cell1;
    private Cell                            space;
    private Cell                            cell2;

    private Float                           widthRatio1;
    private Float                           widthRatio2;
    private Float                           heightRatio;
    private Float                           spaceRatio;

    public TwoColumnContentDialogComponent(TablexiaDialogComponentAdapter dialogComponentAdapter1, TablexiaDialogComponentAdapter dialogComponentAdapter2, Float widthRatio1, Float widthRatio2, Float heightRatio) {
        this(dialogComponentAdapter1, dialogComponentAdapter2, widthRatio1, widthRatio2, heightRatio, DEFAULT_SPACE_RATIO);
    }

    public TwoColumnContentDialogComponent(Float spaceRatio) {
        this(null, null, null, spaceRatio);
    }

    public TwoColumnContentDialogComponent(Float widthRatio1, Float widthRatio2, Float heightRatio, Float spaceRatio) {
        this(null, null, widthRatio1, widthRatio2, heightRatio, spaceRatio);
    }

    public TwoColumnContentDialogComponent(TablexiaDialogComponentAdapter dialogComponentAdapter1, TablexiaDialogComponentAdapter dialogComponentAdapter2, Float widthRatio1, Float widthRatio2, Float heightRatio, Float spaceRatio) {
        this.dialogComponentAdapter1 = dialogComponentAdapter1;
        this.dialogComponentAdapter2 = dialogComponentAdapter2;
        this.widthRatio1 = widthRatio1;
        this.widthRatio2 = widthRatio2;
        this.heightRatio = heightRatio;
        this.spaceRatio = spaceRatio;
    }

    public void setDialogComponentAdapter1(TablexiaDialogComponentAdapter dialogComponentAdapter1) {
        this.dialogComponentAdapter1 = dialogComponentAdapter1;
        resize();
    }

    public void setDialogComponentAdapter2(TablexiaDialogComponentAdapter dialogComponentAdapter2) {
        this.dialogComponentAdapter2 = dialogComponentAdapter2;
        resize();
    }

    @Override
    public void setDialog(TablexiaComponentDialog dialog) {
        super.setDialog(dialog);
        if (dialogComponentAdapter1 != null) {
            this.dialogComponentAdapter1.setDialog(dialog);
        }
        if (dialogComponentAdapter2 != null) {
            this.dialogComponentAdapter2.setDialog(dialog);
        }
    }

    @Override
    public void prepareContent(Cell content) {
        Table table = new Table();
        content.setActor(table);
        cell1 = table.add();
        space = table.add();
        cell2 = table.add();
        resize();
        if (dialogComponentAdapter1 != null) {
            dialogComponentAdapter1.prepareContent(cell1);
        }
        if (dialogComponentAdapter2 != null) {
            dialogComponentAdapter2.prepareContent(cell2);
        }
    }

    private void resize() {
        if (cell1 != null) {
            if (widthRatio1 != null) {
                cell1.width(getDialog().getWidth() * widthRatio1);
            }
            if (heightRatio != null) {
                cell1.height(getDialog().getHeight() * heightRatio);
            }
        }
        if (cell2 != null) {
            if (widthRatio2 != null) {
                cell2.width(getDialog().getWidth() * widthRatio2);
            }
            if (heightRatio != null) {
                cell2.height(getDialog().getHeight() * heightRatio);
            }
        }
        if (space != null) {
            if (spaceRatio != null) {
                space.width(getDialog().getWidth() * spaceRatio);
            }
            if (heightRatio != null) {
                space.height(getDialog().getHeight() * heightRatio);
            }
        }
    }

    @Override
    public void sizeChanged() {
        resize();
        if (dialogComponentAdapter1 != null) {
            dialogComponentAdapter1.sizeChanged();
        }
        if (dialogComponentAdapter2 != null) {
            dialogComponentAdapter2.sizeChanged();
        }
    }

    @Override
    public void prepareBackground(Group backgroundLayer) {
        if (dialogComponentAdapter1 != null) {
            dialogComponentAdapter1.prepareBackground(backgroundLayer);
        }
        if (dialogComponentAdapter2 != null) {
            dialogComponentAdapter2.prepareBackground(backgroundLayer);
        }
    }

    @Override
    public void show() {
        if (dialogComponentAdapter1 != null) {
            dialogComponentAdapter1.show();
        }
        if (dialogComponentAdapter2 != null) {
            dialogComponentAdapter2.show();
        }
    }

    @Override
    public void hide() {
        if (dialogComponentAdapter1 != null) {
            dialogComponentAdapter1.hide();
        }
        if (dialogComponentAdapter2 != null) {
            dialogComponentAdapter2.hide();
        }
    }

    @Override
    public void beforeDraw() {
        if (dialogComponentAdapter1 != null) {
            dialogComponentAdapter1.beforeDraw();
        }
        if (dialogComponentAdapter2 != null) {
            dialogComponentAdapter2.beforeDraw();
        }
    }

    @Override
    public void afterDraw() {
        if (dialogComponentAdapter1 != null) {
            dialogComponentAdapter1.afterDraw();
        }
        if (dialogComponentAdapter2 != null) {
            dialogComponentAdapter2.afterDraw();
        }
    }

    @Override
    public void adaptiveSizeThresholdChanged() {
        if (dialogComponentAdapter1 != null) {
            dialogComponentAdapter1.adaptiveSizeThresholdChanged();
        }
        if (dialogComponentAdapter2 != null) {
            dialogComponentAdapter2.adaptiveSizeThresholdChanged();
        }
    }

    @Override
    public void dialogTouched(float x, float y) {
        if (dialogComponentAdapter1 != null) {
            dialogComponentAdapter1.dialogTouched(x, y);
        }
        if (dialogComponentAdapter2 != null) {
            dialogComponentAdapter2.dialogTouched(x, y);
        }
    }
}
