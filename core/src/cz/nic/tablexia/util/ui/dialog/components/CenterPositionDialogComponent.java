/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;

/**
 * Created by Matyáš Latner.
 */
public class CenterPositionDialogComponent extends TablexiaDialogComponentAdapter {

    private float screenWidth;
    private float screenHeight;

	private float dialogWidth;
	private float dialogHeight;

    public CenterPositionDialogComponent() {
    }

	public void setDialog(TablexiaComponentDialog dialog) {
		super.setDialog(dialog);
		screenWidth     = getViewportWidth();
		screenHeight    = getViewportHeight();
		if (getDialog() != null) {
			dialogWidth = getDialog().getWidth();
			dialogHeight = getDialog().getHeight();
		}
	}

    @Override
    public void show() {
		// dialog size has changed due to another component -> store new size for beforeDraw method
		if (getDialog() != null && (dialogWidth != getDialog().getWidth() || dialogHeight != getDialog().getHeight())) {
			dialogWidth = getDialog().getWidth();
			dialogHeight = getDialog().getHeight();
		}
        centerDialog();
    }

    @Override
    public void beforeDraw() {
		// Call center only if dialog size changes
		if (getDialog() != null && (dialogWidth != getDialog().getWidth() || dialogHeight != getDialog().getHeight())) {
			dialogWidth = getDialog().getWidth();
			dialogHeight = getDialog().getHeight();
			centerDialog();
		}
		// Call center only if screen size changes
        if (screenWidth != getViewportWidth() || screenHeight != getViewportHeight()) {
            screenWidth = getViewportWidth();
            screenHeight = getViewportHeight();
            centerDialog();
        }
    }

    private void centerDialog() {
		getDialog().setPosition(getViewportLeftX() + (screenWidth / 2) - (getDialog().getWidth() / 2), getViewportBottomY() + (screenHeight / 2) - (getDialog().getHeight() / 2));
    }
}
