/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.loader.application.ApplicationAtlasManager;

/**
 * Created by Matyáš Latner.
 */
public class DimmerDialogComponent extends TablexiaDialogComponentAdapter {

    private static final Color DEFAULT_DIMMER_COLOR = Color.BLACK;
    private static final float DEFAULT_DIMMER_ALPHA = 0.7f;

    private Float alpha;
    private Color color;

    private Image   background;

    public DimmerDialogComponent() {
        this(DEFAULT_DIMMER_ALPHA);
    }

    public DimmerDialogComponent(Float alpha) {
        this(alpha, DEFAULT_DIMMER_COLOR);
    }

    public DimmerDialogComponent(Float alpha, Color color) {
        this.alpha = alpha;
        this.color = color;
    }

    @Override
    public void prepareBackground(Group backgroundLayer) {
        background = new Image(ApplicationAtlasManager.getInstance().getColorTextureRegion(new Color(color.r, color.g, color.b, alpha)));
        backgroundLayer.addActor(background);
    }

    @Override
    public void sizeChanged() {
        background.setBounds(getViewportLeftX(),
                     getViewportBottomY(),
                     getViewportWidth(),
                     getViewportHeight());
    }
}
