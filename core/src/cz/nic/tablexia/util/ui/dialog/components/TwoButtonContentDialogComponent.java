/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;

/**
 * Created by Drahomir Karchnak on 27/01/16.
 */
public class TwoButtonContentDialogComponent extends TwoColumnContentDialogComponent{

    private static final float  SPACE_RATIO = 1f / 10;

    private SingleButtonContentDialogComponent firstButton;
    private SingleButtonContentDialogComponent secondButton;

    private class DefaultHideListener extends ClickListener {

        private InputListener externalListener;

        public DefaultHideListener(InputListener externalListener) {
            this.externalListener = externalListener;
        }

        @Override
        public boolean handle(Event e) {
            boolean result =  super.handle(e);
            if (externalListener != null) {
                externalListener.handle(e);
            }
            return result;
        }

        @Override
        public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            getDialog().hide(hasHideAnimation());
        }
    }

    protected boolean hasHideAnimation() {
        return TablexiaComponentDialog.DEFAULT_HIDE_ANIMATION;
    }
    
    public TwoButtonContentDialogComponent(String firstButtonText,
                                           String secondButtonText,
                                           Image firstIcon,
                                           Image secondIcon,
                                           StandardTablexiaButton.TablexiaButtonType firstButtonType,
                                           boolean firstUseOnce,
                                           StandardTablexiaButton.TablexiaButtonType secondButtonType,
                                           boolean secondUseOnce,
                                           InputListener firstButtonInput,
                                           InputListener secondButtonInput) {
        this(firstButtonText,secondButtonText,firstIcon,secondIcon,firstButtonType,secondButtonType,firstButtonInput,secondButtonInput);
        firstButton.useOnce(firstUseOnce);
        secondButton.useOnce(secondUseOnce);
    }

    public TwoButtonContentDialogComponent(String firstButtonText,
                                           String secondButtonText,
                                           StandardTablexiaButton.TablexiaButtonType firstButtonType,
                                           StandardTablexiaButton.TablexiaButtonType secondButtonType,
                                           InputListener firstButtonInput,
                                           InputListener secondButtonInput) {

        super(SPACE_RATIO);
        firstButton  = new SingleButtonContentDialogComponent(firstButtonText, false, firstButtonType, firstButtonInput);
        secondButton = new SingleButtonContentDialogComponent(secondButtonText, false, secondButtonType, secondButtonInput);

        setDialogComponentAdapter1(firstButton);
        setDialogComponentAdapter2(secondButton);

        firstButton.setInputListener(new DefaultHideListener(firstButtonInput));
        secondButton.setInputListener(new DefaultHideListener(secondButtonInput));
    }

    public TwoButtonContentDialogComponent(String firstButtonText,
                                           String secondButtonText,
                                           Image firstIcon,
                                           Image secondIcon,
                                           StandardTablexiaButton.TablexiaButtonType firstButtonType,
                                           StandardTablexiaButton.TablexiaButtonType secondButtonType,
                                           InputListener firstButtonInput,
                                           InputListener secondButtonInput) {

        super(SPACE_RATIO);

        //Sets first buttons icon if needed
        if(firstIcon != null)
            firstButton  = new SingleButtonContentDialogComponent(firstButtonText, firstIcon, firstButtonType, firstButtonInput);
        else
            firstButton  = new SingleButtonContentDialogComponent(firstButtonText, firstButtonType, firstButtonInput);

        //Sets second buttons icon if needed
        if(secondIcon != null)
            secondButton = new SingleButtonContentDialogComponent(secondButtonText, secondIcon, secondButtonType, secondButtonInput);
        else
            secondButton = new SingleButtonContentDialogComponent(secondButtonText, secondButtonType, secondButtonInput);

        setDialogComponentAdapter1(firstButton);
        setDialogComponentAdapter2(secondButton);

        firstButton.setInputListener(new DefaultHideListener(firstButtonInput));
        secondButton.setInputListener(new DefaultHideListener(secondButtonInput));
    }

	public SingleButtonContentDialogComponent getFirstButton() {
		return firstButton;
	}

	public SingleButtonContentDialogComponent getSecondButton() {
		return secondButton;
	}

	public void setButtonsDisabled(){
        firstButton.setEnabled(false);
        secondButton.setEnabled(false);
    }

    public void setButtonsEnabled(){
        firstButton.setEnabled(true);
        secondButton.setEnabled(true);
    }
}
