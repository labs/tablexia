/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;

/**
 * Created by Matyáš Latner.
 */
public abstract class TablexiaDialogComponentAdapter {

    private TablexiaComponentDialog dialog;

    public void setDialog(TablexiaComponentDialog dialog) {
        this.dialog = dialog;
    }

    public final TablexiaComponentDialog getDialog() {
        return dialog;
    }

    protected Stage getStage() {
        return dialog == null ? null : dialog.getStage();
    }

    protected float getViewportWidth() {
        return getStage() != null ? TablexiaSettings.getViewportWidth(getStage()) : 0;
    }

    protected float getViewportHeight() {
        return getStage() != null ? TablexiaSettings.getViewportHeight(getStage()) : 0;
    }

    protected float getViewportLeftX() {
        return getStage() != null ? TablexiaSettings.getViewportLeftX(getStage()) : 0;
    }

    protected float getViewportRightX() {
        return getStage() != null ? TablexiaSettings.getViewportRightX(getStage()) : 0;
    }

    protected float getViewportBottomY() {
        return getStage() != null ? TablexiaSettings.getViewportBottomY(getStage()) : 0;
    }

    protected float getViewportTopY() {
        return getStage() != null ? TablexiaSettings.getViewportTopY(getStage()) : 0;
    }

    public void prepareBackground(Group backgroundLayer) {}
    public void prepareContent(Cell content) {}
    public void show() {}
    public void hide() {}
    public void beforeDraw() {}
    public void afterDraw() {}
    public void drawAdditions(Batch batch, float parentAlpha) {}
    public void adaptiveSizeThresholdChanged() {}
    public void dialogTouched(float x, float y) {}
    public void sizeChanged() {}

}
