/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import cz.nic.tablexia.loader.application.ApplicationInternalSoundManager;

/**
 * Created by bcx on 2/4/16.
 */
public class AlertOnShowDialogComponent extends TablexiaDialogComponentAdapter{
    @Override
    public void show() {
        ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.ALERT).play();
        super.show();
    }
}
