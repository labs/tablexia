/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import net.engio.mbassy.listener.Handler;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;

/**
 * Created by Matyáš Latner.
 */
public class AdaptiveSizeDialogComponent extends TablexiaDialogComponentAdapter {

    public static final float   DIALOG_SCALE_RATIO = 1.4f;

    private boolean             isScaled;

    public AdaptiveSizeDialogComponent() {
        this.isScaled   = false;
    }

    @Override
    public void show() {
        getDialog().processSizeThresholdChanged();
        ApplicationBus.getInstance().subscribe(this);
    }

    @Override
    public void hide() {
        ApplicationBus.getInstance().unsubscribe(this);
    }

    @Handler
    public void handleScreenSizeThresholdChanged(TablexiaApplication.ScreenSizeThresholdChanged screenSizeThresholdChanged) {
        getDialog().processSizeThresholdChanged();
    }

    @Override
    public void adaptiveSizeThresholdChanged() {
        float oldWidth = getDialog().getWidth();
        float oldHeight = getDialog().getHeight();
        float width = getDialog().getWidth();
        float height = getDialog().getHeight();

        if (ComponentScaleUtil.isUnderThreshold()) {
            if (!isScaled) {
                width = getDialog().getOriginalWidth() * DIALOG_SCALE_RATIO;
                height = getDialog().getOriginalHeight() * DIALOG_SCALE_RATIO;
                isScaled = true;
            }
        } else {
            if (isScaled) {
                width = getDialog().getOriginalWidth();
                height = getDialog().getOriginalHeight();
                isScaled = false;
            }
        }

        float positionX = getDialog().getX() + (oldWidth - width) / 2;
        float positionY = getDialog().getY() + (oldHeight - height) / 2;

        getDialog().setTemporaryBounds(positionX, positionY, width, height);
    }
}
