/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;
import cz.nic.tablexia.util.ui.QRCodeScanner;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TextFieldWithPlaceholder;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;

public class UserUuidSyncDialogComponent extends TablexiaDialogComponentAdapter {
    public  static final String                              UUID_CODE_REGEX                      = "((?:(?:[0-9]|[a-f]){8})-(?:(?:[0-9]|[a-f]){4})-(?:(?:[0-9]|[a-f]){4})-(?:(?:[0-9]|[a-f]){4})-(?:(?:[0-9]|[a-f]){12}))";

    private static final ApplicationFontManager.FontType     TEXT_FIELD_FONT                      = ApplicationFontManager.FontType.REGULAR_18;
    private static final ApplicationFontManager.FontType     TEXT_FIELD_FONT_THRESHOLD            = ApplicationFontManager.FontType.REGULAR_26;

    public static final int                                  DIALOG_RESET_POSITION_TIMEOUT        = 7000;

    private static final float                               COMPONENT_WIDTH                      = 0.9f;
    private static final float                               COMPONENT_HEIGHT                     = 50f;

    private static final float                               TEXT_FIELD_WIDTH                     = 0.96f;
    private static final float                               TEXT_FIELD_WIDTH_QR                  = 0.85f;
    private static final float                               TEXT_FIELD_BORDER_OFFSET             = 4;
    private static final float                               TEXT_FIELD_HEIGHT                    = 0.7f * COMPONENT_HEIGHT;
    private static final Color                               TEXT_FIELD_FONT_COLOR                = Color.BLACK;
    private static final Color                               TEXT_FIELD_PLACEHOLDER_FONT_COLOR    = Color.GRAY;

    private static final String                              TEXT_FIELD_PLACEHOLDER_TEXT          = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
    private static final int                                 TEXT_FIELD_MAXIMUM_LENGTH            = 36;

    private enum DialogPosition {
        TOP(1f, true),
        CENTER(0.5f, false);

        private float   shift;
        private boolean showKeyboard;

        DialogPosition(float shift, boolean showKeyboard) {
            this.shift = shift;
            this.showKeyboard = showKeyboard;
        }

        public float getShift() {
            return shift;
        }

        public boolean isShowKeyboard() {
            return showKeyboard;
        }
    }


    private final ClickListenerWithSound qrCodeListenerFinal = new ClickListenerWithSound() {
        @Override
        public void onClick(InputEvent event, float x, float y) {
            if (Tablexia.getCameraOpener().cameraAccessible()) {
                startQRCodeScanner();
            }else {
                showCameraAccessDialog();
            }
        }
    };

    private final boolean            hasQRCodeScanner;
    private final Runnable           wrongQRScannedCallback;

    private InputListener            repositionOnTypeListener, repositionOnTouchListener;
    private Timer                    timer;
    private boolean                  timerSet;

    private Group                    group;
    private Image                    textFieldCursor;
    private TextFieldWithPlaceholder textField;
    private Image                    textFieldBorder;
    private Image                    qrScannerButton;

    //Add QRCodeScanner as a parameter...
    public UserUuidSyncDialogComponent(String syncUUID, Runnable wrongQRScannedCallback) {
        this.wrongQRScannedCallback = wrongQRScannedCallback;

        hasQRCodeScanner = Tablexia.hasQRCodeScanner();

        group = new Group();
        textFieldBorder = new Image(ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.DIALOG_THIN_BORDER));
        textFieldCursor = new Image(ApplicationAtlasManager.getInstance().getColorTexture(TEXT_FIELD_FONT_COLOR));
        qrScannerButton = new Image(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.QR_MAGNIFIER_ICON));

        textField = initializeTextField(syncUUID);
    }

    private TextFieldWithPlaceholder initializeTextField(String initialText) {
        TextFieldWithPlaceholder textFieldWithPlaceholder = new TextFieldWithPlaceholder(
                initialText,
                new TextFieldWithPlaceholder.TablexiaTextFieldStyle(
                        ComponentScaleUtil.isUnderThreshold() ? TEXT_FIELD_FONT_THRESHOLD : TEXT_FIELD_FONT,
                        TEXT_FIELD_FONT_COLOR,
                        textFieldCursor.getDrawable(),
                        null,
                        null
                ),
                TEXT_FIELD_PLACEHOLDER_TEXT
        );

        textFieldWithPlaceholder.setPlaceHolderLabelStyle(new TablexiaLabel.TablexiaLabelStyle(
                ComponentScaleUtil.isUnderThreshold() ? TEXT_FIELD_FONT_THRESHOLD : TEXT_FIELD_FONT,
                TEXT_FIELD_PLACEHOLDER_FONT_COLOR
        ));
        textFieldWithPlaceholder.setMaxLength(TEXT_FIELD_MAXIMUM_LENGTH);

        return textFieldWithPlaceholder;
    }

    public String getText() {
        return textField.getText();
    }

    public boolean hasQRCodeScanner() {
        return hasQRCodeScanner;
    }

    @Override
    public void prepareContent(Cell content) {
        content.setActor(group);
    }

    @Override
    public void adaptiveSizeThresholdChanged() {
        group.clear();

        group.setSize(getDialog().getInnerWidth() * COMPONENT_WIDTH, COMPONENT_HEIGHT);

        if(hasQRCodeScanner) {
            textField.setSize(group.getWidth() * TEXT_FIELD_WIDTH_QR, TEXT_FIELD_HEIGHT);
            textField.setPosition(TEXT_FIELD_BORDER_OFFSET, (group.getHeight() - textField.getHeight()) / 2f);

            qrScannerButton.setSize(COMPONENT_HEIGHT, COMPONENT_HEIGHT);
            qrScannerButton.setPosition(group.getWidth() - qrScannerButton.getWidth(), (group.getHeight() - qrScannerButton.getHeight()) / 2f);
            qrScannerButton.addListener(qrCodeListenerFinal);
            group.addActor(qrScannerButton);
        } else {
            textField.setSize(group.getWidth() * TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT);
            textField.setPosition((group.getWidth() - textField.getWidth()) / 2f, (group.getHeight() - textField.getHeight()) / 2f);
        }
        group.addActor(textField);

        textFieldBorder.setSize(textField.getWidth() + TEXT_FIELD_BORDER_OFFSET * 2, textField.getHeight() + TEXT_FIELD_BORDER_OFFSET * 2);
        textFieldBorder.setPosition(textField.getX() - TEXT_FIELD_BORDER_OFFSET, textField.getY() - TEXT_FIELD_BORDER_OFFSET);
        textFieldBorder.setTouchable(Touchable.disabled);
        group.addActor(textFieldBorder);
    }

    public void setRepositionOnFocusOnMobileDevices(boolean repositionOnFocus) {
        if(repositionOnFocus && TablexiaSettings.getInstance().isRunningOnMobileDevice()) {
            if (repositionOnTypeListener == null) prepareOnTypeListener();
            if (repositionOnTouchListener == null) prepareOnTouchListener();
            textField.addListener(repositionOnTypeListener);
            textField.addListener(repositionOnTouchListener);
        }
        else {
            if (repositionOnTypeListener != null) textField.removeListener(repositionOnTypeListener);
            if (repositionOnTouchListener != null) textField.removeListener(repositionOnTouchListener);
        }
    }

    private void resetTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                repositionDialog(DialogPosition.CENTER);
                timerSet = false;
            }
        }, DIALOG_RESET_POSITION_TIMEOUT);
        timerSet = true;
    }

    private void prepareOnTypeListener() {
        this.repositionOnTypeListener = new InputListener() {
            private static final int COMMON_INT_CODE_FOR_ENTER = 13;
            private static final int ANDROID_INT_CODE_FOR_ENTER = 10;

            @Override
            public boolean keyTyped(InputEvent event, char character) {
                if (timerSet) timer.cancel();
                // have to check also (int)character because of some bug in iOS keyboard. It returns code Input.Keys.ENTER for every key after "done" button pressed
                if ((event.getKeyCode() != Input.Keys.ENTER || ((int) character != ANDROID_INT_CODE_FOR_ENTER || (int) character != COMMON_INT_CODE_FOR_ENTER))) {
                    resetTimer();
                } else {
                    repositionDialog(DialogPosition.CENTER);
                }
                return false;
            }
        };
    }

    public void startQRCodeScanner() {
        if(!Tablexia.hasQRCodeScanner() || Tablexia.getQRCodeScanner().isCameraPreviewActive()) return;

        Tablexia.getQRCodeScanner().setQRCodeListener(new QRCodeScanner.QRCodeListener() {
            @Override
            public void onCodeScanned(String data) {
                Tablexia.getQRCodeScanner().disableScanning();
                Pattern pattern = Pattern.compile(UUID_CODE_REGEX);
                Matcher matcher = pattern.matcher(data);

                if(matcher.find()) {
                    String uuid = matcher.group(1);
                    textField.setText(uuid);
                }
                else {
                    if(wrongQRScannedCallback != null) wrongQRScannedCallback.run();
                }

                getDialog().setTouchable(Touchable.enabled);
                Tablexia.getQRCodeScanner().stopCameraPreview();
                Tablexia.getQRCodeScanner().removeQRCodeListener();
            }

            @Override
            public void scannerCanceled() {
                getDialog().setTouchable(Touchable.enabled);
                Tablexia.getQRCodeScanner().stopCameraPreview();
                Tablexia.getQRCodeScanner().removeQRCodeListener();
            }
        });

        getDialog().setTouchable(Touchable.disabled);
        Tablexia.getQRCodeScanner().enableScanning();
        Tablexia.getQRCodeScanner().startCameraPreview();
    }

    public void showCameraAccessDialog() {
        TablexiaComponentDialogFactory.getInstance().createSingleButtonNotifyDialog(
                ApplicationTextManager.getInstance().getText
                        (ApplicationTextManager.ApplicationTextsAssets.CAMERA_ACCESS_NOTIFICATION),
                null,
                true)
                .show(Tablexia.ERROR_DIALOG_WIDTH, Tablexia.ERROR_DIALOG_HEIGHT);
    }

    public void prepareOnTouchListener(){
        this.repositionOnTouchListener = new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                if (timerSet) timer.cancel();
                repositionDialog(DialogPosition.TOP);
                resetTimer();
                return super.touchDown(event, x, y, pointer, button);
            }
        };
    }

    private void repositionDialog(final DialogPosition dialogPosition) {
        TablexiaComponentDialog dialog = getDialog();
        Stage stage = getStage();
        if(dialog == null || (stage == null)) return;

        dialog.setPosition(
                TablexiaSettings.getViewportLeftX(stage) + TablexiaSettings.getViewportWidth(stage) * 0.5f - dialog.getInnerWidth() * 0.5f,
                TablexiaSettings.getViewportBottomY(stage) + TablexiaSettings.getViewportHeight(stage) * dialogPosition.getShift() - dialog.getOutterHeight() * dialogPosition.getShift()
        );

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                Gdx.input.setOnscreenKeyboardVisible(dialogPosition.isShowKeyboard());
            }
        });
    }
}
