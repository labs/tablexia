/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;

import cz.nic.tablexia.loader.application.ApplicationAtlasManager;

/**
 * Created by Matyáš Latner.
 */
public class ImageContentDialogComponent extends TablexiaDialogComponentAdapter {

    private static final Scaling    DEFAULT_SCALING         = Scaling.fit;
    private static final Float      DEFAULT_WIDTH_RATIO     = null;
    private static final Float      DEFAULT_HEIGHT_RATIO    = null;

    private       Image image;
    private final Float widthRatio;
    private final Float heightRatio;
    private Cell<Image> imageCell;

    public ImageContentDialogComponent(String imageTexture) {
        this(imageTexture, DEFAULT_SCALING);
    }

    public ImageContentDialogComponent(TextureRegion imageTextureRegion) {
        this(imageTextureRegion, DEFAULT_SCALING);
    }

    public ImageContentDialogComponent(TextureRegion imageTextureRegion, Scaling scaling) {
        this(imageTextureRegion, scaling, DEFAULT_WIDTH_RATIO, DEFAULT_HEIGHT_RATIO);
    }

    public ImageContentDialogComponent(TextureRegion imageTexture, Scaling scaling, Float widthRatio) {
        this(imageTexture, scaling, widthRatio, DEFAULT_HEIGHT_RATIO);
    }

    public ImageContentDialogComponent(String imageTexture, Scaling scaling) {
        this(imageTexture, scaling, DEFAULT_WIDTH_RATIO);
    }

    public ImageContentDialogComponent(String imageTexture, Scaling scaling, Float widthRatio) {
        this(imageTexture, scaling, widthRatio, DEFAULT_HEIGHT_RATIO);
    }

    public ImageContentDialogComponent(String imageTexture, Scaling scaling, Float widthRatio, Float heightRatio) {
        this(ApplicationAtlasManager.getInstance().getTextureRegion(imageTexture), scaling, widthRatio, heightRatio);
    }

    public ImageContentDialogComponent(TextureRegion imageTextureRegion, Scaling scaling, Float widthRatio, Float heightRatio) {
        this(new Image(imageTextureRegion), scaling, widthRatio, heightRatio);
    }

    public ImageContentDialogComponent(Image imageTextureRegion) {
        this(imageTextureRegion, DEFAULT_SCALING);
    }

    public ImageContentDialogComponent(Image imageTexture, Scaling scaling) {
        this(imageTexture, scaling, DEFAULT_WIDTH_RATIO, DEFAULT_HEIGHT_RATIO);
    }

    public ImageContentDialogComponent(Image imageTexture, Scaling scaling, Float widthRatio) {
        this(imageTexture, scaling, widthRatio, DEFAULT_HEIGHT_RATIO);
    }

    public ImageContentDialogComponent(Image image, Scaling scaling, Float widthRatio, Float heightRatio) {
        this.widthRatio = widthRatio;
        this.heightRatio = heightRatio;
        this.image = image;
        if (image != null) {
            image.setScaling(scaling);
        }
    }

    public Cell<Image> getImageCell() {
        return imageCell;
    }

    public Image getImage() { return image; }

    @Override
    public void prepareContent(Cell content) {
        content.setActor(image);
        imageCell = content;
        resize();
    }

    @Override
    public void sizeChanged() {
        resize();
    }

    private void resize() {
        if (widthRatio != null) {
            imageCell.width(getDialog().getWidth() * widthRatio);
        }
        if (heightRatio != null) {
            imageCell.height(getDialog().getHeight() * heightRatio);
        }
    }
}
