/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.dialog.components;

import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;

/**
 * Created by Matyáš Latner.
 */
public class TouchCloseDialogComponent extends TablexiaDialogComponentAdapter {

    public interface TouchListener {
        /**
         * Touch listener
         * @param x posX of touch
         * @param y posY of touch
         * @return true if u want to hide the dialog, false if u dont.
         */
        boolean dialogTouched(float x, float y, TablexiaComponentDialog dialog);
    }

    private TouchListener touchListener;

    public TouchCloseDialogComponent() {
        this(null);
    }

    public TouchCloseDialogComponent(TouchListener listener) {
        this.touchListener = listener;
    }

    @Override
    public void dialogTouched(float x, float y) {
        boolean result = true;

        if(touchListener != null)
            result = touchListener.dialogTouched(x, y, getDialog());

        if(result)
            getDialog().hide();
    }
}
