/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui.scrollpane;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;

public class ScrollPaneWithBars extends ScrollPane {

    private enum OrientationType{
        HORIZONTAL, VERTICAL
    }

    private static final float DESKTOP_SCROLL_SIZE =    10f;
    private static final float DEFAULT_SCROLL_SIZE =    3f;
    private static final Color SCROLL_COLOR        =    new Color(0.f,0,0,0.1f);
    private static final Color KNOB_COLOR          =    new Color(0,0,0,0.35f);

    private float scrollSize;

    public ScrollPaneWithBars(Actor widget, boolean hBar, boolean vBar) {
        super(widget);
        this.scrollSize = TablexiaSettings.getInstance().isRunningOnMobileDevice() ? DEFAULT_SCROLL_SIZE : DESKTOP_SCROLL_SIZE;
        this.setStyle(initScrollPaneStyle(hBar, vBar));
        this.setFadeScrollBars(false);
    }

    private ScrollPane.ScrollPaneStyle initScrollPaneStyle(boolean hBar, boolean vBar){
        ScrollPane.ScrollPaneStyle style = new ScrollPane.ScrollPaneStyle();

        if(hBar){
            style.hScrollKnob = getScrollRegionDrawable(KNOB_COLOR, ScrollPaneWithBars.OrientationType.HORIZONTAL);
            style.hScroll = getScrollRegionDrawable(SCROLL_COLOR, ScrollPaneWithBars.OrientationType.HORIZONTAL);
        }

        if(vBar){
            style.vScrollKnob = getScrollRegionDrawable(KNOB_COLOR, ScrollPaneWithBars.OrientationType.VERTICAL);
            style.vScroll = getScrollRegionDrawable(SCROLL_COLOR, ScrollPaneWithBars.OrientationType.VERTICAL);
        }

        return style;
    }

    private TextureRegionDrawable getScrollRegionDrawable(Color color, ScrollPaneWithBars.OrientationType orientation){
        TextureRegionDrawable regionDrawable = new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getColorTextureRegion(color));

        if(orientation == ScrollPaneWithBars.OrientationType.HORIZONTAL) regionDrawable.setMinHeight(scrollSize);
        else regionDrawable.setMinWidth(scrollSize);

        return regionDrawable;
    }
}
