/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.Viewport;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.util.Log;

/**
 * Created by lhoracek on 3/6/15.
 */
public class XFillViewport extends Viewport {

    /**
     * Creates a new viewport using a new {@link com.badlogic.gdx.graphics.OrthographicCamera} with no maximum world size.
     */
    public XFillViewport() {
        setWorldSize(TablexiaSettings.getWorldSize(), TablexiaSettings.getMinWorldHeight());
        setCamera(new OrthographicCamera());
        Log.info(getClass(), "Viewport size: " + TablexiaSettings.getWorldSize() + "x" + TablexiaSettings.getMinWorldHeight());
    }

    private float cameraYOffset;

    @Override
    public void update(int screenWidth, int screenHeight, boolean centerCamera) {
        float newWorldHeight = Math.min(getWorldWidth() / ((float) screenWidth / (float) screenHeight), TablexiaSettings.getWorldSize());

        // FIT into screen keeping at least minimum ration
        //int screenX = (newWorldHeight < TablexiaSettings.getMinWorldHeight()) ? ((int) ((screenWidth - (screenHeight / TablexiaSettings.getMaximumRatio())) / 2)) : 0;
        int screenX = 0;
        int screenY = 0;
        //int newScreenWidth = (newWorldHeight < TablexiaSettings.getMinWorldHeight()) ? ((int) (screenHeight / TablexiaSettings.getMaximumRatio())) : screenWidth;
        int newScreenWidth = screenWidth;
        int newScreenHeight = screenHeight;

        // SET NEW WORLD HEIGHT
        this.setWorldHeight(Math.max(TablexiaSettings.getMinWorldHeight(), newWorldHeight)); // World width dooes not change
        this.setWorldWidth(TablexiaSettings.getWorldSize());
        cameraYOffset = (getWorldHeight() - TablexiaSettings.getMinWorldHeight()) / 2;
        setScreenBounds(screenX, screenY, newScreenWidth, newScreenHeight);
        apply(centerCamera);
    }

    @Override
    public void apply(boolean centerCamera) {
        Log.info(getClass(), "New viewport size (min: " + TablexiaSettings.getMinWorldHeight() + ") position: " + getScreenX() + ":" + getScreenY() + " - bounds: " + getScreenWidth() + ":" + getScreenHeight() + " - world: " + getWorldWidth() + "x" + getWorldHeight() + " - y offset: " + cameraYOffset);

        Gdx.gl.glViewport(getScreenX(), getScreenY(), getScreenWidth(), getScreenHeight());
        getCamera().viewportWidth = getWorldWidth();
        getCamera().viewportHeight = getWorldHeight();
        if (centerCamera) getCamera().position.set(getWorldWidth() / 2, getWorldHeight() / 2, 0);
        getCamera().position.y -= cameraYOffset;
        getCamera().update();
    }

    public float getCameraYOffset() {
        return cameraYOffset;
    }

}