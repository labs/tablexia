/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Timer;


/**
 * Created by Drahomir Karchnak on 05/12/15.
 */
public class MusicUtil {
    public static final float SOUND_EFFECT_INTERVAL   =   0.05f;
    public static final float MAX_EFFECT_DURATION     =   5;

    public static void fadeOut(final Music music, float time) {
		fadeOut(music, time, null, true);
	}

    public static void fadeOut(final Music music, float time, boolean dispose) {
        fadeOut(music, time, null, dispose);
    }

    public static void fadeOut(final Music music, float time, Music.OnCompletionListener completionListener) {
        fadeOut(music, time, completionListener, true);
    }

	public static void fadeIn(final Music music, float time) {
		fadeIn(music, time, true);
	}

    /**
     * Fades Out music by adjusting its volume over given time
     * and disposes it after the effect is done
     * @param music Music you want to apply the effect to
     * @param time Duration of the effect in seconds
     */
    public static void fadeOut(final Music music, float time, final Music.OnCompletionListener completionListener, final boolean dispose) {
        //Wrong arguments ? return...
        if(music == null) {
            return;
        }

        if (!music.isPlaying()) {
            if (dispose) music.dispose();
            return;
        }

        //Volume values we start with and we are trying to reach
        float startVolume = music.getVolume();
        final float endVolume = 0.0f;

        //How many times to repeat the "loop" to reach endVolume
        int repeatCount = (int) (time / SOUND_EFFECT_INTERVAL);

        //Wrong arguments ? return...
        if(time <= 0 || time > MAX_EFFECT_DURATION || repeatCount <= 0) {
            if (dispose) music.dispose();
            return;
        }

        //How much to substract from/add to volume value
        final float volumeStep  = (startVolume - endVolume) / repeatCount;

        //Timer with SOUND_EFFECT_INTERVAL and repeatCount to fit choosen time
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                float currentVolume = music.getVolume();
                float volume = MathUtils.clamp(currentVolume - volumeStep, 0, 1);

                if (volume <= endVolume) {
                    music.stop();
                    if (completionListener != null) completionListener.onCompletion(music);
                    if (dispose) music.dispose();
                    cancel();
                } else {
                    music.setVolume(volume);
                }
            }
        }, 0, SOUND_EFFECT_INTERVAL, repeatCount);
    }


    /**
     * Fades In music by adjusting its volume over given time
     * and disposes it after the effect is done
     * @param music Music you want to apply the effect to
     * @param time Duration of the effect in seconds
     */
    public static void fadeIn(final Music music, float time, final boolean dispose) {
        //Wrong arguments ? return...
        if(music == null || !music.isPlaying()) return;

        //Volume values we start with and we are trying to reach
        float startVolume = music.getVolume();
        final float endVolume = 1.0f;

        //How many times to repeat the "loop" to reach endVolume
        int repeatCount = (int) (time / SOUND_EFFECT_INTERVAL);

        //Wrong arguments ? return...
        if(time <= 0 || time > MAX_EFFECT_DURATION || repeatCount <= 0)
            return;

        //How much to substract from/add to volume value
        final float volumeStep  = (endVolume - startVolume) / repeatCount;

        if(!music.isPlaying())
            music.play();

        //Timer with SOUND_EFFECT_INTERVAL and repeatCount to fit choosen time
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if(music != null) {
                    float currentVolume = music.getVolume();
                    float volume = MathUtils.clamp(currentVolume + volumeStep, 0, 1);

                    if(volume >= endVolume) {
						music.stop();
						if(dispose) music.dispose();
                        cancel();
                    }
                    else {
                        music.setVolume(volume);
                    }
                }
            }
        }, 0, SOUND_EFFECT_INTERVAL, repeatCount);
    }
}