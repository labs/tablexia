/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.actions;

import com.badlogic.gdx.math.Path;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;

/** Moves an actor along a path.
 * @author Justin Shapcott */
public class MoveAlongAction extends TemporalAction {
    private float x, y;
    private boolean rotate;
    private Path<Vector2> path;
    private Vector2 value = new Vector2();

    public MoveAlongAction(Path<Vector2> path, float duration) {
        setPath(path);
        setDuration(duration);
    }

    public Path<Vector2> getPath () {
        return path;
    }

    public void setPath (Path<Vector2> path) {
        this.path = path;
    }

    public boolean isRotate() {
        return rotate;
    }

    public void setRotate (boolean rotate) {
        this.rotate = rotate;
    }

    @Override
    protected void begin () {
        x = actor.getX();
        y = actor.getY();
    }

    @Override
    protected void update (float percent) {
        path.valueAt(value, percent);
        actor.setPosition(x + value.x, y + value.y);
        if (rotate) {
            actor.setRotation(path.derivativeAt(value, percent).angle());
        }
    }

    @Override
    public void reset () {
        super.reset();
        path = null;
    }
}