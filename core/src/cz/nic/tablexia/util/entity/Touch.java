/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util.entity;

import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Vitaliy Vashchenko on 22.8.16.
 */
public class Touch {
    public static final float TIME_BETWEEN_TOUCHES = 300;
    public static final float TIME_BETWEEN_TAPS    = 600;

    private long  time;
    private Actor touchedActor;

    public Touch(Actor touchedActor, long time) {
        this.touchedActor = touchedActor;
        this.time = time;
    }

    public Actor getTouchedActor() {
        return touchedActor;
    }

    public long getTime() {
        return time;
    }

    private static boolean compareTouches(Touch firstTouch, Touch secondTouch, float time) {
        if (firstTouch.getTouchedActor() != null && secondTouch.getTouchedActor() != null) {
            return firstTouch.getTouchedActor().equals(secondTouch.getTouchedActor()) && Math.abs(secondTouch.getTime() - firstTouch.getTime()) < time;
        } else {
            return false;
        }
    }

    public static boolean isTap(Touch firstTouch, Touch secondTouch) {
        return compareTouches(firstTouch, secondTouch, TIME_BETWEEN_TOUCHES);
    }

    public static boolean isDoubleTap(Touch firstTouch, Touch secondTouch) {
        return compareTouches(firstTouch, secondTouch, TIME_BETWEEN_TAPS);
    }

}