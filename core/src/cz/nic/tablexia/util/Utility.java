/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.util;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;
import com.google.common.base.Splitter;

import java.util.Arrays;
import java.util.Map;

import cz.nic.tablexia.screen.AbstractTablexiaScreen;

public class Utility {

    public static AbstractTablexiaScreen<?> getScreenForScreenClass(Class<? extends AbstractTablexiaScreen<?>> screenClass, Map<String, String> initialState, Object ... parameters) {
        //Prepare array of classes for parameters, so we can identify the constructor we need to use.
        Class[] classParameters = new Class[parameters.length];
        for(int i = 0; i < parameters.length; i++) {
            classParameters[i] = parameters[i].getClass();
        }

        try {
            AbstractTablexiaScreen<?> abstractTablexiaScreen = (AbstractTablexiaScreen<?>) ClassReflection.getConstructor(screenClass, classParameters).newInstance(parameters);
            if (initialState != null) {
                abstractTablexiaScreen.setInitialState(initialState);
            }
            return abstractTablexiaScreen;
        } catch (ReflectionException e) {
            Log.err(Utility.class, "Cannot instantiate screen class: " + screenClass + " for parameters: " + Arrays.toString(classParameters), e);
        }
        return null;
    }

    public static Map<String, String> createChecksumMapFromString(String checksums) {
        if (checksums == null) {
            return null;
        }
        return Splitter.on(',').trimResults().withKeyValueSeparator(":").split(checksums);
    }

    public static String transformLocalAssetsPath(String localAssetsPath) {
        return localAssetsPath.replace('/', '.');
    }

    public static int[][] createColorMap(TextureRegion clickmap) {
        int clickmapWidth = clickmap.getRegionWidth();
        int clickmapHeight = clickmap.getRegionHeight();

        Pixmap pixmap = clickmap.getTexture().getTextureData().consumePixmap();

        int [][] colors = new int[clickmapWidth][clickmapHeight];
        for (int i = 0; i < clickmapWidth; i++) {
            for (int j = 0; j < clickmapHeight; j++) {
                colors[i][j] = pixmap.getPixel(clickmap.getRegionX() + i, clickmap.getRegionY() + j);
            }
        }

        pixmap.dispose();
        return colors;
    }

    public static int[][] createColorMap(Texture clickmap) {
        return createColorMap(new TextureRegion(clickmap));
    }

}
