/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.StringBuilder;
import io.sentry.DefaultSentryClientFactory;
import io.sentry.SentryClient;
import io.sentry.connection.Connection;
import io.sentry.connection.EventSendCallback;
import io.sentry.dsn.Dsn;
import io.sentry.event.Event;
import io.sentry.event.EventBuilder;
import io.sentry.event.helper.EventBuilderHelper;
import io.sentry.event.interfaces.ExceptionInterface;
import io.sentry.event.interfaces.SentryInterface;
import io.sentry.event.interfaces.StackTraceInterface;

import net.engio.mbassy.listener.Handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;

/**
 * Created by drahomir on 7/28/16.
 */
public class TablexiaSentry {
    private static final String EXCEPTION_TYPE_TAG_NAME  = "exception_type";
    private static final String MESSAGE_EXTRA_CLASS_NAME = "class";

    public enum ExceptionType {
        JavaException,
        JavaApplicationBusException,
        IOSException
    }

    /**
     * Custom sentry factory which allows to set EventSendCallback
     */
    private class TablexiaSentryFactory extends DefaultSentryClientFactory {
        private EventSendCallback eventSendCallback;

        public SentryClient sentryInstance(String DSN, EventSendCallback callback) {
            eventSendCallback = callback;
            return createSentryClient(new Dsn(DSN));
        }

        @Override
        protected Connection createConnection(Dsn dsn) {
            Connection connection = super.createConnection(dsn);

            if(eventSendCallback != null) connection.addEventSendCallback(eventSendCallback);

            return connection;
        }
    }

    /**
     * Type of information to send via sentry
     */
    private enum InfoType {
        TAG {
            @Override
            protected void insertInfo(TablexiaEventBuilderHelper tablexiaEventBuilderHelper, String name, String value) {
                tablexiaEventBuilderHelper.addTag(name, value);
            }
        },
        EXTRA {
            @Override
            protected void insertInfo(TablexiaEventBuilderHelper tablexiaEventBuilderHelper, String name, String value) {
                tablexiaEventBuilderHelper.addExtra(name, value);
            }
        };

        protected void insertInfo(TablexiaEventBuilderHelper tablexiaEventBuilderHelper, String name, String value) {}
    }

    /**
     * Actual info values to send via sentry
     */
    private enum Info {
        Platform(InfoType.TAG, "platform", new StringRunnable() {
            @Override
            public String run() {
                return Gdx.app.getType().toString();
            }
        }),
        BuildType(InfoType.TAG, "build_type", new StringRunnable() {
            @Override
            public String run() {
                return TablexiaSettings.getInstance().getBuildType().toString();
            }
        }),
        ConnectionType(InfoType.TAG, "connection_type", new StringRunnable() {
            @Override
            public String run() {
                return Tablexia.getConnectionManager().getConnectionType().toString();
            }
        }),
        Language(InfoType.TAG, "language", new StringRunnable() {
            @Override
            public String run() {
                return TablexiaSettings.getInstance().getLocale().toString();
            }
        }),
        UserInfo(InfoType.EXTRA, "user_info", new StringRunnable() {
            @Override
            public String run() {
                return TablexiaSettings.getInstance().getSelectedUser().toString();
            }
        }),
        UserUUID(InfoType.EXTRA, "user_uuid", new StringRunnable() {
            @Override
            public String run() {
                return TablexiaSettings.getInstance().getSelectedUser().getUuid();
            }
        });

        private static final String DEFAULT_FALLBACK_VALUE = "UNDEFINED";

        private final InfoType type;
        private final String name;
        private final String fallBackValue;
        private final StringRunnable stringRunnable;

        Info(InfoType type, String name, StringRunnable stringRunnable) {
            this(type, name, DEFAULT_FALLBACK_VALUE, stringRunnable);
        }

        Info(InfoType type, String name, String fallbackValue, StringRunnable stringRunnable) {
            this.type = type;
            this.name = name;
            this.fallBackValue = fallbackValue;
            this.stringRunnable = stringRunnable;
        }

        public void insert(TablexiaEventBuilderHelper eventBuilderHelper) {
            type.insertInfo(eventBuilderHelper, name, getSafeValue(stringRunnable, fallBackValue));
        }

        private String getSafeValue(StringRunnable runnable, String defaultValue) {
            try {
                return runnable.run();
            }
            catch (Exception e) {
                return defaultValue;
            }
        }
    }

    private interface StringRunnable {
        //Method which obtains the actual value of Info
        String run();
    }

    /**
     * Reports Manager - Saves and Resends reports later
     */
    private static class ReportsManager extends TablexiaAbstractFileManager {
        private static final String  REPORT_FILE_EXTENSION = ".TablexiaReport";
        private static final boolean HIDE_REPORT_FILES     = true;

        public ReportsManager() {
            super(ReportStorageType.EXTERNAL);
        }

        public void storeSentryEvent(Event event) {
            try {
                String fileName  = (HIDE_REPORT_FILES ? "." : "") + event.getId().toString() + REPORT_FILE_EXTENSION;

                File dir = TablexiaAbstractFileManager.getFileStoragePathFileHandle(ReportStorageType.EXTERNAL).file();
                if(!dir.exists()) dir.mkdir();

                File file = new File(dir, fileName);
                if(file.exists()) {
                    file.delete();
                    file.createNewFile();
                }

                FileOutputStream fileOut = new FileOutputStream(file);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOut);
                objectOutputStream.writeObject(event);
                objectOutputStream.close();
                fileOut.close();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }

        private void sendSavedEvents(TablexiaSentry tablexiaSentry) {
            try {
                File dir = TablexiaAbstractFileManager.getFileStoragePathFileHandle(ReportStorageType.EXTERNAL).file();
                if(!dir.exists()) return;

                File[] files = dir.listFiles();

                for(File file : files) {
                    Event e = deserializeEvent(file);
                    if(e != null) {
                        file.delete();
                        tablexiaSentry.sendEvent(e);
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }

        private Event deserializeEvent(File file) {
            Event event = null;

            try {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fis);

                event = (Event) ois.readObject();
                ois.close();
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return event;
        }
    }

    public class TablexiaEventBuilderHelper implements EventBuilderHelper {
        private HashMap<String, String> tablexiaTags;
        private HashMap<String, String> tablexiaExtras;

        public TablexiaEventBuilderHelper() {
            tablexiaTags   = new HashMap<String, String>();
            tablexiaExtras = new HashMap<String, String>();
        }

        public void addTag(String key, String value) {
            tablexiaTags.put(key, value);
        }

        public void addExtra(String key, String value) {
            tablexiaExtras.put(key, value);
        }

        public void removeTag(String key) {
            if(tablexiaTags.containsKey(key))
                tablexiaTags.remove(key);
        }

        public void removeExtra(String key) {
            if(tablexiaExtras.containsKey(key))
                tablexiaExtras.remove(key);
        }

        @Override
        public void helpBuildingEvent(EventBuilder eventBuilder) {
            for(Map.Entry<String, String> tag : tablexiaTags.entrySet()) {
                eventBuilder.withTag(tag.getKey(), tag.getValue());
            }

            for(Map.Entry<String, String> extra : tablexiaExtras.entrySet()) {
                eventBuilder.withExtra(extra.getKey(), extra.getValue());
            }
        }
    }

    /**
     * Actual Sentry client
     */
    private static final String[] SCREEN_INFO_EVENT_KEYS = new String [] {
            AbstractTablexiaGame.RANDOM_SEED_SCREEN_INFO_LABEL,
            AbstractTablexiaGame.GAME_DIFFICULTY_SCREEN_INFO_LABEL
    };

    private static final String LOG_HISTORY_TITLE           = "LOG HISTORY BELLOW";
    private static final int    LOG_HISTORY_LENGTH          = 10; //In messages
    private static final String LOG_HISTORY_TIME_FORMAT     = "HH:mm:ss";

    private static TablexiaSentry instance;

    private final EventSendCallback DEFAULT_SEND_EVENT_FAILURE_CALLBACK = new EventSendCallback() {
        @Override
        public void onFailure(Event event, Exception exception) {
            //Calls all registered callbacks
            for(EventSendCallback eventSendCallback : sendFailureCallbacks) eventSendCallback.onFailure(event, exception);
        }

        @Override
        public void onSuccess(Event event) {

        }
    };

    private Set<EventSendCallback> sendFailureCallbacks;
    private LinkedList<String> logHistory;
    private SentryClient sentry;

    private ReportsManager reportsManager;
    private TablexiaEventBuilderHelper tablexiaEventBuilderHelper;

    private TablexiaSentry(String DSN) {
        this.sentry = new TablexiaSentryFactory().sentryInstance(DSN, DEFAULT_SEND_EVENT_FAILURE_CALLBACK);
        this.sendFailureCallbacks = new HashSet<EventSendCallback>();
        this.reportsManager = new ReportsManager();
        this.tablexiaEventBuilderHelper = new TablexiaEventBuilderHelper();
        this.logHistory = new LinkedList<String>();

        sentry.addBuilderHelper(tablexiaEventBuilderHelper);

        ApplicationBus.getInstance().subscribe(this);
    }

    private static boolean isStarted() {
        return instance != null;
    }

    public static void start(String DSN) {
        if(DSN == null || instance != null) return;

        instance = new TablexiaSentry(DSN);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                e.printStackTrace();
                instance.sendExceptionEvent(t, e);
            }
        });

        instance.addEventSendFailureCallback(new EventSendCallback() {
            @Override
            public void onFailure(Event event, Exception exception) {
                instance.reportsManager.storeSentryEvent(event);
            }

            @Override
            public void onSuccess(Event event) {

            }
        });
    }

    public static void sendSavedReports() {
        if(isStarted()) instance.reportsManager.sendSavedEvents(instance);
    }

    public static void sendCustomException(ExceptionType exceptionType, String msg, StackTraceElement[] stackTraceElements) {
        if(!isStarted()) return;
        instance.buildAndSendExceptionEvent(msg, new StackTraceInterface(stackTraceElements), exceptionType);
    }

    public static void sendCustomException(ExceptionType exceptionType, String msg, Throwable cause) {
        if(!isStarted()) return;
        instance.buildAndSendExceptionEvent(msg, new ExceptionInterface(cause), exceptionType);
    }

    public static void sendCustomMessage(Class clazz, String message) {
        sendCustomMessage(clazz, message, Event.Level.INFO);
    }

    public static void log(Log.TablexiaLogLevel level, Class clazz, String message) {
        log(level, clazz.getSimpleName(), message);
    }

    public static void log(Log.TablexiaLogLevel level, Class clazz, String message, Throwable exception) {
        log(level, clazz.getSimpleName(), message, exception);
    }

    public static void log(Log.TablexiaLogLevel level, String tag, String message) {
        log(level, tag, message, null);
    }

    public static void log(Log.TablexiaLogLevel level, String tag, String message, Throwable exception) {
        if(!isStarted()) return;

        String actualMessage = tag + ": " + message;
        if(exception != null) actualMessage += "\n" + exception.getMessage();

        instance.logHistory.add(getCurrentTime() + " " + level + " " + actualMessage);

        if(instance.logHistory.size() > LOG_HISTORY_LENGTH) {
            instance.logHistory.removeFirst();
        }
    }

    private static String getCurrentTime() {
        return new SimpleDateFormat(LOG_HISTORY_TIME_FORMAT).format(new Date());
    }

    public static void sendCustomMessage(Class clazz, String message, Event.Level messageLevel) {
        if(!isStarted()) return;
        instance.buildAndSendMessageEvent(clazz, message, messageLevel);
    }

    private void addEventSendFailureCallback(EventSendCallback eventSendCallback) {
        if(isStarted()) {
            if (eventSendCallback != null) {
                if (!sendFailureCallbacks.contains(eventSendCallback))
                    sendFailureCallbacks.add(eventSendCallback);
            }
        }
    }

    private void buildAndSendExceptionEvent(String message, SentryInterface sentryInterface, ExceptionType exceptionType) {
        EventBuilder eventBuilder = prepareBasicEventBuilder(message, Event.Level.ERROR)
                .withSentryInterface(sentryInterface)
                .withTag(EXCEPTION_TYPE_TAG_NAME, exceptionType.toString());

        sentry.sendEvent(eventBuilder.build());
        Gdx.app.exit();
    }

    private void buildAndSendMessageEvent(Class clazz, String message, Event.Level messageLevel) {
        EventBuilder eventBuilder = prepareBasicEventBuilder(message, messageLevel)
                .withExtra(MESSAGE_EXTRA_CLASS_NAME, clazz.getName());

        sentry.sendEvent(eventBuilder.build());
    }

    private EventBuilder prepareBasicEventBuilder(String message, Event.Level level) {
        EventBuilder eventBuilder = new EventBuilder()
            .withMessage(message + "\n\n" + LOG_HISTORY_TITLE + "\n" + prepareLogHistoryString())
            .withRelease(TablexiaBuildConfig.APPLICATION_VERSION_NAME)
            .withLevel(level);

        addInfoToBuilder(eventBuilder);

        return eventBuilder;
    }

    private String prepareLogHistoryString() {
        StringBuilder logMessage = new StringBuilder();

        if(!logHistory.isEmpty()) {
            for(String log : logHistory) {
                logMessage.insert(0, log + "\n");
            }
        }

        return logMessage.toString();
    }

    private void addInfoToBuilder(EventBuilder eventBuilder) {
        for(Info info : Info.values()) {
            info.insert(tablexiaEventBuilderHelper);
        }

        sentry.runBuilderHelpers(eventBuilder);
    }

    private void sendExceptionEvent(Thread t, Throwable e) {
        buildAndSendExceptionEvent(e.getMessage(), new ExceptionInterface(e), ExceptionType.JavaException);
    }

    private void sendEvent(Event e) {
        if(isStarted() && e != null) instance.sentry.sendEvent(e);
    }

    @Handler
    public void handleScreenChangedEvent(TablexiaApplication.ScreenChangedEvent screenChangedEvent) {
        for(String infoEventKey : SCREEN_INFO_EVENT_KEYS) {
            tablexiaEventBuilderHelper.removeExtra(infoEventKey);
        }
    }

    @Handler
    public void handleScreenInfoEvent(AbstractTablexiaScreen.ScreenInfoEvent screenInfoEvent) {
        for(String infoEventKey : SCREEN_INFO_EVENT_KEYS) {
            if(infoEventKey.equals(screenInfoEvent.getInfoKey())) {
                tablexiaEventBuilderHelper.addExtra(screenInfoEvent.getInfoKey(), screenInfoEvent.getInfoValue());
            }
        }
    }
}