/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.createuser;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.event.StartFullSynchronizationEvent;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.loader.TablexiaTextureManager;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationAvatarManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.loader.application.TablexiaBadgesManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.menu.user.UserAvatarDefinition;
import cz.nic.tablexia.menu.user.UserMenu;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.about.AboutScreen;
import cz.nic.tablexia.screen.createuser.form.FormActorsLayout;
import cz.nic.tablexia.screen.createuser.form.SignatureInputPane;
import cz.nic.tablexia.screen.createuser.form.SignaturePane;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;
import cz.nic.tablexia.util.CameraOpener;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.listener.DragActorListener;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.Switch;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;
import cz.nic.tablexia.util.ui.TextFieldWithPlaceholder;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AdaptiveSizeDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.BackButtonHideComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DimmerDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DividerContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.SingleButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.SpeechDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TouchCloseDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ViewportMaximumSizeComponent;

public class FormScreen extends AbstractTablexiaScreen<Void> {
    public static final String SCENARIO_STEP_FORM_SCENE_VISIBLE     = "form scene visible";
    public static final String SCENARIO_STEP_MUGSHOTS_VISIBLE       = "mugshot images visible";
    public static final String SCENARIO_STEP_IMAGE_CHOSEN           = "image chosen";
    public static final String SCENARIO_STEP_TEXT_FIELD_TIMEOUT		= "text field timeout";
    public static final String PEN_SHAKED                           = "pen shaked";
    public static final String ALTERNATIVE_TEXT_FIELD_SHOWN         = "alternative text field shown";
    public static final String ALTERNATIVE_NAME_FIELD_HIDDEN        = "alternative name field hidden";

    public static final String FORM_SIGNATURE_DIALOG		        = "signature dialog";

    public static final String FIELD_FORM_NAME					    = "name field";
    public static final String BUTTON_FORM_AGE_PLUS				    = "age plus button";
    public static final String BUTTON_FORM_AGE_MINUS				= "age minus button";
    public static final String SWITCH_BUTTON_FORM_GENDER_FEMALE		= "switch button female";
    public static final String FORM_PEN					            = "formPen";
    public static final String FORM_SIGNATURE				        = "formSignature";
    public static final String FORM_AVATAR_IMAGE					= "avatar image";
    public static final String FORM_STAMP					        = "formStamp";
    public static final String ALTERNATIVE_TEXT_FIELD               = "alternative text field";
    public static final String YES_ICON                             = "yes icon";

    private static final int   PRIVACY_LABEL_WIDTH                  = 340;
    private static final int   PRIVACY_LABEL_HEIGHT                 = 40;
    private static final int   PRIVACY_LABEL_OFFSET_X               = 20;
    private static final int   PRIVACY_LABEL_OFFSET_Y               = 70;

    //TODO - Get rid of all magic values and strings in the code (Use constants)

    private static final Color                                  TEXT_COLOR                      = Color.BLACK;
    private static final ApplicationFontManager.FontType        TEXT_14                         = ApplicationFontManager.FontType.REGULAR_14;
    private static final ApplicationFontManager.FontType        TEXT_20                         = ApplicationFontManager.FontType.REGULAR_20;
    private static final ApplicationFontManager.FontType        TEXT_26                         = ApplicationFontManager.FontType.REGULAR_26;

    private static final Color                                  DEFAULT_LINK_COLOR       = new Color(0.27f, 0.53f, 0.8f,1);

    private   static final int    DEFAULT_USER_AGE                = 10;
    protected static final int    MAX_USER_AGE                    = 99;
    private   static final int    GENDER_SWITCH_MALE              = 0;
    private   static final int    GENDER_SWITCH_FEMALE            = 2;
    private   static final int    GENDER_SWITCH_UNKNOWN           = 1;
	private   static final int	  MAX_NAME_LENGTH				  = 15;

    private static final int    SHAKE_ANIMATION_OFFSET          = 10;
    private static final int    SHAKE_ANIMATION_REPEAT_COUNT    = 6;
    private static final float  SHAKE_ANIMATION_STEP_DURATION   = 0.1f;
    private static final float  SIGNATURE_Y_OFFSET_RATION       = 0.7f;

	private static final int    COMMON_INT_CODE_FOR_ENTER		= 13;
	private static final int    ANDROID_INT_CODE_FOR_ENTER		= 10;

    private static final int    TEXT_FIELD_HINT_DELAY           = 5000;

    protected Actor pen, mugshotFrame, mugshotFrameGreen, stamp, inkpad, ink;
    protected Image stampPlaceholder, mugshotImage, badge, book;
    protected Actor buttonPlus, buttonMinus;
    protected TablexiaLabel ageValue, genderLabel, ageLabel, privacyInfoLabel;
    protected Switch switchGender;
    protected Image  imageMale, imageFemale, signaturePlace, clearName;
    protected SignaturePane signaturePane;
    protected Map<FormValidationEnum, Boolean> validations;
    protected Set<String> alreadyPlayedHints;
    private Group mugshots;
    private Timer timer;
    private boolean timerSet, timerDisabled;
    private boolean finalStamp; //set true when form is complete to prevent unwanted multiple checking of form
    private Image dimmer;
    private TextField alternativeTextField;
    private Image alternativeTextLine, noIcon, yesIcon;
    private Group alternativeTextFieldGroup;

    protected TextField nameField;

    protected int                 age;
    private GenderDefinition    gender;

    private TablexiaComponentDialog dialog;

    protected UserAvatarDefinition userAvatarDefinition;
    private String signature;

    private float defaultViewportHeight;

    //Stored se it can be disposed
    private Texture cursorTexture;

    //Pixmap that gets saved, when user completes the form.
    //Its size is ApplicationAvatarManager.AVATAR_HEIGHT and WIDTH.
    protected Pixmap  customAvatarPixmap;

    //Texture that gets created from customAvatarPicture. This texture has resolution next power of 2 that is greater then customAvatarPixmap width and height.
    //e.q. custom avatar pixmap is 300x360, this texture will be 512x512
    //Its to support older devices with older opengl support, which prefers textures with resolution of power of 2...
    protected TextureRegion customAvatarPreviewTextureRegion;

    //Android GalleryOpener implementation is asynchronous, while desktops and ios is synchronous
    //This field is used to to flag a state, for asynchronous gallery opener implementations...
    private boolean choosingCustomAvatar = false;

    private TablexiaTextureManager textureManager;
    private String badgePath;

    @Override
    public boolean canBePaused() {
        return false;
    }

    @Override
	protected void screenVisible(Map<String, String> screenState) {
        triggerEventFormSceneVisible();
	}

    @Override
    protected Void prepareScreenData(Map<String, String> screenState) {
        final CountDownLatch downLatch = new CountDownLatch(1);
        badgePath = TablexiaBadgesManager.FullRankBadges.BADGE_NONE.getFilePath();

        synchronized (downLatch){
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    textureManager = new TablexiaTextureManager();
                    textureManager.loadTexture(badgePath);
                    textureManager.finishLoading();
                    downLatch.countDown();

                }
            });
        }

        try {
            downLatch.await();
        } catch (InterruptedException e) {
            Log.err(getClass(), "Cannot load texture: " + badgePath);
        }

        return null;
    }

    private void triggerEventFormSceneVisible() {
        AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_STEP_FORM_SCENE_VISIBLE);
    }

    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        final Group group = new Group();
        defaultViewportHeight = getViewportHeight();

        validations = new HashMap<FormValidationEnum, Boolean>();
        alreadyPlayedHints = new HashSet<String>();

        Image background = new TablexiaNoBlendingImage(getApplicationTextureRegion(ApplicationAtlasManager.BACKGROUND_WOODEN));
        ScaleUtil.setBackgroundBounds(background);
        group.addActor(background);

        Image coverName;

        privacyInfoLabel = new TablexiaLabel(getText(FormScreenAssets.TEXT_FORM_LEGAL_INFO), new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_14, DEFAULT_LINK_COLOR));

        privacyInfoLabel.setSize(PRIVACY_LABEL_WIDTH, PRIVACY_LABEL_HEIGHT);
        privacyInfoLabel.setAlignment(Align.center);
        privacyInfoLabel.setWrap(true);
        privacyInfoLabel.addListener(new ClickListenerWithSound(){
            @Override
            public void onClick(InputEvent event, float x, float y) {
                super.onClick(event, x, y);
                Tablexia.getNet().openURI(AboutScreen.LocaleLinks.getLocaleLinksByLocale(TablexiaSettings.getInstance().getLocale()).getZOOU().getLinkAddress());
            }
        });


        group.addActor(book = ScaleUtil.createImageSizePosition(getScreenTextureRegion(FormScreenAssets.BACKGROUND), getStage().getWidth() * FormActorsLayout.BACKGROUND_WIDTH, TablexiaSettings.getMinWorldHeight() * FormActorsLayout.BACKGROUND_HEIGHT, getStage().getWidth() * FormActorsLayout.BACKGROUND_X, 0));
        group.addActor(inkpad = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.INKPAD), getStage().getWidth() * FormActorsLayout.INK_PAD_WIDTH, getStage().getWidth() * FormActorsLayout.INK_PAD_X, getStage().getWidth() * FormActorsLayout.INK_PAD_Y));
        group.addActor(mugshotImage = ScaleUtil.createImageWidthPosition(getApplicationTextureRegion(ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_0), getStage().getWidth() * FormActorsLayout.MUGSHOT_IMAGE_WIDTH, getStage().getWidth() * FormActorsLayout.MUGSHOT_IMAGE_X, getStage().getWidth() * FormActorsLayout.MUGSHOT_IMAGE_Y)); // size for all mugshots
        group.addActor(mugshotFrame = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.MUGSHOT_FRAME), getStage().getWidth() * FormActorsLayout.MUGSHOT_FRAME_WIDTH, getStage().getWidth() * FormActorsLayout.MUGSHOT_FRAME_X, getStage().getWidth() * FormActorsLayout.MUGSHOT_FRAME_Y));
        group.addActor(mugshotFrameGreen = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.MUGSHOT_FRAME_GREEN), getStage().getWidth() * FormActorsLayout.MUGSHOT_FRAME_WIDTH, getStage().getWidth() * FormActorsLayout.MUGSHOT_FRAME_X, getStage().getWidth() * FormActorsLayout.MUGSHOT_FRAME_Y));
        group.addActor(coverName = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.TEXTFIELD_COVERNAME), getStage().getWidth() * FormActorsLayout.COVER_NAME_WIDTH, getStage().getWidth() * FormActorsLayout.COVER_NAME_X, getStage().getWidth() * FormActorsLayout.COVER_NAME_Y));
        group.addActor(stampPlaceholder = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.STAMP_PLACEHOLDER), getStage().getWidth() * FormActorsLayout.STAMP_PLACEHOLDER_WIDTH, getStage().getWidth() * FormActorsLayout.STAMP_PLACEHOLDER_X, getStage().getWidth() * FormActorsLayout.STAMP_PLACEHOLDER_Y));
        group.addActor(signaturePlace = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.TEXTFIELD_SIGNATURE), getStage().getWidth() * FormActorsLayout.SIGNATURE_PLACE_WIDTH, getStage().getWidth() * FormActorsLayout.SIGNATURE_PLACE_X, getStage().getWidth() * FormActorsLayout.SIGNATURE_PLACE_Y));
        group.addActor(ink = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.INK), getStage().getWidth() * FormActorsLayout.INK_WIDTH, getStage().getWidth() * FormActorsLayout.INK_X, getStage().getHeight() / 2));


        group.addActor(privacyInfoLabel);
        privacyInfoLabel.setPosition(book.getX() + PRIVACY_LABEL_OFFSET_X, book.getY() + PRIVACY_LABEL_OFFSET_Y);

        // button for clearing user cover name
        clearName = new Image(getScreenTextureRegion(FormScreenAssets.CLEAR));
        clearName.setPosition(coverName.getX() + coverName.getWidth() + FormActorsLayout.CLEAR_NAME_BUTTON_PADDING, coverName.getY());
        clearName.setSize(clearName.getWidth() / 2, clearName.getHeight() / 2);
        clearName.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                nameField.setText("");
                validate();
                return false;
            }
        });
        group.addActor(clearName);

        // TODO pens and stamps variant
        mugshotFrame.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                showMugshotDialog();
                clearValidations();
            }
        });
        mugshotFrameGreen.setVisible(false);

        this.age = DEFAULT_USER_AGE;
        TablexiaLabel.TablexiaLabelStyle labelStyleFontSize16 = new TablexiaLabel.TablexiaLabelStyle(TEXT_14, TEXT_COLOR);
        TablexiaLabel.TablexiaLabelStyle labelStyleFontSize20 = new TablexiaLabel.TablexiaLabelStyle(TEXT_20, TEXT_COLOR);

        ageLabel = new TablexiaLabel(getText(FormScreenAssets.TEXT_FORM_AGE), labelStyleFontSize16);
        ageValue = new TablexiaLabel(String.valueOf(this.age), labelStyleFontSize20);
        genderLabel = new TablexiaLabel(getText(FormScreenAssets.TEXT_FORM_GENDER), labelStyleFontSize16);

        nameField = new TextFieldWithPlaceholder("", new TextFieldWithPlaceholder.TablexiaTextFieldStyle(ApplicationFontManager.FontType.REGULAR_18, TEXT_COLOR, createCursorForTextField(), null, null), getText("createuser_form_username"));
        nameField.setSize(getStage().getWidth() * FormActorsLayout.NAME_FIELD_WIDTH, getStage().getWidth() * FormActorsLayout.NAME_FIELD_HEIGHT);
        nameField.setPosition(getStage().getWidth() * FormActorsLayout.NAME_FIELD_X, getStage().getWidth() * FormActorsLayout.NAME_FIELD_Y);
        nameField.setFocusTraversal(false);
		nameField.setMaxLength(MAX_NAME_LENGTH);

        timerSet = false;
        timerDisabled = false;

        nameField.addListener(new InputListener() {
            @Override
            public boolean keyTyped(InputEvent event, char character) {

                if(timerSet) timer.cancel();

                // have to check also (int)character because of some bug in iOS keyboard. It returns code Input.Keys.ENTER for every key after "done" button pressed
                if ((event.getKeyCode() == Input.Keys.ENTER && ((int)character == ANDROID_INT_CODE_FOR_ENTER || (int)character == COMMON_INT_CODE_FOR_ENTER)) || event.getKeyCode() == Input.Keys.TAB) {
                    validate();
                } else if(!timerDisabled){
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if(timerDisabled)
                                return;

                            validate();
                            timerSet = false;
                            triggerScenarioStepEvent(SCENARIO_STEP_TEXT_FIELD_TIMEOUT);
                        }
                    }, TEXT_FIELD_HINT_DELAY);
                    timerSet = true;
                }
                return false;
            }
        });
        nameField.setOnscreenKeyboard(new TextField.OnscreenKeyboard() {
            @Override
            public void show(boolean visible) {
                if(TablexiaSettings.getInstance().getPlatform() != TablexiaSettings.Platform.DESKTOP) {
                    Gdx.input.setOnscreenKeyboardVisible(true);
                    showAlternativeTextField();
                    triggerScenarioStepEvent(ALTERNATIVE_TEXT_FIELD_SHOWN);
                }
            }
        });

        ageLabel.setPosition(getStage().getWidth() * FormActorsLayout.AGE_LABEL_X, getStage().getWidth() * FormActorsLayout.AGE_LABEL_Y);
        ageValue.setPosition(getStage().getWidth() * FormActorsLayout.AGE_VALUE_X, getStage().getWidth() * FormActorsLayout.AGE_VALUE_Y);
        genderLabel.setPosition(getStage().getWidth() * FormActorsLayout.GENDER_LABEL_X, getStage().getWidth() * FormActorsLayout.GENDER_LABEL_Y);

        group.addActor(nameField);
        group.addActor(ageValue);
        group.addActor(ageLabel);
        group.addActor(genderLabel);
        group.addActor(imageFemale = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.SEX_FEMALE), getStage().getWidth() * FormActorsLayout.IMAGE_FEMALE_WIDTH, getStage().getWidth() * FormActorsLayout.IMAGE_FEMALE_X, getStage().getWidth() * FormActorsLayout.IMAGE_FEMALE_Y));
        group.addActor(imageMale = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.SEX_MALE), getStage().getWidth() * FormActorsLayout.IMAGE_MALE_WIDTH, getStage().getWidth() * FormActorsLayout.IMAGE_MALE_X, getStage().getWidth() * FormActorsLayout.IMAGE_MALE_Y));
        group.addActor(prepareBadge());
        group.addActor(buttonMinus = new Button(new TextureRegionDrawable(getScreenTextureRegion(FormScreenAssets.BUTTON_MINUS)), new TextureRegionDrawable(getScreenTextureRegion(FormScreenAssets.BUTTON_MINUS_PRESSED))));
        group.addActor(buttonPlus = new Button(new TextureRegionDrawable(getScreenTextureRegion(FormScreenAssets.BUTTON_PLUS)), new TextureRegionDrawable(getScreenTextureRegion(FormScreenAssets.BUTTON_PLUS_PRESSED))));
        buttonMinus.setBounds(getStage().getWidth() * FormActorsLayout.BUTTON_MINUS_X, getStage().getWidth() * FormActorsLayout.BUTTON_MINUS_Y, getStage().getWidth() * FormActorsLayout.BUTTON_MINUS_WIDTH, getStage().getWidth() * FormActorsLayout.BUTTON_MINUS_HEIGHT);
        buttonPlus.setBounds(getStage().getWidth() * FormActorsLayout.BUTTON_PLUS_X, getStage().getWidth() * FormActorsLayout.BUTTON_PLUS_Y, getStage().getWidth() * FormActorsLayout.BUTTON_PLUS_WIDTH, getStage().getWidth() * FormActorsLayout.BUTTON_PLUS_HEIGHT);
        buttonPlus.addListener(new ClickListenerWithSound() {
            @Override
            public void onClick(InputEvent event, float x, float y) {
                onClickPlus();
            }
        });
        buttonMinus.addListener(new ClickListenerWithSound() {
            @Override
            public void onClick(InputEvent event, float x, float y) {
               onClickMinus();
            }
        });

        // TODO images as parameters for Switch's constructor
        switchGender = new Switch(getScreenTextureRegion(FormScreenAssets.SWITCH_BACKGROUND), getScreenTextureRegion(FormScreenAssets.SWITCH_LEFT), getScreenTextureRegion(FormScreenAssets.SWITCH_CENTER), getScreenTextureRegion(FormScreenAssets.SWITCH_RIGHT));
        switchGender.setPosition(getStage().getWidth() * FormActorsLayout.SWITCH_GENDER_X, getStage().getWidth() * FormActorsLayout.SWITCH_GENDER_Y);
        switchGender.setScale(FormActorsLayout.SWITCH_GENDER_SCALE);
        switchGender.setSwitchSelectedListener(new Switch.DragSwitchListener.SwitchSelectedListener() {
            @Override
            public void stepSelected(int step) {
                if (step == GENDER_SWITCH_UNKNOWN) {
                    FormScreen.this.gender = null;
                } else {
                    FormScreen.this.gender = (step == GENDER_SWITCH_FEMALE ? GenderDefinition.FEMALE : GenderDefinition.MALE);
                    validate();
                }
            }
        });
        switchGender.switchToStep(GENDER_SWITCH_UNKNOWN);
        switchGender.setDisabledStep(GENDER_SWITCH_UNKNOWN);
        group.addActor(switchGender);
        imageFemale.addListener(new ClickListenerWithSound() {
            @Override
            public void onClick(InputEvent event, float x, float y) {
                switchGender.switchToStep(GENDER_SWITCH_FEMALE);
                validate();
            }
        });

        imageMale.addListener(new ClickListenerWithSound() {
            @Override
            public void onClick(InputEvent event, float x, float y) {
                switchGender.switchToStep(GENDER_SWITCH_MALE);
                validate();
            }
        });

        // signature pen needs to be below pen and stamp
        signaturePane = new SignaturePane();
        group.addActor(signaturePane);
        signaturePane.setBounds(getSceneWidth() * FormActorsLayout.SIGNATURE_PANE_X, getSceneInnerHeight() * FormActorsLayout.SIGNATURE_PANE_Y, getSceneWidth() * FormActorsLayout.SIGNATURE_PANE_WIDTH, getSceneInnerHeight() * FormActorsLayout.SIGNATURE_PANE_HEIGHT);

        signaturePane.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());

        // pencil and stamp layers
        group.addActor(pen = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.PENCIL_DROPPED), getStage().getWidth() * FormActorsLayout.PEN_WIDTH, getStage().getWidth() * FormActorsLayout.PEN_X, getStage().getWidth() * FormActorsLayout.PEN_Y));
        group.addActor(stamp = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.STAMP_LEFT_DROPPED), getStage().getWidth() * FormActorsLayout.STAMP_WIDTH, getStage().getWidth() * FormActorsLayout.STAMP_X, getStage().getWidth() * FormActorsLayout.STAMP_Y));
        pen.addListener(new DragActorListener(pen, true, new DragActorListener.DragListenerAdapter() {
            @Override
            public void onDropped(float x, float y) {
                if ((x > FormActorsLayout.PEN_DROP_AREA_LEFT_X && x < FormActorsLayout.PEN_DROP_AREA_RIGHT_X) && (y > FormActorsLayout.PEN_DROP_AREA_BOTTOM_Y && y < FormActorsLayout.PEN_DROP_AREA_TOP_Y)) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            showSubscribeDialog();
                        }
                    });
                }
            }

            @Override
            public void onStarted(float x, float y) {
                clearValidations();
                pen.clearActions();
            }

            @Override
            public void onMoveBackStart(float x, float y) {
                validate();
            }
        }));

        stamp.addListener(new DragActorListener(stamp, true, new DragActorListener.DragListenerAdapter() {
            @Override
            public void onDropped(float x, float y) {
                if ((x > FormActorsLayout.STAMP_DROP_AREA_LEFT_X && x < FormActorsLayout.STAMP_DROP_AREA_RIGHT_X) && (y > FormActorsLayout.STAMP_DROP_AREA_BOTTOM_Y && y < FormActorsLayout.STAMP_DROP_AREA_TOP_Y)) {
                    finalStamp = true;
                    if (!validate()) {
                        Log.debug("Validator", "Form is not valid");
                        group.setTouchable(Touchable.enabled);
                        group.addListener(new InputListener() {
                            @Override
                            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                                if(button == Input.Buttons.RIGHT) return false;
                                group.removeListener(this);
                                group.setTouchable(Touchable.childrenOnly);
                                return super.touchDown(event, x, y, pointer, button);
                            }
                        });
                        return;
                    }
					clearValidations();
					setValid(FormValidationEnum.STAMP, true);
                    stampIt();

                    User user = UserDAO.createUser(nameField.getText().trim(), FormScreen.this.age, FormScreen.this.gender, Integer.toString(userAvatarDefinition.number()), signature);
                    if(user != null) {
                        if(UserAvatarDefinition.isUserUsingCustomAvatar(user)) {
                            ApplicationAvatarManager.getInstance().createCustomAvatar(user, customAvatarPixmap, customAvatarPreviewTextureRegion);
                            ApplicationBus.getInstance().post(new UserMenu.RefreshUserMenu()).asynchronously();
                        }

                        UserRankManager.getInstance().onNewUser(user);

                        TablexiaSettings.getInstance().changeUser(user);
                        ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(TablexiaSettings.INITIAL_SCREEN, TablexiaApplication.ScreenTransaction.FADE)).asynchronously();
                        ApplicationBus.getInstance().publishAsync(new StartFullSynchronizationEvent(user));
                    }
                    else {
                        ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(TablexiaSettings.LOADER_SCREEN, TablexiaApplication.ScreenTransaction.FADE)).asynchronously();
                    }
                }
            }

            @Override
            public void onStarted(float x, float y) {
                clearValidations();
                clearStampUseMeAction();
            }

            @Override
            public void onMoveBackStart(float x, float y) {
                validate();
            }
        }));

        alternativeTextFieldGroup = new Group();
        createAlternativeFieldElements();

        nameField.setName(FIELD_FORM_NAME);
        mugshotFrame.setName(FORM_AVATAR_IMAGE);
        buttonPlus.setName(BUTTON_FORM_AGE_PLUS);
        buttonMinus.setName(BUTTON_FORM_AGE_MINUS);
        imageFemale.setName(SWITCH_BUTTON_FORM_GENDER_FEMALE);
        pen.setName(FORM_PEN);
        signaturePane.setName(FORM_SIGNATURE);
        stamp.setName(FORM_STAMP);

        getStage().addActor(group);
        getStage().addActor(alternativeTextFieldGroup);

        validate();
    }

    private void createAlternativeFieldElements() {
        dimmer = new Image(ApplicationAtlasManager.getInstance().getColorTexture(new Color(0, 0, 0, 0.8f)));
        dimmer.setPosition(getViewportLeftX(), getSceneOuterBottomY());
        dimmer.setSize(getViewportWidth(), getSceneOuterHeight());

        alternativeTextField = new TextFieldWithPlaceholder("", new TextFieldWithPlaceholder.TablexiaTextFieldStyle(ApplicationFontManager.FontType.REGULAR_20, Color.WHITE, createCursorForTextField(), null, null), getText("createuser_form_username"));
        alternativeTextField.setSize(getStage().getWidth() * FormActorsLayout.NAME_FIELD_WIDTH, getStage().getWidth() * FormActorsLayout.NAME_FIELD_HEIGHT);
        alternativeTextField.setPosition(getStage().getWidth()/2 - alternativeTextField.getWidth()/2, 3*getStage().getHeight()/5);
        alternativeTextField.setFocusTraversal(false);
        alternativeTextField.setMaxLength(MAX_NAME_LENGTH);
        alternativeTextField.setName(ALTERNATIVE_TEXT_FIELD);
        alternativeTextField.addListener(new InputListener() {
            @Override
            public boolean keyTyped(InputEvent event, char character) {

                // have to check also (int)character because of some bug in iOS keyboard. It returns code Input.Keys.ENTER for every key after "done" button pressed
                if ((event.getKeyCode() == Input.Keys.ENTER && ((int)character == ANDROID_INT_CODE_FOR_ENTER || (int)character == COMMON_INT_CODE_FOR_ENTER)) || event.getKeyCode() == Input.Keys.TAB) {
                    nameField.setText(alternativeTextField.getText());
                    hideAlternativeField();
                    validate();
                }
                return false;
            }
        });

        alternativeTextLine = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(FormScreenAssets.ALTERNATIVE_TEXTFIELD), getStage().getWidth() * FormActorsLayout.COVER_NAME_WIDTH, alternativeTextField.getX(), alternativeTextField.getY());
        alternativeTextLine.setColor(Color.WHITE);

        noIcon = new Image(getScreenTextureRegion(FormScreenAssets.NO_ICON));
        noIcon.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                alternativeTextField.setText("");
                nameField.setText("");
            }
        });
        noIcon.setPosition(alternativeTextField.getX() - PRIVACY_LABEL_OFFSET_X - noIcon.getWidth(), alternativeTextField.getY());

        yesIcon = new Image(getScreenTextureRegion(FormScreenAssets.YES_ICON));
        yesIcon.setName(YES_ICON);
        yesIcon.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                nameField.setText(alternativeTextField.getText());
                hideAlternativeField();
                validate();
                Gdx.input.setOnscreenKeyboardVisible(false);
                triggerScenarioStepEvent(ALTERNATIVE_NAME_FIELD_HIDDEN);
            }
        });
        yesIcon.setPosition(alternativeTextField.getX() + alternativeTextField.getWidth() + PRIVACY_LABEL_OFFSET_X, alternativeTextField.getY());

        alternativeTextFieldGroup.addActor(dimmer);
        alternativeTextFieldGroup.addActor(alternativeTextLine);
        alternativeTextFieldGroup.addActor(alternativeTextField);
        alternativeTextFieldGroup.addActor(noIcon);
        alternativeTextFieldGroup.addActor(yesIcon);
        hideAlternativeField();
    }

    private void hideAlternativeField() {
        alternativeTextFieldGroup.setVisible(false);
    }

    private void showAlternativeTextField() {
        clearValidations();
        alternativeTextFieldGroup.setVisible(true);
        getStage().setKeyboardFocus(alternativeTextField);
    }

    protected void onClickPlus(){
        setAge(Math.min(FormScreen.this.age + 1, MAX_USER_AGE));
        setValid(FormValidationEnum.AGE, true);
        validate();
    }

    protected void onClickMinus(){
        setAge(Math.max(FormScreen.this.age - 1, 1));
        setValid(FormValidationEnum.AGE, true);
        validate();
    }

    protected Actor prepareBadge() {

        Texture badgeTexture = textureManager.getTexture(badgePath);
        badgeTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        badge = ScaleUtil.createImageWidthPosition(
                badgeTexture,
                getStage().getWidth() * FormActorsLayout.BADGE_WIDTH,
                getStage().getWidth() * FormActorsLayout.BADGE_X, getStage().getWidth() * FormActorsLayout.BADGE_Y
        );
        return badge;
    }

    private boolean validate() {

        if (validationEnabled() == false) {
            return true;
        }

        clearValidations();

		setValid(FormValidationEnum.MUGSHOT, userAvatarDefinition != null);
        if (isValid(FormValidationEnum.MUGSHOT) == false) {
            displayHintDialog(FormValidationEnum.MUGSHOT, mugshotImage);
            playValidationSound(FormScreenAssets.VALIDATION_MUGSHOT_SOUND);
            return false;
        }

		setValid(FormValidationEnum.NAME, nameField.getText() != null && !nameField.getText().isEmpty() && nameField.getText().trim().length() > 0);
        if (isValid(FormValidationEnum.NAME) == false) {
            displayHintDialog(FormValidationEnum.NAME, nameField);
            playValidationSound(FormScreenAssets.VALIDATION_USERNAME_SOUND);
            return false;
        }

        if (isValid(FormValidationEnum.AGE) == false) {
            displayHintDialog(FormValidationEnum.AGE, ageValue);
            playValidationSound(FormScreenAssets.VALIDATION_AGE_SOUND);
            return false;
        }

		setValid(FormValidationEnum.GENDER, gender != null);
        if (isValid(FormValidationEnum.GENDER) == false) {
            displayHintDialog(FormValidationEnum.GENDER, switchGender);
            playValidationSound(FormScreenAssets.VALIDATION_GENDER_SOUND);
            return false;
        }

		setValid(FormValidationEnum.SIGNATURE, signature != null);
        if (isValid(FormValidationEnum.SIGNATURE) == false) {
            displayHintDialog(FormValidationEnum.SIGNATURE, signaturePlace);
            playValidationSound(FormScreenAssets.VALIDATION_SIGNATURE_SOUND);
            pen.addAction(Actions.forever(Actions.sequence(Actions.repeat(SHAKE_ANIMATION_REPEAT_COUNT, Actions.sequence(Actions.moveBy(SHAKE_ANIMATION_OFFSET, 0, SHAKE_ANIMATION_STEP_DURATION), Actions.moveBy(-SHAKE_ANIMATION_OFFSET, 0, SHAKE_ANIMATION_STEP_DURATION))), Actions.delay(1), Actions.run(new Runnable() {
                @Override
                public void run() {
                    triggerScenarioStepEvent(PEN_SHAKED);
                }
            }))));
            return false;
        }

       if (isValid(FormValidationEnum.STAMP) == false && !finalStamp) {
            displayHintDialog(FormValidationEnum.STAMP, stampPlaceholder);
            playValidationSound(FormScreenAssets.VALIDATION_STAMP_SOUND);
            stamp.addAction(Actions.forever(Actions.sequence(Actions.repeat(SHAKE_ANIMATION_REPEAT_COUNT, Actions.sequence(Actions.moveBy(SHAKE_ANIMATION_OFFSET, 0, SHAKE_ANIMATION_STEP_DURATION), Actions.moveBy(-SHAKE_ANIMATION_OFFSET, 0, SHAKE_ANIMATION_STEP_DURATION))), Actions.delay(1))));
        }

        return true;
    }

	private void setValid(FormValidationEnum key, boolean valid) {
		validations.put(key, valid);
		nameField.setTouchable(canEdit(FormValidationEnum.NAME) ? Touchable.enabled : Touchable.disabled);
		switchGender.setTouchable(canEdit(FormValidationEnum.GENDER) ? Touchable.enabled : Touchable.disabled);
		imageFemale.setTouchable(canEdit(FormValidationEnum.GENDER) ? Touchable.enabled : Touchable.disabled);
		imageMale.setTouchable(canEdit(FormValidationEnum.GENDER) ? Touchable.enabled : Touchable.disabled);
		buttonPlus.setTouchable(canEdit(FormValidationEnum.AGE) ? Touchable.enabled : Touchable.disabled);
		buttonMinus.setTouchable(canEdit(FormValidationEnum.AGE) ? Touchable.enabled : Touchable.disabled);
		pen.setTouchable(canEdit(FormValidationEnum.SIGNATURE) ? Touchable.enabled : Touchable.disabled);
		stamp.setTouchable(canEdit(FormValidationEnum.STAMP) ? Touchable.enabled : Touchable.disabled);
	}

	private boolean canEdit(FormValidationEnum key) {
		List<FormValidationEnum> shouldBeValid = key.getPredecessors();
		if (shouldBeValid != null) {
			for (FormValidationEnum validation : shouldBeValid) {
				if (!(validations.containsKey(validation) && validations.get(validation))) {
					return false;
				}
			}
		}
		return true;
	}

    private boolean isValid(FormValidationEnum key) {
        return (validations.containsKey(key) && validations.get(key));
    }

    private void clearValidations() {
        clearPenUseMeAction();
        clearStampUseMeAction();

        if (dialog != null)
            dialog.hide();
    }

    private void clearPenUseMeAction() {
        for (Action action : pen.getActions()) {
            if (action instanceof RepeatAction) {
                pen.removeAction(action);
            }
        }
    }

    private void clearStampUseMeAction() {
        for (Action action : stamp.getActions()) {
            if (action instanceof RepeatAction) {
                stamp.removeAction(action);
            }
        }
    }

    protected void stampIt() {
        Drawable d = new TextureRegionDrawable(new TextureRegion(getScreenTextureRegion(FormScreenAssets.STAMP)));
        stampPlaceholder.setDrawable(d);
    }

    protected void showMugshotDialog() {
        mugshots = new Group();

        // grey translucent overlay
        Image overlay = new Image(ApplicationAtlasManager.getInstance().getColorTexture(ApplicationAtlasManager.COLOR_OVERLAY));
        mugshots.addActor(ScaleUtil.setBackgroundBounds(overlay));
        overlay.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                event.stop();
                mugshots.remove();
                choosingCustomAvatar = false;
                return false;
            }
        });

        // avatar pictures
        HorizontalGroup hg = prepareMugshots();

        ScrollPane sp = new ScrollPane(hg);
        sp.setScrollingDisabled(false, true);
        sp.setSize(getSceneWidth(), hg.getHeight());
        sp.setPosition(0, FormActorsLayout.MUGSHOTS_SCROLL_PANE_Y);
        mugshots.addActor(sp);
        getStage().addActor(mugshots);
        AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_STEP_MUGSHOTS_VISIBLE);
	}

    private Actor prepareCustomAvatarMugshot() {
        final Actor avatar = prepareAvatarPicture(UserAvatarDefinition.CUSTOM_AVATAR);
        avatar.setName(String.valueOf(UserAvatarDefinition.CUSTOM_AVATAR.number()));

        avatar.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (Tablexia.getCameraOpener().cameraAccessible()) {
                    event.stop();
                    choosingCustomAvatar = true;

                    //Adds listener, which is waiting for user to choose an image...
                    Tablexia.getCameraOpener().setOnPhotoTakenListener(new CameraOpener.OnPhotoTakenListener() {
                        @Override
                        public void onPhotoTaken(Pixmap pixmap, boolean success) {
                            if (!success) {
                                choosingCustomAvatar = false;
                                return;
                            }

                            if (choosingCustomAvatar) {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        pixmap.setFilter(Pixmap.Filter.BiLinear);
                                        setCustomMugshot(pixmap);
                                    }
                                });
                            }
                        }
                    });

                    //Request Camera View. Calls listener above when finished.
                    Tablexia.getCameraOpener().openCamera();
                } else {
                    TablexiaComponentDialogFactory.getInstance().createSingleButtonNotifyDialog(
                            ApplicationTextManager.getInstance().getText
                                    (ApplicationTextManager.ApplicationTextsAssets.CAMERA_ACCESS_NOTIFICATION),
                            null,
                            true)
                            .show(Tablexia.ERROR_DIALOG_WIDTH, Tablexia.ERROR_DIALOG_HEIGHT);
                }
            }
        });

        return avatar;
    }

    private Actor prepareSingleMugshot(UserAvatarDefinition avatarDefinition) {
        final Actor avatar = prepareAvatarPicture(avatarDefinition);
        avatar.setName(String.valueOf(avatarDefinition.number()));

        avatar.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                event.stop();
                mugshots.remove();
                Drawable d = new TextureRegionDrawable(getApplicationTextureRegion(avatarDefinition.getAvatarPath()));
                mugshotImage.setDrawable(d);
                userAvatarDefinition = avatarDefinition;
                validate();

                disposePreviewTexture();
                triggerScenarioStepEvent(SCENARIO_STEP_IMAGE_CHOSEN );
            }
        });

        return avatar;
    }

    private HorizontalGroup prepareMugshots() {
        HorizontalGroup hg = new HorizontalGroup();
        Actor mugshotAvatar;

        if(Tablexia.hasCameraOpener()) {
            mugshotAvatar = prepareCustomAvatarMugshot();
            hg.addActor(mugshotAvatar);
            if (hg.getHeight() < mugshotAvatar.getHeight()) hg.setHeight(mugshotAvatar.getHeight());
        }

        for(UserAvatarDefinition avatarDefinition : UserAvatarDefinition.getSelectableAvatarDefinitionsInOrder()) {
            mugshotAvatar = prepareSingleMugshot(avatarDefinition);
            hg.addActor(mugshotAvatar);

            if(hg.getHeight() < mugshotAvatar.getHeight()) hg.setHeight(mugshotAvatar.getHeight());
        }
        return hg;
    }

    private Actor prepareAvatarPicture(UserAvatarDefinition avatarDefinition) {
        TextureRegion textureRegion = getApplicationTextureRegion(avatarDefinition.getAvatarPath());
        float mugshotWidth = ScaleUtil.getWidth(textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), FormActorsLayout.MUGSHOT_HEIGHT);

        Image avatar = ScaleUtil.createImageToHeight(textureRegion, FormActorsLayout.MUGSHOT_HEIGHT);
        avatar.getDrawable().setMinHeight(FormActorsLayout.MUGSHOT_HEIGHT);
        avatar.getDrawable().setMinWidth(mugshotWidth);
        Image film = ScaleUtil.createImageToHeight(getScreenTextureRegion(FormScreenAssets.FILM_INSIDE), FormActorsLayout.MUGSHOT_HEIGHT);
        film.getDrawable().setMinHeight(FormActorsLayout.MUGSHOT_HEIGHT);
        film.getDrawable().setMinWidth(mugshotWidth);

        VerticalGroup vg = new VerticalGroup();
        Image frameTop = ScaleUtil.createImageToWidth(getScreenTextureRegion(FormScreenAssets.FILM_OUTERLINE), mugshotWidth);
        frameTop.getDrawable().setMinWidth(mugshotWidth);
        frameTop.getDrawable().setMinHeight(frameTop.getHeight());
        Image frameBottom = ScaleUtil.createImageToWidth(getScreenTextureRegion(FormScreenAssets.FILM_OUTERLINE), mugshotWidth);
        frameBottom.getDrawable().setMinWidth(mugshotWidth);
        frameBottom.getDrawable().setMinHeight(frameTop.getHeight());

        Stack stack = new Stack();
        stack.setSize(mugshotWidth, FormActorsLayout.MUGSHOT_HEIGHT);
        stack.addActor(avatar);
        stack.addActor(film);

        vg.addActor(frameTop);
        vg.addActor(stack);
        vg.addActor(frameBottom);

        vg.setHeight(FormActorsLayout.MUGSHOT_HEIGHT + frameBottom.getHeight() + frameTop.getHeight());
        vg.setWidth(mugshotWidth);

        return vg;
    }

    public void setAge(int age) {
        ageValue.setText(String.valueOf(this.age = age));
    }

    private void showSubscribeDialog() {
        ArrayList<TablexiaDialogComponentAdapter> adapters = new ArrayList<TablexiaDialogComponentAdapter>();

        final SignatureInputPane signatureInputPane = new SignatureInputPane();

        adapters.add(new AdaptiveSizeDialogComponent());
        adapters.add(new ViewportMaximumSizeComponent());
        adapters.add(new CenterPositionDialogComponent());
        adapters.add(new DimmerDialogComponent(0.7f));

        //Signature title and spacers...
        adapters.add(new FixedSpaceContentDialogComponent());
        adapters.add(new TextContentDialogComponent(getText(FormScreenAssets.TEXT_FORM_SIGNATURE), TEXT_26));
        adapters.add(new FixedSpaceContentDialogComponent());
        adapters.add(new DividerContentDialogComponent());
        adapters.add(new FixedSpaceContentDialogComponent());

        //Anonymous signature component
        adapters.add(new TablexiaDialogComponentAdapter() {
            //TODO 4 Desktop - Override sizeChanged?? Resize SignaturePane when resizing the window
            @Override
            public void prepareContent(Cell content) {
                content.getTable().row();
                content.setActor(signatureInputPane)
                        .expand()
                        .fill();
            }
        });

        //Separator between the signarute and buttons components
        adapters.add(new FixedSpaceContentDialogComponent());

        //Two button component (Cancel and Sign button)
        adapters.add(new TwoButtonContentDialogComponent(
                getText(FormScreenAssets.TEXT_FORM_CANCEL),
                getText(FormScreenAssets.TEXT_FORM_SIGN),
                StandardTablexiaButton.TablexiaButtonType.RED,
                StandardTablexiaButton.TablexiaButtonType.GREEN,
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        cancel();
                    }
                },
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        cancel();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Json json = new Json();
                                signature = json.toJson(signatureInputPane.getSignature());
                                signaturePane.setSignature(signatureInputPane.getSignature());
                                FormScreen.this.validate();
                                triggerEventFormSceneVisible();
                            }
                        }).start();
                    }
                }
        ));

        //Show dialog
        TablexiaComponentDialog dialog = TablexiaComponentDialogFactory.getInstance().createDialog(adapters.toArray(new TablexiaDialogComponentAdapter[]{}));
        dialog.setName(FORM_SIGNATURE_DIALOG);
        dialog.show(getSceneWidth(), getSceneInnerHeight());
        clearValidations();
    }

    protected boolean validationEnabled() {
        return true;
    }

    @Override
    protected void screenResized(int width, int height) {
        //TODO 4 Desktop - Resize SignaturePane when resizing the window

        if (signaturePane != null) {
            signaturePane.setPointOffsetY((defaultViewportHeight / 2 - getViewportHeight() / 2) * (getSceneInnerHeight() * SIGNATURE_Y_OFFSET_RATION / getViewportHeight()));
        }
    }

    private void setCustomMugshot(Pixmap pixmap) {
        choosingCustomAvatar = false;
        disposeAvatarPixmap();
        disposePreviewTexture();

        customAvatarPixmap = pixmap;
        customAvatarPreviewTextureRegion = new TextureRegion(new Texture(pixmap));

        mugshotImage.setDrawable(new TextureRegionDrawable(customAvatarPreviewTextureRegion));
        userAvatarDefinition = UserAvatarDefinition.CUSTOM_AVATAR;
        mugshots.remove();
        validate();
    }

    private void disposePreviewTexture() {
        if(customAvatarPreviewTextureRegion != null) {
            customAvatarPreviewTextureRegion.getTexture().dispose();
            customAvatarPreviewTextureRegion = null;
        }
    }

    private void disposeAvatarPixmap() {
        if(customAvatarPixmap != null) {
            customAvatarPixmap.dispose();
            customAvatarPixmap = null;
        }
    }

    private Drawable createCursorForTextField() {
        Pixmap pixmap = new Pixmap(1, TEXT_20.getSize(), Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.drawLine(0, 0, 0, TEXT_20.getSize());
        cursorTexture = new Texture(pixmap);
        pixmap.dispose();
        Image img = new Image(cursorTexture);
        return img.getDrawable();
    }

	private void displayHintDialog(final FormValidationEnum validation, Actor relatedActor) {
		displayHintDialog(validation, relatedActor, false);
	}

	private void displayHintDialog(final FormValidationEnum validation, Actor relatedActor, boolean biggerDialog) {
        ArrayList<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>();
        components.add(new AdaptiveSizeDialogComponent());
        components.add(new SpeechDialogComponent(
                new Point(relatedActor.getX() + relatedActor.getWidth()/2, relatedActor.getY() + relatedActor.getHeight()/2),
                new Vector2(16, 4),
                SpeechDialogComponent.ArrowType.BEND_ARROW));

        components.add(new FixedSpaceContentDialogComponent());
        components.add(new ResizableSpaceContentDialogComponent());

        components.add(new TextContentDialogComponent(getText(validation.getHintText()), true, true));

        components.add(new ResizableSpaceContentDialogComponent());
        components.add(new FixedSpaceContentDialogComponent());

		components.add(new TouchCloseDialogComponent() {
			@Override
			public void dialogTouched(float x, float y) {
				if (validation == FormValidationEnum.AGE) {
					setValid(validation, true);
					validate();
				}
			}
		});

        dialog = TablexiaComponentDialogFactory.getInstance().createDialog(
                getStage(),
                TablexiaComponentDialog.TablexiaDialogType.BUBBLE_SQUARE,
                components.toArray(new TablexiaDialogComponentAdapter[]{}));
        dialog.show(biggerDialog ? FormActorsLayout.HINT_DIALOG_BIGGER_WIDTH : FormActorsLayout.HINT_DIALOG_WIDTH, biggerDialog ? FormActorsLayout.HINT_DIALOG_BIGGER_HEIGHT : FormActorsLayout.HINT_DIALOG_HEIGHT);
    }

    private void playValidationSound(String sound) {
        if (alreadyPlayedHints.contains(sound)) {
            return;
        }

        final Music mu = getMusic(sound);
        mu.play();
        mu.setOnCompletionListener(new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music music) {
                mu.dispose();
            }
        });
        alreadyPlayedHints.add(sound);
    }

    @Override
    public void backButtonPressed() {
            if(mugshots != null) {
                mugshots.remove();
                mugshots = null;
            }
            else {
                timerDisabled = true;
                if(timer != null) timer.cancel();

                ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(TablexiaSettings.LOADER_SCREEN, TablexiaApplication.ScreenTransaction.FADE)).asynchronously();
            }
    }

	private enum FormValidationEnum {
		MUGSHOT(null, "validation_mugshot"),
		NAME(new ArrayList<FormValidationEnum>(Arrays.asList(MUGSHOT)), "validation_username"),
		AGE(new ArrayList<FormValidationEnum>(Arrays.asList(MUGSHOT, NAME)), "validation_age"),
		GENDER(new ArrayList<FormValidationEnum>(Arrays.asList(MUGSHOT, NAME, AGE)), "validation_gender"),
		SIGNATURE(new ArrayList<FormValidationEnum>(Arrays.asList(MUGSHOT, NAME, AGE, GENDER)), "validation_signature"),
		STAMP(new ArrayList<FormValidationEnum>(Arrays.asList(MUGSHOT, NAME, AGE, GENDER, SIGNATURE)), "validation_stamp");

		private List<FormValidationEnum> predecessors;
		private String hintText;

		FormValidationEnum(List<FormValidationEnum> predecessors, String text) {
			this.predecessors = predecessors;
			hintText = text;
		}

		public List<FormValidationEnum> getPredecessors() {
			return predecessors;
		}

		public String getHintText() {
			return hintText;
		}
	}

    @Override
    protected void screenDisposed() {
        disposeAvatarPixmap();

        if(textureManager != null) textureManager.dispose();
        if(cursorTexture != null) cursorTexture.dispose();

        super.screenDisposed();
    }
}