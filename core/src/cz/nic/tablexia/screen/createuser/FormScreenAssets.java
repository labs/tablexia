/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.createuser;

/**
 * Created by drahomir on 6/1/16.
 */
public class FormScreenAssets {
    public static final String GFX_PATH            = "gfx/";
    public static final String AVATAR_PATH         = GFX_PATH + "avatar/";

    public static final String BACKGROUND           = GFX_PATH + "background";
    public static final String INKPAD               = GFX_PATH + "inkpad";
    public static final String MUGSHOT_FRAME        = GFX_PATH + "mugshotframe";
    public static final String MUGSHOT_FRAME_GREEN  = GFX_PATH + "mugshotframe_green";
    public static final String TEXTFIELD_COVERNAME  = GFX_PATH + "textfield_covername";
    public static final String STAMP_PLACEHOLDER    = GFX_PATH + "stampplaceholder";
    public static final String TEXTFIELD_SIGNATURE  = GFX_PATH + "texfield_signature";
    public static final String INK                  = GFX_PATH + "ink";
    public static final String CLEAR                = GFX_PATH + "clear";
    public static final String SEX_FEMALE           = GFX_PATH + "sex_female";
    public static final String SEX_MALE             = GFX_PATH + "sex_male";
    public static final String ALTERNATIVE_TEXTFIELD= GFX_PATH + "alternative_textfield";
    public static final String YES_ICON             = GFX_PATH + "yes";
    public static final String NO_ICON              = GFX_PATH + "no";

    public static final String BUTTON_MINUS         = GFX_PATH + "button_minus";
    public static final String BUTTON_PLUS          = GFX_PATH + "button_plus";
    public static final String BUTTON_MINUS_PRESSED = GFX_PATH + "button_minus_pressed";
    public static final String BUTTON_PLUS_PRESSED  = GFX_PATH + "button_plus_pressed";
    public static final String BUTTON_SETTINGS      = GFX_PATH + "settings_button_unpressed";
    public static final String BUTTON_SETTINGS_PRESSED= GFX_PATH + "settings_button_pressed";

    public static final String SWITCH_BACKGROUND    = GFX_PATH + "switch_background";
    public static final String SWITCH_LEFT          = GFX_PATH + "switch_left";
    public static final String SWITCH_CENTER        = GFX_PATH + "switch_center";
    public static final String SWITCH_RIGHT         = GFX_PATH + "switch_right";

    public static final String PENCIL_DROPPED       = GFX_PATH + "pencil_dropped";
    public static final String STAMP_LEFT_DROPPED   = GFX_PATH + "stamp_left_dropped";

    public static final String STAMP                = GFX_PATH + "stamp";

    public static final String QR_MAGNIFIER_ICON    = GFX_PATH + "qr_magnifier_icon";

    public static final String FILM_OUTERLINE       = AVATAR_PATH + "film_outerline";
    public static final String FILM_INSIDE          = AVATAR_PATH + "film_inside";

    ///// TEXT ASSET KEYS
    public static final String TEXT_FORM_CANCEL     = "createuser_form_cancel";
    public static final String TEXT_FORM_SIGN       = "createuser_form_sign";
    public static final String TEXT_FORM_AGE        = "createuser_form_age";
    public static final String TEXT_FORM_GENDER     = "createuser_form_gender";
    public static final String TEXT_FORM_SIGNATURE  = "createuser_form_signature";
    public static final String TEXT_FORM_LEGAL_INFO = "createuser_form_legal_info";


    ///// MUSIC ASSETS
    public static final String MFX_PATH             = "mfx/";

    public static final String VALIDATION_MUGSHOT_SOUND   = MFX_PATH + "profil_foto.mp3";
    public static final String VALIDATION_USERNAME_SOUND  = MFX_PATH + "profil_jmeno.mp3";
    public static final String VALIDATION_SIGNATURE_SOUND = MFX_PATH + "profil_podpis_pero.mp3";
    public static final String VALIDATION_GENDER_SOUND    = MFX_PATH + "profil_klukholka.mp3";
    public static final String VALIDATION_AGE_SOUND       = MFX_PATH + "profil_vek.mp3";
    public static final String VALIDATION_STAMP_SOUND     = MFX_PATH + "profil_razitko.mp3";

}
