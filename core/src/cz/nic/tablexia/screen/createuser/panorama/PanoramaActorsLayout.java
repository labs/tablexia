/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.createuser.panorama;

/**
 * Created by frantisek on 6.1.16.
 */
public class PanoramaActorsLayout {

    public static final int NEWSPAPER_POINTER_ANIMATION_OFFSET      = 40;
    public static final int SPIN_ROTATION                           = 10 * 360;

    public static final float NEWSPAPER_X                           = 0.2f;
    public static final float NEWSPAPER_Y                           = 0.1f;
    public static final float NEWSPAPER_WIDTH                       = 0.6f;
    public static final float NEWSPAPER_HEIGHT                      = 0.3f;

    public static final float NEWSPAPER_POINTER_SPEED               = 0.5f;
    public static final float BALCONY_FADE_IN_DURATION              = 0.1f;
    public static final float SPIN_SCALE                            = 0.1f;
    public static final float SPIN_DURATION                         = 1.5f;
    public static final float SPIN_SCALE_TO_X                       = 2f;
    public static final float SPIN_SCALE_TO_Y                       = 2f;
    public static final float SPIN_SCALE_TO_DURATION                = 1.5f;
    public static final float SPIN_FADE_OUT_DURATION                = 0.5f;

    public static final float LEFT_FINGER_WIDTH                     = 0.2f;
    public static final float LEFT_FINGER_X                         = 0.2f;

    public static final float RIGHT_FINGER_WIDTH                    = 0.2f;
    public static final float RIGHT_FINGER_X                        = 0.8f;

    public static final float FINGER_FADE_IN                        = 0.5f;
    public static final float FINGER_FADE_OUT                       = 0.2f;
    public static final float FINGER_MOVE_DURATION                  = 0.5f;
    public static final float FINGER_SCALE_TO                       = 0.8f;
    public static final float FINGER_SCALE_DURATION                 = 0.2f;
    public static final float FINGER_ORIGINAL_SCALE_TO              = 1f;
    public static final float FINGER_ORIGINAL_SCALE_DURATION        = 0.1f;

    public static final float LEFT_FINGER_MOVE_TO                   = 0.4f;
    public static final float RIGHT_FINGER_MOVE_TO                  = 0.6f;

    public static final float HINT_WIDTH                            = 0.5f;
    
    public static final float NEWSPAPERS_TITLE_WIDTH                = 0.095f;

    public static final float NEWSPAPERS_OPEN_STAGE_1               = 0.0025f;
    public static final float NEWSPAPERS_OPEN_STAGE_2               = 0.05f;
    public static final float NEWSPAPERS_OPEN_STAGE_3               = 0.075f;
    public static final float NEWSPAPERS_OPEN_STAGE_4               = 0.1f;
    public static final float NEWSPAPERS_OPEN_STAGE_5               = 0.125f;

    public static final float NEWSPAPERS_STREET_TRANSITION_SPEED    = 0.5f;
    public static final float NEWSPAPERS_IMAGE_WIDTH                = 0.6f;
    public static final float NEWSPAPERS_IMAGE_HEIGHT               = 0.8f;
    public static final float NEWSPAPERS_CONTINUE_DIALOG_ARROW_X    = 0.75f;
    public static final float NEWSPAPERS_BUBBLE_DIALOG_ARROW_X      = 0.85f;
    public static final float NEWSPAPERS_DIALOG_ARROW_Y             = 0.75f;

    public static final int NEWSPAPERS_DIALOG_WIDTH                 = 300;
    public static final int NEWSPAPERS_CONTINUE_DIALOG_HEIGHT       = 150;
    public static final int NEWSPAPERS_BUBBLE_DIALOG_HEIGHT         = 90;

    public static final float OFFICE_DIALOG_X                       = 0.45f;
    public static final float OFFICE_DIALOG_Y                       = 0.6f;
    public static final float OFFICE_DIALOG_WIDTH                   = 0.3f;
    public static final float OFFICE_DIALOG_HEIGHT                  = 0.085f;

    public static final float STREET_KNOCK_IMAGE_WIDTH              = 0.2f;
   
    public static final float STREET_TILE2_TITLE_IMAGE_WIDTH        = 0.15f;
    public static final float STREET_TILE3_PLATE_TITLE_IMAGE_WIDTH  = 0.13f;
    public static final float STREET_TILE3_DOOR_SIGN_IMAGE_HEIGHT   = 0.8f;
    
    public static final float STREET_OFFICE_TRANSITION_DURATION     = 0.5f;
    public static final float OFFICE_FIRST_DIALOG_DELAY             = 0.25f;

    public static final float KNOCK_ACTION_DELAY                    = 0.25f;
    public static final float KNOCK_FADE_OUT                        = 0.2f;

    public static final float STREET_HINT_FINGER_WIDTH              = 0.2f;
    public static final float STREET_HINT_FINGER_X                  = 0.2f;
    public static final float STREET_HINT_MOVE_TO                   = 0.4f;
    public static final float STREET_HINT_END                       = 0.7f;
    public static final float STREET_HINT_DELAY                     = 3;

}
