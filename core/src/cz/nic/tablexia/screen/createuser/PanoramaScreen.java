/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.createuser;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaTextureManager;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.createuser.panorama.PanoramaActorsLayout;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.Utility;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;
import cz.nic.tablexia.util.ui.ScrollPaneWithBorders;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AdaptiveSizeDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.BackButtonHideComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DimmerDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.SpeechDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TouchCloseDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ViewportMaximumSizeComponent;

/**
 * Screen showing street animation and office before user create form
 */
public class PanoramaScreen extends AbstractTablexiaScreen<int[][]> {
    public static final String SCENARIO_STEP_NEWSPAPERS_SCENE_VISIBLE   = "newspapers screen visible";
    public static final String SCENARIO_STEP_STREET_SCENE_VISIBLE       = "street screen visible";

    public static final String NEWSPAPERS                               = "newspapers";
    public static final String NEWSPAPERS_CONTINUE_DIALOG               = "newspapers continue dialog";
    public static final String STREET_DOOR                              = "detective office's door";
    public static final String DETECTIVES_DIALOG                        = "detective's dialog";
    public static final String DETECTIVES_DIALOG_READY                  = "detective's dialog ready";

    private static Vector2 NEWSPAPER_TITLE_POSITION = new Vector2(0.26f, 0.16f);
    private static Vector2 SIGN_POSITION            = new Vector2(0.30f, 0.285f);
    private static Vector2 PLATE_TITLE_POSITION     = new Vector2(0.368f, 0.638f);
    private static Vector2 DOOR_SIGN_POSITION       = new Vector2(0.575f, 0f);

    public static final String  GFX_PATH                = "gfx/";
    public static final String  MFX_PATH                = "mfx/";
    private static final String SFX_PATH                = "sfx/";
    private static final String MP3_EXTENSION           = ".mp3";

    private static final String DETECTIVE_MUSIC         = MFX_PATH + "detective/";
    private static final String KNOCK_SOUND             = SFX_PATH + "tuk_";

    private static final String NEWSPAPER               = "newspaper/";
    private static final String ARROW_POINTER_TEXTURE   = GFX_PATH + NEWSPAPER + "arrow_pointer";
    private static final String SPINNER_TEXTURE         = GFX_PATH + NEWSPAPER + "spinner";
    private static final String NEWSPAPER_OPEN_TEXTURE  = GFX_PATH + NEWSPAPER + "open";
    private static final String NEWSPAPER_OPENED_TEXTURE= GFX_PATH + NEWSPAPER + "opened";
    private static final String LEFT_FINGER_TEXTURE     = GFX_PATH + NEWSPAPER + "left_finger";
    private static final String RIGHT_FINGER_TEXTURE    = GFX_PATH + NEWSPAPER + "right_finger";
    private static final String STRETCHHINT_TEXTURE     = GFX_PATH + NEWSPAPER + "stretchhint";

    private static final String SWIPE                   = GFX_PATH + "swipe/";
    private static final String TILE0_TEXTURE           = SWIPE + "tile0";
    private static final String TILE1_TEXTURE           = SWIPE + "tile1";
    private static final String TILE2_TEXTURE           = SWIPE + "tile2";
    private static final String TILE2_TITLE_TEXTURE     = SWIPE + "tile2_title";
    private static final String TILE3_TEXTURE           = SWIPE + "tile3";
    private static final String TILE3_DOOR_SIGN_TEXTURE = SWIPE + "tile3_door_sign";
    private static final String TILE3_PLATE_TITLE_TEXTURE=SWIPE + "tile3_plate_title";
    private static final String KNOCK_TEXTURE           = SWIPE + "knock";

    private static final String OFFICE_BACKGROUND_PATH  = GFX_PATH + "detectiveoffice";
    private static final String ARTICLE_TEXT_PATH       = "newuseranim_topic_";
    private static final String ARTICLE_DIALOG_TEXT     = "createuser_detective_";

    private static final String BALCONY             = GFX_PATH + "balcony";
    private static final String BALCONY_WINTER      = GFX_PATH + "balcony_winter";
    private static final String BALCONY_NEWS_TITLE  = GFX_PATH + "balcony_news_title";
    private static final String BALCONY_BLUR        = GFX_PATH + "balcony_blur";
    private static final String BALCONY_BLUR_WINTER = GFX_PATH + "balcony_blur_winter";

    private static final String SWIPE_OVERSCROLL_RIGHT      = SWIPE + "overscroll_right";
    private static final Color  SWIPE_OVERSCROLL_LEFT_COLOR = new Color(118/255f, 133/255f, 200/255f, 1);

    private static final String NEWSPAPER_DETAIL = GFX_PATH + NEWSPAPER + "detail0";

    private static final String SFX_NEWSPAPER_SPINNER = SFX_PATH + "noviny_prilet.mp3";
    private static final String SFX_NEWSPAPER_OPEN    = SFX_PATH + "noviny_rozbaleni.mp3";
    private static final String SFX_KNOCK_1           = SFX_PATH + "tuk_1.mp3";
    private static final String SFX_KNOCK_2           = SFX_PATH + "tuk_2.mp3";
    private static final String SFX_KNOCK_3           = SFX_PATH + "tuk_3.mp3";

    private static final String MUSIC_1 = MFX_PATH + NEWSPAPER + "1.mp3";
    private static final String MUSIC_2 = MFX_PATH + NEWSPAPER + "2.mp3";
    private static final String MUSIC_3 = MFX_PATH + NEWSPAPER + "3.mp3";
    private static final String MUSIC_4 = MFX_PATH + NEWSPAPER + "4.mp3";
    private static final String MUSIC_5 = MFX_PATH + NEWSPAPER + "5.mp3";

    private static final float    AMBIENT_SOUND_VOLUME = 0.5f;

    private static final String  AMBIENT_RESTAURANT  = MFX_PATH + "ambient/restaurant.mp3";
    private static final String  AMBIENT_STREET      = MFX_PATH + "ambient/swipe_street.mp3";

    private static final int     NUMBER_OF_NEWSPAPER_OPEN_FRAMES        = 5;
    private static final int     NUMBER_OF_OFFICE_KNOCKS                = 3;

    public  static final int     NUMBER_OF_OFFICE_DIALOGS               = 4;

    private static final float   OVERSCROLL_DISTANCE                    = 50;
    private static final float   OVERSCROLL_MIN_SPEED                   = 30;
    private static final float   OVERSCROLL_MAX_SPEED                   = 200;

    private static final Vector2 OFFICE_DIALOG_OFFSET                   = new Vector2(24, 12);
    private static final int     OFFICE_DIALOG_TEXT_PADDING_LEFT        = 4;
    private static final int     OFFICE_DIALOG_TEXT_PADDING_RIGHT       = 24;
	private static final int     NEWSPAPERS_SPEECH_DIALOG_OFFSET		= 4;

    private static final float   NEWSPAPERS_SPEECH_DIALOG_OFFSET_MULTI	= 0.6f;
    private static final float   NEWSPAPER_DETAIL_DIALOG_DIMMER         = 0.6f;
    private static final float   POINTER_Y_RELATIVE_POSITION            = 0.4f;
    private static final int     SPEECH_TEXT_PADDING                    = 4;

    private static final float   NEWS_DIALOG_MAX_WIDTH                  = 0.85f;
    private static final float   NEWS_DIALOG_MAX_HEIGHT                 = 0.85f;

    private static final int     TIMER_OPEN_NEWSPAPER                   = 5*1000;
    private static final int     TIMER_OPEN_ARTICLE                     = 5*1000;

    private Texture clickmap;

    private String clickmapName = prepareScreenAssetsPath(prepareScreenName())+ "excluded/clickmap.png";

    private int clickmapWidth;
    private int clickmapHeight;

    private TablexiaTextureManager textureManager;

    private Group panel;
    private ScrollPaneWithBorders scrollpane;
    private Music ambient;

    private Image leftFinger, rightFinger, hint;
    private Stack newspaperStages;
    private boolean newspapersOpened = false;

    private Image hintFinger;
    private float lastScroll = Float.NEGATIVE_INFINITY;
    private Action lastHintFingerAction;
    private TablexiaComponentDialog newsDetailDialog;
    private TablexiaComponentDialog newsSpeechDialog;
    private TablexiaComponentDialog detectivesDialog;

    private Timer openNewspaperTimer;
    private Timer openTablexiaArticleTimer;

    private int dialogTouchIndex = 0;
    @Override
    public boolean canBePaused() {
        return false;
    }

    @Override
    protected int[][] prepareScreenData(Map<String, String> screenState) {
        final Object loaderLock = new Object();
        synchronized (loaderLock) {

            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    synchronized (loaderLock) {
                        tryToDisposeTextureManager();
                        textureManager = new TablexiaTextureManager(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL);
                        textureManager.loadTexture(clickmapName);
                        textureManager.finishLoading();
                        loaderLock.notify();
                    }
                }
            });

            try {
                loaderLock.wait();
            } catch (InterruptedException e) {
                Log.err(getClass(), "Error while waiting to texture loader!", e);
            }
        }

        clickmap = textureManager.getTexture(clickmapName);
        if (!clickmap.getTextureData().isPrepared()) {
            clickmap.getTextureData().prepare();
        }
        clickmapWidth = clickmap.getWidth();
        clickmapHeight = clickmap.getHeight();
        return Utility.createColorMap(clickmap);
    }

    private void tryToDisposeTextureManager() {
        if (textureManager != null) {
            textureManager.dispose();
            textureManager = null;
        }
    }

    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        prepareBalcony();
    }

    @Override
	protected void screenVisible(Map<String, String> screenState) {
		AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_STEP_NEWSPAPERS_SCENE_VISIBLE);
	}

    @Override
    protected void prepareScreenSoundAssetNames(List<String> soundsFileNames) {
        soundsFileNames.add(SFX_NEWSPAPER_SPINNER);
        soundsFileNames.add(SFX_NEWSPAPER_OPEN);
        soundsFileNames.add(SFX_KNOCK_1);
        soundsFileNames.add(SFX_KNOCK_2);
        soundsFileNames.add(SFX_KNOCK_3);
    }

    public static Rectangle createNewspaperBounds(Stage stage){
        return new Rectangle(   stage.getWidth() * PanoramaActorsLayout.NEWSPAPER_X,
                                stage.getWidth() * PanoramaActorsLayout.NEWSPAPER_Y,
                                stage.getWidth() * PanoramaActorsLayout.NEWSPAPER_WIDTH,
                                stage.getWidth() * PanoramaActorsLayout.NEWSPAPER_HEIGHT);
    }

    /**
     * Create balcony view waiting for click
     * Shows winter image based on current time
     */
    private void prepareBalcony() {
        boolean isWinter = TablexiaSettings.getInstance().isWinterMode();
        ambient = getMusic(AMBIENT_RESTAURANT);
        ambient.setLooping(true);
        ambient.setVolume(AMBIENT_SOUND_VOLUME);
        ambient.play();

        final Image balcony = new TablexiaNoBlendingImage(getScreenTextureRegion(isWinter ? BALCONY_WINTER : BALCONY));
        setActorToFullScene(balcony);
        getStage().addActor(balcony);

        Image balconyNewsTitle = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(BALCONY_NEWS_TITLE),
                balcony.getWidth() * PanoramaActorsLayout.NEWSPAPERS_TITLE_WIDTH,
                NEWSPAPER_TITLE_POSITION.x * balcony.getWidth(),
                NEWSPAPER_TITLE_POSITION.y * balcony.getHeight());
        balconyNewsTitle.setTouchable(Touchable.disabled);
        getStage().addActor(balconyNewsTitle);

        final Image balconyBlur = new TablexiaNoBlendingImage(getScreenTextureRegion(isWinter ? BALCONY_BLUR_WINTER : BALCONY_BLUR));
        setActorToFullScene(balconyBlur);
        balconyBlur.setVisible(false);
        getStage().addActor(balconyBlur);

        final Rectangle newspaperBounds = createNewspaperBounds(getStage());

        final Image pointer = new Image(getScreenTextureRegion(ARROW_POINTER_TEXTURE));
        pointer.setPosition(newspaperBounds.getX() - pointer.getWidth() / 2 - PanoramaActorsLayout.NEWSPAPER_POINTER_ANIMATION_OFFSET, getSceneInnerHeight() * POINTER_Y_RELATIVE_POSITION);
        pointer.addAction(Actions.repeat(RepeatAction.FOREVER, Actions.sequence(Actions.moveBy(PanoramaActorsLayout.NEWSPAPER_POINTER_ANIMATION_OFFSET, 0, PanoramaActorsLayout.NEWSPAPER_POINTER_SPEED), Actions.moveBy(-PanoramaActorsLayout.NEWSPAPER_POINTER_ANIMATION_OFFSET, 0, PanoramaActorsLayout.NEWSPAPER_POINTER_SPEED))));
        pointer.setTouchable(Touchable.disabled);
        getStage().addActor(pointer);

        balcony.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (newspaperBounds.contains(x, y + balcony.getY())) {
                    ambient.dispose();
                    balconyBlur.addAction(Actions.fadeIn(PanoramaActorsLayout.BALCONY_FADE_IN_DURATION));
                    balconyBlur.setVisible(true);
                    pointer.remove();

                    final Sound spinSound = getSound(SFX_NEWSPAPER_SPINNER);
                    spinSound.play();

                    Image spin = new Image(getScreenTextureRegion(SPINNER_TEXTURE));
                    spin.setOrigin(spin.getWidth() / 2, spin.getHeight() / 2);
                    spin.setPosition(getStage().getWidth() / 2 - spin.getWidth() / 2, getSceneInnerHeight() / 2 - spin.getHeight() / 2);
                    spin.setScale(PanoramaActorsLayout.SPIN_SCALE);
                    spin.addAction(Actions.sequence(Actions.parallel(Actions.rotateBy(PanoramaActorsLayout.SPIN_ROTATION, PanoramaActorsLayout.SPIN_DURATION), Actions.scaleTo(PanoramaActorsLayout.SPIN_SCALE_TO_X, PanoramaActorsLayout.SPIN_SCALE_TO_Y, PanoramaActorsLayout.SPIN_SCALE_TO_DURATION)), Actions.parallel(Actions.sequence(Actions.fadeOut(PanoramaActorsLayout.SPIN_FADE_OUT_DURATION), Actions.hide()), Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            prepareOpenAction();
                            spinSound.stop();
                            openNewspaperTimer = new Timer();
                            openNewspaperTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    hideHintsAndOpenNewspaper();
                                }
                            }, TIMER_OPEN_NEWSPAPER);

                            if(TablexiaSettings.getInstance().getPlatform() == TablexiaSettings.Platform.DESKTOP) {
                                hideHintsAndOpenNewspaper();
                                openNewspaperTimer.cancel();
                            }
                        }
                    }))));
                    getStage().addActor(spin);
                }
            }
        });
        balcony.setName(NEWSPAPERS);
    }

    /**
     * Prepare closed newspaper with hint to open
     */
    private void prepareOpenAction() {
        newspaperStages = new Stack();
        ScaleUtil.setFullInnerScene(newspaperStages, getStage());
        newspaperStages.setY(getViewportBottomY());

        final Image[] stages = new Image[NUMBER_OF_NEWSPAPER_OPEN_FRAMES];
        for (int i = 1; i <= NUMBER_OF_NEWSPAPER_OPEN_FRAMES; i++) {
            Image stage = new Image(getScreenTextureRegion(NEWSPAPER_OPEN_TEXTURE + i));
            newspaperStages.addActor(stage);
            stages[i - 1] = stage;
            stage.setVisible(false);
        }
        stages[0].setVisible(true);
        getStage().addActor(newspaperStages);

        leftFinger = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(LEFT_FINGER_TEXTURE), getStage().getWidth() * PanoramaActorsLayout.LEFT_FINGER_WIDTH, getStage().getWidth() * PanoramaActorsLayout.LEFT_FINGER_X - getStage().getWidth() * PanoramaActorsLayout.LEFT_FINGER_WIDTH / 2, getViewportBottomY());
        rightFinger = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(RIGHT_FINGER_TEXTURE), getStage().getWidth() * PanoramaActorsLayout.RIGHT_FINGER_WIDTH, getStage().getWidth() * PanoramaActorsLayout.RIGHT_FINGER_X - getStage().getWidth() * PanoramaActorsLayout.RIGHT_FINGER_WIDTH / 2, getViewportBottomY());
        hint = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(STRETCHHINT_TEXTURE), getStage().getWidth() * PanoramaActorsLayout.HINT_WIDTH, getStage().getWidth() / 2 - getStage().getWidth() * PanoramaActorsLayout.HINT_WIDTH / 2, getViewportBottomY() + leftFinger.getHeight() / 2);

        prepareFingersSweepActions();

        getStage().addActor(hint);
        getStage().addActor(leftFinger);
        getStage().addActor(rightFinger);

        getStage().addListener(new InputListener() {
            private boolean dragging, mouse;
            private Sound sound;
            private float x1start, x2start, x1, x2;

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                super.touchDown(event, x, y, pointer, button);
                // desktop mouse button workaround
                mouse = (button == Input.Buttons.RIGHT);

                if (pointer == 0) {
                    x1start = x1 = x;
                }
                if (pointer == 1) {
                    x2start = x2 = x;
                    dragging = true;
                }
                return true;
            }

            private void showStage(int i) {
                for (Image image : stages) {
                    image.setVisible(false);
                }
                stages[i].setVisible(true);
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                super.touchDragged(event, x, y, pointer);
                if (pointer == 0) {
                    x1 = x;
                } else if (pointer == 1) {
                    x2 = x;
                }
                if (!newspapersOpened) {
                    if (getDiff() > getStage().getWidth() * PanoramaActorsLayout.NEWSPAPERS_OPEN_STAGE_5) {
                            hideHintsAndOpenNewspaper();
                            sound = null;
                            newspapersOpened = true;
                    } else if (getDiff() > getStage().getWidth() * PanoramaActorsLayout.NEWSPAPERS_OPEN_STAGE_4) {
                        showStage(4);
                    } else if (getDiff() > getStage().getWidth() * PanoramaActorsLayout.NEWSPAPERS_OPEN_STAGE_3) {
                        showStage(3);
                    } else if (getDiff() > getStage().getWidth() * PanoramaActorsLayout.NEWSPAPERS_OPEN_STAGE_2) {
                        showStage(2);
                    } else if (getDiff() > getStage().getWidth() * PanoramaActorsLayout.NEWSPAPERS_OPEN_STAGE_1) {
                        showStage(1);
                        if (sound == null) {
                            sound = getSound(SFX_NEWSPAPER_OPEN);
                            sound.play();
                        }
                    } else {
                        stopSound();
                        showStage(0);
                    }
                }
            }

            private void stopSound() {
                if (sound != null) {
                    sound.stop();
                    sound = null;
                }
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                showStage(0);
                stopSound();
                if (pointer == 1) {
                    dragging = false;
                }
            }

            private float getDiff() {
                if (mouse) {
                    return Math.abs(x1start - x1);
                } else if (dragging) {
                    return Math.abs(Math.abs(x1 - x2) - Math.abs(x1start - x2start));
                }
                return 0;
            }

        });
    }

    private void hideHintsAndOpenNewspaper() {
        if (hint != null) hint.remove();
        if (leftFinger != null) leftFinger.remove();
        if (rightFinger != null) rightFinger.remove();
        if (newspaperStages != null) newspaperStages.remove();
        hint = null;
        leftFinger = null;
        rightFinger = null;
        newspaperStages = null;
        prepareOpenedNewspaper();
    }

    private void prepareFingersSweepActions() {
        leftFinger.clearActions();
        rightFinger.clearActions();

        leftFinger.addAction(Actions.sequence(
                Actions.fadeIn(PanoramaActorsLayout.FINGER_FADE_IN)
                , Actions.forever(Actions.sequence(
                        Actions.moveTo(getStage().getWidth() * PanoramaActorsLayout.LEFT_FINGER_MOVE_TO - leftFinger.getWidth() / 2, leftFinger.getY(), PanoramaActorsLayout.FINGER_MOVE_DURATION)
                        , Actions.scaleTo(PanoramaActorsLayout.FINGER_SCALE_TO, PanoramaActorsLayout.FINGER_SCALE_TO, PanoramaActorsLayout.FINGER_SCALE_DURATION)
                        , Actions.moveTo(-leftFinger.getWidth(), leftFinger.getY(), PanoramaActorsLayout.FINGER_MOVE_DURATION)
                        , Actions.scaleTo(PanoramaActorsLayout.FINGER_ORIGINAL_SCALE_TO, PanoramaActorsLayout.FINGER_ORIGINAL_SCALE_TO, PanoramaActorsLayout.FINGER_ORIGINAL_SCALE_DURATION)))));

        rightFinger.addAction(Actions.sequence(
                Actions.fadeIn(PanoramaActorsLayout.FINGER_FADE_IN)
                , Actions.forever(Actions.sequence(
                        Actions.moveTo(getStage().getWidth() * PanoramaActorsLayout.RIGHT_FINGER_MOVE_TO - rightFinger.getWidth() / 2, rightFinger.getY(), PanoramaActorsLayout.FINGER_MOVE_DURATION)
                        , Actions.scaleTo(PanoramaActorsLayout.FINGER_SCALE_TO, PanoramaActorsLayout.FINGER_SCALE_TO, PanoramaActorsLayout.FINGER_SCALE_DURATION)
                        , Actions.moveTo(getStage().getWidth() + rightFinger.getWidth(), rightFinger.getY(), PanoramaActorsLayout.FINGER_MOVE_DURATION)
                        , Actions.scaleTo(PanoramaActorsLayout.FINGER_ORIGINAL_SCALE_TO, PanoramaActorsLayout.FINGER_ORIGINAL_SCALE_TO, PanoramaActorsLayout.FINGER_ORIGINAL_SCALE_DURATION)))));
    }


    public enum ArticleImage {

        //        id       dialog           CZ width height x   y   SK width height x   y      DE width height x   y
        ARTICLE1 (1, Article.BEARD_ARTICLE,     355, 210, 130, 240,     355, 210, 140, 240,         345, 220, 505, 235),
        ARTICLE2 (2, Article.BALOON_ARTICLE,    320, 250, 525, 190,     320, 250, 525, 190,         221, 190, 265, 28),
        ARTICLE3 (3, Article.MOTOBIKE_ARTICLE,  307, 130, 525, 35,      307, 130, 535, 35,          357, 185, 505, 25),
        ARTICLE4 (4, Article.SWIMSUIT_ARTICLE,  223, 190, 255, 40,      223, 190, 265, 40,          349, 210, 130, 240),
        ARTICLE5 (5, Article.DETECTIVE_ARTICLE, 115, 200, 130, 30,      115, 200, 130, 30,          117, 200, 130, 25);

        private final int id;
        private Article articleDialog;
        private final float widthCZ;
        private final float heightCZ;
        private final float xCZ;
        private final float yCZ;
        private final float widthSK;
        private final float heightSK;
        private final float xSK;
        private final float ySK;
        private final float widthDE;
        private final float heightDE;
        private final float xDE;
        private final float yDE;

        ArticleImage(int id, Article articleDialog, float widthCZ, float heightCZ, float xCZ, float yCZ, float widthSK, float heightSK, float xSK, float ySK, float widthDE, float heightDE, float xDE, float yDE){
            this.id                 = id;
            this.articleDialog      = articleDialog;
            this.widthCZ            = widthCZ;
            this.heightCZ           = heightCZ;
            this.xCZ                = xCZ;
            this.yCZ                = yCZ;
            this.widthSK            = widthSK;
            this.heightSK           = heightSK;
            this.xSK                = xSK;
            this.ySK                = ySK;
            this.widthDE            = widthDE;
            this.heightDE           = heightDE;
            this.xDE                = xDE;
            this.yDE                = yDE;
        }

        public int getId() {
            return id;
        }

        public Article getArticleDialog() {
            return articleDialog;
        }

        public float getWidth() {
            String locale = TablexiaSettings.getInstance().getLocale().toString();
            if(locale.equals(TablexiaSettings.LocaleDefinition.sk_SK.getLocale().toString())) return widthSK;
            else if(locale.equals(TablexiaSettings.LocaleDefinition.de_DE.getLocale().toString())) return widthDE;
            else return widthCZ;
        }

        public float getHeight() {
            String locale = TablexiaSettings.getInstance().getLocale().toString();
            if(locale.equals(TablexiaSettings.LocaleDefinition.sk_SK.getLocale().toString())) return heightSK;
            else if(locale.equals(TablexiaSettings.LocaleDefinition.de_DE.getLocale().toString())) return heightDE;
            else return heightCZ;
        }

        public float getX() {
            String locale = TablexiaSettings.getInstance().getLocale().toString();
            if(locale.equals(TablexiaSettings.LocaleDefinition.sk_SK.getLocale().toString())) return xSK;
            else if(locale.equals(TablexiaSettings.LocaleDefinition.de_DE.getLocale().toString())) return xDE;
            else return xCZ;
        }

        public float getY() {
            String locale = TablexiaSettings.getInstance().getLocale().toString();
            if(locale.equals(TablexiaSettings.LocaleDefinition.sk_SK.getLocale().toString())) return ySK;
            else if(locale.equals(TablexiaSettings.LocaleDefinition.de_DE.getLocale().toString())) return yDE;
            else return yCZ;
        }

    }

    /**
     * Prepare opened newspaper with topic details and playback of comments
     */
    private void prepareOpenedNewspaper() {
        openNewspaperTimer.cancel();
        Image opened = new Image(getScreenTextureRegion(NEWSPAPER_OPENED_TEXTURE));
        Image article1 = createArticleImage(ArticleImage.ARTICLE1);
        Image article2 = createArticleImage(ArticleImage.ARTICLE2);
        Image article3 = createArticleImage(ArticleImage.ARTICLE3);
        Image article4 = createArticleImage(ArticleImage.ARTICLE4);
        Image article5 = createArticleImage(ArticleImage.ARTICLE5);

        ScaleUtil.setFullInnerScene(opened, getStage());
        getStage().addActor(opened);
        getStage().addActor(article1);
        getStage().addActor(article2);
        getStage().addActor(article3);
        getStage().addActor(article4);
        getStage().addActor(article5);

        openTablexiaArticleTimer = new Timer();
        openTablexiaArticleTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                showNewsDetailDialog(Article.DETECTIVE_ARTICLE);
                playMusic(Article.DETECTIVE_ARTICLE.musicPath);
            }
        }, TIMER_OPEN_ARTICLE);
    }

    private Image createArticleImage(ArticleImage article) {
        Image articleImage = new Image(getScreenTextureRegion(NEWSPAPER_DETAIL + article.getId()));
        articleImage.setSize(article.getWidth(), article.getHeight());
        articleImage.setPosition(article.getX(), article.getY());
        articleImage.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);

                if (newsDetailDialog != null && newsDetailDialog.isVisible())
                    newsDetailDialog.hide();

                showNewsDetailDialog(article.getArticleDialog());
                playMusic(article.getArticleDialog().musicPath);
            }
        });
        return articleImage;
    }

    private Color getTouchedColor(float x, float y) {
        int clickX = (int) (x / TablexiaSettings.getSceneWidth(getStage()) * clickmapWidth);
        int clickY = clickmapHeight - (int) (y / TablexiaSettings.getSceneInnerHeight(getStage()) * clickmapHeight);

        if (clickX < 0 || clickY < 0 || clickX >= getData().length || clickY >= getData()[0].length)
            return Color.WHITE;
        else
            return new Color(getData()[clickX][clickY]);
    }

    private void showNewsDetailDialog(Article article) {
        openTablexiaArticleTimer.cancel();
        final ArrayList<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>();
        components.add(new AdaptiveSizeDialogComponent());
        components.add(new ViewportMaximumSizeComponent());
        components.add(new DimmerDialogComponent(NEWSPAPER_DETAIL_DIALOG_DIMMER));
        components.add(new TouchCloseDialogComponent(new TouchCloseDialogComponent.TouchListener() {
            @Override
            public boolean dialogTouched(float x, float y, TablexiaComponentDialog dialog) {
                if (article.index == Article.DETECTIVE_ARTICLE.index) {
					moveToStreet();
                }
                return true;
            }
        }));

        components.add(new FixedSpaceContentDialogComponent());
        components.add(new ImageContentDialogComponent(new Image(getScreenTextureRegion(NEWSPAPER_DETAIL + (article.index + 1))), Scaling.fill, 1f, 1f));
        components.add(new FixedSpaceContentDialogComponent());
        components.add(new CenterPositionDialogComponent());
        components.add(new BackButtonHideComponent());

        this.newsDetailDialog = TablexiaComponentDialogFactory.getInstance().createDialog(
                TablexiaComponentDialog.TablexiaDialogType.DIALOG_RECTANGLE,
                components.toArray(new TablexiaDialogComponentAdapter[]{})
        );

		final int origNewsDetailWidth  = getScreenTextureRegion(NEWSPAPER_DETAIL + (article.index + 1)).getRegionWidth();
		final int origNewsDetailHeight = getScreenTextureRegion(NEWSPAPER_DETAIL + (article.index + 1)).getRegionHeight();

        this.newsDetailDialog.setComponentsSizeComputedCallback(new Runnable() {
			@Override
			public void run() {
				if (newsDetailDialog.getOutterHeight() > getSceneInnerHeight() * NEWS_DIALOG_MAX_HEIGHT) {
					int currNewsDetailHeight;
					int currNewsDetailWidth;

					currNewsDetailHeight = (int) (getSceneInnerHeight() * NEWS_DIALOG_MAX_HEIGHT);
					currNewsDetailWidth = (int) (currNewsDetailHeight * ((float)(origNewsDetailWidth) / origNewsDetailHeight));

					if(currNewsDetailWidth > getSceneWidth() * NEWS_DIALOG_MAX_WIDTH) {
						currNewsDetailWidth = (int) (getSceneWidth() * NEWS_DIALOG_MAX_WIDTH);
						currNewsDetailHeight = (int) (currNewsDetailWidth * ((float)(origNewsDetailHeight) / origNewsDetailWidth));
					}
					newsDetailDialog.setSize(currNewsDetailWidth, currNewsDetailHeight);
				}
                showTextDialog(getText(ARTICLE_TEXT_PATH + (article.index + 1)), article.index == Article.DETECTIVE_ARTICLE.index, article.index == Article.DETECTIVE_ARTICLE.index);
            }
		});

        this.newsDetailDialog.show(
                origNewsDetailWidth,
                origNewsDetailHeight
        );
    }

	private void moveToStreet() {
        if (newsDetailDialog != null && newsDetailDialog.isVisible()) newsDetailDialog.hide();

        for (Actor a : getStage().getActors()) {
			a.setTouchable(Touchable.disabled);
			a.addAction(Actions.sequence(Actions.moveBy(-getStage().getWidth(), 0, PanoramaActorsLayout.NEWSPAPERS_STREET_TRANSITION_SPEED), Actions.removeActor()));
		}
		Actor street = prepareSwipeStreet();
		getStage().addActor(street);
		street.setX(getStage().getWidth());
		street.addAction(Actions.sequence(Actions.moveBy(-getStage().getWidth(), 0, PanoramaActorsLayout.NEWSPAPERS_STREET_TRANSITION_SPEED),
                Actions.run(new Runnable(){
                    @Override
                    public void run() {
                        AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_STEP_STREET_SCENE_VISIBLE);
                    }
                })));
	}

	private void showTextDialog(String text, boolean hasButton, final boolean continueOnTouch) {
		ArrayList<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>();

		components.add(new TouchCloseDialogComponent(new TouchCloseDialogComponent.TouchListener() {
            @Override
            public boolean dialogTouched(float x, float y, TablexiaComponentDialog dialog) {
                if (    continueOnTouch &&
                        x > newsSpeechDialog.getOutterLeftX() &&
                        x < newsSpeechDialog.getOutterRightX() &&
                        y > newsSpeechDialog.getOutterBottomY() &&
                        y < newsSpeechDialog.getOutterTopY())
                {
                    moveToStreet();
                }
                return true;
            }
        }));
		components.add(new AdaptiveSizeDialogComponent());
		components.add(
			new SpeechDialogComponent(
				new Point(
					//4 is just a little offset to make sure that dialog appears to the top and right...
					newsDetailDialog.getInnerLeftX() + newsDetailDialog.getInnerWidth() / 2 + NEWSPAPERS_SPEECH_DIALOG_OFFSET,
					newsDetailDialog.getInnerHeight() / 2 + NEWSPAPERS_SPEECH_DIALOG_OFFSET
				),
				new Vector2(newsDetailDialog.getInnerWidth() / 2f * NEWSPAPERS_SPEECH_DIALOG_OFFSET_MULTI, newsDetailDialog.getInnerHeight() / 2f * NEWSPAPERS_SPEECH_DIALOG_OFFSET_MULTI),
				SpeechDialogComponent.ArrowType.BEND_ARROW
			)
		);

        if(hasButton) {
            TablexiaDialogComponentAdapter buttonComponent = new TablexiaDialogComponentAdapter() {
                private static final float ADAPTIVE_SCALE = 1.7f;

                TextureRegion buttonTexture = ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.DIALOG_CLASSIC_BUTTON_SHADOW);
                float currentWidth = buttonTexture.getRegionWidth();
                float currentHeight = buttonTexture.getRegionHeight();

                @Override
                public void drawAdditions(Batch batch, float parentAlpha) {
                    batch.draw(
                            buttonTexture,
                            getDialog().getInnerRightX() - currentWidth / 2,
                            getDialog().getInnerBottomY() + getDialog().getInnerHeight() - currentHeight,
                            currentWidth,
                            currentHeight
                    );
                }

                @Override
                public void adaptiveSizeThresholdChanged() {
                    currentWidth = buttonTexture.getRegionWidth();
                    currentHeight = buttonTexture.getRegionHeight();

                    if (ComponentScaleUtil.isUnderThreshold()) {
                        currentWidth *= ADAPTIVE_SCALE;
                        currentHeight *= ADAPTIVE_SCALE;
                    }
                }
            };
            components.add(buttonComponent);
        }

		components.add(new FixedSpaceContentDialogComponent());
		components.add(new ResizableSpaceContentDialogComponent());

		TextContentDialogComponent textComponent = new TextContentDialogComponent(text, true, true);
        textComponent.setPadding(SPEECH_TEXT_PADDING, SPEECH_TEXT_PADDING);
		components.add(textComponent);

		components.add(new ResizableSpaceContentDialogComponent());
		components.add(new FixedSpaceContentDialogComponent());
        components.add(new BackButtonHideComponent());

		newsSpeechDialog = TablexiaComponentDialogFactory.getInstance().createDialog(
			TablexiaComponentDialog.TablexiaDialogType.BUBBLE_SQUARE,
			components.toArray(new TablexiaDialogComponentAdapter[]{})
        );
        newsSpeechDialog.setName(NEWSPAPERS_CONTINUE_DIALOG);
		newsSpeechDialog.show(PanoramaActorsLayout.NEWSPAPERS_DIALOG_WIDTH, PanoramaActorsLayout.NEWSPAPERS_BUBBLE_DIALOG_HEIGHT);
	}

    private Music playingMusic;

    private void playMusic(String key) {
        stopMusic();
        playingMusic = getMusic(key);
        playingMusic.play();
    }

    private void stopMusic() {
        if (playingMusic != null && playingMusic.isPlaying()) {
            playingMusic.stop();
            playingMusic.dispose();
        }
    }

    /**
     * Prepare street view with swipe and door knock
     */
    private Actor prepareSwipeStreet() {
        ambient = getMusic(AMBIENT_STREET);
        ambient.setVolume(AMBIENT_SOUND_VOLUME);
        ambient.setLooping(true);
        ambient.play();

        panel = new Group();

        ScaleUtil.setFullScreen(panel, getStage());
        panel.setY(getViewportBottomY());
        panel.addActor(prepareStreetGroup());
        panel.setCullingArea(new Rectangle(panel.getX(), panel.getY(), panel.getWidth(), panel.getHeight()));
        prepareSwipeStreetFingerHint();

        return panel;
    }

    /**
     * Prepare detective office with dialogs and playing voice
     */
    private Actor prepareOffice() {
        ambient.dispose();
        Group group = new Group();
        Image background = new TablexiaNoBlendingImage(getScreenTextureRegion(OFFICE_BACKGROUND_PATH));
        setActorToFullScene(background);
        group.addActor(background);
        ScaleUtil.setFullScene(group, getStage());
        return group;
    }

    private void showDialog(final int num) {
        String detectiveText = getText(ARTICLE_DIALOG_TEXT + num);
        dialogTouchIndex = num;
        ArrayList<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>();

        components.add(new TouchCloseDialogComponent(new TouchCloseDialogComponent.TouchListener() {
            @Override
            public boolean dialogTouched(float x, float y, TablexiaComponentDialog dialog) {
                if (dialogTouchIndex != num)
                    return false;

                stopMusic();

                if (num == NUMBER_OF_OFFICE_DIALOGS) {
                    ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(FormScreen.class, TablexiaApplication.ScreenTransaction.FADE));
                } else {
                    showDialog(num + 1);
                }
                return true;
            }
        }));
        components.add(new AdaptiveSizeDialogComponent());
        components.add(new SpeechDialogComponent(
                new Point(getViewportWidth() * PanoramaActorsLayout.OFFICE_DIALOG_X, getViewportHeight() * PanoramaActorsLayout.OFFICE_DIALOG_Y),
                new Vector2(OFFICE_DIALOG_OFFSET),
                SpeechDialogComponent.ArrowType.CLASSIC_SHADOW, true));

        TablexiaDialogComponentAdapter tdca = new TablexiaDialogComponentAdapter() {
            private static final float ADAPTIVE_SCALE = 1.7f;

            TextureRegion buttonTexture = ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.DIALOG_CLASSIC_BUTTON_SHADOW);
            float currentWidth = buttonTexture.getRegionWidth();
            float currentHeight = buttonTexture.getRegionHeight();

            @Override
            public void drawAdditions(Batch batch, float parentAlpha) {
                batch.draw(
                    buttonTexture,
                    getDialog().getInnerRightX() - currentWidth/2,
                    getDialog().getInnerBottomY() + getDialog().getInnerHeight() - currentHeight,
                    currentWidth,
                    currentHeight
                );
            }

            @Override
            public void adaptiveSizeThresholdChanged() {
                currentWidth = buttonTexture.getRegionWidth();
                currentHeight = buttonTexture.getRegionHeight();

                if(ComponentScaleUtil.isUnderThreshold()) {
                    currentWidth  *= ADAPTIVE_SCALE;
                    currentHeight *= ADAPTIVE_SCALE;
                }
            }
        };
        components.add(tdca);

        components.add(new FixedSpaceContentDialogComponent());
        components.add(new ResizableSpaceContentDialogComponent());

        TextContentDialogComponent textComponent = new TextContentDialogComponent(detectiveText, true, true);
        textComponent.setPadding(OFFICE_DIALOG_TEXT_PADDING_LEFT, OFFICE_DIALOG_TEXT_PADDING_RIGHT);
        components.add(textComponent);

        components.add(new ResizableSpaceContentDialogComponent());
        components.add(new FixedSpaceContentDialogComponent());

        detectivesDialog = TablexiaComponentDialogFactory.getInstance().createDialog(
                getStage(),
                TablexiaComponentDialog.TablexiaDialogType.DIALOG_SQUARE_SHADOW,
                components.toArray(new TablexiaDialogComponentAdapter[]{}));

        detectivesDialog.setComponentsSizeComputedCallback(new Runnable() {
            @Override
            public void run() {
                AbstractTablexiaScreen.triggerScenarioStepEvent(DETECTIVES_DIALOG_READY + num);
            }
        });

        detectivesDialog.setName(DETECTIVES_DIALOG + num);

        detectivesDialog.show(getSceneWidth() * PanoramaActorsLayout.OFFICE_DIALOG_WIDTH, getSceneWidth() * PanoramaActorsLayout.OFFICE_DIALOG_HEIGHT, false);
        playMusic(DETECTIVE_MUSIC + num + MP3_EXTENSION);
    }

    private Group prepareStreetGroup() {
        Table group = new Table();
        group.setBounds(getSceneLeftX(), getSceneOuterBottomY(), getSceneWidth(), getSceneOuterHeight());

        TablexiaNoBlendingImage tile00 = new TablexiaNoBlendingImage(getScreenTextureRegion(TILE0_TEXTURE));
        TablexiaNoBlendingImage tile01 = new TablexiaNoBlendingImage(getScreenTextureRegion(TILE1_TEXTURE));

        tile00.setSize((int) (panel.getHeight() * (tile00.getWidth() / tile00.getHeight())), panel.getHeight());
        tile01.setSize((int) (panel.getHeight() * (tile01.getWidth() / tile01.getHeight())), panel.getHeight());

        group.add(tile00).height(tile00.getHeight()).width(tile00.getWidth());
        group.add(tile01).height(tile01.getHeight()).width(tile01.getWidth());
        //tile2
        Group tile2 = new Group();
        TablexiaNoBlendingImage tile02 = new TablexiaNoBlendingImage(getScreenTextureRegion(TILE2_TEXTURE));
        tile02.setSize((int) (panel.getHeight() * (tile02.getWidth() / tile02.getHeight())), panel.getHeight());
        tile2.addActor(tile02);
        Image tile02_title = ScaleUtil.createImageWidthPosition(
                getScreenTextureRegion(TILE2_TITLE_TEXTURE),
                tile02.getWidth() * PanoramaActorsLayout.STREET_TILE2_TITLE_IMAGE_WIDTH,
                SIGN_POSITION.x*tile02.getWidth(),
                SIGN_POSITION.y*tile02.getHeight());

        tile2.addActor(tile02_title);
        group.add(tile2).width(tile02.getWidth()).height(tile02.getHeight());
        //tile3
        final TablexiaNoBlendingImage tile3Background = new TablexiaNoBlendingImage(getScreenTextureRegion(TILE3_TEXTURE));
        tile3Background.setSize((int) (panel.getHeight() * (tile3Background.getWidth() / tile3Background.getHeight())), panel.getHeight());
        final Image tile3DoorSign = ScaleUtil.createImageHeightPosition(getScreenTextureRegion(TILE3_DOOR_SIGN_TEXTURE),
                tile3Background.getHeight() * PanoramaActorsLayout.STREET_TILE3_DOOR_SIGN_IMAGE_HEIGHT,
                DOOR_SIGN_POSITION.x * tile3Background.getWidth(),
                DOOR_SIGN_POSITION.y * tile3Background.getHeight());
        Image tile3PlateTitle = ScaleUtil.createImageWidthPosition(
                getScreenTextureRegion(TILE3_PLATE_TITLE_TEXTURE),
                tile3Background.getWidth() * PanoramaActorsLayout.STREET_TILE3_PLATE_TITLE_IMAGE_WIDTH,
                PLATE_TITLE_POSITION.x * tile3Background.getWidth(),
                PLATE_TITLE_POSITION.y * tile3Background.getHeight());
        //Detective door group
        final Group detectiveDoor = new Group();

        tile3DoorSign.addListener(new ClickListener() {
            private int knockCount = 0;

            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (knockCount < NUMBER_OF_OFFICE_KNOCKS) {
                    knockCount++;
                    Image knock = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(KNOCK_TEXTURE), getStage().getWidth() * PanoramaActorsLayout.STREET_KNOCK_IMAGE_WIDTH, tile3DoorSign.getX() + x, tile3DoorSign.getY() + y);
                    detectiveDoor.addActor(knock);

                    RunnableAction moveToOffice = new RunnableAction();
                    moveToOffice.setRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Actor office = prepareOffice();
                            office.setPosition(getStage().getWidth(), 0);
                            getStage().addActor(office);

                            panel.addAction(Actions.sequence(Actions.moveBy(-getStage().getWidth(), 0, PanoramaActorsLayout.STREET_OFFICE_TRANSITION_DURATION), Actions.removeActor()));
                            final RunnableAction showDialog = new RunnableAction();
                            showDialog.setRunnable(new Runnable() {
                                @Override
                                public void run() {
                                    showDialog(1);
                                }
                            });
                            office.addAction(Actions.sequence(Actions.moveTo(0, 0, PanoramaActorsLayout.STREET_OFFICE_TRANSITION_DURATION), Actions.delay(PanoramaActorsLayout.OFFICE_FIRST_DIALOG_DELAY), showDialog));
                        }
                    });

                    knock.setTouchable(Touchable.disabled);
                    getSound(KNOCK_SOUND + knockCount + MP3_EXTENSION).play();
                    SequenceAction knockActions;
                    if (knockCount != NUMBER_OF_OFFICE_KNOCKS) {
                        knockActions = Actions.sequence(Actions.delay(PanoramaActorsLayout.KNOCK_ACTION_DELAY), Actions.fadeOut(PanoramaActorsLayout.KNOCK_FADE_OUT), Actions.removeActor());
                    } else {
                        knockActions = Actions.sequence(Actions.delay(PanoramaActorsLayout.KNOCK_ACTION_DELAY), Actions.fadeOut(PanoramaActorsLayout.KNOCK_FADE_OUT), moveToOffice, Actions.removeActor());
                    }
                    knock.addAction(knockActions);
                }
            }
        });

        tile3DoorSign.setName(STREET_DOOR);

        detectiveDoor.addActor(tile3Background);
        detectiveDoor.addActor(tile3DoorSign);
        detectiveDoor.addActor(tile3PlateTitle);
        detectiveDoor.setSize(tile3Background.getWidth(), tile3Background.getHeight());
        group.add(detectiveDoor);

        TablexiaNoBlendingImage leftBorder = new TablexiaNoBlendingImage(ApplicationAtlasManager.getInstance().getColorTexture(SWIPE_OVERSCROLL_LEFT_COLOR));
        leftBorder.setSize(OVERSCROLL_DISTANCE, getSceneInnerHeight());

        TablexiaNoBlendingImage rightBorder = new TablexiaNoBlendingImage(getScreenTextureRegion(SWIPE_OVERSCROLL_RIGHT));
        rightBorder.setHeight(getSceneInnerHeight());

        scrollpane = new ScrollPaneWithBorders(group, leftBorder, rightBorder);
        scrollpane.setFillParent(true);
        scrollpane.setScrollingDisabled(false, true);
        scrollpane.setupOverscroll(OVERSCROLL_DISTANCE, OVERSCROLL_MIN_SPEED, OVERSCROLL_MAX_SPEED);
        return scrollpane;
    }

    private void prepareSwipeStreetFingerHint() {
        hintFinger = ScaleUtil.createImageWidthPosition(getScreenTextureRegion(LEFT_FINGER_TEXTURE), getStage().getWidth() * PanoramaActorsLayout.STREET_HINT_FINGER_WIDTH, getStage().getWidth() * PanoramaActorsLayout.STREET_HINT_FINGER_X - getStage().getWidth() * PanoramaActorsLayout.STREET_HINT_FINGER_WIDTH / 2, 0);
        hintFinger.setVisible(false);
        hintFinger.setTouchable(Touchable.disabled);
        panel.addActor(hintFinger);
        scrollpane.addListener(new EventListener() { // animate finger after 3s of inactivity and only if scrolled to less than 0.7 of maxX

            @Override
            public boolean handle(Event event) {
                if(TablexiaSettings.getInstance().getPlatform() == TablexiaSettings.Platform.DESKTOP) scrollToOfficeDoorOnDesktop();
                return handleHintFinger();
            }
        });
        handleHintFinger();
    }

    private void scrollToOfficeDoorOnDesktop() {
        scrollpane.scrollTo(scrollpane.getWidget().getWidth(), 0, 0, 0);
        scrollpane.getListeners().removeAll(scrollpane.getListeners(), false);
    }

    private boolean handleHintFinger() {
        if (scrollpane.getScrollX() != lastScroll) {
            final Action oldAction = lastHintFingerAction;
            hintFinger.addAction(Actions.sequence(Actions.fadeOut(PanoramaActorsLayout.FINGER_FADE_OUT), Actions.run(new Runnable() {
                @Override
                public void run() {
                    hintFinger.removeAction(oldAction);
                    hintFinger.setScale(PanoramaActorsLayout.FINGER_ORIGINAL_SCALE_TO);
                    hintFinger.setPosition(getStage().getWidth() * PanoramaActorsLayout.STREET_HINT_FINGER_X - getStage().getWidth() * PanoramaActorsLayout.STREET_HINT_FINGER_WIDTH / 2, 0);
                }
            })));
            //ScrollPanes getScrollPercentX method returns NaN if scroll pane wasn't touched yet...
            float percentScrollX = Float.isNaN(scrollpane.getScrollPercentX()) ? 0 : scrollpane.getScrollPercentX();
            if (percentScrollX < PanoramaActorsLayout.STREET_HINT_END) {
                hintFinger.addAction(lastHintFingerAction = Actions.sequence(
                        Actions.delay(PanoramaActorsLayout.STREET_HINT_DELAY)
                        , Actions.alpha(0)
                        , Actions.visible(true)
                        , Actions.fadeIn(PanoramaActorsLayout.FINGER_FADE_IN)
                        , Actions.forever(Actions.sequence(
                                Actions.moveTo(getStage().getWidth() * PanoramaActorsLayout.STREET_HINT_MOVE_TO - hintFinger.getWidth() / 2, hintFinger.getY(), PanoramaActorsLayout.FINGER_MOVE_DURATION)
                                , Actions.scaleTo(PanoramaActorsLayout.FINGER_SCALE_TO, PanoramaActorsLayout.FINGER_SCALE_TO, PanoramaActorsLayout.FINGER_SCALE_DURATION)
                                , Actions.moveTo(-hintFinger.getWidth(), hintFinger.getY(), PanoramaActorsLayout.FINGER_MOVE_DURATION)
                                , Actions.scaleTo(PanoramaActorsLayout.FINGER_ORIGINAL_SCALE_TO, PanoramaActorsLayout.FINGER_ORIGINAL_SCALE_TO, PanoramaActorsLayout.FINGER_ORIGINAL_SCALE_DURATION)))));
            }
        }
        lastScroll = scrollpane.getScrollX();
        return false;
    }

    protected void screenResized(int width, int height) {
        if (panel != null && scrollpane != null) {
            ScaleUtil.setFullScreen(panel, getStage());
            panel.setCullingArea(new Rectangle(panel.getX(), panel.getY(), panel.getWidth(), panel.getHeight()));
            float scrollX = scrollpane.getScrollPercentX();
            panel.setY(getViewportBottomY());
            panel.clear();
            panel.addActor(prepareStreetGroup());
            prepareSwipeStreetFingerHint();
            scrollpane.pack();
            scrollpane.setScrollPercentX(scrollX);
            scrollpane.updateVisualScroll();
        }

        if (leftFinger != null && rightFinger != null && hint != null && newspaperStages != null) {
            leftFinger.setY(getViewportBottomY());
            rightFinger.setY(getViewportBottomY());
            hint.setY(getViewportBottomY() + leftFinger.getHeight() / 2);
            newspaperStages.setY(getViewportBottomY());
            prepareFingersSweepActions();
        }
    }

    @Override
    public void backButtonPressed() {
        if (openNewspaperTimer != null) openNewspaperTimer.cancel();
        if (openTablexiaArticleTimer != null) openTablexiaArticleTimer.cancel();

        ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(TablexiaSettings.LOADER_SCREEN, TablexiaApplication.ScreenTransaction.FADE)).asynchronously();
    }

    private enum Article {
        BEARD_ARTICLE       (0, Color.RED,    MUSIC_1),
        BALOON_ARTICLE      (1, Color.GREEN,  MUSIC_2),
        MOTOBIKE_ARTICLE    (2, Color.BLUE,   MUSIC_3),
        SWIMSUIT_ARTICLE    (3, Color.YELLOW, MUSIC_4),
        DETECTIVE_ARTICLE   (4, Color.BLACK,  MUSIC_5);

        private int index;
        private Color color;
        private String musicPath;

        Article(int index, Color color, String musicPath) {
            this.index = index;
            this.color = color;
            this.musicPath = musicPath;
        }
    }
}
