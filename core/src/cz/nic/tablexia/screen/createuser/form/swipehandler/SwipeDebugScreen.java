/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.createuser.form.swipehandler;

import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Map;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.createuser.form.SignatureInputPane;

/**
 * Created by lhoracek on 4/24/15.
 */
public class SwipeDebugScreen extends AbstractTablexiaScreen<Void> {
    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        getStage().setDebugAll(true);
        Actor swiper = new SignatureInputPane();
        swiper.setSize(TablexiaSettings.getWorldSize(), TablexiaSettings.getMinWorldHeight());
        swiper.setPosition(0, 0);
        getStage().addActor(swiper);
    }
}
