/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.createuser.form;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.screen.createuser.form.swipehandler.SwipeHandler;
import cz.nic.tablexia.util.Point;

/**
 * Created by lhoracek on 4/24/15.
 */
public class SignatureInputPane extends SignaturePane {

    SwipeHandler swipeHandler = new SwipeHandler(1000);

    public SignatureInputPane() {
        addListener(swipeHandler);
    }

    @Override
    public List<Array<Point>> getSignature() {
        List<Array<Vector2>> absolutePaths = swipeHandler.getPaths();
        List<Array<Point>> percentagePaths = new ArrayList<Array<Point>>();
        for (Array<Vector2> array : absolutePaths) {
            Array<Point> percentageArray = new Array<Point>(array.size);
            percentagePaths.add(percentageArray);
            // don't use foreach here. It fails with it sometimes due to multi thread call of this method
            for (int i = 0; i < array.size; i++) {
                Vector2 point = array.get(i);
                percentageArray.add(new Point(point.x / getWidth(), point.y / getHeight()));
            }
        }
        return percentagePaths;
    }

    @Override
    public boolean clip() {
        return true;
    }
}
