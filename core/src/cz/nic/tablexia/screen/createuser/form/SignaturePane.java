/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.createuser.form;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;

import java.util.List;

import cz.nic.tablexia.util.Point;

/**
 * Created by lhoracek on 11/6/15.
 */
public class SignaturePane extends Actor {

    private ShapeRenderer shapeRenderer = new ShapeRenderer();
    private List<Array<Point>> signature;

    public SignaturePane() {
    }

    public SignaturePane(List<Array<Point>> signature) {
        setSignature(signature);
    }

    public SignaturePane(String signature) {
        setSignature(signature);
    }


    @Override
    protected void setStage (Stage stage) {
        super.setStage(stage);

        if (stage != null) {
            shapeRenderer.setProjectionMatrix(stage.getCamera().combined);
        }
    }

    public void setSignature(final String signature) {
        if (signature == null || signature.length() == 0) {
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                Json json = new Json();
                SignaturePane.this.signature = json.fromJson(List.class, signature);
            }
        }).start();
    }

    public List<Array<Point>> getSignature() {
        return signature;
    }

    public void setSignature(List<Array<Point>> signature) {
        this.signature = signature;
    }

    public boolean clip() {
        return false;
    }

    private float pointOffsetY;
    public void setPointOffsetY(float offsetY) {
        pointOffsetY = offsetY;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        List<Array<Point>> signature = getSignature();
        if (signature != null && signature.size() > 0) {
            batch.end();
            if (clip()) {
                clipBegin();
            }
            //TODO 4 Desktop... Get rid of glLineWidth, a lot of drivers ignore it...
            Gdx.gl.glLineWidth(3);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(new Color(0x001585ff));
            for (Array<Point> path : signature) {
                if (path.size > 1) {
                    float[] vert = new float[path.size * 2];
                    for (int i = 0; i < path.size; i++) {
                        Point p = path.get(i).cpy().scl(getWidth(), getHeight());
                        Vector2 sc = localToStageCoordinates(new Vector2(p.x, p.y));

                        sc.y -= pointOffsetY;

                        vert[i * 2] = sc.x;
                        vert[i * 2 + 1] = sc.y;
                    }
                    shapeRenderer.polyline(vert);
                }
            }
            shapeRenderer.end();
            Gdx.gl.glLineWidth(1);
            if (clip()) {
                clipEnd();
            }
            batch.begin();
        }
        super.draw(batch, parentAlpha);
    }
}
