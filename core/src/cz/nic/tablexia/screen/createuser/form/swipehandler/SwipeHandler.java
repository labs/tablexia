/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.createuser.form.swipehandler;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.List;

public class SwipeHandler extends InputListener {

    private FixedList<Vector2> inputPoints;

    /**
     * The pointer associated with this swipe event.
     */
    private int inputPointer = 0;

    /**
     * The minimum distance between the first and second point in a drawn line.
     */
    public int initialDistance = 5;

    /**
     * The minimum distance between two points in a drawn line (starting at the second point).
     */
    public int minDistance = 5;
    private final int maxInputPoints;
    private Vector2 lastPoint = new Vector2();

    private SwipeResolver        simplifier = new ResolverRadialChaikin();
    private List<Array<Vector2>> paths      = new ArrayList<Array<Vector2>>();
    private Array<Vector2> lastArray;

    public SwipeHandler(int maxInputPoints) {
        this.inputPoints = new FixedList<Vector2>(maxInputPoints, Vector2.class);
        this.maxInputPoints = maxInputPoints;
        //  resolve(); //copy initial empty list
    }

    /**
     * Returns the fixed list of input points (not simplified).
     *
     * @return the list of input points
     */
    public Array<Vector2> input() {
        return this.inputPoints;
    }

    /**
     * Returns the simplified list of points representing this swipe.
     *
     * @return
     */
    public List<Array<Vector2>> getPaths() {
        return paths;
    }

    /**
     * If the points are dirty, the line is simplified.
     */
    public void resolve() {
        simplifier.resolve(inputPoints, lastArray);
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if(button == Input.Buttons.RIGHT) return false;
        if (pointer != inputPointer || button != inputPointer) {
            return false;
        }

        lastArray = new Array<Vector2>(true, maxInputPoints, Vector2.class);
        paths.add(lastArray);
        //clear points
        inputPoints.clear();

        //starting point
        lastPoint = new Vector2(x, y);
        inputPoints.insert(lastPoint);

        resolve();
        return true;
    }


    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        //on release, the line is simplified
        resolve();
        lastArray = null;
    }


    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (pointer != inputPointer)
            return;
        Vector2 v = new Vector2(x, y);

        //calc length
        float dx = v.x - lastPoint.x;
        float dy = v.y - lastPoint.y;
        float len = (float) Math.sqrt(dx * dx + dy * dy);
        //TODO: use minDistanceSq

        //if we are under required distance
        if (len < minDistance && (inputPoints.size > 1 || len < initialDistance))
            return;

        //add new point
        inputPoints.insert(v);
        lastPoint = v;

        //simplify our new line
        resolve();
    }
}
