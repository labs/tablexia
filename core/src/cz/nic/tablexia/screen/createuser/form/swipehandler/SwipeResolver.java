/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.createuser.form.swipehandler;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public interface SwipeResolver {
	
	/**
	 * Simplifies and smoothes the input.
	 * 
	 * @param input the input of the swipe event
	 * @param output the output instance
	 */
	public void resolve(Array<Vector2> input, Array<Vector2> output);
}
