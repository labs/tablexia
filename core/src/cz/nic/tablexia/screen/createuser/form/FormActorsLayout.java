/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.createuser.form;

/**
 * Created by frantisek on 6.1.16.
 */
public class FormActorsLayout {

    public static final int    MUGSHOT_HEIGHT                   = 250;
    public static final int    MUGSHOTS_SCROLL_PANE_Y           = 156;

    public static final int    HINT_DIALOG_WIDTH                = 200;
    public static final int    HINT_DIALOG_HEIGHT               = 36;

	public static final int    HINT_DIALOG_BIGGER_WIDTH         = 250;
	public static final int    HINT_DIALOG_BIGGER_HEIGHT        = 70;

    public static final int    CLEAR_NAME_BUTTON_PADDING        = 10;

    public static final int    STAMP_DROP_AREA_LEFT_X           = 636;
    public static final int    STAMP_DROP_AREA_RIGHT_X          = 775;
    public static final int    STAMP_DROP_AREA_BOTTOM_Y         = -130;
    public static final int    STAMP_DROP_AREA_TOP_Y            = 50;

    public static final int    PEN_DROP_AREA_LEFT_X             = 526;
    public static final int    PEN_DROP_AREA_RIGHT_X            = 700;
    public static final int    PEN_DROP_AREA_BOTTOM_Y           = -300;
    public static final int    PEN_DROP_AREA_TOP_Y              = -190;

    public static final float  PEN_WIDTH                        = 0.2f;
    public static final float  PEN_X                            = 0.85f;
    public static final float  PEN_Y                            = -0.17f;

    public static final float  STAMP_WIDTH                      = 0.2f;
    public static final float  STAMP_X                          = 0.01f;
    public static final float  STAMP_Y                          = 0.35f;

    public static final float  SIGNATURE_PANE_X                 = 0.53f;
    public static final float  SIGNATURE_PANE_Y                 = 0.1f;
    public static final float  SIGNATURE_PANE_WIDTH             = 0.2f;
    public static final float  SIGNATURE_PANE_HEIGHT            = 0.12f;

    public static final float  SWITCH_GENDER_SCALE              = 0.8f;
    public static final float  SWITCH_GENDER_X                  = 0.725f;
    public static final float  SWITCH_GENDER_Y                  = 0.145f;

    public static final float  BUTTON_PLUS_X                    = 0.615f;
    public static final float  BUTTON_PLUS_Y                    = 0.14f;
    public static final float  BUTTON_PLUS_WIDTH                = 0.035f;
    public static final float  BUTTON_PLUS_HEIGHT               = 0.035f;
    public static final float  BUTTON_MINUS_X                   = 0.535f;
    public static final float  BUTTON_MINUS_Y                   = 0.14f;
    public static final float  BUTTON_MINUS_WIDTH               = 0.035f;
    public static final float  BUTTON_MINUS_HEIGHT              = 0.035f;

    public static final float  IMAGE_FEMALE_WIDTH               = 0.015f;
    public static final float  IMAGE_FEMALE_X                   = 0.80f;
    public static final float  IMAGE_FEMALE_Y                   = 0.143f;
    public static final float  IMAGE_MALE_WIDTH                 = 0.015f;
    public static final float  IMAGE_MALE_X                     = 0.69f;
    public static final float  IMAGE_MALE_Y                     = 0.143f;

    public static final float  AGE_LABEL_X                      = 0.58f;
    public static final float  AGE_LABEL_Y                      = 0.185f;

    public static final float  AGE_VALUE_X                      = 0.58f;
    public static final float  AGE_VALUE_Y                      = 0.14f;

    public static final float  GENDER_LABEL_X                   = 0.73f;
    public static final float  GENDER_LABEL_Y                   = 0.185f;

    public static final float  NAME_FIELD_X                     = 0.575f;
    public static final float  NAME_FIELD_Y                     = 0.21f;
    public static final float  NAME_FIELD_WIDTH                 = 0.2f;
    public static final float  NAME_FIELD_HEIGHT                = 0.05f;

    public static final float BACKGROUND_WIDTH                  = 0.75f;
    public static final float BACKGROUND_HEIGHT                 = 0.98f;
    public static final float BACKGROUND_X                      = 0.13f;

    public static final float INK_PAD_WIDTH                     = 0.4f;
    public static final float INK_PAD_X                         = -0.2f;
    public static final float INK_PAD_Y                         = 0.35f;

    public static final float BADGE_WIDTH                       = 0.3f;
    public static final float BADGE_X                           = 0.18f;
    public static final float BADGE_Y                           = 0.17f;

    public static final float MUGSHOT_IMAGE_WIDTH               = 0.134f;
    public static final float MUGSHOT_IMAGE_X                   = 0.6f;
    public static final float MUGSHOT_IMAGE_Y                   = 0.289f;

    public static final float MUGSHOT_FRAME_WIDTH               = 0.17f;
    public static final float MUGSHOT_FRAME_X                   = 0.58f;
    public static final float MUGSHOT_FRAME_Y                   = 0.27f;

    public static final float COVER_NAME_WIDTH                  = 0.2f;
    public static final float COVER_NAME_X                      = 0.57f;
    public static final float COVER_NAME_Y                      = 0.22f;

    public static final float STAMP_PLACEHOLDER_WIDTH           = 0.1f;
    public static final float STAMP_PLACEHOLDER_X               = 0.73f;
    public static final float STAMP_PLACEHOLDER_Y               = 0.04f;

    public static final float SIGNATURE_PLACE_WIDTH             = 0.15f;
    public static final float SIGNATURE_PLACE_X                 = 0.55f;
    public static final float SIGNATURE_PLACE_Y                 = 0.07f;

    public static final float INK_WIDTH                         = 0.15f;
    public static final float INK_X                             = 0.85f;

    public static final float SETTING_BUTTON_X                  = 0.9f;
    public static final float SETTING_BUTTON_Y                  = 0.42f;
    public static final float SETTING_BUTTON_WIDTH              = 0.05f;
    public static final float SETTING_BUTTON_HEIGHT             = 0.05f;
}
