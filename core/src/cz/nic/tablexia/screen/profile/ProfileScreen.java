/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.profile;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import java.util.Map;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.event.StartFullSynchronizationEvent;
import cz.nic.tablexia.loader.application.ApplicationAvatarManager;
import cz.nic.tablexia.loader.application.TablexiaBadgesManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.user.UserAvatarDefinition;
import cz.nic.tablexia.menu.user.UserMenu;
import cz.nic.tablexia.model.CustomAvatarDAO;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.createuser.FormScreen;
import cz.nic.tablexia.screen.createuser.FormScreenAssets;
import cz.nic.tablexia.screen.createuser.form.FormActorsLayout;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.sync.work.UpdateAvatar;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.qrcode.QrCodeEncoder;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaProgressBar;
import cz.nic.tablexia.util.ui.button.TablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AlertOnShowDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.BackButtonHideComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.CloseButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DimmerDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;

public class ProfileScreen extends FormScreen {
    public static final String IMAGE_FEMALE = "image female";
    public static final String IMAGE_MALE = "image male";
    public static final String AGE_LABEL ="age label";
    public static final String RANK_LABEL= "rank label";
    public static final String PROGRESS_BAR = "progress bar";

    private static final ApplicationFontManager.FontType QR_CODE_DIALOG_HEADING     = ApplicationFontManager.FontType.BOLD_26;
    private static final ApplicationFontManager.FontType RANK_NAME_FONT_TYPE        = ApplicationFontManager.FontType.BOLD_20;
    private static final ApplicationFontManager.FontType UUID_DESCRIPTION_FONT_TYPE = ApplicationFontManager.FontType.BOLD_16;
    private static final ApplicationFontManager.FontType UUID_FONT_TYPE             = ApplicationFontManager.FontType.BOLD_14;

    private static final String PROGRESS_BAR_FILL             = "gfx/progressbar_fill";
    private static final float  PROGRESS_BAR_WIDTH            = 0.8f;
    private static final float  PROGRESS_BAR_BOTTOM_OFFSET    = 0.28f;
    private static final float  PROGRESS_BAR_SPEED            = 3.0f;
    private static final int    DIALOG_WIDTH                  = 480;
    private static final int    DIALOG_HEIGHT                 = 430;

    private static final String PROGRESS_STATUS_ARROW         = "gfx/progress_arrow";
    private static final float  PROGRESS_STATUS_WIDTH         = 0.8f;
    private static final float  PROGRESS_STATUS_ARROWS_WIDTH  = 0.45f;
    private static final float  PROGRESS_STATUS_BADGE_HEIGHT  = 0.2f;
    private static final float  PROGRESS_STATUS_BOTTOM_OFFSET = 0.33f;

    private static final String USER_UUID_ERROR               = "user_uuid_error";
    private static final String USER_UUID_DESCRIPTION_TEXT    = "user_uuid_description";
    private static final String USER_QR_CODE_DESCRIPTION      = "user_qr_code_description";
    private static final String USER_BACK_WITHOUT_SAVE		  = "user_ask_save_changes";

    private static final Color  USER_QR_CODE_BACKGROUND       = Color.CLEAR;
    private static final Color  USER_QR_CODE_FOREGROUND       = Color.BLACK;

    private static final float BADGE_HEIGHT                           = 0.4f;
    private static final float BADGE_POSITION_Y_SCALE                 = 0.52f;

    private static final float USER_UUID_GROUP_HEIGHT                 = 0.08f;
    private static final float USER_UUID_GROUP_Y_SCALE                = 0.07f;
    private static final float USER_UUID_GROUP_PADDING                = 0.065f;
    private static final float USER_UUID_QR_ICON_SIZE                 = 1f;

    private TablexiaProgressBar     progressBar;
    private Texture                 userQrCode; //Store Texture in member field so it can be disposed later.
    private TablexiaButton          settingButton;
    private boolean                 changesAllowed;
    private ClickListener           profilePictureListener;

    private boolean                 settingsSaved = true;
    private boolean                 acceptedSkipSaveSettings = false;

    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        super.screenLoaded(screenState);
        pen.setVisible(false);
        stamp.setVisible(false);
        inkpad.setVisible(false);
        ink.setVisible(false);

        mugshotFrame.clearListeners();
        buttonPlus.setVisible(false);
        buttonMinus.setVisible(false);
        nameField.clearListeners();
        switchGender.setVisible(false);
        clearName.setVisible(false);
        privacyInfoLabel.setVisible(false);

        Drawable d = new TextureRegionDrawable(ApplicationAvatarManager.getInstance().getAvatarTextureRegion(getSelectedUser()));
        badge.setY(getSceneInnerBottomY() + getSceneInnerHeight() * BADGE_POSITION_Y_SCALE);

        mugshotImage.setDrawable(d);
        nameField.setText(getSelectedUser().getName());
        ageValue.setText(String.valueOf(getSelectedUser().getAge()));
        ageValue.setName(AGE_LABEL);
        age = getSelectedUser().getAge();

        imageFemale.setVisible(getSelectedUser().getGender().equals(GenderDefinition.FEMALE));
        imageFemale.setName(IMAGE_FEMALE);
        imageMale.setVisible(getSelectedUser().getGender().equals(GenderDefinition.MALE));
        imageMale.setName(IMAGE_MALE);

        float center = (imageMale.getX() + imageFemale.getX()) / 2;
        imageFemale.setX(center);
        imageMale.setX(center);
        signaturePane.setSignature(getSelectedUser().getSignature());
        stampIt();

        prepareProgressBar();
        prepareProgressStatus();

        prepareSettingsButton();

        prepareUserUuidGroup(getSelectedUser());

        mugshotFrame.setTouchable(Touchable.enabled);
        mugshotFrameGreen.setTouchable(Touchable.enabled);
        mugshotFrame.addCaptureListener(profilePictureListener = new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                if(changesAllowed) {
                    showMugshotDialog();
                    settingsSaved = false;
                }
                super.clicked(event, x, y);
            }
        });
        mugshotFrameGreen.addCaptureListener(profilePictureListener);
    }

    @Override
    public StopScreenExitReason getStopScreenExitReason(AbstractTablexiaScreen<?> nextScreen) {
        //Is settings saved?
        if(!acceptedSkipSaveSettings && !settingsSaved && TablexiaSettings.getInstance().getSelectedUser() != null) return StopScreenExitReason.SettingsNotSaved;
        else return super.getStopScreenExitReason(nextScreen);
    }

    @Override
    public String prepareExitDialogText(AbstractTablexiaScreen<?> nextScreen, StopScreenExitReason reason) {
        //Get a message for screen exit without saving the settings dialog.
        if(reason == StopScreenExitReason.SettingsNotSaved) return getText(USER_BACK_WITHOUT_SAVE);
        else return super.prepareExitDialogText(nextScreen, reason);
    }

    @Override
    protected void onExitDialogCanceled(StopScreenExitReason reason) {
        //User canceled the settings dialog. Continue with the exit action.
        if(reason == StopScreenExitReason.SettingsNotSaved) {
            resetExitDialogState();
            acceptedSkipSaveSettings = true;
            runAfterScreenExitAction();
        }
        else super.onExitDialogCanceled(reason);
    }

    @Override
    protected void onExitDialogConfirmed(StopScreenExitReason reason) {
        //User accepted settings dialog. Save settings and continue with the exit action.
        if(reason == StopScreenExitReason.SettingsNotSaved) {
            saveSettings(getSelectedUser());
            runAfterScreenExitAction();
        }
        else super.onExitDialogCanceled(reason);
    }

    @Override
    public void resetExitDialogState() {
        acceptedSkipSaveSettings = false;
        super.resetExitDialogState();
    }

    @Override
    protected void onClickPlus() {
        setAge(Math.min(ProfileScreen.this.age + 1, MAX_USER_AGE));
        settingsSaved = false;
    }

    @Override
    protected void onClickMinus() {
        setAge(Math.max(ProfileScreen.this.age - 1, 1));
        settingsSaved = false;
    }

    @Override
    public boolean hasSoftBackButton() {
        return true;
    }

    @Override
    protected void screenResized(int width, int height) {
        super.screenResized(width, height);
    }

    @Override
    protected boolean validationEnabled() {
        return false;
    }

    @Override
    protected String prepareScreenName() {
        return "form";
    }

    @Override
    protected Void prepareScreenData(Map<String, String> screenState) {
        return null;
    }

    @Override
    protected Actor prepareBadge() {
        TablexiaBadgesManager.getInstance().refreshBadges();
        badge = ScaleUtil.createImageToHeight(TablexiaBadgesManager.getInstance().getCurrentRankBadgeTexture(), BADGE_HEIGHT * getSceneInnerHeight());
        float pageWidth = (book.getWidth()/2);
        badge.setPosition(book.getX() + pageWidth/2 - badge.getWidth()/2, badge.getY());

        UserRankManager.UserRank rank = UserRankManager.getInstance().getRank(getSelectedUser());
        badge.setName(rank.toString());

        return badge;
    }

    private void prepareSettingsButton() {
        settingButton =  new TablexiaButton("", false,
                getScreenTextureRegion(FormScreenAssets.BUTTON_SETTINGS),
                getScreenTextureRegion(FormScreenAssets.BUTTON_SETTINGS_PRESSED),
                null, null) {
            //had to made this method public in order to be able freely (un)check button without being tightened to logic beneath it
            //(needed when user wants to log out, saved settings, but then changed his mind and stayed on screen)
           @Override
            public void toggleCheckState() {
               allowChangeSettings(!changesAllowed);
               if(!changesAllowed) saveSettings(getSelectedUser());
           }
        }.checkable(true);

        settingButton.setBounds(getStage().getWidth() * FormActorsLayout.SETTING_BUTTON_X, getStage().getWidth() * FormActorsLayout.SETTING_BUTTON_Y, getStage().getWidth() * FormActorsLayout.SETTING_BUTTON_WIDTH, getStage().getWidth() * FormActorsLayout.SETTING_BUTTON_HEIGHT);
        getStage().addActor(settingButton);
    }

    private void allowChangeSettings(boolean allow) {
        buttonPlus.setVisible(allow);
        buttonMinus.setVisible(allow);
        if(settingButton != null) settingButton.setChecked(allow);
        mugshotFrame.setVisible(!allow);
        mugshotFrameGreen.setVisible(allow);
        changesAllowed = allow;
    }

    private void prepareProgressBar() {
        this.progressBar = new TablexiaProgressBar(
                ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.PROGRESS_BAR),
                getScreenTextureRegion(PROGRESS_BAR_FILL)
        );

        float pageWidth = book.getWidth() / 2f;

        this.progressBar.setWidth(pageWidth * PROGRESS_BAR_WIDTH);
        this.progressBar.setPosition(
                book.getX() + (pageWidth / 2f) - (progressBar.getWidth() / 2),
                book.getY() + book.getHeight() * PROGRESS_BAR_BOTTOM_OFFSET
        );

        UserRankManager.RankProgress resultRankProgress = UserRankManager.getInstance().getTotalUserRankProgress(getSelectedUser());

        this.progressBar.setSmooth(false);
        this.progressBar.setPercent(0);
        this.progressBar.setSmooth(true);
        this.progressBar.setSpeed(PROGRESS_BAR_SPEED);
        this.progressBar.setPercent(resultRankProgress.getPercentDone());

        this.progressBar.setName(PROGRESS_BAR);

        getStage().addActor(progressBar);
    }

    private void prepareProgressStatus () {
        final float pageWidth = book.getWidth()/2f;
        final float groupWidth = pageWidth * PROGRESS_STATUS_WIDTH;

        Group group = new Group();
        group.setWidth(groupWidth);
        group.setPosition(book.getX() + pageWidth/2f - group.getWidth()/2f, book.getY() + book.getHeight() * PROGRESS_STATUS_BOTTOM_OFFSET);

        UserRankManager.UserRank currRank = UserRankManager.getInstance().getRank(getSelectedUser());
        UserRankManager.UserRank nextRank = UserRankManager.UserRank.getNextRank(currRank);

        Image currBadge = ScaleUtil.createImageToHeight(ApplicationAtlasManager.getInstance().getTextureRegion(currRank.getBadgeIconKey()), PROGRESS_STATUS_BADGE_HEIGHT * groupWidth);
        group.addActor(currBadge);

        Image progress = ScaleUtil.createImageToWidth(getScreenTextureRegion(PROGRESS_STATUS_ARROW), PROGRESS_STATUS_ARROWS_WIDTH * groupWidth);
        progress.setPosition(group.getWidth()/2 - progress.getWidth()/2, currBadge.getY() + currBadge.getHeight()/2 - progress.getHeight()/2);
        group.addActor(progress);

        Image nextBadge = ScaleUtil.createGreyScaleImageToHeight(ApplicationAtlasManager.getInstance().getTextureRegion(nextRank.getBadgeIconKey()), PROGRESS_STATUS_BADGE_HEIGHT * groupWidth);
        nextBadge.setX(group.getWidth() - nextBadge.getWidth());
        group.addActor(nextBadge);

        if(currRank.hasBadgeName()) {
            TablexiaLabel rankName = new TablexiaLabel(ApplicationTextManager.getInstance().getText(currRank.getNameKey()), new TablexiaLabel.TablexiaLabelStyle(RANK_NAME_FONT_TYPE, Color.BLACK));
            rankName.setSize(group.getWidth(), rankName.getHeight());
            rankName.setAlignment(Align.center);
            rankName.setY(progress.getY() + progress.getHeight());
            rankName.setName(RANK_LABEL);
            group.addActor(rankName);
        }

        getStage().addActor(group);
    }

    private void prepareUserUuidGroup(User user) {
        Group group = new Group();

        float pageWidth = book.getWidth() / 2f;
        group.setSize(pageWidth - (2 * (pageWidth * USER_UUID_GROUP_PADDING)), getSceneInnerHeight() * USER_UUID_GROUP_HEIGHT);
        group.setPosition(book.getX() + USER_UUID_GROUP_PADDING * pageWidth, book.getY() + getSceneInnerHeight() * USER_UUID_GROUP_Y_SCALE);

        if(user.getUuid() == null || user.getUuid().isEmpty()) {
            TablexiaLabel syncNeededLabel = new TablexiaLabel(getText(USER_UUID_ERROR), new TablexiaLabel.TablexiaLabelStyle(UUID_DESCRIPTION_FONT_TYPE, Color.BLACK));
            syncNeededLabel.setWidth(group.getWidth());
            syncNeededLabel.setAlignment(Align.center);
            syncNeededLabel.setWrap(true);
            syncNeededLabel.setPosition(group.getWidth() / 2f - syncNeededLabel.getWidth() / 2f, group.getHeight() / 2f - syncNeededLabel.getHeight() / 2f);
            group.addActor(syncNeededLabel);
        }
        else {
            Image qrCodeImage = new Image(getScreenTextureRegion(FormScreenAssets.QR_MAGNIFIER_ICON));
            qrCodeImage.setSize(group.getHeight() * USER_UUID_QR_ICON_SIZE, group.getHeight() * USER_UUID_QR_ICON_SIZE);
            qrCodeImage.setPosition(group.getWidth() - qrCodeImage.getWidth(), group.getHeight() / 2f - qrCodeImage.getHeight() / 2f);
            group.addActor(qrCodeImage);

            TablexiaLabel uuidDescriptionLabel = new TablexiaLabel(getText(USER_UUID_DESCRIPTION_TEXT), new TablexiaLabel.TablexiaLabelStyle(UUID_DESCRIPTION_FONT_TYPE, Color.BLACK));
            uuidDescriptionLabel.setWidth(group.getWidth());
            uuidDescriptionLabel.setAlignment(Align.center);
            uuidDescriptionLabel.setPosition(0, group.getHeight() - uuidDescriptionLabel.getHeight());
            group.addActor(uuidDescriptionLabel);

            TablexiaLabel uuidLabel = new TablexiaLabel(user.getUuid(), new TablexiaLabel.TablexiaLabelStyle(UUID_FONT_TYPE, Color.BLACK));
            uuidLabel.setWidth(group.getWidth() - qrCodeImage.getWidth());
            uuidLabel.setAlignment(Align.center);
            group.addActor(uuidLabel);

            group.addListener(new ClickListenerWithSound() {
                @Override
                public void onClick(InputEvent event, float x, float y) {
                    showQRCodeDialog();
                }
            });
        }

        getStage().addActor(group);
    }

    private Texture prepareQRCodeTexture() {
        //Do NOT create more than one QR Code Texture
        if(userQrCode == null)
            userQrCode = QrCodeEncoder.createQrCode(getSelectedUser().getUuid(), USER_QR_CODE_BACKGROUND, USER_QR_CODE_FOREGROUND);

        return userQrCode;
    }

    private void showQRCodeDialog() {
        TablexiaComponentDialogFactory.getInstance().createDialog(
                new ResizableSpaceContentDialogComponent(),
                new TextContentDialogComponent(getText(USER_QR_CODE_DESCRIPTION), QR_CODE_DIALOG_HEADING),
                new ResizableSpaceContentDialogComponent(),
                new ImageContentDialogComponent(new Image(prepareQRCodeTexture())),
                new CloseButtonContentDialogComponent(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_CLOSE)),
                new AlertOnShowDialogComponent(),
                new CenterPositionDialogComponent(),
                new DimmerDialogComponent(),
                new BackButtonHideComponent(),
                new ResizableSpaceContentDialogComponent()
        ).show(DIALOG_WIDTH, DIALOG_HEIGHT);
    }

    private int getAgeValue(){
        return Integer.valueOf(ageValue.getText().toString());
    }


    private void saveSettings(User user) {
        user.setAge(getAgeValue());
        UserDAO.updateUser(user);
        if(userAvatarDefinition != null) saveProfilePicture(user);
        settingsSaved = true;
        allowChangeSettings(false);
    }

    private void saveProfilePicture(User user) {
        user.setAvatar(Integer.toString(userAvatarDefinition.number()));

        if (UserAvatarDefinition.isUserUsingCustomAvatar(user)) {
            ApplicationAvatarManager.getInstance().createCustomAvatar(user, customAvatarPixmap, customAvatarPreviewTextureRegion);

            //Do not synchronize when user has no uuid
            if(user.getUuid() != null && !user.getUuid().isEmpty()) RestSynchronizationService.doSyncWork(new UpdateAvatar(user));
        } else {
            //Changing from custom avatar to one of the predefined ones.
            CustomAvatarDAO.deleteCustomAvatar(user.getId());
        }
        UserDAO.updateUser(user);
        ApplicationBus.getInstance().publishAsync(new UserMenu.RefreshUserMenu());
        ApplicationBus.getInstance().publishAsync(new StartFullSynchronizationEvent(user));
    }

    @Override
	public void backButtonPressed() {
        backToInitialScreen();
	}

    @Override
    protected void screenDisposed() {
        if(userQrCode != null) userQrCode.dispose();
        super.screenDisposed();
    }
}