/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.loader;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;

import cz.nic.tablexia.loader.application.ApplicationAtlasManager;

/**
 * Created by Drahomir Karchnak on 09/12/15.
 * Simple Loading Bar...
 */
public class LoadingBar extends Actor {
    /**
     * This class represents a skin for LoadingBar
     * Basically it defines what the LoadingBar will look like
     */
    public static class LoadingBarSkin {
        private Texture foregroundImage, backgroundImage;
        private Texture shadowImage;

        public LoadingBarSkin(Texture foregroundImage, Texture backgroundImage, Color shadowColor) {
            super();
            this.foregroundImage = foregroundImage;
            this.backgroundImage = backgroundImage;
            this.shadowImage     = ApplicationAtlasManager.getInstance().getColorTexture(shadowColor);

            this.foregroundImage.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.backgroundImage.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            this.shadowImage.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        }

        public Texture getForegroundImage() {
            return foregroundImage;
        }

        public Texture getBackgroundImage() {
            return backgroundImage;
        }

        public Texture getShadow() {
            return shadowImage;
        }

        public void dispose() {
            foregroundImage.dispose();
            backgroundImage.dispose();
            shadowImage.dispose();
        }
    }

    public enum LoadingBarActivity {
        PROGRESS,	// Is showing progress
        IDLE,		// IDLE/Infinite state
    }

    private static final float SHADOW_ALPHA_VALUE   = 0.52f;

    private static final float IDLE_BAR_WIDTH = 0.1f;
    private static final float IDLE_BAR_SPEED = .75f;

    //max and current value
    private float current;

	//Helping variables for idle activity
    private float idleCurrent = 0;
    private boolean idleRight = true;

    private LoadingBarActivity activity = LoadingBarActivity.IDLE;
    private LoadingBarSkin skin;

    public LoadingBar(float current, LoadingBarSkin skin) {
        this.current = current;
        this.skin = skin;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
		//Is current activity Progress
        if(activity == LoadingBarActivity.PROGRESS) {
            //make sure current is in interval <0, 1>
			current = MathUtils.clamp(current, 0, 1);
        }
		//Is current activity IDLE
        else if(activity == LoadingBarActivity.IDLE) {
			float step = delta*IDLE_BAR_SPEED;

            if(idleRight) { //Is going right
                float max = 1 - IDLE_BAR_WIDTH;
                if(idleCurrent + step > max) { //should change direction ?
                    idleCurrent = max - (max - (idleCurrent + step));
                    idleRight = false;
                }
                else {
                    idleCurrent += step;
                }
            }
            else { //Is going left
                if(idleCurrent - step < 0) { //should change direction ?
                    idleCurrent = 0 + (Math.abs(idleCurrent - step));
                    idleRight = true;
                }
                else {
                    idleCurrent -= step;
                }
            }

            idleCurrent = MathUtils.clamp(idleCurrent, 0, 1 - IDLE_BAR_WIDTH);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(new Color(this.getColor().r, this.getColor().g, this.getColor().b, this.getColor().a * parentAlpha));

        batch.enableBlending();

        /*
         * Variables for Background/Progress Layer
         */
        float borderX = (skin.foregroundImage.getWidth() - skin.backgroundImage.getWidth()) / 2;
        float borderY = (skin.foregroundImage.getHeight() - skin.backgroundImage.getHeight()) / 2;

        float relativeBorderX = borderX / skin.foregroundImage.getWidth();
        float relativeBorderY = borderY / skin.foregroundImage.getHeight();

        float offsetX = relativeBorderX * getWidth();
        float offsetY = relativeBorderY * getHeight();

        /*
         * Draw Shadow Layer
         */
        Color batchColor = batch.getColor();
        float originalAlpha = batchColor.a;
        batchColor.a *= SHADOW_ALPHA_VALUE;
        batch.setColor(batchColor);

        batch.draw(
                skin.shadowImage,
                getX() + offsetX,
                getY() + offsetY,
                getWidth()  - 2 * offsetX,
                getHeight() - 2 * offsetY
        );

        batchColor.a = originalAlpha;
        batch.setColor(batchColor);

        /*
         * Draw Background/Progress Layer
         */
        if(activity == LoadingBarActivity.PROGRESS) {
            batch.draw(skin.backgroundImage,
                    getX() + offsetX,
                    getY() + offsetY,
                    (getWidth() - 2 * offsetX) * current,
                    getHeight() - 2 * offsetY,
                    0,
                    0,
                    (int) (skin.backgroundImage.getWidth() * current),
                    skin.backgroundImage.getHeight(),
                    false,
                    false
            );
        }
        else if(activity == LoadingBarActivity.IDLE) {
            batch.draw(skin.backgroundImage,
                    getX() + offsetX + (getWidth() - 2 * offsetX) * idleCurrent,
                    getY() + offsetY,
                    (getWidth() - 2 * offsetX) * IDLE_BAR_WIDTH,
                    getHeight() - 2 * offsetY,
                    (int) (skin.backgroundImage.getWidth() * IDLE_BAR_WIDTH),
                    0,
                    (int) (skin.backgroundImage.getWidth() * (idleCurrent + IDLE_BAR_WIDTH)),
                    skin.backgroundImage.getHeight(),
                    false,
                    false
            );
        }

        /*
         * Draw MovingImage/Progress Layer
         */
        batch.draw(skin.foregroundImage, getX(), getY(), getWidth(), getHeight());

        super.draw(batch, parentAlpha);
    }

	/**
	 * Sets current value in percents
	 * @param value
	 */
    public void setCurrentPercentage(float value) {
        value = MathUtils.clamp(value, 0, 1);
        current = value;
    }

	/**
	 * Changes LoadingBarActivity
	 * @param activity
	 */
    public void changeActivity(LoadingBarActivity activity) {
        this.activity = activity;
    }

    public void dispose() {
        skin.dispose();
    }
}
