/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.loader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;

import net.engio.mbassy.listener.Handler;

import java.util.Map;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.event.QRCodeActiveEvent;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaTextureManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.loader.zip.TablexiaAssetsManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.user.UserMenu;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.MusicUtil;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;

public class LoaderScreen extends AbstractTablexiaScreen<Void> {
    //Tower Clock
    //How long does it take for the big hand to rotate by 360 degrees
    private static final int CLOCK_BIG_HAND_SPEED        = 8;
    private static final float CLOCK_HANDS_OFFSET_X      = 223;
    private static final float CLOCK_HANDS_OFFSET_Y      = 238;
    private static final int CLOCK_HANDS_WIDTH           = 10;
    private static final int CLOCK_HANDS_HEIGHT          = 40;
    private static final int CLOCK_HANDS_ORIGIN_OFFSET_Y = 5;

    //Launch Logo
    private static final float LAUNCH_LOGO_WIDTH            = 0.35f;
    private static final float LAUNCH_LOGO_Y_TOP_OFFSET		= 0.10f;
    private static final int   LAUNCH_LOGO_OFFSET           = 20;

    //Loading Bar
    private static final Color LOADING_BAR_SHADOW_COLOR     = Color.BLACK;
    private static final float LOADING_BAR_WIDTH            = 0.52f;
    private static final float LOADING_BAR_OFFSET_Y         = 0.16f;
    private static final float LOADING_BAR_FADE_IN_TIME		= 0.5f;
    private static final float LOADING_BAR_FADE_OUT_TIME	= 0.5f;

	//Loading Status Text
	private static final float STATUS_TEXT_FADE_IN_TIME		= 0.5f;
    private static final float STATUS_TEXT_FADE_OUT_TIME	= 0.5f;
    private static final float LOADING_STATUS_TOP_OFFSET    = 2f;

	//Please Login Text
	private static final float LOGIN_TEXT_WIDTH 			= 0.43f;
	private static final float LOGIN_TEXT_FADE_IN_TIME		= 0.5f;
	private static final float LOGIN_TEXT_FADE_OUT_TIME		= 0.5f;
    private static final float LOGIN_MUSIC_FADE_OUT_TIME    = 1;
    private static final int   LOGIN_OFFSET                 = 55;

    private static final int    WARNING_TEXT_GROUP_OFFSET   = 30;
    private static final int    WARNING_TEXT_GROUP_OFFSET_R = 40;
    private static final int    WARNING_GROUP_MINUS_BAR     = 50;
    private static final float  WARNING_BG_ALPHA            = 0.6f;
    private static final String WARNING_INFO_TEXT           = ApplicationTextManager.ApplicationTextsAssets.WARNING_USING_APP;

    public static final String LOGIN_SPEECH                 = MFX_PATH + "login.mp3";

    private TablexiaTextureManager textureManager;
    private String backgroundAsset = LoaderAssets.LOADER_BACKGROUND;

	private LoadingBar loadingBar;
    private LoadingStatusText loadingStatusText;

    private Table warningContent;

    private Image launchLogo;
	private Image loginText;

    private String currentLanguage;
	private boolean isDownloadingFile = false;

    public LoaderScreen() {
        this.textureManager = new TablexiaTextureManager(TablexiaAbstractFileManager.AssetsStorageType.INTERNAL);
    }

    @Override
    public boolean canBePaused() { return false; }

    @Override
    public boolean canResetState() {
        return false;
    }

    @Override
    protected String prepareScreenAtlasPath(String screenAssetsPath, String screenName) {
        // no atlas loading
        return null;
    }

    @Override
    protected String prepareScreenTextResourcesAssetName() {
        // no text loading
        return null;
    }

    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        currentLanguage = TablexiaSettings.getInstance().getLocale().getLanguage();

        prepareTextures();

        prepareBackground();
		prepareLoginText();
        prepareClockHands();
        prepareLaunchLogo();
        prepareLoadingBar();
        prepareLoadingStatusText();

        //subscribe after initialization to avoid NPE exceptions
        // on events happening before actors are created
        ApplicationBus.getInstance().subscribe(this);
    }

    ////////////////// HANDLERS

    @Handler
    public void onTablexiaAssetsManagerEvent(TablexiaAssetsManager.AssetsManagerEvent assetsManagerEvent) {
        switch (assetsManagerEvent.getEventType()) {
            case AssetCheckStarted:
                showLoadingBar(LOADING_BAR_FADE_IN_TIME);
                loadingBar.changeActivity(LoadingBar.LoadingBarActivity.IDLE);
                loadingStatusText.setStatusText(LoadingStatusText.StatusText.ASSETS_CHECK);
                showStatusText(STATUS_TEXT_FADE_IN_TIME);
                break;
            case ExtractingAssetsStarted:
                loadingStatusText.changeText(LoadingStatusText.StatusText.EXTRACTING_ASSETS, 1);
                loadingBar.changeActivity(LoadingBar.LoadingBarActivity.IDLE);
                break;
            case DownloadStarted:
                //Don't active the loading bar when downloading checksum file
                isDownloadingFile = true;

                loadingBar.changeActivity(LoadingBar.LoadingBarActivity.PROGRESS);

                //Shows loading bar if needed
                showLoadingBar(LOADING_BAR_FADE_IN_TIME);

                loadingStatusText.changeText(LoadingStatusText.StatusText.DOWNLOADING_ASSETS, 1);
                break;
            case DownloadProgress:
                if(isDownloadingFile)
                    loadingBar.setCurrentPercentage(assetsManagerEvent.getProgress());
                break;
            case DownloadFinished:
                if(isDownloadingFile) {
                    isDownloadingFile = false;
                    loadingBar.changeActivity(LoadingBar.LoadingBarActivity.IDLE);
                }
                break;
            case InconsistentConnection:
                break;
            case DownloadFailed:
            case LoadFinished:
                //Hide loadingBar and statusBar
                hideLoadingBar(LOADING_BAR_FADE_OUT_TIME);
                hideStatusText(STATUS_TEXT_FADE_OUT_TIME);
                hideWarningText(STATUS_TEXT_FADE_OUT_TIME);
                break;
        }
    }

    @Handler
    public void onMenuEvent(AbstractMenu.MenuEvent menuEvent) {
        if(menuEvent.getTargetMenu() == UserMenu.class) {
            if(menuEvent.getMenuEventType() == AbstractMenu.MenuEvent.MenuEventType.Open) {
                showLoginText();
            }
            else {
                hideLoginText();
            }
        }
    }


    ////////////////// PREPARING

    private void prepareTextures () {
		backgroundAsset = TablexiaSettings.getInstance().isWinterMode() ? LoaderAssets.LOADER_BACKGROUND_WINTER : LoaderAssets.LOADER_BACKGROUND;
		textureManager.loadTexture(backgroundAsset);

        textureManager.loadTexture(LoaderAssets.LOADER_SMALL_HAND);
        textureManager.loadTexture(LoaderAssets.LOADER_BIG_HAND);

        //Localized textures
        textureManager.loadTexture(LoaderAssets.LOADER_LAUNCH_LOGO + "_" + currentLanguage + ".png");
		textureManager.loadTexture(LoaderAssets.LOADER_LOGIN_TEXT + "_" + currentLanguage + ".png");

        textureManager.loadTexture(LoaderAssets.LOADER_BAR_BACKGROUND);
        textureManager.loadTexture(LoaderAssets.LOADER_BAR_FOREGROUND);
        textureManager.finishLoading();
    }

	private void prepareLoginText() {
		Texture texture = textureManager.getTexture(LoaderAssets.LOADER_LOGIN_TEXT + "_" + currentLanguage + ".png");
		texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

		loginText = new Image(texture);
		loginText.setSize(getSceneWidth() * LOGIN_TEXT_WIDTH, (getSceneWidth() * LOGIN_TEXT_WIDTH) * (texture.getHeight() / (float) texture.getWidth()));

		loginText.setPosition(getSceneWidth() / 2 - (loginText.getWidth() / 2 - LOGIN_OFFSET),
                getSceneInnerBottomY() + getSceneInnerHeight() / 2 - loginText.getHeight() / 2
        );

		loginText.addAction(Actions.alpha(0));
		getStage().addActor(loginText);
    }

    /**
     * Waits for LOGIN_TEXT_SHOW_DELAY seconds
     * and then fades in the text and plays login speech
     */
    private void showLoginText() {
        loginText.addAction(
            new ParallelAction(
                    Actions.fadeIn(LOGIN_TEXT_FADE_IN_TIME),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            Music music = getMusic(LOGIN_SPEECH);
                            if (music != null) music.play();
                        }
                    })
            ));
    }

    private void hideLoginText() {
        loginText.addAction(Actions.fadeOut(LOGIN_TEXT_FADE_OUT_TIME));

        Music music = getMusic(LOGIN_SPEECH);
        if (music != null && music.isPlaying())
            MusicUtil.fadeOut(music, LOGIN_MUSIC_FADE_OUT_TIME);
    }

    private void prepareBackground() {
        Texture texture = textureManager.getTexture(backgroundAsset);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        TablexiaNoBlendingImage background = new TablexiaNoBlendingImage(texture);
        setActorToFullScene(background);

        getStage().addActor(background);
    }

    private void prepareClockHands() {
        //Preparing small hand
        Texture smallHandTexture = textureManager.getTexture(LoaderAssets.LOADER_SMALL_HAND);
        smallHandTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        Image smallHand = new Image(smallHandTexture);

        smallHand.setPosition(CLOCK_HANDS_OFFSET_X + getSceneLeftX(), CLOCK_HANDS_OFFSET_Y + getSceneInnerBottomY());
        smallHand.setSize(CLOCK_HANDS_WIDTH, CLOCK_HANDS_HEIGHT);
        smallHand.setOrigin(smallHand.getWidth() / 2, CLOCK_HANDS_ORIGIN_OFFSET_Y);

        getStage().addActor(smallHand);


        //Preparing big hand
        Texture bigHandTexture = textureManager.getTexture(LoaderAssets.LOADER_BIG_HAND);
        bigHandTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        Image bigHand = new Image(bigHandTexture);

        bigHand.setPosition(CLOCK_HANDS_OFFSET_X + getSceneLeftX(), CLOCK_HANDS_OFFSET_Y + getSceneInnerBottomY());
        bigHand.setSize(CLOCK_HANDS_WIDTH, CLOCK_HANDS_HEIGHT);
        bigHand.setOrigin(bigHand.getWidth() / 2, CLOCK_HANDS_ORIGIN_OFFSET_Y);

        getStage().addActor(bigHand);

        //Make 'em both rotate
        smallHand.addAction(Actions.forever(Actions.rotateBy(-360, CLOCK_BIG_HAND_SPEED)));
        bigHand.addAction(Actions.forever(Actions.rotateBy(-360, CLOCK_BIG_HAND_SPEED / 12.0f)));
    }

    public void prepareLaunchLogo() {
        Texture launchLogoTexture = textureManager.getTexture(LoaderAssets.LOADER_LAUNCH_LOGO + "_" + currentLanguage + ".png");
        launchLogoTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        launchLogo = new Image(launchLogoTexture);

        int width  = (int) (getSceneWidth() * LAUNCH_LOGO_WIDTH);
        int height = (int) (width * (launchLogoTexture.getHeight() / (float)(launchLogoTexture.getWidth())));

        launchLogo.setSize(width, height);
        launchLogo.setPosition(	getSceneWidth() / 2 - (width / 2 - LAUNCH_LOGO_OFFSET),
								getSceneInnerTopY() - height - (getSceneInnerHeight() * LAUNCH_LOGO_Y_TOP_OFFSET));

        getStage().addActor(launchLogo);
    }

    @Override
    public void backButtonPressed() {
        if (Tablexia.hasQRCodeScanner() && Tablexia.getQRCodeScanner().isCameraPreviewActive()) {
            Tablexia.getQRCodeScanner().stopCameraPreview();
            ApplicationBus.getInstance().publishAsync(new QRCodeActiveEvent(false));

        }else {
            ApplicationBus.getInstance().publishAsync(new TablexiaAssetsManager.ApplicationTerminatedEvent());
        }
    }

    @Override
    public void pause() {
        super.pause();
        if (Tablexia.hasQRCodeScanner() && Tablexia.getQRCodeScanner().isCameraPreviewActive()) {
            Tablexia.getQRCodeScanner().stopCameraPreview();
            ApplicationBus.getInstance().publishAsync(new QRCodeActiveEvent(false));
        }
    }

    public void prepareLoadingBar() {
        //Skin for LoadingBar
        LoadingBar.LoadingBarSkin skin = new LoadingBar.LoadingBarSkin(
                textureManager.getTexture(LoaderAssets.LOADER_BAR_FOREGROUND),
                textureManager.getTexture(LoaderAssets.LOADER_BAR_BACKGROUND),
                LOADING_BAR_SHADOW_COLOR
        );

        //Creates loading bar
        this.loadingBar = new LoadingBar(0, skin);

        //Set Size
        loadingBar.setWidth(LOADING_BAR_WIDTH * getSceneWidth());
        loadingBar.setHeight(loadingBar.getWidth() * (skin.getForegroundImage().getHeight() / (float) (skin.getForegroundImage().getWidth())));

        //Set position
        loadingBar.setPosition(getSceneWidth() / 2 - (loadingBar.getWidth() / 2 - LAUNCH_LOGO_OFFSET), LOADING_BAR_OFFSET_Y * getSceneInnerHeight() + getSceneInnerBottomY());
        loadingBar.setVisible(false);
		getStage().addActor(loadingBar);
    }

    public void prepareLoadingStatusText() {
        loadingStatusText = new LoadingStatusText();
        loadingStatusText.setBounds(
            loadingBar.getX(),
            loadingBar.getY() - LOADING_STATUS_TOP_OFFSET,
            loadingBar.getWidth(),
            loadingBar.getHeight()
        );
        getStage().addActor(loadingStatusText);
    }

	private void showLoadingBar(float time) {
		if(!loadingBar.isVisible()) {
            if (UserDAO.selectLastUser() == null) addWarningInfo();
            loadingBar.addAction(Actions.alpha(0));
			loadingBar.setVisible(true);
			loadingBar.addAction(Actions.fadeIn(time));
		}
	}

    private void addWarningInfo() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                //Creates warning about if<16, contact parents
                warningContent = new Table(){
                    @Override
                    protected void drawBackground (Batch batch, float parentAlpha, float x, float y) {
                        Color batchColor = batch.getColor();
                        batchColor.a *= WARNING_BG_ALPHA;
                        batch.setColor(batchColor);
                        super.drawBackground(batch, batchColor.a, x, y);
                    }
                };
                float wholeWarningWidth = loadingBar.getWidth() - WARNING_GROUP_MINUS_BAR;
                float textLabelWidth = wholeWarningWidth - WARNING_TEXT_GROUP_OFFSET_R;

                TablexiaLabel warningTextLabel = new TablexiaLabel(ApplicationTextManager.getInstance().getText(WARNING_INFO_TEXT), new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_16, Color.WHITE), true);
                warningTextLabel.setAlignment(Align.center);
                warningTextLabel.setWrap(true);
                warningTextLabel.pack();
                warningTextLabel.setWidth(textLabelWidth);
                warningTextLabel.pack();
                warningTextLabel.setWidth(textLabelWidth);
                warningTextLabel.setY(WARNING_TEXT_GROUP_OFFSET / 2);
                warningTextLabel.setX(WARNING_TEXT_GROUP_OFFSET / 2);

                NinePatchDrawable background = new NinePatchDrawable(ApplicationInternalTextureManager.getInstance().getPatch(ApplicationInternalTextureManager.InternalNinePatch.WARNING_DIALOG_BG_PATCH));
                warningContent.setBackground(background);
                warningContent.setSize(wholeWarningWidth, warningTextLabel.getHeight() + WARNING_TEXT_GROUP_OFFSET);
                warningContent.setPosition(getSceneWidth() / 2 - loadingBar.getWidth() / 2, loadingBar.getY() - warningContent.getHeight());
                warningContent.add(warningTextLabel).width(warningTextLabel.getWidth());;

                getStage().addActor(warningContent);
            }
        });
    }

	private void hideLoadingBar(float time) {
		if(loadingBar.isVisible()) {
			loadingBar.addAction(new SequenceAction(Actions.fadeOut(time), Actions.visible(false)));
		}
	}

	private void showStatusText(float time) {
        if(loadingStatusText.isVisible())
            return;

        loadingStatusText.setVisible(true);
        loadingStatusText.addAction(Actions.fadeIn(time));
    }

	private void hideStatusText(float time) {
        if(!loadingStatusText.isVisible())
            return;

        loadingStatusText.addAction(new SequenceAction(Actions.fadeOut(time), Actions.visible(false)));
    }

    private void hideWarningText(float time) {
        if(warningContent != null && warningContent.isVisible()) {
            warningContent.addAction(new SequenceAction(Actions.fadeOut(time), Actions.visible(false)));
        }
    }

    @Override
    public boolean hasSoftBackButton() {
        return false;
    }

    @Override
    protected void screenDisposed() {
        super.screenDisposed();
        if(loadingBar!=null)loadingBar.dispose();
    }
}