/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.loader;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by Drahomir Karchnak on 10/12/15.
 */
public class LoadingStatusText extends TablexiaLabel {
    /**
     * Defines all the possible lines
     */
    public enum StatusText {
        NONE(""),
        ASSETS_CHECK(LoaderAssets.LOADER_STATUS_CHECK),
        DOWNLOADING_ASSETS(LoaderAssets.LOADER_STATUS_DOWNLOAD),
        EXTRACTING_ASSETS(LoaderAssets.LOADER_STATUS_ASSETS),
        SYNCHRONIZING(LoaderAssets.LOADER_STATUS_SYNC);

        private String textKey;

        StatusText(String textKey) {
            this.textKey = textKey;
        }

        public String getTextKey() {
            return textKey;
        }
    }

    private static final ApplicationFontManager.FontType     STATUS_TEXT_FONT_TYPE  = ApplicationFontManager.FontType.REGULAR_20;
    private static final Color                               STATUS_TEXT_FONT_COLOR = Color.WHITE;

    public LoadingStatusText() {
        super("", new TablexiaLabelStyle(STATUS_TEXT_FONT_TYPE, STATUS_TEXT_FONT_COLOR));
        setAlignment(Align.center);
    }

    public synchronized void setStatusText(StatusText text) {
        if(text == StatusText.NONE)
            setText("");
        else {
            setText(ApplicationTextManager.getInstance().getText(text.getTextKey()));
        }
    }

    /**
     * Changes current status text with fade in/out animation
     * @param text
     * @param time
     */
    public void changeText(final StatusText text, float time) {
        addAction(new SequenceAction(
                Actions.fadeOut(time / 2),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        setStatusText(text);
                    }
                }),
                Actions.fadeIn(time / 2)));
    }
}