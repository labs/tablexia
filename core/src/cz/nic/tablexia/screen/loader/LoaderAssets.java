/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.loader;

/**
 * Created by Drahomir Karchnak on 08/12/15.
 */
public class LoaderAssets {
	/*
	 * Inner Assets
	 */
    public static final String BASE_PATH = "gfx/";

    public static final String LOADER_BACKGROUND   		= BASE_PATH + "screen_loader_background.jpg";
    public static final String LOADER_BACKGROUND_WINTER = BASE_PATH + "screen_loader_background_winter.jpg";

    public static final String LOADER_SMALL_HAND   		= BASE_PATH + "screen_loader_smallhand.png";
    public static final String LOADER_BIG_HAND     		= BASE_PATH + "screen_loader_bighand.png";

    public static final String LOADER_LAUNCH_LOGO	 = BASE_PATH + "launch";
	public static final String LOADER_LOGIN_TEXT	 = BASE_PATH + "login";

    public static final String LOADER_BAR_BACKGROUND = BASE_PATH + "bar_bg.png";
    public static final String LOADER_BAR_FOREGROUND = BASE_PATH + "bar_fg.png";

    public static final String LOADER_STATUS_CHECK      = "loading_status_check";
    public static final String LOADER_STATUS_DOWNLOAD   = "loading_status_download";
    public static final String LOADER_STATUS_ASSETS     = "loading_status_assets";
    public static final String LOADER_STATUS_SYNC       = "loading_status_sync";

}
