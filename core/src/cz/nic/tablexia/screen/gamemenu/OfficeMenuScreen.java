/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.gamemenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.Map;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaTextureManager;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.model.screen.ScreenDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaScreen;
import cz.nic.tablexia.screen.gamemenu.actors.WallOfGames;
import cz.nic.tablexia.screen.halloffame.HallOfFameScreen;
import cz.nic.tablexia.screen.loader.LoaderScreen;
import cz.nic.tablexia.screen.profile.ProfileScreen;
import cz.nic.tablexia.screen.statistics.StatisticsScreen;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Utility;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

/**
 * Created by lhoracek
 */
public class OfficeMenuScreen extends AbstractTablexiaScreen<int[][]> {
    private static final float HELP_OVERLAY_DIMMER_ALPHA = 0.5f;
    private static final Color HELP_OVERLAY_DIMMER_COLOR = new Color(0, 0, 0, HELP_OVERLAY_DIMMER_ALPHA);

    private static final ApplicationFontManager.FontType    FONT_TYPE       = ApplicationFontManager.FontType.BOLD_14;
    private static final Color                              FONT_COLOR      = Color.BLACK;

    public static final String TOGGLE_HELP_OVERLAY_BUTTON_NAME  = "toggle help overlay button";
    public static final String ENCYCLOPEDIA_IMAGE_NAME          = "encyclopedia image name";
    public static final String STATISTICS_IMAGE_NAME            = "statistics image name";
    public static final String DOOR_TO_HALL_OF_FAME_NAME        = "door to hall of fame";
    public static final String PROFILE_TABLE_NAME               = "profile table name";
    public static final String WALL_OF_GAME_IMAGE_NAME          = "wall of game image name";
    public static final String EVENT_HELP_SHOWN                 = "event help shown";
    public static final String EVENT_HELP_HIDDEN                = "event help hidden";

    enum OfficeImageDefinition {

        ENCYCLOPEDIA(           OfficeMenuAssets.ENCYCLOPEDIA_PRESSED,          25f,    248f,   147,    254),
        ENCYCLOPEDIA_RANKED(    OfficeMenuAssets.ENCYCLOPEDIA_RANKED_PRESSED,   807f,   318f,   122,    393),
        STATISTICS(             OfficeMenuAssets.STATISTICS_PRESSED,            227f,   408f,   131,    234),
        STATISTICS_RANKED(      OfficeMenuAssets.STATISTICS_RANKED_PRESSED,     250f,   408f,   121,    191),
        HALL_OF_FAME_DOOR(      OfficeMenuAssets.HALL_OF_FAME_PRESSED,          351f,   465f,   144,    224),
        HALL_OF_FAME_DOOR_RANKED(OfficeMenuAssets.HALL_OF_FAME_RANKED_PRESSED,  627f,   436f,   136,    302),
        DESK(                   OfficeMenuAssets.DESK,                          348f,   150f,   423,    326),
        DESK_RANKED(            OfficeMenuAssets.DESK_RANKED,                   348f,   170f,   425,    363),
        DESK_PRESSED(           OfficeMenuAssets.DESK_PRESSED,                  348f,   150f,   423,    326),
        DESK_RANKED_PRESSED(    OfficeMenuAssets.DESK_RANKED_PRESSED,           348f,   170f,   425,    363),
        WALL_OF_GAMES(          OfficeMenuAssets.WALL_OF_GAMES_PRESSED,         570f,   538f,   226,    172),
        WALL_OF_GAMES_RANKED(   OfficeMenuAssets.WALL_OF_GAMES_RANKED_PRESSED,  348f,   577f,   226,    172);

        private final String assetName;
        private final float posX;
        private final float posY;
        private final float width;
        private final float height;

        OfficeImageDefinition(String assetName, float posX, float posY, float width, float height) {
            this.assetName = assetName;
            this.posX = posX;
            this.posY = posY;
            this.width = width;
            this.height = height;
        }

        public String getAssetName() {
            return assetName;
        }

        public float getPosX() {
            return posX;
        }

        public float getPosY() {
            return posY;
        }

        public float getWidth() {
            return width;
        }

        public float getHeight() {
            return height;
        }
    }

    enum HelpBubbleDefinition{

        TROPHIES(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.OFFICE_HELP_TROPHIES), OfficeMenuAssets.BUBBLE_TROPHIES,
                450, 400, 120, 77, 30),
        TROPHIES_RANKED(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.OFFICE_HELP_TROPHIES), OfficeMenuAssets.BUBBLE_TROPHIES,
                740, 400, 120, 77, 30),
        ENCYCLOPEDIA(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.MAINMENU_ENCYCLOPEDIA), OfficeMenuAssets.BUBBLE_ENCYCLOPEDIA,
                120, 140, 120, 79, 40),
        ENCYCLOPEDIA_RANKED(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.MAINMENU_ENCYCLOPEDIA), OfficeMenuAssets.BUBBLE_STATISTICS,
                700, 230, 120, 79, 40),
        STATISTICS(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.MAINMENU_STATISTICS), OfficeMenuAssets.BUBBLE_STATISTICS,
                170, 300, 100, 76, 30),
        STATISTICS_RANKED(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.MAINMENU_STATISTICS), OfficeMenuAssets.BUBBLE_STATISTICS,
                170, 300, 100, 76, 30),
        GAMES(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.MAINMENU_GAMES), OfficeMenuAssets.BUBBLE_GAMES,
                530, 330, 80, 66, 30),
        GAMES_RANKED(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.MAINMENU_GAMES), OfficeMenuAssets.BUBBLE_GAMES,
                300, 440, 80, 66, 30),
        PROFILE(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.MAINMENU_PROFILE), OfficeMenuAssets.BUBBLE_DETECTIVES_CARD,
                520, 160, 105, 110, 50),
        PROFILE_RANKED(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.MAINMENU_PROFILE), OfficeMenuAssets.BUBBLE_DETECTIVES_CARD,
                520, 160, 105, 110, 50);

        private String text;
        private String asset;
        private int posX;
        private int posY;
        private int width;
        private int height;
        private int bubbleArray;

        HelpBubbleDefinition(String text, String asset, int posX, int posY, int width, int height, int bubbleArray) {
            this.text = text;
            this.asset = asset;
            this.posX = posX;
            this.posY = posY;
            this.width = width;
            this.height = height;
            this.bubbleArray = bubbleArray;
        }

        public String getText() {
            return text;
        }

        public String getAsset() {
            return asset;
        }

        public int getPosX() {
            return posX;
        }

        public int getPosY() {
            return posY;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public int getBubbleArray() {
            return bubbleArray;
        }
    }


    //If distance of touch up and touch down event is greater than this, click wont be accepted!
    private static final float MAX_DISTANCE_FOR_CLICK    = 14;

    public static final Color HALLOFFAME_COLOR           = Color.GREEN;
    public static final Color STATISTICS_COLOR           = Color.RED;
    public static final Color ENCYCLOPEDIA_COLOR         = Color.BLACK;
    public static final Color PROFILE_COLOR              = Color.BLUE;
    public static final Color WALL_OF_GAMES_COLOR        = Color.ORANGE;

    //Time for help overlay to fadein and fadeout
    private static final float HELP_FADE_TIME            = 0.25f;

    //Help Button
    private static final float HELP_BUTTON_OFFSET        = 0.04f;
    private static final int   HELP_BUTTON_SIZE          = 70;

    private WallOfGames.WallOfGamesResourcesRetriever wallOfGamesResourcesRetriever;

    private Music introSpeechMusic;

    private Image vignette;


    private Group wrapperGroup;
    private Group screenGroup;

    private Group helpGroup;
    private TablexiaButton helpButton;

    private Group helpOverlayGroup;

    //Stack of all office related actors
    private Group officeGroup;

    //Instance of current wall of games, null if there's not any...
    private WallOfGames wallOfGames;

    //Images for office related actors
    private Image hallOfFamePressed;
    private Image statisticsPressed;
    private Image encyclopediaPressed;
    private Image wallOfGamesPressed;

    private Image profilePressed;
    private Image profileUnpressed;

    //Texture region of clickable map
    private TablexiaTextureManager textureManager;
    private Texture clickmap;

    private String clickMapName;

    private int clickMapWidth;
    private int clickMapHeight;

    public OfficeMenuScreen() {
        initializeWallOfGamesResourcesRetriever();
    }

    private void initializeWallOfGamesResourcesRetriever() {
        this.wallOfGamesResourcesRetriever = new WallOfGames.WallOfGamesResourcesRetriever() {
            @Override
            public TextureRegion getTextureRegion(String key) {
                return getScreenTextureRegion(key);
            }

            @Override
            public NinePatch getNinePatch(String key) {
                return getScreenPatch(key);
            }

            @Override
            public TextureRegion getColorTexture(Color color) {
                return getColorTextureRegion(color);
            }
        };
    }

    @Override
    protected String prepareScreenTextResourcesAssetName() {
        return null;
    }

    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        boolean isFirstLogIn = ScreenDAO.getScreenViewCount(getSelectedUser().getId(), getClass().getName()) == 0;

        wrapperGroup = new Group();
        wrapperGroup.setBounds(getSceneLeftX(), getSceneOuterBottomY(), getSceneWidth(), getSceneOuterHeight());

        screenGroup = new Group();
        screenGroup.setSize(getSceneWidth(), getSceneInnerHeight());
        screenGroup.setPosition(getSceneLeftX(), -getSceneOuterBottomY());

        wrapperGroup.addActor(screenGroup);

        // /game menu pages list, which will be later used for turning on sounds after first scroll
        vignette = new Image(getScreenPatch(OfficeMenuAssets.VIGNETTE));
        vignette.setTouchable(Touchable.disabled);

        prepareVignetteAndPagerSize();
        getStage().addActor(vignette);

        // show intro scroll and play intro speech only first time user visits OfficeMenuScreen
        introSpeechMusic = null;
        if(isFirstLogIn) {
            introSpeechMusic = getMusic(OfficeMenuAssets.INTRO_SPEECH);
            introSpeechMusic.play();

            introSpeechMusic.setOnCompletionListener(new Music.OnCompletionListener() {
                @Override
                public void onCompletion(Music music) {
                    music.dispose();
                }
            });
        }

        //OFFICE STUFF

        prepareOfficeGroup();

        //Init
        prepareHelpOverlay();
        prepareInputListener();

        //Have user seen the help overlay ?
        if(TablexiaSettings.getInstance().getSelectedUser().isHelp()) {
            TablexiaSettings.getInstance().getSelectedUser().setHelp(false);
            UserDAO.updateUser(TablexiaSettings.getInstance().getSelectedUser());
            toggleHelpOverlay();
        }

        getStage().addActor(wrapperGroup);
    }

    /**
     *  Creates, prepares and adds InputListener to OfficeMenuPage
     */
    public void prepareInputListener() {
        wrapperGroup.addListener(new ClickListenerWithSound() {
            boolean playClickSound = false;
            float lastTouchDownX, lastTouchDownY;

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;

                super.touchDown(event, x, y, pointer, button);

                lastTouchDownX = x;
                lastTouchDownY = y;

                if(wallOfGames != null && wallOfGames.isShown()) {
                    wallOfGames.hide();
                }

                //Don't do anything if help overlay is still visible
                if (helpOverlayGroup.isVisible()) {
                    toggleHelpOverlay();
                    return false;
                }

                Color color = getTouchedColor(x, y);

                if (color.equals(ENCYCLOPEDIA_COLOR)) {
                    encyclopediaPressed.setVisible(true);
                } else if (color.equals(STATISTICS_COLOR)) {
                    statisticsPressed.setVisible(true);
                } else if (color.equals(HALLOFFAME_COLOR)) {
                    hallOfFamePressed.setVisible(true);
                } else if (color.equals(PROFILE_COLOR)) {
                    profilePressed.setVisible(true);
                    profileUnpressed.setVisible(false);
                } else if(color.equals(WALL_OF_GAMES_COLOR)) {
                    wallOfGamesPressed.setVisible(true);
                }
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                hideAllActions();
            }

            @Override
            public void onClick(InputEvent event, float x, float y) {
                playClickSound = true;

                //Was it click or did user just touched up on wrong icon
                if(Vector2.dst(x, y, lastTouchDownX, lastTouchDownY) > MAX_DISTANCE_FOR_CLICK) {
                    playClickSound = false;
                    return;
                }

                Color color = getTouchedColor(x, y);

                if (color.equals(ENCYCLOPEDIA_COLOR)) {
                    ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(EncyclopediaScreen.class, TablexiaApplication.ScreenTransaction.FADE));
                } else if (color.equals(STATISTICS_COLOR)) {
                    ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(StatisticsScreen.class, TablexiaApplication.ScreenTransaction.FADE));
                } else if (color.equals(HALLOFFAME_COLOR)) {
                    ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(HallOfFameScreen.class, TablexiaApplication.ScreenTransaction.FADE));
                } else if (color.equals(PROFILE_COLOR)) {
                    ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(ProfileScreen.class, TablexiaApplication.ScreenTransaction.FADE));
                } else if (color.equals(WALL_OF_GAMES_COLOR)){
                    showWallOfGames();
                }
                else {
                    playClickSound = false;
                }
            }

            @Override
            protected void playButtonSound() {
                if(playClickSound)
                    super.playButtonSound();
            }
        });
    }

    /**
     * Initializes Toggle Help Button
     */
    private TablexiaButton createToggleHelpButton() {
        final TablexiaButton toggleHelpButton = new TablexiaButton(null, false,
                getScreenTextureRegion(OfficeMenuAssets.HELP_BUTTON_RELEASE),
                getScreenTextureRegion(OfficeMenuAssets.HELP_BUTTON_PRESSED),
                null,
                null)

                .setButtonSize(HELP_BUTTON_SIZE, HELP_BUTTON_SIZE)
                .setInputListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        hideAllActions();
                        toggleHelpOverlay();
                    }
                });

        return toggleHelpButton;
    }

    private void prepareHelpOverlay() {
        helpGroup = new Group();
        helpGroup.setBounds(getSceneLeftX(), getSceneInnerBottomY(), getSceneWidth(), getSceneInnerHeight());

        boolean ranked = TablexiaSettings.isUserHighestRank();

        helpOverlayGroup = new Group();
        helpOverlayGroup.setBounds(helpGroup.getX(), helpGroup.getY(), helpGroup.getWidth(), helpGroup.getHeight());

        Image helpOverlayDimmer = new Image(getColorTextureRegion(HELP_OVERLAY_DIMMER_COLOR));
        helpOverlayDimmer.setSize(getSceneWidth(), getSceneOuterHeight());
        helpOverlayDimmer.setPosition(getSceneLeftX(), getSceneOuterBottomY());
        helpOverlayGroup.addActor(helpOverlayDimmer);

        createBubble(ranked ? HelpBubbleDefinition.TROPHIES_RANKED : HelpBubbleDefinition.TROPHIES, helpOverlayGroup);
        createBubble(ranked ? HelpBubbleDefinition.ENCYCLOPEDIA_RANKED : HelpBubbleDefinition.ENCYCLOPEDIA, helpOverlayGroup);
        createBubble(ranked ? HelpBubbleDefinition.STATISTICS_RANKED : HelpBubbleDefinition.STATISTICS, helpOverlayGroup);
        createBubble(ranked ? HelpBubbleDefinition.GAMES_RANKED : HelpBubbleDefinition.GAMES, helpOverlayGroup);
        createBubble(ranked ? HelpBubbleDefinition.PROFILE_RANKED : HelpBubbleDefinition.PROFILE, helpOverlayGroup);

        //Start invisible
        helpOverlayGroup.setVisible(false);
        helpOverlayGroup.addAction(Actions.alpha(0));

        helpGroup.addActor(helpOverlayGroup);

        helpButton = createToggleHelpButton();
        positionToggleHelpButton();
        helpGroup.addActor(helpButton);
        helpButton.setName(TOGGLE_HELP_OVERLAY_BUTTON_NAME);

        screenGroup.addActor(helpGroup);
    }

    private void createBubble(HelpBubbleDefinition helpBubbleDefinition, Group helpOverlayGroup){
        createBubble(helpBubbleDefinition.getText(), helpBubbleDefinition.getAsset(), helpOverlayGroup, helpBubbleDefinition.getPosX(), helpBubbleDefinition.getPosY(), helpBubbleDefinition.getWidth(), helpBubbleDefinition.getHeight(), helpBubbleDefinition.getBubbleArray());
    }

    private void createBubble(String text, String bubbleImage, Group helpOverlayGroup, int bubbleX, int bubbleY, int bubbleWidth, int bubbleHeight, int bubbleArray) {
        Image bubble = new Image(getScreenTextureRegion(bubbleImage));
        bubble.setPosition(bubbleX, bubbleY);
        bubble.setSize(bubbleWidth,bubbleHeight);
        TablexiaLabel bubbleText = new TablexiaLabel(text, new TablexiaLabel.TablexiaLabelStyle(FONT_TYPE, FONT_COLOR));
        bubbleText.setPosition(bubble.getX() + bubble.getWidth()/2 - bubbleText.getWidth()/2, bubble.getY() + (bubble.getHeight() + bubbleArray)/2 - bubbleText.getHeight()/2);
        helpOverlayGroup.addActor(bubble);
        helpOverlayGroup.addActor(bubbleText);
    }

    private void positionToggleHelpButton() {
        if(helpButton == null) return;
        helpButton.setPosition( //Bottom Right Corner of a viewport with an offset
                getViewportRightX() - helpButton.getWidth() - (HELP_BUTTON_OFFSET * getViewportWidth()),
                getViewportBottomY() + (HELP_BUTTON_OFFSET * getViewportWidth())
        );
    }

    private void prepareOfficeGroup() {
        officeGroup = new Group();

        officeGroup.setSize(getSceneWidth(), getSceneOuterHeight());
        officeGroup.setPosition(getSceneLeftX(), getSceneOuterBottomY());

        boolean rankAchieved = TablexiaSettings.isUserHighestRank();

        Image officeImage = new TablexiaNoBlendingImage(getScreenTextureRegion(rankAchieved ? OfficeMenuAssets.OFFICE_RANKED : OfficeMenuAssets.OFFICE));
        officeImage.setSize(officeGroup.getWidth(), officeGroup.getHeight());

        officeGroup.addActor(officeImage);
        officeGroup.addActor(encyclopediaPressed = createImage(rankAchieved ? OfficeImageDefinition.ENCYCLOPEDIA_RANKED : OfficeImageDefinition.ENCYCLOPEDIA));
        officeGroup.addActor(statisticsPressed = createImage(rankAchieved ? OfficeImageDefinition.STATISTICS_RANKED : OfficeImageDefinition.STATISTICS));
        officeGroup.addActor(hallOfFamePressed = createImage(rankAchieved ? OfficeImageDefinition.HALL_OF_FAME_DOOR_RANKED : OfficeImageDefinition.HALL_OF_FAME_DOOR));
        officeGroup.addActor(profileUnpressed = createImage(rankAchieved ? OfficeImageDefinition.DESK_RANKED : OfficeImageDefinition.DESK));
        officeGroup.addActor(profilePressed = createImage(rankAchieved ? OfficeImageDefinition.DESK_RANKED_PRESSED : OfficeImageDefinition.DESK_PRESSED));
        officeGroup.addActor(wallOfGamesPressed = createImage(rankAchieved ? OfficeImageDefinition.WALL_OF_GAMES_RANKED : OfficeImageDefinition.WALL_OF_GAMES));

        screenGroup.addActor(officeGroup);

        encyclopediaPressed.setName(ENCYCLOPEDIA_IMAGE_NAME);
        statisticsPressed.setName(STATISTICS_IMAGE_NAME);
        hallOfFamePressed.setName(DOOR_TO_HALL_OF_FAME_NAME);
        profileUnpressed.setName(PROFILE_TABLE_NAME);
        wallOfGamesPressed.setName(WALL_OF_GAME_IMAGE_NAME);

        hideAllActions();
    }

    private void hideAllActions() {
        profileUnpressed.setVisible(true);
        profilePressed.setVisible(false);
        encyclopediaPressed.setVisible(false);
        statisticsPressed.setVisible(false);
        hallOfFamePressed.setVisible(false);
        wallOfGamesPressed.setVisible(false);
    }

    /**
     * Helper method for creating image instances
     *
     * @param texture texture name
     * @return return image
     */
    private Image createImage(String texture, Vector2 position) {
        TextureRegion tr = getScreenTextureRegion(texture);

        Image result = new Image(tr);
        result.setPosition(position.x * getSceneWidth(), position.y * getSceneOuterHeight());
        return result;
    }

    private Image createImage(OfficeImageDefinition officeImageDefinition){
        return createImage(officeImageDefinition.getAssetName(), new Vector2(officeImageDefinition.getPosX()/1000, officeImageDefinition.getPosY()/1000),
                officeImageDefinition.getWidth(), officeImageDefinition.getHeight());
    }


    private Image createImage(String texture, Vector2 position, float width, float height) {
        Image result = createImage(texture, position);
        result.setSize(width, height);
        return result;
    }

    /**
     * Gets color from clickMap according to coords x and y
     * @param x X coord
     * @param y Y coord
     * @return color clicked, Color.BLACK if anything went wrong
     */
    private Color getTouchedColor(float x, float y) {
        int clickX = (int) (x / getSceneWidth() * clickMapWidth);
        int clickY = clickMapHeight - (int) (y / getSceneOuterHeight() * clickMapHeight);

        if (clickX >= getData().length ||
                clickY >= getData()[0].length ||
                clickX < 0 || clickY < 0)
        {
            return Color.BLACK;
        }

        return new Color(getData()[clickX][clickY]);
    }

    /**
     * Toggles (FadeIn/FadeOut) help overlay
     */
    private void toggleHelpOverlay() {
        if(helpOverlayGroup.isVisible()) hideHelpOverlay();
        else showHelpOverlay();
    }

    private void hideHelpOverlay() {
        helpButton.setChecked(false);
        helpOverlayGroup.addAction( new SequenceAction(
                Actions.fadeOut(HELP_FADE_TIME),
                Actions.visible(false),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_HELP_HIDDEN);
                    }
                }))
        );
    }

    private void showHelpOverlay() {
        helpButton.setChecked(true);
        helpOverlayGroup.addAction( new SequenceAction(
                Actions.visible(true),
                Actions.fadeIn(HELP_FADE_TIME),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_HELP_SHOWN);
                    }
                })
        ));
    }

    private void showWallOfGames(){
        //Create one if doesn't exist
        if(wallOfGames == null) wallOfGames = WallOfGames.createWallOfGames(wallOfGamesResourcesRetriever, getStage(), getSceneWidth(), getSceneInnerHeight());
        wallOfGames.show();
    }

    public void prepareVignetteAndPagerSize() {
        vignette.setBounds(getViewportLeftX(), getViewportBottomY(), getViewportWidth(), getViewportHeight());
    }

    @Override
    protected void screenResized(int width, int height) {
        super.screenResized(width, height);
        prepareVignetteAndPagerSize();
        positionToggleHelpButton();
    }

    @Override
    protected int[][] prepareScreenData(Map<String, String> screenState) {
        final Object loaderLock = new Object();
        final String suffix = TablexiaSettings.isUserHighestRank() ? "_ranked" : "";
        clickMapName = prepareScreenAssetsPath(prepareScreenName())+ "excluded/clickmap" + suffix + ".png";
        synchronized (loaderLock) {

            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    synchronized (loaderLock) {
                        tryToDisposeTextureManager();
                        textureManager = new TablexiaTextureManager(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL);
                        textureManager.loadTexture(clickMapName);
                        textureManager.finishLoading();
                        loaderLock.notify();
                    }
                }
            });

            try {
                loaderLock.wait();
            } catch (InterruptedException e) {
                Log.err(getClass(), "Error while waiting to texture loader!", e);
            }
        }

        clickmap = textureManager.getTexture(clickMapName);
        if (!clickmap.getTextureData().isPrepared()) {
            clickmap.getTextureData().prepare();
        }
        clickMapWidth = clickmap.getWidth();
        clickMapHeight = clickmap.getHeight();
        return Utility.createColorMap(clickmap);
    }

    private void tryToDisposeTextureManager() {
        if (textureManager != null) {
            textureManager.dispose();
            textureManager = null;
        }
    }

    @Override
    protected void screenDisposed() {
        tryToDisposeTextureManager();
        super.screenDisposed();
    }

    @Override
    public void backButtonPressed() {
        if(helpOverlayGroup.isVisible()) {
            toggleHelpOverlay();
            return;
        }
        if(wallOfGames != null && wallOfGames.isShown()) {
            wallOfGames.hide();
            return;
        }

        //Needs to be run in OpenGL context thread...
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                TablexiaSettings.getInstance().changeUser(null, new TablexiaSettings.LogoutDialogListener() {
                    @Override
                    public void onLogoutAccepted() {
                        ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(LoaderScreen.class, TablexiaApplication.ScreenTransaction.FADE));
                    }

                    @Override
                    public void onLogoutCanceled() {}
                });
            }
        });
    }
}