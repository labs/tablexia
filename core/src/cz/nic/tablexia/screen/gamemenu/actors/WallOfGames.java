/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.gamemenu.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.screen.gamemenu.OfficeMenuAssets;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaProgressBar;

import static cz.nic.tablexia.screen.AbstractTablexiaScreen.triggerScenarioStepEvent;

/**
 * Created by Vitaliy Vashchenko on 24.8.16.
 */
public class WallOfGames extends Group {
    public static final  String                              WALL_OF_GAMES_READY        = "wall of games ready";
    private static final ApplicationFontManager.FontType     GAME_ICON_LABEL_FONT       = ApplicationFontManager.FontType.BOLD_18;
    private static final Color                               GAME_ICON_LABEL_FONT_COLOR = Color.WHITE;

    private static final float                               GAME_ICON_LABEL_BACKGROUND_ALPHA = 0.6f;
    private static final Color                               GAME_ICON_LABEL_BACKGROUND_COLOR = new Color(0, 0, 0, GAME_ICON_LABEL_BACKGROUND_ALPHA);

    private static final float SHOW_HIDE_FADE_DURATION          = 1 / 4f;
    private static final float RELAVITE_MAX_ICON_HEIGHT         = 0.3f;
    private static final float RELATIVE_HEIGHT                  = 0.9f;
    private static final float BUTTONS_GROUP_RELATIVE_WIDTH     = 0.58f;
    private static final float BUTTONS_GROUP_RELATIVE_HEIGHT    = 0.9f;
    private static final float BUTTONS_GROUP_RELATIVE_X         = 0.05f;
    private static final float BUTTONS_GROUP_RELATIVE_Y         = 0.05f;
    private static final float BUTTON_LABEL_BACKGROUND_BORDER_X = 5f;
    private static final float BUTTON_LABEL_BACKGROUND_BORDER_Y = 1f;
    private static final int   PROGRESS_BAR_TOP_PADDING         = 3;
    private static final int   LABEL_TOP_PADDING                = 10;

    private float                                stageWidth, stageHeight;
    private final WallOfGamesResourcesRetriever  resourcesRetriever;
    private       Stage                          stage;
    private       Group                          buttonsGroup, backgroundLayer, underneathLayer;

    private boolean shown = false;

    /**
     * Is used to get resources when creating wall of games
     */
    public interface WallOfGamesResourcesRetriever {
        TextureRegion getTextureRegion(String key);
        NinePatch getNinePatch(String key);
        TextureRegion getColorTexture(Color color);
    }

    private WallOfGames(WallOfGamesResourcesRetriever wallOfGamesResourcesRetriever, Stage stage, float width, float height) {
        this.stageWidth = width;
        this.stageHeight = height;
        this.stage = stage;
        this.resourcesRetriever = wallOfGamesResourcesRetriever;
    }

    public final static WallOfGames createWallOfGames(WallOfGamesResourcesRetriever wallOfGamesResourcesRetriever, Stage stage, float width, float height) {
        WallOfGames wallOfGames = new WallOfGames(wallOfGamesResourcesRetriever, stage, width, height);
        wallOfGames.prepareUnderneathLayer();
        wallOfGames.prepareBackgroundLayer();
        wallOfGames.prepareButtonsLayer();
        wallOfGames.setPosition(width / 2 - wallOfGames.getWidth() / 2, height / 2 - wallOfGames.getHeight() / 2);
        wallOfGames.addAction(Actions.alpha(0));
        return wallOfGames;
    }

    private void prepareBackgroundLayer() {
        backgroundLayer = new Group();
        backgroundLayer.setSize(stageHeight * RELATIVE_HEIGHT * (stageWidth / stageHeight), stageHeight * RELATIVE_HEIGHT);
        Image background = new Image(resourcesRetriever.getTextureRegion(OfficeMenuAssets.WALL_OF_GAMES_BACKGROUND));
        background.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
        background.setFillParent(true);
        background.addListener(wallClickListener);
        backgroundLayer.addActor(background);
        addActor(backgroundLayer);
    }

    private void prepareUnderneathLayer() {
        // TODO: 30.8.16 size underneathLayer with screen size 
        underneathLayer = new Group();
        underneathLayer.setSize(TablexiaSettings.getViewportWidth(stage), TablexiaSettings.getSceneOuterHeight(stage));
        underneathLayer.setPosition(TablexiaSettings.getSceneLeftX(stage), TablexiaSettings.getSceneOuterBottomY(stage));
        underneathLayer.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
        underneathLayer.addListener(wallClickListener);
        stage.addActor(underneathLayer);
    }

    private void prepareButtonsLayer() {
        buttonsGroup = new Group();
        buttonsGroup.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
        buttonsGroup.setSize(BUTTONS_GROUP_RELATIVE_WIDTH * stageWidth, BUTTONS_GROUP_RELATIVE_HEIGHT * RELATIVE_HEIGHT * stageHeight);
        buttonsGroup.setPosition(BUTTONS_GROUP_RELATIVE_X * stageWidth, BUTTONS_GROUP_RELATIVE_Y * stageHeight);
        buttonsGroup.addListener(wallClickListener);
        initializeButtons();
        addActor(buttonsGroup);
    }

    @Override
    public float getWidth() {
        return backgroundLayer.getWidth();
    }

    @Override
    public float getHeight() {
        return backgroundLayer.getHeight();
    }

    private ClickListener wallClickListener = new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            if (event.getListenerActor().getUserObject() != null) {
                GameIconDefinition.performIconAction((GameIconDefinition) (event.getListenerActor().getUserObject()));
            }
            setGameIconButtonsTouchable(Touchable.disabled);
            hide();
            super.clicked(event, x, y);
        }
    };

    private void initializeButtons() {
        Group buttonOverlayLayer = new Group();

        for (final GameIconDefinition gameIconDefinition : GameIconDefinition.values()) {
            TextureRegion pressed = resourcesRetriever.getTextureRegion(gameIconDefinition.getPressed());
            TextureRegion unpressed = resourcesRetriever.getTextureRegion(gameIconDefinition.getUnpressed());
            GameIconTablexiaButton gameIconTablexiaButton = new GameIconTablexiaButton(unpressed, pressed);
            gameIconTablexiaButton.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
            float newHeight = buttonsGroup.getHeight() * RELAVITE_MAX_ICON_HEIGHT;
            float aspectRatio = (float) unpressed.getRegionWidth() / unpressed.getRegionHeight();
            gameIconTablexiaButton.setSize(newHeight * aspectRatio, newHeight);
            gameIconTablexiaButton.setPosition(buttonsGroup.getWidth() * gameIconDefinition.getX(), buttonsGroup.getHeight() * gameIconDefinition.getY());
            gameIconTablexiaButton.setUserObject(gameIconDefinition);
            gameIconTablexiaButton.setInputListener(wallClickListener);
            gameIconTablexiaButton.setName(gameIconDefinition.name());
            buttonsGroup.addActor(gameIconTablexiaButton);

            Group gameNameLabel = createGameIconNameLabel(gameIconDefinition.gameDefinition.getTitle());
            gameNameLabel.setPosition(
                    gameIconTablexiaButton.getX() + gameIconTablexiaButton.getWidth() / 2f - gameNameLabel.getWidth() / 2f,
                    gameIconTablexiaButton.getY() + LABEL_TOP_PADDING
            );

            buttonOverlayLayer.addActor(gameNameLabel);

            UserRankManager.RankProgress rankProgress = UserRankManager.getInstance().getGameRankProgress(TablexiaSettings.getInstance().getSelectedUser(), gameIconDefinition.gameDefinition);


            TablexiaProgressBar tablexiaProgressBar = new TablexiaProgressBar(resourcesRetriever.getNinePatch(OfficeMenuAssets.WALL_OF_GAMES_PROGRESS_BAR_BACKGROUND), resourcesRetriever.getTextureRegion(OfficeMenuAssets.WALL_OF_GAMES_PROGRESS_BAR_FOREGROUND));
            tablexiaProgressBar.setPosition(gameIconTablexiaButton.getX() + gameIconTablexiaButton.getWidth() /2f - tablexiaProgressBar.getWidth()/2,
                    gameNameLabel.getY() - tablexiaProgressBar.getHeight() - PROGRESS_BAR_TOP_PADDING);
            tablexiaProgressBar.setPercent(rankProgress.getPercentDone());
            buttonOverlayLayer.addActor(tablexiaProgressBar);
        }

        buttonsGroup.addActor(buttonOverlayLayer);
    }

    private Group createGameIconNameLabel(String gameName) {
        Group group = new Group();
        //TODO group disable touch should be enough
        group.setTouchable(Touchable.disabled);

        TablexiaLabel label = new TablexiaLabel(gameName, new TablexiaLabel.TablexiaLabelStyle(GAME_ICON_LABEL_FONT, GAME_ICON_LABEL_FONT_COLOR));
        label.setPosition(BUTTON_LABEL_BACKGROUND_BORDER_X, BUTTON_LABEL_BACKGROUND_BORDER_Y);
        label.setTouchable(Touchable.disabled); //TODO try it and remove this

        Image background = new Image(resourcesRetriever.getColorTexture(GAME_ICON_LABEL_BACKGROUND_COLOR));
        background.setSize(label.getWidth() + 2 * BUTTON_LABEL_BACKGROUND_BORDER_X, label.getHeight() + 2 * BUTTON_LABEL_BACKGROUND_BORDER_Y);
        background.setPosition(0, 0);
        background.setTouchable(Touchable.disabled); //TODO try it and remove this

        group.setSize(background.getWidth(), background.getHeight());

        group.addActor(background);
        group.addActor(label);
        return group;
    }

    public void show() {
        if (!stage.getActors().contains(this, false)) stage.addActor(this);
        addAction(Actions.sequence(Actions.fadeIn(SHOW_HIDE_FADE_DURATION), Actions.run(new Runnable() {
            @Override
            public void run() {
                triggerScenarioStepEvent(WALL_OF_GAMES_READY);
            }
        })));
        shown = true;
        setGameIconButtonsTouchable(Touchable.enabled);
    }

    public void hide() {
        addAction(Actions.sequence(Actions.fadeOut(SHOW_HIDE_FADE_DURATION), Actions.run(new Runnable() {
            @Override
            public void run() {
                underneathLayer.remove();
                remove();
                shown = false;
            }
        })));
    }

    public boolean isShown() {
        return shown;
    }

    private void setGameIconButtonsTouchable(Touchable touchable) {
        for(Actor iconButton : buttonsGroup.getChildren() ) {
            iconButton.setTouchable(touchable);
        }
    }

    public enum GameIconDefinition {

        ROBBERY(OfficeMenuAssets.WALL_OF_GAMES_ICON_ROBBERY, OfficeMenuAssets.WALL_OF_GAMES_ICON_ROBBERY_PRESSED, 0.18f, 0.7f, GameDefinition.ROBBERY),
        PURSUIT(OfficeMenuAssets.WALL_OF_GAMES_ICON_PURSUIT, OfficeMenuAssets.WALL_OF_GAMES_ICON_PURSUIT_PRESSED, 0.85f, 0f, GameDefinition.PURSUIT),
        KIDNAPPING(OfficeMenuAssets.WALL_OF_GAMES_ICON_KIDNAPPING, OfficeMenuAssets.WALL_OF_GAMES_ICON_KIDNAPPING_PRESSED, 0.06f, 0f, GameDefinition.KIDNAPPING),
        NIGHTWATCH(OfficeMenuAssets.WALL_OF_GAMES_ICON_NIGHTWATCH, OfficeMenuAssets.WALL_OF_GAMES_ICON_NIGHTWATCH_PRESSED, 0.46f, 0.65f, GameDefinition.NIGHT_WATCH),
        SHOOTINGRANGE(OfficeMenuAssets.WALL_OF_GAMES_ICON_SHOOTINGRANGE, OfficeMenuAssets.WALL_OF_GAMES_ICON_SHOOTINGRANGE_PRESSED, 0f, 0.4f, GameDefinition.SHOOTING_RANGE),
        INTHEDARKNESS(OfficeMenuAssets.WALL_OF_GAMES_ICON_INTHEDARKNESS, OfficeMenuAssets.WALL_OF_GAMES_ICON_INTHEDARKNESS_PRESSED, 0.7f, 0.71f, GameDefinition.IN_THE_DARKNESS),
        CRIMESCENE(OfficeMenuAssets.WALL_OF_GAMES_ICON_CRIMESCENE, OfficeMenuAssets.WALL_OF_GAMES_ICON_CRIMESCENE_PRESSED, 0.67f, 0.33f, GameDefinition.CRIME_SCENE),
        RUNES(OfficeMenuAssets.WALL_OF_GAMES_ICON_RUNES, OfficeMenuAssets.WALL_OF_GAMES_ICON_RUNES_PRESSED, 0.45f, 0.03f, GameDefinition.RUNES),
        SAFE(OfficeMenuAssets.WALL_OF_GAMES_ICON_SAFE, OfficeMenuAssets.WALL_OF_GAMES_ICON_SAFE_PRESSED, 1f, 0.38f, GameDefinition.SAFE),
        PROTOCOL(OfficeMenuAssets.WALL_OF_GAMES_ICON_PROTOCOL, OfficeMenuAssets.WALL_OF_GAMES_ICON_PROTOCOL_PRESSED,1.1f,0.7f, GameDefinition.PROTOCOL),
        ON_THE_TRAIL(OfficeMenuAssets.WALL_OF_GAMES_ICON_ON_THE_TRAIL, OfficeMenuAssets.WALL_OF_GAMES_ICON_ON_THE_TRAIL_PRESSED, 0.28f, 0.33f, GameDefinition.ON_THE_TRAIL),
        MEMORY_GAME(OfficeMenuAssets.WALL_OF_GAMES_ICON_MEMORY_GAME, OfficeMenuAssets.WALL_OF_GAMES_ICON_MEMORY_GAME_PRESSED,1.25f,0.35f, GameDefinition.MEMORY_GAME),
        ATTENTION_GAME(OfficeMenuAssets.WALL_OF_GAMES_ICON_ATTENTION_GAME, OfficeMenuAssets.WALL_OF_GAMES_ICON_ATTENTION_GAME_PRESSED,1.12f,0f, GameDefinition.ATTENTION_GAME);

        private String unpressed, pressed;
        private float x, y;
        private GameDefinition gameDefinition;

        GameIconDefinition(String unpressed, String pressed, float x, float y, GameDefinition gameDefinition) {
            this.unpressed = unpressed;
            this.pressed = pressed;
            this.x = x;
            this.y = y;
            this.gameDefinition = gameDefinition;
        }

        public String getUnpressed() {
            return unpressed;
        }

        public String getPressed() {
            return pressed;
        }

        public float getX() {
            return x;
        }

        public float getY() {
            return y;
        }

        public static void performIconAction(GameIconDefinition gameIconDefinition){
            ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(GamePageScreen.class, TablexiaApplication.ScreenTransaction.FADE, null, gameIconDefinition.gameDefinition));
        }

        //For test

        public GameDefinition getGameDefinition() {
            return gameDefinition;
        }
    }
}