/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.gamemenu;

public final class OfficeMenuAssets {
    public static final String GFX_PATH = "gfx/";
    public static final String SFX_PATH = "sfx/";
    public static final String MFX_PATH = "mfx/";

    public static final String OFFICE                   = GFX_PATH + "office";
    public static final String OFFICE_RANKED            = GFX_PATH + "office_ranked";
    public static final String OFFICE_CLICKMAP          = GFX_PATH + "clickmap";
    public static final String OFFICE_RANKED_CLICKMAP   = GFX_PATH + "clickmap_ranked";
    public static final String BUBBLE_ENCYCLOPEDIA      = GFX_PATH + "bubble-encyklopedia";
    public static final String BUBBLE_TROPHIES          = GFX_PATH + "bubble-trophies";
    public static final String BUBBLE_DETECTIVES_CARD   = GFX_PATH + "bubble-detectives_card";
    public static final String BUBBLE_STATISTICS        = GFX_PATH + "bubble-statistics";
    public static final String BUBBLE_GAMES             = GFX_PATH + "bubble-games";


    public static final String VIGNETTE                 = GFX_PATH + "vignetting";

    public static final String DESK                     = GFX_PATH + "desk";
    public static final String DESK_PRESSED             = GFX_PATH + "desk_pressed";
    public static final String ENCYCLOPEDIA_PRESSED     = GFX_PATH + "encyclopedia_pressed";
    public static final String HALL_OF_FAME_PRESSED     = GFX_PATH + "halloffame_pressed";
    public static final String STATISTICS_PRESSED       = GFX_PATH + "statistics_pressed";
    public static final String WALL_OF_GAMES_PRESSED    = GFX_PATH + "wall_of_games_pressed";

    public static final String DESK_RANKED                  = GFX_PATH + "desk_ranked";
    public static final String DESK_RANKED_PRESSED          = GFX_PATH + "desk_ranked_pressed";
    public static final String ENCYCLOPEDIA_RANKED_PRESSED  = GFX_PATH + "encyclopedia_ranked_pressed";
    public static final String HALL_OF_FAME_RANKED_PRESSED  = GFX_PATH + "halloffame_ranked_pressed";
    public static final String STATISTICS_RANKED_PRESSED    = GFX_PATH + "statistics_ranked_pressed";
    public static final String WALL_OF_GAMES_RANKED_PRESSED = GFX_PATH + "wall_of_games_ranked_pressed";

    public static final String HELP_BUTTON_PRESSED      = GFX_PATH + "helpbutton_pressed";
    public static final String HELP_BUTTON_RELEASE      = GFX_PATH + "helpbutton_unpressed";

    public static final String INTRO_SPEECH             = MFX_PATH + "intro.mp3";

    //////// WALL OF GAMES ////////
    private static final String WALL_OF_GAMES_PATH = GFX_PATH + "wallofgames/";
    private static final String PRESSED = "_pressed";
    
    public static final String WALL_OF_GAMES_BACKGROUND                 = WALL_OF_GAMES_PATH + "background";
    public static final String WALL_OF_GAMES_ICON_ROBBERY               = WALL_OF_GAMES_PATH + "robbery";
    public static final String WALL_OF_GAMES_ICON_ROBBERY_PRESSED       = WALL_OF_GAMES_ICON_ROBBERY + PRESSED;
    public static final String WALL_OF_GAMES_ICON_PURSUIT               = WALL_OF_GAMES_PATH + "pursuit";
    public static final String WALL_OF_GAMES_ICON_PURSUIT_PRESSED       = WALL_OF_GAMES_ICON_PURSUIT + PRESSED;
    public static final String WALL_OF_GAMES_ICON_KIDNAPPING            = WALL_OF_GAMES_PATH + "kidnapping";
    public static final String WALL_OF_GAMES_ICON_KIDNAPPING_PRESSED    = WALL_OF_GAMES_ICON_KIDNAPPING + PRESSED;
    public static final String WALL_OF_GAMES_ICON_NIGHTWATCH            = WALL_OF_GAMES_PATH + "nightwatch";
    public static final String WALL_OF_GAMES_ICON_NIGHTWATCH_PRESSED    = WALL_OF_GAMES_ICON_NIGHTWATCH + PRESSED;
    public static final String WALL_OF_GAMES_ICON_SHOOTINGRANGE         = WALL_OF_GAMES_PATH + "shootingrange";
    public static final String WALL_OF_GAMES_ICON_SHOOTINGRANGE_PRESSED = WALL_OF_GAMES_ICON_SHOOTINGRANGE + PRESSED;
    public static final String WALL_OF_GAMES_ICON_INTHEDARKNESS         = WALL_OF_GAMES_PATH + "inthedarkness";
    public static final String WALL_OF_GAMES_ICON_INTHEDARKNESS_PRESSED = WALL_OF_GAMES_ICON_INTHEDARKNESS+ PRESSED;
    public static final String WALL_OF_GAMES_ICON_CRIMESCENE            = WALL_OF_GAMES_PATH + "crimescene";
    public static final String WALL_OF_GAMES_ICON_CRIMESCENE_PRESSED    = WALL_OF_GAMES_ICON_CRIMESCENE + PRESSED;
    public static final String WALL_OF_GAMES_ICON_RUNES                 = WALL_OF_GAMES_PATH + "runes";
    public static final String WALL_OF_GAMES_ICON_RUNES_PRESSED         = WALL_OF_GAMES_ICON_RUNES + PRESSED;
    public static final String WALL_OF_GAMES_ICON_PROTOCOL              = WALL_OF_GAMES_PATH + "protocol";
    public static final String WALL_OF_GAMES_ICON_PROTOCOL_PRESSED      = WALL_OF_GAMES_ICON_PROTOCOL + PRESSED;
    public static final String WALL_OF_GAMES_ICON_SAFE                  = WALL_OF_GAMES_PATH + "safe";
    public static final String WALL_OF_GAMES_ICON_SAFE_PRESSED          = WALL_OF_GAMES_ICON_SAFE + PRESSED;
    public static final String WALL_OF_GAMES_ICON_ON_THE_TRAIL          = WALL_OF_GAMES_PATH + "onthetrail";
    public static final String WALL_OF_GAMES_ICON_ON_THE_TRAIL_PRESSED  = WALL_OF_GAMES_ICON_ON_THE_TRAIL + PRESSED;
    public static final String WALL_OF_GAMES_ICON_MEMORY_GAME           = WALL_OF_GAMES_PATH + "memorygame";
    public static final String WALL_OF_GAMES_ICON_MEMORY_GAME_PRESSED   = WALL_OF_GAMES_ICON_MEMORY_GAME + PRESSED;
    public static final String WALL_OF_GAMES_ICON_ATTENTION_GAME        = WALL_OF_GAMES_PATH + "attention";
    public static final String WALL_OF_GAMES_ICON_ATTENTION_GAME_PRESSED= WALL_OF_GAMES_ICON_ATTENTION_GAME + PRESSED;

    public static final String WALL_OF_GAMES_PROGRESS_BAR_BACKGROUND    = WALL_OF_GAMES_PATH + "progressbar_background";
    public static final String WALL_OF_GAMES_PROGRESS_BAR_FOREGROUND    = WALL_OF_GAMES_PATH + "progressbar_foreground";
}
