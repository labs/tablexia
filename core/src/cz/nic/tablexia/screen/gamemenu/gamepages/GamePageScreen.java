/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.gamemenu.gamepages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaAtlasManager;
import cz.nic.tablexia.loader.TablexiaSoundManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Utility;

public class GamePageScreen extends AbstractTablexiaScreen<GamePageScreen.GamePageCommonAssetManager> {
    public enum GamePageDefinition {
        RobberyPage(        GameDefinition.ROBBERY,         "robberypage"),
        PursuitPage(        GameDefinition.PURSUIT,         "pursuitpage"),
        KidnappingPage(     GameDefinition.KIDNAPPING,      "kidnappingpage"),
        NightWatchPage(     GameDefinition.NIGHT_WATCH,     "nightwatchpage"),
        ShootingRangePage(  GameDefinition.SHOOTING_RANGE,  "shootingrangepage"),
        InTheDarknessPage(  GameDefinition.IN_THE_DARKNESS, "inthedarknesspage"),
        RunesPage(          GameDefinition.RUNES,           "runespage"),
        CrimeScenePage(     GameDefinition.CRIME_SCENE,     "crimescenepage"),
        ProtocolPage(       GameDefinition.PROTOCOL,        "protocolpage"),
        SafeGame(           GameDefinition.SAFE,            "safepage"),
        OnTheTrailPage(     GameDefinition.ON_THE_TRAIL,    "onthetrialpage"),
        MemoryGame(         GameDefinition.MEMORY_GAME,     "memorygamepage"),
        AttentionGame(      GameDefinition.ATTENTION_GAME,     "attentionpage");

        GameDefinition gameDefinition;
        String         gamePagePath;

        GamePageDefinition(GameDefinition gameDefinition, String gamePagePath) {
            this.gameDefinition =gameDefinition;
            this.gamePagePath = gamePagePath;
        }

        public static String getGamePagePathForGameDefinition(GameDefinition gameDefinition) {
            for(GamePageDefinition gamePageDefinition : GamePageDefinition.values()) {
                if(gamePageDefinition.gameDefinition == gameDefinition) return gamePageDefinition.gamePagePath;
            }

            Log.info(GamePageScreen.class, "Cannot get a game page path for game definition: " + gameDefinition);
            return null;
        }
    }
    /**
     * Used for common game page assets, which are shared between all game page screens...
     */
    protected class GamePageCommonAssetManager {
        private TablexiaAtlasManager atlasManager;
        private TablexiaSoundManager soundManager;

        protected String commonAtlasPath;

        public GamePageCommonAssetManager() {
            this.atlasManager = new TablexiaAtlasManager(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL);
            this.soundManager = new TablexiaSoundManager(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL);
        }

        public void load() {
            final CountDownLatch countDownLatch = new CountDownLatch(1);

            //LibGDX is creating textures while loading atlases, that's why we need to run this on main thread, which has an opengl context...
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    loadCommonGamePageAtlas();
                    loadCommonGamePageSounds();

                    countDownLatch.countDown();
                }
            });

            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                Log.err(getClass(), "Cannot wait for loading common game page assets!", e);
            }
        }

        public void dispose() {
            atlasManager.dispose();
            soundManager.dispose();
        }

        public TextureRegion getTextureRegion(String regionName) {
            return atlasManager.getTextureRegionFromAtlas(commonAtlasPath, regionName, null);
        }

        public Sound getSound(String soundName) {
            return soundManager.getSound(prepareCommonGamePageAssetPath() + Utility.transformLocalAssetsPath(soundName));
        }

        private void loadCommonGamePageAtlas() {
            this.commonAtlasPath = prepareCommonGamePageAssetPath() + prepareScreenName(GamePageScreen.class) + ATLAS_FILE_TYPE;
            atlasManager.loadAtlas(commonAtlasPath);
            atlasManager.finishLoading();
        }

        private void loadCommonGamePageSounds() {
            List<String> sounds = getCommonGamePageSounds();

            if(sounds == null) return;

            for(String sound : sounds) {
                soundManager.loadSound(prepareCommonGamePageAssetPath() + Utility.transformLocalAssetsPath(sound));
            }

            soundManager.finishLoading();
        }

        //List of shared sounds between all game page screens...
        private List<String> getCommonGamePageSounds() {
            List<String> sounds = new ArrayList<String>();
            sounds.add(GamePageAssets.STEP_EASY_MEDIUM);
            sounds.add(GamePageAssets.STEP_MEDIUM_EASY);
            sounds.add(GamePageAssets.STEP_MEDIUM_HARD);
            sounds.add(GamePageAssets.STEP_HARD_MEDIUM);
            sounds.add(GamePageAssets.STEP_HARD_BONUS);
            sounds.add(GamePageAssets.STEP_BONUS_HARD);
            return sounds;
        }

        private String prepareCommonGamePageAssetPath() {
            return prepareScreenAssetsPath(prepareScreenName(GamePageScreen.class));
        }
    }

    private static final String ATLAS_FILE_TYPE  = ".atlas";

    private static final float INTRO_SOUND_DELAY = 1.0f; //Delay in seconds

    private GameDefinition gameDefinition;

    public GamePageScreen(GameDefinition gameDefinition) {
        this.gameDefinition = gameDefinition;
    }

    @Override
    protected GamePageCommonAssetManager prepareScreenData(Map<String, String> screenState) {
        GamePageCommonAssetManager gamePageCommonAssetManager = new GamePageCommonAssetManager();
        gamePageCommonAssetManager.load();
        return gamePageCommonAssetManager;
    }

    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        GamePageGroup gamePageGroup = new GamePageGroup(this, gameDefinition);
        gamePageGroup.setBounds(
                getSceneLeftX(),
                getSceneInnerBottomY(),
                getSceneWidth(),
                getSceneInnerHeight()
        );
        getStage().addActor(gamePageGroup);

        getStage().addAction(Actions.sequence(Actions.delay(INTRO_SOUND_DELAY, Actions.run(new Runnable() {
            @Override
            public void run() {
                Music music = getMusic(GamePageAssets.INTRO);
                if(music != null) music.play();
            }
        }))));
    }

    public GameDefinition getGameDefinition() {
        return gameDefinition;
    }

    public void startGame(){
        ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(gameDefinition.getScreenClass(),
                TablexiaApplication.ScreenTransaction.FADE));
    }

    /**
     * Get Texture Region from common path for all game page screens
     * @param regionName
     */
    public TextureRegion getCommonGamePageTextureRegion(String regionName) {
        return getData().getTextureRegion(regionName);
    }

    /**
     * Get sounds from common path for all game page screens
     * @param soundName
     */
    public Sound getCommonGamePageSound(String soundName) {
        return getData().getSound(soundName);
    }

    @Override
    protected String prepareScreenName() {
        return GamePageDefinition.getGamePagePathForGameDefinition(gameDefinition);
    }

    @Override
    protected void prepareScreenSoundAssetNames(List<String> soundsFileNames) {
        super.prepareScreenSoundAssetNames(soundsFileNames);
    }

    @Override
    protected void screenDisposed() {
        getData().dispose();
    }

    @Override
    public boolean equals(Object obj) {
        if(!super.equals(obj)) return false;
        if(obj instanceof GamePageScreen) {
            //GamePageScreens are not equal unless their game definition is equal as well...
            return getGameDefinition() == ((GamePageScreen) obj).getGameDefinition();
        }
        return false;
    }
}