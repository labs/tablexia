/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.gamemenu.gamepages;

/**
 * Created by mazzers on 13.9.16.
 */
public class GamePageAssets {

    public static final String GFX_PATH = "gfx/";
    public static final String SFX_PATH = "sfx/";

    public static final String BACKGROUND            = GFX_PATH + "background";
    public static final String TITLE                 = GFX_PATH + "title";
    public static final String START_BUTTON_RELEASED = GFX_PATH + "startbutton";
    public static final String START_BUTTON_PRESSED  = GFX_PATH + "startbutton_pressed";

    public static final String PROGRESSBAR           = GFX_PATH + "progressbar";
    public static final String PROGRESSBAR_FILL      = GFX_PATH + "progressbar_fill";

    public static final String DIFF_BAR              = GFX_PATH + "difficultyseekbar_background";
    public static final String DIFF_THUMB_EASY       = GFX_PATH + "difficultyseekbar_thumb_easy";
    public static final String DIFF_THUMB_HARD       = GFX_PATH + "difficultyseekbar_thumb_hard";
    public static final String DIFF_THUMB_MEDIUM     = GFX_PATH + "difficultyseekbar_thumb_medium";
    public static final String DIFF_THUMB_BONUS      = GFX_PATH + "difficultyseekbar_thumb_bonus";

    public static final String STEP_EASY_MEDIUM      = SFX_PATH + "step_1_2.mp3";
    public static final String STEP_MEDIUM_EASY      = SFX_PATH + "step_2_1.mp3";
    public static final String STEP_MEDIUM_HARD      = SFX_PATH + "step_2_3.mp3";
    public static final String STEP_HARD_MEDIUM      = SFX_PATH + "step_3_2.mp3";
    public static final String STEP_HARD_BONUS       = SFX_PATH + "step_3_4.mp3";
    public static final String STEP_BONUS_HARD       = SFX_PATH + "step_4_3.mp3";

    /// SFX ///
    public static final String INTRO                 = SFX_PATH + "intro.mp3";
}