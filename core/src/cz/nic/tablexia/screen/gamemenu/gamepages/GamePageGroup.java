/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.gamemenu.gamepages;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.model.UserDifficultySettingsDAO;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.screen.profile.ProfileScreen;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.Switch;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaProgressBar;
import cz.nic.tablexia.util.ui.button.TablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AlertOnShowDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.BackButtonHideComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.CloseButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DimmerDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;

/**
 * Represents a single page with game in Main Menu
 * Created by lhoracek on 3/4/15.
 * Edited by Drahomir Karchnak on 2/12/15.
 */
public class GamePageGroup extends Group {
    public static final String START_BUTTON = "start button";
    public static final String DIFFICULTY_BUTTON_EASY = "difficulty button easy";
    public static final String DIFFICULTY_BUTTON_MEDIUM = "difficulty button medium";
    public static final String DIFFICULTY_BUTTON_HARD = "difficulty button hard";
    public static final String DIFFICULTY_BUTTON_BONUS = "difficulty button bonus";
    public static final String BONUS_DIFFICULTY_LOCKED_DIALOG_NAME = "bonus difficulty locked dialog";

    private static final float TITLE_WIDTH = 0.6f;
    private static final float START_BUTTON_WIDTH = 0.15f;
    private static final float DIFFICULTY_THUMB_WIDTH = 0.074f;
    private static final float DIFFICULTY_BAR_WIDTH = 0.28f;

    private static final float TITLE_TOP_OFFSET = 0.03f;
    private static final float START_BUTTON_BOTTOM_OFFSET = 0.26f;
    private static final float DIFFICULTY_BOTTOM_OFFSET = 0.1f;
    private static final float DIFFICULTY_BAR_BOTTOM_OFFSET = 0.04f;

    private static final float START_BUTTON_MIN_HEIGHT = 0.33f;
    private static final float START_BUTTON_MAX_HEIGHT = 0.36f;

    private static final float TITLE_MIN_HEIGHT = 0.25f;
    private static final float TITLE_MAX_HEIGHT = 0.37f;

    private static final float DESKTOP_SCALE_HD = 0.7f;
    private static final float DESKTOP_SCALE_NONE = 1f;

    /*
     *  offset to the center of edge buttons on difficulty bar
     *  multiply this value with difficulty bars width to get offset for current screens coord system
     *
     *  14 is distance to center of the edge button
     *  251 is width of difficulty bar texture
     */
    private static final float DIFFICULTY_BAR_EDGE_OFFSET = 14 / 251f;

    private static final float PROGRESS_BAR_WIDTH = 0.6f;
    private static final float PROGRESS_BAR_BOTTOM_OFFSET = 0.002f;

    private static final char PROGRESS_STATUS_SEPARATOR = '/';
    private static final float PROGRESS_STATUS_OFFSET_X = 0.01f;

    private static final float NEW_GAME_LABEL_OFFSET_RATIO = 1f / 10;

    private static final int BONUS_DIFFICULTY_LOCKED_DIALOG_WIDTH = 400;
    private static final int BONUS_DIFFICULTY_LOCKED_DIALOG_HEIGHT = 300;

    //Font for difficulty labels
    private static final ApplicationFontManager.FontType DIFFICULTY_FONT_STYLE = ApplicationFontManager.FontType.BOLD_18;

    //Progress status font
    private static final ApplicationFontManager.FontType PROGRESS_STATUS_FONT = ApplicationFontManager.FontType.BOLD_18;

    //Progress status font
    private static final ApplicationFontManager.FontType NEW_GAME_STATUS_FONT = ApplicationFontManager.FontType.BOLD_18;

    private GameDefinition gameDefinition;

    private Stack diffStack;
    private Switch.DragSwitchListener dragSwitchListener;

    private TablexiaComponentDialog bonusDifficultyLockedDialog;

    //Enabled playing sound steps on difficulty change
    private boolean stepSoundsEnabled = false;

    //Step sounds assets
    private Sound step_easy_medium, step_medium_easy;
    private Sound step_medium_hard, step_hard_medium;
    private Sound step_hard_bonus, step_bonus_hard;

    private int lastDragDifficultyStep = 0;

    private TablexiaProgressBar gameMenuPageProgress;

    private TablexiaLabel progressLabel;

    boolean updateGameDifficultyInDB = true;

    private GamePageScreen screen;

    public GamePageGroup(GamePageScreen screen, GameDefinition gameDefinition) {
        this.screen = screen;
        this.gameDefinition = gameDefinition;

        prepareSounds();
        prepareBackground();
        prepareTitle();
        prepareStartButton();
        prepareDifficulty();
        prepareGameProgress();
    }

    private void prepareBackground() {
        Image backgroundImage = new Image(getScreen().getScreenTextureRegion(GamePageAssets.BACKGROUND));
        backgroundImage.setBounds(
                getScreen().getSceneLeftX(),
                getScreen().getSceneOuterBottomY(),
                getScreen().getSceneWidth(),
                getScreen().getSceneOuterHeight()
        );
        addActor(backgroundImage);
    }

    /**
     * Prepares/Loads Sounds
     */
    private void prepareSounds() {
        step_easy_medium = getScreen().getCommonGamePageSound(GamePageAssets.STEP_EASY_MEDIUM);
        step_medium_easy = getScreen().getCommonGamePageSound(GamePageAssets.STEP_MEDIUM_EASY);

        step_medium_hard = getScreen().getCommonGamePageSound(GamePageAssets.STEP_MEDIUM_HARD);
        step_hard_medium = getScreen().getCommonGamePageSound(GamePageAssets.STEP_HARD_MEDIUM);

        step_hard_bonus = getScreen().getCommonGamePageSound(GamePageAssets.STEP_HARD_BONUS);
        step_bonus_hard = getScreen().getCommonGamePageSound(GamePageAssets.STEP_BONUS_HARD);
    }

    /**
     * Creates and repares Title
     */
    public void prepareTitle() {
        //Resources
        TextureRegion title = getScreen().getScreenTextureRegion(GamePageAssets.TITLE);
        final Image titleImage = new Image(title);

        //Size
        int titleWidth = (int) (getScreen().getSceneWidth() * TITLE_WIDTH);
        int titleHeight = (int) (titleWidth * ((float) title.getRegionHeight() / (float) title.getRegionWidth()));

        //Makes sure Height is always between min and max height
        float tempHeight = MathUtils.clamp(titleHeight, TITLE_MIN_HEIGHT * getScreen().getSceneInnerHeight(), TITLE_MAX_HEIGHT * getScreen().getSceneInnerHeight());
        if (tempHeight != titleHeight) {
            titleHeight = (int) tempHeight;
            titleWidth = (int) (titleHeight * (title.getRegionWidth() / (float) title.getRegionHeight()));
        }

        //Position
        int titleX = (int) (getScreen().getSceneWidth() / 2 - titleWidth / 2);
        int titleY = (int) (getScreen().getSceneInnerTopY() - titleHeight - (getScreen().getSceneInnerHeight() * TITLE_TOP_OFFSET));

        titleImage.setPosition(titleX, titleY);
        titleImage.setSize(titleWidth, titleHeight);

        addActor(titleImage);
    }

    /**
     * Initializes StartButton Pressed and Released
     */
    public void prepareStartButton() {
        // Prepare Start Button UP/Released
        TextureRegion startPressed = getScreen().getScreenTextureRegion(GamePageAssets.START_BUTTON_PRESSED);
        TextureRegion startReleased = getScreen().getScreenTextureRegion(GamePageAssets.START_BUTTON_RELEASED);

        TablexiaButton startButton = new TablexiaButton(null, false, startReleased, startPressed, null, null)
                .adaptiveSize(true);

        // Size
        int startWidth = (int) (getScreen().getSceneWidth() * START_BUTTON_WIDTH);
        int startHeight = (int) (startWidth * (startPressed.getRegionHeight() / (float) startPressed.getRegionWidth()));


        //Makes sure Height is always between min and max height
        float tempHeight = MathUtils.clamp(startHeight, START_BUTTON_MIN_HEIGHT * getScreen().getSceneInnerHeight(), START_BUTTON_MAX_HEIGHT * getScreen().getSceneInnerHeight());
        if (tempHeight != startHeight) {
            startHeight = (int) tempHeight;
            startWidth = (int) (startHeight * (startPressed.getRegionWidth() / (float) startPressed.getRegionHeight()));
        }

        // Position
        int startX = (int) getScreen().getSceneWidth() / 2 - startWidth / 2;
        int startY = (int) ((getScreen().getSceneInnerHeight() * START_BUTTON_BOTTOM_OFFSET));

        startButton.setSize(startWidth, startHeight);
        startButton.setPosition(startX, startY);

        startButton.setName(START_BUTTON);

        startButton.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                getScreen().startGame();
            }
        });

        addActor(startButton);
    }

    private enum DifficultyOption {
        EASY(GameDifficulty.EASY, GamePageAssets.DIFF_THUMB_EASY, DIFFICULTY_BUTTON_EASY),
        MEDIUM(GameDifficulty.MEDIUM, GamePageAssets.DIFF_THUMB_MEDIUM, DIFFICULTY_BUTTON_MEDIUM),
        HARD(GameDifficulty.HARD, GamePageAssets.DIFF_THUMB_HARD, DIFFICULTY_BUTTON_HARD),
        BONUS(GameDifficulty.BONUS, GamePageAssets.DIFF_THUMB_BONUS, DIFFICULTY_BUTTON_BONUS);

        private float snapPointPosition;
        private GameDifficulty gameDifficulty;
        private String texturePath;
        private String buttonName;

        DifficultyOption(GameDifficulty gameDifficulty, String texturePath, String buttonName) {
            this.gameDifficulty = gameDifficulty;
            this.texturePath = texturePath;
            this.buttonName = buttonName;

        }

        public TablexiaLabel createLabel(Color color) {
            TablexiaLabel tablexiaLabel = new TablexiaLabel(gameDifficulty.getTextDescription(), new TablexiaLabel.TablexiaLabelStyle(DIFFICULTY_FONT_STYLE, color));
            tablexiaLabel.setAlignment(Align.center);

            return tablexiaLabel;
        }
    }

    /**
     * Creates and prepares Difficulty slider, Difficulty labels and Buttons
     */
    private void prepareDifficulty() {
        stepSoundsEnabled = false;

        final float barPositionY = getScreen().getSceneInnerHeight() * DIFFICULTY_BOTTOM_OFFSET;

        // Difficulty bar
        TextureRegion barTextureRegion = getScreen().getCommonGamePageTextureRegion(GamePageAssets.DIFF_BAR);
        Image barImage = new Image(barTextureRegion);

        int barWidth = (int) (getScreen().getSceneWidth() * DIFFICULTY_BAR_WIDTH);
        int barHeight = (int) (barWidth * (barTextureRegion.getRegionHeight() / (float) barTextureRegion.getRegionWidth()));

        int barX = (int) getScreen().getSceneWidth() / 2 - barWidth / 2;
        int barY = (int) (barPositionY + (getScreen().getSceneWidth() * DIFFICULTY_BAR_BOTTOM_OFFSET));

        barImage.setPosition(barX, barY);
        barImage.setSize(barWidth, barHeight);
        addActor(barImage);

        //Difficulty Thumb
        final TextureRegion diffThumbTextureRegion = getScreen().getCommonGamePageTextureRegion(GamePageAssets.DIFF_THUMB_EASY);

        float scaleFactor = TablexiaSettings.getInstance().isUseHdAssets() ? DESKTOP_SCALE_HD : DESKTOP_SCALE_NONE;

        float diffThumbWidth = getScreen().getSceneWidth() * DIFFICULTY_THUMB_WIDTH * scaleFactor;
        float diffThumbHeight = (int) (diffThumbWidth * (diffThumbTextureRegion.getRegionHeight() / (float) diffThumbTextureRegion.getRegionWidth()));

        //Centers thumb difficulty button to difficulty bar
        float diffThumbY = barY + (barHeight / 2) - (diffThumbHeight / 2);

        diffStack = new Stack();

        Color color = isBonusDifficultyUnlocked() ? Color.BLACK : Color.DARK_GRAY;

        for (DifficultyOption diffOption : DifficultyOption.values()) {
            diffOption.snapPointPosition = (barWidth / (DifficultyOption.values().length - 1)) * diffOption.ordinal() + barX - diffThumbWidth / 2;
            TablexiaLabel tablexiaLabel = diffOption.createLabel(diffOption == DifficultyOption.BONUS ? color : Color.BLACK);
            tablexiaLabel.setPosition(diffOption.snapPointPosition + (diffThumbWidth / 2) - tablexiaLabel.getWidth() / 2, barPositionY);
            addActor(tablexiaLabel);

            Actor diffButton = new Actor();
            diffButton.setSize(diffThumbWidth, diffThumbHeight);
            diffButton.setPosition(diffOption.snapPointPosition + diffThumbWidth / 2 - diffButton.getWidth() / 2, diffThumbY);
            diffButton.setName(diffOption.buttonName);

            diffButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    updateDifficultySeekBar(diffOption.gameDifficulty);
                }
            });
            addActor(diffButton);
            diffStack.addActor(new Image(getScreen().getCommonGamePageTextureRegion(diffOption.texturePath)));
        }

        diffStack.setSize(diffThumbWidth, diffThumbHeight);
        diffStack.setPosition(DifficultyOption.EASY.snapPointPosition, diffThumbY);
        addActor(diffStack);

        diffStack.addListener(dragSwitchListener = new Switch.DragSwitchListener(diffStack,
                DifficultyOption.EASY.snapPointPosition,
                DifficultyOption.BONUS.snapPointPosition,
                DifficultyOption.MEDIUM.snapPointPosition,
                DifficultyOption.HARD.snapPointPosition));

        dragSwitchListener.setSwitchMovedListener(new Switch.DragSwitchListener.SwitchMovedListener() {
            @Override
            public void movedToStep(int step) {
                for (int i = 0; i < diffStack.getChildren().size; i++) {
                    diffStack.getChildren().get(i).setVisible(i == step);
                    if (i == step) {
                        if (lastDragDifficultyStep != step) {
                            GameDifficulty currentDiff = GameDifficulty.getGameDifficultyForDifficultyNumber(step + 1);
                            GameDifficulty previousDiff = GameDifficulty.getGameDifficultyForDifficultyNumber(lastDragDifficultyStep + 1);

                            if (stepSoundsEnabled) {
                                playStepSound(previousDiff, currentDiff);
                            }

                            //if bonus difficulty is not unlocked yet, return button to hard difficulty
                            if (currentDiff == GameDifficulty.BONUS && !isBonusDifficultyUnlocked()) {
                                dragSwitchListener.switchToStep(GameDifficulty.HARD.getDifficultyNumber() - 1);
                                lastDragDifficultyStep = GameDifficulty.HARD.getDifficultyNumber() - 1;
                                showBonusDifficultyLockedDialog();
                            } else {
                                lastDragDifficultyStep = step;
                            }
                        }
                    }
                }
            }
        });
        dragSwitchListener.setSwitchSelectedListener(new Switch.DragSwitchListener.SwitchSelectedListener() {
            @Override
            public void stepSelected(int step) {
                if (updateGameDifficultyInDB) {
                    UserDifficultySettingsDAO.saveSettingsForUser(TablexiaSettings.getInstance().getSelectedUser().getId(), gameDefinition.getGameNumber(), GameDifficulty.getVisibleGameDifficultyList().get(step).getDifficultyNumber());
                }
            }
        });
        updateGameDifficultyFromDB();

        stepSoundsEnabled = true;
    }

    private boolean isBonusDifficultyUnlocked() {
        return GameDAO.isBonusDifficultyUnlocked(gameDefinition.getGameNumber(), TablexiaSettings.getInstance().getSelectedUser().getId());
    }

    private void showBonusDifficultyLockedDialog() {
        if (bonusDifficultyLockedDialog != null) return;
        bonusDifficultyLockedDialog = TablexiaComponentDialogFactory.getInstance().createStandardUntouchableDialog(
                new FixedSpaceContentDialogComponent(),
                new TextContentDialogComponent(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.DIFFICULTY_LOCKED_TITLE), ApplicationFontManager.FontType.BOLD_20, true, true),
                new ResizableSpaceContentDialogComponent(),
                new TextContentDialogComponent(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.DIFFICULTY_LOCKED_DESCRIPTION) + gameDefinition.getTitle(), true, true),
                new ResizableSpaceContentDialogComponent(),
                new TextContentDialogComponent(
                        ApplicationTextManager.getInstance().getFormattedText(
                                ApplicationTextManager.ApplicationTextsAssets.DIFFICULTY_LOCKED_CURRENT,
                                GameDAO.requiredHardGamesCount(gameDefinition.getGameNumber(),
                                        TablexiaSettings.getInstance().getSelectedUser().getId()))),
                new CloseButtonContentDialogComponent(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_UNDERSTAND)).setInputListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        hideBonusDifficultyLockedDialog();
                    }
                }),
                new AlertOnShowDialogComponent(),
                new CenterPositionDialogComponent(),
                new DimmerDialogComponent(),
                new BackButtonHideComponent() {
                    @Override
                    public void onBackButtonPress(TablexiaApplication.BackButtonPressed event) {
                        super.onBackButtonPress(event);
                        hideBonusDifficultyLockedDialog();
                    }
                },
                new ResizableSpaceContentDialogComponent()
        );
        bonusDifficultyLockedDialog.setName(BONUS_DIFFICULTY_LOCKED_DIALOG_NAME);
        bonusDifficultyLockedDialog.show(BONUS_DIFFICULTY_LOCKED_DIALOG_WIDTH, BONUS_DIFFICULTY_LOCKED_DIALOG_HEIGHT);
    }

    private void hideBonusDifficultyLockedDialog() {
        if (bonusDifficultyLockedDialog == null) return;
        bonusDifficultyLockedDialog.hide();
        bonusDifficultyLockedDialog = null;
    }

    private void updateGameDifficultyFromDB() {
        updateDifficultySeekBar(UserDifficultySettingsDAO.getUserSettingsByGame(TablexiaSettings.getInstance().getSelectedUser().getId(), gameDefinition.getGameNumber()), false);
    }

    private void updateDifficultySeekBar(GameDifficulty gameDifficulty) {
        updateDifficultySeekBar(gameDifficulty, true);
    }

    private void updateDifficultySeekBar(GameDifficulty gameDifficulty, boolean updateGameDifficultyInDB) {
        this.updateGameDifficultyInDB = updateGameDifficultyInDB;
        dragSwitchListener.switchToStep(gameDifficulty.getDifficultyNumber() - 1);
        this.updateGameDifficultyInDB = true;
    }

    public void prepareGameProgress() {
        this.gameMenuPageProgress = new TablexiaProgressBar(
                getScreen().getScreenTextureRegion(GamePageAssets.PROGRESSBAR),
                getScreen().getScreenTextureRegion(GamePageAssets.PROGRESSBAR_FILL)
        );

        this.gameMenuPageProgress.setWidth(getScreen().getSceneWidth() * PROGRESS_BAR_WIDTH);
        this.gameMenuPageProgress.setPosition(
                getScreen().getSceneWidth() / 2 - gameMenuPageProgress.getWidth() / 2,
                getScreen().getSceneInnerBottomY() + (getScreen().getSceneOuterHeight() * PROGRESS_BAR_BOTTOM_OFFSET)
        );

        UserRankManager.RankProgress rankProgress = UserRankManager.getInstance().getGameRankProgress(TablexiaSettings.getInstance().getSelectedUser(), gameDefinition);

        this.progressLabel = new TablexiaLabel(getProgressLabelText(rankProgress), new TablexiaLabel.TablexiaLabelStyle(PROGRESS_STATUS_FONT, Color.BLACK));
        this.progressLabel.setPosition(
                gameMenuPageProgress.getX() + gameMenuPageProgress.getWidth() + PROGRESS_STATUS_OFFSET_X * getScreen().getSceneWidth(),
                gameMenuPageProgress.getY() + gameMenuPageProgress.getHeight() / 2 - progressLabel.getHeight() / 2
        );

        this.gameMenuPageProgress.addListener(new ClickListenerWithSound() {
            @Override
            public void onClick(InputEvent event, float x, float y) {
                ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(ProfileScreen.class, TablexiaApplication.ScreenTransaction.FADE));
                gameMenuPageProgress.setTouchable(Touchable.disabled); //Makes sure you wont click twice...
            }
        });

        updateGameProgress();

        addActor(gameMenuPageProgress);
        addActor(progressLabel);

        //New Game
        if (UserRankManager.getInstance().isNewGameForUser(TablexiaSettings.getInstance().getSelectedUser(), gameDefinition)) {
            TablexiaLabel newGameLabel = new TablexiaLabel(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.GAME_STATUS_NEW), new TablexiaLabel.TablexiaLabelStyle(NEW_GAME_STATUS_FONT, Color.BLACK));
            newGameLabel.setPosition(progressLabel.getX() + progressLabel.getWidth() + ((getScreen().getSceneWidth() - (progressLabel.getX() + progressLabel.getWidth())) * NEW_GAME_LABEL_OFFSET_RATIO),
                    progressLabel.getY());
            addActor(newGameLabel);
        }
    }

    public void updateGameProgress() {
        UserRankManager.RankProgress rankProgress = UserRankManager.getInstance().getGameRankProgress(TablexiaSettings.getInstance().getSelectedUser(), gameDefinition);

        if (gameMenuPageProgress == null) return;
        this.gameMenuPageProgress.setPercent(rankProgress.getPercentDone());

        if (progressLabel == null) return;
        progressLabel.setText(getProgressLabelText(rankProgress));
    }

    private String getProgressLabelText(UserRankManager.RankProgress rankProgress) {
        return (rankProgress.getCurrXP() - rankProgress.getMinXP()) + Character.toString(PROGRESS_STATUS_SEPARATOR) + (rankProgress.getNextXP() - rankProgress.getMinXP());
    }

    private void playStepSound(GameDifficulty previous, GameDifficulty current) {
        switch (previous) {
            case EASY:
                step_easy_medium.play();
                break;
            case MEDIUM:
                if (current == GameDifficulty.EASY)
                    step_medium_easy.play();
                else
                    step_medium_hard.play();
                break;
            case HARD:
                if (current == GameDifficulty.MEDIUM)
                    step_hard_medium.play();
                else
                    step_hard_bonus.play();
                break;
            case BONUS:
                step_bonus_hard.play();
                break;
            case TUTORIAL:
                break;
        }
    }

    private GamePageScreen getScreen() {
        return screen;
    }
}
