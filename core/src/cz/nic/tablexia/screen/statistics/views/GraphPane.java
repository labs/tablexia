/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.statistics.views;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.statistics.StatisticsGameScoreResolver;
import cz.nic.tablexia.screen.statistics.StatisticsScreen;
import cz.nic.tablexia.screen.statistics.enums.GraphStyle;
import cz.nic.tablexia.screen.statistics.interfaces.GameClickListener;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.actionwidget.Action;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;

/**
 * Created by danilov on 11/6/15.
 */
public class GraphPane extends WidgetGroup {

    public static final float POINT_SIZE = 20f, MIN_GRAPH_WIDTH = 593f;
    private static final float LINE_SIZE = 5.5f, EFFECTS_SPEED = 0.4f, CLICK_AREA_SIZE = 50f;

    private float maxScore, minScore;
    private boolean screenResized, initialResize;
    private GameDefinition selectedGameDefinition;
    private StatisticsGameScoreResolver statisticsGameScoreResolver;
    private GameDifficulty lastSelectedGameDifficulty;
    private GameClickListener gameClickListener;
    private GraphStyle graphStyle;
    private StatisticsScreen statisticsScreen;
    private TextureRegion lineTextureRegion, pointTextureRegion;
    private List<Game> allGames, easyGames, mediumGames, hardGames, bonusGames;
    private List<Float> easyAverageScores, mediumAverageScores, hardAverageScores, bonusAverageScores;
    private List<Image> easyPoints, mediumPoints, hardPoints, bonusPoints;
    private Set<GameDifficulty> selectedDifficulties;
    private List<Actor> clickAreas;

    public GraphPane(List<Game> allGames, GraphStyle graphStyle, StatisticsScreen screen, GameDefinition selectedGameDefinition, Set<GameDifficulty> selectedDifficulties, GameDifficulty lastSelectedGameDifficulty, GameClickListener gameClickListener) {
        this.allGames = allGames;
        this.graphStyle = graphStyle;
        this.statisticsScreen = screen;
        this.selectedGameDefinition = selectedGameDefinition;
        this.lastSelectedGameDifficulty = lastSelectedGameDifficulty;
        this.selectedDifficulties = selectedDifficulties;
        this.gameClickListener = gameClickListener;

        statisticsGameScoreResolver = new StatisticsGameScoreResolver(selectedGameDefinition);
        createGameLists();
        createAverageScoresLists();
        createPointsLists();

        maxScore = statisticsGameScoreResolver.getMaxScore(allGames);
        minScore = statisticsGameScoreResolver.getMinScore(allGames);

        setWidth(getWidthOfGraphByGames());

        initialResize = true;

    }

    enum GraphDifficultyColor {
        EASY(GameDifficulty.EASY, Color.OLIVE, "point_olive"),
        MEDIUM(GameDifficulty.MEDIUM, Color.ORANGE, "point_orange"),
        HARD(GameDifficulty.HARD, Color.FIREBRICK, "point_brick_red"),
        BONUS(GameDifficulty.BONUS, Color.SLATE, "point_slate");

        private GameDifficulty difficulty;
        private Color color;
        private String textureRegionName;

        GraphDifficultyColor(GameDifficulty difficulty, Color color, String textureRegionName) {
            this.difficulty = difficulty;
            this.color = color;
            this.textureRegionName = textureRegionName;
        }

        public GameDifficulty getDifficulty() {
            return difficulty;
        }

        public Color getColor() {
            return color;
        }

        public String getTextureRegionName() {
            return textureRegionName;
        }
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();

        screenResized = true;
        if(initialResize) {
            screenResized = false;
            initialResize = false;
        }

        clear();
        clearPointsLists();
        drawAllGraphs();

        statisticsScreen.position();
    }

    private void drawAllGraphs() {
        if (selectedDifficulties.contains(GameDifficulty.EASY)) drawGraph(easyGames, GameDifficulty.EASY);
        if (selectedDifficulties.contains(GameDifficulty.MEDIUM)) drawGraph(mediumGames, GameDifficulty.MEDIUM);
        if (selectedDifficulties.contains(GameDifficulty.HARD)) drawGraph(hardGames, GameDifficulty.HARD);
        if (selectedDifficulties.contains(GameDifficulty.BONUS)) drawGraph(bonusGames, GameDifficulty.BONUS);
    }

    private void drawGraph(List<Game> games, GameDifficulty difficulty) {
        setGraphColor(difficulty);

        switch (graphStyle) {
            case DAILY:
                createScorePoints(games);
                break;

            case AVERAGE:
                createAverageScorePoints(difficulty);
                break;
        }

        //Order of drawing points, lines and click areas is important because of better clicking on points/games
        boolean isNeededToRedraw = isNeededToRedraw(difficulty);
        createAndAddLines(getPointsByDifficulty(difficulty), isNeededToRedraw);
        addPointsToGraph(getPointsByDifficulty(difficulty), isNeededToRedraw);
        addPointClickAreasToGraph();


    }

    private void setGraphColor(GameDifficulty newDifficulty) {

        for (GraphDifficultyColor graphDiffColor : GraphDifficultyColor.values()) {
            if (graphDiffColor.getDifficulty() == newDifficulty) {
                lineTextureRegion = statisticsScreen.getColorTextureRegion(graphDiffColor.getColor());
                pointTextureRegion = statisticsScreen.getScreenTextureRegion(StatisticsScreen.GFX_PATH + graphDiffColor.getTextureRegionName());
            }
        }
    }

     /*======================= Creation of points and areas =======================*/

    private void createScorePoints(List<Game> games) {
        for (Game game : games) {
            float score = selectedGameDefinition.getGameResultResolver(game.getGameScoreResolverVersion()).getComparisonScore(game);
            createPoint(game, score, getGamesByDifficulty(GameDifficulty.getByGame(game)).size(), games.indexOf(game), -1, null);
        }


    }

    private void createAverageScorePoints(GameDifficulty difficulty) {
        List<Game> games = getGamesByDifficulty(difficulty);
        List<Float> averageScores = getAverageScoreByDifficulty(difficulty);
        int i = 0;
        if(games != null && games.size() != 0 && averageScores != null && averageScores.size() != 0) {
            for (float aveScore : averageScores) {
             String dateText = statisticsGameScoreResolver.getDateForAverageGameScore(i,games);
                createPoint(games.get(i), aveScore, averageScores.size(), i++, aveScore, dateText);
            }
        }
    }

    private void createPoint(final Game game, float score, int numberOfGames, int index, final float averageScore, final String dateText) {
        Actor clickArea = new Actor();
        final Image graphPoint = new Image(pointTextureRegion);

        setPropertiesToGraphPoint(clickArea, score, numberOfGames, index, isFlipOfGraphNeeded(selectedGameDefinition));
        setPropertiesToGraphPoint(graphPoint, score, numberOfGames, index, isFlipOfGraphNeeded(selectedGameDefinition));

        clickAreas.add(createPointClickArea(clickArea));
        getPointsByDifficulty(GameDifficulty.getByGame(game)).add(graphPoint);

        clickArea.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (gameClickListener != null) {
                    if (graphStyle == GraphStyle.DAILY) {
                        gameClickListener.onGameClicked(event, graphPoint.getX(), graphPoint.getY(), game);
                    } else {
                        gameClickListener.onAverageGameClicked(event, graphPoint.getX(), graphPoint.getY(), game, averageScore, dateText);
                    }
                }
            }
        });

        clickArea.addListener(new ClickListenerWithSound());

    }

    private Actor createPointClickArea(Actor actor) {

        actor.setHeight(CLICK_AREA_SIZE);
        actor.setWidth(CLICK_AREA_SIZE);
        actor.setX(actor.getX() - CLICK_AREA_SIZE / 2 + POINT_SIZE / 2);
        actor.setY(actor.getY() - CLICK_AREA_SIZE / 2 + POINT_SIZE / 2);
        return actor;
    }

    private void addPointClickAreasToGraph() {
        for (Actor area : clickAreas) {
            addActor(area);
        }
    }

    private boolean isFlipOfGraphNeeded(GameDefinition definition) {
        switch (definition) {
            case PURSUIT:
            case KIDNAPPING:
            case IN_THE_DARKNESS:
            case MEMORY_GAME:
                return true;
        }
        return false;
    }

    private void setPropertiesToGraphPoint(Actor actor, float score, int numberOfGames, int index, boolean flippedGraph) {

        float graphPadding = 40f;
        actor.setSize(POINT_SIZE, POINT_SIZE);

        if (numberOfGames < 2) {
            actor.setX((getWidth() / 2) - POINT_SIZE / 2);
        } else {
            actor.setX(graphPadding + ((getWidth() - POINT_SIZE - 2 * graphPadding) / (numberOfGames - 1)) * index);
        }

        if (maxScore == minScore) {
            actor.setY(getHeight() / 2 - POINT_SIZE / 2);
        } else {
            float differenceScore = (flippedGraph ? maxScore - score : score - minScore);
            actor.setY(((((differenceScore) / (maxScore - minScore))) * (getHeight() - POINT_SIZE - 2 * graphPadding)) + graphPadding);
        }
    }

    private boolean isNeededToRedraw(GameDifficulty difficulty) {
        return ((difficulty == lastSelectedGameDifficulty || lastSelectedGameDifficulty == null) && !screenResized);
    }

    private void addMoveEffect(Image point,boolean logEvent) {
        point.addAction(new SequenceAction(moveTo(point.getX(), point.getY(), EFFECTS_SPEED),Actions.run(new Runnable() {
            //For Test
            @Override
            public void run() {
                if(logEvent){
                    logReadyEvent();
                }
            }
        })));
        point.setY(0);
    }

    private void addFadeInEffect(Image line) {
        Color color = line.getColor();
        line.setColor(color.r, color.g, color.b, 0);
        line.addAction(new SequenceAction(Actions.delay(EFFECTS_SPEED), Actions.fadeIn(EFFECTS_SPEED)));
    }

    private void addPointsToGraph(List<Image> points, boolean addEffect) {
        for (int i = 0;i<points.size();i++) {
            Image point = points.get(i);
            if (addEffect) {
                addMoveEffect(point,canLogEvent(i,points.size()));
            }
            addActor(point);
        }
    }

    //For test
    private void logReadyEvent(){
        if(statisticsScreen!=null)
            statisticsScreen.setGraphEvent();
    }

    //For Test
    private boolean canLogEvent(int indexAddPoint,int pointsSize){
        return (indexAddPoint == pointsSize-1);
    }

    private void createAndAddLines(List<Image> lines, boolean addEffect) {

        Image line;
        for (int i = 0; i < lines.size() - 1; i++) {
            float lineLength = getLineSize(lines.get(i).getX(), lines.get(i + 1).getX(), lines.get(i).getY(), lines.get(i + 1).getY());
            float angle = getLineAngle(lines.get(i).getX(), lines.get(i + 1).getX(), lines.get(i).getY(), lines.get(i + 1).getY());

            line = new Image(this.lineTextureRegion);
            line.setSize(LINE_SIZE, lineLength);
            line.setX(lines.get(i).getX() + getRotationShiftX(angle));
            line.setY(lines.get(i).getY() + getRotationShiftY(angle));
            line.setRotation(angle - 90);
            if (addEffect) addFadeInEffect(line);
            addActor(line);
        }
    }

    /*======================= Points and games lists operations =======================*/


    private void createAverageScoresLists() {
        easyAverageScores = statisticsGameScoreResolver.getDailyAverageScores(getGamesByDifficulty(GameDifficulty.EASY));
        mediumAverageScores = statisticsGameScoreResolver.getDailyAverageScores(getGamesByDifficulty(GameDifficulty.MEDIUM));
        hardAverageScores = statisticsGameScoreResolver.getDailyAverageScores(getGamesByDifficulty(GameDifficulty.HARD));
        bonusAverageScores = statisticsGameScoreResolver.getDailyAverageScores(getGamesByDifficulty(GameDifficulty.BONUS));
    }

    private void createPointsLists() {
        easyPoints = new ArrayList<Image>();
        mediumPoints = new ArrayList<Image>();
        hardPoints = new ArrayList<Image>();
        bonusPoints = new ArrayList<Image>();
        clickAreas = new ArrayList<Actor>();
    }

    private void clearPointsLists() {
        easyPoints.clear();
        mediumPoints.clear();
        hardPoints.clear();
        bonusPoints.clear();
        clickAreas.clear();
    }

    private void createGameLists() {

        easyGames = new ArrayList<Game>();
        mediumGames = new ArrayList<Game>();
        hardGames = new ArrayList<Game>();
        bonusGames = new ArrayList<Game>();

        for (Game game : allGames) {
            switch (GameDifficulty.getByGame(game)) {
                case EASY:
                    easyGames.add(game);
                    break;

                case MEDIUM:
                    mediumGames.add(game);
                    break;

                case HARD:
                    hardGames.add(game);
                    break;

                case BONUS:
                    bonusGames.add(game);
                    break;
            }
        }
    }


    private List<Image> getPointsByDifficulty(GameDifficulty difficulty) {
        switch (difficulty) {
            case EASY:
                return easyPoints;
            case MEDIUM:
                return mediumPoints;
            case HARD:
                return hardPoints;
            case BONUS:
                return bonusPoints;
        }
        return null;
    }

    private List<Float> getAverageScoreByDifficulty(GameDifficulty difficulty) {
        switch (difficulty) {
            case EASY:
                return easyAverageScores;
            case MEDIUM:
                return mediumAverageScores;
            case HARD:
                return hardAverageScores;
            case BONUS:
                return bonusAverageScores;
        }
        return null;
    }

    private List<Game> getGamesByDifficulty(GameDifficulty difficulty) {
        switch (difficulty) {
            case EASY:
                return easyGames;
            case MEDIUM:
                return mediumGames;
            case HARD:
                return hardGames;
            case BONUS:
                return bonusGames;
        }
        return null;
    }

    /*======================= Size of component operations =======================*/

    /**resets height in order to recalculate size of new height of graph when resizing*/
    public void resetHeight() {
        setHeight(0);
    }

    //used for determination of width of graph and spacing between points/games
    private float getWidthOfGraphByGames() {
        float size;

        if(graphStyle == GraphStyle.DAILY) {
            size = getSizeOfLongestList(easyGames, mediumGames, hardGames, bonusGames);
        } else {
            size = getSizeOfLongestList(easyAverageScores, mediumAverageScores, hardAverageScores, bonusAverageScores);
        }

        //width of click area plus added spacing
        size *= CLICK_AREA_SIZE * 1.5f;
        return size > MIN_GRAPH_WIDTH ? size : MIN_GRAPH_WIDTH;
    }

    private int getSizeOfLongestList(List<?>... lists) {
        int size = 0;
        for(List list : lists) {
            if(list.size() > size) size = list.size();
        }
        return size;
    }

     /*======================= Calculating graph operations =======================*/

    private float getLineSize(float x1, float x2, float y1, float y2) {

        float lengthX = Math.abs(x1 - x2);
        float lengthY = Math.abs(y1 - y2);

        return (float) Math.sqrt(lengthX * lengthX + lengthY * lengthY);
    }

    private float getLineAngle(float x1, float x2, float y1, float y2) {
        return (float) Math.toDegrees(Math.atan2(y2 - y1, x2 - x1));
    }

    private float getRotationShiftX(float angle) {

        if (angle >= 45) {
            return POINT_SIZE / 2 - LINE_SIZE / 2;
        } else if (angle < 45 && angle >= -45) {
            return POINT_SIZE / 2;
        } else if (angle < -45) {
            return POINT_SIZE / 2 + LINE_SIZE / 2;
        }
        return 0;
    }

    private float getRotationShiftY(float angle) {
        if (angle >= 45) {
            return POINT_SIZE / 2;
        } else if (angle < 45 && angle >= -45) {
            return POINT_SIZE / 2 + LINE_SIZE / 2;
        } else if (angle < -45) {
            return POINT_SIZE / 2;
        }
        return 0;
    }

    /*======================= Gets Methods for testing =======================*/
    public List<Game> getEasyGames() {
        return easyGames;
    }

    public List<Game> getMediumGames() {
        return mediumGames;
    }

    public List<Game> getHardGames() {
        return hardGames;
    }

    public List<Float> getEasyAverageScores() {
        return easyAverageScores;
    }

    public List<Float> getMediumAverageScores() {
        return mediumAverageScores;
    }

    public List<Float> getHardAverageScores() {
        return hardAverageScores;
    }

    public List<Image> getEasyPoints() {
        return easyPoints;
    }

    public List<Image> getMediumPoints() {
        return mediumPoints;
    }

    public List<Image> getHardPoints() {
        return hardPoints;
    }

    public Set<GameDifficulty> getSelectedDifficulties() {
        return selectedDifficulties;
    }

    public float getMaxScore() {
        return maxScore;
    }

    public float getMinScore() {
        return minScore;
    }

    public List<Actor> getClickAreas() {
        return clickAreas;
    }
}
