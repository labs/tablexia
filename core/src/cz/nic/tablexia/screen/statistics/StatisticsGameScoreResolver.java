/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.statistics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.screen.statistics.views.GraphPane;
import cz.nic.tablexia.shared.model.Game;

/**
 * Created by danilov on 11/12/15.
 */
public class StatisticsGameScoreResolver {

    private GameDefinition selectedGameDefinition;

    public StatisticsGameScoreResolver(GameDefinition selectedGameDefinition) {
        this.selectedGameDefinition = selectedGameDefinition;
    }

    public List<Float> getDailyAverageScores(List<Game> games) {
        List<Float> averageScores = new ArrayList<Float>();

        long lastTime = 0;  //time of previous game
        float score = 0;    //sum of score
        int index = 1;      //number of games
        float point = 0;    //point of graph

        if (games != null && games.size() > 0) {
            for (Game game : games) {
                if (games.indexOf(game) == 0) lastTime = game.getGameStartTime();

                if (isNotSameDay(game.getGameStartTime(), lastTime)) {
                    averageScores.add(point);
                    score = 0;
                    index = 1;
                }
                lastTime = game.getGameStartTime();
                score += selectedGameDefinition.getGameResultResolver(game.getGameScoreResolverVersion()).getComparisonScore(game);
                point = score / index;
                index++;
            }
            averageScores.add(point);
        }
        return averageScores;

    }

    private boolean isNotSameDay(long time1, long time2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTimeInMillis(time1);
        cal2.setTimeInMillis(time2);

        return cal1.get(Calendar.DAY_OF_YEAR) != cal2.get(Calendar.DAY_OF_YEAR) ||
                cal1.get(Calendar.MONTH) != cal2.get(Calendar.MONTH) ||
                cal1.get(Calendar.YEAR) != cal2.get(Calendar.YEAR);
    }

    /**Method cycle through provided games (of 1 difficulty) in order to find specific game
    corresponding to clicked average point on graph (searchedIndex is index of that point)*/
    public String getDateForAverageGameScore(int searchedIndex, List<Game> games) {

        if(games.size() == 0 ) return "";

        Game game = games.get(0);
        int indexOfAverageScoreGame = 0;
        long time = game.getGameStartTime();
        int i = 0;

        while (searchedIndex != indexOfAverageScoreGame) {
            game = games.get(i);
            if (isNotSameDay(time, game.getGameStartTime())) indexOfAverageScoreGame++;
            time = game.getGameStartTime();
            i++;
        }
        return game.getGameStartDate(false);
    }


    public float getMaxScore(List<Game> games) {
        float maxScore = -Float.MAX_VALUE;
        for (Game game : games) {
            float score = selectedGameDefinition.getGameResultResolver(game.getGameScoreResolverVersion()).getComparisonScore(game);
            if (score > maxScore) maxScore = score;
        }
        return maxScore;
    }

    public float getMinScore(List<Game> games) {
        float minScore = Float.MAX_VALUE;
        for (Game game : games) {
            float score = selectedGameDefinition.getGameResultResolver(game.getGameScoreResolverVersion()).getComparisonScore(game);
            if (score < minScore) minScore = score;
        }
        return minScore;
    }
}
