/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.statistics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.statistics.enums.GraphStyle;
import cz.nic.tablexia.screen.statistics.interfaces.GameClickListener;
import cz.nic.tablexia.screen.statistics.views.GraphPane;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.Switch;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;
import cz.nic.tablexia.util.ui.button.TablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AdaptiveSizeDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.SpeechDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TouchCloseDialogComponent;



public class StatisticsScreen extends AbstractTablexiaScreen<Void> {
    public static final String UP__BUTTON                   = "up button";
    public static final String DOWN_BUTTON                  = "down button";

    public static final String SWITCH_GRAPH_GAME            = "graph set game";
    public static final String SWITCH_GRAPH_AVERAGE         = "graph set average";
    public static final String GRAPHS                       = "graphs";

    public static final String GRAPH_PANE                   = "graph pane";
    public static final String NOTEBOOK                     = "notebook";
    public static final String SCROLL_PANE                  = "scroll pane" ;
    public static final String CONTENT                      ="content";

    public static final String EVENT_GRAPH_READY            = "event graph ready";

    public static final String DIALOG                       = "point dialog";
    public static final String CUPS_LIST_DIALOG             = "cups list dialog";

    public enum DifficultyButtons {
        EASY(DifficultyDefinition.EASY, "gamedifficulty_easy"),
        MEDIUM(DifficultyDefinition.MEDIUM, "gamedifficulty_medium"),
        HARD(DifficultyDefinition.HARD, "gamedifficulty_hard");
        private String name;



        DifficultyButtons(DifficultyDefinition difficulty, String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }




    private static final ApplicationFontManager.FontType DEFAULT_FONT_TYPE    = ApplicationFontManager.FontType.BOLD_16;
    private static final ApplicationFontManager.FontType DIFFICULTY_FONT_TYPE = ApplicationFontManager.FontType.BOLD_20;
    private static final ApplicationFontManager.FontType TITLE_FONT_TYPE      = ApplicationFontManager.FontType.BOLD_30;

    private static final float GRAPH_DIALOG_WIDTH               = 270;
    private static final float GRAPH_DIALOG_HEIGHT              = 180;
    private static final float COMPONENTS_PADDING               = 10f;
    private static final float COMPONENTS_THRESHOLD_PADDING     = 52f;
    private static final float TITLE_PADDING                    = 70f;
    private static final float DRAWER_SCALE                     = 0.6f;
    private static final float DRAWER_POSITION_X                = -10f;
    private static final float SWITCH_GRAPH_POSITION_X_SCALE    = 0.3f;
    private static final float SWITCH_GRAPH_POSITION_Y_SCALE    = 0.88f;
    private static final float TITLE_POSITION_Y_SCALE           = 0.91f;
    private static final float SWITCH_GRAPH_PADDING_X           = 52f;
    private static final float SWITCH_GRAPH_PADDING_Y           = 5f;
    private static final float DIFFICULTIES_SCALE               = 0.8f;
    private static final float DIFFICULTIES_POSITION_X_SCALE    = 0.52f;
    private static final float DIFFICULTIES_POSITION_Y_SCALE    = 0.9f;
    private static final float NOTEBOOK_POSITION_SCALE          = 0.3f;
    private static final float NOTEBOOK_WIDTH_SCALE             = 0.7f;
    private static final float NOTEBOOK_HEIGHT_SCALE            = 0.85f;
    private static final float BOOKMARKS_GROUP_ALIGN_X          = 0.12f;
    private static final float BOOKMARKS_GROUP_SCALE            = 0.23f;
    private static final float BOOKMARKS_PADDING                = 30f;
    private static final float BOOKMARKS_PADDING_X              = 30f;
    private static final float BOOKMARKS_PADDING_Y              = 0.15f;
    private static final float UP_DOWN_BUTTON_SIZE              = 50;
    private static final float UP_DOWN_BUTTON_SIZE_Y            = 50;
    private static final float BLOCK_IMAGE_X_SCALE              = 0.1f;
    private static final float BLOCK_IMAGE_Y_SCALE              = 0.11f;
    private static final float BLOCK_IMAGE_WIDTH_SCALE          = 0.65f;
    private static final float THREE_ARROW_ALIGN_X              = 0.75f;

    public static final String                                  GFX_PATH                        = "gfx/";
    private static final String                                 STATISTICS_SPEECH               = MFX_PATH + "statistics.mp3";
    private static final String                                 SCREEN_STATE_SWITCH_KEY         = "selected_switch";
    private static final String                                 SCREEN_STATE_GAME_KEY           = "selected_game";
    private static final String                                 SCREEN_STATE_DIFFICULTY_SET     = "1";
    private static final String                                 SCREEN_STATE_DIFFICULTY_UNSET   = "0";
    private static final String                                 ARROW_UP_TEXT                   = "arrow_up";
    private static final String                                 ARROW_DOWN_TEXT                 = "arrow_down";
    private static final String                                 PRESSED_TEXT                    = "_pressed";


    private static final GameDefinition                         DEFAULT_GAME                        = GameDefinition.ROBBERY;
    private static final int                                    DIFFICULTY_BUTTON_SIZE              = 70;
    private static final int                                    DIFFICULTY_LABEL_UNDERLINE_HEIGHT   = 2;
    private static final Color                                  DIFFICULTY_LABEL_HIGHLIGHT_COLOR    = new Color(0xdba77aff);
    private static final Color                                  DIFFICULTY_LABEL_UNDERLINE_COLOR    = Color.BLACK;
    private static final String                                 DIFFICULTY_LABEL_TEXT_PREFIX_KEY    = "gamedifficulty_";
    private static final ApplicationFontManager.FontType        DIALOG_BOLD_FONT                    = ApplicationFontManager.FontType.BOLD_16;

    private Group                       content;
    private Image                       background;
    private TablexiaLabel               titleLabel;
    private Image                       imageDrawer;
    private WidgetGroup                 switchGraphType;
    private Container<Group>            notebook;
    private Stack                       graphs;
    private WidgetGroup                 difficulties;
    private Group                       bookmarksGroup;
    private GraphStyle                  graphStyle;
    private Set<GameDifficulty>         selectedGameDifficulties;
    private GameDifficulty              lastSelectedDifficulty;
    private GameDefinition              selectedGameDefinition;
    private Switch                      switchGraph;
    private ScrollPane                  scrollPane;
    private float                       distanceScrolled;
    private Table                       graphPaneHolder;
    private GraphPane                   graphPane;
    private Map<GameDifficulty, TablexiaButton> difficultyButtons;
    private TablexiaComponentDialog     dialog;
    private TablexiaButton              upButton, downButton;
    private Image                       blockImage;
    private int                         mover;
    private float[]                     bookmarksYPositions;
    private boolean[]                   positionUnVisible;
    private TablexiaLabel               threeArrows;

    //test
    private int                         testGraphEventCount         = 0;
    @Override
    public boolean hasSoftBackButton() {
        return true;
    }

    @Override
    protected void screenPaused(Map<String, String> screenState) {
        screenState.put(GameDifficulty.EASY.name(),    String.valueOf(selectedGameDifficulties.contains(GameDifficulty.EASY) ? SCREEN_STATE_DIFFICULTY_SET : SCREEN_STATE_DIFFICULTY_UNSET));
        screenState.put(GameDifficulty.MEDIUM.name(),  String.valueOf(selectedGameDifficulties.contains(GameDifficulty.MEDIUM) ? SCREEN_STATE_DIFFICULTY_SET : SCREEN_STATE_DIFFICULTY_UNSET));
        screenState.put(GameDifficulty.HARD.name(),    String.valueOf(selectedGameDifficulties.contains(GameDifficulty.HARD) ? SCREEN_STATE_DIFFICULTY_SET : SCREEN_STATE_DIFFICULTY_UNSET));
        screenState.put(GameDifficulty.BONUS.name(),    String.valueOf(selectedGameDifficulties.contains(GameDifficulty.BONUS) ? SCREEN_STATE_DIFFICULTY_SET : SCREEN_STATE_DIFFICULTY_UNSET));
        screenState.put(SCREEN_STATE_GAME_KEY,      Integer.toString(selectedGameDefinition.getGameNumber()));
        screenState.put(SCREEN_STATE_SWITCH_KEY,    String.valueOf(graphStyle == GraphStyle.DAILY ? 0 : 2));
    }

    @Override
    public void setScrollableLayoutFocus() {
        if(scrollPane != null){
            getStage().setScrollFocus(scrollPane);
        }
    }

    @Override
    public void enableScrollableLayoutFocus() {
        if(scrollPane != null){
            scrollPane.setScrollingDisabled(false,true);
            getStage().setScrollFocus(scrollPane);
        }
    }

    @Override
    public void disableScrollableLayoutFocus() {
        if(scrollPane != null){
            scrollPane.setScrollingDisabled(true,true);
            getStage().setScrollFocus(null);
        }
    }

    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        content = new Group();

        content.addActor(background = new TablexiaNoBlendingImage(getApplicationTextureRegion(ApplicationAtlasManager.BACKGROUND_WOODEN)));
        content.addActor(titleLabel = new TablexiaLabel(getText("statistics_label"), new TablexiaLabel.TablexiaLabelStyle(TITLE_FONT_TYPE, Color.BLACK)));
        content.addActor(imageDrawer = new Image(getScreenTextureRegion(GFX_PATH + "cardfile")));
        content.addActor(blockImage = new Image(getScreenTextureRegion(GFX_PATH + "block")));
        // TODO fix other names for tests
        content.setName(CONTENT);

        graphs = new Stack();
        graphs.setName(GRAPHS);
        scrollPane = new ScrollPane(null);
        scrollPane.setName(SCROLL_PANE);
        graphPaneHolder = new Table();

        bookmarksGroup = new Group();
        createUpAndDownButtons();

        selectedGameDifficulties = new HashSet<GameDifficulty>();

        switchGraph = new Switch(getScreenTextureRegion(GFX_PATH + "switch_background"), getScreenTextureRegion(GFX_PATH + "switch_left"), getScreenTextureRegion(GFX_PATH + "switch_center"), getScreenTextureRegion(GFX_PATH + "switch_right"));
        switchGraph.setSwitchSelectedListener(switchListener);
        switchGraph.setDisabledStep(1);


        switchGraphType = new WidgetGroup();

        TablexiaLabel left  = new TablexiaLabel(getText("statistics_games"), new TablexiaLabel.TablexiaLabelStyle(DEFAULT_FONT_TYPE, Color.BLACK));
        TablexiaLabel right = new TablexiaLabel(getText("statistics_average"), new TablexiaLabel.TablexiaLabelStyle(DEFAULT_FONT_TYPE, Color.BLACK));
        left.addListener(leftButtonListener);
        left.setName(SWITCH_GRAPH_GAME);
        right.addListener(rightButtonListener);
        right.setName(SWITCH_GRAPH_AVERAGE);

        switchGraphType.addActor(left);
        switchGraphType.addActor(right);
        switchGraphType.addActor(switchGraph);
        switchGraph.setX(left.getWidth() + SWITCH_GRAPH_PADDING_X / 2);

        right.setX(left.getWidth() + switchGraph.getWidth() + SWITCH_GRAPH_PADDING_X);
        left.setY(SWITCH_GRAPH_PADDING_Y);
        right.setY(SWITCH_GRAPH_PADDING_Y);
        left.setAlignment(Align.bottomRight);
        right.setAlignment(Align.bottomLeft);
        content.addActor(switchGraphType);

        blockImage.setPosition(getSceneWidth() * BLOCK_IMAGE_X_SCALE, getSceneInnerHeight() * BLOCK_IMAGE_Y_SCALE);
        blockImage.setSize(blockImage.getPrefWidth() * BLOCK_IMAGE_WIDTH_SCALE, getStage().getHeight() - upButton.getHeight() - downButton.getHeight() - titleLabel.getHeight() - BOOKMARKS_PADDING);
        bookmarksGroup.setSize(blockImage.getWidth() - blockImage.getWidth()*BOOKMARKS_GROUP_SCALE, blockImage.getHeight() - 2*BOOKMARKS_PADDING_Y*blockImage.getHeight());
        setSelectedGraphType(screenState);

        threeArrows = new TablexiaLabel(">>>", new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_20, Color.DARK_GRAY));
        threeArrows.setPosition(bookmarksGroup.getX() + THREE_ARROW_ALIGN_X *bookmarksGroup.getWidth(), blockImage.getY() + BOOKMARKS_PADDING);
        content.addActor(threeArrows);

        difficultyButtons = new HashMap<GameDifficulty, TablexiaButton>();

        float componentsPadding = isUnderScaleThreshold() ? COMPONENTS_THRESHOLD_PADDING : COMPONENTS_PADDING;
        difficulties = new HorizontalGroup().space(componentsPadding);

        for (GameDifficulty gameDifficulty : GameDifficulty.getVisibleGameDifficultyList()) {
            String diffString = gameDifficulty.name().toLowerCase();
            final Image difficultyLabelUnderLine = new Image(getColorTextureRegion(DIFFICULTY_LABEL_UNDERLINE_COLOR));
            TablexiaButton diffButton = new TablexiaButton(null, false,
                    getScreenTextureRegion(GFX_PATH + "difficultyradio_" + diffString + "_unpressed"),
                    getScreenTextureRegion(GFX_PATH + "difficultyradio_" + diffString + "_pressed"),
                    getScreenTextureRegion(GFX_PATH + "difficultyradio_" + diffString + "_pressed"),
                    null) {

                @Override
                public void setChecked() {
                    super.setChecked();
                    difficultyLabelUnderLine.setVisible(true);
                }

                @Override
                public void setUnchecked() {
                    super.setUnchecked();
                    difficultyLabelUnderLine.setVisible(false);
                }
            }
                    .setButtonSize(DIFFICULTY_BUTTON_SIZE, DIFFICULTY_BUTTON_SIZE)
                    .checkable(true)
                    .setInputListener(difficultyClickListener)
                    .adaptiveSize(true);
            difficulties.addActor(diffButton);
            prepareDifficultyButtonLabel(difficultyLabelUnderLine, diffString);
            diffButton.setUserObject(gameDifficulty);
            diffButton.setName(gameDifficulty.getDescriptionResourceKey());
            difficultyButtons.put(gameDifficulty, diffButton);
        }
        difficulties.setScale(DIFFICULTIES_SCALE);
        content.addActor(difficulties);

        content.addActor(notebook = new Container<Group>());
        NinePatch chartBackground = getScreenPatch(GFX_PATH + "chart_background");
        notebook.setBackground(new NinePatchDrawable(chartBackground));
        notebook.setActor(graphs);
        notebook.fill();
        notebook.setName(NOTEBOOK);

        content.addActor(bookmarksGroup);

        List<GameMenuDefinition> menuDefinitions = GameMenuDefinition.getSortedGameMenuDefinitionList();
        bookmarksYPositions = new float[menuDefinitions.size()];
        positionUnVisible = new boolean[menuDefinitions.size()];
        mover = 0;
        for (int i = 0; i < menuDefinitions.size(); i++) {
            final GameMenuDefinition gameMenuDefinition = menuDefinitions.get(i);

            CustomGroup gameNameVerticalGroup = new CustomGroup(gameMenuDefinition, getColorTextureRegion(DIFFICULTY_LABEL_HIGHLIGHT_COLOR), bookmarksGroup.getWidth(), BOOKMARKS_PADDING_X);
            gameNameVerticalGroup.setUserObject(gameMenuDefinition);
            float yPosition = bookmarksGroup.getHeight() - ((i+1) * (gameNameVerticalGroup.getHeight() + BOOKMARKS_PADDING));
            gameNameVerticalGroup.setPosition(0, yPosition);
            gameNameVerticalGroup.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    selectGame(gameMenuDefinition.getGameDefinition(), 0);
                }
            });

            bookmarksYPositions[i] = yPosition;
            positionUnVisible[i] = !(gameNameVerticalGroup.getY() < (bookmarksGroup.getY()));
            if(gameNameVerticalGroup.getY() < (bookmarksGroup.getY()))gameNameVerticalGroup.setVisible(false);

            gameNameVerticalGroup.setName(gameMenuDefinition.toString());
            bookmarksGroup.addActor(gameNameVerticalGroup);

        }

        getStage().addActor(content);
        getStage().setScrollFocus(scrollPane);
        screenResized(0, 0);
    }



    private void prepareDifficultyButtonLabel(Image underLine, String name) {
        if(!isUnderScaleThreshold()) {
            VerticalGroup diffLabelGroup = new VerticalGroup();
            TablexiaLabel diffLabel = new TablexiaLabel(ApplicationTextManager.getInstance().getText(DIFFICULTY_LABEL_TEXT_PREFIX_KEY + name), new TablexiaLabel.TablexiaLabelStyle(DIFFICULTY_FONT_TYPE, Color.BLACK));
            underLine.getDrawable().setMinWidth(diffLabel.getWidth());
            underLine.getDrawable().setMinHeight(DIFFICULTY_LABEL_UNDERLINE_HEIGHT);
            diffLabelGroup.addActor(diffLabel);
            diffLabelGroup.addActor(underLine);
            difficulties.addActor(diffLabelGroup);
        }
    }

    @Override
    protected void screenVisible(Map<String, String> screenState) {
        if (screenState.containsKey(SCREEN_STATE_GAME_KEY)){
            selectGame(GameDefinition.getGameDefinitionForGameNumber(Integer.parseInt(screenState.get(SCREEN_STATE_GAME_KEY))), 0);
            for (GameDifficulty gameDifficulty: GameDifficulty.getVisibleGameDifficultyList()) {
                setDifficultyButtonChecked(screenState.containsKey(gameDifficulty.name()) && SCREEN_STATE_DIFFICULTY_SET.equals(screenState.get(gameDifficulty.name())), gameDifficulty);
            }
        } else {
            selectFirstGameAndDifficultyAvailable();
        }
    }

    @Override
    protected String prepareIntroMusicPath() {
        return STATISTICS_SPEECH;
    }

    private void createUpAndDownButtons() {
        content.addActor(upButton = createButton(
                (blockImage.getX() + (blockImage.getWidth()/2 - UP_DOWN_BUTTON_SIZE/2)), blockImage.getTop(), ARROW_UP_TEXT,UP__BUTTON, new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        int nextValue = selectedGameDefinition.getGameNumber() - 1;
                        if(nextValue < 1) nextValue = GameDefinition.values().length;
                        selectGame(GameDefinition.getGameDefinitionForGameNumber(nextValue), 1);
                    }
                }));

        content.addActor(downButton = createButton((blockImage.getX() + (blockImage.getWidth()/2 - UP_DOWN_BUTTON_SIZE/2)), blockImage.getY() - UP_DOWN_BUTTON_SIZE_Y, ARROW_DOWN_TEXT,DOWN_BUTTON, new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                int nextValue = selectedGameDefinition.getGameNumber() + 1;
                if(nextValue > GameDefinition.values().length) nextValue = 1;

                selectGame(GameDefinition.getGameDefinitionForGameNumber(nextValue), -1);
            }
        }));
    }

    private TablexiaButton createButton(float positionX, float positionY, String texture, String name,ClickListener listener) {
        TablexiaButton button = new TablexiaButton(null, false, getScreenTextureRegion(GFX_PATH + texture), getScreenTextureRegion(GFX_PATH + texture + PRESSED_TEXT), null, null);
        button.setButtonSize(UP_DOWN_BUTTON_SIZE, UP_DOWN_BUTTON_SIZE_Y)
                .setButtonPosition(positionX, positionY)
                .adaptiveSizePositionFix(false)
                .setEnabled()
                .setInputListener(listener)
                .setName(name);

        return button;
    }

    private void selectFirstGameAndDifficultyAvailable() {
        List<Game> games;
        boolean gameSelected = false;
        for(int i = 1; i < GameMenuDefinition.values().length+1; i++) {
            games = getGames(GameDefinition.getGameDefinitionForGameNumber(i));
            for(Game game : games) {
                GameDifficulty difficulty = GameDifficulty.getByGame(game);
                if(difficulty.hasScore()) {
                    selectGame(GameDefinition.getGameDefinitionForGameNumber(i), 0);
                    setDifficultyButtonChecked(true, difficulty);
                    gameSelected = true;
                    break;
                }
            }
            if(gameSelected){
                mover-=(i-1);
                break;
            }
        }

        if(!gameSelected) {
            selectGame(DEFAULT_GAME, 0);
            setDifficultyButtonChecked(true, GameDifficulty.DEFAULT_DIFFICULTY);
        }
        else{
            moveAllBookmarks();
        }
    }

    private void setDifficultyButtonChecked(boolean selected, GameDifficulty difficulty) {
        TablexiaButton difficultyButton = difficultyButtons.get(difficulty);
        if (difficultyButton != null) {
            difficultyButton.setChecked(selected);
            if(selected) {
                selectedGameDifficulties.add(difficulty);
            }
        } else {
            Log.err(getClass(), "No difficulty button for difficulty definition: " + difficulty);
        }
    }

    /**used when selecting first available or default game */
    private void selectGame(GameDefinition definition, int changeMover) {
        if(selectedGameDefinition == definition) return;
        selectedGameDefinition = definition;
        boolean moveBookmarks = false;

        for(Actor actor : bookmarksGroup.getChildren()) {
            int bookmarkIndex = bookmarksGroup.getChildren().indexOf(actor, true);
            CustomGroup bookmark = (CustomGroup) bookmarksGroup.getChildren().get(bookmarkIndex);
            bookmark.setSelected(bookmarkIndex == (definition.getGameNumber() - 1));
            if(!bookmark.isVisible() && bookmarkIndex == (definition.getGameNumber() - 1)){
                mover += changeMover;
                moveBookmarks = true;
            }
        }

        if(moveBookmarks) moveAllBookmarks();

        //When changing game, scroll to start
        scrollPane.scrollTo(0, 0, 0, 0);
        datasetChanged();
    }

    private void moveAllBookmarks() {
        int i=0;
        int size = bookmarksGroup.getChildren().size;
        for(Actor actor : bookmarksGroup.getChildren()) {
            int bookmarkIndex = bookmarksGroup.getChildren().indexOf(actor, true);
            CustomGroup bookmark = (CustomGroup) bookmarksGroup.getChildren().get(bookmarkIndex);
            if(mover < 0) mover += size;
            int index = (i + mover)%size;
            bookmark.setY(bookmarksYPositions[index]);
            bookmark.setVisible(positionUnVisible[index]);
            i++;
        }
    }

    private void setSelectedGraphType(Map<String,String> screenState) {
        if(screenState.containsKey(SCREEN_STATE_SWITCH_KEY)) graphStyle = (Integer.parseInt(screenState.get(SCREEN_STATE_SWITCH_KEY)) == 0 ? GraphStyle.DAILY : GraphStyle.AVERAGE);
        switchGraph.switchToStep(graphStyle == GraphStyle.AVERAGE ? 2 : 0);
    }

    private InputListener leftButtonListener = new ClickListenerWithSound() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            switchGraph.switchToStep(0);
        }
    };

    private InputListener rightButtonListener = new ClickListenerWithSound() {
        @Override
        public void onClick(InputEvent event, float x, float y) {
            switchGraph.switchToStep(2);
        }
    };

    private Switch.DragSwitchListener.SwitchSelectedListener switchListener = new Switch.DragSwitchListener.SwitchSelectedListener() {
        @Override
        public void stepSelected(int step) {
            graphStyle = (step == 0 ? GraphStyle.DAILY : GraphStyle.AVERAGE);

            Log.info(((Object) this).getClass(), "Selected graph type step " + step + " " + graphStyle);

            scrollPane.scrollTo(0, 0, 0, 0);
            datasetChanged();

        }
    };

    private InputListener difficultyClickListener = new ClickListener() {

        @Override
        public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            GameDifficulty gameDifficulty = (GameDifficulty) event.getListenerActor().getUserObject();
            boolean selected = ((TablexiaButton) event.getListenerActor()).isChecked();

            alternateSelectedDifficulty(gameDifficulty);

            Log.info(((Object) this).getClass(), (selected ? "Selected" : "Unselected") + " difficulty " + gameDifficulty.name());

            datasetChanged();

            //When changing difficulty, leave same scrolled distance
            scrollPane.scrollTo(distanceScrolled, 0, 0, 0);

        }
    };

    private GameClickListener graphClickListener = new GameClickListener() {

        @Override
        public void onAverageGameClicked(InputEvent event, float x, float y, Game game, float averageScore, String dateText) {
            GameDefinition gameDefinition = GameDefinition.getGameDefinitionForGameNumber(game.getGameNumber());
            createPointDetailDialog(x,
                    y,
                    null,
                    gameDefinition.getTitle(),
                    GameDifficulty.getGameDifficultyForDifficultyNumber(game.getGameDifficulty()).getTextDescription(),
                    dateText,
                    String.format("%s %s",
                            ApplicationTextManager.getInstance().getText(GameDefinition.getGameDefinitionForGameNumber(game.getGameNumber()).getStatisticsAverageScoreDialogText()),
                            getCorrectedScore(selectedGameDefinition, game, averageScore)));
        }

        @Override
        public void onGameClicked(InputEvent event, float x, float y, final Game game) {

            GameDefinition gameDefinition = GameDefinition.getGameDefinitionForGameNumber(game.getGameNumber());
            createPointDetailDialog(x,
                    y,
                    new TablexiaDialogComponentAdapter() {

                        private static final int    CUPS_TABLE_SPACING  = 2;
                        private static final float  CUP_WIDTH_RATIO     = 1f / 6;
                        private static final float  CUP_HEIGHT_RATIO    = 1f / 5;

                        private List<Cell> cupCells;

                        @Override
                        public void prepareContent(Cell content) {
                            Table table = new Table();
                            table.defaults().space(CUPS_TABLE_SPACING);

                            cupCells = new ArrayList<Cell>();
                            content.setActor(table);

                            TextureRegion[] cups = getRatingStars(game);


                            for(int i = 0; i < cups.length; i++) {
                                Image cup;
                                cup = new Image(cups[i]);
                                cup.setScaling(Scaling.fit);
                                cupCells.add(table.add(cup));
                            }



                            resize();
                        }

                        @Override
                        public void sizeChanged() {
                            resize();
                        }

                        private void resize() {
                            for (Cell cell: cupCells) {
                                cell.width(getDialog().getWidth() * CUP_WIDTH_RATIO).height(getDialog().getHeight() * CUP_HEIGHT_RATIO);
                            }
                        }



                    },
                    gameDefinition.getTitle(),
                    GameDifficulty.getGameDifficultyForDifficultyNumber(game.getGameDifficulty()).getTextDescription(),
                    game.getGameStartDate(true),
                    String.format("%s %s",
                            ApplicationTextManager.getInstance().getText(gameDefinition.getStatisticsScoreDialogText()),
                            getCorrectedScore(selectedGameDefinition, game)));
        }

        private void createPointDetailDialog(float x, float y, final TablexiaDialogComponentAdapter cupsComponent, String gameTitle, String gameDifficulty, String date, String score) {

            final float absoluteClickX = getGraphDialogsX(x);
            final float absoluteClickY = getGraphDialogsY(y);
            if(isPointOutsideTheView(absoluteClickX)) return;

            ArrayList<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>();
            components.add(new TouchCloseDialogComponent());
            components.add(new AdaptiveSizeDialogComponent());
            components.add(new SpeechDialogComponent(
                                   new Point(absoluteClickX, absoluteClickY),
                                   SpeechDialogComponent.ArrowType.ARROW) {

                               /**Overriden methods, where instead of size of whole stage is used size of
                                * notebook, where are graph shown*/
                               @Override
                               public boolean isDialogOnTheLeftHalfOfScreen(float dialogX) {
                                   return absoluteClickX < (notebook.getX() + (notebook.getWidth()/2));
                               }

                               @Override
                               public boolean isDialogOnTheTopHalfOfScreen(float dialogY) {
                                   return absoluteClickY < (notebook.getY() + (notebook.getHeight()/2));
                               }

                               @Override
                               public boolean isDialogOutOfScreenLeftRight() {
                                   return  getDialog().getOutterRightX() > notebook.getX() + notebook.getWidth() - STAGE_CULLING_PADDING ||
                                           getDialog().getOutterLeftX() < notebook.getX() - STAGE_CULLING_PADDING;
                               }

                               @Override
                               public boolean isDialogOutOfScreenTopDown() {
                                   return  getDialog().getInnerTopY() > notebook.getY() + notebook.getHeight() - STAGE_CULLING_PADDING||
                                           getDialog().getOutterBottomY() < notebook.getY() - STAGE_CULLING_PADDING;
                               }
                           }
            );
            if (cupsComponent != null) {
                components.add(new ResizableSpaceContentDialogComponent());
                components.add(cupsComponent);



            }
            components.add(new ResizableSpaceContentDialogComponent());
            components.add(new TextContentDialogComponent(gameTitle, DIALOG_BOLD_FONT, false));
            components.add(new TextContentDialogComponent(String.format("%s %s", ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.GAME_DIFFICULTY_NAME), gameDifficulty, false), DIALOG_BOLD_FONT, false));
            components.add(new TextContentDialogComponent(date, DIALOG_BOLD_FONT, false));
            components.add(new TextContentDialogComponent(score, DIALOG_BOLD_FONT, false));
            components.add(new ResizableSpaceContentDialogComponent());

            dialog = TablexiaComponentDialogFactory.getInstance().createDialog(
                    getStage(),
                    TablexiaComponentDialog.TablexiaDialogType.BUBBLE_SQUARE,
                    components.toArray(new TablexiaDialogComponentAdapter[]{}));
            dialog.setName(DIALOG);
            dialog.setDialogListener(null);
            dialog.show(GRAPH_DIALOG_WIDTH, GRAPH_DIALOG_HEIGHT);



        }
    };

    private TextureRegion[] getRatingStars(Game game) {
        TextureRegion[] textures = new TextureRegion[3];
        TextureRegion starAchievedTexture = getScreenTextureRegion(GFX_PATH + "ratingstar_achieved");
        TextureRegion starNotAchievedTexture = getScreenTextureRegion(GFX_PATH + "ratingstar_not_achieved");
        int starsAchieved = GameDAO.getGameResult(game).getStarCount();

        for(int i = 0; i < starsAchieved; i++) {
            textures[i] = starAchievedTexture;
        }

        for(int i = 0; i < 3 - starsAchieved; i++) {
            textures[starsAchieved + i] = starNotAchievedTexture;
        }

        return textures;
    }

    /**Checks if surface of point is seen / hidden (50% of it) */
    private boolean isPointOutsideTheView(float clickX) {
        if(clickX >  notebook.getX() + graphs.getX() + graphs.getWidth() || clickX < notebook.getX() + graphs.getX()) return true;
        return false;
    }

    private String getCorrectedScore(GameDefinition gameDefinition, Game game) {
        return gameDefinition.getGameResultResolver(game.getGameScoreResolverVersion()).getFormattedScore(game);
    }

    private String getCorrectedScore(GameDefinition gameDefinition, Game game, float averageScore) {
        return gameDefinition.getGameResultResolver(game.getGameScoreResolverVersion()).getFormattedScore(game);
    }

    /**returning absolute value of clicked position on X axis on screen*/
    private float getGraphDialogsX(float clickX) {
        return notebook.getX() + graphs.getX() + (clickX - scrollPane.getScrollX()) + GraphPane.POINT_SIZE / 2;
    }

    /**returning absolute value of clicked position on Y axis on screen*/
    private float getGraphDialogsY(float clickY) {
        return notebook.getY() + graphs.getY() + clickY + GraphPane.POINT_SIZE / 2 + content.getY();
    }

    private void alternateSelectedDifficulty(GameDifficulty newDifficulty) {
        if (selectedGameDifficulties.contains(newDifficulty)) {
            selectedGameDifficulties.remove(newDifficulty);
        } else {
            selectedGameDifficulties.add(newDifficulty);
        }
        lastSelectedDifficulty = newDifficulty;
    }

    private void datasetChanged() {
        graphs.clear();
        hideDialog();

        if(selectedGameDefinition != null) {
            graphPane = drawGraph(getGames(selectedGameDefinition));

            graphPane.setName(GRAPH_PANE);

            //if is graphPane wide, put it into scrollPane
            if(graphPane.getWidth() <= GraphPane.MIN_GRAPH_WIDTH){
                distanceScrolled = 0;
                graphs.add(graphPane);
            } else {
                graphPaneHolder.clear();
                graphPaneHolder.add(graphPane).width(graphPane.getWidth()).height(graphPane.getHeight());

                distanceScrolled = scrollPane.getScrollX();

                scrollPane.setActor(graphPaneHolder);
                scrollPane.setOverscroll(true, false);

                graphs.add(scrollPane);
                enableScrollableLayoutFocus();
            }
            position();

        }
    }



    private GraphPane drawGraph(List<Game> games) {
        graphPane = new GraphPane(games, graphStyle, this, selectedGameDefinition, selectedGameDifficulties, lastSelectedDifficulty, graphClickListener);
        lastSelectedDifficulty = null;
        return graphPane;
    }


    public void position() {
        titleLabel.setPosition(TITLE_PADDING, getStage().getHeight() * TITLE_POSITION_Y_SCALE);
        background.setBounds(0, 0, getStage().getWidth(), getStage().getHeight());
        ScaleUtil.setImageHeight(imageDrawer, getStage().getHeight() * DRAWER_SCALE);
        imageDrawer.setPosition(DRAWER_POSITION_X, (getStage().getHeight() / 2) - (imageDrawer.getHeight() / 2));
        switchGraphType.setPosition(getStage().getWidth() * SWITCH_GRAPH_POSITION_X_SCALE, getStage().getHeight() * SWITCH_GRAPH_POSITION_Y_SCALE);
        difficulties.setPosition(getStage().getWidth() * DIFFICULTIES_POSITION_X_SCALE, getStage().getHeight() * DIFFICULTIES_POSITION_Y_SCALE);
        notebook.setBounds(getStage().getWidth() * NOTEBOOK_POSITION_SCALE, 0, getStage().getWidth() * NOTEBOOK_WIDTH_SCALE, getStage().getHeight() * NOTEBOOK_HEIGHT_SCALE);
        blockImage.setPosition(getSceneWidth() * BLOCK_IMAGE_X_SCALE, titleLabel.getY() - upButton.getHeight() - blockImage.getHeight());
        threeArrows.setPosition(bookmarksGroup.getX() + THREE_ARROW_ALIGN_X *bookmarksGroup.getWidth(), bookmarksGroup.getY());
        upButton.setButtonPosition(blockImage.getX() + (blockImage.getWidth()/2 - upButton.getWidth()/2), blockImage.getTop());
        downButton.setPosition(blockImage.getX() + (blockImage.getWidth()/2 - downButton.getWidth()/2), blockImage.getY() - downButton.getHeight());
        bookmarksGroup.setPosition(blockImage.getX() + blockImage.getWidth()* BOOKMARKS_GROUP_ALIGN_X,blockImage.getY() + (blockImage.getHeight()/2 - bookmarksGroup.getHeight()/2));
        scrollPane.setSize(graphs.getWidth(), graphs.getHeight());
        if (graphPaneHolder.getCells().size > 0) graphPaneHolder.getCells().get(0).width(graphPane.getWidth()).height(graphs.getHeight());
        if (graphPane != null) graphPaneHolder.setSize(graphPane.getWidth(), graphPane.getHeight());
    }

    private List<Game> getGames(GameDefinition definition) {
        return GameDAO.getGamesForUserAndDefinition(getSelectedUser().getId(), definition);
    }

    private void hideDialog() {
        if(dialog != null) dialog.hide();
    }

    @Override
    public void screenResized(int width, int height) {
        if(graphPane != null) graphPane.resetHeight();
        ScaleUtil.setFullscreenBounds(content, getStage());
        position();
        hideDialog();
    }

    public void setGraphEvent(){
        triggerScenarioStepEvent(EVENT_GRAPH_READY+testGraphEventCount);
        testGraphEventCount++;
    }

}