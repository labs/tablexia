/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.statistics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.util.ui.TablexiaLabel;

public class CustomGroup extends Group {
    private static final int   HIGHLIGHT_IMAGE_HEIGHT_OFFSET    = 5;

    private Image backgroundImage;
    private TablexiaLabel gameNameTablexiaLabel;
    private boolean selected;

    public CustomGroup(GameMenuDefinition gameMenuDefinition, TextureRegion highlightTextureRegion, float backgroundWidth, float bookmarksPaddingX) {
        super();
        gameNameTablexiaLabel = new TablexiaLabel(gameMenuDefinition.getTitle(), new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_16, Color.BLACK));
        gameNameTablexiaLabel.setX(bookmarksPaddingX);
        backgroundImage = new Image(highlightTextureRegion);
        backgroundImage.setHeight(gameNameTablexiaLabel.getHeight() + HIGHLIGHT_IMAGE_HEIGHT_OFFSET);
        backgroundImage.setX(0);
        backgroundImage.setWidth(backgroundWidth);
        backgroundImage.setVisible(false);
        selected = false;

        addActor(backgroundImage);
        addActor(gameNameTablexiaLabel);
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        backgroundImage.setVisible(selected);
        if(selected) gameNameTablexiaLabel.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.BOLD_16, Color.BLACK));
        else gameNameTablexiaLabel.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_16, Color.BLACK));
    }

    public boolean isSelected() {
        return selected;
    }
}
