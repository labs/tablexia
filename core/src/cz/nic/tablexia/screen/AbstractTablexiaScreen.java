/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.async.AsyncTask;
import com.badlogic.gdx.utils.viewport.Viewport;

import net.engio.mbassy.listener.Handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.ApplicationBus.ApplicationEvent;
import cz.nic.tablexia.bus.event.DownloadUserSynchronizationEvent;
import cz.nic.tablexia.bus.event.StartFullSynchronizationEvent;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaAtlasManager;
import cz.nic.tablexia.loader.TablexiaDataManager;
import cz.nic.tablexia.loader.TablexiaSoundManager;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.TablexiaBadgesManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.main.sound.VolumeBar;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.model.screen.ScreenDAO;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.MusicUtil;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.Utility;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;
import cz.nic.tablexia.util.ui.XFillViewport;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;

public abstract class AbstractTablexiaScreen<T> extends ScreenAdapter {

    private class TablexiaMusic implements Music {
        private static final float DEFAULT_VOLUME = 1f;

        private String musicName;
        private Music  music;

        private TablexiaMusic(String musicName, Music music) {
            this.musicName = musicName;
            this.music = music;
        }

        private void updateVolume() {
            if(TablexiaSettings.getInstance().isSoundMuted()) setVolume(0);
            else setVolume(DEFAULT_VOLUME * TablexiaSettings.getInstance().getSoundLevel());
        }

        public String getMusicName() {
            return musicName;
        }

        @Override
        public void play() {
            ApplicationBus.getInstance().subscribe(this);
            updateVolume();
            music.play();
        }

        @Override
        public void pause() {
            music.pause();
        }

        @Override
        public void stop() {
            ApplicationBus.getInstance().unsubscribe(this);
            music.stop();
        }

        @Override
        public boolean isPlaying() {
            return music.isPlaying();
        }

        @Override
        public void setLooping(boolean isLooping) {
            music.setLooping(isLooping);
        }

        @Override
        public boolean isLooping() {
            return music.isLooping();
        }

        @Override
        public void setVolume(float volume) {
            music.setVolume(volume);
        }

        @Override
        public float getVolume() {
            return music.getVolume();
        }

        @Override
        public void setPan(float pan, float volume) {
            music.setPan(pan, volume);
        }

        @Override
        public void setPosition(float position) {
            music.setPosition(position);
        }

        @Override
        public float getPosition() {
            return music.getPosition();
        }

        @Override
        public void dispose() {
            Log.debug(AbstractTablexiaScreen.this.getClass(), "DISPOSING MUSIC: " + getMusicName());
            ApplicationBus.getInstance().unsubscribe(this);
            notDisposedMusics.remove(getMusicName());
            music.dispose();
        }

        @Override
        public String toString() {
            return "MUSIC: " + getMusicName();
        }

        @Override
        public void setOnCompletionListener(OnCompletionListener listener) {
            music.setOnCompletionListener(listener);
        }

        @Handler
        public void handleSoundMuteEvent(TablexiaSettings.SoundMuteEvent soundMuteEvent) {
            updateVolume();
        }
    }

    private static final String TEXT_LOADER_BASE_PATH   = "text/";
    public  static final String MFX_PATH                = "mfx/";
    private static final String ATLAS_FILE_TYPE         = ".atlas";
    private static final String SOUND_FILE_TYPE         = ".mp3";
    private static final String INTRO_SPEECH_MUSIC_NAME = "intro_speech_music";

    private String screenName;
    private String screenAssetsPath;
    private String screenAtlasPath;

    private Stage                 stage;
    private boolean               isScreenPaused;
    private TablexiaAtlasManager  atlasManager;
    private TablexiaSoundManager  soundManager;
    private TextManager           textManager;
    private DataManager           dataManager;
    private ScreenLoadingListener screenLoadingListener;
    private Map<String, String>   screenState;
    private Set<Disposable>       disposables               = new HashSet<Disposable>();
    private Music                 introMusic;
    private Color                 pauseColor                = TablexiaAtlasManager.COLOR_OVERLAY;

    // synchronization helpers
    private static Long           lastPauseTimeForFullSync;
    private static Long           lastTimeForUserSync = System.currentTimeMillis();

    private StopScreenExitReason stopScreenExitReason;
    private Runnable              screenExitAction;

    // run full synchronization 1 hour after pause
    private static final long     FULL_SYNC_DELAY = 60 * 60 * 1000;
    // synchronize actual user on every screen resume but not sooner than 30 sec from last sync
    private static final long     USER_SYNC_DELAY = 30 * 1000;

    public static final float     MUSIC_FADE_OUT_DURATION  = 1f;

    private boolean                                       loadingComplete;
    private boolean                                       loadingStarted;
    private boolean                                       loadAsync;
    private TablexiaAbstractFileManager.AssetsStorageType storageType;

    private boolean backButtonAllowed = false;

    private Map<String, TablexiaMusic> notDisposedMusics;

    private static class TextManager extends TablexiaDataManager<I18NBundle> {

        private static class TextLoader implements AsyncTask<I18NBundle> {

            private String textResourceFileName;
            private Locale locale;

            public TextLoader(String textResourceFileName, Locale locale) {
                this.textResourceFileName = textResourceFileName;
                this.locale = locale;
            }

            @Override
            public I18NBundle call() throws Exception {
                FileHandle baseFileHandle = Gdx.files.internal(textResourceFileName);
                try {
                    return I18NBundle.createBundle(baseFileHandle, locale);
                } catch (MissingResourceException e) {
                    Log.info(((Object) this).getClass(), "No text bundle found: " + baseFileHandle.path());
                    return null;
                }
            }
        }

        public void load(Locale locale, String textFileName) {
            if (textFileName != null) {
                setAsyncTask(new TextLoader(textFileName, locale));
            }
        }
    }

    private class DataManager extends TablexiaDataManager<T> {

        private Map<String, String> initialState;

        public void setInitialState(Map<String, String> initialState) {
            this.initialState = initialState;
        }

        public class DataLoader implements AsyncTask<T> {

            @Override
            public T call() throws Exception {
                // restore screen state
                screenState = TablexiaStorage.getInstance().loadScreenState(AbstractTablexiaScreen.this, getSelectedUser());
                if (initialState != null) {
                    screenState.putAll(initialState);
                }
                return prepareScreenData(screenState);
            }
        }

        public void load() {
            setAsyncTask(new DataLoader());
        }
    }

    public interface ScreenLoadingListener {
        void loadingComplete();
    }

    public static class ScreenInfoEvent implements ApplicationEvent {

        private String infoKey;
        private String infoValue;

        public ScreenInfoEvent(String infoKey, String infoValue) {
            this.infoKey = infoKey;
            this.infoValue = infoValue;
        }

        public String getInfoKey() {
            return infoKey;
        }

        public String getInfoValue() {
            return infoValue;
        }
    }


    public AbstractTablexiaScreen() {
        this(true, TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL);
    }

    public AbstractTablexiaScreen(boolean loadAsync, TablexiaAbstractFileManager.AssetsStorageType storageType) {
        stage = prepareStage();
        this.loadAsync = loadAsync;
        this.storageType = storageType;
        atlasManager = new TablexiaAtlasManager(storageType);
        soundManager = new TablexiaSoundManager(storageType);
        textManager = new TextManager();
        dataManager = new DataManager();

        isScreenPaused = true;
        loadingComplete = false;
        loadingStarted = false;

        notDisposedMusics = new HashMap<String, TablexiaMusic>();
    }

    public void setInitialState(Map<String, String> initialState) {
        dataManager.setInitialState(initialState);
    }

    public TablexiaAbstractFileManager.AssetsStorageType getStorageType() {
        return storageType;
    }

    public boolean isLoadAsync() {
        return loadAsync;
    }

    private String getScreenName() {
        if(screenName == null) screenName = prepareScreenName();
        return screenName;
    }

    private String getScreenAssetsPath() {
        if(screenAssetsPath == null) screenAssetsPath = prepareScreenAssetsPath(getScreenName());
        return screenAssetsPath;
    }

    private String getScreenAtlasPath() {
        if(screenAtlasPath == null) screenAtlasPath = prepareScreenAtlasPath(getScreenAssetsPath(), getScreenName());
        return screenAtlasPath;
    }

    //////////////////////////// SCREEN EXIT CONTROL

    /**
     * All reasons to stop screen exit and show a dialog for
     */
    public enum StopScreenExitReason {
        None(true),
        SoundMuted(false),
        RunningGameQuit(false),
        SettingsNotSaved(false);

        boolean isExitAllowed;

        StopScreenExitReason(boolean isExitAllowed) {
            this.isExitAllowed = isExitAllowed;
        }
    }

    /**
     * Returns false if you can change screen without any problems.
     * Otherwise takes care of showing dialogs and exiting the screen.
     */
    public boolean handleScreenExit(AbstractTablexiaScreen<?> nextScreen, Runnable exitAction) {
        //Get current reason to stop screen exit.
        stopScreenExitReason = getStopScreenExitReason(nextScreen);
        if(stopScreenExitReason.isExitAllowed) {
            return false;
        }

        Log.info(getClass(), "Can't exit screen now. Reason: " + stopScreenExitReason);
        this.screenExitAction = exitAction;

        //Shows dialog for particular reason.
        showExitScreenDialog(prepareExitDialogText(nextScreen, stopScreenExitReason));
        return true;
    }

    private void showExitScreenDialog(String text) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                TablexiaComponentDialog exitDialog = TablexiaComponentDialogFactory.getInstance().createWarningYesNoDialog(
                        text,
                        new ClickListener() {
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                onExitDialogConfirmed(stopScreenExitReason);
                            }
                        },
                        new ClickListener() {
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                onExitDialogCanceled(stopScreenExitReason);
                            }
                        }, true, true);

                exitDialog.show(TablexiaComponentDialogFactory.WARNING_DIALOG_WIDTH, TablexiaComponentDialogFactory.WARNING_DIALOG_HEIGHT);
            }
        });
    }

    /**
     * Get a reason why you cant exit the screen now.
     * - Takes care of a problem when switching to a screen where sound is mandatory but sounds are muted.
     */
    public StopScreenExitReason getStopScreenExitReason(AbstractTablexiaScreen<?> nextScreen) {
        if (nextScreen != null && nextScreen.isSoundMandatory() && TablexiaSettings.getInstance().isSoundMuted()) return StopScreenExitReason.SoundMuted;
        return StopScreenExitReason.None;
    }

    /**
     * Returns a message for particular exit dialog.
     * - Takes care of sound muted dialog
     */
    public String prepareExitDialogText(AbstractTablexiaScreen<?> nextScreen, StopScreenExitReason reason) {
        if(reason == StopScreenExitReason.SoundMuted) return nextScreen.prepareSoundMutedQuestion();
        else return "";
    }

    //Called according to which button user pressed in the dialog
    protected void onExitDialogCanceled(StopScreenExitReason stopScreenExitReason) {}
    protected void onExitDialogConfirmed(StopScreenExitReason stopScreenExitReason) {
        //if user confirmed sound muted dialog, unmute sound and change the screen.
        if(stopScreenExitReason == StopScreenExitReason.SoundMuted) {
            TablexiaSettings.getInstance().toggleSoundMute();
            ApplicationBus.getInstance().publishAsync(new VolumeBar.UnmuteEvent());
            runAfterScreenExitAction();
        }
    }

    public void resetExitDialogState(){}

    /**
     * Runs the action which was stopped by shown exit dialog.
     */
    protected void runAfterScreenExitAction() {
        if(screenExitAction != null) screenExitAction.run();
    }

    //////////////////////////// SOUND

    public boolean isSoundMandatory() {
        return false;
    }

    public String prepareSoundMutedQuestion() {
        return ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SOUND_MUTED_QUESTION);
    }

    //////////////////////////// SCREEN PAUSE

    public boolean canBePaused() {
        return true;
    }

    public boolean canStopActions () {
        return false;
    }

    public boolean canResetState () {
        return true;
    }

    protected boolean canSyncOnResume() {
        return true;
    }

    public boolean isScreenPaused() {
        return canBePaused() && isScreenPaused && canStopActions();
    }

    public Color getPauseColor(){
        return pauseColor;
    }


//////////////////////////// PRELOADER

    private static final String                             PRELOADER_DEFAULT_ASSETS_PATH   = "_preloader/";
    private static final String                             DEFAULT_PRELOADER_SPEECH_FILE   = "help";

    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {

    }

    public boolean hasPreloader() {
        return false;
    }

    public boolean preloaderHasSpeech() {
        return false;
    }

    public void playPreloaderSpeech(final Music speech) {
        if (speech != null) {
            final TablexiaMusic tablexiaSpeech = toTablexiaMusic(INTRO_SPEECH_MUSIC_NAME, speech);
            tablexiaSpeech.play();
            tablexiaSpeech.setOnCompletionListener(new Music.OnCompletionListener() {
                @Override
                public void onCompletion(Music music) {
                    tablexiaSpeech.dispose();
                }
            });
        }
    }

    public String preparePreloaderAtlasPath() {
        // prepare standard preloader atlas path
        return PRELOADER_DEFAULT_ASSETS_PATH + getScreenName() + "/" + getScreenName() + ATLAS_FILE_TYPE;
    }

    public String preparePreloaderSpeechPath() {
        // prepare standard preloader speech path
        return PRELOADER_DEFAULT_ASSETS_PATH + getScreenName() + "/" + Utility.transformLocalAssetsPath(preparePreloaderSpeechFileBaseName()) + SOUND_FILE_TYPE;
    }

    protected String preparePreloaderSpeechFileBaseName() {
        return DEFAULT_PRELOADER_SPEECH_FILE;
    }

//////////////////////////// LOADING LISTENER

    private void notifyLoadingCompleteListeners() {
        if (screenLoadingListener != null) {
            screenLoadingListener.loadingComplete();
        }
    }

    public void setLoadingListener(ScreenLoadingListener listener) {
        screenLoadingListener = listener;
    }

    public void resetLoadingListener() {
        screenLoadingListener = null;
    }


//////////////////////////// STAGE

    private Stage prepareStage() {
        return new Stage(createViewport());
    }

    public Stage getStage() {
        return stage;
    }

    public void setPosition(float positionX, float positionY) {
        stage.addAction(moveTo(positionX, positionY));
    }

    public void addAction(Action action) {
        stage.addAction(action);
    }

    protected Viewport createViewport() {
        return new XFillViewport();
    }

    public boolean isUnderScaleThreshold() {
        return ComponentScaleUtil.isUnderThreshold();
    }

    public void setScrollableLayoutFocus() {}

    public void enableScrollableLayoutFocus() {}

    public void disableScrollableLayoutFocus() {}


//////////////////////////// INPUT PROCESSORS

    public InputProcessor getInputProcessor() {
        return getStage();
    }


//////////////////////////// LIBGDX LIFECYCLE

    @Override
    public final void show() {
		// back button event handling
        ApplicationBus.getInstance().subscribe(this);
        if (!loadingComplete) {
            startLoading();
        }
    }

    @Override
    public final void hide() {
        ApplicationBus.getInstance().unsubscribe(this);
    }

    @Override
    public final void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        if (loadingComplete) {
            performScreenResized(width, height);
        }
    }

    @Override
    public final void render(float delta) {
        if (!loadingComplete) {
            // SCREEN LOADING
            if (loadingStarted) {

                if (loadAsync) {
                    if (!atlasManager.update()) return;
                    if (!soundManager.update()) return;
                    if (!textManager.update()) return;
                    if (!dataManager.update()) return;
                } else {
                    atlasManager.finishLoading();
                    soundManager.finishLoading();
                    textManager.finishLoading();
                    dataManager.finishLoading();
                }

                loadingComplete = true;
                loadingStarted = false;
                performScreenLoaded();
                notifyLoadingCompleteListeners();
            }
        } else {
            // SCREEN RENDERING
            if(stage != null) {
                screenRender(delta);
                if (!isScreenPaused()) {
                    screenAct(delta);
                    stage.act(delta);
                }
                stage.draw();
            }

        }

        if (lastTimeForUserSync != null && hasSynchronizedUser() && canSyncOnResume() && System.currentTimeMillis() - lastTimeForUserSync > USER_SYNC_DELAY) {
            lastTimeForUserSync = System.currentTimeMillis();
            ApplicationBus.getInstance().publishAsync(new DownloadUserSynchronizationEvent(getSelectedUser()));
        }
    }

    private boolean hasSynchronizedUser() {
        return getSelectedUser() != null && getSelectedUser().getUuid() != null && !getSelectedUser().getUuid().isEmpty();
    }

    public void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }

    @Override
    public void pause() {
        if (loadingComplete && canBePaused() && !isScreenPaused()) {
            performScreenPaused();
        }
    }

    @Override
    public final void dispose() {
        performScreenDisposed();
        atlasManager.dispose();
        soundManager.dispose();
        textManager.dispose();
        dataManager.dispose();
        disposeAllMusic();
        disposeAdditionalDisposables();
        stage.dispose();
    }

    private void disposeAdditionalDisposables() {
        for (Disposable d : disposables) {
            d.dispose();
        }
    }

    private void disposeAllMusic() {
        for (Music music : new ArrayList<Music>(notDisposedMusics.values())) {
            Log.err(getClass(), music.toString() + " IS NOT DISPOSED!");
            MusicUtil.fadeOut(music, MUSIC_FADE_OUT_DURATION);
        }
        notDisposedMusics.clear();
    }


//////////////////////////// INTRO MUSIC

    protected void prepareIntroMusic() {
        String introMusicPath = prepareIntroMusicPath();
        if(introMusicPath != null && !introMusicPath.isEmpty()) {
            introMusic = getMusic(introMusicPath);
        }
    }

    private void playIntroMusic() {
        if (introMusic != null && !TablexiaSettings.getInstance().isSoundMuted()) {
            introMusic.play();
        }
    }

    protected Music getIntroMusic() {
        return introMusic;
    }


//////////////////////////// TABLEXIA SCREEN LIFECYCLE

    private final void performScreenLoaded() {
        Log.info(getClass(), "[ ------- Screen Loaded ------- ]");
        backButtonAllowed = false;

        // reset screen state after loading screen with state
        TablexiaStorage.getInstance().resetScreenState(this);

        // Refreshes full resolution rank badges
        TablexiaBadgesManager.getInstance().refreshBadges();

        //Clear the stage (Makes sure every game starts with empty stage)
        getStage().clear();
        screenLoaded(screenState);

        if(getSelectedUser() != null) { //this happens in LoaderScreen, when no user is selected yet
            prepareIntroMusic();
            setScreenViewCount(getSelectedUser().getId(), getClass().getName(), System.currentTimeMillis());
        }
    }

    public final void performScreenVisible() {
        Log.info(getClass(), "[ ------- Screen Visible ------- ]");
        ApplicationBus.getInstance().post(new ScreenVisibleEvent(getClass())).asynchronously();
        screenVisible(screenState);
        performScreenResumed();
        playIntroMusic();

        backButtonAllowed = true;
    }

    private final void performScreenResized(int width, int height) {
        Log.info(getClass(), "[ ------- Screen Resized ------- ]");
        screenResized(width, height);
    }

    protected final void performScreenPaused() {
        if (!isScreenPaused) {
            isScreenPaused = true;
            lastPauseTimeForFullSync = System.currentTimeMillis();
            Log.info(getClass(), "[ ------- Screen Paused ------- ]");
            // save screen state
            Map<String, String> screenState = new HashMap<String, String>();
            screenPaused(screenState);
            TablexiaStorage.getInstance().saveScreenState(this, screenState, getSelectedUser());
        }
    }

    protected final void performScreenResumed() {
        if (isScreenPaused) {
            if (lastPauseTimeForFullSync != null && canSyncOnResume() && System.currentTimeMillis() - lastPauseTimeForFullSync > FULL_SYNC_DELAY) {
                lastPauseTimeForFullSync = null;
                ApplicationBus.getInstance().publishAsync(new StartFullSynchronizationEvent(UserDAO.selectActiveUsers()));
            }
            isScreenPaused = false;
            Log.info(getClass(), "[ ------- Screen Resumed ------- ]");
            screenResumed();
        }
    }

    private final void performScreenDisposed() {
        Log.info(getClass(), "[ ------- Screen Disposed ------- ]");
        screenDisposed();
        TablexiaComponentDialog.DialogVisibleEvent.clearVisibleDialogCount();
    }

    protected void screenLoaded(Map<String, String> screenState) {
    }

    protected void screenVisible(Map<String, String> screenState) {
    }

    protected void screenRender(float delta) {
    }

    protected void screenAct(float delta) {
    }

    protected void screenPaused(Map<String, String> screenState) {
    }

    protected void screenResumed() {
    }

    protected void screenDisposed() {
    }

    protected void screenResized(int width, int height) {
    }


//////////////////////////// TABLEXIA SCREEN CALLBACKS

    // SFX
    protected void prepareScreenSoundAssetNames(List<String> soundsFileNames) {
        // no standard sound file names
    }

    // GFX
    protected void prepareScreenAtlases(List<String> atlasesNames) {
        // loads screen standard atlas
        String atlasPath = getScreenAtlasPath();
        if(atlasPath != null) atlasesNames.add(atlasPath);
    }

    protected String prepareScreenAtlasPath(String screenAssetsPath, String screenName) {
        // prepare standard screen atlas name
        return screenAssetsPath + screenName + ATLAS_FILE_TYPE;
    }

    // TEXT
    protected String prepareScreenTextResourcesAssetName() {
        // prepare standard screen text resource file name
        return getScreenName();
    }

    // DATA
    protected T prepareScreenData(Map<String, String> screenState) {
        return null;
    }


    protected String prepareScreenName() {
        return prepareScreenName(getClass());
    }

    protected String prepareScreenName(Class clazz) {
        String className = clazz.getName().toLowerCase();
        Pattern p = Pattern.compile("(\\w+)screen");
        Matcher m = p.matcher(className);
        if (m.find()) {
            return m.group(1);
        } else {
            throw new RuntimeException("Invalid screen class name! " + className);
        }
    }

    protected String prepareScreenAssetsPath(String screenName) {
        // standard screen assets path
        return "screen/" + screenName + "/";
    }


    protected void printScreenInfo(String infoKey, String infoValue) {
        ApplicationBus.getInstance().post(new ScreenInfoEvent(infoKey, infoValue)).asynchronously();
    }


//////////////////////////// LOADERS

    protected void startLoading() {
        isScreenPaused = true;
        loadingComplete = false;
        loadingStarted = true;

        startAtlasLoader();
        startSoundLoader();
        startTextLoader();
        startDataLoader();
    }

    protected void startAtlasLoader() {
        List<String> atlasesFileNames = new ArrayList<String>();
        prepareScreenAtlases(atlasesFileNames);
        for (String atlasFileName : atlasesFileNames) {
            atlasManager.loadAtlas(atlasFileName);
        }
    }

    private void startSoundLoader() {
        List<String> soundsFileNames = new ArrayList<String>();
        prepareScreenSoundAssetNames(soundsFileNames);
        for (String soundFileName : soundsFileNames) {
            loadSound(createLocalAssetPath(soundFileName));
        }
    }

    protected void loadSound(String soundName) {
        soundManager.loadSound(soundName);
    }

    private void startTextLoader() {
        String textResourcesAssetName = prepareScreenTextResourcesAssetName();
        if (textResourcesAssetName != null) {
            textManager.load(TablexiaSettings.getInstance().getLocale(), TEXT_LOADER_BASE_PATH + getScreenAssetsPath() + textResourcesAssetName);
        }
    }

    protected void startDataLoader() {
        dataManager.load();
    }


//////////////////////////// BACK BUTTON CALLBACK

	public void backButtonPressed() {
        backToInitialScreen();
    }

	@Handler(priority = TablexiaApplication.BackButtonPressed.SCREEN_PRIORITY)
	public void backButtonPressed(TablexiaApplication.BackButtonPressed event) {
        if (event.shouldProcess(TablexiaApplication.BackButtonPressed.SCREEN_PRIORITY)) {
			event.setProcessWithPriority(TablexiaApplication.BackButtonPressed.SCREEN_PRIORITY);
            if (loadingComplete && backButtonAllowed && getClass().equals(TablexiaApplication.getActualScreenClass())) {
                backButtonPressed();
            }
		}
	}

    public boolean hasSoftBackButton() {
        return TablexiaSettings.getInstance().hasSoftBackButton();
    }

    public boolean hasAlternativeControls() {
        return TablexiaSettings.getInstance().hasAlternativeControls();
    }
    
    @Handler
    public void escButtonPressed(TablexiaApplication.EscButtonPressed event){
        if (hasAlternativeControls())
            ApplicationBus.getInstance().publishAsync(new TablexiaApplication.BackButtonPressed());
    }

//////////////////////////// ASSETS ACCESS

    public Animation getAnimationForAtlas(String atlasName, String regionName, int framesCount, float frameDuration) {
        return atlasManager.getAnimationFromAtlas(atlasName, regionName, framesCount, frameDuration);
    }

    public Animation getScreenAnimation(String regionName, int framesCount, float frameDuration) {
        return getAnimationForAtlas(getScreenAtlasPath(), regionName, framesCount, frameDuration);
    }

    public TextureRegion getTextureRegionForAtlas(String atlasName, String regionName, Integer index) {
        return getTextureRegionForAtlas(atlasName, regionName, index, true);
    }

    public TextureRegion getTextureRegionForAtlas(String atlasName, String regionName, Integer index, boolean printErrors) {
        return atlasManager.getTextureRegionFromAtlas(atlasName, regionName, index, printErrors);
    }

    public TextureRegion getTextureRegionForAtlas(String atlasName, String regionName) {
        return getTextureRegionForAtlas(atlasName, regionName, null);
    }

    public TextureRegion getScreenTextureRegion(String regionName) {
        return atlasManager.getTextureRegionFromAtlas(getScreenAtlasPath(), regionName, null);
    }

    public TextureRegion getScreenTextureRegion(String regionName, boolean printErrors) {
        return atlasManager.getTextureRegionFromAtlas(getScreenAtlasPath(), regionName, null, printErrors);
    }

    public TextureRegion getScreenTextureRegion(String regionName, Integer index) {
        return atlasManager.getTextureRegionFromAtlas(getScreenAtlasPath(), regionName, index);
    }

    public NinePatch    getScreenPatch(String patchName) {
        return atlasManager.getPatchFromAtlas(getScreenAtlasPath(), patchName);
    }

    public TextureRegion getApplicationTextureRegion(String textureName) {
        return ApplicationAtlasManager.getInstance().getTextureRegion(textureName);
    }

    public TextureRegion getColorTextureRegion(Color color) {
        return ApplicationAtlasManager.getInstance().getColorTextureRegion(color);
    }

    public Sound getSound(String soundName, boolean useLocalAssetPath) {
        return soundManager.getSound(useLocalAssetPath ? createLocalAssetPath(soundName) : soundName);
    }

    public Sound getSound(String soundName) {
        return getSound(soundName, true);
    }

    public Music getMusic(String musicName) {
        Music music = notDisposedMusics.get(musicName);
        if (music == null) {
            music = toTablexiaMusic(musicName, Gdx.audio.newMusic(TablexiaAbstractFileManager.getFileStoragePathFileHandle(storageType, getScreenAssetsPath() + Utility.transformLocalAssetsPath(musicName))));
        }
        return music;
    }

    private TablexiaMusic toTablexiaMusic(String musicName, Music music) {
        Music loadedMusic = notDisposedMusics.get(musicName);
        if (loadedMusic != null) {
            loadedMusic.dispose();
        }

        TablexiaMusic tablexiaMusic = new TablexiaMusic(musicName, music);
        notDisposedMusics.put(musicName, tablexiaMusic);
        return tablexiaMusic;
    }

    protected String prepareIntroMusicPath() {
        return null;
    }

    public String getText(String key) {
        return textManager.getResult().get(key);
    }

    public FileHandle getFile(String fileName) {
        return TablexiaAbstractFileManager.getFileStoragePathFileHandle(storageType, getScreenAssetsPath() + Utility.transformLocalAssetsPath(fileName));
    }

    public String getFormattedText(String key, Object... args) {
        return textManager.getResult().format(key, args);
    }

    public T getData() {
        return dataManager.getResult();
    }

    public void setScreenViewCount(long userId, String screenName, long time) {
        ScreenDAO.setScreenVisitTime(userId, screenName, time);
    }

    private int getScreenViewCount(long userId, String screenName) {
        return ScreenDAO.getScreenViewCount(userId, screenName);
    }

//////////////////////////// SCREEN SCENE

    public float getSceneLeftX() {
        return TablexiaSettings.getSceneLeftX(getStage());
    }

    public float getSceneRightX() {
        return TablexiaSettings.getSceneRightX(getStage());
    }

    public float getSceneOuterBottomY() {
        return TablexiaSettings.getSceneOuterBottomY(getStage());
    }

    public float getSceneInnerBottomY() {
        return TablexiaSettings.getSceneInnerBottomY(getStage());
    }

    public float getSceneInnerTopY() {
        return TablexiaSettings.getSceneInnerTopY(getStage());
    }

    public float getSceneOuterTopY() {
        return TablexiaSettings.getSceneOuterTopY(getStage());
    }

    public float getSceneWidth() {
        return TablexiaSettings.getSceneWidth(getStage());
    }

    public float getSceneOuterHeight() {
        return TablexiaSettings.getSceneOuterHeight(getStage());
    }

    public float getSceneInnerHeight() {
        return TablexiaSettings.getSceneInnerHeight(getStage());
    }

    public void setActorToFullScene(Actor actor) {
        ScaleUtil.setFullScene(actor, getStage());
    }


//////////////////////////// SCREEN VIEWPORT

    public float getViewportLeftX() {
        return TablexiaSettings.getViewportLeftX(getStage());
    }

    public float getViewportRightX() {
        return TablexiaSettings.getViewportRightX(getStage());
    }

    public float getViewportBottomY() {
        return TablexiaSettings.getViewportBottomY(getStage());
    }

    public float getViewportTopY() {
        return TablexiaSettings.getViewportTopY(getStage());
    }

    public float getViewportWidth() {
        return TablexiaSettings.getViewportWidth(getStage());
    }

    public float getViewportHeight() {
        return TablexiaSettings.getViewportHeight(getStage());
    }


//////////////////////////// UTILITY

    protected void backToInitialScreen() {
        ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(TablexiaSettings.INITIAL_SCREEN, TablexiaApplication.ScreenTransaction.FADE)).asynchronously();
    }

    protected String createLocalAssetPath(String fileName) {
        return getScreenAssetsPath() + Utility.transformLocalAssetsPath(fileName);
    }

    protected User getSelectedUser() {
        return TablexiaSettings.getInstance().getSelectedUser();
    }


//////////////////////////// EVENT

    public static class ScreenVisibleEvent implements ApplicationEvent {
        private final Class<? extends AbstractTablexiaScreen> actualScreenClass;

        private ScreenVisibleEvent(Class<? extends AbstractTablexiaScreen> actualScreenClass) {
            this.actualScreenClass = actualScreenClass;
        }

        public Class<? extends AbstractTablexiaScreen> getActualScreenClass() {
            return actualScreenClass;
        }
    }

//////////////////////////// DIALOG LISTENER

    protected TablexiaComponentDialog.ShowDialogListener dialogListener = new TablexiaComponentDialog.ShowDialogListener() {
        @Override
        public void dialogShowed() {
            disableScrollableLayoutFocus();
        }

        @Override
        public void dialogHided() {
            enableScrollableLayoutFocus();
        }
    };

/////////////////////// AUTOMATED TESTING
    public static <T> void triggerScenarioStepEvent(T step) {
        Log.info("trigger event", step.toString());
        ApplicationBus.getInstance().publishAsync(new ScenarioStepEvent<T>(step));
    }

    public static class ScenarioStepEvent<T> implements ApplicationBus.ApplicationEvent {
        private T step;

        public ScenarioStepEvent(T step) {
            this.step = step;
        }

        public T getStep() {
            return step;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AbstractTablexiaScreen<?>) {
            return getClass() == ((AbstractTablexiaScreen) obj).getClass();
        }
        return false;
    }
}
