/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.about.assets;

public enum LicenseDefinition {
    
    APACHE_2_0("Apache 2.0 License", "https://www.apache.org/licenses/LICENSE-2.0"),
    MIT("MIT License", "https://opensource.org/licenses/MIT"),
    BSD("BSD License", "https://opensource.org/licenses/BSD-3-Clause"),
    ECLIPSE_1_0("Eclipse Public License - v 1.0", "https://www.eclipse.org/legal/epl-v10.html"),
    SENTRY("Copyright (c) 2016 Functional Software, Inc.", "https://github.com/getsentry/sentry-java/blob/master/LICENSE"),
    ROBOWM("RoboVM(TM)", "https://robovm.com/license/");
    
    String name, url;


    LicenseDefinition(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getURL() {
        return url;
    }
}
