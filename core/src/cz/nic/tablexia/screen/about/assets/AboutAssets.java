/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.about.assets;

/**
 * Created by vitaliy on 6.11.15.
 */
public class AboutAssets {

    public static final String GFX_PATH = "gfx/";
    /*
    Images
     */
    public static final String LOGO_TABLEXIA = GFX_PATH + "tablexia";
    public static final String LOGO_NIC = GFX_PATH + "logo_nic";
    public static final String MAIL = GFX_PATH + "mail";
    public static final String TWITTER = GFX_PATH + "twitter";
    public static final String FACEBOOK = GFX_PATH + "facebook";
    public static final String CEF_LOGO = GFX_PATH + "cef_logo";
    /*
      Text from properties
     */
    public static final String ABOUT_INFO_1 = "about_info_1";
    public static final String ABOUT_INFO_2 = "about_info_2";
    public static final String ABOUT_INFO_3 = "about_info_3";
    public static final String ABOUT_INFO_4 = "about_info_4";

    public static final String PARAGRAPH_1 = "paragraph_1";
    public static final String PARAGRAPH_2 = "paragraph_2";
    public static final String PARAGRAPH_2_DETAILS = "paragraph_2_details";
    public static final String PARAGRAPH_3 = "paragraph_3";
    public static final String PARAGRAPH_3_DETAILS_1_1 = "paragraph_3_details_1_1";
    public static final String PARAGRAPH_3_DETAILS_1_2 = "paragraph_3_details_1_2";
    public static final String PARAGRAPH_3_DETAILS_2 = "paragraph_3_details_2";
    public static final String PARAGRAPH_3_DETAILS_2_2 = "paragraph_3_details_2_2";
    public static final String PARAGRAPH_3_DETAILS_3_1 = "paragraph_3_details_3_1";
    public static final String PARAGRAPH_3_DETAILS_3_2 = "paragraph_3_details_3_2";
    public static final String PARAGRAPH_4 = "paragraph_4";
    public static final String PARAGRAPH_4_DETAILS = "paragraph_4_details";


    public static final String PSYCHOLOGICAL_CONCEPT = "psychological_concept";
    public static final String PSYCHOLOGICAL_CONCEPT_DETAILS = "psychological_concept_details";
    public static final String DEVELOPERS = "developers";
    public static final String GAME_CONCEPT = "game_concept";
    public static final String GAME_CONCEPT_DETAILS = "game_concept_details";
    public static final String DEVELOPERS_DETAILS_1 = "developers_details_1";
    public static final String DEVELOPERS_DETAILS_2 = "developers_details_2";
    public static final String ILLUSTRATORS = "illustrators";
    public static final String ILLUSTRATORS_DETAILS = "illustrators_details";


}
