/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.about.assets;

public enum LibraryDefinition {
    
    LIBGDX("libGDX", "https://libgdx.badlogicgames.com/", LicenseDefinition.APACHE_2_0),
    MBASSADOR("mbassador", "https://github.com/bennidi/mbassador", LicenseDefinition.MIT),
    JCABI_MANIFESTS("jcabi-manifests", "http://manifests.jcabi.com/", LicenseDefinition.BSD),
    SQLITE("sqlite-jdbc", "https://github.com/xerial/sqlite-jdbc", LicenseDefinition.APACHE_2_0),
    SQLDROID("sqldroid", "https://github.com/SQLDroid/SQLDroid", LicenseDefinition.MIT),
    JUNIT("JUnit", "http://junit.org/", LicenseDefinition.ECLIPSE_1_0),
    ROBOVM("robovm", "https://robovm.com/", LicenseDefinition.ROBOWM),
    LIBGDX_UTILS("libgdx-utils", "https://bitbucket.org/dermetfan/libgdx-utils/wiki/Home", LicenseDefinition.APACHE_2_0),
    GUAVA("guava", "https://code.google.com/p/guava-libraries/", LicenseDefinition.APACHE_2_0),
    ZXING("zxing", "https://github.com/zxing/zxing", LicenseDefinition.APACHE_2_0),
    SENTRY("sentry-java", "https://github.com/getsentry/sentry-java", LicenseDefinition.SENTRY);

    private String            name;
    private String            url;
    private LicenseDefinition licence;

    LibraryDefinition(String name, String url, LicenseDefinition licence) {
        this.name = name;
        this.url = url;
        this.licence = licence;
    }


    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public LicenseDefinition getLicence() {
        return licence;
    }

}
