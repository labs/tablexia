/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.about;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.screen.about.assets.AboutAssets;


public enum Links {

    NIC_CS("CZ.NIC", "https://www.nic.cz/setlang/?language=cs", LinkGroups.OTHER),
    NIC_EN("CZ.NIC", "https://www.nic.cz/setlang/?language=en", LinkGroups.OTHER),
    TABLEXIA("www.tablexia.cz", "https://www.tablexia.cz/cs/", LinkGroups.OTHER),
    TABLEXIA_SK("www.tablexia.cz", "https://www.tablexia.cz/sk/", LinkGroups.OTHER),
    TABLEXIA_DE("www.tablexia.cz", "https://www.tablexia.cz/de/", LinkGroups.OTHER),
    TABLEXIA_GIT("GitLab CZ.NIC", "https://gitlab.labs.nic.cz/labs/tablexia", LinkGroups.OTHER),
    TABLEXIA_FACEBOOK("Tablexia Facebook", "https://www.facebook.com/tablexia", LinkGroups.SOCIAL),
    TABLEXIA_MAIL("Tablexia Mail", "mailto:info@tablexia.cz", LinkGroups.SOCIAL),
    TABLEXIA_TWITTER("Tablexia Twitter", "https://twitter.com/tablexia", LinkGroups.SOCIAL),
    TABLEXIA_ZOOU_CS(AboutAssets.PARAGRAPH_3_DETAILS_3_2, "https://www.tablexia.cz/cs/zoou/", LinkGroups.OTHER),
    TABLEXIA_ZOOU_SK(AboutAssets.PARAGRAPH_3_DETAILS_3_2, "https://www.tablexia.cz/sk/zoou/", LinkGroups.OTHER),
    TABLEXIA_ZOOU_DE(AboutAssets.PARAGRAPH_3_DETAILS_3_2, "https://www.tablexia.cz/de/zoou/", LinkGroups.OTHER);
    
    private String linkName;
    private String linkAddress;
    private LinkGroups linkGroup;


    Links(String linkName, String linkAddress, LinkGroups linkGroup) {
        this.linkName = linkName;
        this.linkAddress = linkAddress;
        this.linkGroup = linkGroup;
    }

    public String getLinkAddress() {
        return linkAddress;
    }

    public String getLinkName() {
        return linkName;
    }

    public LinkGroups getLinkGroup() {
        return linkGroup;
    }

    public static List<Links> getGroupLinks(LinkGroups group) {
        List<Links> linksList = new ArrayList<Links>();
        for (Links link : Links.values()) {
            if (link.getLinkGroup() == group) {
                linksList.add(link);
            }
        }
        return linksList;
    }
}
