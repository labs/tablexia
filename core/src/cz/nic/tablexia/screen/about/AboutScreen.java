/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.about;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.about.assets.AboutAssets;
import cz.nic.tablexia.screen.about.assets.LibraryDefinition;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.button.TablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AdaptiveSizeDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.AlertOnShowDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.CloseButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DimmerDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ScrollPaneDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.ViewportMaximumSizeComponent;
import cz.nic.tablexia.util.ui.scrollpane.ScrollPaneWithBars;

public class AboutScreen extends AbstractTablexiaScreen<Void> {

    public enum LocaleLinks{
        CS(TablexiaSettings.LocaleDefinition.cs_CZ.getLocale(), Links.TABLEXIA, Links.NIC_CS, Links.TABLEXIA_ZOOU_CS),
        SK(TablexiaSettings.LocaleDefinition.sk_SK.getLocale(), Links.TABLEXIA_SK, Links.NIC_CS, Links.TABLEXIA_ZOOU_SK),
        DE(TablexiaSettings.LocaleDefinition.de_DE.getLocale(), Links.TABLEXIA_DE, Links.NIC_EN, Links.TABLEXIA_ZOOU_DE);

        private final Locale    locale;
        private final Links     tablexiaWeb;
        private final Links     nicWeb;
        private final Links     zoou;

        LocaleLinks(Locale locale, Links tablexiaWeb, Links nicWeb, Links zoou){
            this.locale = locale;
            this.tablexiaWeb = tablexiaWeb;
            this.nicWeb = nicWeb;
            this.zoou = zoou;
        }

        public static LocaleLinks getLocaleLinksByLocale(Locale locale){
            for (LocaleLinks current: values()){
                if(current.locale == locale)
                    return current;
            }

            throw new RuntimeException("Couldn't get links for locale " + locale);
        }

        public Links getTablexiaWeb() {
            return tablexiaWeb;
        }

        public Links getNicWeb() {
            return nicWeb;
        }

        public Links getZOOU() {
            return zoou;
        }
    }

    private static final Color   DEFAULT_FONT_COLOR       = TablexiaSettings.getDefaultFontColor();
    private static final Color   DEFAULT_BACKGROUND_COLOR = TablexiaSettings.getDefaultBackgroundColor();
    private static final Color   DEFAULT_LINK_COLOR       = new Color(0.27f, 0.53f, 0.8f,1);

    private static final float CONTENT_RELATIVE_WIDTH   = 0.9f;
    private static final float CONTENT_RELATIVE_HEIGHT  = 0.85f;
    private static final int   LOGO_CEF_MAX_HEIGHT      = 45;
    private static final int   LOGO_TABLEXIA_MAX_HEIGHT = 60;
    private static final int   LOGO_CZ_NIC_MAX_HEIGHT   = 20;
    private static final int   SOCIAL_MAX_HEIGHT        = 40;
    private static final int   CONTENT_PAD              = 10;
    private static final int   SMALL_PAD                = 5;
    private static final int   BIG_PAD                  = 20;

    private static final int   CLICKABLE_LABLE_WIDTH_MODIFICATOR = 2;

    private static final int    DIALOG_WIDTH = 500;
    private static final int    DIALOG_HEIGHT = 400;
    private static final int    DIALOG_CONTENT_PADDING = 10;    

    private static final int DEFAULT_FONT_PAD  = 5;
    private static final int BIG_FONT_PAD      = 10;

    private static final String SCREEN_STATE_SCROLL_POSITION_Y = "scrollPositionY";
    private static final String SEPARATOR = ",";

    private final TablexiaLabel.TablexiaLabelStyle LABEL_STYLE_18      = new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_18, DEFAULT_FONT_COLOR);
    private final TablexiaLabel.TablexiaLabelStyle BOLD_LABEL_STYLE_18 = new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.BOLD_18, DEFAULT_FONT_COLOR);
    private final TablexiaLabel.TablexiaLabelStyle BOLD_LABEL_STYLE_20 = new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.BOLD_20, DEFAULT_FONT_COLOR);
    private final TablexiaLabel.TablexiaLabelStyle BOLD_LABEL_STYLE_26 = new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.BOLD_26, DEFAULT_FONT_COLOR);

    private Table                       backgroundTable;
    private Container<TablexiaButton>   contentStack;
    private Table                       content;

    private ScrollPane contentScrollPane;
    private float      contentWidth;
    private float      contentHeight;

    private LabelClickListener labelClickListener;

    @Override
    public boolean hasSoftBackButton() {
        return true;
    }

    @Override
    protected Viewport createViewport() {
        return new ExtendViewport(TablexiaSettings.getWorldSize(), TablexiaSettings.getMinWorldHeight());
    }

    @Override
    protected void screenPaused(Map<String, String> screenState) {
        screenState.put(SCREEN_STATE_SCROLL_POSITION_Y, String.valueOf(contentScrollPane.getScrollPercentY()));
    }

    @Override
    public void setScrollableLayoutFocus() {
        if(contentScrollPane != null){
            getStage().setScrollFocus(contentScrollPane);
        }
    }

    @Override
    public void enableScrollableLayoutFocus() {
        if(contentScrollPane != null){
            contentScrollPane.setScrollingDisabled(true, false);
            getStage().setScrollFocus(contentScrollPane);
        }
    }

    @Override
    public void disableScrollableLayoutFocus() {
        if(contentScrollPane != null){
            contentScrollPane.setScrollingDisabled(true,true);
            getStage().setScrollFocus(null);
        }
    }

    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        TextureRegionDrawable backgroundTexture = new TextureRegionDrawable(getColorTextureRegion(DEFAULT_BACKGROUND_COLOR));
        backgroundTable = new Table();
        contentStack = new Container<TablexiaButton>();
        backgroundTable.background(backgroundTexture);
        backgroundTable.setFillParent(true);
        prepareScreenLayout();

        if (screenState.containsKey(SCREEN_STATE_SCROLL_POSITION_Y)) {
            contentScrollPane.pack();
            contentScrollPane.setScrollPercentY(Float.valueOf(screenState.get(SCREEN_STATE_SCROLL_POSITION_Y)));
            contentScrollPane.updateVisualScroll();
        }

    }


    /**
     * Prepare screen content layout
     */
    private void prepareScreenLayout() {
        contentWidth = CONTENT_RELATIVE_WIDTH * getViewportWidth();
        contentHeight = CONTENT_RELATIVE_HEIGHT * getViewportHeight();

        content = new Table();
        content.setSize(contentWidth, contentHeight);
        labelClickListener = new LabelClickListener();

        prepareContent();

        contentScrollPane = new ScrollPaneWithBars(content, false, true);
        contentScrollPane.setScrollingDisabled(true, false);
        contentScrollPane.setFillParent(true);

        contentStack.setFillParent(true);
        contentStack.align(Align.bottomLeft);

        getStage().addActor(backgroundTable);
        getStage().addActor(contentScrollPane);
        getStage().addActor(contentStack);
        getStage().setScrollFocus(contentScrollPane);

        contentStack.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes(), true);
    }


    private void prepareContent() {
        LocaleLinks localeLinks = LocaleLinks.getLocaleLinksByLocale(TablexiaSettings.getInstance().getLocale());

        Links nicWeb = localeLinks.getNicWeb();
        Links tablexiaWeb = localeLinks.getTablexiaWeb();
        Links tablexiaZOOU = localeLinks.getZOOU();

        addImageToContainer(AboutAssets.LOGO_TABLEXIA, LOGO_TABLEXIA_MAX_HEIGHT, tablexiaWeb);
        content.row();

        addDefaultTextToContainer(AboutAssets.ABOUT_INFO_1);

        //add cz.nic logo

        addImageToContainer(AboutAssets.LOGO_NIC, LOGO_CZ_NIC_MAX_HEIGHT, nicWeb);
        content.row();

        //add text from properties
        addDefaultTextToContainer(AboutAssets.ABOUT_INFO_2);

        addDefaultTextToContainer(AboutAssets.ABOUT_INFO_3);

        addParagraphName(AboutAssets.PARAGRAPH_1);

        addBoldTextToContainer(AboutAssets.PSYCHOLOGICAL_CONCEPT);
        addDefaultTextToContainer(AboutAssets.PSYCHOLOGICAL_CONCEPT_DETAILS);

        addBoldTextToContainer(AboutAssets.DEVELOPERS);
        addDefaultTextToContainer(AboutAssets.DEVELOPERS_DETAILS_1);
        addDefaultTextToContainer(AboutAssets.DEVELOPERS_DETAILS_2);

        addBoldTextToContainer(AboutAssets.GAME_CONCEPT);
        addDefaultTextToContainer(AboutAssets.GAME_CONCEPT_DETAILS);

        addBoldTextToContainer(AboutAssets.ILLUSTRATORS);
        addDefaultTextToContainer(AboutAssets.ILLUSTRATORS_DETAILS);

        addParagraphName(AboutAssets.PARAGRAPH_2);
        addDefaultTextToContainer(AboutAssets.PARAGRAPH_2_DETAILS);

        addParagraphName(AboutAssets.PARAGRAPH_3);
        addDefaultTextToContainer(AboutAssets.PARAGRAPH_3_DETAILS_1_1);

        addLabelLink(Links.TABLEXIA_GIT);

        addDefaultTextToContainer(AboutAssets.PARAGRAPH_3_DETAILS_1_2);

        TablexiaLabel librariesDetails = createStyledLabel(getText(AboutAssets.PARAGRAPH_3_DETAILS_2), LABEL_STYLE_18);
        TablexiaLabel libClick = createStyledLabel(getText(AboutAssets.PARAGRAPH_3_DETAILS_2_2), new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.BOLD_20, DEFAULT_LINK_COLOR));
        libClick.addListener(new ClickListenerWithSound() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                List<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>() {
                    {
                        add(new CenterPositionDialogComponent());
                        add(new DimmerDialogComponent());
                        add(new ViewportMaximumSizeComponent());
                        add(new AdaptiveSizeDialogComponent());

                        add(new ResizableSpaceContentDialogComponent());
                        add(new ScrollPaneDialogComponent(createLibrariesParagraph()));

                        add(new ResizableSpaceContentDialogComponent());
                        add(new CloseButtonContentDialogComponent(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_CLOSE)));
                        add(new AlertOnShowDialogComponent());
                        add(new AdaptiveSizeDialogComponent());
                        add(new CenterPositionDialogComponent());
                    }
                };

                TablexiaComponentDialog dialog = TablexiaComponentDialogFactory.getInstance().createDialog(components.toArray(new TablexiaDialogComponentAdapter[]{}));
                dialog.setDialogListener(dialogListener);
                dialog.show(DIALOG_WIDTH, DIALOG_HEIGHT, false);
            }
        });
    
        content.add(librariesDetails).center().width(contentWidth).padTop(DEFAULT_FONT_PAD).padBottom(DEFAULT_FONT_PAD);
        content.row();
        content.add(libClick).center().width(libClick.getWidth() * CLICKABLE_LABLE_WIDTH_MODIFICATOR).padTop(DEFAULT_FONT_PAD).padBottom(DEFAULT_FONT_PAD);
        content.row();

        addDefaultTextToContainer(AboutAssets.PARAGRAPH_3_DETAILS_3_1);
        addLabelLink(tablexiaZOOU, getText(tablexiaZOOU.getLinkName()));

        addParagraphName(AboutAssets.PARAGRAPH_4);
        addDefaultTextToContainer(AboutAssets.PARAGRAPH_4_DETAILS);

        addSocial();

        addDefaultTextToContainer(AboutAssets.ABOUT_INFO_4);

        addBigLabelLink(tablexiaWeb);

        addImageToContainer(AboutAssets.CEF_LOGO, LOGO_CEF_MAX_HEIGHT, null);
        content.row();

        content.pad(CONTENT_PAD);
    }


    /**
     * Add default text
     *
     * @param textID
     */
    private void addDefaultTextToContainer(String textID) {
        addTextToContainer(textID, LABEL_STYLE_18, false);
        content.row();
    }

    private void addBoldTextToContainer(String textID) {
        addTextToContainer(textID, BOLD_LABEL_STYLE_18, false);
        content.row();
    }

    private void addParagraphName(String textID) {
        addTextToContainer(textID, BOLD_LABEL_STYLE_26, false);
        content.row();
    }

    private void addBigBoldTextToContainer(String textID) {
        addTextToContainer(textID, BOLD_LABEL_STYLE_20, true);
        content.row();
    }

    /**
     * @param textID
     * @param style
     * @param bigFont
     */
    private void addTextToContainer(String textID, TablexiaLabel.TablexiaLabelStyle style, boolean bigFont) {
        float labelPad = DEFAULT_FONT_PAD;
        if (bigFont) {
            labelPad = BIG_FONT_PAD;
        }
        content.add(createStyledLabel(getText(textID), style)).width(contentWidth - SMALL_PAD).padTop(labelPad).padBottom(labelPad);
    }


    /**
     * Add image to content container
     *
     * @param path      path to image in assets folder
     * @param maxHeight height limit
     */
    private void addImageToContainer(String path, int maxHeight, Links link) {
        Actor actor = new Image(getScreenTextureRegion(path));
        float actorHeight = actor.getHeight();
        float actorWidth = actor.getWidth();
        float actorSizeRatio = actorWidth / actorHeight;
        if (actorHeight > maxHeight) {
            actorHeight = maxHeight;
            actorWidth = actorSizeRatio * actorHeight;
        }
        if (link != null) {
            actor.setUserObject(link);
            actor.addListener(labelClickListener);
        }
        content.add(actor).width(actorWidth).height(actorHeight).align(Align.center).pad(BIG_PAD);
//        content.row();
    }


    private void addSocial() {
        Table socialTable = new Table();
        String[] paths = {AboutAssets.FACEBOOK,AboutAssets.MAIL, AboutAssets.TWITTER};
        int i = 0;
        List<Links> linksList = Links.getGroupLinks(LinkGroups.SOCIAL);
        for (Links links : linksList) {
            Actor actor = new Image(getScreenTextureRegion(paths[i]));
            float actorHeight = actor.getHeight();
            float actorWidth = actor.getWidth();
            float actorSizeRatio = actorWidth / actorHeight;
            if (actorHeight > SOCIAL_MAX_HEIGHT) {
                actorHeight = SOCIAL_MAX_HEIGHT;
                actorWidth = actorSizeRatio * actorHeight;
            }
            actor.setUserObject(links);
            actor.addListener(labelClickListener);
            socialTable.add(actor).width(actorWidth).height(actorHeight).pad(BIG_PAD);
            i++;
        }
        content.add(socialTable).width(contentWidth).center();
        content.row();
    }

    private void addLabelLink(Links link){
        addLabelLink(link, link.getLinkName());
    }

    private void addLabelLink(Links link, String linkName) {
        content.add(createLabelWithLink(link, false, linkName)).pad(BIG_PAD);
        content.row();
    }

    private void addBigLabelLink(Links link) {
        content.add(createLabelWithLink(link, true, link.getLinkName()));
        content.row();
    }

    private TablexiaLabel createLabelWithLink(Links link, boolean bigFont, String linkName) {
       return createLabelWithLink(link,bigFont, false, linkName);
    }
    
    private TablexiaLabel createLabelWithLink(Links link, boolean bigFont, boolean addSeparator, String linkName){
        TablexiaLabel.TablexiaLabelStyle style = new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_18, DEFAULT_LINK_COLOR);
        if (bigFont) {
            style = new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_20, DEFAULT_LINK_COLOR);
        }
        String name = addSeparator ? linkName + SEPARATOR : linkName;
        TablexiaLabel label = new TablexiaLabel(name, style);
        label.setUserObject(link);
        label.addListener(labelClickListener);
        return label;
    }


    private TablexiaLabel createStyledLabel(String text, TablexiaLabel.TablexiaLabelStyle labelStyle) {
        TablexiaLabel newLabel = new TablexiaLabel(text, labelStyle);
        newLabel.setAlignment(Align.center);
        newLabel.setWrap(true);

        return newLabel;

    }

    private Table createLibrariesParagraph() {
        Table licencesTable = new Table();
        TablexiaLabel.TablexiaLabelStyle libraryNameStyle = BOLD_LABEL_STYLE_20;
        TablexiaLabel.TablexiaLabelStyle libraryURLStyle = new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_18, DEFAULT_LINK_COLOR);
        TablexiaLabel.TablexiaLabelStyle libraryLicenceTypeStyle = BOLD_LABEL_STYLE_20;
        licencesTable.setWidth(DIALOG_WIDTH);
        licencesTable.pad(DIALOG_CONTENT_PADDING);
        List<LibraryDefinition> libraries = Arrays.asList(LibraryDefinition.values());
        for (final LibraryDefinition library : libraries) {
            TablexiaLabel linkName = new TablexiaLabel(library.getName(), libraryNameStyle);
            TablexiaLabel licenceType = new TablexiaLabel(library.getLicence().getName(), libraryLicenceTypeStyle);
            TablexiaLabel licenceURL = new TablexiaLabel(library.getLicence().getURL(), libraryURLStyle);
            licenceURL.addListener(new ClickListenerWithSound(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    Tablexia.getNet().openURI(library.getLicence().getURL());
                }
            });
            licencesTable.add(linkName).center().expandX();
            licencesTable.row();
            licencesTable.add(licenceType).center().expandX();
            licencesTable.row();
            licencesTable.add(licenceURL).center().expandX();
            licencesTable.row();

            final TablexiaLabel linkURL = new TablexiaLabel(library.getUrl(), libraryURLStyle);
            linkURL.addListener(new ClickListenerWithSound(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    Tablexia.getNet().openURI(library.getUrl());
                }
            });
            licencesTable.add(linkURL).center().expandX().padBottom(DIALOG_CONTENT_PADDING);
            licencesTable.row();
        }

        return licencesTable;

    }

    private static class LabelClickListener extends ClickListenerWithSound {
        @Override
        public void onClick(InputEvent event, float x, float y) {
            String url = ((Links) event.getListenerActor().getUserObject()).getLinkAddress();
            Tablexia.getNet().openURI(url);
        }
    }
}