/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.halloffame;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.DistanceFieldFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import net.engio.mbassy.listener.Handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.game.trophy.GameTrophyDefinition;
import cz.nic.tablexia.game.trophy.ITrophyDefinition;
import cz.nic.tablexia.game.trophy.UserTrophyDefinition;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.model.game.UserTrophy;
import cz.nic.tablexia.model.screen.ScreenDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.halloffame.assets.HallOfFameAssets;
import cz.nic.tablexia.screen.halloffame.trophy.TrophyHelper;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.structure.Trio;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;
import cz.nic.tablexia.util.ui.ScrollPaneWithBorders;
import cz.nic.tablexia.util.ui.TablexiaImage;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AdaptiveSizeDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.BackButtonHideComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DimmerDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DividerContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.SpeechDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TouchCloseDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnTextContentDialogComponent;

public class HallOfFameScreen extends AbstractTablexiaScreen<Map<ITrophyDefinition, Boolean>> {
    public static final String ACTOR_DOOR_NAME					    = "actor door";
    public static final String TROPHY_HEAP_NAME                     = "heap of trophies";
    public static final String SCORE_TABLE_DIALOG_NAME              = "score table dialog";

    private static final Point  SCREEN_SIZE                         = new Point(TablexiaSettings.getWorldSize(), TablexiaSettings.getMinWorldHeight());
    private static final int    TROPHY_DIALOG_WIDTH                 = 350;
    private static final int    TROPHY_DIALOG_HEIGHT                = 350;
    private static final int    SCORE_TABLE_DIALOG_WIDTH            = 280;
    private static final int    SCORE_TABLE_DIALOG_HEIGHT           = 300;
    private static final int    BACKGROUND_TILE_REPEAT_COUNT_RANKED = 85;
    private static final int    BACKGROUND_TILE_REPEAT_COUNT        = 99;
    private static final long   MOUSE_CHEESE_ANIM_DURATION          = 1500;
    private static final long   MOUSE_DIALOG_DURATION               = 8000;
    private static final float  MOUSE_DIALOG_PADDING                = 8;
    private static final float  MOUSE_DIALOG_PADDING_TRESHOLD_SCALE = 1.7f;
    private static final int    MOUSE_RANK_UP_TIMEOUT               = 2 * 24 * 60 * 60 * 1000; //2 days in ms

    private static final int    HEAP_WIDTH                          = 398;
    private static final int    HEAP_HEIGHT                         = 538;
    private static final int    MOUSE_WIDTH                         = 98;
    private static final int    MOUSE_HEIGHT                        = 120;
    private static final int    DOOR_Y_OFFSET                       = -8;

    private static final Vector2  MOUSE_DIALOG_OFFSET               = new Vector2(32, 16);

    public static final     String  HALL_OF_FAME_SPEECH             = MFX_PATH + "halloffame.mp3";
    private static final    String  SCREEN_STATE_SCROLL_POSITION_X  = "scrollPositionX";
    public static final     String  SCROLL_TO_TROPHY_KEY            = "scrollToTrophy";
    private static final    String  COUNT_SUM_TEXT_KEY              = "count_sum";

    private static final    float   TROPHIES_OFFSET_X               = 0.2f;
    private static final    float   TROPHIES_OFFSET_Y               = -0.02f;

    private static final ApplicationFontManager.FontType    TROPHY_DIALOG_TITLE_FONT            = ApplicationFontManager.FontType.BOLD_20;
    private static final ApplicationFontManager.FontType    TROPHY_DIALOG_DESCRIPTION_FONT      = ApplicationFontManager.FontType.REGULAR_16;
    private static final Color                              TROPHY_DIALOG_TITLE_COLOR           = Color.BLACK;
    private static final Color                              TROPHY_DIALOG_DESCRIPTION_COLOR     = Color.BLACK;
    private static final float                              TROPHY_DIALOG_BANNER_WIDTH_RATIO    = 3f / 5;
    private static final float                              TROPHY_DIALOG_BANNER_HEIGHT_RATIO   = 1f / 4;
    private static final float                              TROPHY_DIALOG_IMAGE_WIDTH_RATIO     = 4f / 5;
    private static final float                              TROPHY_DIALOG_IMAGE_HEIGHT_RATIO    = 2f / 5;
    private static final Scaling                            TROPHY_DIALOG_BANNER_SCALING        = Scaling.fit;
    private static final Scaling                            TROPHY_DIALOG_IMAGE_SCALING         = Scaling.fit;

    private static final Trio<Float,Float,Float>    MOUSE_POSITION_PROPERTIES = new Trio<Float,Float,Float>(0.8f, 0.155f, 1f);
    private static final Trio<Float,Float,Float>    CARPET_POSITION_PROPERTIES = new Trio<Float,Float,Float>(0.08f, 0.01f, 1f);

    private static final int                        BACKGROUND_IOS_SPACING_WORKAROUND = -2;

    private static final ApplicationFontManager.FontType    SUM_DIALOG_FONT_TYPE_BOLD    = ApplicationFontManager.FontType.BOLD_12;
    private static final ApplicationFontManager.FontType    SUM_DIALOG_FONT_TYPE    = ApplicationFontManager.FontType.REGULAR_12;
    private static final Color                              SUM_DIALOG_FONT_COLOR   = Color.BLACK;
    private static final int                                SUM_DIALOG_TITLE_ALIGN  = Align.left;
    private static final int                                SUM_DIALOG_VALUE_ALIGN  = Align.right;

    private static final float                              BADGE_TABLE_VERTICAL_SPACE          = 2f;
    private static final float                              BADGE_TABLE_HORIZONTAL_SPACE        = 12f;
    private static final float                              BADGE_TABLE_BADGE_HEIGHT            = 0.222f;

    private static final float                              BADGE_BOARD_POS_X                   = 0.67f;
    private static final float                              BADGE_BOARD_POS_Y                   = 0.47f;
    private static final float                              BADGE_BOARD_WIDTH                   = 0.14f;
    private static final float                              BADGE_BOARD_BADGE_WIDTH             = 0.132f;
    private static final float                              BADGE_BOARD_BADGE_INACTIVE_ALPHA    = 0.5f;
    private static final float                              BADGE_BOARD_BADGE_MAX_ANGLE         = 8f; //Ranges from -MAX_ANGLE to +MAX_ANGLE
    private static final long                               BADGE_BOARD_RANDOM_SEED             = 1338L; //Random seed for badge rotation...
    private static final int                                BADGE_BOARD_BADGES_PER_ROW          = 4;

    private static final float                              BADGE_BOARD_DIALOG_WIDTH            = 0.8f;
    private static final float                              BADGE_BOARD_DIALOG_HEIGHT           = 0.8f;
    private static final float                              BADGE_BOARD_DIALOG_TABLE_WIDTH      = 0.9f;
    private static final float                              BADGE_BOARD_DIALOG_TABLE_HEIGHT     = 0.85f;

    private static final TablexiaLabel.TablexiaLabelStyle   TROPHY_HEAP_LABEL_STYLE             = new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.BOLD_20, new Color(0.16f, 0.16f, 0.16f, 1f));
    private static final float                              TROPHY_HEAP_LABEL_GROUP_X           = 0.59f;
    private static final float                              TROPHY_HEAP_LABEL_GROUP_Y           = 0.23f;
    private static final float                              TROPHY_HEAP_LABEL_GROUP_WIDTH       = 0.17f;
    private static final float                              TROPHY_HEAP_LABEL_GROUP_HEIGHT      = 0.095f;

    private static final float TROPHY_DEFAULT_HD_SCALE = 0.35f;

    private HorizontalGroup backgroundGroup;
    private Group foregroundGroup;
    private TablexiaNoBlendingImage wallImage;

    private ScrollPaneWithBorders scrollPane;
    private Image mouseImage;
    private Timer mouseTimer;
    private long rankUpTime;
    private long visitTime;

    @Override
    protected Viewport createViewport() {
        return new ExtendViewport(0, TablexiaSettings.getMinWorldHeight());
    }

    @Override
    protected void screenPaused(Map<String, String> screenState) {
        screenState.put(SCREEN_STATE_SCROLL_POSITION_X, String.valueOf(scrollPane.getScrollPercentX()));
    }

    @Override
    public void setScrollableLayoutFocus() {
        if(scrollPane != null){
            getStage().setScrollFocus(scrollPane);
        }
    }

    @Override
    public void enableScrollableLayoutFocus() {
        if(scrollPane != null){
            scrollPane.setScrollingDisabled(false, true);
            getStage().setScrollFocus(scrollPane);
        }
    }

    @Override
    public void disableScrollableLayoutFocus() {
        if(scrollPane != null){
            scrollPane.setScrollingDisabled(true,true);
            getStage().setScrollFocus(null);
        }
    }

    // GFX
    @Override
    protected void prepareScreenAtlases(List<String> atlasesNames) {
        super.prepareScreenAtlases(atlasesNames);
        atlasesNames.add(AbstractTablexiaGame.GLOBAL_ATLAS_NAME);
    }

    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        Stack contentStack = new Stack();
        contentStack.setHeight(getStage().getViewport().getScreenHeight());
        prepareBackgroundLayer();
        prepareForegroundLayer();

        contentStack.addActor(backgroundGroup);
        contentStack.addActor(foregroundGroup);

        wallImage = new TablexiaNoBlendingImage(getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_WALL));
        scrollPane = new ScrollPaneWithBorders(contentStack, wallImage);
        scrollPane.setScrollingDisabled(false, true);
        scrollPane.setFillParent(true);

        getStage().addActor(scrollPane);

        if (screenState.containsKey(SCREEN_STATE_SCROLL_POSITION_X)) {
            scrollPane.pack();
            scrollPane.setScrollPercentX(Float.valueOf(screenState.get(SCREEN_STATE_SCROLL_POSITION_X)));
            scrollPane.updateVisualScroll();
        }

        if (screenState.containsKey(SCROLL_TO_TROPHY_KEY)) {
            scrollToTrophy(screenState.get(SCROLL_TO_TROPHY_KEY));
        }

        rankUpTime = UserRankManager.getInstance().getLastTimeRankedUp(getSelectedUser());
        visitTime  = ScreenDAO.getScreenLastVisitTime(getSelectedUser().getId(), getClass().getName());
        getStage().setScrollFocus(scrollPane);
    }

    @Override
    protected Map<ITrophyDefinition, Boolean> prepareScreenData(Map<String, String> screenState) {
        Map<ITrophyDefinition, Boolean> map = new HashMap<ITrophyDefinition, Boolean>();
        final List<ITrophyDefinition> trophies = new ArrayList<ITrophyDefinition>();

        trophies.addAll(Arrays.asList(GameTrophyDefinition.values()));
        trophies.addAll(Arrays.asList(UserTrophyDefinition.values()));

        for (final ITrophyDefinition trophy : trophies) {
            map.put(trophy, trophy.hasTrophy(getSelectedUser()));
        }
        return map;
    }

     @Override
     protected String prepareIntroMusicPath() {
         return HALL_OF_FAME_SPEECH;
     }

    private Image createImageForHorizontalGroup(TextureRegion textureRegion, float imageHeight) {
        Image resultImage = ScaleUtil.createImageToHeight(textureRegion, imageHeight);
        resultImage.getDrawable().setMinWidth(resultImage.getWidth());
        resultImage.getDrawable().setMinHeight(resultImage.getHeight());
        return resultImage;
    }

    private void prepareBackgroundLayer() {
        backgroundGroup = new HorizontalGroup();
        backgroundGroup.setHeight(SCREEN_SIZE.y);

        boolean ranked = TablexiaSettings.isUserHighestRank();



        Image imgDoor;
        Image imgDoorTitle;
        TextureRegion textureTile;

        if (ranked) {
            imgDoor = createImageForHorizontalGroup(getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_DOOR_RANKED), SCREEN_SIZE.y);
            imgDoorTitle = createImageForHorizontalGroup(getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_DOOR_TITLE_RANKED), SCREEN_SIZE.y);
            textureTile = getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_BACKGROUND_TILE_RANKED);
        }else {
            imgDoor = createImageForHorizontalGroup(getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_DOOR), SCREEN_SIZE.y);
            imgDoorTitle = createImageForHorizontalGroup(getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_DOOR_TITLE), SCREEN_SIZE.y);
            textureTile = getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_BACKGROUND_TILE);
        }


        Stack startTile = new Stack();
        startTile.add(imgDoor);
        startTile.add(imgDoorTitle);

        backgroundGroup.addActor(startTile);
        startTile.setName(ACTOR_DOOR_NAME);


        int tilesCount = ranked ? BACKGROUND_TILE_REPEAT_COUNT_RANKED : BACKGROUND_TILE_REPEAT_COUNT;

        for (int i = 0; i < tilesCount; i++) {
            Image imgBackground = createImageForHorizontalGroup(textureTile, SCREEN_SIZE.y);
            backgroundGroup.addActor(imgBackground);
        }
        backgroundGroup.space(BACKGROUND_IOS_SPACING_WORKAROUND);
    }

    private void prepareDoorButton() {
        final TextureRegion textureDoorButton = getScreenTextureRegion(TablexiaSettings.isUserHighestRank() ? HallOfFameAssets.HALL_OF_FAME_DOOR_BUTTON_UNPRESSED_RANKED : HallOfFameAssets.HALL_OF_FAME_DOOR_BUTTON_UNPRESSED);
        final Image imgDoorButton = createImageForHorizontalGroup(textureDoorButton, getViewportHeight());
        imgDoorButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TextureRegion textureDoor = getScreenTextureRegion(TablexiaSettings.isUserHighestRank() ? HallOfFameAssets.HALL_OF_FAME_DOOR_BUTTON_PRESSED_RANKED : HallOfFameAssets.HALL_OF_FAME_DOOR_BUTTON_PRESSED);
                imgDoorButton.setDrawable(new SpriteDrawable(new Sprite(textureDoor)));
                backToInitialScreen();
            }
        });
        imgDoorButton.setY(DOOR_Y_OFFSET);
        foregroundGroup.addActor(imgDoorButton);

    }

    private void prepareForegroundLayer() {
        foregroundGroup = new Group();
        foregroundGroup.setSize(backgroundGroup.getWidth(), SCREEN_SIZE.y);

        int threeStarsCount = UserTrophy.AllStarsGame.getNumberOfAllStarsResults(getSelectedUser());
        int trophiesCount = getAchievedTrophies(getSelectedUser());

        if(!TablexiaSettings.isUserHighestRank()) prepareCarpet();
        prepareDoorButton();
        prepareTrophyHeap(threeStarsCount);
        prepareTrophies();
        prepareMouse(trophiesCount);
        prepareBadgeBoard();
    }

    private void prepareBadgeBoard() {
        Group badgeBoardGroup = new Group();
        badgeBoardGroup.setPosition(BADGE_BOARD_POS_X * SCREEN_SIZE.x, BADGE_BOARD_POS_Y * SCREEN_SIZE.y);

        final Image badgeBoard = ScaleUtil.createImageToHeight(getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_BADGE_BOARD), SCREEN_SIZE.x * BADGE_BOARD_WIDTH);

        badgeBoard.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                showBadgeBoardDialog();
            }
        });

        badgeBoardGroup.addActor(badgeBoard);

        TablexiaRandom random = new TablexiaRandom(BADGE_BOARD_RANDOM_SEED);

        Table table = new Table();
        table.setSize(badgeBoard.getWidth(), badgeBoard.getHeight());
        table.defaults().space(BADGE_TABLE_VERTICAL_SPACE);
        table.defaults().spaceLeft(BADGE_TABLE_HORIZONTAL_SPACE).spaceRight(BADGE_TABLE_HORIZONTAL_SPACE);

        UserRankManager.UserRank currentRank = UserRankManager.getInstance().getRank(getSelectedUser());
        int rowCount = 0;
        for(UserRankManager.UserRank rank : UserRankManager.UserRank.values()) { //TODO - Add UserRankManager.getRanksOrdered() instead of values()...
            if(!rank.isShownOnBadgeBoard()) continue;

            Image badgeImage;

            if(currentRank.getId() >= rank.getId()) {
                badgeImage = ScaleUtil.createImageToWidth(ApplicationAtlasManager.getInstance().getTextureRegion(rank.getBadgeIconKey()), BADGE_BOARD_BADGE_WIDTH * badgeBoard.getWidth());
            }
            else {
                //GreyScale image...
                badgeImage = ScaleUtil.createGreyScaleImageToWidth(ApplicationAtlasManager.getInstance().getTextureRegion(rank.getBadgeIconKey()), BADGE_BOARD_BADGE_WIDTH * badgeBoard.getWidth());
                badgeImage.setColor(badgeImage.getColor().r, badgeImage.getColor().g, badgeImage.getColor().b, badgeImage.getColor().a * BADGE_BOARD_BADGE_INACTIVE_ALPHA);
            }

            //Not touchable
            badgeImage.setTouchable(Touchable.disabled);

            //Rotate badge
            badgeImage.setOrigin(Align.center);
            float degrees = (random.nextFloat() - 0.5f) * BADGE_BOARD_BADGE_MAX_ANGLE;
            badgeImage.rotateBy(degrees);

            //Add badge to table
            Cell<Image> badgeImageCell = table.add();
            badgeImageCell.setActor(badgeImage);
            badgeImageCell.width(badgeImage.getWidth());
            badgeImageCell.height(badgeImage.getHeight());

            rowCount++; //processing rows in table...
            if(rowCount >= BADGE_BOARD_BADGES_PER_ROW) {
                table.row();
                rowCount = 0;
            }
        }

        badgeBoardGroup.addActor(table);

        foregroundGroup.addActor(badgeBoardGroup);
    }

    private void showBadgeBoardDialog() {
        UserRankManager.UserRank currentRank = UserRankManager.getInstance().getRank(getSelectedUser());

        float dialogWidth  = TablexiaComponentDialogFactory.getDialogStage().getWidth()  * BADGE_BOARD_DIALOG_WIDTH;
        float dialogHeight = TablexiaComponentDialogFactory.getDialogStage().getHeight() * BADGE_BOARD_DIALOG_HEIGHT;

        List<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>();

        components.add(new FixedSpaceContentDialogComponent());
        components.add(new TextContentDialogComponent(getText(HallOfFameAssets.TEXT_BADGE_CABINET_LIST), ApplicationFontManager.FontType.BOLD_26, Color.WHITE, true));
        components.add(new DividerContentDialogComponent(Color.WHITE));
        components.add(new FixedSpaceContentDialogComponent());

        TablexiaRandom random = new TablexiaRandom(BADGE_BOARD_RANDOM_SEED);

        final Table table = new Table();
        float tableWidth  = dialogWidth  * BADGE_BOARD_DIALOG_TABLE_WIDTH;
        float tableHeight = dialogHeight * BADGE_BOARD_DIALOG_TABLE_HEIGHT;

        table.setSize(tableWidth, tableHeight);

        int rowCount = 0;
        int badgesCount = UserRankManager.UserRank.values().length;
        for(UserRankManager.UserRank rank : UserRankManager.UserRank.values()) {
            if(!rank.isShownOnBadgeBoard()) continue;

            Image badgeImage;
            String badgeName = getText(HallOfFameAssets.TEXT_BADGE_CABINET_LOCKED);

            if(currentRank.getId() >= rank.getId()) {
                badgeImage = ScaleUtil.createImageToHeight(getScreenTextureRegion(rank.getHallOfFameBadge()), BADGE_TABLE_BADGE_HEIGHT * tableHeight);
                badgeName = ApplicationTextManager.getInstance().getText(rank.getNameKey());
            }
            else {
                badgeImage = ScaleUtil.createGreyScaleImageToHeight(getScreenTextureRegion(rank.getHallOfFameBadge()), BADGE_TABLE_BADGE_HEIGHT * tableHeight);
                badgeImage.setColor(badgeImage.getColor().r, badgeImage.getColor().g, badgeImage.getColor().b, badgeImage.getColor().a * BADGE_BOARD_BADGE_INACTIVE_ALPHA);
            }

            TablexiaLabel badgeNameLabel = new TablexiaLabel(badgeName, new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.BOLD_20, Color.WHITE));

            //Create group for laben and badge image
            Group group = new Group();
            int rowsCount = badgesCount % 2 == 0 ? badgesCount / BADGE_BOARD_BADGES_PER_ROW : badgesCount / BADGE_BOARD_BADGES_PER_ROW + 1;
            group.setSize(tableWidth / BADGE_BOARD_BADGES_PER_ROW, tableHeight / rowsCount);

            //Set up badge label
            badgeNameLabel.setWidth(group.getWidth());
            badgeNameLabel.setAlignment(Align.center);
            group.addActor(badgeNameLabel);

            //Set up and rotate badge image
            badgeImage.setPosition(group.getWidth()/2 - badgeImage.getWidth()/2, badgeNameLabel.getHeight());

            badgeImage.setOrigin(Align.center);
            float degrees = (random.nextFloat() - 0.5f) * BADGE_BOARD_BADGE_MAX_ANGLE;
            badgeImage.rotateBy(degrees);

            group.addActor(badgeImage);

            //add badge group (image + label) to the actual table...
            Cell<Group> groupCell = table.add(group);
            groupCell.width(group.getWidth());
            groupCell.height(group.getHeight());

            rowCount++;
            if(rowCount >= BADGE_BOARD_BADGES_PER_ROW) {
                rowCount = 0;
                table.row();
            }
        }

        components.add(new TablexiaDialogComponentAdapter() {
            @Override
            public void prepareContent(Cell content) {
                content.expand();
                content.setActor(table);
            }
        });

        components.add(new FixedSpaceContentDialogComponent());

        components.add(new CenterPositionDialogComponent());
        components.add(new DimmerDialogComponent());
        components.add(new TouchCloseDialogComponent());
        components.add(new BackButtonHideComponent());

        TablexiaComponentDialog dialog = TablexiaComponentDialogFactory.getInstance().createDialog(TablexiaComponentDialog.TablexiaDialogType.DIALOG_BADGE_BOARD, components.toArray(new TablexiaDialogComponentAdapter[]{}));
        dialog.setDialogListener(dialogListener);
        dialog.show(dialogWidth, dialogHeight);
    }

    private void prepareTrophyHeap(int threeStarsCount) {
        final Map<GameDefinition, Integer> starResults = UserTrophy.AllStarsGame.getNumberOfStarsResults(getSelectedUser());

        final TextureRegion texture = getScreenTextureRegion(TrophyHelper.TrophyHeapAssets.getTrophyHeapAssetNameForStarCount(threeStarsCount));
        final TablexiaImage heapImage = new TablexiaImage(texture, TrophyHelper.TrophyHeapAssets.getTrophyHeapAssetNameForStarCount(threeStarsCount));
        heapImage.setName(TROPHY_HEAP_NAME);

        Trio<Float,Float,Float> heapProperties = TrophyHelper.heapProperties;
        int heapX = (int)(heapProperties.getFirst() * SCREEN_SIZE.x);
        int heapY = (int)(heapProperties.getSecond() * SCREEN_SIZE.y);
        float heapScale = heapProperties.getThird();
        heapImage.setPosition(heapX, heapY);
        heapImage.setScale(heapScale);
        heapImage.setSize(HEAP_WIDTH,HEAP_HEIGHT);
        //Create a group for trophy heap label and put a label right in the middle of it
        Group labelGroup = new Group();

        labelGroup.setBounds(
            heapImage.getX() + heapImage.getWidth() * TROPHY_HEAP_LABEL_GROUP_X,
            heapImage.getY() + heapImage.getHeight() * TROPHY_HEAP_LABEL_GROUP_Y,
            heapImage.getWidth() * TROPHY_HEAP_LABEL_GROUP_WIDTH,
            heapImage.getHeight() * TROPHY_HEAP_LABEL_GROUP_HEIGHT
        );

        TablexiaLabel label = new TablexiaLabel(String.valueOf(threeStarsCount), TROPHY_HEAP_LABEL_STYLE);
        labelGroup.addActor(label);
        label.setPosition(labelGroup.getWidth() / 2f - label.getWidth() / 2f, labelGroup.getHeight() / 2f - label.getHeight() / 2f);

        labelGroup.rotateBy(-15);

        heapImage.addListener(new HeapDialogListener());
        labelGroup.addListener(new HeapDialogListener());

        foregroundGroup.addActor(heapImage);
        foregroundGroup.addActor(labelGroup);
    }

    private void prepareTrophies() {
        final List<ITrophyDefinition> trophies = new ArrayList<ITrophyDefinition>();
        trophies.addAll(Arrays.asList(GameTrophyDefinition.values()));
        trophies.addAll(Arrays.asList(UserTrophyDefinition.values()));

        boolean ranked = TablexiaSettings.isUserHighestRank();

        for (final ITrophyDefinition trophy : trophies) {
            final TextureRegion texture;
            final TablexiaImage trophyImage;
            TrophyHelper.TrophyType trophyType = getData().get(trophy) ?  TrophyHelper.TrophyType.FULL : TrophyHelper.TrophyType.EMPTY;
            if(ranked) {
                texture = getScreenTextureRegion(TrophyHelper.getRankedFullPath(trophy.getTrophyNameKey(), trophyType));
                trophyImage = new TablexiaImage(texture, TrophyHelper.getRankedFullPath(trophy.getTrophyNameKey(), trophyType));
            }else {
                texture = getScreenTextureRegion(TrophyHelper.getFullPath(trophy.getTrophyNameKey(), trophyType));
                trophyImage = new TablexiaImage(texture, TrophyHelper.getFullPath(trophy.getTrophyNameKey(), trophyType));
            }
            trophyImage.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    Log.info(getClass(), "TROPHY CLICKED: " + trophy.getTrophyName());

                    TextureRegion banner = getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_DIALOG_BANNER);
                    String fullPath = ranked ? TrophyHelper.getRankedFullPath(trophy.getTrophyNameKey(), TrophyHelper.TrophyType.SMALL) : TrophyHelper.getFullPath(trophy.getTrophyNameKey(), TrophyHelper.TrophyType.SMALL);
                    TextureRegion texture = getTextureRegionForAtlas(AbstractTablexiaGame.GLOBAL_ATLAS_NAME, fullPath);

                    String name = trophy.getTrophyName();
                    String desc = trophy.getTrophyDescription();

                    TablexiaComponentDialog dialog = TablexiaComponentDialogFactory.getInstance().createStandardDialog(
                            new FixedSpaceContentDialogComponent(),
                            new ImageContentDialogComponent(banner, TROPHY_DIALOG_BANNER_SCALING, TROPHY_DIALOG_BANNER_WIDTH_RATIO, TROPHY_DIALOG_BANNER_HEIGHT_RATIO),
                            new ResizableSpaceContentDialogComponent(),
                            new ImageContentDialogComponent(texture, TROPHY_DIALOG_IMAGE_SCALING, TROPHY_DIALOG_IMAGE_WIDTH_RATIO, TROPHY_DIALOG_IMAGE_HEIGHT_RATIO),
                            new ResizableSpaceContentDialogComponent(),
                            new TextContentDialogComponent(name, TROPHY_DIALOG_TITLE_FONT, TROPHY_DIALOG_TITLE_COLOR),
                            new FixedSpaceContentDialogComponent(),
                            new TextContentDialogComponent(desc, TROPHY_DIALOG_DESCRIPTION_FONT, TROPHY_DIALOG_DESCRIPTION_COLOR),
                            new FixedSpaceContentDialogComponent(),
							new BackButtonHideComponent());
                    dialog.setDialogListener(dialogListener);
                    dialog.show(TROPHY_DIALOG_WIDTH, TROPHY_DIALOG_HEIGHT);
                }
            });

            Trio<Float, Float, Float> properties = ranked ? TrophyHelper.trophiesPropertiesRanked.get(trophy) : TrophyHelper.trophiesProperties.get(trophy);
            int trophyX = (int) ((TROPHIES_OFFSET_X + properties.getFirst()) * SCREEN_SIZE.x);
            int trophyY = (int) ((TROPHIES_OFFSET_Y + properties.getSecond()) * SCREEN_SIZE.y);
            float trophyScale = properties.getThird();

            trophyImage.setPosition(trophyX, trophyY);

            if (TablexiaSettings.getInstance().isUseHdAssets()){
                if (ranked) trophyImage.setScale((1-TROPHY_DEFAULT_HD_SCALE)*trophyScale);
                else trophyImage.setScale(TROPHY_DEFAULT_HD_SCALE);
            }else {
                trophyImage.setScale(trophyScale);
            }

            trophyImage.setName(trophy.getTrophyName());
            foregroundGroup.addActor(trophyImage);
        }
    }

    private void prepareMouse(final int trophiesCount) {
        TrophyHelper.MouseTexts.resetAllTextCounters();

        final TextureRegion mouseTexture = getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_MOUSE_UNPRESSED);
        mouseImage = new Image(mouseTexture);

        mouseImage.setSize(MOUSE_WIDTH, MOUSE_HEIGHT);
        mouseImage.setScale(MOUSE_POSITION_PROPERTIES.getThird());
        mouseImage.setPosition(SCREEN_SIZE.x * MOUSE_POSITION_PROPERTIES.getFirst(), SCREEN_SIZE.y * MOUSE_POSITION_PROPERTIES.getSecond());

        mouseImage.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                String textKey = TrophyHelper.MouseTexts.getMouseText(trophiesCount);
                String actualText = textKey == null ? null : getText(textKey);
                showMouseDialog(actualText);
            }
        });

        foregroundGroup.addActor(mouseImage);
    }

    @Override
    protected void screenResized(int width, int height) {
        super.screenResized(width, height);
        backgroundGroup.setHeight(height);
    }

    @Handler
    private void onScreenVisibleEvent(AbstractTablexiaScreen.ScreenVisibleEvent screenVisibleEvent) {
        if(screenVisibleEvent.getActualScreenClass()==HallOfFameScreen.class){
            handleMouseRankUpDialog();
        }
    }

    private void handleMouseRankUpDialog() {
        long timeInMs = rankUpTime - visitTime;

        if(timeInMs > 0  && (visitTime == 0 || timeInMs <= MOUSE_RANK_UP_TIMEOUT)) {
            Log.info(getClass(), "User: " + getSelectedUser().getName() + " has ranked up since the last visit of " + getClass().getSimpleName());

            showMouseDialog(getText(TrophyHelper.MouseTexts.MOUSE_RANKUP_TEXT));
        }
    }

    private void showMouseDialog(String text) {
        if(mouseTimer != null) mouseTimer.cancel();
        mouseTimer = new Timer();

        if(text != null) {
            float imageWidth = mouseImage.getWidth() * mouseImage.getScaleX();
            float imageHeight = mouseImage.getHeight() * mouseImage.getScaleY();

            float dialogX = mouseImage.getX() - scrollPane.getScrollX() + imageWidth / 2f;
            float dialogY = mouseImage.getY() + imageHeight / 2f;

            ArrayList<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>();
            components.add(new TouchCloseDialogComponent(new TouchCloseDialogComponent.TouchListener() {
                @Override
                public boolean dialogTouched(float x, float y, TablexiaComponentDialog dialog) {
                    hideMouse();
                    mouseTimer.cancel();
                    return true;
                }
            }));
            components.add(new AdaptiveSizeDialogComponent());
            components.add(new SpeechDialogComponent(new Point(dialogX, dialogY), MOUSE_DIALOG_OFFSET, SpeechDialogComponent.ArrowType.CLASSIC));

            components.add(new FixedSpaceContentDialogComponent());
            components.add(new ResizableSpaceContentDialogComponent());

            components.add(new TextContentDialogComponent(text, false));

            components.add(new ResizableSpaceContentDialogComponent());
            components.add(new FixedSpaceContentDialogComponent());

            ApplicationFontManager.FontType defaultFontType = TextContentDialogComponent.getDefaultFontType();
            DistanceFieldFont font = ApplicationFontManager.getInstance().getDistanceFieldFont(defaultFontType);
            GlyphLayout layout = new GlyphLayout();
            layout.setText(font, text);

            float padding = ComponentScaleUtil.isUnderThreshold() ? MOUSE_DIALOG_PADDING * MOUSE_DIALOG_PADDING_TRESHOLD_SCALE : MOUSE_DIALOG_PADDING;
            float dialogWidth = layout.width * defaultFontType.getScale() + 2 * padding;
            float dialogHeight = layout.height;

            final TablexiaComponentDialog tcd = TablexiaComponentDialogFactory.getInstance().createDialog(
                    getStage(),
                    TablexiaComponentDialog.TablexiaDialogType.BUBBLE_ROUNDED,
                    components.toArray(new TablexiaDialogComponentAdapter[]{}));
            tcd.setDialogListener(dialogListener);
            tcd.show(dialogWidth, dialogHeight);

            mouseTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    hideMouse();
                    tcd.hide();
                }
            }, MOUSE_DIALOG_DURATION);

            showMouse(false);
        }
        else {
            showMouse(true);

            mouseTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    hideMouse();
                }
            }, MOUSE_CHEESE_ANIM_DURATION);
        }
    }

    private void hideMouse() {
        final TextureRegion mouseTexture = getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_MOUSE_UNPRESSED);
        mouseImage.setDrawable(new SpriteDrawable(new Sprite(mouseTexture)));
        mouseImage.setTouchable(Touchable.enabled);
    }

    private void showMouse(boolean cheese) {
        TextureRegion mousePressedTexture;

        if(cheese)
            mousePressedTexture = getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_MOUSE_CHEESE);
        else
            mousePressedTexture = getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_MOUSE_PRESSED);

        mouseImage.setDrawable(new SpriteDrawable(new Sprite(mousePressedTexture)));
        mouseImage.setTouchable(Touchable.disabled);
    }

    private void prepareCarpet() {
        final TextureRegion carpetTexture = getScreenTextureRegion(HallOfFameAssets.HALL_OF_FAME_CARPET);
        final Image carpetImage = new Image(carpetTexture);
        carpetImage.setScale(CARPET_POSITION_PROPERTIES.getThird());
        carpetImage.setPosition(SCREEN_SIZE.x * CARPET_POSITION_PROPERTIES.getFirst(), SCREEN_SIZE.y * CARPET_POSITION_PROPERTIES.getSecond());

        foregroundGroup.addActor(carpetImage);
    }

    private void scrollToTrophy(String trophyName) {
        if (trophyName == null) {
            return;
        }

        ITrophyDefinition trophyDefinition = TrophyHelper.getTrophyDefinitionByName(trophyName);
        Trio<Float, Float, Float> position = TrophyHelper.trophiesProperties.get(trophyDefinition);
        int trophyX = (int) (position.getFirst() * SCREEN_SIZE.x);

        final TextureRegion texture = getScreenTextureRegion(TrophyHelper.getFullPath(trophyDefinition.getTrophyNameKey(), TrophyHelper.TrophyType.FULL));
        float trophyWidth = texture.getRegionWidth() * position.getThird();

        scrollPane.layout();
        scrollPane.setScrollX(trophyX - TablexiaSettings.getSceneWidth(getStage()) / 2 + trophyWidth / 2);
        scrollPane.updateVisualScroll();
    }

    private int getAchievedTrophies(User user) {
        int achievedTrophies = 0;

        for(GameTrophyDefinition gameTrophyDefinition : GameTrophyDefinition.values()) {
            if(gameTrophyDefinition.hasTrophy(user)) achievedTrophies++;
        }

        for(UserTrophyDefinition userTrophyDefinition : UserTrophyDefinition.values()) {
            if(userTrophyDefinition.hasTrophy(user)) achievedTrophies++;
        }

        return achievedTrophies;
    }

    @Override
    protected void screenDisposed() {
        if(mouseTimer != null) mouseTimer.cancel();
        super.screenDisposed();
    }


    class HeapDialogListener extends ClickListener {

        @Override
        public void clicked(InputEvent event, float x, float y) {
            final Map<GameDefinition, Integer> starResults = UserTrophy.AllStarsGame.getNumberOfStarsResults(getSelectedUser());
            Log.info(getClass(), "HEAP CLICKED ");
            int sum = 0;
            List<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>();
            components.add(new ResizableSpaceContentDialogComponent());
            for (GameDefinition gameDefinition : GameDefinition.getSortedGameDefinitionList()) {
                int numOfStars = 0;
                if (starResults.containsKey(gameDefinition)) {
                    numOfStars = starResults.get(gameDefinition);
                    sum += numOfStars;
                }
                components.add(new ResizableSpaceContentDialogComponent(1f / 90));
                components.add(new TwoColumnTextContentDialogComponent(
                        GameMenuDefinition.getGameMenuDefinitionForGameDefinition(gameDefinition).getTitle(),
                        String.valueOf(numOfStars),
                        SUM_DIALOG_FONT_TYPE,
                        SUM_DIALOG_FONT_COLOR,
                        Integer.valueOf(SUM_DIALOG_TITLE_ALIGN),
                        Integer.valueOf(SUM_DIALOG_VALUE_ALIGN)));
            }
            components.add(new ResizableSpaceContentDialogComponent(1f / 70));
            components.add(new DividerContentDialogComponent());
            components.add(new ResizableSpaceContentDialogComponent(1f / 70));
            components.add(new TwoColumnTextContentDialogComponent(
                    getText(COUNT_SUM_TEXT_KEY),
                    String.valueOf(sum),
                    SUM_DIALOG_FONT_TYPE_BOLD,
                    SUM_DIALOG_FONT_COLOR,
                    Integer.valueOf(SUM_DIALOG_TITLE_ALIGN),
                    Integer.valueOf(SUM_DIALOG_VALUE_ALIGN)));
            components.add(new ResizableSpaceContentDialogComponent());
            components.add(new BackButtonHideComponent());

            TablexiaComponentDialog dialog = TablexiaComponentDialogFactory.getInstance().createStandardDialog(components.toArray(new TablexiaDialogComponentAdapter[]{}));
            dialog.setName(SCORE_TABLE_DIALOG_NAME);
            dialog.setDialogListener(dialogListener);
            dialog.show(SCORE_TABLE_DIALOG_WIDTH, SCORE_TABLE_DIALOG_HEIGHT);
        }
    }
}