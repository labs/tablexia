/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.halloffame.trophy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.trophy.GameTrophyDefinition;
import cz.nic.tablexia.game.trophy.ITrophyDefinition;
import cz.nic.tablexia.game.trophy.UserTrophyDefinition;
import cz.nic.tablexia.screen.halloffame.assets.HallOfFameAssets;
import cz.nic.tablexia.util.structure.Trio;

/**
 * Created by Václav Tarantík on 6.3.15.
 */
public class TrophyHelper {

    public static final String TROPHY_SUBFOLDER_NAME   = "trophy/";
    public static final String TROPHY_PREFIX           = "trophy_";
    public static final String RANKED_SUBFOLDER_NAME   = "ranked/";

    public static final Trio<Float,Float,Float> heapProperties = new Trio<Float,Float,Float>(0.25f, -0.05f, 1f);

    public enum TrophyHeapAssets {

        HALLOFFAME_TROPHYHEAP_00(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_00, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_00_RANKED, 0), //
        HALLOFFAME_TROPHYHEAP_01(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_01, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_01_RANKED, 1), //
        HALLOFFAME_TROPHYHEAP_02(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_02, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_02_RANKED, 2), //
        HALLOFFAME_TROPHYHEAP_03(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_03, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_03_RANKED, 3), //
        HALLOFFAME_TROPHYHEAP_04(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_04, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_04_RANKED, 4), //
        HALLOFFAME_TROPHYHEAP_05(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_05, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_05_RANKED, 5), //
        HALLOFFAME_TROPHYHEAP_06(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_06, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_06_RANKED, 6), //
        HALLOFFAME_TROPHYHEAP_07(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_07, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_07_RANKED, 7), //
        HALLOFFAME_TROPHYHEAP_08(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_08, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_08_RANKED, 8), //
        HALLOFFAME_TROPHYHEAP_09(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_09, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_09_RANKED, 9), //
        HALLOFFAME_TROPHYHEAP_10(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_10, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_10_RANKED, 10), //
        HALLOFFAME_TROPHYHEAP_15(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_15, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_15_RANKED, 15), //
        HALLOFFAME_TROPHYHEAP_20(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_20, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_20_RANKED, 20), //
        HALLOFFAME_TROPHYHEAP_30(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_30, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_30_RANKED, 30), //
        HALLOFFAME_TROPHYHEAP_40(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_40, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_40_RANKED, 40), //
        HALLOFFAME_TROPHYHEAP_60(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_60, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_60_RANKED, 60),
        HALLOFFAME_TROPHYHEAP_80(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_80, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_80_RANKED, 80),
        HALLOFFAME_TROPHYHEAP_100(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_100, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_100_RANKED, 100),
        HALLOFFAME_TROPHYHEAP_130(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_130, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_130_RANKED, 130),
        HALLOFFAME_TROPHYHEAP_160(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_160, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_160_RANKED, 160),
        HALLOFFAME_TROPHYHEAP_200(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_200, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_200_RANKED, 200),
        HALLOFFAME_TROPHYHEAP_250(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_250, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_250_RANKED, 250),
        HALLOFFAME_TROPHYHEAP_300(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_300, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_300_RANKED, 300),
        HALLOFFAME_TROPHYHEAP_350(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_350, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_350_RANKED, 350),
        HALLOFFAME_TROPHYHEAP_400(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_400, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_400_RANKED, 400),
        HALLOFFAME_TROPHYHEAP_450(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_450, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_450_RANKED, 450),
        HALLOFFAME_TROPHYHEAP_500(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_500, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_500_RANKED, 500),
        HALLOFFAME_TROPHYHEAP_550(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_550, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_550_RANKED, 550),
        HALLOFFAME_TROPHYHEAP_600(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_600, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_600_RANKED, 600),
        HALLOFFAME_TROPHYHEAP_650(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_650, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_650_RANKED, 650),
        HALLOFFAME_TROPHYHEAP_700(HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_700, HallOfFameAssets.HALL_OF_FAME_TROPHY_HEAP_700_RANKED, 700);

        private String trophyHeapAsset;
        private String rankedTrophyHeapAsset;
        private int starCount;

        TrophyHeapAssets(String trophyHeapAsset, String rankedTrophyHeapAsset, int starCount) {
            this.trophyHeapAsset = trophyHeapAsset;
            this.rankedTrophyHeapAsset = rankedTrophyHeapAsset;
            this.starCount = starCount;
        }

        public String getTrophyHeapAssetName() {

            if (TablexiaSettings.isUserHighestRank()) return rankedTrophyHeapAsset;
            return trophyHeapAsset;
        }

        public int getStarCount() {
            return starCount;
        }

        public static String getTrophyHeapAssetNameForStarCount(int starCount) {
            for (int i = TrophyHeapAssets.values().length - 1; i >= 0; i--) {
                TrophyHeapAssets trophyHeapAssets = TrophyHeapAssets.values()[i];
                if (starCount >= trophyHeapAssets.getStarCount()) {
                    return trophyHeapAssets.getTrophyHeapAssetName();
                }
            }
            return TrophyHeapAssets.values()[0].getTrophyHeapAssetName();
        }
    }

    public static final Map<ITrophyDefinition, Trio<Float,Float,Float>> trophiesProperties;//x relative position, y relative position, scale relative to original size

    static {
        trophiesProperties = new HashMap<ITrophyDefinition, Trio<Float, Float,Float>>();

        trophiesProperties.put(GameTrophyDefinition.ROBBERY_PLAY1, new Trio<Float, Float, Float>(2.4f, 0.4f, 1f));// robbery_1_finished1,17
        trophiesProperties.put(GameTrophyDefinition.ROBBERY_PLAY2, new Trio<Float, Float, Float>(2.05f, 0.4f, 1f));// robbery_5_finished,14
        trophiesProperties.put(GameTrophyDefinition.ROBBERY_PLAY3, new Trio<Float, Float, Float>(2.15f, 0.6f, 1f));// robbery_10_finished,15
        trophiesProperties.put(GameTrophyDefinition.ROBBERY_DIFF1, new Trio<Float, Float, Float>(2.25f, 0.2f, 1f));// robbery_1_limit,16
        trophiesProperties.put(GameTrophyDefinition.ROBBERY_DIFF2, new Trio<Float, Float, Float>(2.65f, 0.2f, 1f));// robbery_2_limit,19
        trophiesProperties.put(GameTrophyDefinition.ROBBERY_BONUS, new Trio<Float, Float, Float>(10.43f, 0.275f, 0.6f));// robbery_bonus_finished,59

        trophiesProperties.put(GameTrophyDefinition.PURSUIT_PLAY1, new Trio<Float, Float, Float>(1.51f, 0.62f, 1f));// pursuit_1_finished,9
        trophiesProperties.put(GameTrophyDefinition.PURSUIT_PLAY2, new Trio<Float, Float, Float>(1.35f, 0.55f, 1f));// pursuit_5_finished,7
        trophiesProperties.put(GameTrophyDefinition.PURSUIT_PLAY3, new Trio<Float, Float, Float>(1.51f, 0.33f, 1f));// pursuit_10_finished,10
        trophiesProperties.put(GameTrophyDefinition.PURSUIT_DIFF1, new Trio<Float, Float, Float>(1.74f, 0.68f, 1f));// pursuit_1_limit,11
        trophiesProperties.put(GameTrophyDefinition.PURSUIT_DIFF2, new Trio<Float, Float, Float>(1.81f, 0.39f, 1f));// pursuit_2_limit,12
        trophiesProperties.put(GameTrophyDefinition.PURSUIT_BONUS, new Trio<Float, Float, Float>(10.63f, 0.5f, 0.6f));// pursuit_bonus_finished,61

        trophiesProperties.put(GameTrophyDefinition.KIDNAPPING_PLAY1, new Trio<Float, Float, Float>(0.95f, 0.65f, 1f));// kidnapping_1_finished,3
        trophiesProperties.put(GameTrophyDefinition.KIDNAPPING_PLAY2, new Trio<Float, Float, Float>(1.0f, 0.2f, 1f));// kidnapping_5_finished,4
        trophiesProperties.put(GameTrophyDefinition.KIDNAPPING_PLAY3, new Trio<Float, Float, Float>(1.15f, 0.5f, 1f));// kidnapping_10_finished,5
        trophiesProperties.put(GameTrophyDefinition.KIDNAPPING_DIFF1, new Trio<Float, Float, Float>(0.75f, 0.4f, 1f));// kidnapping_1_limit,1
        trophiesProperties.put(GameTrophyDefinition.KIDNAPPING_DIFF2, new Trio<Float, Float, Float>(0.6f, 0.7f, 1f));// kidnapping_2_limit,0
        trophiesProperties.put(GameTrophyDefinition.KIDNAPPING_BONUS, new Trio<Float, Float, Float>(10.53f, 0.07f, 0.56f));// kidnapping_bonus_finished,60

        trophiesProperties.put(GameTrophyDefinition.NIGHT_WATCH_PLAY1, new Trio<Float, Float, Float>(3.5f, 0.5f, 1f));// night_watch_1_finished,22
        trophiesProperties.put(GameTrophyDefinition.NIGHT_WATCH_PLAY2, new Trio<Float, Float, Float>(3.63f, 0.6f, 1f));// night_watch_5_finished,23
        trophiesProperties.put(GameTrophyDefinition.NIGHT_WATCH_PLAY3, new Trio<Float, Float, Float>(3.8f, 0.5f, 1f));// night_watch_10_finished,25
        trophiesProperties.put(GameTrophyDefinition.NIGHT_WATCH_DIFF1, new Trio<Float, Float, Float>(3.78f, 0.77f, 1f));// night_watch_1_limit,24
        trophiesProperties.put(GameTrophyDefinition.NIGHT_WATCH_DIFF2, new Trio<Float, Float, Float>(3.25f, 0.5f, 1f));// night_watch_2_limit,21
        trophiesProperties.put(GameTrophyDefinition.NIGHT_WATCH_BONUS, new Trio<Float, Float, Float>(10.13f, 0.7f, 0.5f));// night_watch_bonus_finished,55

        trophiesProperties.put(GameTrophyDefinition.SHOOTING_RANGE_PLAY1, new Trio<Float, Float, Float>(4.0f, 0.7f, 1f));// shooting_range_1_finished,26
        trophiesProperties.put(GameTrophyDefinition.SHOOTING_RANGE_PLAY2, new Trio<Float, Float, Float>(4.1f, 0.4f, 1f));// shooting_range_5_finished,27
        trophiesProperties.put(GameTrophyDefinition.SHOOTING_RANGE_PLAY3, new Trio<Float, Float, Float>(4.5f, 0.55f, 1f));// shooting_range_10_finished,30
        trophiesProperties.put(GameTrophyDefinition.SHOOTING_RANGE_DIFF1, new Trio<Float, Float, Float>(4.35f, 0.5f, 0.6f));// shooting_range_1_limit,29
        trophiesProperties.put(GameTrophyDefinition.SHOOTING_RANGE_DIFF2, new Trio<Float, Float, Float>(4.27f, 0.7f, 0.8f));// shooting_range_2_limit,28
        trophiesProperties.put(GameTrophyDefinition.SHOOTING_RANGE_BONUS, new Trio<Float, Float, Float>(10.01f, 0.15f, 0.6f));// shooting_range_bonus_finished,56

        trophiesProperties.put(GameTrophyDefinition.IN_THE_DARKNESS_PLAY1, new Trio<Float, Float, Float>(4.7f, 0.43f, 0.7f));// in_the_darkness_1_finished,32
        trophiesProperties.put(GameTrophyDefinition.IN_THE_DARKNESS_PLAY2, new Trio<Float, Float, Float>(5.05f, 0.77f, 0.7f));// in_the_darkness_5_finished,33
        trophiesProperties.put(GameTrophyDefinition.IN_THE_DARKNESS_PLAY3, new Trio<Float, Float, Float>(4.85f, 0.1f, 0.7f));// in_the_darkness_10_finished.34
        trophiesProperties.put(GameTrophyDefinition.IN_THE_DARKNESS_DIFF1, new Trio<Float, Float, Float>(4.85f, 0.65f, 0.7f));// in_the_darkness_1_limit,31
        trophiesProperties.put(GameTrophyDefinition.IN_THE_DARKNESS_DIFF2, new Trio<Float, Float, Float>(5.21f, 0.4f, 0.7f));// in_the_darkness_2_limit,35
        trophiesProperties.put(GameTrophyDefinition.IN_THE_DARKNESS_BONUS, new Trio<Float, Float, Float>(10.66f, 0.13f, 0.5f));// in_the_darkness_bonus_finished,62

        trophiesProperties.put(GameTrophyDefinition.CRIME_SCENE_PLAY1, new Trio<Float, Float, Float>(5.6f, 0.65f, 0.5f));// crime_scene_1_finished,36
        trophiesProperties.put(GameTrophyDefinition.CRIME_SCENE_PLAY2, new Trio<Float, Float, Float>(5.45f, 0.42f, 0.5f));// crime_scene_5_finished,37
        trophiesProperties.put(GameTrophyDefinition.CRIME_SCENE_PLAY3, new Trio<Float, Float, Float>(5.75f,  0.23f, 0.5f));// crime_scene_10_finished.38
        trophiesProperties.put(GameTrophyDefinition.CRIME_SCENE_DIFF1, new Trio<Float, Float, Float>(6.36f, 0.12f, 0.5f));// crime_scene_1_limit,39
        trophiesProperties.put(GameTrophyDefinition.CRIME_SCENE_DIFF2, new Trio<Float, Float, Float>(6.1f, 0.14f, 0.5f));// crime_scene_2_limit,40
        trophiesProperties.put(GameTrophyDefinition.CRIME_SCENE_BONUS, new Trio<Float, Float, Float>(10.93f, 0.3f, 0.6f));// crime_scene_bonus_finished,64

        trophiesProperties.put(GameTrophyDefinition.RUNES_PLAY1, new Trio<Float, Float, Float>(5.89f, 0.81f, 0.5f));// runes_1_finished,41
        trophiesProperties.put(GameTrophyDefinition.RUNES_PLAY2, new Trio<Float, Float, Float>(5.63f, 0.21f, 0.5f));// runes_5_finished,42
        trophiesProperties.put(GameTrophyDefinition.RUNES_PLAY3, new Trio<Float, Float, Float>(5.36f, 0.63f, 0.5f));// runes_10_finished,43
        trophiesProperties.put(GameTrophyDefinition.RUNES_DIFF1, new Trio<Float, Float, Float>(5.37f, 0.1f, 0.5f));// runes_1_limit,44
        trophiesProperties.put(GameTrophyDefinition.RUNES_DIFF2, new Trio<Float, Float, Float>(5.98f, 0.21f, 0.5f));// runes_2_limit,45
        trophiesProperties.put(GameTrophyDefinition.RUNES_BONUS, new Trio<Float, Float, Float>(10.83f, 0.21f, 0.6f));// runes_bonus_finished,63

        trophiesProperties.put(GameTrophyDefinition.PROTOCOL_PLAY1, new Trio<>(6.6f,0.5f,0.5f)); //protocol_1_finished,46
        trophiesProperties.put(GameTrophyDefinition.PROTOCOL_PLAY2, new Trio<>(6.72f,0.2f,0.5f)); //protocol_5_finished,47
        trophiesProperties.put(GameTrophyDefinition.PROTOCOL_PLAY3, new Trio<>(6.87f,0.28f,0.5f)); //protocol_10_finished,48
        trophiesProperties.put(GameTrophyDefinition.PROTOCOL_DIFF1, new Trio<>(7.05f,0.2f,0.5f));  //protocol_1_limit,49
        trophiesProperties.put(GameTrophyDefinition.PROTOCOL_DIFF2, new Trio<>(7.2f,0.4f,0.5f));  //protocol_2_limit,50
        trophiesProperties.put(GameTrophyDefinition.PROTOCOL_BONUS, new Trio<>(10.23f, 0.2f, 0.6f));// protocol_bonus_finished,57

        trophiesProperties.put(GameTrophyDefinition.SAFE_PLAY1, new Trio<>(7.4f,0.55f,0.5f)); //safe_1_finished,51
        trophiesProperties.put(GameTrophyDefinition.SAFE_PLAY2, new Trio<>(7.48f,0.2f,0.5f)); //safe_5_finished,52
        trophiesProperties.put(GameTrophyDefinition.SAFE_PLAY3, new Trio<>(7.6f,0.33f,0.5f)); //safe_10_finished,53
        trophiesProperties.put(GameTrophyDefinition.SAFE_DIFF1, new Trio<>(7.7f,0.6f,0.5f));  //safe_1_limit,54
        trophiesProperties.put(GameTrophyDefinition.SAFE_DIFF2, new Trio<>(7.8f,0.22f,0.5f));  //safe_2_limit,55
        trophiesProperties.put(GameTrophyDefinition.SAFE_BONUS, new Trio<>(10.38f, 0.03f, 0.5f));// safe_bonus_finished,58

        trophiesProperties.put(GameTrophyDefinition.ON_THE_TRAIL_PLAY1, new Trio<>(8.0f,0.35f,0.5f)); //on_the_trail_1_finished,60
        trophiesProperties.put(GameTrophyDefinition.ON_THE_TRAIL_PLAY2, new Trio<>(8.14f,0.45f,0.5f)); //on_the_trail_5_finished,61
        trophiesProperties.put(GameTrophyDefinition.ON_THE_TRAIL_PLAY3, new Trio<>(8.26f,0.28f,0.5f)); //on_the_trail_10_finished,62
        trophiesProperties.put(GameTrophyDefinition.ON_THE_TRAIL_DIFF1, new Trio<>(8.05f,0.1f,0.5f));  //on_the_trail_1_limit,63
        trophiesProperties.put(GameTrophyDefinition.ON_THE_TRAIL_DIFF2, new Trio<>(8.37f,0.15f,0.5f));  //on_the_trail_2_limit,64
        trophiesProperties.put(GameTrophyDefinition.ON_THE_TRAIL_BONUS, new Trio<>(10.14f, 0.1f, 0.5f));//on_the_trail_bonus_finished,65

        trophiesProperties.put(GameTrophyDefinition.MEMORY_GAME_PLAY1, new Trio<>(8.49f,0.27f,0.5f)); //memory_game_1_finished,66
        trophiesProperties.put(GameTrophyDefinition.MEMORY_GAME_PLAY2, new Trio<>(8.84f,0.62f,0.5f)); //memory_game_5_finished,67
        trophiesProperties.put(GameTrophyDefinition.MEMORY_GAME_PLAY3, new Trio<>(8.94f,0.45f,0.5f)); //memory_game_10_finished,68
        trophiesProperties.put(GameTrophyDefinition.MEMORY_GAME_DIFF1, new Trio<>(8.66f,0.7f,0.5f));  //memory_game_1_limit,69
        trophiesProperties.put(GameTrophyDefinition.MEMORY_GAME_DIFF2, new Trio<>(8.74f,0.4f,0.5f));  //memory_game_2_limit,70
        trophiesProperties.put(GameTrophyDefinition.MEMORY_GAME_BONUS, new Trio<>(9.96f, 0.7f, 0.5f));// memory_game_bonus_finished,71

        trophiesProperties.put(GameTrophyDefinition.ATTENTION_GAME_PLAY1, new Trio<>(9.1f,0.65f,0.6f)); //attention_game_1_finished,66
        trophiesProperties.put(GameTrophyDefinition.ATTENTION_GAME_PLAY2, new Trio<>(9.15f,0.2f,0.6f)); //attention_game_5_finished,67
        trophiesProperties.put(GameTrophyDefinition.ATTENTION_GAME_PLAY3, new Trio<>(9.6f,0.38f,0.6f)); //attention_game_10_finished,68
        trophiesProperties.put(GameTrophyDefinition.ATTENTION_GAME_DIFF1, new Trio<>(9.45f,0.45f,0.6f));  //attention_game_1_limit,69
        trophiesProperties.put(GameTrophyDefinition.ATTENTION_GAME_DIFF2, new Trio<>(9.29f,0.4f,0.6f));  //attention_game_2_limit,70
        trophiesProperties.put(GameTrophyDefinition.ATTENTION_GAME_BONUS, new Trio<>(11.05f, 0.41f, 0.6f));//attention_game_bonus_finished,71

        //USER TROPHIES
        trophiesProperties.put(UserTrophyDefinition.CONSECUTIVELY_DAYSBACK_3, new Trio<Float, Float, Float>(0.87f, 0.38f, 1f));//user_consecutively3days,2
        trophiesProperties.put(UserTrophyDefinition.CONSECUTIVELY_DAYSBACK_5, new Trio<Float, Float, Float>(1.3f, 0.75f, 1f));//user_consecutively5days,6
        trophiesProperties.put(UserTrophyDefinition.CONSECUTIVELY_DAYSBACK_8, new Trio<Float, Float, Float>(2.45f, 0.6f, 1f));//user_consecutively8days,18
        trophiesProperties.put(UserTrophyDefinition.ONEDAYALLGAMES_0STARS, new Trio<Float, Float, Float>(3.05f, 0.55f, 1f));//user_consecutivelyallgames0stars,20
        trophiesProperties.put(UserTrophyDefinition.CONSECUTIVELYALLGAMES_MEDIUMDIFFICULTY, new Trio<Float, Float, Float>(1.92f, 0.65f, 1f));//user_consecutivelyallgames2stars,13
        trophiesProperties.put(UserTrophyDefinition.CONSECUTIVELYALLGAMES_HARDDIFFICULTY, new Trio<Float, Float, Float>(1.3f, 0.05f, 1f));//user_consecutivelyallgames3stars,8

    }

    public static final Map<ITrophyDefinition, Trio<Float,Float,Float>> trophiesPropertiesRanked;//x relative position, y relative position, scale relative to original size

    static {
        trophiesPropertiesRanked = new HashMap<ITrophyDefinition, Trio<Float, Float,Float>>();

        trophiesPropertiesRanked.put(GameTrophyDefinition.ROBBERY_PLAY1, new Trio<Float, Float, Float>(1.41f, 0.62f, 0.6f));// robbery_1_finished1,17
        trophiesPropertiesRanked.put(GameTrophyDefinition.ROBBERY_PLAY2, new Trio<Float, Float, Float>(1.69f, 0.43f, 0.6f));// robbery_5_finished,14
        trophiesPropertiesRanked.put(GameTrophyDefinition.ROBBERY_PLAY3, new Trio<Float, Float, Float>(1.8f, 0.37f, 0.6f));// robbery_10_finished,15
        trophiesPropertiesRanked.put(GameTrophyDefinition.ROBBERY_DIFF1, new Trio<Float, Float, Float>(1.4f, 0.3f, 0.6f));// robbery_1_limit,16
        trophiesPropertiesRanked.put(GameTrophyDefinition.ROBBERY_DIFF2, new Trio<Float, Float, Float>(2.01f, 0.32f, 0.7f));// robbery_2_limit,19
        trophiesPropertiesRanked.put(GameTrophyDefinition.ROBBERY_BONUS, new Trio<Float, Float, Float>(1.57f, 0.31f, 0.6f));// robbery_bonus_finished,59

        trophiesPropertiesRanked.put(GameTrophyDefinition.PURSUIT_PLAY1, new Trio<Float, Float, Float>(2.32f, 0.48f, 0.6f));// pursuit_1_finished,9
        trophiesPropertiesRanked.put(GameTrophyDefinition.PURSUIT_PLAY2, new Trio<Float, Float, Float>(2.63f, 0.32f, 0.6f));// pursuit_5_finished,7
        trophiesPropertiesRanked.put(GameTrophyDefinition.PURSUIT_PLAY3, new Trio<Float, Float, Float>(2.44f, 0.6f, 0.6f));// pursuit_10_finished,10
        trophiesPropertiesRanked.put(GameTrophyDefinition.PURSUIT_DIFF1, new Trio<Float, Float, Float>(2.9f, 0.5f, 0.6f));// pursuit_1_limit,11
        trophiesPropertiesRanked.put(GameTrophyDefinition.PURSUIT_DIFF2, new Trio<Float, Float, Float>(2.64f, 0.62f, 0.6f));// pursuit_2_limit,12
        trophiesPropertiesRanked.put(GameTrophyDefinition.PURSUIT_BONUS, new Trio<Float, Float, Float>(2.44f, 0.15f, 0.6f));// pursuit_bonus_finished,61

        trophiesPropertiesRanked.put(GameTrophyDefinition.KIDNAPPING_PLAY1, new Trio<Float, Float, Float>(3.19f, 0.5f, 0.8f));// kidnapping_1_finished,3
        trophiesPropertiesRanked.put(GameTrophyDefinition.KIDNAPPING_PLAY2, new Trio<Float, Float, Float>(3.3f, 0.15f, 0.8f));// kidnapping_5_finished,4
        trophiesPropertiesRanked.put(GameTrophyDefinition.KIDNAPPING_PLAY3, new Trio<Float, Float, Float>(3.52f, 0.1f, 0.8f));// kidnapping_10_finished,5
        trophiesPropertiesRanked.put(GameTrophyDefinition.KIDNAPPING_DIFF1, new Trio<Float, Float, Float>(3.67f, 0.16f, 0.8f));// kidnapping_1_limit,1
        trophiesPropertiesRanked.put(GameTrophyDefinition.KIDNAPPING_DIFF2, new Trio<Float, Float, Float>(3.39f, 0.32f, 0.8f));// kidnapping_2_limit,0
        trophiesPropertiesRanked.put(GameTrophyDefinition.KIDNAPPING_BONUS, new Trio<Float, Float, Float>(3.54f, 0.6f, 0.7f));// kidnapping_bonus_finished,60

        trophiesPropertiesRanked.put(GameTrophyDefinition.NIGHT_WATCH_PLAY1, new Trio<Float, Float, Float>(3.84f, 0.5f, 0.7f));// night_watch_1_finished,22
        trophiesPropertiesRanked.put(GameTrophyDefinition.NIGHT_WATCH_PLAY2, new Trio<Float, Float, Float>(3.95f, 0.33f, 0.7f));// night_watch_5_finished,23
        trophiesPropertiesRanked.put(GameTrophyDefinition.NIGHT_WATCH_PLAY3, new Trio<Float, Float, Float>(4.19f, 0.33f, 0.7f));// night_watch_10_finished,25
        trophiesPropertiesRanked.put(GameTrophyDefinition.NIGHT_WATCH_DIFF1, new Trio<Float, Float, Float>(4.02f, 0.64f, 0.7f));// night_watch_1_limit,24
        trophiesPropertiesRanked.put(GameTrophyDefinition.NIGHT_WATCH_DIFF2, new Trio<Float, Float, Float>(4.35f, 0.4f, 0.7f));// night_watch_2_limit,21
        trophiesPropertiesRanked.put(GameTrophyDefinition.NIGHT_WATCH_BONUS, new Trio<Float, Float, Float>(4.07f, 0.2f, 0.6f));// night_watch_bonus_finished,55

        trophiesPropertiesRanked.put(GameTrophyDefinition.SHOOTING_RANGE_PLAY1, new Trio<Float, Float, Float>(4.5f, 0.64f, 0.7f));// shooting_range_1_finished,26
        trophiesPropertiesRanked.put(GameTrophyDefinition.SHOOTING_RANGE_PLAY2, new Trio<Float, Float, Float>(4.7f, 0.5f, 0.7f));// shooting_range_5_finished,27
        trophiesPropertiesRanked.put(GameTrophyDefinition.SHOOTING_RANGE_PLAY3, new Trio<Float, Float, Float>(4.86f, 0.64f, 0.7f));// shooting_range_10_finished,30
        trophiesPropertiesRanked.put(GameTrophyDefinition.SHOOTING_RANGE_DIFF1, new Trio<Float, Float, Float>(4.56f, 0.18f, 0.7f));// shooting_range_1_limit,29
        trophiesPropertiesRanked.put(GameTrophyDefinition.SHOOTING_RANGE_DIFF2, new Trio<Float, Float, Float>(4.98f, 0.41f, 0.7f));// shooting_range_2_limit,28
        trophiesPropertiesRanked.put(GameTrophyDefinition.SHOOTING_RANGE_BONUS, new Trio<Float, Float, Float>(4.85f, 0.1f, 0.7f));// shooting_range_bonus_finished,56

        trophiesPropertiesRanked.put(GameTrophyDefinition.IN_THE_DARKNESS_PLAY1, new Trio<Float, Float, Float>(5.1f, 0.62f, 0.6f));// in_the_darkness_1_finished,32
        trophiesPropertiesRanked.put(GameTrophyDefinition.IN_THE_DARKNESS_PLAY2, new Trio<Float, Float, Float>(5.17f, 0.32f, 0.7f));// in_the_darkness_5_finished,33
        trophiesPropertiesRanked.put(GameTrophyDefinition.IN_THE_DARKNESS_PLAY3, new Trio<Float, Float, Float>(5.21f, 0.8f, 0.7f));// in_the_darkness_10_finished.34
        trophiesPropertiesRanked.put(GameTrophyDefinition.IN_THE_DARKNESS_DIFF1, new Trio<Float, Float, Float>(5.32f, 0.6f, 0.7f));// in_the_darkness_1_limit,31
        trophiesPropertiesRanked.put(GameTrophyDefinition.IN_THE_DARKNESS_DIFF2, new Trio<Float, Float, Float>(5.45f, 0.4f, 0.7f));// in_the_darkness_2_limit,35
        trophiesPropertiesRanked.put(GameTrophyDefinition.IN_THE_DARKNESS_BONUS, new Trio<Float, Float, Float>(5.29f, 0.2f, 0.7f));// in_the_darkness_bonus_finished,62

        trophiesPropertiesRanked.put(GameTrophyDefinition.CRIME_SCENE_PLAY1, new Trio<Float, Float, Float>(5.62f, 0.69f, 0.6f));// crime_scene_1_finished,36
        trophiesPropertiesRanked.put(GameTrophyDefinition.CRIME_SCENE_PLAY2, new Trio<Float, Float, Float>(5.76f, 0.4f, 0.6f));// crime_scene_5_finished,37
        trophiesPropertiesRanked.put(GameTrophyDefinition.CRIME_SCENE_PLAY3, new Trio<Float, Float, Float>(5.63f,  0.17f, 0.7f));// crime_scene_10_finished.38
        trophiesPropertiesRanked.put(GameTrophyDefinition.CRIME_SCENE_DIFF1, new Trio<Float, Float, Float>(5.86f, 0.1f, 0.7f));// crime_scene_1_limit,39
        trophiesPropertiesRanked.put(GameTrophyDefinition.CRIME_SCENE_DIFF2, new Trio<Float, Float, Float>(6.01f, 0.2f, 0.7f));// crime_scene_2_limit,40
        trophiesPropertiesRanked.put(GameTrophyDefinition.CRIME_SCENE_BONUS, new Trio<Float, Float, Float>(6.32f, 0.15f, 0.7f));// crime_scene_bonus_finished,64

        trophiesPropertiesRanked.put(GameTrophyDefinition.RUNES_PLAY1, new Trio<Float, Float, Float>(6.45f, 0.72f, 0.7f));// runes_1_finished,41
        trophiesPropertiesRanked.put(GameTrophyDefinition.RUNES_PLAY2, new Trio<Float, Float, Float>(6.5f, 0.18f, 0.7f));// runes_5_finished,42
        trophiesPropertiesRanked.put(GameTrophyDefinition.RUNES_PLAY3, new Trio<Float, Float, Float>(6.6f, 0.6f, 0.7f));// runes_10_finished,43
        trophiesPropertiesRanked.put(GameTrophyDefinition.RUNES_DIFF1, new Trio<Float, Float, Float>(6.64f, 0.15f, 0.7f));// runes_1_limit,44
        trophiesPropertiesRanked.put(GameTrophyDefinition.RUNES_DIFF2, new Trio<Float, Float, Float>(6.74f, 0.55f, 0.7f));// runes_2_limit,45
        trophiesPropertiesRanked.put(GameTrophyDefinition.RUNES_BONUS, new Trio<Float, Float, Float>(6.86f, 0.14f, 0.7f));// runes_bonus_finished,63

        trophiesPropertiesRanked.put(GameTrophyDefinition.PROTOCOL_PLAY1, new Trio<>(7.03f,0.7f,0.7f)); //protocol_1_finished,46
        trophiesPropertiesRanked.put(GameTrophyDefinition.PROTOCOL_PLAY2, new Trio<>(7.04f,0.15f,0.7f)); //protocol_5_finished,47
        trophiesPropertiesRanked.put(GameTrophyDefinition.PROTOCOL_PLAY3, new Trio<>(7.34f,0.31f,0.7f)); //protocol_10_finished,48
        trophiesPropertiesRanked.put(GameTrophyDefinition.PROTOCOL_DIFF1, new Trio<>(7.17f,0.15f,0.7f));  //protocol_1_limit,49
        trophiesPropertiesRanked.put(GameTrophyDefinition.PROTOCOL_DIFF2, new Trio<>(7.49f,0.4f,0.7f));  //protocol_2_limit,50
        trophiesPropertiesRanked.put(GameTrophyDefinition.PROTOCOL_BONUS, new Trio<>(7.62f, 0.15f, 0.7f));// protocol_bonus_finished,57

        trophiesPropertiesRanked.put(GameTrophyDefinition.SAFE_PLAY1, new Trio<>(7.81f,0.5f,0.7f)); //safe_1_finished,51
        trophiesPropertiesRanked.put(GameTrophyDefinition.SAFE_PLAY2, new Trio<>(8.1f,0.33f,0.7f)); //safe_5_finished,52
        trophiesPropertiesRanked.put(GameTrophyDefinition.SAFE_PLAY3, new Trio<>(7.94f,0.55f,0.7f)); //safe_10_finished,53
        trophiesPropertiesRanked.put(GameTrophyDefinition.SAFE_DIFF1, new Trio<>(8.41f,0.5f,0.7f));  //safe_1_limit,54
        trophiesPropertiesRanked.put(GameTrophyDefinition.SAFE_DIFF2, new Trio<>(8.21f,0.15f,0.7f));  //safe_2_limit,55
        trophiesPropertiesRanked.put(GameTrophyDefinition.SAFE_BONUS, new Trio<>(7.89f, 0.1f, 0.7f));// safe_bonus_finished,58

        trophiesPropertiesRanked.put(GameTrophyDefinition.ON_THE_TRAIL_PLAY1, new Trio<>(8.65f,0.35f,0.7f)); //on_the_trail_1_finished,60
        trophiesPropertiesRanked.put(GameTrophyDefinition.ON_THE_TRAIL_PLAY2, new Trio<>(9.23f,0.6f,0.7f)); //on_the_trail_5_finished,61
        trophiesPropertiesRanked.put(GameTrophyDefinition.ON_THE_TRAIL_PLAY3, new Trio<>(8.93f,0.3f,0.6f)); //on_the_trail_10_finished,62
        trophiesPropertiesRanked.put(GameTrophyDefinition.ON_THE_TRAIL_DIFF1, new Trio<>(9.20f,0.15f,0.7f));  //on_the_trail_1_limit,63
        trophiesPropertiesRanked.put(GameTrophyDefinition.ON_THE_TRAIL_DIFF2, new Trio<>(8.73f,0.11f,0.6f));  //on_the_trail_2_limit,64
        trophiesPropertiesRanked.put(GameTrophyDefinition.ON_THE_TRAIL_BONUS, new Trio<>(9.08f, 0.2f, 0.6f));//on_the_trail_bonus_finished,65

        trophiesPropertiesRanked.put(GameTrophyDefinition.MEMORY_GAME_PLAY1, new Trio<>(9.42f,0.3f,0.7f)); //memory_game_1_finished,66
        trophiesPropertiesRanked.put(GameTrophyDefinition.MEMORY_GAME_PLAY2, new Trio<>(9.6f,0.62f,0.7f)); //memory_game_5_finished,67
        trophiesPropertiesRanked.put(GameTrophyDefinition.MEMORY_GAME_PLAY3, new Trio<>(9.88f,0.4f,0.7f)); //memory_game_10_finished,68
        trophiesPropertiesRanked.put(GameTrophyDefinition.MEMORY_GAME_DIFF1, new Trio<>(9.72f,0.63f,0.7f));  //memory_game_1_limit,69
        trophiesPropertiesRanked.put(GameTrophyDefinition.MEMORY_GAME_DIFF2, new Trio<>(9.62f,0.1f,0.7f));  //memory_game_2_limit,70
        trophiesPropertiesRanked.put(GameTrophyDefinition.MEMORY_GAME_BONUS, new Trio<>(10.07f, 0.4f, 0.7f));// memory_game_bonus_finished,71

        trophiesPropertiesRanked.put(GameTrophyDefinition.ATTENTION_GAME_PLAY1, new Trio<>(10.29f,0.5f,0.7f)); //attention_game_1_finished,66
        trophiesPropertiesRanked.put(GameTrophyDefinition.ATTENTION_GAME_PLAY2, new Trio<>(10.49f,0.1f,0.7f)); //attention_game_5_finished,67
        trophiesPropertiesRanked.put(GameTrophyDefinition.ATTENTION_GAME_PLAY3, new Trio<>(10.5f,0.5f,0.7f)); //attention_game_10_finished,68
        trophiesPropertiesRanked.put(GameTrophyDefinition.ATTENTION_GAME_DIFF1, new Trio<>(10.66f,0.49f,0.7f));  //attention_game_1_limit,69
        trophiesPropertiesRanked.put(GameTrophyDefinition.ATTENTION_GAME_DIFF2, new Trio<>(10.77f,0.49f,0.7f));  //attention_game_2_limit,70
        trophiesPropertiesRanked.put(GameTrophyDefinition.ATTENTION_GAME_BONUS, new Trio<>(10.89f, 0.15f, 0.7f));//attention_game_bonus_finished,71

        //USER TROPHIES
        trophiesPropertiesRanked.put(UserTrophyDefinition.CONSECUTIVELY_DAYSBACK_3, new Trio<Float, Float, Float>(0.7f, 0.4f, 0.6f));//user_consecutively3days,2
        trophiesPropertiesRanked.put(UserTrophyDefinition.CONSECUTIVELY_DAYSBACK_5, new Trio<Float, Float, Float>(0.89f, 0.32f, 0.6f));//user_consecutively5days,6
        trophiesPropertiesRanked.put(UserTrophyDefinition.CONSECUTIVELY_DAYSBACK_8, new Trio<Float, Float, Float>(1.02f, 0.52f, 0.6f));//user_consecutively8days,18
        trophiesPropertiesRanked.put(UserTrophyDefinition.ONEDAYALLGAMES_0STARS, new Trio<Float, Float, Float>(0.79f, 0.5f, 0.6f));//user_consecutivelyallgames0stars,20
        trophiesPropertiesRanked.put(UserTrophyDefinition.CONSECUTIVELYALLGAMES_MEDIUMDIFFICULTY, new Trio<Float, Float, Float>(1.22f, 0.35f, 0.6f));//user_consecutivelyallgames2stars,13
        trophiesPropertiesRanked.put(UserTrophyDefinition.CONSECUTIVELYALLGAMES_HARDDIFFICULTY, new Trio<Float, Float, Float>(0.85f, 0.1f, 0.7f));//user_consecutivelyallgames3stars,8

    }

    public enum TrophyType{
        EMPTY,
        SMALL,
        FULL
    }

    public static List<String> getTrophyNamesList() {
        List<String> trophyNamesToBeLoaded = new ArrayList<String>();
        //common trophies
        for(GameTrophyDefinition trophyDefinition: GameTrophyDefinition.values()){
            for(TrophyType trophyType : TrophyType.values()){
                trophyNamesToBeLoaded.add(getFullPath(trophyDefinition.getTrophyNameKey(),trophyType));
            }
        }
        //user trophies
        for(UserTrophyDefinition trophyDefinition: UserTrophyDefinition.values()){
            for(TrophyType trophyType : TrophyType.values()){
                trophyNamesToBeLoaded.add(getFullPath(trophyDefinition.getTrophyNameKey(),trophyType));
            }
        }
        return trophyNamesToBeLoaded;
    }

    public static String getFullPath(String imageName, TrophyType trophyType){
        return TROPHY_SUBFOLDER_NAME + TROPHY_PREFIX + imageName + "_" + trophyType.toString().toLowerCase();
    }

    public static String getRankedFullPath(String imageName, TrophyType trophyType){
        return TROPHY_SUBFOLDER_NAME + RANKED_SUBFOLDER_NAME + TROPHY_PREFIX + imageName + "_" + trophyType.toString().toLowerCase();
    }


    public static List<ITrophyDefinition> getPossibleTrophiesForGame(GameDefinition gameDef) {
        List<ITrophyDefinition> list = new ArrayList<ITrophyDefinition>();

        list.addAll(Arrays.asList(UserTrophyDefinition.values()));

        for (GameTrophyDefinition trophy : GameTrophyDefinition.values()) {
            if (trophy.getGameDefinition().equals(gameDef)) {
                list.add(trophy);
            }
        }
        return list;
    }

    public static ITrophyDefinition getTrophyDefinitionByName(String name) {
        ITrophyDefinition trophy = null;

        try {
            trophy = GameTrophyDefinition.valueOf(name);
        } catch (IllegalArgumentException ex) {
            // we don't find trophy in game setion
        }

        try {
            trophy = UserTrophyDefinition.valueOf(name);
        } catch (IllegalArgumentException ex) {
            // we don't find trophy in user setion
        }

        return trophy;
    }

    public enum MouseTexts {
        MOUSE_TEXTS_0       (0,     "mouse_0_1", "mouse_0_2", "mouse_0_3", "mouse_0_4"),
        MOUSE_TEXTS_4       (4,     "mouse_4_1"),
        MOUSE_TEXTS_6       (6,     "mouse_6_1"),
        MOUSE_TEXTS_8       (8,     "mouse_8_1"),
        MOUSE_TEXTS_11      (11,    "mouse_11_1"),
        MOUSE_TEXTS_15      (15,    "mouse_15_1"),
        MOUSE_TEXTS_17      (17,    "mouse_17_1"),
        MOUSE_TEXTS_19      (19,    "mouse_19_1"),
        MOUSE_TEXTS_21      (21,    "mouse_21_1"),
        MOUSE_TEXTS_24      (24,    "mouse_24_1"),
        MOUSE_TEXTS_26      (26,    "mouse_26_1", "mouse_26_2", "mouse_26_3"),
        MOUSE_TEXTS_28      (28,    "mouse_28_1"),
        MOUSE_TEXTS_31      (31,    "mouse_31_1"),
        MOUSE_TEXTS_33      (33,    "mouse_33_1"),
        MOUSE_TEXTS_35      (35,    "mouse_35_1"),
        MOUSE_TEXTS_37      (37,    "mouse_37_1"),
        MOUSE_TEXTS_39      (39,    "mouse_39_1"),
        MOUSE_TEXTS_41      (41,    "mouse_41_1"),
        MOUSE_TEXTS_44      (44,    "mouse_44_1"),
        MOUSE_TEXTS_46      (46,    "mouse_46_1", "mouse_46_2", "mouse_46_3");

        public static final String MOUSE_RANKUP_TEXT = "mouse_rankup";

        private String[] texts;
        private int numOfStars;
        private int actualIndex = 0;

        MouseTexts(int numOfStars, String... texts) {
            this.texts = texts;
            this.numOfStars = numOfStars;
        }

        public String getNextText() {
            String result = null;

            if (actualIndex < texts.length) {
                result = texts[actualIndex];
                actualIndex++;
            }

            return result;
        }

        public int getNumOfStars() {
            return numOfStars;
        }

        public void resetTextCounter() {
            actualIndex = 0;
        }

        public static void resetAllTextCounters() {
            for (MouseTexts text : values()) {
                text.resetTextCounter();
            }
        }

        public static String getMouseText(int trophyCount) {
            MouseTexts finalText = MOUSE_TEXTS_0;

            for (MouseTexts text : values()) {
                if(trophyCount >= text.getNumOfStars() && text.getNumOfStars() > finalText.getNumOfStars()) {
                    finalText = text;
                }
            }

            return finalText.getNextText();
        }
    }
}
