/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.halloffame.assets;

/**
 * Created by Václav Tarantík on 2.3.15.
 */
public final class HallOfFameAssets {
    public static final String HALL_OF_FAME_ASSET_PREFIX    = "screen_halloffame_";

    public static final String HALL_OF_FAME_ASSET_TROPHYHEAP    = "trophyheap-";
    public static final String HALL_OF_FAME_TROPHY_PATH         = "trophy/";
    public static final String HALL_OF_FAME_RANKED_PATH         = "ranked/";

    public static final String HALL_OF_FAME_BACKGROUND_TILE         = HALL_OF_FAME_ASSET_PREFIX + "background_tile";
    public static final String HALL_OF_FAME_DOOR                    = HALL_OF_FAME_ASSET_PREFIX + "background_start";
    public static final String HALL_OF_FAME_DOOR_TITLE              = HALL_OF_FAME_ASSET_PREFIX + "background_start_title";
    public static final String HALL_OF_FAME_DOOR_BUTTON_PRESSED     = HALL_OF_FAME_ASSET_PREFIX + "background_door_pressed";
    public static final String HALL_OF_FAME_DOOR_BUTTON_UNPRESSED   = HALL_OF_FAME_ASSET_PREFIX + "background_door_unpressed";
    public static final String HALL_OF_FAME_WALL                    = HALL_OF_FAME_ASSET_PREFIX + "wall";

    public static final String HALL_OF_FAME_BACKGROUND_TILE_RANKED          = HALL_OF_FAME_RANKED_PATH + HALL_OF_FAME_ASSET_PREFIX + "background_tile";
    public static final String HALL_OF_FAME_DOOR_RANKED                     = HALL_OF_FAME_RANKED_PATH + HALL_OF_FAME_ASSET_PREFIX + "background_start";
    public static final String HALL_OF_FAME_DOOR_TITLE_RANKED               = HALL_OF_FAME_RANKED_PATH + HALL_OF_FAME_ASSET_PREFIX + "background_start_title";
    public static final String HALL_OF_FAME_DOOR_BUTTON_PRESSED_RANKED      = HALL_OF_FAME_RANKED_PATH + HALL_OF_FAME_ASSET_PREFIX + "background_door_pressed";
    public static final String HALL_OF_FAME_DOOR_BUTTON_UNPRESSED_RANKED    = HALL_OF_FAME_RANKED_PATH + HALL_OF_FAME_ASSET_PREFIX + "background_door_unpressed";

    public static final String HALL_OF_FAME_MOUSE_CHEESE        = HALL_OF_FAME_ASSET_PREFIX + "mouse_cheese_pressed";
    public static final String HALL_OF_FAME_MOUSE_PRESSED       = HALL_OF_FAME_ASSET_PREFIX + "mouse_pressed";
    public static final String HALL_OF_FAME_MOUSE_UNPRESSED     = HALL_OF_FAME_ASSET_PREFIX + "mouse_unpressed";
    public static final String HALL_OF_FAME_CARPET              = HALL_OF_FAME_ASSET_PREFIX + "background_carpet";
    public static final String HALL_OF_FAME_DIALOG_BANNER       = HALL_OF_FAME_ASSET_PREFIX + "dialog_banner";
    public static final String HALL_OF_FAME_BADGE_BOARD         = HALL_OF_FAME_ASSET_PREFIX + "badgeboard";

    public static final String TROPHY_HEAP_PATH = HALL_OF_FAME_TROPHY_PATH + "trophyheap/";

    public static final String HALL_OF_FAME_TROPHY_HEAP_00  = TROPHY_HEAP_PATH +   "0";
    public static final String HALL_OF_FAME_TROPHY_HEAP_01  = TROPHY_HEAP_PATH +   "1";
    public static final String HALL_OF_FAME_TROPHY_HEAP_02  = TROPHY_HEAP_PATH +   "2";
    public static final String HALL_OF_FAME_TROPHY_HEAP_03  = TROPHY_HEAP_PATH +   "3";
    public static final String HALL_OF_FAME_TROPHY_HEAP_04  = TROPHY_HEAP_PATH +   "4";
    public static final String HALL_OF_FAME_TROPHY_HEAP_05  = TROPHY_HEAP_PATH +   "5";
    public static final String HALL_OF_FAME_TROPHY_HEAP_06  = TROPHY_HEAP_PATH +   "6";
    public static final String HALL_OF_FAME_TROPHY_HEAP_07  = TROPHY_HEAP_PATH +   "7";
    public static final String HALL_OF_FAME_TROPHY_HEAP_08  = TROPHY_HEAP_PATH +   "8";
    public static final String HALL_OF_FAME_TROPHY_HEAP_09  = TROPHY_HEAP_PATH +   "9";
    public static final String HALL_OF_FAME_TROPHY_HEAP_10  = TROPHY_HEAP_PATH +  "10";
    public static final String HALL_OF_FAME_TROPHY_HEAP_15  = TROPHY_HEAP_PATH +  "15";
    public static final String HALL_OF_FAME_TROPHY_HEAP_20  = TROPHY_HEAP_PATH +  "20";
    public static final String HALL_OF_FAME_TROPHY_HEAP_30  = TROPHY_HEAP_PATH +  "30";
    public static final String HALL_OF_FAME_TROPHY_HEAP_40  = TROPHY_HEAP_PATH +  "40";
    public static final String HALL_OF_FAME_TROPHY_HEAP_60  = TROPHY_HEAP_PATH +  "60";
    public static final String HALL_OF_FAME_TROPHY_HEAP_80  = TROPHY_HEAP_PATH +  "80";
    public static final String HALL_OF_FAME_TROPHY_HEAP_100 = TROPHY_HEAP_PATH + "100";
    public static final String HALL_OF_FAME_TROPHY_HEAP_130 = TROPHY_HEAP_PATH + "130";
    public static final String HALL_OF_FAME_TROPHY_HEAP_160 = TROPHY_HEAP_PATH + "160";
    public static final String HALL_OF_FAME_TROPHY_HEAP_200 = TROPHY_HEAP_PATH + "200";
    public static final String HALL_OF_FAME_TROPHY_HEAP_250 = TROPHY_HEAP_PATH + "250";
    public static final String HALL_OF_FAME_TROPHY_HEAP_300 = TROPHY_HEAP_PATH + "300";
    public static final String HALL_OF_FAME_TROPHY_HEAP_350 = TROPHY_HEAP_PATH + "350";
    public static final String HALL_OF_FAME_TROPHY_HEAP_400 = TROPHY_HEAP_PATH + "400";
    public static final String HALL_OF_FAME_TROPHY_HEAP_450 = TROPHY_HEAP_PATH + "450";
    public static final String HALL_OF_FAME_TROPHY_HEAP_500 = TROPHY_HEAP_PATH + "500";
    public static final String HALL_OF_FAME_TROPHY_HEAP_550 = TROPHY_HEAP_PATH + "550";
    public static final String HALL_OF_FAME_TROPHY_HEAP_600 = TROPHY_HEAP_PATH + "600";
    public static final String HALL_OF_FAME_TROPHY_HEAP_650 = TROPHY_HEAP_PATH + "650";
    public static final String HALL_OF_FAME_TROPHY_HEAP_700 = TROPHY_HEAP_PATH + "700";

    public static final String HALL_OF_FAME_TROPHY_HEAP_00_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH +  "0";
    public static final String HALL_OF_FAME_TROPHY_HEAP_01_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH +  "1";
    public static final String HALL_OF_FAME_TROPHY_HEAP_02_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH +  "2";
    public static final String HALL_OF_FAME_TROPHY_HEAP_03_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH +  "3";
    public static final String HALL_OF_FAME_TROPHY_HEAP_04_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH +  "4";
    public static final String HALL_OF_FAME_TROPHY_HEAP_05_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH +  "5";
    public static final String HALL_OF_FAME_TROPHY_HEAP_06_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH +  "6";
    public static final String HALL_OF_FAME_TROPHY_HEAP_07_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH +  "7";
    public static final String HALL_OF_FAME_TROPHY_HEAP_08_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH +  "8";
    public static final String HALL_OF_FAME_TROPHY_HEAP_09_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH +  "9";
    public static final String HALL_OF_FAME_TROPHY_HEAP_10_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "10";
    public static final String HALL_OF_FAME_TROPHY_HEAP_15_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "15";
    public static final String HALL_OF_FAME_TROPHY_HEAP_20_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "20";
    public static final String HALL_OF_FAME_TROPHY_HEAP_30_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "30";
    public static final String HALL_OF_FAME_TROPHY_HEAP_40_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "40";
    public static final String HALL_OF_FAME_TROPHY_HEAP_60_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "60";
    public static final String HALL_OF_FAME_TROPHY_HEAP_80_RANKED   = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "80";
    public static final String HALL_OF_FAME_TROPHY_HEAP_100_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "100";
    public static final String HALL_OF_FAME_TROPHY_HEAP_130_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "130";
    public static final String HALL_OF_FAME_TROPHY_HEAP_160_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "160";
    public static final String HALL_OF_FAME_TROPHY_HEAP_200_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "200";
    public static final String HALL_OF_FAME_TROPHY_HEAP_250_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "250";
    public static final String HALL_OF_FAME_TROPHY_HEAP_300_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "300";
    public static final String HALL_OF_FAME_TROPHY_HEAP_350_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "350";
    public static final String HALL_OF_FAME_TROPHY_HEAP_400_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "400";
    public static final String HALL_OF_FAME_TROPHY_HEAP_450_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "450";
    public static final String HALL_OF_FAME_TROPHY_HEAP_500_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "500";
    public static final String HALL_OF_FAME_TROPHY_HEAP_550_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "550";
    public static final String HALL_OF_FAME_TROPHY_HEAP_600_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "600";
    public static final String HALL_OF_FAME_TROPHY_HEAP_650_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "650";
    public static final String HALL_OF_FAME_TROPHY_HEAP_700_RANKED  = TROPHY_HEAP_PATH + HALL_OF_FAME_RANKED_PATH + "700";

    public static final String TEXT_BADGE_CABINET_LIST     = "badge_cabinet_list";
    public static final String TEXT_BADGE_CABINET_LOCKED   = "badge_cabinet_locked";

    private static final String BADGE_PATH                              = "badges/";

    public static final String BADGE_NONE                          = BADGE_PATH + "badge_none";
    public static final String BADGE_1                             = BADGE_PATH + "badge1";
    public static final String BADGE_2                             = BADGE_PATH + "badge2";
    public static final String BADGE_3                             = BADGE_PATH + "badge3";
    public static final String BADGE_4                             = BADGE_PATH + "badge4";
    public static final String BADGE_5                             = BADGE_PATH + "badge5";
    public static final String BADGE_6                             = BADGE_PATH + "badge6";
    public static final String BADGE_7                             = BADGE_PATH + "badge7";
    public static final String BADGE_8                             = BADGE_PATH + "badge8";
    public static final String BADGE_9                             = BADGE_PATH + "badge9";
    public static final String BADGE_10                            = BADGE_PATH + "badge10";
    public static final String BADGE_11                            = BADGE_PATH + "badge11";
}
