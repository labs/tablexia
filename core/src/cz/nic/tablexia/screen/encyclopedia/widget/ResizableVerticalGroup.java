/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.widget;

import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;

import cz.nic.tablexia.util.Point;

/**
 * Created by Václav Tarantík on 21.8.15.
 */
public class ResizableVerticalGroup extends VerticalGroup {
    protected Point size;

    public ResizableVerticalGroup(Point size){
        this.size = size;
    }

    @Override
    public float getPrefWidth() {
        return  size.x;
    }

    @Override
    public float getPrefHeight() {
        return size.y;
    }
}
