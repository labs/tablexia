/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import net.engio.mbassy.listener.Handler;

import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.encyclopedia.assets.EncyclopediaAssets;
import cz.nic.tablexia.screen.encyclopedia.content.ContentParser;
import cz.nic.tablexia.screen.encyclopedia.content.actors.PlaybackStateButton;
import cz.nic.tablexia.screen.encyclopedia.content.model.Content;
import cz.nic.tablexia.screen.encyclopedia.menu.MenuItem;
import cz.nic.tablexia.screen.encyclopedia.menu.MenuWidget;
import cz.nic.tablexia.screen.encyclopedia.widget.ResizableScrollPaneWithBars;
import cz.nic.tablexia.screen.encyclopedia.widget.ResizableWidgetGroup;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.MusicUtil;
import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

public class EncyclopediaScreen extends AbstractTablexiaScreen<Void> {
    public static final String MENU                         = "menu";

    public static final String BUTTON_PLUS_NAME             = "button plus";
    public static final String BUTTON_MINUS_NAME            = "button minus";
    public static final String CONTENT_TABLE                = "content table";
    public static final String CONTENT_SCOLL_PANE           = "scroll pane";
    public static final String EVENT_PAGE_SHOWED            = "page showed: ";

    private static final String SOURCE_FILE_EXTENSION           = ".json";
    private static final float  FONT_BUTTON_WIDTH               = 70;
    private static final float  FONT_BUTTON_ADAPTIVE_SIZE_RATIO = 1.3f;
    private static final float  BUTTONS_MARGIN_LEFT             = 30;
    private static final float  BUTTONS_GAP                     = 20;
    private static final float  CONTENT_RELATIVE_WIDTH          = 0.70f;
    private static final int    CONTENT_PAD_TOP                 = 30;
    private static final float  MENU_RELATIVE_HEIGHT            = 0.80f;
    private static final int    CONTENT_PAD_LEFT                = 120;

    private static final int RIGHT_BUTTONS_PAD_BOTTOM = 10;

    private static final int BUTTON_MINUS = 0;
    private static final int BUTTON_PLUS  = 1;

    private static final float MUSIC_FADE_OUT_SHORT_DURATION = 0.5f;

    private static final Color  BACKGROUND_COLOR                = TablexiaSettings.getDefaultBackgroundColor();
    private static final String FONT_PLUS_TEXT                  = "+A";
    private static final String FONT_MINUS_TEXT                 = "-A";
    private static final String MUSIC_EXTENSION                 = ".mp3";
    private static final String MENU_STATE_SCROLL_POSITION_Y    = "menuScrollPositionY";
    private static final String CONTENT_STATE_SCROLL_POSITION_Y = "contentScrollPositionY";
    private static final String MENU_SELECTED_ITEM              = "menuSelectedItem";
    private static final int    DEFAULT_SELECTED_MENU_ITEM      = 0;
    public static final String  ENCYCLOPEDIA_SPEECH             = MFX_PATH + "encyclopedia.mp3";

    private static final boolean SHOW_BOUNDING_BOXES = TablexiaSettings.getInstance().isShowBoundingBoxes();

    private Table                       rightPanelStack;
    private Table                       container;
    private Table                       contentStack;
    private Table                       content;
    private ScrollPane                  contentScrollPane;
    private Table                       rightPanel;
    private MenuWidget                  menu;
    private ScrollPane                  menuScrollPane;
    private Group                       buttonsPanel;
    private Music                       playedMusic;
    private PlaybackStateButton			musicButton;
    private ResizeFontListener          resizeFontListener;
    private float                       rightPanePosition;
    private static EncyclopediaFont     actualRegularFont = EncyclopediaFont.DEFAULT_REGULAR;
    private static EncyclopediaFont     actualBoldFont    = EncyclopediaFont.DEFAULT_BOLD;
    private static EncyclopediaFont     actualHeaderFont  = EncyclopediaFont.DEFAULT_HEADER;

    public Music getPlayedMusic() {
        return playedMusic;
    }

    @Override
    public boolean hasSoftBackButton() {
        return true;
    }

    @Override
    protected Viewport createViewport() {
        return new ExtendViewport(TablexiaSettings.getWorldSize(), TablexiaSettings.getMinWorldHeight());
    }

    @Override
    public void setScrollableLayoutFocus() {
        if(menuScrollPane != null){
            getStage().setScrollFocus(contentScrollPane);
        }
    }

    @Override
    protected void screenPaused(Map<String, String> screenState) {
        if (menuScrollPane.getScrollPercentY() != Float.NaN) {
            screenState.put(MENU_STATE_SCROLL_POSITION_Y, String.valueOf(menuScrollPane.getScrollPercentY()));
        }
        if (contentScrollPane.getScrollPercentY() != Float.NaN) {
            screenState.put(CONTENT_STATE_SCROLL_POSITION_Y, String.valueOf(contentScrollPane.getScrollPercentY()));
        }
        screenState.put(MENU_SELECTED_ITEM, String.valueOf(menu.getSelectedItemIndex()));

        Log.debug(this.getClass(), "SCREEN ----- SAVING -------");
        for (Map.Entry<String, String> entry : screenState.entrySet()) {
            Log.debug(this.getClass(), entry.getKey() + ": " + entry.getValue());
        }
        Log.info(getClass(),"CONTENT: " + content.getY() + " TABLEXIA DIF: " + ( (getSceneInnerHeight()-content.getHeight())));
        Log.debug(this.getClass(), "content.size: " + contentScrollPane.getMaxY());
    }

    @Override
    protected void screenLoaded(Map<String, String> screenState) {
        resizeFontListener = new ResizeFontListener();
        container = new Table();
        container.setDebug(SHOW_BOUNDING_BOXES, SHOW_BOUNDING_BOXES);
        container.setBackground(new TextureRegionDrawable(getColorTextureRegion(BACKGROUND_COLOR)));
        getStage().addActor(container);
        container.setFillParent(true);

        prepareContentLayout();
        prepareRightMenu();

        int selectedItem = DEFAULT_SELECTED_MENU_ITEM;
        if (screenState.containsKey(MENU_SELECTED_ITEM)) {
            selectedItem = Integer.valueOf(screenState.get(MENU_SELECTED_ITEM));
        }
        menu.selectItem(selectedItem);
        prepareButtons();

        if (screenState.containsKey(MENU_STATE_SCROLL_POSITION_Y)) {
            menuScrollPane.pack();
            menuScrollPane.setScrollPercentY(Float.valueOf(screenState.get(MENU_STATE_SCROLL_POSITION_Y)));
            menuScrollPane.updateVisualScroll();
        }

        if (screenState.containsKey(CONTENT_STATE_SCROLL_POSITION_Y)) {
            contentScrollPane.pack();
            contentScrollPane.setScrollPercentY(Float.valueOf(screenState.get(CONTENT_STATE_SCROLL_POSITION_Y)));
            contentScrollPane.updateVisualScroll();
        }

        prepareMouseListener();
        getStage().setScrollFocus(contentScrollPane);

        contentStack.pack();
    }

    @Override
    protected void screenResized(int width, int height) {
        float contentWidth = CONTENT_RELATIVE_WIDTH * getViewportWidth();
        contentStack.setSize(contentWidth, getViewportHeight());
        contentScrollPane.setSize(contentWidth, getViewportHeight());
        float rightPanelWidth = (1 - CONTENT_RELATIVE_WIDTH) * getViewportWidth();
        rightPanelStack.setSize(rightPanelWidth, getViewportHeight());
        menuScrollPane.setSize(rightPanelWidth, getViewportHeight() * MENU_RELATIVE_HEIGHT);
        buttonsPanel.setSize(rightPanelWidth, getViewportHeight() * (1 - MENU_RELATIVE_HEIGHT));
        rightPanel.setSize(rightPanelWidth, getViewportHeight());
    }

    @Override
    protected String prepareIntroMusicPath() {
        return ENCYCLOPEDIA_SPEECH;
    }

    private void prepareContentLayout() {
        //TODO add culling
        contentStack = new Table();
        contentStack.setDebug(SHOW_BOUNDING_BOXES, SHOW_BOUNDING_BOXES);
        contentStack.setSize(CONTENT_RELATIVE_WIDTH * getViewportWidth(), getViewportHeight());
        content = new Table();
        content.padLeft(CONTENT_PAD_LEFT);
        content.align(Align.top);
        contentScrollPane = new ResizableScrollPaneWithBars(content, false, true);
        contentScrollPane.setScrollingDisabled(true, false);
        contentScrollPane.setSize(CONTENT_RELATIVE_WIDTH * getViewportWidth(), getViewportHeight());

        contentScrollPane.setName(CONTENT_SCOLL_PANE);
        content.setName(CONTENT_TABLE);

        contentStack.add(contentScrollPane).padTop(CONTENT_PAD_TOP);
        container.add(contentStack);
    }


    private void prepareRightMenu() {
        float rightPanelWidth = (1 - CONTENT_RELATIVE_WIDTH) * getSceneWidth();
        menu = new MenuWidget(this, new Point(rightPanelWidth, getSceneInnerHeight() * MENU_RELATIVE_HEIGHT), EncyclopediaAssets.BOOKMARK, new MenuWidget.OnMenuItemClickListener() {
            @Override
            public void onMenuItemClicked(MenuItem clickedMenuItem) {
                showPage(clickedMenuItem.getResourcePageName());
                updateMenuScrollPosition(clickedMenuItem.ordinal() + 1);
                disposeMusic();
            }
        });


        menuScrollPane = new ResizableScrollPaneWithBars(menu, false, true);
        menuScrollPane.setScrollingDisabled(true, false);

        menuScrollPane.setSize(rightPanelWidth, getViewportHeight() * MENU_RELATIVE_HEIGHT);

        rightPanePosition = getViewportWidth() - rightPanelWidth;

        buttonsPanel = new ResizableWidgetGroup();
        buttonsPanel.setSize(rightPanelWidth, getViewportHeight() * (1 - MENU_RELATIVE_HEIGHT));

        rightPanelStack = new Table();

        rightPanel = new Table();
        rightPanel.setPosition(rightPanePosition, getViewportHeight());
        rightPanel.setSize(rightPanelWidth, getViewportHeight());
        rightPanel.add(menuScrollPane);
        rightPanel.row();
        rightPanel.add(buttonsPanel);
        rightPanelStack.add(rightPanel).padTop(CONTENT_PAD_TOP);

        container.add(rightPanelStack);

        menu.setName(MENU);
        menu.updateSize(rightPanelWidth);
    }

    private void prepareMouseListener() {
        if (TablexiaSettings.getInstance().getPlatform() == TablexiaSettings.Platform.DESKTOP) {
            container.addListener(new InputListener() {
                @Override
                public boolean mouseMoved(InputEvent event, float x, float y) {
                    if (x > rightPanePosition && getStage().getScrollFocus() != menuScrollPane) {
                        getStage().setScrollFocus(menuScrollPane);
                    } else if (x < rightPanePosition && getStage().getScrollFocus() != contentScrollPane) {
                        getStage().setScrollFocus(contentScrollPane);
                    }

                    return super.mouseMoved(event, x, y);
                }
            });
        }
    }

    private void prepareButtons() {
        TablexiaButton btnFontPlus = new StandardTablexiaButton(FONT_PLUS_TEXT, StandardTablexiaButton.TablexiaButtonType.BLUE)
                .setButtonBounds(BUTTONS_MARGIN_LEFT, RIGHT_BUTTONS_PAD_BOTTOM, FONT_BUTTON_WIDTH, FONT_BUTTON_WIDTH)
                .adaptiveSizeRatio(FONT_BUTTON_ADAPTIVE_SIZE_RATIO)
                .setInputListener(resizeFontListener);

        TablexiaButton btnFontMinus = new StandardTablexiaButton(FONT_MINUS_TEXT, StandardTablexiaButton.TablexiaButtonType.BLUE)
                .setButtonBounds(btnFontPlus.getX() + btnFontPlus.getWidth() + BUTTONS_GAP, RIGHT_BUTTONS_PAD_BOTTOM, FONT_BUTTON_WIDTH, FONT_BUTTON_WIDTH)
                .adaptiveSizeRatio(FONT_BUTTON_ADAPTIVE_SIZE_RATIO)
                .setInputListener(resizeFontListener);

        btnFontMinus.setUserObject(BUTTON_MINUS);
        btnFontPlus.setUserObject(BUTTON_PLUS);

        btnFontPlus.setName(BUTTON_PLUS_NAME);
        btnFontMinus.setName(BUTTON_MINUS_NAME);

        buttonsPanel.addActor(btnFontPlus);
        buttonsPanel.addActor(btnFontMinus);
    }

    private void showPage(String pageResourceName) {
        if (content.hasChildren()) {
            content.clearChildren();
        }
        contentScrollPane.setScrollY(0);

        FileHandle fileHandle = getFile(pageResourceName + SOURCE_FILE_EXTENSION);
        List<Content> contentList = ContentParser.parseEncyclopediaPageContent(fileHandle.readString());

        for (Content c : contentList) {
            c.addToCell(content, this, (CONTENT_RELATIVE_WIDTH * getViewportWidth()) - CONTENT_PAD_LEFT);
            content.row();
        }
        triggerScenarioStepEvent(EVENT_PAGE_SHOWED+pageResourceName);
    }

    public void playMusic(String music, final PlaybackStateButton musicButton) {
        if (this.musicButton != null && this.musicButton != musicButton){
            disposeMusic();
			resetMusicButtonState();
        }
        this.musicButton = musicButton;
        Music m = getMusic(EncyclopediaAssets.SFX_PATH + music + MUSIC_EXTENSION);
        m.setOnCompletionListener(new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music music) {
                resetMusicButtonState();
            }
        });
        m.play();
        if (m.isPlaying()) Log.debug(getClass(), "Playing");
        playedMusic = m;
    }

    public void togglePlaying() {
        if (playedMusic != null) {
            playedMusic.pause();
        }
    }

    private void resetMusicButtonState() {
        if (musicButton != null) musicButton.setChecked(false);
    }

    public void disposeMusic() {
        Music introMusic = getIntroMusic();
        if (introMusic != null && introMusic.isPlaying()) {
            MusicUtil.fadeOut(introMusic, MUSIC_FADE_OUT_DURATION);
        }

        if (playedMusic != null ) {
            if (playedMusic.isPlaying()) {
                MusicUtil.fadeOut(playedMusic,MUSIC_FADE_OUT_SHORT_DURATION);
            }else {
                playedMusic.dispose();
            }
            resetMusicButtonState();
            playedMusic = null;
        }
    }

    public void updateContentSize() {
        List<Content> contentList = ContentParser.getContentList();
        for (Content c : contentList) {
            c.refreshContent(this);
        }
        content.pack();
    }

    private void updateMenuScrollPosition(int index){
        int thirdPart = MenuItem.values().length/3;
        float position;
        if (index< thirdPart){
            position = 0f;
        }else if (index< thirdPart *2){
            position = 0.5f;
        }else {
            position = 1f;
        }
        menuScrollPane.setScrollPercentY(position);
    }

    public static EncyclopediaFont getActualRegularFont() {
        return actualRegularFont;
    }
    public static EncyclopediaFont getActualBoldFont() {
        return actualBoldFont;
    }

    public static EncyclopediaFont getHeaderActualFont() {
        return actualHeaderFont;
    }

    public void decreaseFontSize() {
        actualRegularFont = actualRegularFont.getSmaller();
        actualBoldFont = actualBoldFont.getSmaller();
        actualHeaderFont = actualHeaderFont.getSmaller();
    }

    public void increaseFontSize() {
        actualRegularFont = actualRegularFont.getLarger();
        actualBoldFont = actualBoldFont.getLarger();
        actualHeaderFont = actualHeaderFont.getLarger();
    }

	private class ResizeFontListener extends ClickListener {

        @Override
        public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            if (event.getListenerActor().getUserObject().equals(BUTTON_PLUS)) {
                increaseFontSize();
            } else {
                decreaseFontSize();
            }
            updateContentSize();
        }

    }

    @Override
    protected void screenDisposed() {
        if (playedMusic != null) {
            playedMusic.dispose();

            playedMusic = null;
        }
        super.screenDisposed();
    }

    @Handler
    private void handleMuteSound(TablexiaSettings.SoundMuteEvent event) {
        if(event.isSoundMuted()) {
            disposeMusic();
            resetMusicButtonState();
        }
    }
}
