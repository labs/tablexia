/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia;

import cz.nic.tablexia.loader.application.ApplicationFontManager;

/**
 * Created by vitaliy on 23.12.15.
 */
public enum EncyclopediaFont {
    SMALL_REGULAR(ApplicationFontManager.FontType.ENCYCLOPEDIA_REGULAR_18),
    DEFAULT_REGULAR(ApplicationFontManager.FontType.ENCYCLOPEDIA_REGULAR_20),
    LARGE_REGULAR(ApplicationFontManager.FontType.ENCYCLOPEDIA_REGULAR_26),

    SMALL_BOLD(ApplicationFontManager.FontType.ENCYCLOPEDIA_BOLD_18),
    DEFAULT_BOLD(ApplicationFontManager.FontType.ENCYCLOPEDIA_BOLD_20),
    LARGE_BOLD(ApplicationFontManager.FontType.ENCYCLOPEDIA_BOLD_26),

    SMALL_HEADER(ApplicationFontManager.FontType.ENCYCLOPEDIA_BOLD_20),
    DEFAULT_HEADER(ApplicationFontManager.FontType.ENCYCLOPEDIA_BOLD_26),
    LARGE_HEADER(ApplicationFontManager.FontType.ENCYCLOPEDIA_BOLD_30);

    private ApplicationFontManager.FontType fontType;

    EncyclopediaFont(ApplicationFontManager.FontType fontType) {
        this.fontType = fontType;
    }

    public ApplicationFontManager.FontType getFontType() {
        return fontType;
    }

    public EncyclopediaFont getSmaller() {
        int index = ordinal();
        if (index!=0&&index!=3 && index!=6){
            index--;
        }
        return values()[index];
    }

    public EncyclopediaFont getLarger(){
        int index = ordinal();
        if (index!=2&&index!=5&&index!=8){
            index++;
        }
        return values()[index];
    }
}
