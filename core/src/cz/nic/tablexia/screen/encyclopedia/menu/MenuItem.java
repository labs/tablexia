/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.menu;

/**
 * Created by Václav Tarantík on 24.8.15.
 */
public enum MenuItem {
    ABOUT_ENCYCLOPEDIA("introduction","introduction"),
    SPECIFIC_LEARNING_DISABILITIES("specific_learning_disabilities","specific_learning_disabilities"),
    DYSGRAPHIA("dysgraphia","dysgraphia"),
    DYSLEXIA("dyslexia","dyslexia"),
    DYSORTHOGRAPHIA("dysorthographia","dysorthographia"),
    MEMORY("memory","memory"),
    SPATIAL_ORIENTATION("spatial_orientation","spatial_orientation"),
    SERIALITY("seriality","seriality"),
    AUDITORY_DISTINCTION("auditory_distinction","auditory_distinction"),
    VISUAL_DISTINCTION("visual_distinction","visual_distinction"),
    VERBAL_SKILLS("verbal_skills","verbal_skills"),
    FAMOUS_DYSLECTICS("famous_dyslectics","famous_dyslectics");

    private String title;
    private String resourcePage;

    MenuItem(String title, String resourcePage){
        this.title = title;
        this.resourcePage = resourcePage;
    }

    public String getResourcePageName() {
        return resourcePage;
    }

    public String getTitle() {
        return title;
    }
}
