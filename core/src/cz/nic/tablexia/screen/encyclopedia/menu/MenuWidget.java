/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.menu;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;
import java.util.Map;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

/**
 * Created by Václav Tarantík on 26.8.15.
 */
public class MenuWidget extends VerticalGroup {
    private static final float BUTTON_TOP_BOTTOM_PADDING    =   20f;
    private static final float  SCROLL_PANE_SIZE            =   15f;

    Array<MenuItem> menuItemValues;
    private Map<MenuItem, TablexiaButton> menuItems;
    private NinePatchDrawable       selectionDrawable;
    private AbstractTablexiaScreen  screen;
    private OnMenuItemClickListener onItemClickListener;
    private int                     selectedItemIndex = -1;

    public MenuWidget(AbstractTablexiaScreen screen, Point size, String selectionDrawableResource, OnMenuItemClickListener onItemClickListener) {
        menuItemValues = new Array<MenuItem>(MenuItem.values());
        menuItems = new HashMap<MenuItem, TablexiaButton>();
        this.screen = screen;
        selectionDrawable = new NinePatchDrawable(new NinePatch(screen.getScreenPatch(selectionDrawableResource)));
        selectionDrawable.setMinWidth(size.x);
        this.onItemClickListener = onItemClickListener;
        initItems();
    }

    private void initItems() {
        for (final MenuItem item : menuItemValues) {
            final TablexiaButton buttonMenuItem = new TablexiaButton(
                    screen.getText(item.getTitle()),
                    true,
                    new NinePatch(new Texture(new Pixmap(4, 4, Pixmap.Format.RGBA4444)), 0 ,0, 0, 0),
                    selectionDrawable.getPatch(),
                    null,
                    null
            );
            buttonMenuItem.adaptiveSize(false);
            buttonMenuItem.setChecked(false);
            buttonMenuItem.onlyOnClick(true);
            buttonMenuItem.getLabel().setAlignment(Align.left);
            buttonMenuItem.getLabel().setWrap(true);
            buttonMenuItem.removeListener();
            buttonMenuItem.setName(item.getTitle());
            buttonMenuItem.setInputListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    selectItem(item.ordinal());
                }
            });
            menuItems.put(item, buttonMenuItem);
            addActor(buttonMenuItem);
        }
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);
    }

    public void updateSize(float width) {
        super.setWidth(width);

        for (final MenuItem item : menuItemValues) {
            TablexiaButton btn = menuItems.get(item);

            if(btn == null) continue;

            btn.setWidth(width-SCROLL_PANE_SIZE);
            btn.setHeight(btn.getLabel().getTablexiaLabelStyle().getFontType().getSize() + 2 * BUTTON_TOP_BOTTOM_PADDING);
        }
    }

    public void selectItem(int position) {
        MenuItem searchedItem = menuItemValues.get(position);

        //Reset all items
        for(TablexiaButton currMenuItem : menuItems.values()) {
            currMenuItem.setUnchecked();
        }

        //Choose and highlight the one selected
        TablexiaButton menuItem = menuItems.get(searchedItem);
        if(menuItem != null) {
            menuItem.setChecked(true);

            if(selectedItemIndex != position) {
                onItemClickListener.onMenuItemClicked(menuItemValues.get(position));
                selectedItemIndex = position;
            }
        }
    }

    public interface OnMenuItemClickListener {
        void onMenuItemClicked(MenuItem clickedMenuItem);
    }

    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    //Gets for testing

    public int getMenuItemCount(){
        return menuItems.size();
    }

    public String getMenuItemTitle(int index){
        return menuItemValues.get(index).getTitle();
    }

    public Array<MenuItem> getMenuItemValues() {
        return menuItemValues;
    }
}
