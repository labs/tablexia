/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.assets;

/**
 * Created by Václav Tarantík on 25.8.15.
 */
public class EncyclopediaAssets {
    public static final String GFX_PATH = "gfx/";
    public static final String SFX_PATH = "sfx/";

    public static final String BOOKMARK = GFX_PATH + "bookmark";
    public static final String HINT_BACKGROUND = GFX_PATH + "hint_background";
	public static final String PAUSE_ICON = GFX_PATH + "pause_icon";
	public static final String PLAY_ICON = GFX_PATH + "play_icon";

    //TEXT
    public static final String PLAY = "play";
    public static final String PAUSE = "pause";
    public static final String HINT_TEXT_TITLE = "hint_header";

}

