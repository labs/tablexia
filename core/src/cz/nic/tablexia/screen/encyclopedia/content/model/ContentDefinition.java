/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.content.model;

import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;

import cz.nic.tablexia.util.Log;

/**
 * Created by Matyáš Latner on 16.12.15.
 */
public enum ContentDefinition {

    HEADER          (Header.class),
    TEXT_CONTENT    (TextContent.class),
    IMAGE_CONTENT   (ImageContent.class),
    HINT            (Hint.class);

    private Class<? extends Content>    clazz;
    private final String                className;

    ContentDefinition(Class<? extends Content> clazz) {
        this.clazz  = clazz;
        className = clazz.getName();
    }

    public String getClassName() {
        return className;
    }

    public Content getNewInstance(JsonValue jsonValue) {
        try {
            return (Content) ClassReflection.getConstructor(clazz, JsonValue.class).newInstance(jsonValue);
        } catch (ReflectionException e) {
            Log.err(getClass(), "Cannot create new instance of encyclopedia container: " + className, e);
        }
        return null;
    }

    public static ContentDefinition getContentDefinitionForClassName(String className) {
        if (className != null && className.length() > 0) {
            for (ContentDefinition contentDefinition: ContentDefinition.values()) {
                if (contentDefinition.className.equals(className)) {
                    return contentDefinition;
                }
            }
        }
        return null;
    }
}
