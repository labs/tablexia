/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.content.model;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.JsonValue;

import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by Václav Tarantík on 3.9.15.
 */
public abstract class Content {

    public static final int PAD_RIGHT = 20;
    public static final int PAD_BOTTOM = 10;

    abstract Actor render(AbstractTablexiaScreen screen,float parentContainerWidth);
    abstract void update(AbstractTablexiaScreen screen);

    public Cell addToCell(Table contentTable,AbstractTablexiaScreen screen, float parentContainerWidth){
        return contentTable.add(render(screen, parentContainerWidth)).expandX().fillX().padRight(PAD_RIGHT).padBottom(PAD_BOTTOM).left();
    }

    protected void checkJsonContent(JsonValue jsonValue, String contentKey) {
        if (!jsonValue.has(contentKey)) {
            throw new RuntimeException("Json \"" + contentKey + "\" key not found in " + getClass().getName());
        }
    }

    public void refreshContent(AbstractTablexiaScreen screen){
            this.update(screen);
    }
}
