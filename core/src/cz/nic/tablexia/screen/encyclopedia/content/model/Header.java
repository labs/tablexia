/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.content.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.JsonValue;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.main.sound.VolumeBar;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaFont;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaScreen;
import cz.nic.tablexia.screen.encyclopedia.assets.EncyclopediaAssets;
import cz.nic.tablexia.screen.encyclopedia.content.actors.PlaybackStateButton;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;

/**
 * Created by Václav Tarantík on 23.9.15.
 */
public class Header extends Content {

    private static final String             JSON_SOUND_KEY              = "soundResource";
    private static final String             JSON_TEXT_CONTENT_KEY       = "textContent";
    private static final ContentDefinition  TEXT_CONTENT_DEFINITION     = ContentDefinition.TEXT_CONTENT;
    private static final float              BUTTON_ADAPTIVE_SIZE_RATIO  = 1.2f;
    private static final Color              DEFAULT_FONT_COLOR          = TablexiaSettings.getDefaultFontColor();
    private static final int                PAD_RIGHT                   = 25;
    private static final int                PAD_TOP                     = 20;
    private static final int                PAD_BOTTOM                  = 25;
    private static final int                BUTTON_WIDTH                = 180;

    private String        soundResource;
    private TextContent   textContent;
    private Table         headerTable;
    private TablexiaLabel headerLabel;

    public Header(JsonValue jsonValue) {
        soundResource = jsonValue.getString(JSON_SOUND_KEY);
         checkJsonContent(jsonValue, JSON_TEXT_CONTENT_KEY);
        textContent = (TextContent) TEXT_CONTENT_DEFINITION.getNewInstance(jsonValue.get(JSON_TEXT_CONTENT_KEY).get(TEXT_CONTENT_DEFINITION.getClassName()));
    }

    @Override
    public Actor render(final AbstractTablexiaScreen screen, float parentContainerWidth) {
        return createHeaderTable(screen, parentContainerWidth);
    }

    @Override
    void update(AbstractTablexiaScreen screen) {
        EncyclopediaFont fontType = ((EncyclopediaScreen)screen).getHeaderActualFont();
        headerLabel.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(fontType.getFontType(), DEFAULT_FONT_COLOR));
    }

    private Table createHeaderTable(final AbstractTablexiaScreen screen, float parentContainerWidth) {
        headerTable = new Table();
        headerTable.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
        EncyclopediaFont fontType = ((EncyclopediaScreen)screen).getHeaderActualFont();
        headerLabel = (TablexiaLabel) textContent.render(screen, parentContainerWidth);
        headerLabel.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(fontType.getFontType(), DEFAULT_FONT_COLOR));
        headerLabel.setWrap(true);

        if (soundResource != null) {
            final EncyclopediaScreen encyclopediaScreen = ((EncyclopediaScreen) (screen));
            final PlaybackStateButton button = new PlaybackStateButton(screen.getText(EncyclopediaAssets.PLAY), 
                                                                            screen.getText(EncyclopediaAssets.PAUSE), 
                                                                            new Image(screen.getScreenTextureRegion(EncyclopediaAssets.PLAY_ICON)),
                                                                            new Image(screen.getScreenTextureRegion(EncyclopediaAssets.PAUSE_ICON)));
					button.adaptiveSizeRatio(BUTTON_ADAPTIVE_SIZE_RATIO).checkable(true);
                    button.setInputListener(new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            Runnable playButtonAction = new Runnable() {
                                @Override
                                public void run() {
                                    if (button.isChecked()) {
                                        encyclopediaScreen.playMusic(soundResource, button);
                                    } else {
                                        encyclopediaScreen.togglePlaying();
                                    }
                                }
                            };

                            if(TablexiaSettings.getInstance().isSoundMuted() && button.isChecked()) {
                                showSoundMutedDialog(playButtonAction, button);
                            } else {
                                playButtonAction.run();
                            }
                        }
                    });
            headerTable.add(headerLabel).maxWidth(parentContainerWidth - BUTTON_WIDTH - PAD_RIGHT).fillX().expandX().left();
            headerTable.add(button).right().padRight(PAD_RIGHT);
        } else {
            headerTable.add(headerLabel).expandX().fillX().left();
        }
        return headerTable;
    }

    private void showSoundMutedDialog(final Runnable onSuccessCallback, final PlaybackStateButton button) {
        TablexiaComponentDialogFactory.getInstance().createWarningYesNoDialog(
                ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SOUND_MUTED_QUESTION),
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        TablexiaSettings.getInstance().toggleSoundMute();
                        ApplicationBus.getInstance().publishAsync(new VolumeBar.UnmuteEvent());
                        onSuccessCallback.run();
                    }
                },
                new ClickListener(){
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        button.setChecked(false);
                    }
                },
                true, true
        ).show(TablexiaComponentDialogFactory.WARNING_DIALOG_WIDTH, TablexiaComponentDialogFactory.WARNING_DIALOG_HEIGHT);
    }

    @Override
    public Cell addToCell(Table contentTable, AbstractTablexiaScreen screen, float parentContainerWidth) {
        return  contentTable.add(render(screen, parentContainerWidth)).expandX().fillX().padBottom(PAD_BOTTOM).padTop(PAD_TOP).left();
    }

    public void setSoundResource(String soundResource) {
        this.soundResource = soundResource;
    }

    public void setTextContent(TextContent textContent) {
        this.textContent = textContent;
    }
}
