/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.content.actors;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.util.ui.button.ImageTablexiaButton;

public class PlaybackStateButton extends ImageTablexiaButton{
    
    private Image  playIcon;
    private Image  pausedIcon;
    private String playText;
    private String pausedText;
    
    public PlaybackStateButton(String playText, String pausedText, Image playIcon, Image pausedIcon) {
        super(playText, playIcon);
        this.playIcon = playIcon;
        this.pausedIcon = pausedIcon;
        this.playText = playText;
        this.pausedText = pausedText;
    }
    
    @Override
    public void setChecked(boolean checked) {
        setIcon(checked ? pausedIcon : playIcon);
        text(checked ? pausedText : playText);
        super.setChecked(checked);
    }

}
