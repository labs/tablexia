/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.content.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.JsonValue;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaFont;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaScreen;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by Václav Tarantík on 3.9.15.
 */
public class TextContent extends Content {

    private static final String JSON_TEXT_KEY      = "text";
    private static final String JSON_BOLD_KEY      = "bold";
    private static final Color  DEFAULT_FONT_COLOR = TablexiaSettings.getDefaultFontColor();

    private String  text;
    private boolean bold;
    private TablexiaLabel paragraph;

    public TextContent(JsonValue jsonValue) {
        checkJsonContent(jsonValue, JSON_TEXT_KEY);
        text = jsonValue.getString(JSON_TEXT_KEY);
        bold = jsonValue.has(JSON_BOLD_KEY) ? jsonValue.getBoolean(JSON_BOLD_KEY) : false;
    }

    @Override
    public Actor render(AbstractTablexiaScreen screen, float parentContainerWidth) {
        EncyclopediaFont fontType;
        if (isBold()){
            fontType = ((EncyclopediaScreen)screen).getActualBoldFont();
        }else{
            fontType = ((EncyclopediaScreen)screen).getActualRegularFont();
        }
        return createParagraph(fontType,screen.getText(getText()));
    }

    @Override
    void update(AbstractTablexiaScreen screen) {
        EncyclopediaFont fontType;
        if (isBold()){
            fontType = ((EncyclopediaScreen)screen).getActualBoldFont();
        }else{
            fontType = ((EncyclopediaScreen)screen).getActualRegularFont();
        }
        paragraph.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(fontType.getFontType(), DEFAULT_FONT_COLOR));
    }

    private TablexiaLabel createParagraph(EncyclopediaFont fontType, String text){
        TablexiaLabel.TablexiaLabelStyle labelStyle = new TablexiaLabel.TablexiaLabelStyle(fontType.getFontType(), DEFAULT_FONT_COLOR);
        paragraph = new TablexiaLabel(text, labelStyle);
        paragraph.setWrap(true);

        return paragraph;
    }

    public String getText() {
        return text;
    }

    public boolean isBold() {
        return bold;
    }
}
