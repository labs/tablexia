/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.content.model;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.JsonValue;

import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.encyclopedia.assets.EncyclopediaAssets;

/**
 * Created by Václav Tarantík on 3.9.15.
 */
public class ImageContent extends Content {

    private static final String JSON_IMAGE_KEY      = "image";
    private static final float  MAX_IMAGE_HEIGHT    = 300;

    private String image;

    public ImageContent(JsonValue jsonValue){
        checkJsonContent(jsonValue, JSON_IMAGE_KEY);
        image = jsonValue.getString(JSON_IMAGE_KEY);
    }

    @Override
    public Actor render(AbstractTablexiaScreen screen,float parentContainerWidth) {
        return new Image(screen.getScreenTextureRegion(EncyclopediaAssets.GFX_PATH + image));
    }

    @Override
    void update(AbstractTablexiaScreen screen) {

    }

    @Override
    public Cell addToCell(Table contentTable, AbstractTablexiaScreen screen, float parentContainerWidth) {
        Actor a = render(screen, parentContainerWidth);
        float actorHeight = a.getHeight();
        float actorWidth = a.getWidth();
        float actorSizeRatio = actorWidth/actorHeight;
        if(actorHeight > MAX_IMAGE_HEIGHT) {
            actorHeight = MAX_IMAGE_HEIGHT;
            actorWidth = actorSizeRatio*actorHeight;
        }
        return super.addToCell(contentTable, screen, parentContainerWidth).width(actorWidth).height(actorHeight).align(Align.center);
    }

    public String getImage() {
        return image;
    }
}
