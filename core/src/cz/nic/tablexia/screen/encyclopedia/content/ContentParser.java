/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.content;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.screen.encyclopedia.content.model.Content;
import cz.nic.tablexia.screen.encyclopedia.content.model.ContentDefinition;

/**
 * Created by Václav Tarantík on 2.9.15.
 */
public class ContentParser {

     private static List<Content>  contentList;

    public static List<Content> parseEncyclopediaPageContent(String jsonLine) {
        contentList = new ArrayList<Content>();

        JsonReader jsonReader = new JsonReader();
        JsonValue jsonValue = jsonReader.parse(jsonLine);

        for (int i = 0; i < jsonValue.size; i++) {
            JsonValue rootObject = jsonValue.get(i);
            if (rootObject != null) {
                for (int j = 0; j < rootObject.size; j++) {
                    JsonValue componentObject = rootObject.get(j);
                    if (componentObject.name() != null && componentObject.name().length() > 0) {
                        ContentDefinition contentDefinition = ContentDefinition.getContentDefinitionForClassName(componentObject.name());
                        if (contentDefinition != null) {
                            contentList.add(contentDefinition.getNewInstance(componentObject));
                        }
                    }
                }
            }
        }

        return contentList;
    }

    public static List<Content> getContentList(){
        return contentList;
    }


}
