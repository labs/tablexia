/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.screen.encyclopedia.content.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.JsonValue;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaFont;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaScreen;
import cz.nic.tablexia.screen.encyclopedia.assets.EncyclopediaAssets;
import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by Václav Tarantík on 3.9.15.
 */

public class Hint extends Content {

    private static final ContentDefinition IMAGE_CONTENT_DEFINITION     = ContentDefinition.IMAGE_CONTENT;
    private static final ContentDefinition TEXT_CONTENT_DEFINITION      = ContentDefinition.TEXT_CONTENT;
    private static final Color             DEFAULT_FONT_COLOR           = TablexiaSettings.getDefaultFontColor();
    private static final String            JSON_TEXT_CONTENT_KEY        = "textContent";
    private static final String            JSON_IMAGE_CONTENT_KEY       = "imageContent";
    private static final float             IMAGE_RELATIVE_WIDTH_PORTION = 0.3f;
    private static final float             HINT_MAX_HEIGHT              = 200;
    private static final int               PAD_LEFT                     = 10;
    private static final int               IMAGE_PAD_LEFT               = 20;
    private static final int               PAD_TOP                      = 25;


    private ImageContent  imageContent;
    private TextContent   textContent;
    private Table         hintTable;
    private Image         hintImage;
    private TablexiaLabel hintLabel;
    private TablexiaLabel hintTitle;

    public Hint(JsonValue jsonValue) {
        checkJsonContent(jsonValue, JSON_TEXT_CONTENT_KEY);
        textContent = (TextContent) TEXT_CONTENT_DEFINITION.getNewInstance(jsonValue.get(JSON_TEXT_CONTENT_KEY).get(TEXT_CONTENT_DEFINITION.getClassName()));
        checkJsonContent(jsonValue, JSON_IMAGE_CONTENT_KEY);
        imageContent = (ImageContent) IMAGE_CONTENT_DEFINITION.getNewInstance(jsonValue.get(JSON_IMAGE_CONTENT_KEY).get(IMAGE_CONTENT_DEFINITION.getClassName()));
    }

    public Hint(ImageContent imageContent, TextContent textContent) {
        this.imageContent = imageContent;
        this.textContent = textContent;
    }

    public ImageContent getImageContent() {
        return imageContent;
    }

    public void setImageContent(ImageContent imageContent) {
        this.imageContent = imageContent;
    }

    public TextContent getTextContent() {
        return textContent;
    }

    public void setTextContent(TextContent textContent) {
        this.textContent = textContent;
    }

    @Override
    public Actor render(AbstractTablexiaScreen screen, float parentContainerWidth) {
        return createHintTable(screen, parentContainerWidth);
    }

    @Override
    void update(AbstractTablexiaScreen screen) {
        EncyclopediaFont labelFontType = ((EncyclopediaScreen) screen).getActualRegularFont();
        EncyclopediaFont titleFontType = ((EncyclopediaScreen) screen).getActualBoldFont();
        TablexiaLabel.TablexiaLabelStyle labelHintStyle = new TablexiaLabel.TablexiaLabelStyle(labelFontType.getFontType(), DEFAULT_FONT_COLOR);
        TablexiaLabel.TablexiaLabelStyle titleHintStyle = new TablexiaLabel.TablexiaLabelStyle(titleFontType.getFontType(), DEFAULT_FONT_COLOR);
        hintLabel.setTablexiaLabelStyle(labelHintStyle);
        hintTitle.setTablexiaLabelStyle(titleHintStyle);
    }

    private Table createHintTable(AbstractTablexiaScreen screen, float parentContainerWidth) {
        EncyclopediaFont fontType = ((EncyclopediaScreen)screen).getActualBoldFont();
        TablexiaLabel.TablexiaLabelStyle titleLabelStyle = new TablexiaLabel.TablexiaLabelStyle(fontType.getFontType(), DEFAULT_FONT_COLOR);
        hintTable = new Table();
        hintTable.setWidth(parentContainerWidth);
        hintTable.setBackground(new NinePatchDrawable(new NinePatch(screen.getScreenPatch(EncyclopediaAssets.HINT_BACKGROUND))));
        Table textsTable = new Table();

        hintImage = (Image) imageContent.render(screen, parentContainerWidth);
        hintTitle = new TablexiaLabel(screen.getText(EncyclopediaAssets.HINT_TEXT_TITLE), titleLabelStyle);
        hintLabel = (TablexiaLabel) textContent.render(screen, parentContainerWidth);

        Point imageOriginalSize = new Point(hintImage.getDrawable().getMinWidth(), hintImage.getDrawable().getMinHeight());
        float desiredImageWidth = parentContainerWidth * IMAGE_RELATIVE_WIDTH_PORTION;
        float desiredImageHeight = (desiredImageWidth / imageOriginalSize.x) * imageOriginalSize.y;
        if (desiredImageHeight > HINT_MAX_HEIGHT) {
            desiredImageHeight = HINT_MAX_HEIGHT;
            desiredImageWidth = (imageOriginalSize.x / imageOriginalSize.y) * desiredImageHeight;
        }
        hintImage.setSize(desiredImageWidth, desiredImageHeight);

        textsTable.add(hintTitle).align(Align.left).padLeft(PAD_LEFT);
        textsTable.row();
        textsTable.add(hintLabel).fillX().expandX().padLeft(PAD_LEFT);

        hintTable.add(hintImage).width(desiredImageWidth).height(desiredImageHeight).padLeft(IMAGE_PAD_LEFT);
        hintTable.add(textsTable).fillX().expandX();
        return hintTable;
    }

    @Override
    public Cell addToCell(Table contentTable, AbstractTablexiaScreen screen, float parentContainerWidth) {
        return super.addToCell(contentTable, screen, parentContainerWidth).padTop(PAD_TOP);
    }
}
