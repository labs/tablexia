/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.sync;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.badlogic.gdx.net.HttpRequestHeader;
import com.badlogic.gdx.utils.JsonWriter;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.atomic.AtomicLong;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaBuildConfig;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.shared.rest.UserRestPath;
import cz.nic.tablexia.shared.security.SecurityHelper;
import cz.nic.tablexia.shared.security.SecurityRestHeader;
import cz.nic.tablexia.shared.security.SecurityRestPath;
import cz.nic.tablexia.sync.work.SyncWork;
import cz.nic.tablexia.util.Log;

/**
 * Created by frantisek on 13.1.16.
 */
public class RestSynchronizationService {

    // request timeout in milliseconds
    private static final int REQUEST_TIMEOUT = 10000;

    public enum SynchronizationType {
        FULL,
        INCREMENT,
    }

    private static final String PROTOCOL_DIVIDER    = "://";
    private static final String PORT_DIVIDER        = ":";

    private static final String FALLBACK_REST_PROTOCOL  = "http";
    private static final String FALLBACK_REST_HOST      = "localhost";
    private static final int    FALLBACK_REST_PORT      = 8080;
    private static final String SECRET_KEY              = TablexiaBuildConfig.TABLEXIA_SERVER_SECRET;

    private static final String REST_ADDRESS =
            (TablexiaBuildConfig.TABLEXIA_SERVER_PROTOCOL != null ? TablexiaBuildConfig.TABLEXIA_SERVER_PROTOCOL : FALLBACK_REST_PROTOCOL) +
            PROTOCOL_DIVIDER +
            (TablexiaBuildConfig.TABLEXIA_SERVER_HOST != null ? TablexiaBuildConfig.TABLEXIA_SERVER_HOST : FALLBACK_REST_HOST) +
            PORT_DIVIDER +
            (TablexiaBuildConfig.TABLEXIA_SERVER_PORT != null ? TablexiaBuildConfig.TABLEXIA_SERVER_PORT : FALLBACK_REST_PORT);

    public static AtomicLong secretTime;

    private HttpRequestBuilder builder;
    private Net.HttpResponseListener listener;

    public RestSynchronizationService(String url, String httpMethod, Net.HttpResponseListener listener) {
        builder = new HttpRequestBuilder();
        builder.newRequest();
        HttpRequestBuilder.json.setOutputType(JsonWriter.OutputType.json);
		builder.header(HttpRequestHeader.ContentType, "application/json");
        builder.method(httpMethod);
        builder.url(url);
        builder.timeout(REQUEST_TIMEOUT);
        this.listener = listener;
    }

    public void setJsonObject(Object obj) {
        builder.jsonContent(obj);
    }

    public void send() {
        if (secretTime != null) {
            setSecurityHeaders();
        }
        Net.HttpRequest request = builder.build();
        Log.debug(this.getClass(), "Calling remote url: " + request.getUrl());
        Tablexia.getNet().sendHttpRequest(request, listener);
    }

    public void setHeader(String key, String value) {
        builder.header(key, value);
    }

    public void setSecurityHeaders() {
        try {
            setHeader(SecurityRestHeader.HEADER_AUTH_KEY, SecurityHelper.getSecretHash(SECRET_KEY, secretTime.get()));
            setHeader(SecurityRestHeader.HEADER_AUTH_TIME, secretTime.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.err(RestSynchronizationService.class, "failed to set security headers", e);
        } catch (UnsupportedEncodingException e) {
            Log.err(RestSynchronizationService.class, "failed to set security headers", e);
        }
    }

    public static synchronized void doSyncWork(SyncWork work) {
        if (Tablexia.getConnectionManager().isUsingMobileData()) {
            Log.info(RestSynchronizationService.class, "Using mobile data connection - synchronization disabled");
            return;
        }
        if(!TablexiaSettings.getInstance().getBuildType().isSyncWithDTB()){
            Log.info(RestSynchronizationService.class, "Synchronization for actual build type disabled.");
            return;
        }
        work.process(new RestSynchronizationService(work.getUrl(), work.getHttpMethod(), work));
    }

    public static String getCreateUserUrl() {
        return REST_ADDRESS + UserRestPath.getCreateUserUrl();
    }

    public static String getConfirmUserUrl(String uuid) {
        return REST_ADDRESS + UserRestPath.getConfirmUserUrl(uuid);
    }

    public static String getDownloadUserUrl(String uuid) {
        return REST_ADDRESS + UserRestPath.getDownloadUserUrl(uuid);
    }

    public static String getDeleteUserUrl(String uuid) {
        return REST_ADDRESS + UserRestPath.getDeleteUserUrl(uuid);
    }

    public static String getUserAvatarUploadUrl(String uuid) {
        return REST_ADDRESS + UserRestPath.getUserAvatarUploadUrl(uuid);
    }

    public static String getUserAvatarDownloadUrl(String uuid) {
        return REST_ADDRESS + UserRestPath.getUserAvatarDownloadUrl(uuid);
    }

    public static String getUserAvatarActualUrl(String uuid) {
        return REST_ADDRESS + UserRestPath.getUserAvatarActualUrl(uuid);
    }

    public static String getSecurityGetUrl() {
        return REST_ADDRESS + SecurityRestPath.SECURITY_PATH + SecurityRestPath.SECURITY_GET;
    }
}
