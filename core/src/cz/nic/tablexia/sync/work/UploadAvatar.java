/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.sync.work;

import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpStatus;
import com.badlogic.gdx.utils.Json;

import cz.nic.tablexia.menu.user.UserAvatarDefinition;
import cz.nic.tablexia.model.CustomAvatarDAO;
import cz.nic.tablexia.shared.model.CustomAvatarDataPacket;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.util.Log;

public class UploadAvatar extends SyncWork {

    public UploadAvatar(User user) {
        setUser(user);
    }

    @Override
    public String getUrl() {
        return RestSynchronizationService.getUserAvatarUploadUrl(getUser().getUuid());
    }

    @Override
    public String getHttpMethod() {
        return Net.HttpMethods.POST;
    }

    @Override
    protected void send(RestSynchronizationService sync) {
        if(!UserAvatarDefinition.isUserUsingCustomAvatar(getUser())) {
            Log.info(getClass(), "User " + getUser() + " is not using custom avatar");
            return;
        }

        byte[] avatarData = CustomAvatarDAO.getCustomAvatarForID(getUser().getId());
        if(avatarData == null || avatarData.length == 0) return;

        CustomAvatarDataPacket customAvatarDataPacket = new CustomAvatarDataPacket(avatarData, null);

        sync.setJsonObject(customAvatarDataPacket);
        sync.send();
    }

    @Override
    protected void onSuccessfulResponse(Net.HttpResponse httpResponse) {
        long start = System.currentTimeMillis();

        Log.info(getClass(), "Avatar for user: " + getUser() + " has been uploaded to the server!");
        String syncAtLongJson = httpResponse.getResultAsString();

        Json json = new Json();
        json.setIgnoreUnknownFields(true);

        final Long syncAt = json.fromJson(Long.class, syncAtLongJson);

        if(syncAt != null) {
            if(CustomAvatarDAO.markAvatarAsSync(getUser().getId(), syncAt)) {
                Log.info(getClass(), "Avatar for user: " + getUser() + " has been marked as synchronized!");
            } else {
                Log.err(getClass(), "Unknown error when trying to mark avatar for user: " + getUser() + " as synchronized!");
            }
        }

        long finish = System.currentTimeMillis();

        Log.debug(this.getClass(), "Duration: " + getTime(start, finish));
    }

    @Override
    protected int getExpectedSuccessResponseCode() {
        return HttpStatus.SC_OK;
    }
}
