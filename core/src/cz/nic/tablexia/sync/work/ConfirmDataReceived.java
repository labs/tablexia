/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.sync.work;

import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestHeader;
import com.badlogic.gdx.net.HttpStatus;

import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.model.screen.ScreenDAO;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.util.Log;

/**
 * Created by frantisek on 15.1.16.
 */
public class ConfirmDataReceived extends SyncWork {

    public ConfirmDataReceived(User user) {
        setUser(user);
    }

    @Override
    public String getUrl() {
        return RestSynchronizationService.getConfirmUserUrl(getUser().getUuid());
    }

    @Override
    public void send(RestSynchronizationService sync) {
        sync.setHeader(HttpRequestHeader.ContentType, "application/json");
        sync.send();
    }

    @Override
    protected void onSuccessfulResponse(Net.HttpResponse httpResponse) {
        GameDAO.markGamesAsSync(getUser().getGames());
        ScreenDAO.markScreensAsSync(getUser().getScreens());
        Log.debug(this.getClass(), String.format("Marking %d of games as synchronized", getUser().getGames() != null ? getUser().getGames().size() : 0));
        RestSynchronizationService.doSyncWork(new DownloadUser(getUser().getUuid()));
    }

    @Override
    protected int getExpectedSuccessResponseCode() {
        return HttpStatus.SC_OK;
    }
}
