/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.sync.work;

import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpStatus;
import com.badlogic.gdx.utils.Json;

import cz.nic.tablexia.model.CustomAvatarDAO;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.util.Log;

/**
 * Takes care of whole custom avatar updating process
 * Uploads user avatar if the avatar hasnt been uploaded/synchronized yet.
 * Downloads Avatar if user needs it.
 * Checks whether or not the remote avatar is newer than the local one and downloads it if needed. (support for eventual avatar changing)
 */
public class UpdateAvatar extends SyncWork {
    public UpdateAvatar(User user) {
        setUser(user);
    }

    @Override
    public String getHttpMethod() {
        return Net.HttpMethods.POST;
    }

    @Override
    public String getUrl() {
        return RestSynchronizationService.getUserAvatarActualUrl(getUser().getUuid());
    }

    @Override
    protected void send(RestSynchronizationService sync) {
        try {
            Long syncAt = CustomAvatarDAO.getUserAvatarSyncAtTime(getUser().getId());

            if (syncAt == null) {
                RestSynchronizationService.doSyncWork(new UploadAvatar(getUser()));
            } else {
                //Sending syncAt time to server to find out, whether or not the avatar needs to be updated!
                sync.setJsonObject(syncAt);
                sync.send();
            }
        } catch (CustomAvatarDAO.CustomAvatarNotFoundException e) {
            RestSynchronizationService.doSyncWork(new DownloadAvatar(getUser()));
        }
    }

    @Override
    protected void onSuccessfulResponse(Net.HttpResponse httpResponse) {
        String jsonResultString = httpResponse.getResultAsString();

        Json json = new Json();
        json.setIgnoreUnknownFields(true);

        //Servers response. False means that avatar needs to be updated!
        final Boolean result = json.fromJson(Boolean.class, jsonResultString);

        if(result == null) {
            Log.err(getClass(), "Request for avatar's status failed. Server responded with " + result + "!");
            return;
        }

        if(!result) {
            RestSynchronizationService.doSyncWork(new DownloadAvatar(getUser()));
        }
    }

    @Override
    protected int getExpectedSuccessResponseCode() {
        return HttpStatus.SC_OK;
    }
}