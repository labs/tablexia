/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.sync.work;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestHeader;
import com.badlogic.gdx.net.HttpStatus;
import com.badlogic.gdx.utils.Json;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationAvatarManager;
import cz.nic.tablexia.menu.user.UserAvatarDefinition;
import cz.nic.tablexia.model.CustomAvatarDAO;
import cz.nic.tablexia.shared.model.CustomAvatarDataPacket;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.util.Log;

/**
 * Created by drahomir on 3/24/16.
 */
public class DownloadAvatar extends SyncWork{

    public DownloadAvatar(User user) {
        setUser(user);
    }

    @Override
    public String getUrl() {
        return RestSynchronizationService.getUserAvatarDownloadUrl(getUser().getUuid());
    }

    @Override
    protected void send(RestSynchronizationService sync) {
        //exit if users avatar hasnt been uploaded yet or users avatar already exists in avatars folder
        if(!UserAvatarDefinition.isUserUsingCustomAvatar(getUser())) {
            Log.info(getClass(), "User " + getUser() + " is not using custom avatar");
            return;
        }

        sync.setHeader(HttpRequestHeader.ContentType, "application/json");
        sync.send();
    }

    @Override
    protected void onSuccessfulResponse(Net.HttpResponse httpResponse) {
        long start = System.currentTimeMillis();

        String customAvatarPacketJson = httpResponse.getResultAsString();

        Json json = new Json();
        json.setIgnoreUnknownFields(true);

        final CustomAvatarDataPacket customAvatarDataPacket = json.fromJson(CustomAvatarDataPacket.class, customAvatarPacketJson);

        if(customAvatarDataPacket != null && customAvatarDataPacket.hasAvatarData() && customAvatarDataPacket.hasSyncAt()) {
            CustomAvatarDAO.insertCustomAvatar(getUser().getId(), customAvatarDataPacket.getAvatarData(), customAvatarDataPacket.getSyncAt());
        }

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                ApplicationAvatarManager.getInstance().addCustomAvatarToAtlas(getUser(), customAvatarDataPacket.getAvatarData());
                ApplicationBus.getInstance().post(new ApplicationAvatarManager.AvatarUpdatedEvent()).asynchronously();
            }
        });

        long finish = System.currentTimeMillis();

        Log.debug(this.getClass(), "Duration: " + getTime(start, finish));
    }

    @Override
    protected int getExpectedSuccessResponseCode() {
        return HttpStatus.SC_OK;
    }
}
