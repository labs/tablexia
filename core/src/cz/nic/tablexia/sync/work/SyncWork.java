/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.sync.work;

import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpStatus;
import com.badlogic.gdx.utils.Timer;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;

import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.util.Log;

/**
 * Created by frantisek on 15.1.16.
 */
public abstract class SyncWork implements Net.HttpResponseListener {

    private static final int MAX_RETRY_COUNT = 3;
    // delay in seconds
    private static final int RETRY_DELAY = 1;
    private int retryCount = 0;

    private RestSyncListener listener;
    private User user;

    // stores request that should be send after getting security time from server
    private static List<SyncWork> queuedRequests = new CopyOnWriteArrayList<SyncWork>();

    public String getHttpMethod() {
        return Net.HttpMethods.POST;
    }

    public void registerListener(RestSyncListener listener) {
        this.listener = listener;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public final void handleHttpResponse(Net.HttpResponse httpResponse) {
        // request timeout
        if (httpResponse.getStatus().getStatusCode() == -1) {
            failed(new Exception(String.format("request %s timeout", this.getUrl())));
            return;
        }

        // server response with unauthorized status code.
        // We need to get new security time, store all requests to queue and process them after we have security time from server
        if (httpResponse.getStatus().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
            Log.err(SyncWork.class, "Request not authorized");
            RestSynchronizationService.secretTime = null;
            queuedRequests.add(this);
            sendSecurityRequest();
            return;
        }

        if (httpResponse.getStatus().getStatusCode() != getExpectedSuccessResponseCode()) {
            Log.debug(getClass(), "Wrong Response Code received! " + httpResponse.getStatus().getStatusCode() + " (expected: " + getExpectedSuccessResponseCode() + ")");
            if(listener != null) listener.onWrongResponseCode(httpResponse.getStatus().getStatusCode());
            return;
        }

        onSuccessfulResponse(httpResponse);
        if (listener != null) {
            listener.onSuccess(getUser());
        }
    }

    // override this method to handle success response from server
    protected void onSuccessfulResponse(Net.HttpResponse httpResponse) {}

    @Override
    public void failed(Throwable t) {
        Log.err(this.getClass(), String.format("sync request failed: %s", t.getMessage()), t);
        // retry mechanism if request fails
        if (retryCount < MAX_RETRY_COUNT) {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    retryCount++;
                    Log.debug(SyncWork.this.getClass(), String.format("another try for request: %s", getUrl()));
                    send(new RestSynchronizationService(getUrl(), getHttpMethod(), SyncWork.this));
                }
            }, RETRY_DELAY);
        } else if (listener != null) {
            listener.onFailure(t);
        }
    }

    @Override
    public void cancelled() {
        Log.info(this.getClass(), "sync request cancelled");
        if (listener != null) {
            listener.onCancelled();
        }
    }

    /**
     * Helper method to print measured time
     *
     * @param start
     * @param finish
     * @return
     */
    protected String getTime(long start, long finish) {
        long result = finish - start;
        long minutes = result / (60 * 1000);
        float seconds = (result - minutes * 60 * 1000) / 1000.0f;
        return String.format("%d min %f sec", minutes, seconds);
    }

    public final void process(final RestSynchronizationService sync) {
        if (RestSynchronizationService.secretTime == null) {
            if (queuedRequests.isEmpty()) {
                queuedRequests.add(this);
                sendSecurityRequest();
            } else {
                queuedRequests.add(this);
            }
        } else {
            send(sync);
            processRequestQueue();
        }
    }

    private void sendSecurityRequest() {
        RestSynchronizationService securityRequest = new RestSynchronizationService(RestSynchronizationService.getSecurityGetUrl(), Net.HttpMethods.POST, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                String timeString = httpResponse.getResultAsString();
                if (timeString == null || timeString.isEmpty()) {
                    Log.err(SyncWork.class, "failed to receive security time");
                    return;
                }

                RestSynchronizationService.secretTime = new AtomicLong(Long.valueOf(timeString));
                processRequestQueue();
            }

            @Override
            public void failed(Throwable t) {
                Log.err(SyncWork.class, "Security request failed", t);
            }

            @Override
            public void cancelled() {
                Log.err(SyncWork.class, "Security request cancelled");
            }
        });
        securityRequest.send();
    }

    private void processRequestQueue() {
        if (queuedRequests.isEmpty()) {
            return;
        }

        Log.debug(this.getClass(), String.format("request queue size: %s", queuedRequests.size()));

        for (int i = 0; i < queuedRequests.size(); i++) {
            SyncWork sync = queuedRequests.get(i);
            queuedRequests.remove(i);
            RestSynchronizationService.doSyncWork(sync);
        }
    }

    protected RestSyncListener getListener() {
        return listener;
    }

    public abstract String getUrl();
    protected abstract void send(RestSynchronizationService sync);
    protected abstract int getExpectedSuccessResponseCode();


//////////// Response listener class

    public interface RestSyncListener {
        void onSuccess(User user);
        void onFailure(Throwable t);
        void onWrongResponseCode(int responseCode);
        void onCancelled();
    }
}
