/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.sync.work;

import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestHeader;
import com.badlogic.gdx.net.HttpStatus;

import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.util.Log;

/**
 * Created by frantisek on 19.1.16.
 */
public class DeleteUser extends SyncWork {

    public DeleteUser(User user) {
        setUser(user);
    }

    @Override
    public String getUrl() {
        return RestSynchronizationService.getDeleteUserUrl(getUser().getUuid());
    }

    @Override
    public String getHttpMethod() {
        return Net.HttpMethods.DELETE;
    }

    @Override
    public void send(RestSynchronizationService sync) {
        //TODO - This won't prevent NPE when user has no uuid. Method getUrl() gets called first, and throws NPE because of getUser().getUuid();
        if (getUser() == null || getUser().getUuid() == null || getUser().getUuid().isEmpty()) {
            Log.err(this.getClass(), "Can't delete user from REST server without uuid");
            return;
        }
        sync.setHeader(HttpRequestHeader.ContentType, "application/json");
        sync.send();
    }

    @Override
    protected int getExpectedSuccessResponseCode() {
        return HttpStatus.SC_OK;
    }
}
