/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.sync.work;

import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpStatus;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.model.UserDifficultySettingsDAO;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.model.screen.ScreenDAO;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.rest.UserRestPath;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.util.Log;

/**
 * Created by frantisek on 15.1.16.
 */
public class PushDataToServer extends SyncWork {

    public class UserUuidUpdatedEvent implements ApplicationBus.ApplicationEvent {

        private long userId;
        private String userUuid;

        private UserUuidUpdatedEvent(long userId, String userUuid) {
            this.userId = userId;
            this.userUuid = userUuid;
        }

        public String getUserUuid() {
            return userUuid;
        }

        public long getUserId() {
            return userId;
        }
    }

    private final RestSynchronizationService.SynchronizationType syncType;

    public PushDataToServer(User user, RestSynchronizationService.SynchronizationType syncType) {
        setUser(user);
        getUser().setGames(GameDAO.selectUsersGamesForSync(user.getId()));
        getUser().setScreens(ScreenDAO.selectAllScreenForSync(user.getId()));
		getUser().setDifficultySettings(UserDifficultySettingsDAO.getUserSettings(user.getId()));
        this.syncType = syncType;
    }

    @Override
    public String getUrl() {
        return RestSynchronizationService.getCreateUserUrl();
    }

    @Override
    public void send(RestSynchronizationService sync) {
        sync.setJsonObject(getUser());
        sync.send();

        Log.debug(this.getClass(), String.format("sending user '%s' uuid: %s", getUser().getName(), getUser().getUuid()));
    }

    @Override
    protected void onSuccessfulResponse(Net.HttpResponse httpResponse) {
        Log.debug(this.getClass(), "received uuid: " + httpResponse.getHeader("uuid"));

        String uuid = httpResponse.getHeader(UserRestPath.USER_UUID);
        if (uuid == null || uuid.isEmpty()) {
            Log.err(this.getClass(), "Failed to receive uuid from REST server");
            return;
        }

        ApplicationBus.getInstance().publishAsync(new UserUuidUpdatedEvent(getUser().getId(), uuid));

        if (syncType == RestSynchronizationService.SynchronizationType.FULL || getUser().getUuid() == null) {
            if (UserDAO.updateUserUuid(getUser().getId(), uuid)) {
                getUser().setUuid(uuid);
                RestSynchronizationService.doSyncWork(new ConfirmDataReceived(getUser()));
            }
        } else {
            GameDAO.markGamesAsSync(getUser().getGames());
            ScreenDAO.markScreensAsSync(getUser().getScreens());
            Log.debug(this.getClass(), String.format("Marking %d of games as synchronized", getUser().getGames() != null ? getUser().getGames().size() : 0));
        }
    }

    @Override
    protected int getExpectedSuccessResponseCode() {
        return HttpStatus.SC_CREATED;
    }
}
