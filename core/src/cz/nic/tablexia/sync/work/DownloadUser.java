/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.sync.work;

import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestHeader;
import com.badlogic.gdx.net.HttpStatus;
import com.badlogic.gdx.utils.Json;

import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.menu.user.UserAvatarDefinition;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.util.Log;

/**
 * Created by frantisek on 18.1.16.
 */
public class DownloadUser extends SyncWork {

    public static final int USER_NOT_FOUND_STATUS_CODE = 404;
    private final String uuid;

    public DownloadUser(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String getUrl() {
        return RestSynchronizationService.getDownloadUserUrl(uuid);
    }

    @Override
    public void send(RestSynchronizationService sync) {
        sync.setHeader(HttpRequestHeader.ContentType, "application/json");
        sync.send();
    }

    @Override
    protected void onSuccessfulResponse(Net.HttpResponse httpResponse) {
        String userJson = httpResponse.getResultAsString();
        Json json = new Json();
        json.setIgnoreUnknownFields(true);
        User user = json.fromJson(User.class, userJson);

        if (user == null) {
            Log.err(this.getClass(), "Failed to deserialize user from JSON");
            return;
        }

        long start = System.currentTimeMillis();
        user.setId(importUser(user));
        long finish = System.currentTimeMillis();

        setUser(user);

        UserRankManager.getInstance().refreshUserRank(user);

        //Download custom avatar if needed
        if(UserAvatarDefinition.isUserUsingCustomAvatar(getUser())) {
            RestSynchronizationService.doSyncWork(new UpdateAvatar(getUser()));
        }

        Log.debug(this.getClass(), "Duration: " + getTime(start, finish));
    }

    protected Long importUser(User user){
        return UserDAO.importUser(user);
    }

    @Override
    protected int getExpectedSuccessResponseCode() {
        return HttpStatus.SC_OK;
    }
}
