/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.user;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import net.engio.mbassy.listener.Handler;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.event.MenuControlEvent;
import cz.nic.tablexia.bus.event.QRCodeActiveEvent;
import cz.nic.tablexia.loader.application.ApplicationAvatarManager;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.loader.application.ApplicationInternalSoundManager;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.screen.createuser.PanoramaScreen;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.sync.work.DownloadUser;
import cz.nic.tablexia.sync.work.PushDataToServer;
import cz.nic.tablexia.sync.work.SyncWork;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AdaptiveSizeDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.AlertOnShowDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.BackButtonHideComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DimmerDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.PositiveNegativeButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.UserUuidSyncDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ViewportMaximumSizeComponent;
import cz.nic.tablexia.util.ui.scrollpane.ScrollPaneWithBars;

/**
 * UserMenu container
 *
 * @author Matyáš Latner
 */
public class UserMenu extends AbstractMenu {
    public static final String NEW_USER_BUTTON				= "new user button";

    private static final MenuControlType    MENU_CONTROL_TYPE   	 = MenuControlType.AUTOMATIC;
    private static final boolean            MENU_SCREEN_PAUSE  		 = false;
    private static final int                USER_ITEM_HEIGHT    	 = 80;
    private static final int                MENU_PADDING             = 30;

    private static final int    SYNC_REQUEST_DIALOG_WIDTH            = 450;
    private static final int    SYNC_REQUEST_DIALOG_HEIGHT           = 300;
    private static final float  SYNC_DIALOG_TITLE_PAD                = 0.15f;

    private static final float  SYNC_FAILED_DIALOG_BOTTOM_PADDING    = 1/40f;

    public static final String  CODE_REGEX                           = "((?:(?:[0-9]|[a-f]){8})-(?:(?:[0-9]|[a-f]){4})-(?:(?:[0-9]|[a-f]){4})-(?:(?:[0-9]|[a-f]){4})-(?:(?:[0-9]|[a-f]){12}))";

    private List<User> users;

    //Saves the last state of uuid sync text field... (Used to restore its state on try again button press)
    private String lastSyncUUID;
    private TablexiaComponentDialog syncDialog;
    private UserUuidSyncDialogComponent uuidSyncDialogComponent;
    private ScrollPane scrollPane;
    private Container<ScrollPane> container;

    public UserMenu(Float width, Float height) {
        super(width, height);
        users = new ArrayList<>();
        ApplicationBus.getInstance().subscribe(this);
    }

    @Override
    protected boolean isScreenPause() {
        resizeContainerSize();
        return MENU_SCREEN_PAUSE;
    }

    @Override
    protected void sizeChanged() {
        resizeContainerSize();
        super.sizeChanged();
    }

    private void resizeContainerSize(){
        if(getStage() != null && container != null) {
            float scrollPaneHeight = TablexiaSettings.getViewportHeight(Tablexia.getActualScreen().getStage()) - SELECTBOX_LOCALE_HEIGHT - 4 * MAINMENU_PADDING;
            container.height(scrollPaneHeight);
            container.invalidateHierarchy();
        }
    }

    @Override
    protected MenuControlType getMenuControlType() {
        return MENU_CONTROL_TYPE;
    }

    @Override
    public void initMenuItems(Container<Group> layoutContainer) {
        final VerticalGroup menuItemsLayout = new VerticalGroup();

        final float userMenuItemWidth = background.getWidth() - MENU_PADDING;
        this.users = UserDAO.selectActiveUsers();
        for (final User user : users) {
            final LoginUserMenuItem menuItem = new LoginUserMenuItem(user);
            final Group userItemGroup = getGroupForMenuItem(menuItem, getWidth());
            userItemGroup.setSize(userMenuItemWidth, USER_ITEM_HEIGHT);
            userItemGroup.addListener(new ClickListener() {

                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (!event.isHandled()) {
                        ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.BUTTON_CLICKED).play();
                        doMenuAction(menuItem.getMenuAction(), true, true);
                        menuItem.performAction();
                    }
                }
            });
            menuItemsLayout.addActor(userItemGroup);
            menuItemsLayout.expand().fill();
            userItemGroup.setName(user.getName());
        }
        final UserMenuNewSelectBoxItemGroup userMenuNewSelectBoxItemGroup = new UserMenuNewSelectBoxItemGroup() {
            @Override
            public void performAction() {
                ApplicationBus.getInstance().post(new MenuControlEvent(UserMenu.class, MenuAction.HIDE, true)).asynchronously();
                ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(PanoramaScreen.class, TablexiaApplication.ScreenTransaction.MOVE_UP)).asynchronously();
            }

            @Override
            public User getUser() {
                return null;
            }
        };
        userMenuNewSelectBoxItemGroup.setName(NEW_USER_BUTTON);


        userMenuNewSelectBoxItemGroup.setSize(userMenuItemWidth, USER_ITEM_HEIGHT);
        userMenuNewSelectBoxItemGroup.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.BUTTON_CLICKED).play();
                userMenuNewSelectBoxItemGroup.performAction();
            }
        });
        menuItemsLayout.addActor(userMenuNewSelectBoxItemGroup);
        menuItemsLayout.expand().fill();

        final UserMenuDownloadSelectBoxItemGroup userDownload = new UserMenuDownloadSelectBoxItemGroup() {
            @Override
            public void performAction() {
                showSyncDialog(lastSyncUUID);
            }

            @Override
            public User getUser() {
                return null;
            }
        };

        userDownload.setSize(userMenuItemWidth, USER_ITEM_HEIGHT);
        userDownload.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.BUTTON_CLICKED).play();
                userDownload.performAction();
            }
        });
        menuItemsLayout.addActor(userDownload);
        menuItemsLayout.expand().fill();

        if(TablexiaSettings.getInstance().getPlatform() == TablexiaSettings.Platform.DESKTOP) {
            final UserMenuExitSelectBoxItemGroup exitApp = new UserMenuExitSelectBoxItemGroup() {
                @Override
                public void performAction() {
                    TablexiaComponentDialog exitDialog = TablexiaComponentDialogFactory.getInstance().createExitDialog();
                    exitDialog.show(TablexiaComponentDialogFactory.WARNING_DIALOG_WIDTH, TablexiaComponentDialogFactory.WARNING_DIALOG_HEIGHT);
                }

                @Override
                public User getUser() {
                    return null;
                }
            };
            exitApp.setSize(userMenuItemWidth, USER_ITEM_HEIGHT);
            exitApp.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.BUTTON_CLICKED).play();
                    exitApp.performAction();
                }
            });
            menuItemsLayout.addActor(exitApp);
            menuItemsLayout.expand().fill();
        }

        scrollPane = new ScrollPaneWithBars(menuItemsLayout, false, true);
        scrollPane.setFillParent(true);
        container = new Container<>(scrollPane).fill();
        container.align(Align.top);
        container.height(Tablexia.getActualScreen().getViewportHeight() - SELECTBOX_LOCALE_HEIGHT - 4 * MAINMENU_PADDING);

        layoutContainer.setActor(container);

        scrollPane.setScrollingDisabled(true,false);
        Tablexia.getActualScreen().getStage().setScrollFocus(scrollPane);
    }

    private void showSyncDialog(final String syncUUID) {
        uuidSyncDialogComponent = new UserUuidSyncDialogComponent(
                syncUUID,
                new Runnable() {
                    @Override
                    public void run() {
                        showQRSyncFailedDialog();
                    }
                }
        );
        uuidSyncDialogComponent.setRepositionOnFocusOnMobileDevices(true);
        ArrayList<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>() {
            {
                add(new CenterPositionDialogComponent());
                add(new DimmerDialogComponent());
                add(new BackButtonHideComponent());
                add(new ViewportMaximumSizeComponent());
                add(new AdaptiveSizeDialogComponent());
                add(new FixedSpaceContentDialogComponent(SYNC_DIALOG_TITLE_PAD));
                add(new TextContentDialogComponent(
                    uuidSyncDialogComponent.hasQRCodeScanner() ?
                            ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYNC_REQUEST_QR_DIALOG_TEXT) :
                            ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYNC_REQUEST_DIALOG_TEXT)
                ));
                add(new ResizableSpaceContentDialogComponent());
                add(uuidSyncDialogComponent);
                add(new ResizableSpaceContentDialogComponent());
                add(new PositiveNegativeButtonContentDialogComponent(
                    new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            lastSyncUUID = uuidSyncDialogComponent.getText();
                            syncDialog = null;

                            if (lastSyncUUID != null && lastSyncUUID.length() > 0 && lastSyncUUID.toLowerCase().matches(CODE_REGEX)) {
                                List<User> beforeSyncUsers = UserDAO.selectActiveUsers();
                                runSyncWork(lastSyncUUID);
                                for(User beforeUser: beforeSyncUsers){
                                    if(lastSyncUUID.equals(beforeUser.getUuid())){
                                        showSyncFailedDialog(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYNC_REQUEST_UUID_EXISTS));
                                        break;
                                    }
                                }
                            }
                            else { //ID was blank or didn't match the ID regex
                                showSyncFailedDialog(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYNC_REQUEST_WRONG_ID));
                            }

                            //Prevents keyboard getting stuck
                            Gdx.input.setOnscreenKeyboardVisible(false);
                        }
                    },
                    new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            lastSyncUUID = null;
                            syncDialog = null;

                            //Prevents keyboard getting stuck
                            Gdx.input.setOnscreenKeyboardVisible(false);
                        }
                    },
                    PositiveNegativeButtonContentDialogComponent.PositiveNegativeButtonType.CONFIRM_DECLINE
                ));
                add(new FixedSpaceContentDialogComponent());
                add(new BackButtonHideComponent());
            }
        };

        syncDialog = TablexiaComponentDialogFactory.getInstance().createDialog(components.toArray(new TablexiaDialogComponentAdapter[]{}));
        syncDialog.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
        syncDialog.show(SYNC_REQUEST_DIALOG_WIDTH, SYNC_REQUEST_DIALOG_HEIGHT);
    }

    private void runSyncWork(String uuid) {
        DownloadUser syncWork = new DownloadUser(uuid);
        syncWork.registerListener(new SyncWork.RestSyncListener() {
            @Override
            public void onSuccess(User user) {
                ApplicationBus.getInstance().post(new RefreshUserMenu()).asynchronously();
            }

            @Override
            public void onFailure(Throwable t) {
                showSyncFailedDialog(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYNC_REQUEST_ERROR));
            }

            @Override
            public void onWrongResponseCode(int responseCode) {
                String failReason = ApplicationTextManager.ApplicationTextsAssets.SYNC_REQUEST_ERROR;

                if(responseCode == DownloadUser.USER_NOT_FOUND_STATUS_CODE)
                    failReason = ApplicationTextManager.ApplicationTextsAssets.SYNC_REQUEST_WRONG_ID;

                showSyncFailedDialog(ApplicationTextManager.getInstance().getText(failReason));
            }

            @Override
            public void onCancelled() {
                //no action
            }
        });
        RestSynchronizationService.doSyncWork(syncWork);
    }

    private void showQRSyncFailedDialog() {
        showSyncFailedDialog(
                ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYNC_REQUEST_WRONG_ID),
                null,
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        uuidSyncDialogComponent.startQRCodeScanner();
                    }
                }
        );
    }

    private void showSyncFailedDialog(final String reason) {
        showSyncFailedDialog(
                reason,
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        lastSyncUUID = null;
                    }
                }, //Cancel Button
                new ClickListener() { //Try again
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        showSyncDialog(lastSyncUUID);
                    }
                }
        );
    }

    private void showSyncFailedDialog(final String reason, final InputListener cancelListener, final InputListener againListener) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                List<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>() {
                    {
                        add(new CenterPositionDialogComponent());
                        add(new DimmerDialogComponent());
                        add(new ViewportMaximumSizeComponent());
                        add(new AdaptiveSizeDialogComponent());

                        add(new ResizableSpaceContentDialogComponent());
                        add(new TextContentDialogComponent(reason));

                        add(new ResizableSpaceContentDialogComponent());
                        add(new TwoButtonContentDialogComponent(
                                ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_DECLINE),
                                ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_RETRY),
                                new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_NO_ICON)),
                                new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_REPEAT_ICON)),
                                StandardTablexiaButton.TablexiaButtonType.RED,
                                StandardTablexiaButton.TablexiaButtonType.GREEN,
                                cancelListener,
                                againListener
                        ));
                        add(new FixedSpaceContentDialogComponent(SYNC_FAILED_DIALOG_BOTTOM_PADDING));

                        add(new AlertOnShowDialogComponent());
                        add(new AdaptiveSizeDialogComponent());
                        add(new CenterPositionDialogComponent());
                    }
                };

                TablexiaComponentDialog dialog = TablexiaComponentDialogFactory.getInstance().createDialog(components.toArray(new TablexiaDialogComponentAdapter[]{}));
                dialog.show(SYNC_REQUEST_DIALOG_WIDTH, SYNC_REQUEST_DIALOG_HEIGHT, false);
            }
        });
    }

    @Override
    public void dispose() {
        ApplicationBus.getInstance().unsubscribe(this);
    }

    @Override
    protected void disableScroll(boolean disable) {
        if(scrollPane == null || scrollPane.isScrollingDisabledY() == disable)
            return;

        scrollPane.setScrollingDisabled(true, disable);

        if(disable){

            if(!TablexiaComponentDialog.DialogVisibleEvent.isDialogVisible()) {
                Tablexia.getActualScreen().setScrollableLayoutFocus();
                Tablexia.getActualScreen().enableScrollableLayoutFocus();
            }

        }else {

            Tablexia.getActualScreen().disableScrollableLayoutFocus();
            Tablexia.getActualScreen().getStage().setScrollFocus(scrollPane);
        }


        scrollPane.layout();
    }

//////////////////////////// CREATE USER EVENT HANDLING

    @Handler
    public void handleCreatedUserEvent(final UserDAO.CreatedUserEvent createdUserEvent) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                UserRankManager.getInstance().onNewUser(createdUserEvent.getUser());
                initMenuItems();
            }
        });
    }

    @Handler
    public void handleRefreshUserMenuEvent(RefreshUserMenu refreshEvent) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                initMenuItems();
            }
        });
    }

    @Handler
    public void updateUserUuid(PushDataToServer.UserUuidUpdatedEvent event) {
        for (final User user : users) {
            if (user != null && user.getId() == event.getUserId()) {
                user.setUuid(event.getUserUuid());
            }
        }
    }

    @Handler
    public void onAvatarsUpdatedEvent(ApplicationAvatarManager.AvatarUpdatedEvent avatarUpdatedEvent) {
       Gdx.app.postRunnable(new Runnable() {
           @Override
           public void run() {
               initMenuItems();
           }
       });
    }

//////////////////////////// QR CAMERA EVENT HANDLING

    @Handler
    public void handleQRCodeActiveEvent(QRCodeActiveEvent activeEvent){
        if(uuidSyncDialogComponent != null && uuidSyncDialogComponent.getDialog() != null) {
            uuidSyncDialogComponent.getDialog().setTouchable(activeEvent.isActive() ? Touchable.disabled : Touchable.enabled);
        }
    }

    public static class RefreshUserMenu implements ApplicationBus.ApplicationEvent {

    }
}
