/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.user;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.main.user.UserSelectBox;
import cz.nic.tablexia.util.ui.TablexiaLabel;


/**
 * Created by Aneta Steimarová on 1.2.18.
 */

public abstract class UserMenuExitSelectBoxItemGroup extends Actor implements UserSelectBox.UserSelectBoxItem {
    private static final String                                 EXIT_APP_TEXT_KEY   = ApplicationTextManager.ApplicationTextsAssets.EXIT_APP_BUTTON;

    private static final ApplicationFontManager.FontType        EXIT_APP_TEXT_FONT  = ApplicationFontManager.FontType.BOLD_20;
    private static final Color                                  EXIT_APP_TEXT_COLOR = new Color(0.098f, 0.086f, 0.075f, 1f);
    private static final int                                    EXIT_APP_TEXT_ALIGN = Align.center;

    private final NinePatch background;
    private TablexiaLabel textLabel;

    public UserMenuExitSelectBoxItemGroup() {
        background  = ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUBUTTON_BACKGROUND);

        textLabel = new TablexiaLabel(ApplicationTextManager.getInstance().getText(EXIT_APP_TEXT_KEY), new TablexiaLabel.TablexiaLabelStyle(EXIT_APP_TEXT_FONT, EXIT_APP_TEXT_COLOR));
        textLabel.setAlignment(EXIT_APP_TEXT_ALIGN);
    }

    public abstract void performAction();

    @Override
    public void draw(Batch batch, float parentAlpha) {
        EXIT_APP_TEXT_COLOR.a = parentAlpha;

        background.draw(batch, getX(), getY(), getWidth(), getHeight());

        textLabel.setBounds(getX(), getY(), getWidth(), getHeight());
        textLabel.draw(batch, parentAlpha);
    }
}
