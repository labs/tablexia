/*
 *
 *  * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.menu.user;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.event.DeleteUserSynchronizationEvent;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.IMenuItem;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;

public class LoginUserMenuSelectBoxItemGroup extends UserMenuSelectBoxItemGroup {


    private static final int BIN_IMAGE_OFFSET = 10;
    private static final int BIN_VERTICAL_TOUCH_AREA = 40;
    private static final int BIN_WIDTH = 19;
    private static final int BIN_HEIGHT = 30;

    protected static final int MAX_VISIBLE_TITLE_WIDTH = 120;

    private Image binImage;

    public LoginUserMenuSelectBoxItemGroup(IMenuItem menuItem, float width) {
        super(menuItem, width);
        binImage = new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_BIN));
        binImage.setSize(BIN_WIDTH, BIN_HEIGHT);
        binImage.setPosition(getX() + getWidth() - binImage.getWidth() - BIN_IMAGE_OFFSET, getY() + (getHeight() / 2 - binImage.getHeight() / 2));

        addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                if (isBinImageTouched(x, y)) {
                    TablexiaComponentDialogFactory.getInstance().createWarningYesNoDialog(
                            String.format("%s %s?", ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.USERMENU_DELETE_USER), getUser().getName()),
                            new ClickListenerWithSound() {
                                @Override
                                public void onClick(InputEvent event, float x, float y) {
                                    getUser().setDeleted(true);
                                    UserDAO.updateUser(getUser());
                                    ApplicationBus.getInstance().publishAsync(new DeleteUserSynchronizationEvent(getUser()));
                                    ApplicationBus.getInstance().publishAsync(new UserMenu.RefreshUserMenu());
                                }
                            },
                            true
                    ).show(TablexiaComponentDialogFactory.WARNING_DIALOG_WIDTH, TablexiaComponentDialogFactory.WARNING_DIALOG_HEIGHT);
                    return true;
                } else {
                    return super.touchDown(event, x, y, pointer, button);
                }
            }
        });

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        binImage.getDrawable().draw(batch, getX() + getWidth() - binImage.getWidth() - BIN_IMAGE_OFFSET,
                getY() + (getHeight() / 2 - binImage.getHeight() / 2), binImage.getWidth(), binImage.getHeight());
    }

    private boolean isBinImageTouched(float x, float y) {
        float xMin = getWidth() - BIN_VERTICAL_TOUCH_AREA - BIN_IMAGE_OFFSET;
        float yMin = 0;

        Rectangle r = new Rectangle(xMin, yMin, BIN_VERTICAL_TOUCH_AREA + BIN_IMAGE_OFFSET, getHeight());

        return r.contains(x, y);
    }

}
