/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.user;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.main.user.UserSelectBox;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by Matyáš Latner.
 */
public abstract class UserMenuNewSelectBoxItemGroup extends Actor implements UserSelectBox.UserSelectBoxItem {

    private static final ApplicationFontManager.FontType    NEWUSER_TEXT_FONT   = ApplicationFontManager.FontType.BOLD_20;
    private static final Color                              NEWUSER_TEXT_COLOR  = new Color(0.098f, 0.086f, 0.075f, 1f);
    private static final int                                NEWUSER_TEXT_ALIGN  = Align.center;

    private final Image         background;
    private final String        text;

    private TablexiaLabel       textLabel;

    public UserMenuNewSelectBoxItemGroup() {
        background  = new Image(ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND));
        text        = ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.USERMENU_NEWUSER);
        textLabel   = new TablexiaLabel(text, new TablexiaLabel.TablexiaLabelStyle(NEWUSER_TEXT_FONT, NEWUSER_TEXT_COLOR));
        textLabel.setAlignment(NEWUSER_TEXT_ALIGN);
    }

    public abstract void performAction();

    @Override
    public void draw(Batch batch, float parentAlpha) {
        NEWUSER_TEXT_COLOR.a = parentAlpha;

        background.setBounds(getX(), getY(), getWidth(), getHeight());
        background.draw(batch, parentAlpha);

        textLabel.setBounds(getX(), getY(), getWidth(), getHeight());
        textLabel.draw(batch, parentAlpha);
    }
}
