/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.user;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import net.engio.mbassy.listener.Handler;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationAvatarManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.menu.IMenuItem;
import cz.nic.tablexia.menu.main.user.UserSelectBox;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by Matyáš Latner.
 */
public class UserMenuSelectBoxItemGroup extends Group implements UserSelectBox.UserSelectBoxItem {

    private static final ApplicationFontManager.FontType        TEXT_FONT                   = ApplicationFontManager.FontType.REGULAR_20;
    private static final int                                    TEXT_LEFT_OFFSET            = 15;
    private static final int                                    IMAGE_WIDTH_OFFSET          = 10;
    private static final float                                  BADGE_ICON_HEIGHT           = 0.42f;
    private static final float                                  BADGE_ICON_OFFSET_X         = -0.75f;
    private static final Color                                  FOREGROUND_COLOR            = new Color(0.322f, 0.278f, 0.255f, 1f);
    private static final String                                 THREE_DOTS                  = "\u2026";
    private static final int                                    MIN_VISIBLE_TITLE_LENGTH    = 8;

    protected static final int                                  IMAGE_BORDER_OFFSET         = 4;
    protected static final int                                  MAX_VISIBLE_TITLE_WIDTH     = 150;

    private final Image                 background;
    private final TextureRegionDrawable image;
    private final UserMenuItem          userMenuItem;
    private final String                visibleName;
    private       boolean               showBadge = false;

    private       TextureRegionDrawable badgeIcon;

    private TablexiaLabel textLabel;

    public UserMenuSelectBoxItemGroup(IMenuItem menuItem, float width) {
        this.userMenuItem = (UserMenuItem)menuItem;

        new Image(ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND));
        background  = new Image(ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND));
        image       = new TextureRegionDrawable(ApplicationAvatarManager.getInstance().getAvatarTextureRegion(userMenuItem.getUser()));
        visibleName = userMenuItem.getTitle().length() <= MIN_VISIBLE_TITLE_LENGTH ? userMenuItem.getTitle() : getVisibleTitle(userMenuItem.getTitle());
        textLabel   = new TablexiaLabel("", new TablexiaLabel.TablexiaLabelStyle(TEXT_FONT, FOREGROUND_COLOR));
        textLabel.setAlignment(Align.left);
        setShowBadge(true);
    }

    public void performAction() {
        userMenuItem.performAction();
    }

    public User getUser() {
        return userMenuItem.getUser();
    }

    public void setShowBadge(boolean showBadge) {
        this.showBadge = showBadge;
        onShowBadgeSet();
    }

    private void onShowBadgeSet() {
        if(isShowBadge()) {
            ApplicationBus.getInstance().subscribe(this);
            updateBadgeIcon();
        }
        else {
            ApplicationBus.getInstance().unsubscribe(this);
            badgeIcon = null;
        }
    }

    private void updateBadgeIcon() {
        if(!UserRankManager.getInstance().getRank(getUser()).isShownOnUserMenu())
            return;

        if (badgeIcon == null)
            badgeIcon = new TextureRegionDrawable();

        badgeIcon.setRegion(ApplicationAtlasManager.getInstance().getTextureRegion(UserRankManager.getInstance().getRank(getUser()).getBadgeIconKey()));
    }

    public boolean isShowBadge() {
        return showBadge;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        FOREGROUND_COLOR.a = parentAlpha;
        textLabel.setColor(FOREGROUND_COLOR);

        background.setBounds(getX(), getY(), getWidth(), getHeight());
        background.draw(batch, parentAlpha);

        float imageWidth = getHeight();
        image.draw(batch, getX() + IMAGE_BORDER_OFFSET, getY() + IMAGE_BORDER_OFFSET, imageWidth - IMAGE_BORDER_OFFSET * 2 - IMAGE_WIDTH_OFFSET, getHeight() - IMAGE_BORDER_OFFSET * 2 );

        if(badgeIcon != null) {
            float badgeIconHeight = BADGE_ICON_HEIGHT * getHeight();
            float badgeIconWidth  = badgeIconHeight * (badgeIcon.getRegion().getRegionWidth() / (float) badgeIcon.getRegion().getRegionHeight());

            badgeIcon.draw(
                    batch,
                    getX() + imageWidth - IMAGE_BORDER_OFFSET - IMAGE_WIDTH_OFFSET + (badgeIconWidth * BADGE_ICON_OFFSET_X),
                    getY() + IMAGE_BORDER_OFFSET,
                    badgeIconWidth,
                    badgeIconHeight
            );
        }

        float textPositionXOffset = imageWidth + TEXT_LEFT_OFFSET;
        float textPositionX = getX() + textPositionXOffset;

        textLabel.setText(visibleName);
        textLabel.setBounds(textPositionX, getY(), getWidth() - textPositionXOffset, getHeight());
        textLabel.draw(batch, parentAlpha);
    }

    private String getVisibleTitle(String fullTitle){
        BitmapFont font = ApplicationFontManager.getInstance().getDistanceFieldFont(TEXT_FONT);
        GlyphLayout layout = new GlyphLayout();
        String visibleTitle = fullTitle;
        layout.setText(font, visibleTitle);
        if (layout.width*TEXT_FONT.getScale()> MAX_VISIBLE_TITLE_WIDTH) {
            int shortenBy = 1;
            visibleTitle = fullTitle.substring(0, fullTitle.length()- shortenBy);
            layout.setText(font, visibleTitle);
            while (layout.width*TEXT_FONT.getScale() > MAX_VISIBLE_TITLE_WIDTH && visibleTitle.length() > MIN_VISIBLE_TITLE_LENGTH && shortenBy < fullTitle.length()-MIN_VISIBLE_TITLE_LENGTH) {
                shortenBy++;
                visibleTitle = fullTitle.substring(0, fullTitle.length()- shortenBy);
                layout.setText(font, visibleTitle);
            }
            visibleTitle += THREE_DOTS;
        }
        return visibleTitle;
    }

    @Handler
    public void onUserRankUpEvent(UserRankManager.UserRankUpEvent userRankUpEvent) {
        if(!userRankUpEvent.getUser().equals(getUser())) {
            return;
        }

        updateBadgeIcon();
    }
}
