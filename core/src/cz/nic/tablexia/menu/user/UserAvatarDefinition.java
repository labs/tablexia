/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.definitions.INumberedDefinition;
import cz.nic.tablexia.util.Log;

/**
 * Created by Vitaliy Vashchenko on 8.1.16.
 */

public enum  UserAvatarDefinition implements INumberedDefinition {
    AVATAR_0(0,   ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_0, 14, false),
    AVATAR_1(1,   ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_1, 10, true),
    AVATAR_2(2,   ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_2, 1,  true),
    AVATAR_3(3,   ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_3, 11, true),
    AVATAR_4(4,   ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_4, 12, true),
    AVATAR_5(5,   ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_5, 13, true),
    AVATAR_6(6,   ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_6, 2,  true),
    AVATAR_7(7,   ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_7, 3,  true),
    AVATAR_8(8,   ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_8, 4,  true),
    AVATAR_9(9,   ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_9, 5,  true),
    AVATAR_10(10, ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_10, 6, true),
    AVATAR_11(11, ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_11, 7, true),
    AVATAR_12(12, ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_12, 8, true),
    AVATAR_13(13, ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_13, 9, true),
    AVATAR_14(14, ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_14, 15, true),

    CUSTOM_AVATAR(999, ApplicationAtlasManager.USERMENU_MENUITEM_CUSTOM_AVATAR,  0, false);

    private static final UserAvatarDefinition FALLBACK_VARIANT = AVATAR_0;

    private int number;
    private String avatarPath;
    private int order;
    private boolean selectable;

    UserAvatarDefinition(int number, String avatarPath, int order, boolean selectable) {
        this.number = number;
        this.avatarPath = avatarPath;
        this.order = order;
        this.selectable = selectable;
    }

    public int number() {
        return number;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public int getOrder() {
        return order;
    }

    public boolean isSelectable() {
        return selectable;
    }

    public static String getAvatar(String id){
        return getAvatar(Integer.parseInt(id));
    }

    public static boolean isUserUsingCustomAvatar(User user) {
        return Integer.parseInt(user.getAvatar()) == UserAvatarDefinition.CUSTOM_AVATAR.number();
    }

    public static String getAvatar(int id){
        for(UserAvatarDefinition avatarDefinition : values()) {
            if(avatarDefinition.number() == id) return avatarDefinition.getAvatarPath();
        }

        Log.info(UserAvatarDefinition.class, "Couldn't find avatar with id: " + id + ". Using Fallback avatar instead!");
        return FALLBACK_VARIANT.getAvatarPath();
    }

    public static ArrayList<UserAvatarDefinition> getSelectableAvatarDefinitionsInOrder() {
        ArrayList<UserAvatarDefinition> selectableAvatars = new ArrayList<>();
        for(UserAvatarDefinition avatarDefinition : UserAvatarDefinition.values()) {
            if(avatarDefinition.isSelectable()) selectableAvatars.add(avatarDefinition);
        }
        Collections.sort(selectableAvatars, new Comparator<UserAvatarDefinition>() {
            @Override
            public int compare(UserAvatarDefinition avatar1, UserAvatarDefinition avatar2) {
                return avatar1.getOrder() - avatar2.getOrder();
            }
        });
        return selectableAvatars;
    }
}