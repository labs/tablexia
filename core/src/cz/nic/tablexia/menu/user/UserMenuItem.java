/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.user;

import com.badlogic.gdx.scenes.scene2d.Group;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.IMenuItem;
import cz.nic.tablexia.shared.model.User;

/**
 * Created by frantisek on 11.1.16.
 */
public class UserMenuItem implements IMenuItem {

    private User user;

    private static final Class<UserMenuSelectBoxItemGroup> USER_ITEM_CLASS = UserMenuSelectBoxItemGroup.class;


    public UserMenuItem(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String getTitle() {
        return user.getName();
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String[] getIcons() {
        // TODO return user avatar
        return null;
    }

    @Override
    public Class<? extends Group> getItemGroupClass() {
        return USER_ITEM_CLASS;
    }

    @Override
    public void performAction() {
        TablexiaSettings.getInstance().changeUser(this.user);
    }

    @Override
    public AbstractMenu.MenuAction getMenuAction() {
        return AbstractMenu.MenuAction.HIDE;
    }

    @Override
    public IMenuItem[] getSubmenu() {
        return null;
    }
}