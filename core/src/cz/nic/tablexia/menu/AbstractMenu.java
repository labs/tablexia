/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationExternalSoundManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationInternalSoundManager;
import cz.nic.tablexia.menu.main.locale.LocaleSelectBox;
import cz.nic.tablexia.menu.main.sound.VolumeBar;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.menu.main.sound.SoundMuteButton;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;

/**
 * Abstract Tablexia menu
 *
 * @author Matyáš Latner
 */
public abstract class AbstractMenu extends Stack implements Disposable {
    public static final String GAME_SUBMENU_OPENED              = "game submenu opened";
    public static final String OPEN_CLOSE_MENU_BUTTON_NAME      = "open/close menu button";

    public static final String SCENARIO_STEP_READY_MENU         = "ready menu";
    public static final String SCENARIO_STEP_OPENED_MENU        = "opened menu";

    private static final String TABLEXIA_VERSION_LABEL = "%s\r\n%s";

    protected static final float  SCROLL_PANE_INIT_SIZE           = 200f;

    protected enum MenuControlType {

        MANUAL      (true,   false,  true),
        AUTOMATIC   (false,  true,   false);

        private boolean hasSwitch;
        private boolean modal;
        private boolean draggable;

        MenuControlType(boolean hasSwitch, boolean modal, boolean draggable) {
            this.hasSwitch = hasSwitch;
            this.modal = modal;
            this.draggable = draggable;
        }

        public boolean isHasSwitch() {
            return hasSwitch;
        }

        public boolean isModal() {
            return modal;
        }

        public boolean isDraggable() {
            return draggable;
        }
    }

    public enum MenuAction {

        OPEN(new MenuActionMethod() {
            @Override
            public void doAction(AbstractMenu abstractMenu, boolean animated, boolean force) {
                abstractMenu.openMenu(animated, force);
            }
        }),
        CLOSE(new MenuActionMethod() {
            @Override
            public void doAction(AbstractMenu abstractMenu, boolean animated, boolean force) {
                abstractMenu.closeMenu(animated, force);
            }
        }),
        SHOW(new MenuActionMethod() {
            @Override
            public void doAction(AbstractMenu abstractMenu, boolean animated, boolean force) {
                abstractMenu.closeMenu(animated, true);
            }
        }),
        HIDE(new MenuActionMethod() {
            @Override
            public void doAction(AbstractMenu abstractMenu, boolean animated, boolean force) {
                abstractMenu.hideMenu(animated);
            }
        });

        private interface MenuActionMethod {
            void doAction(AbstractMenu abstractMenu, boolean animated, boolean force);
        }

        private MenuActionMethod actionMethod;

        MenuAction(MenuActionMethod actionMethod) {
            this.actionMethod = actionMethod;
        }

        public void doAction(AbstractMenu abstractMenu, boolean animated, boolean force) {
            actionMethod.doAction(abstractMenu, animated, force);
        }
    }

    private static final int                    LAYOUT_CONTAINER_ALIGN              = Align.left;

    private static final int                    DRAG_OFFSET_TO_DISABLE_CONTROL      = 5;
    private static final float                  MOVE_DISTANCE_TO_PLAY_SOUND_RATIO   = 1f / 3;

    private static final float                  MENU_MINIMAL_WIDTH                  = 400;
    private static final float                  MENU_MOVE_DURATION                  = 0.8f;
    private static final Interpolation.PowOut   MENU_MOVE_INTERPOLATION             = Interpolation.pow4Out;
    private static final float                  MENU_CONTROLLER_SIZE_RATIO          = 8f / 10;
    private static final float                  OPEN_CLOSE_BUTTON_TOP_OFFSET_RATIO  = 1f / 10;
    private static final float                  REACTION_BORDER_WIDTH_RATIO         = 1f / 3;
    public  static final int                    MAX_OPEN_CLOSE_BUTTON_WIDTH         = 65;

    public static final float                               MAINMENU_PADDING                = 30f;
    public static final float                               APP_NAME_TEXT_PADDING_LEFT      = 10f;
    public static final float                               SOUND_MUTE_BUTTON_PADDING_LEFT  = 5f;

    public static final int                                 SELECTBOX_LOCALE_HEIGHT         = 40;
    private static final float                              SELECTBOX_LOCALE_WIDTH_RATIO    = 0.5f;
    private static final ApplicationFontManager.FontType    APP_NAME_TEXT_FONT          = ApplicationFontManager.FontType.REGULAR_10;
    private static final int                                APP_NAME_TEXT_ALIGN             = Align.center;
    private static final float                              SOUND_MUTE_BUTTON_WIDTH_RATIO   = 0.2f;
    private static final float                              APP_NAME_WIDTH_RATIO            = 0.3f;

    private final Container<Group>  layoutContainer;
    protected final Container<Table>localeMenuContainer;
    private float                   menuHidePositionX;
    private float                   menuClosePositionX;
    private int                     menuOpenPositionX;
    private float                   menuGravityPositionX;
    private int                     menuPositionY;
    private Action                  moveAction;
    private boolean                 isReady;
    private boolean                 isClosing;
    private boolean                 isOpening;
    private boolean                 openEventFired;
    private Boolean                 lastPauseState;
    private final Stack             backgroundStack;

    private boolean                 disableMenuGravity = false;
    private int                     initialPositionX;
    private boolean                 disableControl;

    private HorizontalGroup         horizontalGroup;
    private Stack                   borderStack;

    private Cell                    openHandlerTopOffsetCell;
    private Image                   reactionBorder;
    protected Image                 background;
    private Image                   border;

    public AbstractMenu(Float width, Float height) {
        background    = new TablexiaNoBlendingImage(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.MAINMENU_BACKGROUND));
        border        = new Image(new TiledDrawable(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.MAINMENU_BORDER)));

        setBounds(0, 0, width > MENU_MINIMAL_WIDTH ? width : MENU_MINIMAL_WIDTH, height);

        // layout container - contains menu without language change button
        layoutContainer = new Container<Group>();
        layoutContainer.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes(), false);

        Table localeTable = getLocaleMenu();   //Table with language change button, sound setting and Tablexia version
        Table completeMenuTable = new Table(); //Table with complete menu
        localeMenuContainer = new Container<Table>();
        Container<Table> completeMenuContainer = new Container<Table>();

        localeTable.setFillParent(true);
        localeTable.bottom();
        localeTable.pad(MAINMENU_PADDING);

        localeMenuContainer.bottom();
        localeMenuContainer.setActor(localeTable);

        completeMenuTable.setFillParent(true);
        completeMenuTable.bottom();
        completeMenuTable.add(layoutContainer).expandY().fillY();
        completeMenuTable.row();
        completeMenuTable.add(localeMenuContainer).fill().bottom();

        completeMenuContainer.bottom();
        completeMenuContainer.setActor(completeMenuTable);

        backgroundStack = new Stack();
        backgroundStack.add(background);
        backgroundStack.add(completeMenuContainer);
        if (getMenuControlType().isDraggable()) {
            backgroundStack.addListener(dragListener);
        }

        borderStack = new Stack();
        borderStack.add(border);

        horizontalGroup = new HorizontalGroup();
        horizontalGroup.setHeight(height);
        horizontalGroup.addActor(backgroundStack);
        horizontalGroup.addActor(borderStack);

        addActor(horizontalGroup);

        isClosing = false;
        isOpening = false;
        openEventFired = false;

        prepareBackgroundSize();

        // open close button
        if (getMenuControlType().isHasSwitch()) {
            Button openCloseButton = new Button(new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.MAINMENU_SWITCH)));
            openCloseButton.getStyle().up.setMinWidth(border.getWidth());
            openCloseButton.align(Align.topRight);
            openCloseButton.addListener(new ClickListener() {

                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (!isDisableControl()) {
                        ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.BUTTON_CLICKED).play();
                        disableMenuGravity(true);
                        toggleMenu();
                    }
                }
            });
            if (getMenuControlType().isDraggable()) {
                openCloseButton.addListener(dragListener);
            }

            openCloseButton.setName(OPEN_CLOSE_MENU_BUTTON_NAME);

            Table openCloseButtonContainer = new Table();
            openHandlerTopOffsetCell = openCloseButtonContainer.add();
            openCloseButtonContainer.row();
            openCloseButtonContainer.add(openCloseButton).expandX().right();
            openCloseButtonContainer.row();
            openCloseButtonContainer.add().expandY();
            borderStack.addActor(openCloseButtonContainer);

            reactionBorder = new Image(new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getColorTextureRegion(Color.RED)));
            reactionBorder.getDrawable().setMinHeight(getHeight());
            reactionBorder.addAction(Actions.alpha(0));
            if (getMenuControlType().isDraggable()) {
                reactionBorder.addListener(dragListener);
            }
            Table reactionBorderContainer = new Table();
            reactionBorderContainer.add(reactionBorder).width(border.getWidth() * REACTION_BORDER_WIDTH_RATIO).expandY();
            reactionBorderContainer.add().expandX().expandY();
            borderStack.addActor(reactionBorderContainer);
        }

        prepareMenuPositions();
        hideMenu(false);
    }

    protected Image getBackground() {
        return background;
    }

    private void prepareBackgroundSize() {
        float backgroundWidthRatio = getWidth() / (background.getDrawable().getMinWidth() + border.getDrawable().getMinWidth());
        float backgroundWidth = background.getDrawable().getMinWidth() * backgroundWidthRatio;
        float borderWidth = border.getDrawable().getMinWidth() * backgroundWidthRatio;
        borderWidth = borderWidth > MAX_OPEN_CLOSE_BUTTON_WIDTH ? MAX_OPEN_CLOSE_BUTTON_WIDTH : borderWidth;

        background.getDrawable().setMinWidth(backgroundWidth);
        background.setWidth(backgroundWidth);
        border.getDrawable().setMinWidth(borderWidth);
        border.setWidth(borderWidth);

        float height = getParent() != null ? getParent().getHeight() : getHeight();

        background.getDrawable().setMinHeight(height);
        background.setHeight(height);
        border.getDrawable().setMinHeight(height);
        border.setHeight(height);
        if (openHandlerTopOffsetCell != null) {
            openHandlerTopOffsetCell.height(height * OPEN_CLOSE_BUTTON_TOP_OFFSET_RATIO);
        }
        if (reactionBorder != null) {
            reactionBorder.getDrawable().setMinHeight(height);
        }
        if (horizontalGroup != null) {
            horizontalGroup.setHeight(height);
        }
        if (backgroundStack != null) {
            backgroundStack.setHeight(height);
        }
    }

    private Table getLocaleMenu() {
        Table footerTable = new Table();

        //Locale select box
        float footerContentWidth = getBackground().getDrawable().getMinWidth() - (2 * MAINMENU_PADDING) - APP_NAME_TEXT_PADDING_LEFT - SOUND_MUTE_BUTTON_PADDING_LEFT;
        footerTable.add(new LocaleSelectBox(SELECTBOX_LOCALE_HEIGHT)).width(footerContentWidth * SELECTBOX_LOCALE_WIDTH_RATIO).center();

        final SoundMuteButton soundMuteButton =  TablexiaSettings.getInstance().isRunningOnMobileDevice() ?
                new SoundMuteButton(SELECTBOX_LOCALE_HEIGHT) :
                new VolumeBar(SELECTBOX_LOCALE_HEIGHT);

        footerTable.add(soundMuteButton).width(footerContentWidth * SOUND_MUTE_BUTTON_WIDTH_RATIO).padLeft(SOUND_MUTE_BUTTON_PADDING_LEFT);

        //App name and version
        TablexiaLabel appName = new TablexiaLabel(
                String.format(TABLEXIA_VERSION_LABEL, TablexiaSettings.getInstance().getAppName(), TablexiaSettings.getInstance().getApplicationVersionName()),
                new TablexiaLabel.TablexiaLabelStyle(APP_NAME_TEXT_FONT, Color.BLACK));
        appName.setAlignment(APP_NAME_TEXT_ALIGN);
        appName.setWrap(true);
        footerTable.add(appName).width(footerContentWidth * APP_NAME_WIDTH_RATIO).padLeft(APP_NAME_TEXT_PADDING_LEFT);
        return footerTable;
    }

    protected Container<Table> getLocaleMenuContainer() {
        return localeMenuContainer;
    }

    @Override
    protected void sizeChanged() {
        if (background != null && border != null) {
            prepareBackgroundSize();
            super.sizeChanged();
        }
    }

    @Override
    public void dispose() {
    }

    protected void removeMoveAction() {
        if (moveAction != null) {
            removeAction(moveAction);
            moveAction = null;
        }
    }

    protected abstract MenuControlType getMenuControlType();

    protected abstract boolean isScreenPause();


//////////////////////////// MENU ITEMS

    public void initMenuItems() {
        layoutContainer.clear();
        initMenuItems(layoutContainer);
    }

    protected abstract void initMenuItems(Container<Group> layoutContainer);

    protected Group getGroupForMenuItem(IMenuItem menuItem, float width) {
        try {
            return (Group) ClassReflection.getConstructor(menuItem.getItemGroupClass(), IMenuItem.class, float.class).newInstance(menuItem, width);
        } catch (ReflectionException e) {
            Log.err(getClass(), "Cannot instantiate menu item group class: " + menuItem.getItemGroupClass(), e);
        }
        return null;
    }


//////////////////////////// MENU MOVEMENT

    public boolean isMenuOpen() {
        return getX() >= menuOpenPositionX;
    }

    public boolean isMenuClose() {
        return getX() == menuClosePositionX;
    }

    public boolean isMenuHidden() {
        return getX() <= menuHidePositionX;
    }

    private void prepareMenuPositions() {
        isOpening = false;
        isClosing = false;
        menuHidePositionX = -getWidth();
        menuClosePositionX = -(getWidth() - (border.getWidth() * MENU_CONTROLLER_SIZE_RATIO));
        menuOpenPositionX = 0;
        menuGravityPositionX = menuClosePositionX / 2;
        menuPositionY = 0;
    }

    public boolean isModal() {
        return getMenuControlType().isModal();
    }

    private void toggleMenu() {
        if (isOpening || isMenuOpen()) {
            closeMenu(true, false);
        } else {
            openMenu(true, false);
        }
    }

    void hideMenu(boolean animated) {
        if (!isMenuHidden()) {
            isReady = false;
            removeMoveAction();
            disableScroll(true);
            if (animated) {
                moveAction = Actions.sequence(
                        Actions.moveTo(menuHidePositionX, menuPositionY, MENU_MOVE_DURATION, MENU_MOVE_INTERPOLATION),
                        Actions.run(new Runnable() {

                            @Override
                            public void run() {
                                menuHidden();
                            }
                        }));
                addAction(moveAction);
            } else {
                setPosition(menuHidePositionX, menuPositionY);
                menuHidden();
            }
        }
    }

    private void menuHidden() {
        moveAction = null;
        isClosing = false;
    }

    void closeMenu(boolean animated, boolean force) {
        if ((force || !isMenuHidden()) && !isMenuClose() && !isClosing) {
            removeMoveAction();
            isOpening = false;
            isClosing = true;
            disableScroll(true);
            if (animated) {
                moveAction = Actions.sequence(
                        Actions.moveTo(menuClosePositionX, menuPositionY, MENU_MOVE_DURATION, MENU_MOVE_INTERPOLATION),
                        Actions.run(new Runnable() {

                            @Override
                            public void run() {
                                menuClosed();
                            }
                        }));
                addAction(moveAction);
            } else {
                setPosition(menuClosePositionX, menuPositionY);
                menuClosed();
            }
        }
    }

    private void menuClosed() {
        moveAction = null;
        isClosing = false;
        ApplicationBus.getInstance().publishAsync(new VolumeBar.MenuClosedEvent());
        setMenuReady(SCENARIO_STEP_READY_MENU);
    }

    void openMenu(boolean animated, boolean force) {
        if ((force || !isMenuHidden()) && !isMenuOpen() && !isOpening) {
            removeMoveAction();
            isClosing = false;
            isOpening = true;
            disableScroll(false);
            if (animated) {
                moveAction = Actions.sequence(
                        Actions.moveTo(menuOpenPositionX, menuPositionY, MENU_MOVE_DURATION, MENU_MOVE_INTERPOLATION),
                        Actions.run(new Runnable() {

                            @Override
                            public void run() {
                                menuOpened();
							}
                        }));
                addAction(moveAction);
            } else {
                setPosition(menuOpenPositionX, menuPositionY);
                menuOpened();
            }
        }
    }

    private void menuOpened() {
        moveAction = null;
        isOpening = false;
        setMenuReady(SCENARIO_STEP_OPENED_MENU);
    }

    protected void disableScroll(boolean disable){

    }

    public void doMenuAction(MenuAction menuAction, boolean animated, boolean force) {
        if (menuAction != null) {
            menuAction.doAction(this, animated, force);
        }
    }

    private void setMenuReady(String openCloseEvent) {
        if (!isReady) {
            isReady = true;
        }
        AbstractTablexiaScreen.triggerScenarioStepEvent(openCloseEvent);
    }


//////////////////////////// POSITION CHANGED

    @Override
    protected void positionChanged() {
        boolean pauseState = isScreenPause() && !isMenuHidden() && !isMenuClose();
        if (lastPauseState == null || lastPauseState != pauseState) {
            lastPauseState = pauseState;
            ApplicationBus.getInstance().post(new MenuPauseEvent(pauseState)).asynchronously();
        }

        if(isMenuOpen() && !openEventFired) {
            openEventFired = true;
            fireMenuEvent(MenuEvent.MenuEventType.Open);
        }
        else if(!isMenuOpen() && openEventFired) {
            openEventFired = false;
            fireMenuEvent(MenuEvent.MenuEventType.Close);
        }

        super.positionChanged();
    }


//////////////////////////// MENU EVENT

    private void fireMenuEvent(MenuEvent.MenuEventType menuEventType) {
        ApplicationBus.getInstance().post(new MenuEvent(this.getClass(), menuEventType)).asynchronously();
    }

    public static class MenuEvent implements  ApplicationBus.ApplicationEvent {
        public enum MenuEventType {
            Open,
            Close
        }

        private Class<? extends AbstractMenu> targetMenu;
        private MenuEventType menuEventType;

        private MenuEvent(Class<? extends AbstractMenu> targetMenu, MenuEventType menuEventType) {
            this.targetMenu = targetMenu;
            this.menuEventType = menuEventType;
        }

        public Class<? extends AbstractMenu> getTargetMenu() {
            return targetMenu;
        }

        public MenuEventType getMenuEventType() {
            return menuEventType;
        }
    }


//////////////////////////// PAUSE EVENT

    public static class MenuPauseEvent implements ApplicationBus.ApplicationEvent {

        private boolean isPause;

        private MenuPauseEvent(boolean isPause) {
            this.isPause = isPause;
        }

        public boolean isPause() {
            return isPause;
        }
    }


//////////////////////////// MENU MOVE SOUND

    private float   lastMenuX           = 0;
    private boolean openSoundPlayed     = false;
    private boolean closeSoundPlayed    = false;

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        playMenuMoveSound();
    }

    private synchronized void playMenuMoveSound() {
        float positionX = getX();
        float distance = Math.abs(lastMenuX - positionX);
        if (distance > background.getDrawable().getMinWidth() * MOVE_DISTANCE_TO_PLAY_SOUND_RATIO) {
            if (positionX > menuClosePositionX) {
                if (positionX > lastMenuX) {
                    if (!openSoundPlayed) {
                        openSoundPlayed = true;
                        closeSoundPlayed = false;
                        ApplicationExternalSoundManager.getInstance().getSound(ApplicationExternalSoundManager.MAINMENU_OPEN).play();
                    }
                } else if (positionX < lastMenuX) {
                    if (!closeSoundPlayed) {
                        closeSoundPlayed = true;
                        openSoundPlayed = false;
                        ApplicationExternalSoundManager.getInstance().getSound(ApplicationExternalSoundManager.MAINMENU_CLOSE).play();
                    }
                }
            }
            lastMenuX = positionX;
        }
    }


//////////////////////////// MENU MOVE PROGRESS

    public float getMenuOpenPercentage() {
        return 1 - Math.abs(getX() / menuClosePositionX);
    }


//////////////////////////// MENU DRAGGING

    protected void disableMenuGravity(boolean disableMenuGravity) {
        this.disableMenuGravity = disableMenuGravity;
    }

    protected boolean isDisableControl() {
        return disableControl;
    }

    private DragListener dragListener = new DragListener() {

        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if(button == Input.Buttons.RIGHT) return false;
            removeMoveAction();
            disableMenuGravity(false);
            disableControl = false;
            initialPositionX = Gdx.input.getX();

            if(getX() < menuGravityPositionX){
                disableScroll(false);
            }

            return super.touchDown(event, x, y, pointer, button);
        }

        @Override
        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            float positionX = getX();
            if (!disableMenuGravity) {
                if (positionX < menuGravityPositionX) {
                    isClosing = false;
                    isOpening = false;
                    disableScroll(true);
                    closeMenu(true, true);
                }
                if (positionX >= menuGravityPositionX) {
                    isClosing = false;
                    isOpening = false;
                    openMenu(true, true);
                }
            }
            super.touchUp(event, x, y, pointer, button);
        }

        @Override
        public void touchDragged(InputEvent event, float x, float y, int pointer) {
            if (moveAction != null) {
                isClosing = false;
                isOpening = false;
                removeMoveAction();
            }
            int inputX = Gdx.input.getX();
            int offsetX = inputX - initialPositionX;
            if (Math.abs(offsetX) > DRAG_OFFSET_TO_DISABLE_CONTROL) {
                disableControl = true;
            }
            float newPositionX = getX() + offsetX;

            if (newPositionX > menuOpenPositionX) {
                newPositionX = menuOpenPositionX;
            } else if (newPositionX < menuClosePositionX) {
                newPositionX = menuClosePositionX;
            }
            setPosition(newPositionX, menuPositionY);
            initialPositionX = inputX;
            super.touchDragged(event, x, y, pointer);
        }
    };

}
