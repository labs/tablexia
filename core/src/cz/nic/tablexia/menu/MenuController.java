/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Disposable;

import net.engio.mbassy.listener.Handler;

import java.util.HashMap;
import java.util.Map;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.event.MenuControlEvent;
import cz.nic.tablexia.bus.event.StartFullSynchronizationEvent;
import cz.nic.tablexia.menu.main.MainMenu;
import cz.nic.tablexia.menu.user.UserMenu;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;

/**
 * Menu container
 *
 * @author Matyáš Latner
 */
public class MenuController extends Group implements Disposable {

    private static final float                                      MAIN_MENU_WIDTH_RATIO = 2f/7;

    private final Group                                             screenDimmerLayer;
    private final Actor                                             screenDimmer;
    private final Map<Class<? extends AbstractMenu>, AbstractMenu>  menus;
    private AbstractMenu[]                                          menuCache;

    private boolean                                                 loadingComplete = false;

    public MenuController(float width, float height) {
        // click through the viewport
        setTouchable(Touchable.childrenOnly);

        // init event bus handlers
        ApplicationBus.getInstance().subscribe(this);

        // screen dimmer initialization
        screenDimmerLayer = new Group();
        addActor(screenDimmerLayer);
        screenDimmer = new MenuDimmer(width, height);
        screenDimmer.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                for (AbstractMenu menu : menus.values()) {
                    if (!menu.isModal()) {
                        menu.closeMenu(true, false);
                    }
                }
            }
        });

        // menus initialization
        menus = new HashMap<Class<? extends AbstractMenu>, AbstractMenu>();
        initMenu(MainMenu.class);
        initMenu(UserMenu.class);
    }


    public boolean isMenuClosed(final Class<? extends AbstractMenu> menuClass) {
        return menus.get(menuClass).isMenuClose();
    }

    public boolean isMenuHidden(final Class<? extends AbstractMenu> menuClass) {
        return menus.get(menuClass).isMenuHidden();
    }

    public void initMenu (Class<? extends AbstractMenu> menuClass) {
        try {
            AbstractMenu menu = menuClass.getConstructor(Float.class, Float.class).newInstance(getWidth() * MAIN_MENU_WIDTH_RATIO, getHeight());
            menus.put(menuClass, menu);
            cacheMenus();
            menu.initMenuItems();
            addActor(menu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cacheMenus() {
        menuCache = new AbstractMenu[menus.size()];
        int index = 0;
        for (AbstractMenu menu : menus.values()) {
            menuCache[index] = menu;
            index++;
        }
    }

    public void doMenuAction(final Class<? extends AbstractMenu> menuClass, final AbstractMenu.MenuAction menuAction, final boolean animated, final boolean force) {
        Gdx.app.postRunnable(new Runnable() {

            @Override
            public void run() {
                menus.get(menuClass).doMenuAction(menuAction, animated, force);
            }
        });
    }

    public void setDimmerColor(Color color){
        ((MenuDimmer) screenDimmer).setDimmerColor(color);
    }


//////////////////////////// LIBGDX API

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        if(screenDimmer!= null) {
            screenDimmer.setSize(getWidth(), getHeight());
        }
        if (menus != null) {
            for (AbstractMenu menu: menus.values()) {
                menu.setHeight(getHeight());
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        float openPercentage = 0;

        for(AbstractMenu menu : menuCache) {
            float menuOpenPercentage = menu.getMenuOpenPercentage();
            if (menuOpenPercentage > openPercentage) {
                openPercentage = menuOpenPercentage;
            }
        }

        if (openPercentage <= 0) {
            if (screenDimmer.getStage() != null) {
                screenDimmer.remove();
            }
        } else {
            if (screenDimmer.getStage() == null) {
                screenDimmerLayer.addActor(screenDimmer);
            }
            screenDimmer.setColor(screenDimmer.getColor().r, screenDimmer.getColor().g, screenDimmer.getColor().b, openPercentage);
        }
    }

    @Override
    public void dispose() {
        ApplicationBus.getInstance().unsubscribe(this);
        for (AbstractMenu menu: menus.values()) {
            menu.dispose();
        }
    }


//////////////////////////// MENU CONTROL HANDLERS

    @Handler
    public void handleMenuControlEvent(MenuControlEvent menuControlEvent) {
        doMenuAction(menuControlEvent.getMenu(), menuControlEvent.getMenuAction(), menuControlEvent.isAnimated(), false);
    }

	@Handler(priority = TablexiaApplication.BackButtonPressed.MENU_PRIORITY)
	public void onBackButtonDown(TablexiaApplication.BackButtonPressed event) {
		if (!isMenuClosed(MainMenu.class) && !isMenuHidden(MainMenu.class) && event.shouldProcess(TablexiaApplication.BackButtonPressed.MENU_PRIORITY)) {
			event.setProcessWithPriority(TablexiaApplication.BackButtonPressed.MENU_PRIORITY);
			doMenuAction(MainMenu.class, AbstractMenu.MenuAction.CLOSE, true, false);
		}
	}


//////////////////////////// MENU FLOW HANDLERS

    @Handler
    public void handleApplicationLoadingCompleteEvent(Tablexia.ApplicationLoadingCompleteEvent applicationLoadingCompleteEvent) {
        loadingComplete = true;
        screenVisibleEvent(null);
    }

    @Handler
    public void handleSelectedUserEvent(TablexiaSettings.SelectedUserEvent selectedUserEvent) {
        if (!selectedUserEvent.isUserSelected()) {
            doMenuAction(MainMenu.class, AbstractMenu.MenuAction.HIDE, true, true);
        }
    }

    @Handler
    public void screenVisibleEvent(AbstractTablexiaScreen.ScreenVisibleEvent screenVisibleEvent) {
        if (TablexiaSettings.LOADER_SCREEN.equals(TablexiaApplication.getActualScreenClass())) {
            if (loadingComplete && TablexiaSettings.getInstance().getSelectedUser() == null) {
                ApplicationBus.getInstance().publish(new StartFullSynchronizationEvent(UserDAO.selectActiveUsers()));
				doMenuAction(UserMenu.class, AbstractMenu.MenuAction.OPEN, true, true);
            }
        } else {
            if (TablexiaSettings.getInstance().getSelectedUser() != null) {
                doMenuAction(MainMenu.class, AbstractMenu.MenuAction.SHOW, true, true);
            }
        }
    }

    public static class UserLoginMenuOpenedEvent implements ApplicationBus.ApplicationEvent {
        public UserLoginMenuOpenedEvent() {}
    }
}
