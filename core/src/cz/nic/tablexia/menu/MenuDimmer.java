/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import cz.nic.tablexia.loader.TablexiaAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;

/**
 * MainMenu dimmer
 *
 * @author Matyáš Latner
 */
public class MenuDimmer extends Image {

    public static final  Color DIMMER_COLOR               = TablexiaAtlasManager.COLOR_OVERLAY;

    public MenuDimmer(float width, float height) {
        super(ApplicationAtlasManager.getInstance().getColorTextureRegion(DIMMER_COLOR));
        setSize(width, height);
    }

    public void setDimmerColor(Color color){
        TextureRegion textureRegion = ApplicationAtlasManager.getInstance().getColorTextureRegion(color);
        setDrawable(new TextureRegionDrawable(textureRegion));

    }
}
