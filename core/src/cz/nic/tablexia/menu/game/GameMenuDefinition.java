/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.game;

import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.INumberComparator;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.IMenuItem;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.shared.model.definitions.INumberedDefinition;

/**
 * Created by Matyáš Latner.
 */
public enum GameMenuDefinition implements ApplicationBus.ApplicationEvent, IMenuItem, INumberedDefinition {

    ROBBERY         (GameDefinition.ROBBERY,            ApplicationTextManager.ApplicationTextsAssets.GAME_ROBBERY_TITLE, 			ApplicationTextManager.ApplicationTextsAssets.GAME_ROBBERY_DESCRIPTION),
    PURSUIT         (GameDefinition.PURSUIT,            ApplicationTextManager.ApplicationTextsAssets.GAME_PURSUIT_TITLE, 			ApplicationTextManager.ApplicationTextsAssets.GAME_PURSUIT_DESCRIPTION),
    KIDNAPPING      (GameDefinition.KIDNAPPING,         ApplicationTextManager.ApplicationTextsAssets.GAME_KIDNAPPING_TITLE, 		ApplicationTextManager.ApplicationTextsAssets.GAME_KIDNAPPING_DESCRIPTION),
    NIGHT_WATCH     (GameDefinition.NIGHT_WATCH,        ApplicationTextManager.ApplicationTextsAssets.GAME_NIGHT_WATCH_TITLE, 		ApplicationTextManager.ApplicationTextsAssets.GAME_NIGHT_WATCH_DESCRIPTION),
    SHOOTING_RANGE  (GameDefinition.SHOOTING_RANGE,     ApplicationTextManager.ApplicationTextsAssets.GAME_SHOOTING_RANGE_TITLE, 	ApplicationTextManager.ApplicationTextsAssets.GAME_SHOOTING_RANGE_DESCRIPTION),
    IN_THE_DARKNESS (GameDefinition.IN_THE_DARKNESS,    ApplicationTextManager.ApplicationTextsAssets.GAME_IN_THE_DARKNESS_TITLE,	ApplicationTextManager.ApplicationTextsAssets.GAME_IN_THE_DARKNESS_DESCRIPTION),
    RUNES           (GameDefinition.RUNES,              ApplicationTextManager.ApplicationTextsAssets.GAME_RUNES_TITLE,             ApplicationTextManager.ApplicationTextsAssets.GAME_RUNES_DESCRIPTION),
    CRIME_SCENE     (GameDefinition.CRIME_SCENE,        ApplicationTextManager.ApplicationTextsAssets.GAME_CRIME_SCENE_TITLE,	    ApplicationTextManager.ApplicationTextsAssets.GAME_CRIME_SCENE_DESCRIPTION),
    PROTOCOL        (GameDefinition.PROTOCOL,           ApplicationTextManager.ApplicationTextsAssets.GAME_PROTOCOL_TITLE,          ApplicationTextManager.ApplicationTextsAssets.GAME_PROTOCOL_DESCRIPTION),
    SAFE            (GameDefinition.SAFE,               ApplicationTextManager.ApplicationTextsAssets.GAME_SAFE_TITLE,              ApplicationTextManager.ApplicationTextsAssets.GAME_SAFE_DESCRIPTION),
    ON_THE_TRAIL    (GameDefinition.ON_THE_TRAIL,       ApplicationTextManager.ApplicationTextsAssets.GAME_ON_THE_TRAIL_TITLE,      ApplicationTextManager.ApplicationTextsAssets.GAME_ON_THE_TRAIL_DESCRIPTION),
    MEMORY_GAME     (GameDefinition.MEMORY_GAME,        ApplicationTextManager.ApplicationTextsAssets.MEMORY_GAME_TITLE,            ApplicationTextManager.ApplicationTextsAssets.MEMORY_GAME_DESCRIPTION),
    ATTENTION_GAME  (GameDefinition.ATTENTION_GAME,     ApplicationTextManager.ApplicationTextsAssets.ATTENTION_GAME_TITLE,         ApplicationTextManager.ApplicationTextsAssets.ATTENTION_GAME_DESCRIPTION);


    private static final Class<GameMenuItemGroup> MENU_ITEM_GROUP_CLASS = GameMenuItemGroup.class;

    private GameDefinition  gameDefinition;
    private String          titleKey;
    private String          descriptionKey;

    GameMenuDefinition(GameDefinition gameDefinition, String titleKey, String descriptionKey){
        this.gameDefinition = gameDefinition;
        this.titleKey = titleKey;
        this.descriptionKey = descriptionKey;
    }

    public GameDefinition getGameDefinition() {
        return gameDefinition;
    }

    @Override
    public String getTitle() {
        return ApplicationTextManager.getInstance().getResult().get(titleKey);
    }

    @Override
    public String getDescription() {
        return ApplicationTextManager.getInstance().getResult().get(descriptionKey);
    }

    @Override
    public String[] getIcons() {
        return null;
    }

    @Override
    public Class<? extends Group> getItemGroupClass() {
        return MENU_ITEM_GROUP_CLASS;
    }

    @Override
    public AbstractMenu.MenuAction getMenuAction() {
        return AbstractMenu.MenuAction.CLOSE;
    }

    @Override
    public IMenuItem[] getSubmenu() {
        return null;
    }

    @Override
    public void performAction() {
        ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(GamePageScreen.class, TablexiaApplication.ScreenTransaction.FADE, null, gameDefinition));
    }

    public static GameMenuDefinition getGameMenuDefinitionForGameDefinition(GameDefinition gameDefinition) {
        for (GameMenuDefinition gameMenuDefinition : GameMenuDefinition.values()) {
            if (gameDefinition == gameMenuDefinition.gameDefinition) {
                return gameMenuDefinition;
            }
        }
        return null;
    }

    public static List<GameMenuDefinition> getSortedGameMenuDefinitionList(){
        List<GameMenuDefinition> sorted = Arrays.asList(values());
        Collections.sort(sorted, new INumberComparator());
        return sorted;
    }

    @Override
    public int number() {
        return gameDefinition.number();
    }
}
