/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import net.engio.mbassy.listener.Handler;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.menu.IMenuItem;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaProgressBar;

/**
 * Created by Matyáš Latner.
 */
public class GameMenuItemGroup extends Container<Table> {

    private static final ApplicationFontManager.FontType     TITLE_FONT_TYPE         = ApplicationFontManager.FontType.BOLD_18;
    private static final Color                               TITLE_FONT_COLOR        = new Color(0.325f, 0.278f, 0.255f, 1);

    private static final ApplicationFontManager.FontType     DESCRIPTION_FONT_TYPE   = ApplicationFontManager.FontType.REGULAR_10;
    private static final Color                               DESCRIPTION_FONT_COLOR  = new Color(0.325f, 0.278f, 0.255f, 1);

    private static final Color  HIGHLITED_COLOR             = new Color(0.780f, 0.698f, 0.600f, 1);
    private static final Color  HIGHLITER_COLOR             = new Color(0.647f, 0.22f, 0.22f, 1);
    private static final float  LABELS_PADDING_DEFAULT      = 6f;
    private static final float  ARROW_PADING_RIGHT          = 15f;
    private static final float  CONTAINER_BOTTOM_PADDING    = 4f;
    private static final float  GROUP_TRIM                  = 60f;
    private static final float  GROUP_PAD_RIGHT             = 10f;
    private static final float  GROUP_PAD_LEFT              = 10f;
    private static final float  GAME_ITEM_PADDING           = 8f;

    private final Table                 layoutContainer;
    private final Image                 arrowIcon;
    private final Image                 restartIcon;
    private final Cell<Image>           iconCell;
    private final Image                 highliter;
    private       NinePatchDrawable     background;
    private       TextureRegionDrawable highlitedBackground;
    private       TablexiaProgressBar   tablexiaProgressBar;

    private Class<? extends AbstractTablexiaGame> abstractTablexiaGameClass;
    private GameDefinition gameDefinition;

    public GameMenuItemGroup(IMenuItem menuItem, float width) {
        this.abstractTablexiaGameClass = ((GameMenuDefinition) menuItem).getGameDefinition().getScreenClass();
        this.gameDefinition = GameDefinition.getGameDefinitionForClass(abstractTablexiaGameClass);

        layoutContainer = new Table();
        layoutContainer.setTouchable(Touchable.enabled);
        layoutContainer.addListener(new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                setPressed();
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                setUnpressed();
                super.touchUp(event, x, y, pointer, button);
            }
        });

        background          = new NinePatchDrawable(ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.MAINMENU_CHILDITEM_BACKGROUND));
        highlitedBackground = new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getColorTextureRegion(HIGHLITED_COLOR));

        setActor(layoutContainer);
        padBottom(CONTAINER_BOTTOM_PADDING);

        // highliter
        highliter = new Image(ApplicationAtlasManager.getInstance().getColorTexture(HIGHLITER_COLOR));
        layoutContainer.add(highliter);

        // texts
        TablexiaLabel titleLabel = new TablexiaLabel(menuItem.getTitle(), new TablexiaLabel.TablexiaLabelStyle(TITLE_FONT_TYPE, TITLE_FONT_COLOR));

        TablexiaLabel descriptionLabel = new TablexiaLabel(menuItem.getDescription(), new TablexiaLabel.TablexiaLabelStyle(DESCRIPTION_FONT_TYPE, DESCRIPTION_FONT_COLOR));

        tablexiaProgressBar = new TablexiaProgressBar(ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.PROGRESS_BAR_BACKGROUND_9PATCH),
                ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.PROGRESS_BAR_FILL));
        if (TablexiaSettings.getInstance().getSelectedUser()!=null){
            updateProgressBars();
        }
        
        Table labelsContainer = new Table();
        labelsContainer.add(titleLabel).align(Align.left);
        labelsContainer.row();
        labelsContainer.add(descriptionLabel).align(Align.left);
        labelsContainer.align(Align.left);

        //TODO - Check this and make it better!
        float cellHeight =  titleLabel.getTablexiaLabelStyle().getFontType().getSize() +
                            descriptionLabel.getTablexiaLabelStyle().getFontType().getSize() +
                            2 * LABELS_PADDING_DEFAULT;

        layoutContainer.add(labelsContainer).height(cellHeight + GAME_ITEM_PADDING).width(width - GROUP_TRIM).padRight(GROUP_PAD_RIGHT).padLeft(GROUP_PAD_LEFT);

        // arrow
        arrowIcon = new Image(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.MAINMENU_CHILDITEM_ARROW));
        restartIcon = new Image(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.MAINMENU_CHILDITEM_RESTART));
        iconCell = layoutContainer.add();
        iconCell.expandX().align(Align.right).padRight(ARROW_PADING_RIGHT);
        changeIconToArrow();
        
        labelsContainer.row();
        labelsContainer.add(tablexiaProgressBar).align(Align.left);

        // highliter size
        highliter.getDrawable().setMinWidth(5);
        highliter.getDrawable().setMinHeight(layoutContainer.getMinHeight());
        unhighlite();

        setUnpressed();
        ApplicationBus.getInstance().subscribe(this);
    }

    private void highlite() {
        highliter.setVisible(true);
    }

    private void unhighlite() {
        highliter.setVisible(false);
    }

    private void changeIconToArrow() {
        restartIcon.remove();
        iconCell.setActor(arrowIcon);
    }

    private void changeIconToRestart() {
        arrowIcon.remove();
        iconCell.setActor(restartIcon);
    }

    private void setUnpressed() {
        layoutContainer.setBackground(background);
    }

    private void setPressed() {
        layoutContainer.setBackground(highlitedBackground);
    }
    
    private void updateProgressBars(){
        UserRankManager.RankProgress rankProgress = UserRankManager.getInstance().getGameRankProgress(TablexiaSettings.getInstance().getSelectedUser(), gameDefinition);
        tablexiaProgressBar.setPercent(rankProgress.getPercentDone());
    }

    @Handler
    public void onScreenChangedEvent(TablexiaApplication.ScreenChangedEvent screenChangedEvent) {
        if (screenChangedEvent.getScreenClass() == abstractTablexiaGameClass) {
            highlite();
            changeIconToRestart();
        } else {
            unhighlite();
            changeIconToArrow();
        }
    }

    @Handler
    public void handleSelectedUserEvent(TablexiaSettings.SelectedUserEvent selectedUserEvent) {
        if (selectedUserEvent.isUserSelected()) {
            updateProgressBars();
        }
    }
    
    @Handler 
    public void handleRefreshMainMenuProgressBarsEvent(RefreshMainMenuProgressBarsEvent progressBarsEvent){
        if (TablexiaSettings.getInstance().getSelectedUser()!=null){
            updateProgressBars();
        }
    }
    
    public static class RefreshMainMenuProgressBarsEvent implements ApplicationBus.ApplicationEvent {
        
    }

}
