/*
 *
 *  * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.menu.main.sound;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import net.engio.mbassy.listener.Handler;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.button.TablexiaButton;


public class VolumeBar extends SoundMuteButton {

    private float height;
    private Image volumeBarImage;
    private Image volumeBarPointImage;
    private Image background;
    private Group volumeBarGroup;
    private boolean muteDisabled = false;

    private TablexiaButton volumeUpButton;
    private TablexiaButton volumeDownButton;

    private boolean sliderShown = false;

    private static final int DEFAULT_VOLUME_BUTTON_SIZE = 25;
    private static final int DEFAULT_VERTICAL_PADDING = 15;

    private static final float UNMUTE_VOLUME_VALUE = 0.25f;

    public VolumeBar(float height) {
        super(height);
        this.height = height;
        ApplicationBus.getInstance().subscribe(this);
        initSlider();
    }

    private void initSlider() {

        volumeBarGroup = new Group();
        background = new Image(ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND));
        volumeBarGroup.addActor(background);

        volumeBarImage = new Image(new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.MAINMENU_VOLUME_BAR_BACKGROUND)));
        volumeBarPointImage = new Image(new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.MAINMENU_VOLUME_BAR_POINT)));

        volumeBarImage.setSize(volumeBarImage.getWidth() * 0.25f, volumeBarImage.getHeight() * 0.25f);
        volumeBarPointImage.setSize(volumeBarPointImage.getWidth() * 0.25f, volumeBarPointImage.getHeight() * 0.25f);

        volumeUpButton = new TablexiaButton("+", false, ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUBUTTON_BACKGROUND),
                ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND),
                ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND),
                ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND));


        volumeUpButton.setSize(DEFAULT_VOLUME_BUTTON_SIZE, DEFAULT_VOLUME_BUTTON_SIZE);

        volumeDownButton = new TablexiaButton("-", false, ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUBUTTON_BACKGROUND),
                ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND),
                ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND),
                ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND));

        volumeDownButton.setSize(DEFAULT_VOLUME_BUTTON_SIZE, DEFAULT_VOLUME_BUTTON_SIZE);


        volumeUpButton.setInputListener(new ClickListenerWithSound() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TablexiaSettings.getInstance().increaseSoundLevel();
                updateVolumeBar();
            }
        });

        volumeDownButton.setInputListener(new ClickListenerWithSound() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                TablexiaSettings.getInstance().decreaseSoundLevel();
                updateVolumeBar();
            }
        });


        volumeBarGroup.addActor(volumeBarImage);
        volumeBarGroup.addActor(volumeBarPointImage);

        volumeBarGroup.addActor(volumeUpButton);
        volumeBarGroup.addActor(volumeDownButton);


        volumeBarGroup.setSize(height, volumeBarImage.getHeight() + 40 + volumeUpButton.getHeight() + volumeDownButton.getHeight());
        background.setSize(volumeBarGroup.getWidth(), volumeBarGroup.getHeight());

        volumeBarGroup.setPosition((getWidth() / 2 - volumeBarGroup.getWidth() / 2), getHeight() + 10);
        volumeBarImage.setPosition(volumeBarGroup.getWidth() / 2 - volumeBarImage.getWidth() / 2, volumeDownButton.getTop() + DEFAULT_VERTICAL_PADDING);
        volumeBarPointImage.setX(volumeBarImage.getX() + (volumeBarImage.getWidth() / 2 - volumeBarPointImage.getWidth() / 2));

        volumeUpButton.setPosition(volumeBarGroup.getWidth() / 2 - volumeUpButton.getWidth() / 2, volumeBarImage.getTop() + DEFAULT_VERTICAL_PADDING);
        volumeDownButton.setPosition(volumeBarGroup.getWidth() / 2 - volumeDownButton.getWidth() / 2, 5);

        updateVolumeBar();

    }

    public void updateVolumeBar() {
        volumeBarPointImage.setY(volumeBarImage.getY() - (volumeBarPointImage.getHeight() / 2) + (TablexiaSettings.getInstance().getSoundLevel() * volumeBarImage.getHeight()));

        volumeUpButton.setEnabled(TablexiaSettings.getInstance().getSoundLevel() != 1f);
        volumeDownButton.setEnabled(TablexiaSettings.getInstance().getSoundLevel() != 0f);

        if (volumeDownButton != null && muteDisabled && TablexiaSettings.getInstance().getSoundLevel() == UNMUTE_VOLUME_VALUE) {
            volumeDownButton.setDisabled();
        }
    }


    private void buttonClickAction() {
        if (!sliderShown) {
            updateVolumeBar();
            addActor(volumeBarGroup);
            sliderShown = true;
        } else {
            removeActor(volumeBarGroup);
            sliderShown = false;
        }
    }

    @Override
    protected void setDisabled() {
        muteDisabled = true;
        updateVolumeBar();
    }

    @Override
    protected void setEnabled() {
        muteDisabled = false;
        updateVolumeBar();
    }

    @Override
    protected ClickListener getListener() {
        return new ClickListenerWithSound() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (event.getListenerActor().equals(buttonGroup)) {
                    buttonClickAction();
                }
                super.clicked(event, x, y);
            }
        };
    }

    @Handler
    public void handleMenuControlEvent(MenuClosedEvent menuClosedEvent) {
        if (sliderShown) {
            removeActor(volumeBarGroup);
            sliderShown = false;
        }
    }

    @Handler
    public void handleUnmuteEvent(UnmuteEvent unmuteEvent) {
        TablexiaSettings.getInstance().setSoundLevel(UNMUTE_VOLUME_VALUE);
        updateVolumeBar();
    }

    public static class MenuClosedEvent implements ApplicationBus.ApplicationEvent {
        public MenuClosedEvent() {
        }
    }

    public static class UnmuteEvent implements ApplicationBus.ApplicationEvent {
        public UnmuteEvent() {
        }
    }
}
