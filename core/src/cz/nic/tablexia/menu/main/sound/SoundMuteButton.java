/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.main.sound;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import net.engio.mbassy.listener.Handler;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;

/**
 * Created by drahomir on 7/15/16.
 */
public class SoundMuteButton extends Group {
    private static final float DEFAULT_OPACITY  = 1.0f;
    private static final float DISABLED_OPACITY = 0.25f;

    private static final float ICON_SIZE_SCALE  = 0.6f;

    private TextureRegionDrawable buttonIconPressed;
    private TextureRegionDrawable buttonIconUnpressed;

    private Image   buttonBackground;
    private Image   buttonIcon;
    private Image   disabledOverlay;

    protected Group   buttonGroup;
    private ClickListener clickListener;

    public SoundMuteButton(float height) {
        setSize(height, height);
        clickListener = getListener();
        initialize();
    }

    private void initialize() {
        buttonGroup = new Group();

        ApplicationBus.getInstance().subscribe(this);

        buttonIconPressed   = new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.MAINMENU_ICON_SOUNDS_PRESSED));
        buttonIconUnpressed = new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.MAINMENU_ICON_SOUNDS_UNPRESSED));

        buttonBackground = new Image(ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND));
        buttonBackground.setSize(getWidth(), getHeight());
        buttonGroup.addActor(buttonBackground);

        buttonIcon = new Image();
        buttonIcon.setSize(getWidth() * ICON_SIZE_SCALE, getHeight() * ICON_SIZE_SCALE);
        buttonIcon.setPosition((getWidth() - buttonIcon.getWidth()) / 2f, (getHeight() - buttonIcon.getHeight()) / 2);
        buttonGroup.addActor(buttonIcon);

        disabledOverlay = new Image(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.MAINMENU_ICON_SOUNDS_DISABLED));
        disabledOverlay.setPosition(0, 0);
        disabledOverlay.setSize(getWidth(), getHeight());
        disabledOverlay.setVisible(false);
        buttonGroup.addActor(disabledOverlay);

        buttonGroup.addListener(clickListener);

        addActor(buttonGroup);

        if(TablexiaSettings.getInstance().isSoundMuted()) setMuted();
        else setUnmuted();
    }

    protected void setMuted() {
        buttonIcon.setDrawable(buttonIconPressed);
    }

    protected void setUnmuted() {
        buttonIcon.setDrawable(buttonIconUnpressed);
    }

    protected void setDisabled() {
        removeListener(clickListener);
        disabledOverlay.setVisible(true);
        setColor(getColor().r, getColor().g, getColor().b, DISABLED_OPACITY);
    }

    protected void setEnabled() {
        addListener(clickListener);
        disabledOverlay.setVisible(false);
        setColor(getColor().r, getColor().g, getColor().b, DEFAULT_OPACITY);
    }

    @Handler
    public void handleSoundMuteEvent(TablexiaSettings.SoundMuteEvent soundMuteEvent) {
        if(soundMuteEvent.isSoundMuted()) setMuted();
        else setUnmuted();
    }

    @Handler
    public void handleScreenChangedEvent(TablexiaApplication.ScreenChangedEvent screenChangedEvent) {
        if(TablexiaApplication.getActualScreen().isSoundMandatory())
            setDisabled();
        else
            setEnabled();
    }

    protected ClickListener getListener(){
      return new ClickListenerWithSound() {
            @Override
            public void onClick(InputEvent event, float x, float y) {
                TablexiaSettings.getInstance().toggleSoundMute();
            }
        };
    }
}
