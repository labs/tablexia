/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.main.locale;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.util.ui.AbstractTablexiaSelectBox;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by Matyáš Latner.
 */
public class LocaleSelectBox extends AbstractTablexiaSelectBox<LocaleSelectBox.LocaleItem> {

    private static final ApplicationFontManager.FontType        LOCALE_TEXT_FONT    = ApplicationFontManager.FontType.REGULAR_14;
    private static final Color                                  LOCALE_TEXT_COLOR   = new Color(0.098f, 0.086f, 0.075f, 1f);
    private static final int                                    LOCALE_TEXT_ALIGN   = Align.center;

    public class LocaleItem extends Actor {
        private final Image                             background;
        private final String                            text;
        private final GlyphLayout                       glyphLayout;
        private final TablexiaSettings.LocaleDefinition localeDefinition;
        private final TablexiaLabel                     textLabel;

        public LocaleItem(TablexiaSettings.LocaleDefinition localeDefinition) {
            this.localeDefinition = localeDefinition;
            background  = new Image(ApplicationAtlasManager.getInstance().getPatch(ApplicationAtlasManager.USERMENU_MENUITEM_BACKGROUND));
            text        = ApplicationTextManager.getInstance().getText(localeDefinition.getDescriptionKey());
            glyphLayout = new GlyphLayout();

            textLabel   = new TablexiaLabel(text, new TablexiaLabel.TablexiaLabelStyle(LOCALE_TEXT_FONT, LOCALE_TEXT_COLOR));
            textLabel.setAlignment(LOCALE_TEXT_ALIGN);
        }

        public TablexiaSettings.LocaleDefinition getLocaleDefinition() {
            return localeDefinition;
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            LOCALE_TEXT_COLOR.a = parentAlpha;

            background.setBounds(getX(), getY(), getWidth(), getHeight());
            background.draw(batch, parentAlpha);

            textLabel.setBounds(getX(), getY(), getWidth(), getHeight());
            textLabel.draw(batch, parentAlpha);
        }
    }

    private final ChangeListener changeListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            TablexiaSettings.getInstance().setLocale(((LocaleSelectBox)event.getTarget()).getSelected().getLocaleDefinition());
        }
    };

    public LocaleSelectBox(float itemHeight) {
        super(itemHeight);
        ApplicationBus.getInstance().subscribe(this);
        setCheckValidity(false);
        prepareLocales();
    }

    private void prepareLocales() {
        removeCaptureListener(changeListener);
        LocaleItem selectedLocaleItem = null;
        List<LocaleItem> localeItems = new ArrayList<>();

        //is language selected manually by user, or used system settings?
        TablexiaSettings.LocaleDefinition selectedLocaleDefinition = TablexiaSettings.getInstance().getLocaleDefinition() == TablexiaSettings.LocaleDefinition.SYSTEM ?
                TablexiaSettings.getInstance().getSystemLocale() : TablexiaSettings.getInstance().getLocaleDefinition();

        for (TablexiaSettings.LocaleDefinition localeDefinition : TablexiaSettings.LocaleDefinition.getSelectableLocaleDefinitions()) {
            LocaleItem localeItem = new LocaleItem(localeDefinition);
            localeItems.add(localeItem);
            if(localeDefinition == selectedLocaleDefinition) selectedLocaleItem = localeItem;
        }
        localeItems.remove(selectedLocaleItem);
        setItems(localeItems.toArray(new LocaleItem[]{}));
        setSelected(selectedLocaleItem);
        addCaptureListener(changeListener);
    }

    @Override
    public boolean onSelectBoxItemSelected(LocaleItem item) {
        getSelectBoxList().changeToSelected();
        prepareLocales();
        return true;
    }

    @Override
    protected void drawSelectedItem(Batch batch, float parentAlpha, LocaleItem selected, float width, float height) {
        selected.setBounds(getX(), getY(), width, height);
        selected.draw(batch, parentAlpha);
    }

    @Override
    protected void drawListItem(Batch batch, float parentAlpha, LocaleItem item, float x, float y, float width, float height) {
        item.setBounds(x, y, width, height);
        item.draw(batch, parentAlpha);
    }
}
