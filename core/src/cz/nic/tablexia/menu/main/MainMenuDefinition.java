/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.main;

import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.ApplicationBus.ApplicationEvent;
import cz.nic.tablexia.bus.event.SubMenuControlEvent;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.IMenuItem;
import cz.nic.tablexia.menu.game.GameMenuDefinition;
import cz.nic.tablexia.screen.about.AboutScreen;
import cz.nic.tablexia.screen.encyclopedia.EncyclopediaScreen;
import cz.nic.tablexia.screen.gamemenu.OfficeMenuScreen;
import cz.nic.tablexia.screen.halloffame.HallOfFameScreen;
import cz.nic.tablexia.screen.profile.ProfileScreen;
import cz.nic.tablexia.screen.statistics.StatisticsScreen;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;


public enum MainMenuDefinition implements IMenuItem {

	GAMES				(false, new String[]{ApplicationAtlasManager.MAINMENU_ICON_GAMES_UNPRESSED, ApplicationAtlasManager.MAINMENU_ICON_GAMES_PRESSED}, ApplicationTextManager.ApplicationTextsAssets.MAINMENU_GAMES, GameMenuDefinition.getSortedGameMenuDefinitionList().toArray(new IMenuItem[0]), null, null) {
		@Override
		public void performAction() {
			ApplicationBus.getInstance().post(new SubMenuControlEvent(MainMenu.class, this, SubMenuControlEvent.SubMenuAction.TOGGLE)).asynchronously();
		}
	},
	OFFICE				(false, new String[]{ApplicationAtlasManager.MAINMENU_ICON_OFFICE_UNPRESSED,		ApplicationAtlasManager.MAINMENU_ICON_OFFICE_PRESSED}, 			ApplicationTextManager.ApplicationTextsAssets.MAINMENU_OFFICE,			null, AbstractMenu.MenuAction.CLOSE,	new Tablexia.ChangeScreenEvent(OfficeMenuScreen.class,		TablexiaApplication.ScreenTransaction.FADE)),
	HALL_OF_FAME		(false, new String[]{ApplicationAtlasManager.MAINMENU_ICON_HALLOFFAME_UNPRESSED,    ApplicationAtlasManager.MAINMENU_ICON_HALLOFFAME_PRESSED},      ApplicationTextManager.ApplicationTextsAssets.MAINMENU_HALLOFFAME,      null, AbstractMenu.MenuAction.CLOSE,    new Tablexia.ChangeScreenEvent(HallOfFameScreen.class,      TablexiaApplication.ScreenTransaction.FADE)),
	STATISTICS			(false, new String[]{ApplicationAtlasManager.MAINMENU_ICON_STATISTICS_UNPRESSED,    ApplicationAtlasManager.MAINMENU_ICON_STATISTICS_PRESSED},      ApplicationTextManager.ApplicationTextsAssets.MAINMENU_STATISTICS,      null, AbstractMenu.MenuAction.CLOSE,    new Tablexia.ChangeScreenEvent(StatisticsScreen.class,      TablexiaApplication.ScreenTransaction.FADE)),
	ENCYCLOPEDIA		(false, new String[]{ApplicationAtlasManager.MAINMENU_ICON_ENCYCLOPEDIA_UNPRESSED,  ApplicationAtlasManager.MAINMENU_ICON_ENCYCLOPEDIA_PRESSED},    ApplicationTextManager.ApplicationTextsAssets.MAINMENU_ENCYCLOPEDIA,    null, AbstractMenu.MenuAction.CLOSE,    new Tablexia.ChangeScreenEvent(EncyclopediaScreen.class,    TablexiaApplication.ScreenTransaction.FADE)),
	ABOUT_APPLICATION	(false, new String[]{ApplicationAtlasManager.MAINMENU_ICON_ABOUT_UNPRESSED,         ApplicationAtlasManager.MAINMENU_ICON_ABOUT_PRESSED},           ApplicationTextManager.ApplicationTextsAssets.MAINMENU_ABOUT,           null, AbstractMenu.MenuAction.CLOSE,    new Tablexia.ChangeScreenEvent(AboutScreen.class,           TablexiaApplication.ScreenTransaction.FADE)),
	PROFILE				(false, new String[]{ApplicationAtlasManager.MAINMENU_ICON_PROFILE_UNPRESSED,       ApplicationAtlasManager.MAINMENU_ICON_PROFILE_PRESSED},         ApplicationTextManager.ApplicationTextsAssets.MAINMENU_PROFILE,         null, AbstractMenu.MenuAction.CLOSE,    new Tablexia.ChangeScreenEvent(ProfileScreen.class,         TablexiaApplication.ScreenTransaction.FADE)),
	LOGOUT				(false, new String[]{ApplicationAtlasManager.MAINMENU_ICON_LOGOUT_UNPRESSED,        ApplicationAtlasManager.MAINMENU_ICON_LOGOUT_PRESSED},          ApplicationTextManager.ApplicationTextsAssets.MAINMENU_LOGOUT,          null, AbstractMenu.MenuAction.CLOSE,    null) {
		@Override
		public void performAction() {
			Runnable logoutAction = new Runnable() {
				@Override
				public void run() {
					TablexiaSettings.getInstance().changeUser(null, new TablexiaSettings.LogoutDialogListener() {
						@Override
						public void onLogoutAccepted() {
							ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(TablexiaSettings.LOADER_SCREEN, TablexiaApplication.ScreenTransaction.FADE)).asynchronously();
						}

						@Override
						public void onLogoutCanceled() {
							Tablexia.getActualScreen().resetExitDialogState();
						}
					});
				}
			};

			if(!Tablexia.getActualScreen().handleScreenExit(null, logoutAction)) logoutAction.run();
        }
	},
	EXIT				(true, new String[]{ApplicationAtlasManager.MAINMENU_ICON_EXIT_UNPRESSED,        	ApplicationAtlasManager.MAINMENU_ICON_EXIT_PRESSED},          	ApplicationTextManager.ApplicationTextsAssets.EXIT_APP_BUTTON,          null, AbstractMenu.MenuAction.CLOSE,    null) {
		@Override
		public void performAction() {
			TablexiaComponentDialog exitDialog = TablexiaComponentDialogFactory.getInstance().createExitDialog();
			exitDialog.show(TablexiaComponentDialogFactory.WARNING_DIALOG_WIDTH, TablexiaComponentDialogFactory.WARNING_DIALOG_HEIGHT);
		}
	};

	private static final Class<MainMenuItemGroup> MENU_ITEM_GROUP_CLASS = MainMenuItemGroup.class;

	private boolean desktopOnly;
	private String[] icons;
	private String menuTextKey;
	private IMenuItem[] submenu;
	private AbstractMenu.MenuAction menuAction;
	private ApplicationEvent action;

	MainMenuDefinition(boolean desktopOnly, String[] icons, String nameResource, IMenuItem[] submenu, AbstractMenu.MenuAction menuAction, ApplicationEvent action) {
		this.desktopOnly = desktopOnly;
		this.icons = icons;
		this.menuTextKey = nameResource;
		this.submenu = submenu;
		this.menuAction = menuAction;
		this.action = action;
	}

    public ApplicationEvent getAction() {
		return action;
	}

	@Override
	public String getTitle() {
		return ApplicationTextManager.getInstance().getText(menuTextKey);
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public Class<? extends Group> getItemGroupClass() {
		return MENU_ITEM_GROUP_CLASS;
	}

	@Override
	public void performAction() {
		ApplicationBus.getInstance().publishAsync(this.getAction());
	}

	public String[] getIcons() {
		return icons;
	}

	public IMenuItem[] getSubmenu() {
		return submenu;
	}

	@Override
	public AbstractMenu.MenuAction getMenuAction() {
		return menuAction;
	}

    public static List<MainMenuDefinition> getItemsForMenu() {
        List<MainMenuDefinition> mainMenu = new ArrayList<MainMenuDefinition>();
        for (MainMenuDefinition mainMenuDefinition : MainMenuDefinition.values()) {
			if(!mainMenuDefinition.desktopOnly || TablexiaSettings.getInstance().getPlatform() == TablexiaSettings.Platform.DESKTOP)
                mainMenu.add(mainMenuDefinition);
            }
        return mainMenu;
    }
}
