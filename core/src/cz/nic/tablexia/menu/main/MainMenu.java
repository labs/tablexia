/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.main;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import net.engio.mbassy.listener.Handler;

import java.util.HashMap;
import java.util.Map;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.event.SubMenuControlEvent;
import cz.nic.tablexia.loader.application.ApplicationInternalSoundManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.IMenuItem;
import cz.nic.tablexia.menu.main.user.UserSelectBox;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.scrollpane.ScrollPaneWithBars;

/**
 * MainMenu container
 *
 * @author Matyáš Latner
 */
public class MainMenu extends AbstractMenu {
    private static final MenuControlType                    MENU_CONTROL_TYPE               = MenuControlType.MANUAL;
    private static final boolean                            MENU_SCREEN_PAUSE               = true;

    private static final int                                SELECTBOX_USER_HEIGHT           = 70;
    private static final float                              MENU_PADDING                    = 80;
    private static final float                              USER_SELECT_BOX_PADDING         = 25;

    private Map<IMenuItem, Group>   subMenus = new HashMap<IMenuItem, Group>();
    private VerticalGroup mainMenuVerticalGroup;
    private ScrollPane scrollPane;
    private Container scrollPaneContainer;

    public MainMenu(Float width, Float height) {
        super(width, height);
        ApplicationBus.getInstance().subscribe(this);
    }

    @Override
    protected boolean isScreenPause() {
        adoptScrollPaneContainerSize();
        return MENU_SCREEN_PAUSE;
    }

    @Override
    protected void sizeChanged() {
        adoptScrollPaneContainerSize();
        super.sizeChanged();
    }

    private void adoptScrollPaneContainerSize() {
        if(getStage() != null) { //need to resize left menu properly (not working in sizeChanged() method when first initiating)
            float scrollPaneHeight = TablexiaSettings.getViewportHeight(getStage()) - SELECTBOX_USER_HEIGHT - USER_SELECT_BOX_PADDING * 2 - localeMenuContainer.getHeight();
            scrollPaneContainer.height(scrollPaneHeight);
            scrollPaneContainer.invalidateHierarchy();
        }
    }

    @Override
    protected MenuControlType getMenuControlType() {
        return MENU_CONTROL_TYPE;
    }

    @Override
    public void initMenuItems(Container<Group> layoutContainer) {
        subMenus.clear();
        mainMenuVerticalGroup = new VerticalGroup();

        for (final IMenuItem menuItem : MainMenuDefinition.getItemsForMenu()) {
            Group menuItemContainer = getGroupForMenuItem(menuItem, getWidth());
            menuItemContainer.setName(menuItem.getTitle());
            menuItemContainer.setUserObject(menuItem);
            menuItemContainer.addListener(menuTouchInputListener);
            mainMenuVerticalGroup.addActor(menuItemContainer);
            mainMenuVerticalGroup.expand().fill();

            IMenuItem[] subMenu = menuItem.getSubmenu();
            if (subMenu != null) {
                VerticalGroup submenuVerticalGroup = new VerticalGroup();
                subMenus.put(menuItem, submenuVerticalGroup);
                submenuVerticalGroup.expand().fill();

                for (final IMenuItem submenuItem : subMenu) {
                    Group submenuItemContainer = getGroupForMenuItem(submenuItem, background.getWidth() - MENU_PADDING);
                    submenuItemContainer.setUserObject(submenuItem);
                    submenuItemContainer.addListener(menuTouchInputListener);
                    submenuItemContainer.setName(submenuItem.toString());
                    submenuVerticalGroup.addActor(submenuItemContainer);
                }
            }
        }

        Table layoutTable = new Table();
        UserSelectBox userSelectBox = new UserSelectBox(SELECTBOX_USER_HEIGHT);
        scrollPane = new ScrollPaneWithBars(mainMenuVerticalGroup, false, true);
        scrollPane.setFillParent(true);
        scrollPaneContainer = new Container();

        layoutTable.add(userSelectBox).fill().align(Align.top).pad(USER_SELECT_BOX_PADDING);
        layoutTable.row();
        layoutTable.add(scrollPaneContainer).fill();
        layoutTable.row();
        layoutTable.add(localeMenuContainer);
        layoutTable.setFillParent(true);

        layoutContainer.setActor(layoutTable);

        scrollPaneContainer.setActor(scrollPane);
        scrollPaneContainer.height(SCROLL_PANE_INIT_SIZE);
        scrollPaneContainer.align(Align.top);
        scrollPaneContainer.width(background.getWidth() - MENU_PADDING);

        scrollPane.setScrollingDisabled(true, false);
        Tablexia.getActualScreen().getStage().setScrollFocus(scrollPane);
    }

    private void performMenuAction(IMenuItem menuItem) {
        if (!isDisableControl()) {
            ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.BUTTON_CLICKED).play();
            if (menuItem.getMenuAction() != null) {
                disableMenuGravity(true);
            }
            doMenuAction(menuItem.getMenuAction(), true, true);
            menuItem.performAction();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        ApplicationBus.getInstance().unsubscribe(this);
    }

    @Override
    protected void disableScroll(boolean disable) {
        if(scrollPane == null || scrollPane.isScrollingDisabledY() == disable)
            return;

        scrollPane.setScrollingDisabled(true, disable);

        if(disable) {
            if(!TablexiaComponentDialog.DialogVisibleEvent.isDialogVisible()) {
                Tablexia.getActualScreen().setScrollableLayoutFocus();
                Tablexia.getActualScreen().enableScrollableLayoutFocus();
            }
        } else
            Tablexia.getActualScreen().getStage().setScrollFocus(scrollPane);

        scrollPane.layout();
    }

    private InputListener menuTouchInputListener = new ClickListener() {

        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if(button == Input.Buttons.RIGHT) return false;
            return pointer == 0;
        }

        @Override
        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            boolean touchUpOver = isOver(event.getListenerActor(), x, y);
            if (touchUpOver) {
                performMenuAction((IMenuItem)event.getListenerActor().getUserObject());
            }
            super.touchUp(event, x, y, pointer, button);
        }
    };
    
//////////////////////////// SUBMENU CONTROL EVENT HANDLING

    @Handler
    public void onSubMenuControlEvent(SubMenuControlEvent subMenuControlEvent) {
        Group subMenu = subMenus.get(subMenuControlEvent.getMenuItem());
        if (subMenu != null) {
            if (subMenuControlEvent.getSubMenuAction().isOpen(subMenu.hasParent()) && !subMenu.hasParent()) {
                mainMenuVerticalGroup.addActorAt(((MainMenuDefinition) subMenuControlEvent.getMenuItem()).ordinal() + 1, subMenu);
            } else {
                subMenu.remove();
            }
        }
        AbstractTablexiaScreen.triggerScenarioStepEvent(GAME_SUBMENU_OPENED);
    }
}
