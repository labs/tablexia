/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.main.user;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import net.engio.mbassy.listener.Handler;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationAvatarManager;
import cz.nic.tablexia.menu.user.UserMenu;
import cz.nic.tablexia.menu.user.UserMenuItem;
import cz.nic.tablexia.menu.user.UserMenuNewSelectBoxItemGroup;
import cz.nic.tablexia.menu.user.UserMenuSelectBoxItemGroup;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.screen.createuser.PanoramaScreen;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.ui.AbstractTablexiaSelectBox;

/**
 * Created by Matyáš Latner.
 */
public class UserSelectBox extends AbstractTablexiaSelectBox<UserSelectBox.UserSelectBoxItem> {

    public interface UserSelectBoxItem {
        void performAction();
        User getUser();
        void setBounds (float x, float y, float width, float height);
        void draw(Batch batch, float parentAlpha);
    }


    private static final int    TRIANGLE_WIDTH              = 12;
    private static final int    TRIANGLE_HEIGHT             = 12;
    private static final int    TRIANGLE_BOTTOM_OFFSET      = 3;
    private static final int    TRIANGLE_RIGHT_OFFSET       = 3;
    private static final int    SELECTOR_RIGHT_OFFSET       = 10;
    private static final int    SELECTOR_BOTTOM_OFFSET      = 4;
    private static final int    SELECTOR_WIDTH              = 5;
    private static final int    SELECTOR_TOP_OFFSET         = 8;
    private static final Color  FOREGROUND_COLOR            = new Color(0.322f, 0.278f, 0.255f, 1f);
    private static final int    ITEMS_OVERLAP_SIZE          = 1;


    private TextureRegionDrawable triangle;
    private TextureRegionDrawable selector;

    private final ChangeListener changeListener;

    public UserSelectBox(float itemHeight) {
        super(itemHeight);
        ApplicationBus.getInstance().subscribe(this);

        selector = (new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getColorTextureRegion(FOREGROUND_COLOR)));
        triangle = (new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.USERMENU_MENUITEM_TRIANGLE)));

        this.changeListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                //On user change
                //not in use now, but may come in handy :P
            }
        };

        setCheckValidity(false);
        prepareActiveUsers();
    }

    private void prepareActiveUsers() {
        removeCaptureListener(changeListener);
        UserMenuSelectBoxItemGroup selectedUserItem = null;
        List<UserSelectBoxItem> userItems = new ArrayList<UserSelectBoxItem>();
        for (User user: UserDAO.selectActiveUsers()) {
            UserMenuSelectBoxItemGroup userItem = new UserMenuSelectBoxItemGroup(new UserMenuItem(user), getWidth());
            if (user.equals(TablexiaSettings.getInstance().getSelectedUser())) {
                selectedUserItem = userItem;
            }
            else {
                userItems.add(userItem);
            }
        }
        UserMenuNewSelectBoxItemGroup newUserItem = new UserMenuNewSelectBoxItemGroup() {
            @Override
            public void performAction() {
                TablexiaSettings.getInstance().changeUser(null, new TablexiaSettings.LogoutDialogListener() {
                    @Override
                    public void onLogoutAccepted() {
                        ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(PanoramaScreen.class, TablexiaApplication.ScreenTransaction.FADE)).asynchronously();
                    }

                    @Override
                    public void onLogoutCanceled() {}
                });
            }

            @Override
            public User getUser() {
                return null;
            }
        };
        userItems.add(newUserItem);

        setItems(userItems.toArray(new UserSelectBoxItem[]{}));
        if (selectedUserItem != null) {
            setSelected(selectedUserItem);
        }
        else {
            setSelected(newUserItem);
        }
        addCaptureListener(changeListener);
    }

    @Override
    protected void drawSelectedItem(Batch batch, float parentAlpha, UserSelectBoxItem selected, float width, float height) {
        selected.setBounds(getX(), getY(), width, height);
        selected.draw(batch, parentAlpha);
        triangle.draw(batch, getX() + getWidth() - TRIANGLE_WIDTH - TRIANGLE_RIGHT_OFFSET, getY() + TRIANGLE_BOTTOM_OFFSET, TRIANGLE_WIDTH, TRIANGLE_HEIGHT);
    }

    @Override
    protected void drawListItem(Batch batch, float parentAlpha, UserSelectBoxItem item, float x, float y, float width, float height) {
        height = height + ITEMS_OVERLAP_SIZE;
        item.setBounds(x, y, width, height);
        item.draw(batch, parentAlpha);
        if (getSelection() != null && getSelection().contains(item)) {
            selector.draw(batch, x + width - SELECTOR_RIGHT_OFFSET, y + SELECTOR_BOTTOM_OFFSET, SELECTOR_WIDTH, height - SELECTOR_TOP_OFFSET);
        }
    }

    @Override
    public boolean onSelectBoxItemSelected(final UserSelectBoxItem item) {
        TablexiaSettings.getInstance().changeUser(item.getUser(), new TablexiaSettings.LogoutDialogListener() {
            @Override
            public void onLogoutAccepted() {
                getSelectBoxList().changeToSelected();

                if(item.getUser() == null) { //Creating new user...
                    ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(PanoramaScreen.class, TablexiaApplication.ScreenTransaction.FADE)).asynchronously();
                }
            }

            @Override
            public void onLogoutCanceled() {}
        });

        //returns true, turns off default behaviour
        return true;
    }

//////////////////////////// SELECTED USER EVENT HANDLING

    @Handler
    public void onSelectedUserEvent(final TablexiaSettings.SelectedUserEvent selectedUserEvent) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                if (selectedUserEvent.isUserSelected()) {
                    prepareActiveUsers();
                }
            }
        });
    }

    @Handler
    public void onAvatarsUpdatedEvent(ApplicationAvatarManager.AvatarUpdatedEvent avatarUpdatedEvent) {
        prepareActiveUsers();
    }

    @Handler
    public void onRefreshUserMenuEvent(UserMenu.RefreshUserMenu refreshUserMenuEvent) {
        prepareActiveUsers();
    }
}
