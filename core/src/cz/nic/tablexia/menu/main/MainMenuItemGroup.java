/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.menu.main;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.menu.IMenuItem;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by Matyáš Latner.
 */
public class MainMenuItemGroup extends Container<Table> {

    private static final Color      HIGHLITED_COLOR         = new Color(0.780f, 0.698f, 0.600f, 1);
    private static final Color      DIVIDER_COLOR           = new Color(0.780f, 0.698f, 0.600f, 1);

    private static final ApplicationFontManager.FontType MENU_ITEM_FONT_TYPE     = ApplicationFontManager.FontType.BOLD_20;
    private static final Color      MENU_ITEM_FONT_COLOR    = new Color(0.325f, 0.278f, 0.255f, 1);
    private static final float      ICON_RIGHT_SPACE_RATIO  = 1f / 20;
    private static final float      ICON_SCALE              = 0.8f;

    private Image iconUnpressedImage;
    private Image iconPressedImage;

    private final TextureRegionDrawable highlitedBackground;

    public MainMenuItemGroup(final IMenuItem menuItem, float width) {
        Table layoutTable = new Table();
        layoutTable.setFillParent(true);
        fillX();
        layoutTable.align(Align.left);
        setActor(layoutTable);
        initializeDefaultListener();
        setTouchable(Touchable.enabled);

        // highliter
        highlitedBackground = new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getColorTextureRegion(HIGHLITED_COLOR));

        // icons
        Stack iconStack = new Stack();
        layoutTable.add(iconStack);
        String[] menuItemIcons = menuItem.getIcons();
        if (menuItemIcons != null) {
            if (menuItemIcons.length > 0) {
                iconUnpressedImage = createIcon(menuItemIcons[0], iconStack);
            }
            if (menuItemIcons.length > 1) {
                iconPressedImage = createIcon(menuItemIcons[1], iconStack);
            }
        }
        setUnpressed();

        // horizontal space
        layoutTable.add().minWidth(width * ICON_RIGHT_SPACE_RATIO);

        // title
        String menuItemTitle = menuItem.getTitle();
        if (menuItemTitle != null) {
            TablexiaLabel label = new TablexiaLabel(menuItemTitle, new TablexiaLabel.TablexiaLabelStyle(MENU_ITEM_FONT_TYPE, MENU_ITEM_FONT_COLOR));
            layoutTable.add(label).expandX().align(Align.left);
        }
        layoutTable.row();

        // divider
        layoutTable.add();
        layoutTable.add();
        layoutTable.add(new Image(ApplicationAtlasManager.getInstance().getColorTexture(DIVIDER_COLOR))).fillX();
    }

    protected void initializeDefaultListener() {
        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                setPressed();
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                setUnpressed();
            }
        });
    }

    private Image createIcon(String iconName, Stack iconStack) {
        Image icon = null;
        if (iconName != null) {
            icon = new Image(ApplicationAtlasManager.getInstance().getTextureRegion(iconName));
            icon.setOrigin(Align.center);
            icon.setScale(ICON_SCALE);
            iconStack.add(icon);
        }

        return icon;
    }

    public void setUnpressed() {
        setBackground(null);
        if (iconUnpressedImage != null) {
            iconUnpressedImage.setVisible(true);
        }
        if (iconPressedImage != null) {
            iconPressedImage.setVisible(false);
        }
    }

    public void setPressed() {
        setBackground(highlitedBackground);
        if (iconUnpressedImage != null) {
            iconUnpressedImage.setVisible(false);
        }
        if (iconPressedImage != null) {
            iconPressedImage.setVisible(true);
        }
    }
}
