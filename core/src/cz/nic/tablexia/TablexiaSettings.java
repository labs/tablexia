/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.gamemenu.OfficeMenuScreen;
import cz.nic.tablexia.screen.loader.LoaderScreen;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;

public class TablexiaSettings {

    public  static final String LOGOUT_DIALOG       = "logout dialog";

    private static final int    DEFAULT_SCREEN_WIDTH     = 1000;
    private static final double MAXIMUM_RATIO            = 9.0 / 18.5;
    private static final int    MIN_SCREEN_HEIGHT        = (int) (DEFAULT_SCREEN_WIDTH * MAXIMUM_RATIO);
    private static final Color  DEFAULT_BACKGROUND_COLOR = new Color(0.99f, 0.88f, 0.71f, 1);
    private static final Color  DEFAULT_FONT_COLOR       = Color.BLACK;

    public static final Class<? extends AbstractTablexiaScreen<?>> LOADER_SCREEN     = LoaderScreen.class;
    public static final Class<? extends AbstractTablexiaScreen<?>> INITIAL_SCREEN    = OfficeMenuScreen.class;
    public static Long                                       GAMES_RANDOM_SEED = null;

    public Long lastSeed = null;

    private static final String  PREFERENCES_KEY         = "cz.nic.tablexia.";
    private static final String  LAST_LOCALE_KEY         = "last_locale";
    public  static final String  LOCALE_KEY              = "locale";
    private static final String  SELECTED_USER           = "selected_user";
    private static final String  SOUND_LEVEL_KEY         = "sound_level_setting";
    private static final String  SOUND_MUTED_KEY         = "sound_muted_setting";

	//Interval <start month; end month>
	private static final int     WINTER_MODE_START_MONTH = 10;
	private static final int     WINTER_MODE_END_MONTH	 = 2;
	private static       boolean winterMode              = false;

    private static final    String      APP_NAME                    = TablexiaBuildConfig.APP_NAME;
    private final           String      APPLICATION_VERSION_NAME    = TablexiaBuildConfig.APPLICATION_VERSION_NAME;
    private final           int         APPLICATION_VERSION_CODE    = TablexiaBuildConfig.APPLICATION_VERSION_CODE;
    private final           String      MODEL_VERSION_NAME          = TablexiaBuildConfig.MODEL_VERSION_NAME;
    private final           int         MODEL_VERSION_CODE          = TablexiaBuildConfig.MODEL_VERSION_CODE;
    private                 Platform    PLATFORM;
    private final           BuildType   BUILD_TYPE;
    private final           boolean     HAS_SOFT_BACK_BUTTON;
    private final           boolean     SUPPORT_ALTERNATIVE_CONTROLS;
    private static          String      HW_SERIAL_NUMBER;

    private Preferences      preferences;
    private LocaleDefinition systemLocale;
    private LocaleDefinition selectedLocale;
	private LocaleDefinition lastLocale;
    private User             selectedUser;

    private boolean          soundMuted;
    private boolean          hdAssets = false;
    private float            soundLevel;
    protected static boolean languageChosen = false;


    public enum Platform {

        UNKNOWN (-1,    null),
        DESKTOP (1,     Application.ApplicationType.Desktop, true),
        ANDROID (2,     Application.ApplicationType.Android),
        iOS     (3,     Application.ApplicationType.iOS, true);

        private static Platform FALLBACK_PLATFORM = UNKNOWN;

        private final int id;
        private final Application.ApplicationType applicationType;
        private final boolean   assetsBundled;

        Platform(int id, Application.ApplicationType applicationType, boolean assetsBundled) {
            this.id                 = id;
            this.applicationType    = applicationType;
            this.assetsBundled  = assetsBundled;
        }

        Platform(int id, Application.ApplicationType applicationType){
            this(id, applicationType, false);
        }

        public int getId() {
            return id;
        }

        public boolean isAssetsBundled() {
            return assetsBundled;
        }

        public static Platform getPlatformForApplicationType(Application.ApplicationType applicationType) {
            for (Platform platform : values()) {
                if (platform.applicationType != null && platform.applicationType.equals(applicationType)) {
                    return platform;
                }
            }
            return FALLBACK_PLATFORM;
        }
    }


    public enum BuildType {
        //                      boxes   aInfo   sInfo   dMenu   btName  bReport  debugBtn   syncDTB
        RELEASE (1, "release", false,  false,  false,  false,  false,  true,    false,     true,   false, Log.TablexiaLogLevel.ERROR),
        DEBUG   (2, "debug",   false,  true,   true,   true,   true,   true,    true,      true,   true,  Log.TablexiaLogLevel.DEBUG),
        DEVEL   (3, "devel",   true,   true,   true,   true,   true,   false,   true,      true,   true,  Log.TablexiaLogLevel.DEBUG),
        ITEST   (4, "iTest",   false,  true,   true,   false,  true,   false,   false,     false,  false, Log.TablexiaLogLevel.DEBUG);

        private final static BuildType FALLBACK_VARIANT = BuildType.DEVEL;

        private       int                  id;
        private final String               key;
        private       boolean              boundingBoxes;
        private       boolean              applicationInfo;
        private       boolean              screenInfo;
        private       boolean              debugMenu;
        private       boolean              showBuildTypeInName;
        private       boolean              bugReport;
        private       boolean              showDebugButtons;
        private       boolean              syncWithDTB;
        private       boolean              logFile;
        private final Log.TablexiaLogLevel logLevel;


        BuildType(int id, String key, boolean boundingBoxes, boolean applicationInfo, boolean screenInfo, boolean debugMenu, boolean showBuildTypeInName, boolean bugReport, boolean showDebugButtons, boolean syncWithDTB, boolean logFile, Log.TablexiaLogLevel logLevel) {
            this.id = id;
            this.key = key;
            this.boundingBoxes = boundingBoxes;
            this.applicationInfo = applicationInfo;
            this.screenInfo = screenInfo;
            this.debugMenu = debugMenu;
            this.showBuildTypeInName = showBuildTypeInName;
            this.bugReport = bugReport;
            this.showDebugButtons = showDebugButtons;
            this.syncWithDTB = syncWithDTB;
            this.logFile = logFile;
            this.logLevel = logLevel;
        }

        public int getId() {
            return id;
        }

        public String getKey() {
            return key;
        }

        public String getAppName() {
            return showBuildTypeInName ? APP_NAME + " " + key.toUpperCase() : APP_NAME;
        }

        public boolean isBoundingBoxes() {
            return boundingBoxes;
        }

        public boolean isApplicationInfo() {
            return applicationInfo;
        }

        public boolean isScreenInfo() {
            return screenInfo;
        }

        public boolean isDebugMenu() {
            return debugMenu;
        }

        public boolean isBugReport() {
            return bugReport;
        }

        public boolean isShowDebugButtons() {
            return showDebugButtons;
        }

        public boolean isSyncWithDTB() {
            return syncWithDTB;
        }

        public boolean isLogFile() { return logFile; }

        public Log.TablexiaLogLevel getLogLevel() {
            return logLevel;
        }

        public static BuildType getBuildTypeForKey(String key) {
            for (BuildType buildType : BuildType.values()) {
                if (buildType.key.equals(key)) {
                    return buildType;
                }
            }
            return FALLBACK_VARIANT;
        }
    }


//////////////////////////// SETTINGS SINGLETON

    private static volatile TablexiaSettings instance;
    private static final Object mutex = new Object();

    private TablexiaSettings(BuildType buildType, Locale systemLocale, boolean hasSoftBackButton, boolean supportAlternativeControls, boolean hdAssets) {
        BUILD_TYPE = buildType;
        HAS_SOFT_BACK_BUTTON = hasSoftBackButton;
        SUPPORT_ALTERNATIVE_CONTROLS = supportAlternativeControls;
        this.systemLocale = LocaleDefinition.getLocaleDefinitionForLocale(systemLocale);
        languageChosen = selectedLocale == LocaleDefinition.FALLBACK_VARIANT || languageChosen;
        this.hdAssets = hdAssets;
    }

    public static TablexiaSettings getInstance() {
        TablexiaSettings settings = instance;
        if (settings == null) {
            synchronized (mutex){
                settings = instance;
                if(settings== null){
                    String exceptionMessage = "Tablexia settings is not initialized!";
                    Log.err(TablexiaSettings.class, exceptionMessage);
                }
            }

        }
        return settings;
    }

    static void init(String buildTypeKey, Locale systemLocale, boolean hasSoftBackButton, boolean supportAlternativeControls, String hwSerial, boolean hdAssets) {
        TablexiaSettings.init(BuildType.getBuildTypeForKey(buildTypeKey), systemLocale, hasSoftBackButton, supportAlternativeControls, hwSerial, hdAssets);
    }


    static void init(BuildType buildType, Locale systemLocale, boolean hasSoftBackButton, boolean supportAlternativeControls, String hwSerialNumber, boolean hdAssets) {
        if (instance != null) {
            String exceptionMessage = "Tablexia settings already initialized!";
            Log.err(TablexiaSettings.class, exceptionMessage);
            return;
        }

        instance = new TablexiaSettings(buildType, systemLocale, hasSoftBackButton, supportAlternativeControls, hdAssets);
        TablexiaSettings.HW_SERIAL_NUMBER = hwSerialNumber;
    }

    void dispose() {
        instance = null;
    }


//////////////////////////// LIBGDX PREFERENCES

    public void loadPreferences(boolean reset) {
        preferences = Gdx.app.getPreferences(PREFERENCES_KEY + BUILD_TYPE.getKey());
        lastLocale = LocaleDefinition.getLocaleDefinitionForKey(preferences.getString(LAST_LOCALE_KEY));
        selectedLocale = LocaleDefinition.getLocaleDefinitionForKey(preferences.getString(LOCALE_KEY));
        selectedUser = reset ? null : UserDAO.selectUser(preferences.getLong(SELECTED_USER));
        languageChosen = selectedLocale != LocaleDefinition.FALLBACK_VARIANT || languageChosen;

        //If system speech is supported and is not default speech (czech), set flag.
        //If czech was fallback variant, correct flag is already set.
        for (LocaleDefinition localeDefinition : LocaleDefinition.values()) {
            if (localeDefinition == getSystemLocale() && localeDefinition != LocaleDefinition.cs_CZ) languageChosen = true;
        }

		Calendar calendar = GregorianCalendar.getInstance();
		winterMode = (calendar.get(Calendar.MONTH) > WINTER_MODE_START_MONTH) || (calendar.get(Calendar.MONTH) < WINTER_MODE_END_MONTH);

		soundLevel = (preferences.contains(SOUND_LEVEL_KEY)) ? preferences.getFloat(SOUND_LEVEL_KEY) : 1f;
        soundMuted = (preferences.contains(SOUND_MUTED_KEY)) ? preferences.getBoolean(SOUND_MUTED_KEY) : false;
    }

//////////////////////////// SETTINGS ACCESS

    public String getFullName() {
        return getAppName() + " " + getApplicationVersionName();
    }

    public String getAppName() {
        return getBuildType().getAppName();
    }

    public String getApplicationVersionName() {
        return APPLICATION_VERSION_NAME;
    }

    public int getApplicationVersionCode() {
        return APPLICATION_VERSION_CODE;
    }

    public String getModelVersionName() {
        return MODEL_VERSION_NAME;
    }

    public int getModelVersionCode() {
        return MODEL_VERSION_CODE;
    }

    public Platform getPlatform() {
        if (PLATFORM == null) {
            PLATFORM = Platform.getPlatformForApplicationType(Gdx.app.getType());
        }
        return PLATFORM;
    }

    public boolean hasSoftBackButton() {
        return HAS_SOFT_BACK_BUTTON;
    }
    
    public boolean hasAlternativeControls() {
        return SUPPORT_ALTERNATIVE_CONTROLS;
    }

    public String getHwSerialNumber() {
        return HW_SERIAL_NUMBER;
    }

    public boolean isWinterMode() { return winterMode; }

    public boolean isShowBoundingBoxes() {
        return getBuildType().isBoundingBoxes();
    }

    public Log.TablexiaLogLevel getLogLevel() {
        return BUILD_TYPE.getLogLevel();
    }

    public BuildType getBuildType() {
        return BUILD_TYPE;
    }

    public static Color getDefaultBackgroundColor() {
        return DEFAULT_BACKGROUND_COLOR;
    }

    public static Color getDefaultFontColor() {
        return DEFAULT_FONT_COLOR;
    }

    public boolean isHdAssets() {
        return hdAssets;
    }

    public boolean isUseHdAssets(){
        return hdAssets && TablexiaBuildConfig.ASSETS_HD_CHECKSUM != "";
    }

    //////////////////////////// SOUND MUTE

    public boolean isSoundMuted() { return soundMuted; }

    public void toggleSoundMute() {
        this.soundMuted = !this.soundMuted;
        preferences.putBoolean(SOUND_MUTED_KEY, soundMuted);
        preferences.flush();

        ApplicationBus.getInstance().publishAsync(new SoundMuteEvent(soundMuted));
    }

    public static class SoundMuteEvent implements ApplicationBus.ApplicationEvent {
        private boolean soundMuted;

        private SoundMuteEvent(boolean soundMuted) {
            this.soundMuted = soundMuted;
        }

        public boolean isSoundMuted() {
            return soundMuted;
        }
    }

    public void setSoundLevel(float soundLevel){
        this.soundLevel = soundLevel;
        preferences.putFloat(SOUND_LEVEL_KEY, soundLevel);
        preferences.flush();
        boolean shouldBeMuted = soundLevel == 0f;
        if(soundMuted != shouldBeMuted){
            soundMuted = shouldBeMuted;
            preferences.putBoolean(SOUND_MUTED_KEY, soundMuted);
            preferences.flush();

            ApplicationBus.getInstance().publishAsync(new SoundMuteEvent(soundMuted));
        }
    }

    public float getSoundLevel() {
        return soundLevel;
    }

    public void decreaseSoundLevel(){
        this.soundLevel = Math.max(0f, soundLevel - 0.25f);
        preferences.putFloat(SOUND_LEVEL_KEY, soundLevel);
        preferences.flush();
        if (soundLevel == 0f) {
            soundMuted = true;
            preferences.putBoolean(SOUND_MUTED_KEY, true);
            preferences.flush();
            ApplicationBus.getInstance().publishAsync(new SoundMuteEvent(soundMuted));
        }
    }

    public void increaseSoundLevel(){
        this.soundLevel = Math.min(1f, soundLevel + 0.25f);
        preferences.putFloat(SOUND_LEVEL_KEY, soundLevel);
        preferences.flush();
        soundMuted = false;
        preferences.putBoolean(SOUND_MUTED_KEY, false);
        preferences.flush();
        ApplicationBus.getInstance().publishAsync(new SoundMuteEvent(soundMuted));
    }

    //////////////////////////// USER SETTINGS

    public static class SelectedUserEvent implements ApplicationBus.ApplicationEvent {

        private boolean userSelected;

        private SelectedUserEvent(boolean userSelected) {
            this.userSelected = userSelected;
        }

        public boolean isUserSelected() {
            return userSelected;
        }
    }

    public void changeUser(User user, LogoutDialogListener logoutDialogListener) {
        if(this.selectedUser == null)
            setSelectedUser(user);

        if (!this.selectedUser.equals(user)) {
            showLogoutUserDialog(user, logoutDialogListener);
        }
    }

    public void changeUser(User user) {
        changeUser(user, null);
    }

    private void setSelectedUser(User user) {
        selectedUser = user;

        if (selectedUser == null) {
            preferences.remove(SELECTED_USER);
        } else {
            preferences.putLong(SELECTED_USER, selectedUser.getId());
        }
        preferences.flush();
        ApplicationBus.getInstance().publishAsync(new SelectedUserEvent(user != null));
    }

    private void setLastLocale(LocaleDefinition lastLocale) {
        this.lastLocale = lastLocale;

        if (lastLocale == null) {
            preferences.remove(LAST_LOCALE_KEY);
        } else {
            preferences.putString(LAST_LOCALE_KEY, lastLocale.getLocaleKey());
        }
        preferences.flush();
    }

    public interface LogoutDialogListener {
        void onLogoutAccepted();
        void onLogoutCanceled();
    }

    public void showLogoutUserDialog(final User user, final LogoutDialogListener logoutDialogListener) {
        TablexiaComponentDialog dialog = TablexiaComponentDialogFactory.getInstance().createWarningYesNoDialog(
                ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.USER_LOGOUT_MESSAGE),
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        setSelectedUser(user);

                        if (logoutDialogListener != null)
                            logoutDialogListener.onLogoutAccepted();
                    }
                },
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        if(logoutDialogListener!= null)
                            logoutDialogListener.onLogoutCanceled();
                    }
                },
                true, true);

        dialog.setName(LOGOUT_DIALOG);

        dialog.show(TablexiaComponentDialogFactory.WARNING_DIALOG_WIDTH, TablexiaComponentDialogFactory.WARNING_DIALOG_HEIGHT);
    }

    public User getSelectedUser() {
        return selectedUser;
    }


    public enum AssetsPackDefinition {

        DEFAULT(""),
        HD("_HD");

        private String packSuffix;

        AssetsPackDefinition(String packSuffix) {
            this.packSuffix = packSuffix;
        }

        public String getPackSuffix() {
            return packSuffix;
        }

        public static String getAssetsPackSuffix(){
            return TablexiaSettings.getInstance().isUseHdAssets() ? HD.getPackSuffix() : DEFAULT.getPackSuffix();
        }
    }

//////////////////////////// LOCALE SETTINGS

    public static class LocaleChangedEvent implements ApplicationBus.ApplicationEvent {

        private TablexiaSettings.LocaleDefinition localeDefinition;

        private LocaleChangedEvent(TablexiaSettings.LocaleDefinition localeDefinition) {
            this.localeDefinition = localeDefinition;
        }

        public TablexiaSettings.LocaleDefinition getLocaleDefinition() {
            return localeDefinition;
        }
    }

    public enum LocaleDefinition {
        SYSTEM  (null,                                                              null,                   "system", ApplicationTextManager.ApplicationTextsAssets.LANGUAGE_SYSTEM,  true, null, false) {
            @Override
            public int getLocaleNumber() {
                return getLocaleDefinitionForLocale(instance.systemLocale.locale).modelLocaleDefinition.number();
            }
        },
        cs_CZ   (cz.nic.tablexia.shared.model.definitions.LocaleDefinition.cs_CZ,   new Locale("cs", "CZ"), "cs_CZ",  ApplicationTextManager.ApplicationTextsAssets.LANGUAGE_CZECH,   true, "čeština",      true),
        sk_SK   (cz.nic.tablexia.shared.model.definitions.LocaleDefinition.sk_SK,   new Locale("sk", "SK"), "sk_SK",  ApplicationTextManager.ApplicationTextsAssets.LANGUAGE_SLOVAK,  true, "slovenčina",   true),
        de_DE   (cz.nic.tablexia.shared.model.definitions.LocaleDefinition.de_DE,   new Locale("de", "DE"), "de_DE",  ApplicationTextManager.ApplicationTextsAssets.LANGUAGE_GERMAN,  true, "deutsch",      true);

        public final static  LocaleDefinition DEFAULT_LOCALE   = LocaleDefinition.cs_CZ;
        private final static LocaleDefinition FALLBACK_VARIANT = LocaleDefinition.SYSTEM;

        private final cz.nic.tablexia.shared.model.definitions.LocaleDefinition modelLocaleDefinition;
        private final Locale                                                    locale;
        private final String                                                    localeKey;
        private final String                                                    descriptionKey;
        private final boolean                                                   enabled;
        private final String                                                    nativeLocaleName;
        private final boolean                                                   selectable;

        private static List<LocaleDefinition> enabledLocaleDefinitions;

        LocaleDefinition(cz.nic.tablexia.shared.model.definitions.LocaleDefinition modelLocaleDefinition, Locale locale, String localeKey, String descriptionKey, boolean enabled, String nativeLocaleName, boolean selectable) {
            this.modelLocaleDefinition  = modelLocaleDefinition;
            this.locale                 = locale;
            this.localeKey              = localeKey;
            this.descriptionKey         = descriptionKey;
            this.enabled                = enabled;
            this.nativeLocaleName       = nativeLocaleName;
            this.selectable             = selectable;
        }

        public String getLocaleKey() {
            return localeKey;
        }

        public int getLocaleNumber() {
            return getLocaleDefinitionForLocale(getLocale()).modelLocaleDefinition.number();
        }

        public String getDescriptionKey() {
            return descriptionKey;
        }

        public String getNativeLocaleName() {
            return nativeLocaleName;
        }

        public Locale getLocale() {
            return locale != null ? locale : instance.systemLocale.locale;
        }

        @Override
        public String toString() {
            if (ApplicationTextManager.getInstance().update()) {
                return ApplicationTextManager.getInstance().getText(descriptionKey);
            } else {
                return name();
            }
        }

        public static LocaleDefinition getLocaleDefinitionForKey(String key) {
            for (LocaleDefinition localeDefinition : getEnabledLocaleDefinitions()) {
                if (localeDefinition.localeKey.equals(key)) {
                    return localeDefinition;
                }
            }
            return FALLBACK_VARIANT;
        }

        public static LocaleDefinition getLocaleDefinitionForLocale(Locale locale) {
            for (LocaleDefinition localeDefinition : getEnabledLocaleDefinitions()) {
                if (localeDefinition.locale != null && localeDefinition.locale.equals(locale)) {
                    languageChosen = true;
                    return localeDefinition;
                }
            }
            languageChosen = false;
            return DEFAULT_LOCALE;
        }

        public static List<LocaleDefinition> getEnabledLocaleDefinitions() {
            if (enabledLocaleDefinitions == null) {
                enabledLocaleDefinitions = new ArrayList<LocaleDefinition>();
                for (LocaleDefinition localeDefinition : LocaleDefinition.values()) {
                    if (localeDefinition.enabled) {
                        enabledLocaleDefinitions.add(localeDefinition);
                    }
                }
            }
            return enabledLocaleDefinitions;
        }

        public static List<LocaleDefinition> getSelectableLocaleDefinitions() {
            List<LocaleDefinition> selectableLocaleDefinitions = new ArrayList<>();
                for (LocaleDefinition localeDefinition : LocaleDefinition.values()) {
                    if (localeDefinition.selectable) {
                        selectableLocaleDefinitions.add(localeDefinition);
                    }
                }
            return selectableLocaleDefinitions;
        }
    }

    public void setLocale(LocaleDefinition localeDefinition) {
        setLastLocale(selectedLocale);
        selectedLocale = localeDefinition;
        preferences.putString(LOCALE_KEY, localeDefinition.getLocaleKey());
        preferences.flush();
        if (hasLocaleChanged()) {
            ApplicationBus.getInstance().post(new LocaleChangedEvent(localeDefinition)).asynchronously();
        }
    }

	public boolean hasLocaleChanged() {
		return lastLocale == null || !lastLocale.getLocale().getLanguage().equals(selectedLocale.getLocale().getLanguage());
	}

    public LocaleDefinition getLocaleDefinition() {
        return selectedLocale;
    }

	public void returnToLastLocale() {
		if (hasLocaleChanged()) {
			setLocale(getLastLocale());
		}
	}

	public LocaleDefinition getLastLocale() {
		return lastLocale;
	}

	public Locale getLocale() {
        return selectedLocale.getLocale();
    }

    public LocaleDefinition getSystemLocale() {
        return systemLocale;
    }


//////////////////////////// SCREEN SIZE

    public static int getWorldSize() {
        return DEFAULT_SCREEN_WIDTH;
    }

    public static double getMaximumRatio() {
        return MAXIMUM_RATIO;
    }

    public static int getMinWorldHeight() {
        return MIN_SCREEN_HEIGHT;
    }

    private static float calculateWordSizeOffset() {
        return (getWorldSize() - getMinWorldHeight()) / 2f;
    }


//////////////////////////// SCENE SIZE

    public static float getSceneLeftX(Stage stage) {
        return 0;
    }

    public static float getSceneRightX(Stage stage) {
        return stage.getWidth();
    }

    public static float getSceneOuterBottomY(Stage stage) {
        return -calculateWordSizeOffset();
    }

    public static float getSceneInnerBottomY(Stage stage) {
        return 0;
    }

    public static float getSceneInnerTopY(Stage stage) {
        return getSceneInnerBottomY(stage) + MIN_SCREEN_HEIGHT;
    }

    public static float getSceneOuterTopY(Stage stage) {
        return getSceneInnerTopY(stage) + calculateWordSizeOffset();
    }

    public static float getSceneWidth(Stage stage) {
        return getSceneRightX(stage) - getSceneLeftX(stage);
    }

    public static float getSceneOuterHeight(Stage stage) {
        return getSceneOuterTopY(stage) - getSceneOuterBottomY(stage);
    }

    public static float getSceneInnerHeight(Stage stage) {
        return getSceneInnerTopY(stage) - getSceneInnerBottomY(stage);
    }


//////////////////////////// SCENE VIEWPORT

    public static float getViewportLeftX(Stage stage) {
        return 0;
    }

    public static float getViewportRightX(Stage stage) {
        return getViewportLeftX(stage) + stage.getWidth();
    }

    public static float getViewportBottomY(Stage stage) {
        return stage.getCamera().position.y - stage.getHeight() / 2;
    }

    public static float getViewportTopY(Stage stage) {
        return getViewportBottomY(stage) + stage.getHeight();
    }

    public static float getViewportWidth(Stage stage) {
        return getViewportRightX(stage) - getViewportLeftX(stage);
    }

    public static float getViewportHeight(Stage stage) {
        return getViewportTopY(stage) - getViewportBottomY(stage);
    }

	public static float getActorSceneVerticalCenter(Stage stage, Actor actor) {
		return getViewportHeight(stage) / 2f - actor.getHeight() / 2f + getViewportBottomY(stage);
	}

//////////////////////////// DEVICE INFO

    public boolean isRunningOnMobileDevice() {
        return  Gdx.app.getType() == Application.ApplicationType.Android ||
                Gdx.app.getType() == Application.ApplicationType.iOS;
    }


    //Gets for testing


    public Long getLastSeed() {
        return lastSeed;
    }

    public void setLastSeed(Long lastSeed) {
        this.lastSeed = lastSeed;
    }

    public static boolean isUserHighestRank(){
        return UserRankManager.getInstance().getRank(TablexiaSettings.getInstance().getSelectedUser()) == UserRankManager.UserRank.RANK_XI;
    }
}