/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.model;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.games.protocol.ProtocolGame;
import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;

/**
 * Created by lmarik on 5.5.17.
 */

public class EmptyCard extends Image {
    
    public EmptyCard(ProtocolGame protocolGame){
        super(protocolGame.getScreenTextureRegion(ProtocolAssets.MENU_NEXT));
        setSize(ProtocolGame.CARD_SMALL_SIZE,ProtocolGame.CARD_SMALL_SIZE);
        
    }
    
}
