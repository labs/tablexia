/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.gameobjects.descriptors;

import java.util.Arrays;
import java.util.List;

import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;

public enum  WallObjectDescriptor {
    //MEDIUM
    CUCKOO_CLOCK    (GameObjectType.CUCKOO_CLOCK,   WallType.LEFT_WALL,  new EdgePoints(new float[]{0.06f, 0.97f}, new float[]{0.1f, 0.03f},   new float[]{0.92f, 0.98f},  new float[]{0.93f, 0.04f}), Arrays.asList( new float[]{0.044f, 0.55f},  new float[]{0.42f,  0.87f},  new float[]{0.92f,  0.65f})),
    LADY_AND_PANDA  (GameObjectType.LADY_AND_PANDA, WallType.LEFT_WALL,  new EdgePoints(new float[]{0.11f, 0.68f}, new float[]{0.19f, 0.04f},  new float[]{0.86f, 0.94f},  new float[]{0.93f, 0.31f}), Arrays.asList( new float[]{0.08f,  0.66f},  new float[]{0.45f,  0.8f},   new float[]{0.92f,  0.65f})),
    LANDSCAPE       (GameObjectType.LANDSCAPE,      WallType.RIGHT_WALL, new EdgePoints(new float[]{0.09f, 0.93f}, new float[]{0.08f, 0.38f},  new float[]{0.9f, 0.62f},   new float[]{0.9f, 0.05f}),  Arrays.asList( new float[]{0.08f,  0.66f},  new float[]{0.57f,  0.87f},  new float[]{0.92f,  0.65f})),
    MIRROR          (GameObjectType.MIRROR,         WallType.RIGHT_WALL, new EdgePoints(new float[]{0.2f, 0.97f},  new float[]{0.07f, 0.18f},  new float[]{0.91f, 0.82f},  new float[]{0.74f, 0.03f}), Arrays.asList( new float[]{0.1f,  0.54f},   new float[]{0.5f,   0.78f},  new float[]{0.9f,  0.54f})),
    //BONUS
    KABUKI          (GameObjectType.KABUKI,         WallType.LEFT_WALL,  new EdgePoints(new float[]{0.07f, 0.9f},  new float[]{0.1f, -0.13f},  new float[]{0.9f, 1.07f},   new float[]{0.93f, 0.13f}),  Arrays.asList( new float[]{0.07f,  0.66f},  new float[]{0.6f,   0.78f},  new float[]{0.83f, 0.74f})),
    MASK_AFRICA     (GameObjectType.MASK_AFRICA,    WallType.RIGHT_WALL, new EdgePoints(new float[]{0.1f, 1.04f},  new float[]{0.07f, 0.12f},  new float[]{0.95f, 0.9f},   new float[]{0.74f, -0.02f}), Arrays.asList( new float[]{0.07f,  0.66f},  new float[]{0.4f,   0.78f},  new float[]{0.83f, 0.74f})),
    TRAVEL_HAT      (GameObjectType.TRAVEL_HAT,     WallType.LEFT_WALL,  new EdgePoints(new float[]{0.1f, 0.88f},  new float[]{0.1f, -0.1f},   new float[]{0.87f, 1.1f},   new float[]{0.87f, 0.15f}),  Arrays.asList( new float[]{0.07f,  0.66f},  new float[]{0.5f,   0.9f},   new float[]{0.83f, 0.74f}));


    private GameObjectType objectType;
    private WallType wallOrientation;

    private EdgePoints edgePoints;


    private List<float[]> wallFixRatio;

    WallObjectDescriptor(GameObjectType objectType, WallType wallOrientation, EdgePoints edgePoints, List<float[]> wallFixRatio){
        this.objectType = objectType;
        this.wallOrientation = wallOrientation;
        this.edgePoints = edgePoints;


        this.wallFixRatio = wallFixRatio;
    }

    public static WallObjectDescriptor getDescriptorByType(GameObjectType objectType){

        for (WallObjectDescriptor descriptor: values()){
            if(descriptor.objectType == objectType)
                return descriptor;
        }

        throw new RuntimeException("Couldn't get wall descriptor for object: " + objectType);
    }


    public WallType getWallOrientation() {
        return wallOrientation;
    }

    public float[] getFixRatio(WallType wallType){
        return wallFixRatio.get(wallType.getOriginType());
    }

    public EdgePoints getEdgePoints() {
        return edgePoints;
    }
}
