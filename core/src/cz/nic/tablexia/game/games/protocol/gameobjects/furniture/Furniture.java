/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.gameobjects.furniture;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.nic.tablexia.game.games.protocol.controller.RoomPosition;
import cz.nic.tablexia.game.games.protocol.gameobjects.ObjectModel;
import cz.nic.tablexia.game.games.protocol.utils.ObjectsPositionComparator;

/**
 * Created by lmarik on 9.8.17.
 */

public class Furniture extends ObjectModel {

    private FurnitureType     type;
    private Image             furnitureImage;
    private TextureRegion     texture;

    private List<ObjectModel> objectOnFurniture;

    public Furniture(FurnitureType type, TextureRegion texture, float resizePercent) {
        super();
        this.type = type;
        this.texture = texture;

        furnitureImage = new Image(texture);

        furnitureImage.setWidth((furnitureImage.getDrawable().getMinWidth() * resizePercent * scaleFactor) / 100);
        furnitureImage.setHeight((furnitureImage.getDrawable().getMinHeight() * resizePercent * scaleFactor) / 100);

        float xCenter = furnitureImage.getWidth() * type.getMiddleRatio()[0];
        float yCenter = furnitureImage.getHeight() * type.getMiddleRatio()[1];

        objectOnFurniture = new ArrayList<>();

        centerPosition = new Vector2(xCenter, yCenter);
        addActor(furnitureImage);

        furnitureImage.setTouchable(Touchable.disabled);
    }

    public void addObjectOnFurniture(ObjectModel objectModel, float xPosition, float yPosition) {
        addActor(objectModel);

        objectModel.setPosition(xPosition, yPosition);
        objectModel.setLastPosition(new Vector2(xPosition, yPosition));

        objectOnFurniture.add(objectModel);
    }

    public void removeObjectFromFurniture(ObjectModel objectModel) {
        removeActor(objectModel);
        objectOnFurniture.remove(objectModel);
    }

    private void controlObjectPosition() {
        Collections.sort(objectOnFurniture, new ObjectsPositionComparator());

        for (int i = 0; i < objectOnFurniture.size(); i++) {
            objectOnFurniture.get(i).setZIndex(i + 1);
        }
    }

    @Override
    public void clearObjects() {
        for (ObjectModel objectModel : objectOnFurniture) {
            removeActor(objectModel);
        }

        objectOnFurniture.clear();
    }

    public Image getFurnitureImage() {
        return furnitureImage;
    }


    @Override
    public float getImageWidth() {
        return furnitureImage.getWidth();
    }

    @Override
    public float getImageHeight() {
        return furnitureImage.getHeight();
    }

    @Override
    public TextureRegion getTexture() {
        return texture;
    }

    @Override
    public FurnitureType getType() {
        return type;
    }

    @Override
    public void zIndexChanged() {
        controlObjectPosition();
    }

    @Override
    public RoomPosition getRoomPosition() {
        return type.getMapPosition();
    }
}
