/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.game.games.protocol.ProtocolGame;
import cz.nic.tablexia.game.games.protocol.controller.RoomPosition;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.EdgePoints;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.WallObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.Furniture;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;
import cz.nic.tablexia.game.games.protocol.utils.ProtocolDragListener;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.ui.TablexiaLabel;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.color;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by lmarik on 4.5.17.
 */

public class GameObject extends ObjectModel {

    private static final Color                               CARD_TEXT_COLOR        = Color.BLACK;
    private static final ApplicationFontManager.FontType     CARD_TEXT_FONT         = ApplicationFontManager.FontType.BOLD_18;

    private static final Interpolation OBJECT_INTERPOLATION 		    = Interpolation.pow4Out;
    private static final Interpolation CARD_INTERPOLATION               = Interpolation.swingOut;
    private static final Interpolation BIGGER_INTERPOLATION             = Interpolation.linear;

    private static final String        MIDDLE_PREFIX                    = "_m";
    private static final float         MOVE_DURATION                    = 0.3f;
    private static final float         SMALL_DURATION                   = 0.235f;
    private static final float         BIGGER_DURATION                  = 0.08f;

    private static final float         DROP_ANIM_DURATION               = 0.1f;
    private static final float         OBJECT_BIGGER_SCALE              = 1.8f;

    private static final float         LABEL_WITH_RATIO                 = 0.95f;
    private static final float         LABEL_PADDING_BOTTOM             = 18f;


    private boolean             biggerScale     = false;
    private boolean             inRoom          = false;
    private int                 menuPosition    = -1;

    private GameObjectType          type;
    private ProtocolDragListener    dragListener;
    private Image                   objectImage;
    private ProtocolGame            protocolGame;

    private Furniture furniture;
    private WallType wallPositionType;
    private Vector2 positionBeforeScale;

    //Wall control points;
    private Vector2 rBPoint;
    private Vector2 lBPoint;
    private Vector2 lTPoint;
    private Vector2 rTPoint;

    private TablexiaLabel           descriptionLabel;
    private TextureRegion           cardDrawable;
    private TextureRegion           objectDrawable;


    private String                  cardText;
    private boolean                 correct;
    private boolean                 selected;
    private boolean                 fixed;
    private boolean                 scaling;
    private boolean                 rotate;
    private boolean                 middle;

    public GameObject(GameObjectType type, ProtocolGame protocolGame, String cardText, boolean inPicture) {
        super();
        this.type = type;
        this.inRoom = inPicture;
        this.cardText = cardText;
        this.protocolGame = protocolGame;

        roomPosition = null;
        selected = false;
        fixed = false;
        rotate = false;
        middle = false;

        cardDrawable = protocolGame.getScreenTextureRegion(type.getCardTexturePath());
        objectDrawable = protocolGame.getScreenTextureRegion(type.getObjectTexturePath());

        initObjectImage();
        initPoints();
        initLabelDescription();
        initDragListener();
    }

//////////////////////////// INITIALIZATION

    public void initPoints(){
        float minWidth = getMinWidth();
        float minHeight = getMinHeight();

        float xCenter = minWidth * type.getCenterRatio()[0];
        float yCenter = minHeight * type.getCenterRatio()[1];
        centerPosition = new Vector2(xCenter, yCenter);

        float[] pointRatio = type.getPutRatio();

        if(pointRatio != null) {
            putPoint = new Vector2(minWidth * pointRatio[0], minHeight * pointRatio[1]);
        }

        float[] wallRatio = type.getWallRatio();

        if(wallRatio != null){
            wallPoint = new Vector2(minWidth * wallRatio[0], minHeight * wallRatio[1]);
            initEdgePoint(WallObjectDescriptor.getDescriptorByType(type));
        }
    }


    private void changeWallPointPosition(){
        if(type.getWallRatio() == null)
            return;

        float minWidth = getMinWidth();
        float minHeight = getMinHeight();

        if(!middle) {
            wallPoint.x = minWidth * (rotate ? (1f - type.getWallRatio()[0]) : type.getWallRatio()[0]);
            wallPoint.y = minHeight * type.getWallRatio()[1];
        }else {
            wallPoint.x = minWidth / 2;
            wallPoint.y = minHeight / 2;
        }

    }


    private void initEdgePoint(WallObjectDescriptor descriptor){
        float minWidth = getMinWidth();
        float minHeight = getMinHeight();

        if(!middle) {
            EdgePoints edgePoints = descriptor.getEdgePoints();
            rBPoint = new Vector2(minWidth * (rotate ? (1.f - edgePoints.getlBRatio()[0]) : edgePoints.getrBRatio()[0]), minHeight * (rotate ? edgePoints.getlBRatio()[1] : edgePoints.getrBRatio()[1]));
            rTPoint = new Vector2(minWidth * (rotate ? (1.f - edgePoints.getlTRatio()[0]) : edgePoints.getrTRatio()[0]), minHeight * (rotate ? edgePoints.getlTRatio()[1] : edgePoints.getrTRatio()[1]));

            lBPoint = new Vector2(minWidth * (rotate ? (1.f - edgePoints.getrBRatio()[0]) : edgePoints.getlBRatio()[0]), minHeight * (rotate ? edgePoints.getrBRatio()[1] : edgePoints.getlBRatio()[1]));
            lTPoint = new Vector2(minWidth * (rotate ? (1.f - edgePoints.getrTRatio()[0]) : edgePoints.getlTRatio()[0]), minHeight * (rotate ? edgePoints.getrTRatio()[1] : edgePoints.getlTRatio()[1]));
        }else {
            float topRatio = 0.97f;
            float bottomRatio = 0.04f;

            rBPoint = new Vector2(minWidth * topRatio, minHeight * bottomRatio);
            rTPoint = new Vector2(minWidth * topRatio, minHeight * topRatio);

            lBPoint = new Vector2(minWidth * bottomRatio, minHeight * bottomRatio);
            lTPoint = new Vector2(minWidth * bottomRatio, minHeight * topRatio);
        }

    }

    public void initDragListener() {
        if (dragListener != null && isFixed())
            return;

        dragListener = new ProtocolDragListener(this, protocolGame);
        addListener(dragListener);
    }

    private void initObjectImage() {
        objectImage = new Image(getTextureByPosition());

        changeSize();

        addActor(objectImage);

    }

    private void initLabelDescription() {
        float labelWidth = ProtocolGame.CARD_BIG_SIZE * LABEL_WITH_RATIO;

        descriptionLabel = new TablexiaLabel(cardText, new TablexiaLabel.TablexiaLabelStyle(CARD_TEXT_FONT, CARD_TEXT_COLOR));
        descriptionLabel.setSize(labelWidth, descriptionLabel.getMinHeight());
        descriptionLabel.setAlignment(Align.bottom);
        descriptionLabel.setWrap(true);
        descriptionLabel.setVisible(false);

        addActor(descriptionLabel);
    }

    private void changeImage() {
        objectImage.setDrawable(new TextureRegionDrawable(getTextureByPosition()));
    }

    private void changeSize() {
        if (isInRoom()) {
            objectImage.setSize(getMinWidth(), getMinHeight());
        } else
            objectImage.setSize(ProtocolGame.CARD_SMALL_SIZE, ProtocolGame.CARD_SMALL_SIZE);
    }

    private TextureRegion getTextureByPosition() {
        if (inRoom) {
            return objectDrawable;
        } else {
            return cardDrawable;
        }
    }

    public void destroyListener() {
        clearListeners();
        dragListener = null;
    }

//////////////////////////// CONTROL ACTION

    public void setControlColorAction(float delay, Runnable finishAction) {
        Color correctColor = new Color(correct ? Color.GREEN : Color.RED);
        if(finishAction != null) {
            objectImage.addAction(sequence(color(correctColor), Actions.delay(delay), run(finishAction)));
        }else {
            objectImage.addAction(color(correctColor));
        }
    }

//////////////////////////// MOVE ACTIONS

    public void returnObjectOnLastPosition(boolean anim, Runnable finishAction) {
        if (lastPosition == null)
            return;

        if (anim) {

            if (finishAction != null)
                addAction(sequence(moveTo(lastPosition.x, lastPosition.y, MOVE_DURATION, getInterpolation()), run(finishAction)));
            else
                addAction(moveTo(lastPosition.x, lastPosition.y, MOVE_DURATION, getInterpolation()));

        } else {
            setPosition(lastPosition.x, lastPosition.y);
        }
    }

    private Interpolation getInterpolation() {
        return inRoom ? OBJECT_INTERPOLATION : CARD_INTERPOLATION;
    }

    public void moveToUp() {
        addAction(moveTo(lastPosition.x, lastPosition.y + ProtocolGame.CARD_SMALL_SIZE / 3, DROP_ANIM_DURATION, CARD_INTERPOLATION));
    }

    public void moveToDown() {
        addAction(moveTo(lastPosition.x, lastPosition.y - ProtocolGame.CARD_SMALL_SIZE / 3, DROP_ANIM_DURATION, CARD_INTERPOLATION));
    }

    public void moveToPosition(boolean anim, float xPosition, float yPosition, Runnable finishAction) {
        if (anim) {
            if(finishAction != null) addAction(sequence(moveTo(xPosition, yPosition, DROP_ANIM_DURATION, CARD_INTERPOLATION), run(finishAction)));
            else addAction(sequence(moveTo(xPosition, yPosition, DROP_ANIM_DURATION, CARD_INTERPOLATION)));
        } else {
            setPosition(xPosition, yPosition);
        }

        lastPosition = new Vector2(xPosition, yPosition);
    }

    public boolean canPutOnPosition(RoomPosition position) {
        return type.checkPosition(position);
    }

    public void rotateObject(boolean rotate){
        this.rotate = rotate;
        objectDrawable.flip(rotate ^ objectDrawable.isFlipX(), false);

        if(canPutOnPosition(RoomPosition.WALL)) {
            changeWallPointPosition();
            initEdgePoint(WallObjectDescriptor.getDescriptorByType(type));
        }
    }

    public void middleTexture(boolean useMiddleTexture){
        if(middle == useMiddleTexture)
            return;

        objectDrawable = protocolGame.getScreenTextureRegion(type.getObjectTexturePath() + (useMiddleTexture ? MIDDLE_PREFIX : ""));
        objectImage.setDrawable(new TextureRegionDrawable(objectDrawable));
        changeSize();
        this.middle = useMiddleTexture;
    }


//////////////////////////// SCALE ACTIONS

    public void scaleCard() {
        if (!isBiggerScale()) {
            scaleToBiggerSizeWithTitle();
        } else {
            scaleToSmallSizeWithoutTitle();
        }

        scaling = true;
    }

    private void scaleToBiggerSizeWithTitle() {
        positionBeforeScale = new Vector2(objectImage.getX(), objectImage.getY());
        float xMove = objectImage.getX() - ProtocolGame.CARD_BIG_SIZE / 2;
        float yMove = getYMovePosition(objectImage.getY());

        objectImage.addAction(sequence(parallel(moveTo(xMove, yMove, BIGGER_DURATION, BIGGER_INTERPOLATION),
                scaleTo(OBJECT_BIGGER_SCALE, OBJECT_BIGGER_SCALE, BIGGER_DURATION, BIGGER_INTERPOLATION)), run(new Runnable() {
            @Override
            public void run() {
                biggerScale = true;
                scaling = false;
                descriptionLabel.setPosition(xMove, yMove + LABEL_PADDING_BOTTOM);
                descriptionLabel.setVisible(true);
                protocolGame.setBiggerScaleEvent(type);
            }
        })));
    }


    private void scaleToSmallSizeWithoutTitle() {
        descriptionLabel.setVisible(false);

        objectImage.addAction(sequence(parallel(moveTo(positionBeforeScale.x, positionBeforeScale.y, SMALL_DURATION, CARD_INTERPOLATION),
                scaleTo(1f, 1f, SMALL_DURATION, CARD_INTERPOLATION)), run(new Runnable() {
            @Override
            public void run() {
                biggerScale = false;
                scaling = false;
                protocolGame.resetDragLayoutObject();
                protocolGame.setSmallerScaleEvent(type);
            }
        })));
    }

//////////////////////////// GETTERS SETTERS

    private float getYMovePosition(float y) {
        switch (menuPosition) {
            case 0:
                return y - ProtocolGame.CARD_BIG_SIZE / 2;
            case 1:
                return y - ProtocolGame.CARD_SMALL_SIZE / 2;
            default:
                return y;
        }
    }

    public void setInRoom(boolean inRoom) {
        this.inRoom = inRoom;

        changeImage();
        changeSize();
    }

    public Vector2 getFurniturePosition() {
        float xFurniture = 0;
        float yFurniture = 0;

        if (furniture != null) {
            xFurniture = furniture.getX();
            yFurniture = furniture.getY();
        }

        return new Vector2(xFurniture, yFurniture);
    }

    public boolean isPositionOnLast() {
        return lastPosition.x == getX() && lastPosition.y == getY();
    }

    @Override
    public Vector2 getPutPointScreenPosition() {
        if(putPoint == null)
            return null;

        return new Vector2(getX() + putPoint.x, getY() + putPoint.y);
    }

    @Override
    public Vector2 getWallPointScreenPosition() {
        if(wallPoint == null)
            return null;

        return new Vector2(getX() + wallPoint.x, getY() + wallPoint.y);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public boolean isInRoom() {
        return inRoom;
    }


    public boolean isBiggerScale() {
        return biggerScale;
    }

    public void setMenuPosition(int menuPosition) {
        this.menuPosition = menuPosition;
    }

    public int getMenuPosition() {
        return menuPosition;
    }

    private boolean isFixed() {
        return fixed;
    }

    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }

    public Furniture getFurniture() {
        return furniture;
    }

    public void setFurniture(Furniture furniture) {
        this.furniture = furniture;
    }

    public WallType getWallPositionType() {
        return wallPositionType;
    }

    public void setWallPositionType(WallType wallPositionType) {
        this.wallPositionType = wallPositionType;
    }

    public boolean isScaling() {
        return scaling;
    }

    public boolean isRotate() {
        return rotate;
    }

    public Vector2 getrBPoint() {
        return rBPoint;
    }

    public Vector2 getlBPoint() {
        return lBPoint;
    }

    public Vector2 getlTPoint() {
        return lTPoint;
    }

    public Vector2 getrTPoint() {
        return rTPoint;
    }

    @Override
    public TypeObjectDescriptor getTypePosition() {
        return furniture != null ? furniture.getType() : null;
    }

    @Override
    public Vector2 getScreenPosition() {
        Vector2 furniturePosition = getFurniturePosition();
        return new Vector2(getX() + furniturePosition.x,getY() + furniturePosition.y);
    }

    public float getMinWidth(){
        return objectImage.getDrawable().getMinWidth() * scaleFactor;
    }

    public float getMinHeight(){
        return objectImage.getDrawable().getMinHeight() * scaleFactor;
    }

    @Override
    public float getImageWidth() {
        return objectDrawable.getRegionWidth() * scaleFactor;
    }

    @Override
    public float getImageHeight() {
        return objectDrawable.getRegionHeight() * scaleFactor;
    }

    @Override
    public GameObjectType getType() {
        return type;
    }

    @Override
    public TextureRegion getTexture() {
        return objectDrawable;
    }
}
