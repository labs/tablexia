/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.controller;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObject;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.TypeObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.FurnitureType;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.util.Log;


/**
 * Created by lmarik on 17.5.17.
 */

public class ObjectsController {

    private static final int BETWEEN_CIRCULAR_SECTOR        = 120;
    public  static final int VERTICAL_CIRCULAR_SECTOR       = 105;
    public  static final int HORIZONTAL_CIRCULAR_SECTOR     = 65;

    private Map<GameObjectType, List<PositionPair>> solution = new HashMap<>();
    private List<GameObjectType> objectsOnWall = new LinkedList<>();

    public ObjectsController() {

    }

//////////////////////////// INIT SOLUTION

    public void addGameObjectTypeToSolution(GameObjectType objectType) {
        if (!solution.containsKey(objectType))
            solution.put(objectType, new ArrayList<>());
    }

    public void addPositionToSolution(GameObjectType keyObject, Preposition preposition, TypeObjectDescriptor valueObject) {
        if (!solution.containsKey(keyObject)) {
            solution.put(keyObject, new ArrayList<>());
        }
        solution.get(keyObject).add(new PositionPair(preposition, valueObject));

        if(valueObject instanceof WallType)
            objectsOnWall.add(keyObject);
    }

    public void clearSolution() {
        solution.clear();
        objectsOnWall.clear();
    }

    public boolean isObjectOnWall(GameObjectType objectType){
        return objectsOnWall.contains(objectType) || objectType == GameObjectType.KIMONO;
    }

//////////////////////////// OBJECTS CONTROL

    /**
     * Control correct objects position
     *
     * @param objects - objects
     */
    public void controlPositions(HashMap<GameObjectType, GameObject> objects) {

        for (GameObject object : objects.values()) {

            List<PositionPair> positionPairs = getPositionDescriptionByGameObjectType(object.getType());
            if (positionPairs == null) {
                object.setCorrect(true);
            } else {

                boolean correct = true;

                for (int i = 0; i < positionPairs.size(); i++) {
                    PositionPair positionPair = positionPairs.get(i);
                    if (positionPair.getObjectType() instanceof GameObjectType) {
                        GameObjectType objectType = (GameObjectType) positionPair.getObjectType();
                        switch (positionPair.getPreposition()) {
                            case FRONT:
                                correct &= controlBehindFront(objects, object, objectType, Preposition.FRONT);
                                break;
                            case BEHIND:
                                correct &= controlBehindFront(objects, object, objectType, Preposition.BEHIND);
                                break;
                            case RIGHT:
                                correct &= controlLeftRight(objects, object, objectType, Preposition.RIGHT);
                                break;
                            case LEFT:
                                correct &= controlLeftRight(objects, object, objectType, Preposition.LEFT);
                                break;
                            case BETWEEN:

                                if (!(positionPairs.get(i).getObjectType() instanceof GameObjectType)) {
                                    Log.err(getClass(), "Cannot finish control. Bad class type");
                                    return;
                                }

                                i++;
                                GameObjectType second = (GameObjectType) positionPairs.get(i).getObjectType();
                                correct &= controlBetween(objects, object, objectType, second);
                                break;
                            default:
                                correct &= true;
                        }
                    } else if (positionPair.getObjectType() instanceof FurnitureType) {
                        FurnitureType furnitureType = (FurnitureType) positionPair.getObjectType();
                        correct &= (object.getFurniture() != null && object.getFurniture().getType().ordinal() == furnitureType.ordinal());
                    } else if (positionPair.getObjectType() instanceof WallType) {
                        WallType wallType = (WallType) positionPair.getObjectType();
                        correct &= (object.getWallPositionType() != null && object.getWallPositionType().ordinal() == wallType.ordinal());
                    }

                }

                object.setCorrect(correct);
            }
        }

    }

    /**
     * Check left/right position controlled object with subject
     *
     * @param objects
     * @param controlled  - controlled object
     * @param subject     - subject object type
     * @param preposition - LEFT/RIGHT
     * @return
     */
    private boolean controlLeftRight(HashMap<GameObjectType, GameObject> objects, GameObject controlled, GameObjectType subject, Preposition preposition) {
        Vector2 controlMiddle = new Vector2(controlled.getCenterScreenPosition()).add(controlled.getFurniturePosition());
        Vector2 subjectMiddle = new Vector2(objects.get(subject).getCenterScreenPosition()).add(objects.get(subject).getFurniturePosition());

        double angle = preposition == Preposition.LEFT ? 270.0 : 90.0;

        float startAngle = (float) calculationAngleToCircularSector(angle + ObjectsController.HORIZONTAL_CIRCULAR_SECTOR / 2);
        float endAngle = (float) calculationAngleToCircularSector(angle - ObjectsController.HORIZONTAL_CIRCULAR_SECTOR / 2);


        return isPointInCircularSector(controlMiddle, subjectMiddle, startAngle, endAngle);
    }

    /**
     * Check behind/front position controlled object with second one
     *
     * @param objects
     * @param controlled  - controlled object
     * @param subjectType - subject object type
     * @param preposition BEHIND/FRONT
     * @return
     */
    private boolean controlBehindFront(HashMap<GameObjectType, GameObject> objects, GameObject controlled, GameObjectType subjectType, Preposition preposition) {
        GameObject subject = objects.get(subjectType);
        Vector2 controlPosition = new Vector2(controlled.getCenterScreenPosition()).add(controlled.getFurniturePosition());
        Vector2 subjectPosition = new Vector2(subject.getCenterScreenPosition()).add(subject.getFurniturePosition());

        //Check position if control object intersects the subject object
        if (TablexiaSettings.getInstance().getBuildType() != TablexiaSettings.BuildType.ITEST && checkIntersectsBetweenObjects(controlled, subject)) {
            if (preposition == Preposition.BEHIND) return controlPosition.y > subjectPosition.y;
            else return controlPosition.y <= subjectPosition.y;
        }

        double angle = preposition == Preposition.BEHIND ? 0.0 : 180.0;

        float startAngle = (float) calculationAngleToCircularSector(angle + ObjectsController.VERTICAL_CIRCULAR_SECTOR / 2);
        float endAngle = (float) calculationAngleToCircularSector(angle - ObjectsController.VERTICAL_CIRCULAR_SECTOR / 2);

        return isPointInCircularSector(controlPosition, subjectPosition, startAngle, endAngle);
    }

    /**
     * Check between position of controlled object with two subjects
     *
     * @param objects
     * @param controlled - controlled object
     * @param subjectOne - first subject
     * @param subjectTwo - second subject
     * @return
     */
    private boolean controlBetween(HashMap<GameObjectType, GameObject> objects, GameObject controlled, GameObjectType subjectOne, GameObjectType subjectTwo) {
        Vector2 first = new Vector2(objects.get(subjectOne).getCenterScreenPosition()).add(objects.get(subjectOne).getFurniturePosition());
        Vector2 second = new Vector2(objects.get(subjectTwo).getCenterScreenPosition()).add(objects.get(subjectTwo).getFurniturePosition());

        Vector2 control = new Vector2(controlled.getCenterScreenPosition().add(controlled.getFurniturePosition()));
        double angle = 180 - computeAngle(control, first);

        float startAngle = (float) calculationAngleToCircularSector(angle + BETWEEN_CIRCULAR_SECTOR / 2);
        float endAngle = (float) calculationAngleToCircularSector(angle - BETWEEN_CIRCULAR_SECTOR / 2);

        return isPointInCircularSector(second, control, startAngle, endAngle);
    }

    private double calculationAngleToCircularSector(double angle) {
        angle = (90 - angle);

        if (Math.abs(angle) > 180) {
            angle = angle > 0 ? (angle - 360) : (angle + 360);
        }

        return angle;
    }

    private boolean isPointInCircularSector(Vector2 point, Vector2 center, float startAngle, float endAngle) {
        Vector2 rel = new Vector2(point.x - center.x, point.y - center.y);
        float angle = (float) Math.atan2(rel.y, rel.x);

        if (startAngle > endAngle) {
            float temp = startAngle;
            startAngle = endAngle;
            endAngle = temp;
            return !(angle >= Math.toRadians(startAngle) && angle <= Math.toRadians(endAngle));
        }

        return angle >= Math.toRadians(startAngle) && angle <= Math.toRadians(endAngle);
    }


    private double computeAngle(Vector2 first, Vector2 second) {
        return Math.toDegrees(Math.atan2(first.x - second.x, second.y - first.y));
    }

    private boolean checkIntersectsBetweenObjects(GameObject first, GameObject second) {
        return (first.getX() < second.getX() + second.getImageWidth()
                && (first.getX() + first.getImageWidth() > second.getX()
                && first.getY() < second.getY() + second.getImageHeight()
                && (first.getY() + first.getImageHeight()) > second.getY()));
    }

    public List<PositionPair> getPositionDescriptionByGameObjectType(GameObjectType objectType) {
        return solution.get(objectType);
    }

    public Map<GameObjectType, List<PositionPair>> getSolution() {
        return solution;
    }

    @Override
    public String toString() {
        String print = "SOLUTION: \n";

        for (GameObjectType key : solution.keySet()) {
            print += "\t" + key + "\n";
            List<PositionPair> positionPairs = solution.get(key);
            if (positionPairs != null) {
                for (PositionPair positionPair : positionPairs) {
                    print += positionPair.toString() + "\n";
                }
            }
            print += "\n";
        }

        return print;
    }
}
