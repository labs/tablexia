/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.assets;


/**
 * Created by lmarik on 4.5.17.
 */

public final class ProtocolAssets {

    private static final String     GFX_PATH                = "gfx/";
    private static final String     SFX_PATH                = "sfx/";
    private static final String     COMMON                  = "common/";

    private static final String     CARDS_PATH              = GFX_PATH +  "cards/";
    private static final String     OBJECTS_PATH            = GFX_PATH + "objects/";
    private static final String     SCREEN_PATH             = GFX_PATH + "screen/";
    private static final String     FURNITURE_PATH          = GFX_PATH + "furniture/";

//-------------------------------------- COMMON --------------------------------------

//////////////////////////// MENU

    private static final String     MENU_PATH               =  "menu/";

    public  static final String     MENU_NEXT               = GFX_PATH + MENU_PATH + "next";
    public  static final String     MENU_BACKGROUND         = GFX_PATH + MENU_PATH + "cards_widget_background";

    public  static final String     RULE_BACKGROUND         = GFX_PATH + MENU_PATH + "rule_background";

    public  static final String     BUTTON_PRESSED          = "_pressed";
    public  static final String     BUTTON_DISABLED         = "_disabled";
    public  static final String     ARROW_DOWN              = GFX_PATH + MENU_PATH + "arrow_down";
    public  static final String     ARROW_DOWN_PRESSED      = GFX_PATH + MENU_PATH + "arrow_down" + BUTTON_PRESSED;
    public  static final String     ARROW_DOWN_DISABLED     = GFX_PATH + MENU_PATH + "arrow_down" + BUTTON_DISABLED;
    public  static final String     ARROW_UP                = GFX_PATH + MENU_PATH + "arrow_up";
    public  static final String     ARROW_UP_PRESSED        = GFX_PATH + MENU_PATH + "arrow_up" + BUTTON_PRESSED;
    public  static final String     ARROW_UP_DISABLED       = GFX_PATH + MENU_PATH + "arrow_up" + BUTTON_DISABLED;

//////////////////////////// TEXT

    public static final String      COMPLETE_TEXT           = "game_protocol_complete_button";
    public static final String      VICTORY_NO_STAR_TEXT    = "game_protocol_result_0";
    public static final String      VICTORY_ONE_STAR_TEXT   = "game_protocol_result_1";
    public static final String      VICTORY_TWO_STAR_TEXT   = "game_protocol_result_2";
    public static final String      VICTORY_THREE_STAR_TEXT = "game_protocol_result_3";
    public static final String      VICTORY_SUMMARY_TEXT    = "game_protocol_victory_summary";

    public static final String      TEXT_AND                = "game_protocol_text_and";
    public static final String      TEXT_FROM               = "game_protocol_preposition_from";

    public static final String      WALL_TEXT               = "game_protocol_wall";

//////////////////////////// SOUND

    public static final String      VICTORY_NO_STAR_SOUND    = COMMON + SFX_PATH + "result_0.mp3";
    public static final String      VICTORY_ONE_STAR_SOUND   = COMMON + SFX_PATH + "result_1.mp3";
    public static final String      VICTORY_TWO_STAR_SOUND   = COMMON + SFX_PATH + "result_2.mp3";
    public static final String      VICTORY_THREE_STAR_SOUND = COMMON + SFX_PATH + "result_3.mp3";

//-------------------------------------- EASY --------------------------------------

//////////////////////////// BACKGROUND

    public  static final String     TOY_STORE_BACKGROUND    = SCREEN_PATH + "background_toy_store";

//////////////////////////// CARDS

    public  static final String     ANGEL_CARD              = CARDS_PATH + "angel";
    public  static final String     BABY_CARRIAGE_CARD      = CARDS_PATH + "baby_carriage";
    public  static final String     BAG_OF_MARBLES_CARD     = CARDS_PATH + "bag_of_marbles";
    public  static final String     BIG_DOLL_CARD           = CARDS_PATH + "big_doll";
    public  static final String     BEAR_CARD               = CARDS_PATH + "bear";
    public  static final String     BLUE_CAR_CARD           = CARDS_PATH + "blue_car";
    public  static final String     BOWLING_CARD            = CARDS_PATH + "bowling";
    public  static final String     COLOR_BALL_CARD         = CARDS_PATH + "color_ball";
    public  static final String     CUBES_TOY_CARD          = CARDS_PATH + "cubes_toy";
    public  static final String     DEVIL_CARD              = CARDS_PATH + "devil";
    public  static final String     DOG_CARD                = CARDS_PATH + "dog";
    public  static final String     DOLLHOUSE_CARD          = CARDS_PATH + "dollhouse";
    public  static final String     DRUM_CARD               = CARDS_PATH + "drum";
    public  static final String     FOOTBALL_CARD           = CARDS_PATH + "football";
    public  static final String     HEN_CARD                = CARDS_PATH + "hen";
    public  static final String     INK_STAMPS_CARD         = CARDS_PATH + "ink_stamps";
    public  static final String     NICHOLAS_CARD           = CARDS_PATH + "nicholas";
    public  static final String     SOLDIER_CARD            = CARDS_PATH + "nutcracker";
    public  static final String     PAPER_DRAKE_CARD        = CARDS_PATH + "paper_drake";
    public  static final String     PULLING_DOCK_CARD       = CARDS_PATH + "pulling_duck";
    public  static final String     RAG_DOLL_CARD           = CARDS_PATH + "rag_doll";
    public  static final String     RED_CAR_CARD            = CARDS_PATH + "red_car";
    public  static final String     ROCKING_HORSE_CARD      = CARDS_PATH + "rocking_horse";
    public  static final String     ROOSTER_CARD            = CARDS_PATH + "rooster";
    public  static final String     SLINGSHOT_CARD          = CARDS_PATH + "slingshot";
    public  static final String     SMALL_DOLL_CARD         = CARDS_PATH + "small_doll";
    public  static final String     TRAIN_CARD              = CARDS_PATH + "train";
    public  static final String     WHIPPING_TOP_CARD       = CARDS_PATH + "trompo";
    public  static final String     WHISTLE_CARD            = CARDS_PATH + "whistle";
    public  static final String     WOODEN_HORSE_CARD       = CARDS_PATH + "wooden_horse";


//////////////////////////// OBJECTS

    public  static final String     ANGEL_OBJECT            = OBJECTS_PATH + "angel";
    public  static final String     BABY_CARRIAGE_OBJECT    = OBJECTS_PATH + "baby_carriage";
    public  static final String     BAG_OF_MARBLES_OBJECT   = OBJECTS_PATH + "bag_of_marbles";
    public  static final String     BEAR_OBJECT             = OBJECTS_PATH + "bear";
    public  static final String     BIG_DOLL_OBJECT         = OBJECTS_PATH + "big_doll";
    public  static final String     BLUE_CAR_OBJECT         = OBJECTS_PATH + "blue_car";
    public  static final String     BOWLING_OBJECT          = OBJECTS_PATH + "bowling";
    public  static final String     COLOR_BALL_OBJECT       = OBJECTS_PATH + "color_ball";
    public  static final String     CUBES_TOY_OBJECT        = OBJECTS_PATH + "cubes_toy";
    public  static final String     DEVIL_OBJECT            = OBJECTS_PATH + "devil";
    public  static final String     DOG_OBJECT              = OBJECTS_PATH + "dog";
    public  static final String     DOLLHOUSE_OBJECT        = OBJECTS_PATH + "dollhouse";
    public  static final String     DRUM_OBJECT             = OBJECTS_PATH + "drum";
    public  static final String     FOOTBALL_OBJECT         = OBJECTS_PATH + "football";
    public  static final String     HEN_OBJECT              = OBJECTS_PATH + "hen";
    public  static final String     INK_STAMPS_OBJECT       = OBJECTS_PATH + "ink_stamps";
    public  static final String     NICHOLAS_OBJECT         = OBJECTS_PATH + "nicholas";
    public  static final String     SOLDIER_OBJECT          = OBJECTS_PATH + "nutcracker";
    public  static final String     PAPER_DRAKE_OBJECT      = OBJECTS_PATH + "paper_drake";
    public  static final String     PULLING_DOCK_OBJECT     = OBJECTS_PATH + "pulling_duck";
    public  static final String     RAG_DOLL_OBJECT         = OBJECTS_PATH + "rag_doll";
    public  static final String     RED_CAR_OBJECT          = OBJECTS_PATH + "red_car";
    public  static final String     ROCKING_HORSE_OBJECT    = OBJECTS_PATH + "rocking_horse";
    public  static final String     ROOSTER_OBJECT          = OBJECTS_PATH + "rooster";
    public  static final String     SLINGSHOT_OBJECT        = OBJECTS_PATH + "slingshot";
    public  static final String     SMALL_DOLL_OBJECT       = OBJECTS_PATH + "small_doll";
    public  static final String     TRAIN_OBJECT            = OBJECTS_PATH + "train";
    public  static final String     WHIPPING_TOP_OBJECT     = OBJECTS_PATH + "trompo";
    public  static final String     WHISTLE_OBJECT          = OBJECTS_PATH + "whistle";
    public  static final String     WOODEN_HORSE_OBJECT     = OBJECTS_PATH + "wooden_horse";


//////////////////////////// TEXT

    public static final String      ANGEL_TEXT              = "game_protocol_easy_angel";
    public static final String      BABY_CARRIAGE_TEXT      = "game_protocol_easy_baby_carriage";
    public static final String      BAG_OF_MARBLES_TEXT     = "game_protocol_easy_bag_of_marbles";
    public static final String      BEAR_TEXT               = "game_protocol_easy_bear";
    public static final String      BIG_DOLL_TEXT           = "game_protocol_easy_big_doll";
    public static final String      SMALL_DOLL_TEXT         = "game_protocol_easy_small_doll";
    public static final String      CAR_TEXT                = "game_protocol_easy_car";
    public static final String      BOWLING_TEXT            = "game_protocol_easy_bowling";
    public static final String      BALL_TEXT               = "game_protocol_easy_ball";
    public static final String      CUBES_TOY_TEXT          = "game_protocol_easy_cubes_toy";
    public static final String      DEVIL_TEXT              = "game_protocol_easy_devil";
    public static final String      DOG_TEXT                = "game_protocol_easy_dog";
    public static final String      DOLLHOUSE_TEXT          = "game_protocol_easy_dollhouse";
    public static final String      SMALL_HORSE_TEXT        = "game_protocol_easy_small_horse";
    public static final String      BIG_HORSE_TEXT          = "game_protocol_easy_big_horse";
    public static final String      DRUM_TEXT               = "game_protocol_easy_drum";
    public static final String      HEN_TEXT                = "game_protocol_easy_hen";
    public static final String      INK_STAMPS_TEXT         = "game_protocol_easy_ink_stamps";
    public static final String      NICHOLAS_TEXT           = "game_protocol_easy_nicholas";
    public static final String      SOLDIER_TEXT            = "game_protocol_easy_soldier";
    public static final String      DRAKE_TEXT              = "game_protocol_easy_dragon";
    public static final String      DUCK_TEXT               = "game_protocol_easy_duck";
    public static final String      ROOSTER_TEXT            = "game_protocol_easy_rooster";
    public static final String      SLINGSHOT_TEXT          = "game_protocol_easy_slingshot";
    public static final String      TRAIN_TEXT              = "game_protocol_easy_train";
    public static final String      WHIPPING_TOP_TEXT       = "game_protocol_easy_whipping_top";
    public static final String      WHISTLE_TEXT            = "game_protocol_easy_whistle";

//////////////////////////// OBJECT DESCRIPTION

    public static final String      MARBLES                 = "game_protocol_descriptor_bag_of_marbles";
    public static final String      FOR_DOLLS               = "game_protocol_descriptor_dollhouse";

//-------------------------------------- MEDIUM --------------------------------------


//////////////////////////// BACKGROUND

    public static final String JUNK_SHOP_BACKGROUND         = SCREEN_PATH + "background_junk_shop";

//////////////////////////// FURNITURE

    public static final String BIG_SHELF_LEFT               = FURNITURE_PATH + "big_shelf_left";
    public static final String BIG_SHELF_RIGHT              = FURNITURE_PATH + "big_shelf_right";
    public static final String CHEST_OF_DRAWERS_LEFT        = FURNITURE_PATH + "chest_of_drawers_left";
    public static final String CHEST_OF_DRAWERS_RIGHT       = FURNITURE_PATH + "chest_of_drawers_right";
    public static final String SMALL_SHELF_LEFT             = FURNITURE_PATH + "small_shelf_left";
    public static final String SMALL_SHELF_RIGHT            = FURNITURE_PATH + "small_shelf_right";

//////////////////////////// FURNITURE - TEXT

    public static final String F_SHELF_TEXT                 = "game_protocol_furniture_shelf";
    public static final String F_CHEST_OF_DRAWERS_TEXT      = "game_protocol_furniture_chest_of_drawers";

//////////////////////////// CARDS

    public static final String BIG_LAMP_CARD                = CARDS_PATH + "big_lamp";
    public static final String BOWL_CARD                    = CARDS_PATH + "bowl";
    public static final String BOX_WITH_JEWELERY_CARD       = CARDS_PATH + "box_with_jewelery";
    public static final String CAMERA_CARD                  = CARDS_PATH + "camera";
    public static final String CHAIR_CARD                   = CARDS_PATH + "chair";
    public static final String CHESS_TABLE_CARD             = CARDS_PATH + "chess_table";
    public static final String COLLECTION_OF_COINS_CARD     = CARDS_PATH + "collection_of_coins";
    public static final String CUCKOO_CLOCK_CARD            = CARDS_PATH + "cuckoo_clock";
    public static final String GLASS_VASE_CARD              = CARDS_PATH + "glass_vase";
    public static final String GLOBUS_CARD                  = CARDS_PATH + "globus";
    public static final String LADY_AND_PANDA_CARD          = CARDS_PATH + "lady_and_panda";
    public static final String LANDSCAPE_CARD               = CARDS_PATH + "landscape";
    public static final String MIRROR_CARD                  = CARDS_PATH + "mirror";
    public static final String PEARLS_CARD                  = CARDS_PATH + "pearls";
    public static final String PENDULUM_CLOCK_CARD          = CARDS_PATH + "pendulum_clock";
    public static final String PORCELAIN_MUG_CARD           = CARDS_PATH + "porcelain_mug";
    public static final String PORCELAIN_PLATE_CARD         = CARDS_PATH + "porcelain_plate";
    public static final String PORCELAIN_POTTY_CARD         = CARDS_PATH + "porcelain_potty";
    public static final String PORCELAIN_STATUETTE_CARD     = CARDS_PATH + "porcelain_statuette";
    public static final String PORCELAIN_VASE_CARD          = CARDS_PATH + "porcelain_vase";
    public static final String RED_KETTLE_CARD              = CARDS_PATH + "red_kettle";
    public static final String SEAT_CARD                    = CARDS_PATH + "seat";
    public static final String SMALL_LAMP_CARD              = CARDS_PATH + "small_lamp";
    public static final String TABLE_CARD                   = CARDS_PATH + "table";
    public static final String TEA_SET_CARD                 = CARDS_PATH + "tea_set";
    public static final String TELESCOPE_CARD               = CARDS_PATH + "telescope";
    public static final String TIN_PLATE_CARD               = CARDS_PATH + "tin_plate";
    public static final String UMBRELLA_CARD                = CARDS_PATH + "umbrella";
    public static final String WATCH_CARD                   = CARDS_PATH + "watch";


//////////////////////////// OBJECTS

    public static final String BIG_LAMP_OBJECT               = OBJECTS_PATH + "big_lamp";
    public static final String BOWL_OBJECT                   = OBJECTS_PATH + "bowl";
    public static final String BOX_WITH_JEWELERY_OBJECT      = OBJECTS_PATH + "box_with_jewelery";
    public static final String CAMERA_OBJECT                 = OBJECTS_PATH + "camera";
    public static final String CHAIR_OBJECT                  = OBJECTS_PATH + "chair";
    public static final String CHESS_TABLE_OBJECT            = OBJECTS_PATH + "chess_table";
    public static final String COLLECTION_OF_COINS_OBJECT    = OBJECTS_PATH + "collection_of_coins";
    public static final String CUCKOO_CLOCK_OBJECT           = OBJECTS_PATH + "cuckoo_clock";
    public static final String GLASS_VASE_OBJECT             = OBJECTS_PATH + "glass_vase";
    public static final String GLOBUS_OBJECT                 = OBJECTS_PATH + "globus";
    public static final String LADY_AND_PANDA_OBJECT         = OBJECTS_PATH + "lady_and_panda";
    public static final String LANDSCAPE_OBJECT              = OBJECTS_PATH + "landscape";
    public static final String MIRROR_OBJECT                 = OBJECTS_PATH + "mirror";
    public static final String PEARLS_OBJECT                 = OBJECTS_PATH + "pearls";
    public static final String PENDULUM_CLOCK_OBJECT         = OBJECTS_PATH + "pendulum_clock";
    public static final String PORCELAIN_MUG_OBJECT          = OBJECTS_PATH + "porcelain_mug";
    public static final String PORCELAIN_PLATE_OBJECT        = OBJECTS_PATH + "porcelain_plate";
    public static final String PORCELAIN_POTTY_OBJECT        = OBJECTS_PATH + "porcelain_potty";
    public static final String PORCELAIN_STATUETTE_OBJECT    = OBJECTS_PATH + "porcelain_statuette";
    public static final String PORCELAIN_VASE_OBJECT         = OBJECTS_PATH + "porcelain_vase";
    public static final String RED_KETTLE_OBJECT             = OBJECTS_PATH + "red_kettle";
    public static final String SEAT_OBJECT                   = OBJECTS_PATH + "seat";
    public static final String SMALL_LAMP_OBJECT             = OBJECTS_PATH + "small_lamp";
    public static final String TABLE_OBJECT                  = OBJECTS_PATH + "table";
    public static final String TEA_SET_OBJECT                = OBJECTS_PATH + "tea_set";
    public static final String TELESCOPE_OBJECT              = OBJECTS_PATH + "telescope";
    public static final String TIN_PLATE_OBJECT              = OBJECTS_PATH + "tin_plate";
    public static final String UMBRELLA_OBJECT               = OBJECTS_PATH + "umbrella";
    public static final String WATCH_OBJECT                  = OBJECTS_PATH + "watch";

//////////////////////////// TEXT

    public static final String BIG_LAMP_TEXT                = "game_protocol_medium_big_lamp";
    public static final String BOWL_TEXT                    = "game_protocol_medium_bowl";
    public static final String BOX_JEWELERY_TEXT            = "game_protocol_medium_jewelery";
    public static final String CAMERA_TEXT                  = "game_protocol_medium_camera";
    public static final String CHAIR_TEXT                   = "game_protocol_medium_chair";
    public static final String COLLECTION_TEXT              = "game_protocol_medium_collection";
    public static final String CLOCK_TEXT                   = "game_protocol_medium_clock";
    public static final String VASE_TEXT                    = "game_protocol_medium_vase";
    public static final String GLOBUS_TEXT                  = "game_protocol_medium_globus";
    public static final String LADY_TEXT                    = "game_protocol_medium_lady";
    public static final String LANDSCAPE_TEXT               = "game_protocol_medium_landscape";
    public static final String MIRROR_TEXT                  = "game_protocol_medium_mirror";
    public static final String PEARLS_TEXT                  = "game_protocol_medium_pearls";
    public static final String MUG_TEXT                     = "game_protocol_medium_mug";
    public static final String PLATE_TEXT                   = "game_protocol_medium_plate";
    public static final String POTTY_TEXT                   = "game_protocol_medium_potty";
    public static final String STATUETTE_TEXT               = "game_protocol_medium_statuette";
    public static final String KETTLE_TEXT                  = "game_protocol_medium_kettle";
    public static final String SEAT_TEXT                    = "game_protocol_medium_seat";
    public static final String SMALL_LAMP_TEXT              = "game_protocol_medium_small_lamp";
    public static final String TABLE_TEXT                   = "game_protocol_medium_table";
    public static final String TEA_SET_TEXT                 = "game_protocol_medium_set";
    public static final String TELESCOPE_TEXT               = "game_protocol_medium_telescope";
    public static final String UMBRELLA_TEXT                = "game_protocol_medium_umbrella";
    public static final String WATCH_TEXT                   = "game_protocol_medium_watch";

//////////////////////////// OBJECT DESCRIPTION

    public static final String JEWELERY                     = "game_protocol_descriptor_with_jewelery";
    public static final String COINS                        = "game_protocol_descriptor_of_coins";
    public static final String PANDA                        = "game_protocol_descriptor_and_panda";


//-------------------------------------- HARD --------------------------------------

//////////////////////////// BACKGROUND

    public static final String LABORATORY_BACKGROUND         = SCREEN_PATH + "laboratory";

//////////////////////////// FURNITURE

    public static final String SMALL_TABLE                   = FURNITURE_PATH + "small_table";
    public static final String BIG_TABLE                     = FURNITURE_PATH + "big_table";

//////////////////////////// FURNITURE - TEXT

    public static final String F_TABLE_B_TEXT                = "game_protocol_furniture_table_b";
    public static final String F_TABLE_S_TEXT                = "game_protocol_furniture_table_s";
    public static final String F_CABINET_TEXT                = "game_protocol_furniture_cabinet";

//////////////////////////// CARDS

    public static final String BIG_BOX_CARD                  = CARDS_PATH + "big_box";
    public static final String BIG_ROUND_FLASK_CARD          = CARDS_PATH + "big_round_flask";
    public static final String BLACK_INGREDIENT_CARD         = CARDS_PATH + "black_ingredient";
    public static final String BLACK_LIQUID_CARD             = CARDS_PATH + "black_liquid";
    public static final String BLUE_LIQUID_CARD              = CARDS_PATH + "blue_liquid";
    public static final String BOOK_IN_STAND_CARD            = CARDS_PATH + "book_in_stand";
    public static final String BOOKS_CARD                    = CARDS_PATH + "books";
    public static final String DIARY_CARD                    = CARDS_PATH + "diary";
    public static final String GRAY_INGREDIENT_CARD          = CARDS_PATH + "gray_ingredient";
    public static final String GREEN_LIQUID_CARD             = CARDS_PATH + "green_liquid";
    public static final String LIGHT_BURNER_CARD             = CARDS_PATH + "light_burner";
    public static final String LIGHT_OFF_BURNER_CARD         = CARDS_PATH + "light_off_burner";
    public static final String LIGHT_TUBE_CARD               = CARDS_PATH + "light_tube";
    public static final String OBLONG_FLASK_CARD             = CARDS_PATH + "oblong_flask";
    public static final String OPEN_BOOK_CARD                = CARDS_PATH + "open_book";
    public static final String ORANGE_LIQUID_CARD            = CARDS_PATH + "orange_liquid";
    public static final String PAPER_BOX_CARD                = CARDS_PATH + "paper_box";
    public static final String PEN_CARD                      = CARDS_PATH + "pen";
    public static final String PILE_OF_BOXES_CARD            = CARDS_PATH + "pile_of_boxes";
    public static final String ROUND_FLASK_CARD              = CARDS_PATH + "round_flask";
    public static final String SMALL_BOX_CARD                = CARDS_PATH + "small_box";
    public static final String SMOKE_TUBE_CARD               = CARDS_PATH + "smoke_tube";
    public static final String TUBE_CARD                     = CARDS_PATH + "tube";
    public static final String TUBES_IN_STAND_CARD           = CARDS_PATH + "tubes_in_stand";
    public static final String WHITE_INGREDIENT_CARD         = CARDS_PATH + "white_ingredient";
    public static final String YELLOW_LIQUID_CARD            = CARDS_PATH + "yellow_liquid";




//////////////////////////// OBJECTS

    public static final String BIG_BOX_OBJECT                = OBJECTS_PATH + "big_box";
    public static final String BIG_ROUND_FLASK_OBJECT        = OBJECTS_PATH + "big_round_flask";
    public static final String BLACK_INGREDIENT_OBJECT       = OBJECTS_PATH + "black_ingredient";
    public static final String BLACK_LIQUID_OBJECT           = OBJECTS_PATH + "black_liquid";
    public static final String BLUE_LIQUID_OBJECT            = OBJECTS_PATH + "blue_liquid";
    public static final String BOOK_IN_STAND_OBJECT          = OBJECTS_PATH + "book_in_stand";
    public static final String BOOKS_OBJECT                  = OBJECTS_PATH + "books";
    public static final String DIARY_OBJECT                  = OBJECTS_PATH + "diary";
    public static final String GRAY_INGREDIENT_OBJECT        = OBJECTS_PATH + "gray_ingredient";
    public static final String GREEN_LIQUID_OBJECT           = OBJECTS_PATH + "green_liquid";
    public static final String LIGHT_BURNER_OBJECT           = OBJECTS_PATH + "light_burner";
    public static final String LIGHT_OFF_BURNER_OBJECT       = OBJECTS_PATH + "light_off_burner";
    public static final String LIGHT_TUBE_OBJECT             = OBJECTS_PATH + "light_tube";
    public static final String OBLONG_FLASK_OBJECT           = OBJECTS_PATH + "oblong_flask";
    public static final String OPEN_BOOK_OBJECT              = OBJECTS_PATH + "open_book";
    public static final String ORANGE_LIQUID_OBJECT          = OBJECTS_PATH + "orange_liquid";
    public static final String PAPER_BOX_OBJECT              = OBJECTS_PATH + "paper_box";
    public static final String PEN_OBJECT                    = OBJECTS_PATH + "pen";
    public static final String PILE_OF_BOXES_OBJECT          = OBJECTS_PATH + "pile_of_boxes";
    public static final String ROUND_FLASK_OBJECT            = OBJECTS_PATH + "round_flask";
    public static final String SMALL_BOX_OBJECT              = OBJECTS_PATH + "small_box";
    public static final String SMOKE_TUBE_OBJECT             = OBJECTS_PATH + "smoke_tube";
    public static final String TUBE_OBJECT                   = OBJECTS_PATH + "tube";
    public static final String TUBES_IN_STAND_OBJECT         = OBJECTS_PATH + "tubes_in_stand";
    public static final String WHITE_INGREDIENT_OBJECT       = OBJECTS_PATH + "white_ingredient";
    public static final String YELLOW_LIQUID_OBJECT          = OBJECTS_PATH + "yellow_liquid";


//////////////////////////// TEXT

    public static final String BOX_TEXT                      = "game_protocol_hard_box";
    public static final String BOX_S_TEXT                    = "game_protocol_hard_box_s";
    public static final String FLASK_TEXT                    = "game_protocol_hard_flask";
    public static final String INGREDIENT_TEXT               = "game_protocol_hard_ingredient";
    public static final String LIQUID_TEXT                   = "game_protocol_hard_liquid";
    public static final String BOOK_TEXT                     = "game_protocol_hard_book";
    public static final String BOOKS_TEXT                    = "game_protocol_hard_books";
    public static final String DIARY_TEXT                    = "game_protocol_hard_diary";
    public static final String BURNER_TEXT                   = "game_protocol_hard_burner";
    public static final String TUBE_TEXT                     = "game_protocol_hard_tube";
    public static final String TUBES_TEXT                    = "game_protocol_hard_tubes";
    public static final String PEN_TEXT                      = "game_protocol_hard_pen";
    public static final String PILE_TEXT                     = "game_protocol_hard_pile";


//////////////////////////// OBJECT DESCRIPTION

    public static final String IN_STAND                     = "game_protocol_descriptor_in_stand";
    public static final String OF_BOXES                     = "game_protocol_descriptor_of_boxes";
    public static final String SMOKE                        = "game_protocol_descriptor_smoke";

//-------------------------------------- EASY --------------------------------------

//////////////////////////// BACKGROUND

    public static final String TRAVELER_ROOM_BACKGROUND     = SCREEN_PATH + "traveler_room";

//////////////////////////// FURNITURE

    public static final String CABINET_LEFT                 = FURNITURE_PATH + "cabinet_left";
    public static final String CABINET_RIGHT                = FURNITURE_PATH + "cabinet_right";
    public static final String ROUND_TABLE                  = FURNITURE_PATH + "round_table";

//////////////////////////// FURNITURE - TEXT

//////////////////////////// CARDS

    public static final String BAG_CARD                     = CARDS_PATH + "bag";
    public static final String BIG_BOOK_CARD                = CARDS_PATH + "big_book";
    public static final String BIG_GLOBE_CARD               = CARDS_PATH + "big_globe";
    public static final String BROWN_BOOK_CARD              = CARDS_PATH + "brown_book";
    public static final String ELEPHANT_CARD                = CARDS_PATH + "elephant";
    public static final String GIRAFFE_CARD                 = CARDS_PATH + "giraffe";
    public static final String GRAY_BOOK_CARD               = CARDS_PATH + "gray_book";
    public static final String KABUKI_CARD                  = CARDS_PATH + "kabuki";
    public static final String KIMONO_CARD                  = CARDS_PATH + "kimono";
    public static final String LETTERS_CARD                 = CARDS_PATH + "letters";
    public static final String LION_CARD                    = CARDS_PATH + "lion";
    public static final String MASK_AFRICA_CARD             = CARDS_PATH + "mask_africa";
    public static final String SHELLS_CARD                  = CARDS_PATH + "shells";
    public static final String SMALL_BOOK_CARD              = CARDS_PATH + "small_book";
    public static final String SMALL_GLOBE_CARD             = CARDS_PATH + "small_globe";
    public static final String PILE_OF_BOOKS_CARD           = CARDS_PATH + "stack_of_books";
    public static final String STAND_FOR_POSTER_CARD        = CARDS_PATH + "stand_for_poster";
    public static final String TABLE_WITH_MAPS_CARD         = CARDS_PATH + "table_with_maps";
    public static final String CHINESE_VASE_CARD            = CARDS_PATH + "chinese_vase";
    public static final String COMPASS_CARD                 = CARDS_PATH + "compass";
    public static final String TRAVEL_HAT_CARD              = CARDS_PATH + "travel_hat";

//////////////////////////// OBJECTS

    public static final String BAG_OBJECT                   = OBJECTS_PATH + "bag";
    public static final String BIG_BOOK_OBJECT             = OBJECTS_PATH + "big_book";
    public static final String BIG_GLOBE_OBJECT             = OBJECTS_PATH + "big_globe";
    public static final String BROWN_BOOK_OBJECT            = OBJECTS_PATH + "brown_book";
    public static final String ELEPHANT_OBJECT              = OBJECTS_PATH + "elephant";
    public static final String GIRAFFE_OBJECT               = OBJECTS_PATH + "giraffe";
    public static final String GRAY_BOOK_OBJECT             = OBJECTS_PATH + "gray_book";
    public static final String KABUKI_OBJECT                = OBJECTS_PATH + "kabuki";
    public static final String KIMONO_OBJECT                = OBJECTS_PATH + "kimono";
    public static final String LETTERS_OBJECT               = OBJECTS_PATH + "letters";
    public static final String LION_OBJECT                  = OBJECTS_PATH + "lion";
    public static final String MASK_AFRICA_OBJECT           = OBJECTS_PATH + "mask_africa";
    public static final String SHELLS_OBJECT                = OBJECTS_PATH + "shells";
    public static final String SMALL_BOOK_OBJECT            = OBJECTS_PATH + "small_book";
    public static final String SMALL_GLOBE_OBJECT           = OBJECTS_PATH + "small_globe";
    public static final String PILE_OF_BOOKS_OBJECT         = OBJECTS_PATH + "stack_of_books";
    public static final String STAND_FOR_POSTER_OBJECT      = OBJECTS_PATH + "stand_for_poster";
    public static final String TABLE_WITH_MAPS_OBJECT       = OBJECTS_PATH + "table_with_maps";
    public static final String CHINESE_VASE_OBJECT          = OBJECTS_PATH + "chinese_vase";
    public static final String COMPASS_OBJECT               = OBJECTS_PATH + "compass";
    public static final String TRAVEL_HAT_OBJECT            = OBJECTS_PATH + "travel_hat";

//////////////////////////// TEXT

    public static final String BAG_TEXT                     = "game_protocol_bonus_bag";
    public static final String GLOBE_TEXT                   = "game_protocol_bonus_globe";
    public static final String KABUKI_MASK_TEXT             = "game_protocol_bonus_kabuki";
    public static final String MASK_TEXT                    = "game_protocol_bonus_mask";
    public static final String LETTER_TEXT                  = "game_protocol_bonus_letter";
    public static final String KIMONO_TEXT                  = "game_protocol_bonus_kimono";
    public static final String STAND_TEXT                   = "game_protocol_bonus_stand";
    public static final String COMPASS_TEXT                 = "game_protocol_bonus_compass";
    public static final String HAT_TEXT                     = "game_protocol_bonus_hat";
    public static final String TABLE_B_TEXT                 = "game_protocol_bonus_table";
    public static final String PILE_B_TEXT                  = "game_protocol_bonus_pile";
    public static final String STATUETTE_GIRAFFE_TEXT       = "game_protocol_bonus_statuette_giraffe";
    public static final String STATUETTE_LION_TEXT          = "game_protocol_bonus_statuette_lion";
    public static final String STATUETTE_ELEPHANT_TEXT      = "game_protocol_bonus_statuette_elephant";

//////////////////////////// OBJECT DESCRIPTION

    public static final String ELEPHANT                     = "game_protocol_descriptor_elephant";
    public static final String GIRAFFE                      = "game_protocol_descriptor_giraffe";
    public static final String LION                         = "game_protocol_descriptor_lion";
    public static final String WITH_SHELLS                  = "game_protocol_descriptor_with_shells";
    public static final String OF_BOOKS                     = "game_protocol_descriptor_of_books";
    public static final String WITH_POSTERS                 = "game_protocol_descriptor_with_posters";
    public static final String WITH_MAPS                    = "game_protocol_descriptor_with_maps";
    public static final String IN_PACKAGES                  = "game_protocol_descriptor_in_packages";
}
