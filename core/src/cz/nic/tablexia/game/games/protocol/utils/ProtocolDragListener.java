/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.utils;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import cz.nic.tablexia.game.games.protocol.ProtocolGame;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObject;
import cz.nic.tablexia.util.listener.DragActorListener;

/**
 * Created by lmarik on 5.5.17.
 */

public class ProtocolDragListener extends DragActorListener {

    private static final int    DRAG_THRESHOLD      = 8;
    private int                 dragDeltaTime       = 0;

    private GameObject gameObject;
    private ProtocolGame protocolGame;

    private boolean dragging;
    private Vector2 lastObjectPosition;

    public ProtocolDragListener(GameObject gameObject, ProtocolGame protocolGame) {
        super(gameObject);

        this.protocolGame = protocolGame;
        this.gameObject = gameObject;

        dragging = false;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if(button == Input.Buttons.RIGHT) return false;
        if (pointer > 0 || protocolGame.isDragHaveAnotherObject(gameObject) || (protocolGame.isDragDisable() && !gameObject.isBiggerScale()) || gameObject.isScaling())
            return false;


        float xPosition = gameObject.getX();
        float yPosition = gameObject.getY();

        //Compute position for dragLayer
        if (!gameObject.isBiggerScale() || !protocolGame.isObjectInDraggedLayout(gameObject)) {
            if (gameObject.isInRoom()) {
                Vector2 furniturePosition = gameObject.getFurniturePosition();
                xPosition += furniturePosition.x;
                yPosition += furniturePosition.y;
            } else {
                xPosition += (protocolGame.getViewportWidth() - ProtocolGame.PANEL_WIDTH) - protocolGame.getRoomGroup().getX();
                yPosition += protocolGame.getSceneInnerBottomY() - protocolGame.getRoomGroup().getY();
            }

            protocolGame.addObjectToDragLayout(gameObject, xPosition, yPosition);
        }

        lastObjectPosition = new Vector2(xPosition, yPosition);
        gameObject.clearActions();
        gameObject.setSelected(true);
        protocolGame.setFinishButtonEnable(false);

        return super.touchDown(event, x, y, pointer, button);
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (gameObject.isBiggerScale() || !gameObject.isSelected() || gameObject.isScaling())
            return;

        super.touchDragged(event, x, y, pointer);

        if (dragDeltaTime++ == DRAG_THRESHOLD) {
            dragging = true;
        }

        if (dragging && gameObject.isInRoom()) {
            float xMax = protocolGame.getRoomGroup().getX() + gameObject.getX() + gameObject.getImageWidth();
            if (xMax >= (protocolGame.getViewportWidth() - ProtocolGame.PANEL_WIDTH)) {
                protocolGame.cardsWidgetAnimDropCards(gameObject.getY() - protocolGame.getRoomGroup().getY() + gameObject.getImageHeight() / 2);
            } else {
                protocolGame.resetAnimDropCards();
            }
        }
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (!gameObject.isSelected() || gameObject.isScaling())
            return;

        super.touchUp(event, x, y, pointer, button);

        if (!gameObject.isInRoom() && lastObjectPosition != null && gameObject.getX() == lastObjectPosition.x && gameObject.getY() == lastObjectPosition.y) {
            //high line object
            protocolGame.setDragDisable(gameObject.isBiggerScale());
            gameObject.scaleCard();
        } else {
            //set new position is move to menu or picture / not collision return last position
            if (!gameObject.isInRoom()) {

                float xRoomPosition = gameObject.getX() + ProtocolGame.CARD_SMALL_SIZE / 2 - gameObject.getImageWidth() / 2;
                float yRoomPosition = gameObject.getY() + ProtocolGame.CARD_SMALL_SIZE / 3;

                Vector2 centerPosition = new Vector2(xRoomPosition + gameObject.getCenterPosition().x, yRoomPosition + gameObject.getCenterPosition().y);
                Vector2 putPoint = gameObject.getPutPoint() != null ? new Vector2(xRoomPosition + gameObject.getPutPoint().x, yRoomPosition + gameObject.getPutPoint().y) : null;
                Vector2 wallPoint = gameObject.getWallPoint() != null ? new Vector2(xRoomPosition + gameObject.getWallPoint().x, yRoomPosition + gameObject.getWallPoint().y) : null;

                PositionTransfer positionTransfer = protocolGame.getRoomPosition(centerPosition, putPoint, wallPoint);

                if (positionTransfer == null || !gameObject.canPutOnPosition(positionTransfer.getRoomPosition())) {
                    //Bad position
                    returnObject();
                } else {
                    protocolGame.addGameObjectToRoom(gameObject, xRoomPosition, yRoomPosition, positionTransfer);
                }

            } else {

                float xMax = protocolGame.getRoomGroup().getX() + gameObject.getX() + gameObject.getImageWidth();

                if (xMax >= (protocolGame.getViewportWidth() - ProtocolGame.PANEL_WIDTH)) {
                    protocolGame.addGameObjectToMenu(gameObject);
                } else {

                    PositionTransfer correctPosition = protocolGame.getRoomPosition(gameObject.getCenterScreenPosition(), gameObject.getPutPointScreenPosition(), gameObject.getWallPointScreenPosition());

                    if (correctPosition == null || !gameObject.canPutOnPosition(correctPosition.getRoomPosition())) {
                        returnObject();
                    } else {
                        protocolGame.controlObjectPosition(gameObject, correctPosition);
                        protocolGame.checkLayoutAfterAction();
                    }
                }
            }
        }

        gameObject.setSelected(false);
        dragging = false;
        lastObjectPosition = null;
        dragDeltaTime = 0;
    }

    private void returnObject() {
        gameObject.returnObjectOnLastPosition(true, new Runnable() {
            @Override
            public void run() {
                protocolGame.resetDragLayoutObject();

                if(gameObject.isInRoom())
                    protocolGame.addObjectForOutline(gameObject, true);
            }
        });
    }
}
