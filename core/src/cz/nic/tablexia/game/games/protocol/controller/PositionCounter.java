/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.model.RoomGroup;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.util.Log;

/**
 * Created by lmarik on 3.8.17.
 */

public class PositionCounter {

    private class Shift {
        private int xShift = 0;
        private int yShift = 0;
    }

    private class PositionObject {
        GameObjectType type;
        Map<GameObjectType, Shift> neighbors;

        PositionObject(GameObjectType root) {
            this.type = root;
            neighbors = new HashMap<>();
        }

    }


    private List<PositionObject> queueMap;

    public PositionCounter() {
        queueMap = new ArrayList<>();
    }

    public void initRoots(List<GameObjectType> typeList) {
        for (GameObjectType type : typeList) {
            queueMap.add(new PositionObject(type));
        }
    }

    public void addRoot(GameObjectType type) {
        queueMap.add(new PositionObject(type));
    }

    public void computeShift(Preposition preposition, GameObjectType computeObject, GameObjectType first, GameObjectType second, int horizontalSum, int verticalSum) {
        Shift shift = new Shift();
        PositionObject root = getRoot(computeObject);
        if (root == null) {
            Log.err(getClass(), "Counting failed!");
            return;
        }

        switch (preposition) {
            case LEFT:
                shift.xShift -= RoomGroup.MIN_OBJECTS_DISTANCE / 2;
                break;
            case RIGHT:
                shift.xShift += RoomGroup.MIN_OBJECTS_DISTANCE / 2;
                break;
            case BEHIND:
                shift.yShift += RoomGroup.MIN_OBJECTS_DISTANCE / 2;
                break;
            case FRONT:
                shift.yShift -= RoomGroup.MIN_OBJECTS_DISTANCE / 2;
                break;
            case BETWEEN:
                if (second != null) {

                    Shift shiftSecond = new Shift();

                    if (verticalSum >= horizontalSum) {

                        shift.xShift -= RoomGroup.MIN_OBJECTS_DISTANCE / 2;
                        shiftSecond.xShift += RoomGroup.MIN_OBJECTS_DISTANCE / 2;

                    } else if (verticalSum < horizontalSum) {

                        shift.yShift -= RoomGroup.MIN_OBJECTS_DISTANCE / 2;
                        shiftSecond.yShift += RoomGroup.MIN_OBJECTS_DISTANCE / 2;
                    }

                    root.neighbors.put(second, shiftSecond);
                }
                break;
        }


        root.neighbors.put(first, shift);
    }

    public float[] countMiddleShifts(GameObjectType countObject) {
        float[] middleShift = new float[]{0, 0};

        Stack<PositionObject> stackChild = new Stack<>();
        Stack<PositionObject> stackParent = new Stack<>();

        stackParent.push(getRoot(countObject));
        stackChild.push(getRoot(countObject));

        // count shift by child
        while (!stackChild.isEmpty()) {
            PositionObject root = stackChild.pop();

            for (GameObjectType neighbor : root.neighbors.keySet()) {

                Shift neighborShift = root.neighbors.get(neighbor);

                middleShift[0] += neighborShift.xShift;
                middleShift[1] += neighborShift.yShift;

                stackChild.push(getRoot(neighbor));
            }
        }

        // cound shift by parent
        while (!stackParent.isEmpty()) {
            PositionObject root = stackParent.pop();

            for (PositionObject dependObject : getDependObjects(root, countObject)) {
                Shift dependShift = dependObject.neighbors.get(root.type);

                middleShift[0] -= dependShift.xShift;
                middleShift[1] -= dependShift.yShift;

                stackParent.push(dependObject);
            }
        }

        return middleShift;
    }

    private List<PositionObject> getDependObjects(PositionObject object, GameObjectType countObject) {
        List<PositionObject> depend = new ArrayList<>();

        for (PositionObject root : queueMap) {

            if (root.neighbors.containsKey(object.type)) {

                if (object.type == countObject) {
                    depend.add(root);
                    continue;
                }

                if (!root.neighbors.containsKey(countObject)) {
                    depend.add(root);
                }

            }
        }

        return depend;
    }


    private PositionObject getRoot(GameObjectType type) {
        for (PositionObject object : queueMap) {
            if (object.type == type)
                return object;
        }

        return null;
    }

    public void clearData() {
        queueMap.clear();
    }

}
