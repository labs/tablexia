/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.model.shader;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by lmarik on 29.1.18.
 */

final class Shader {
    static final Vector3 LINE_COLOR = new Vector3(255 / 255f, 228 / 255f, 181 / 255f);

    static final String FRAGMENT_SHADER =  "#ifdef GL_ES\n" +
            "precision mediump float;\n" +
            "precision mediump int;\n" +
            "#endif\n" +
            "\n" +
            "uniform sampler2D u_texture;\n" +
            "\n" +
            "uniform vec3 u_color;\n" +
            "\n" +
            "varying vec4 v_color;\n" +
            "varying vec2 v_texCoord;\n" +
            "varying vec2 one_pixel;\n" +
            "\n" +
            "#define VALUE_OUT_MAX          0.8\n" +
            "#define STEP                   1.0\n" +
            "#define OUTSIDE_OFFSET         3.0\n" +
            "#define INSIDE_OFFSET          5.0\n" +
            "#define ALPHA_EXPONENT         1.8\n" +
            "#define MINIMUM_ALPHA_CNT      5.0\n" +
            "\n" +
            "vec3 rgb2hsv(vec3 c){\n" +
            "    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);\n" +
            "    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));\n" +
            "    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));\n" +
            "\n" +
            "    float d = q.x - min(q.w, q.y);\n" +
            "    float e = 1.0e-10;\n" +
            "    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);\n" +
            "}\n" +
            "\n" +
            "float correctOutline(vec2 coordination, float offset){\n " +
            "       float alphaCnt = 0.0;\n" +
            "       bool correct = false;\n" +
            "       \n" +
            "       for(float x = -offset; x <= offset; x += STEP){\n" +
            "           for(float y = -offset; y<= offset; y+= STEP){\n" +
            "           \n" +
            "               if(x != 0.0 && y != 0.0 ){\n" +
            "               \n" +
            "                   float testAlpha = texture2D(u_texture, coordination + vec2((one_pixel.x * x), (one_pixel.y * y))).a;\n" +
            "                   if(testAlpha <= 0.1){\n" +
            "                       alphaCnt+= 1.0;\n" +
            "                   }\n" +
            "               }\n" +
            "               \n" +
            "           }\n" +
            "           \n" +
            "       }\n" +
            "       return alphaCnt;\n" +
            "}\n" +
            "\n" +
            "void main() {\n" +
            "   vec4 pixelColor = texture2D(u_texture, v_texCoord);\n" +
            "   vec4 newColor = vec4(u_color, 0.0);\n" +
            "\n" +
            "\n" +
            "   if(pixelColor.a != 0.0){\n" +
            "       vec3 hsv = rgb2hsv(vec3(pixelColor));\n" +
            "       if(hsv.z < VALUE_OUT_MAX){\n" +
            "           \n" +
            "           \n" +
            "           if(pixelColor.a < VALUE_OUT_MAX && correctOutline(v_texCoord,OUTSIDE_OFFSET) != 0.0){\n" +
            "               \n" +
            "               newColor.a = pixelColor.a;\n" +
            "               \n" +
            "           }else{\n" +
            "               float alphaCnt = correctOutline(v_texCoord,INSIDE_OFFSET);\n" +
            "               if(alphaCnt >= MINIMUM_ALPHA_CNT){\n" +
            "                   \n" +
            "                  newColor.a = (pow(alphaCnt, ALPHA_EXPONENT) / 100.0);\n" +
            "               }\n" +
            "           }\n" +
            "       \n" +
            "       }\n" +
            "   }\n" +
            "   gl_FragColor = newColor;\n" +
            "}";

    static final String VERTEX_SHADER = "uniform mat4 u_projTrans;\n" +
            "\n" +
            "attribute vec4 a_position;\n" +
            "attribute vec2 a_texCoord0;\n" +
            "attribute vec4 a_color;\n" +
            "\n" +
            "varying vec4 v_color;\n" +
            "varying vec2 v_texCoord;\n" +
            "varying vec2 one_pixel;\n" +
            "\n" +
            "uniform vec2 u_viewportInverse;\n" +
            "\n" +
            "void main() {\n" +
            "    gl_Position = u_projTrans * a_position;\n" +
            "    v_texCoord = a_texCoord0;\n" +
            "    v_color = a_color;\n" +
            "    one_pixel =  vec2(u_viewportInverse.x / 2.0, u_viewportInverse / 2.0);\n" +
            "}";

}
