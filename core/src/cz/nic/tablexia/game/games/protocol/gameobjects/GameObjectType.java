/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.gameobjects;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;
import cz.nic.tablexia.game.games.protocol.controller.RoomPosition;

/**
 * Created by lmarik on 4.5.17.
 */

public enum GameObjectType implements TypeObjectDescriptor {

    //EASY
    ANGEL               (ProtocolAssets.ANGEL_CARD,                 ProtocolAssets.ANGEL_OBJECT,                ProtocolAssets.ANGEL_TEXT,              new float[]{0.38f, 0.102f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    BABY_CARRIAGE       (ProtocolAssets.BABY_CARRIAGE_CARD,         ProtocolAssets.BABY_CARRIAGE_OBJECT,        ProtocolAssets.BABY_CARRIAGE_TEXT,      new float[]{0.76f, 0.135f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    BAG_OF_MARBLES      (ProtocolAssets.BAG_OF_MARBLES_CARD,        ProtocolAssets.BAG_OF_MARBLES_OBJECT,       ProtocolAssets.BAG_OF_MARBLES_TEXT,     new float[]{0.38f, 0.27f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    BEAR                (ProtocolAssets.BEAR_CARD,                  ProtocolAssets.BEAR_OBJECT,                 ProtocolAssets.BEAR_TEXT,               new float[]{0.43f, 0.07f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    BIG_DOLL            (ProtocolAssets.BIG_DOLL_CARD,              ProtocolAssets.BIG_DOLL_OBJECT,             ProtocolAssets.BIG_DOLL_TEXT,           new float[]{0.44f, 0.055f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    BLUE_CAR            (ProtocolAssets.BLUE_CAR_CARD,              ProtocolAssets.BLUE_CAR_OBJECT,             ProtocolAssets.CAR_TEXT,                new float[]{0.34f, 0.18f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    BOWLING             (ProtocolAssets.BOWLING_CARD,               ProtocolAssets.BOWLING_OBJECT,              ProtocolAssets.BOWLING_TEXT,            new float[]{0.44f, 0.08f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    COLOR_BALL          (ProtocolAssets.COLOR_BALL_CARD,            ProtocolAssets.COLOR_BALL_OBJECT,           ProtocolAssets.BALL_TEXT,               new float[]{0.37f, 0.178f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    CUBES_TOY           (ProtocolAssets.CUBES_TOY_CARD,             ProtocolAssets.CUBES_TOY_OBJECT,            ProtocolAssets.CUBES_TOY_TEXT,          new float[]{0.34f, 0.33f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    DEVIL               (ProtocolAssets.DEVIL_CARD,                 ProtocolAssets.DEVIL_OBJECT,                ProtocolAssets.DEVIL_TEXT,              new float[]{0.49f, 0.345f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    DOG                 (ProtocolAssets.DOG_CARD,                   ProtocolAssets.DOG_OBJECT,                  ProtocolAssets.DOG_TEXT,                new float[]{0.527f, 0.217f},  null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    DOLLHOUSE           (ProtocolAssets.DOLLHOUSE_CARD,             ProtocolAssets.DOLLHOUSE_OBJECT,            ProtocolAssets.DOLLHOUSE_TEXT,          new float[]{0.447f, 0.126f},  null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    DRUM                (ProtocolAssets.DRUM_CARD,                  ProtocolAssets.DRUM_OBJECT,                 ProtocolAssets.DRUM_TEXT,               new float[]{0.38f, 0.316f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    FOOTBALL            (ProtocolAssets.FOOTBALL_CARD,              ProtocolAssets.FOOTBALL_OBJECT,             ProtocolAssets.BALL_TEXT,               new float[]{0.41f, 0.21f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    HEN                 (ProtocolAssets.HEN_CARD,                   ProtocolAssets.HEN_OBJECT,                  ProtocolAssets.HEN_TEXT,                new float[]{0.35f, 0.18f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    INK_STAMPS          (ProtocolAssets.INK_STAMPS_CARD,            ProtocolAssets.INK_STAMPS_OBJECT,           ProtocolAssets.INK_STAMPS_TEXT,         new float[]{0.37f, 0.23f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    NICHOLAS            (ProtocolAssets.NICHOLAS_CARD,              ProtocolAssets.NICHOLAS_OBJECT,             ProtocolAssets.NICHOLAS_TEXT,           new float[]{0.38f, 0.04f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    SOLDIER             (ProtocolAssets.SOLDIER_CARD,               ProtocolAssets.SOLDIER_OBJECT,              ProtocolAssets.SOLDIER_TEXT,            new float[]{0.328f, 0.085f},  null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    PAPER_DRAKE         (ProtocolAssets.PAPER_DRAKE_CARD,           ProtocolAssets.PAPER_DRAKE_OBJECT,          ProtocolAssets.DRAKE_TEXT,              new float[]{0.346f, 0.085f},  null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    PULL_DUCK           (ProtocolAssets.PULLING_DOCK_CARD,          ProtocolAssets.PULLING_DOCK_OBJECT,         ProtocolAssets.DUCK_TEXT,               new float[]{0.389f, 0.32f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    RAG_DOLL            (ProtocolAssets.RAG_DOLL_CARD,              ProtocolAssets.RAG_DOLL_OBJECT,             ProtocolAssets.SMALL_DOLL_TEXT,         new float[]{0.41f, 0.236f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    RED_CAR             (ProtocolAssets.RED_CAR_CARD,               ProtocolAssets.RED_CAR_OBJECT,              ProtocolAssets.CAR_TEXT,                new float[]{0.34f, 0.18f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    ROCK_HORSE          (ProtocolAssets.ROCKING_HORSE_CARD,         ProtocolAssets.ROCKING_HORSE_OBJECT,        ProtocolAssets.BIG_HORSE_TEXT,          new float[]{0.417f, 0.043f},  null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    ROOSTER             (ProtocolAssets.ROOSTER_CARD,               ProtocolAssets.ROOSTER_OBJECT,              ProtocolAssets.ROOSTER_TEXT,            new float[]{0.365f, 0.032f},  null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    SLINGSHOT           (ProtocolAssets.SLINGSHOT_CARD,             ProtocolAssets.SLINGSHOT_OBJECT,            ProtocolAssets.SLINGSHOT_TEXT,          new float[]{0.548f, 0.072f},  null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    SMALL_DOLL          (ProtocolAssets.SMALL_DOLL_CARD,            ProtocolAssets.SMALL_DOLL_OBJECT,           ProtocolAssets.SMALL_DOLL_TEXT,         new float[]{0.4f, 0.037f},    null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    TRAIN               (ProtocolAssets.TRAIN_CARD,                 ProtocolAssets.TRAIN_OBJECT,                ProtocolAssets.TRAIN_TEXT,              new float[]{0.43f, 0.053f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    WHIPPING_TOP        (ProtocolAssets.WHIPPING_TOP_CARD,          ProtocolAssets.WHIPPING_TOP_OBJECT,         ProtocolAssets.WHIPPING_TOP_TEXT,       new float[]{0.385f, 0.03f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    WHISTLE             (ProtocolAssets.WHISTLE_CARD,               ProtocolAssets.WHISTLE_OBJECT,              ProtocolAssets.WHISTLE_TEXT,            new float[]{0.55f, 0.245f},   null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    WOODEN_HORSE        (ProtocolAssets.WOODEN_HORSE_CARD,          ProtocolAssets.WOODEN_HORSE_OBJECT,         ProtocolAssets.SMALL_HORSE_TEXT,        new float[]{0.389f, 0.035f},  null,   null,   new RoomPosition[]{RoomPosition.FLOOR}),
    //MEDIUM
    BIG_LAMP            (ProtocolAssets.BIG_LAMP_CARD,              ProtocolAssets.BIG_LAMP_OBJECT,             ProtocolAssets.BIG_LAMP_TEXT,           new float[]{0.526f,0.029f},   null,                         null,                       new RoomPosition[]{RoomPosition.FLOOR}),
    BOWL                (ProtocolAssets.BOWL_CARD,                  ProtocolAssets.BOWL_OBJECT,                 ProtocolAssets.BOWL_TEXT,               new float[]{0.515f,0.189f},   new float[]{0.5f, 0.2f},      null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    BOX_WITH_JEWELERY   (ProtocolAssets.BOX_WITH_JEWELERY_CARD,     ProtocolAssets.BOX_WITH_JEWELERY_OBJECT,    ProtocolAssets.BOX_JEWELERY_TEXT,       new float[]{0.333f,0.254f},   new float[]{0.45f, 0.54f},    null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    CAMERA              (ProtocolAssets.CAMERA_CARD,                ProtocolAssets.CAMERA_OBJECT,               ProtocolAssets.CAMERA_TEXT,             new float[]{0.47f,0.069f},    null,                         null,                       new RoomPosition[]{RoomPosition.FLOOR}),
    CHAIR               (ProtocolAssets.CHAIR_CARD,                 ProtocolAssets.CHAIR_OBJECT,                ProtocolAssets.CHAIR_TEXT,              new float[]{0.57f,0.033f},    null,                         null,                       new RoomPosition[]{RoomPosition.FLOOR}),
    CHESS_TABLE         (ProtocolAssets.CHESS_TABLE_CARD,           ProtocolAssets.CHESS_TABLE_OBJECT,          ProtocolAssets.TABLE_TEXT,              new float[]{0.532f,0.035f},   null,                         null,                       new RoomPosition[]{RoomPosition.FLOOR}),
    COLLECTION_OF_COINS (ProtocolAssets.COLLECTION_OF_COINS_CARD,   ProtocolAssets.COLLECTION_OF_COINS_OBJECT,  ProtocolAssets.COLLECTION_TEXT,         new float[]{0.476f,0.118f},   new float[]{0.47f, 0.51f},    null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    CUCKOO_CLOCK        (ProtocolAssets.CUCKOO_CLOCK_CARD,          ProtocolAssets.CUCKOO_CLOCK_OBJECT,         ProtocolAssets.CLOCK_TEXT,              new float[]{0.7f,0.65f},      null,                         new float[]{0.55f, 0.58f},  new RoomPosition[]{RoomPosition.WALL}),
    GLASS_VASE          (ProtocolAssets.GLASS_VASE_CARD,            ProtocolAssets.GLASS_VASE_OBJECT,           ProtocolAssets.VASE_TEXT,               new float[]{0.58f,0.09f},     new float[]{0.5f, 0.07f},     null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    GLOBUS              (ProtocolAssets.GLOBUS_CARD,                ProtocolAssets.GLOBUS_OBJECT,               ProtocolAssets.GLOBUS_TEXT,             new float[]{0.365f,0.089f},   new float[]{0.32f,0.1f},      null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    LADY_AND_PANDA      (ProtocolAssets.LADY_AND_PANDA_CARD,        ProtocolAssets.LADY_AND_PANDA_OBJECT,       ProtocolAssets.LADY_TEXT,               new float[]{0.623f,0.15f},    new float[]{0.623f,0.15f},    new float[]{0.5f,0.5f},     new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE, RoomPosition.WALL}),
    LANDSCAPE           (ProtocolAssets.LANDSCAPE_CARD,             ProtocolAssets.LANDSCAPE_OBJECT,            ProtocolAssets.LANDSCAPE_TEXT,          new float[]{0.53f,0.189f},    new float[]{0.53f,0.189f},    new float[]{0.5f,0.5f},     new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE, RoomPosition.WALL}),
    MIRROR              (ProtocolAssets.MIRROR_CARD,                ProtocolAssets.MIRROR_OBJECT,               ProtocolAssets.MIRROR_TEXT,             new float[]{0.428f,0.072f},   null,                         new float[]{0.45f,0.42f},   new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.WALL}),
    PEARLS              (ProtocolAssets.PEARLS_CARD,                ProtocolAssets.PEARLS_OBJECT,               ProtocolAssets.PEARLS_TEXT,             new float[]{0.39f,0.053f},    new float[]{0.41f, 0.2f},     null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    PENDULUM_CLOCK      (ProtocolAssets.PENDULUM_CLOCK_CARD,        ProtocolAssets.PENDULUM_CLOCK_OBJECT,       ProtocolAssets.CLOCK_TEXT,              new float[]{0.664f,0.024f},   null,                         null,                       new RoomPosition[]{RoomPosition.FLOOR}),
    PORCELAIN_MUG       (ProtocolAssets.PORCELAIN_MUG_CARD,         ProtocolAssets.PORCELAIN_MUG_OBJECT,        ProtocolAssets.MUG_TEXT,                new float[]{0.483f,0.2f},     new float[]{0.42f, 0.19f},    null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    PORCELAIN_PLATE     (ProtocolAssets.PORCELAIN_PLATE_CARD,       ProtocolAssets.PORCELAIN_PLATE_OBJECT,      ProtocolAssets.PLATE_TEXT,              new float[]{0.548f,0.25f},    new float[]{0.5f, 0.5f},      null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    PORCELAIN_POTTY     (ProtocolAssets.PORCELAIN_POTTY_CARD,       ProtocolAssets.PORCELAIN_POTTY_OBJECT,      ProtocolAssets.POTTY_TEXT,              new float[]{0.62f,0.178f},    new float[]{0.58f, 0.28f},    null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    PORCELAIN_STATUETTE (ProtocolAssets.PORCELAIN_STATUETTE_CARD,   ProtocolAssets.PORCELAIN_STATUETTE_OBJECT,  ProtocolAssets.STATUETTE_TEXT,          new float[]{0.546f,0.09f},    new float[]{0.48f, 0.15f},    null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    PORCELAIN_VASE      (ProtocolAssets.PORCELAIN_VASE_CARD,        ProtocolAssets.PORCELAIN_VASE_OBJECT,       ProtocolAssets.VASE_TEXT,               new float[]{0.61f,0.089f},    new float[]{0.55f, 0.06f},    null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    RED_KETTLE          (ProtocolAssets.RED_KETTLE_CARD,            ProtocolAssets.RED_KETTLE_OBJECT,           ProtocolAssets.KETTLE_TEXT,             new float[]{0.54f,0.11f},     new float[]{0.54f, 0.15f},    null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    SEAT                (ProtocolAssets.SEAT_CARD,                  ProtocolAssets.SEAT_OBJECT,                 ProtocolAssets.SEAT_TEXT,               new float[]{0.578f,0.059f},   null,                         null,                       new RoomPosition[]{RoomPosition.FLOOR}),
    SMALL_LAMP          (ProtocolAssets.SMALL_LAMP_CARD,            ProtocolAssets.SMALL_LAMP_OBJECT,           ProtocolAssets.SMALL_LAMP_TEXT,         new float[]{0.548f,0.09f},    new float[]{0.48f, 0.12f},    null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    TABLE               (ProtocolAssets.TABLE_CARD,                 ProtocolAssets.TABLE_OBJECT,                ProtocolAssets.TABLE_TEXT,              new float[]{0.5f,0.067f},     null,                         null,                       new RoomPosition[]{RoomPosition.FLOOR}),
    TEA_SET             (ProtocolAssets.TEA_SET_CARD,               ProtocolAssets.TEA_SET_OBJECT,              ProtocolAssets.TEA_SET_TEXT,            new float[]{0.58f,0.07f},     new float[]{0.5f, 0.28f},     null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    TELESCOPE           (ProtocolAssets.TELESCOPE_CARD,             ProtocolAssets.TELESCOPE_OBJECT,            ProtocolAssets.TELESCOPE_TEXT,          new float[]{0.55f,0.304f},    new float[]{0.5f, 0.48f},     null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    TIN_PLATE           (ProtocolAssets.TIN_PLATE_CARD,             ProtocolAssets.TIN_PLATE_OBJECT,            ProtocolAssets.PLATE_TEXT,              new float[]{0.54f,0.247f},    new float[]{0.5f, 0.5f},      null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    UMBRELLA            (ProtocolAssets.UMBRELLA_CARD,              ProtocolAssets.UMBRELLA_OBJECT,             ProtocolAssets.UMBRELLA_TEXT,           new float[]{0.266f,0.064f},   new float[]{0.25f,0.08f},     null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    WATCH               (ProtocolAssets.WATCH_CARD,                 ProtocolAssets.WATCH_OBJECT,                ProtocolAssets.WATCH_TEXT,              new float[]{0.43f,0.188f},    new float[]{0.42f, 0.5f},     null,                       new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    //HARD
    BIG_BOX             (ProtocolAssets.BIG_BOX_CARD,               ProtocolAssets.BIG_BOX_OBJECT,              ProtocolAssets.BOX_TEXT,                new float[]{0.59f,0.053f},    null,                         null,  new RoomPosition[]{RoomPosition.FLOOR}),
    BIG_ROUND_FLASK     (ProtocolAssets.BIG_ROUND_FLASK_CARD,       ProtocolAssets.BIG_ROUND_FLASK_OBJECT,      ProtocolAssets.FLASK_TEXT,              new float[]{0.49f,0.104f},    new float[]{0.44f, 0.1f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    BLACK_INGREDIENT    (ProtocolAssets.BLACK_INGREDIENT_CARD,      ProtocolAssets.BLACK_INGREDIENT_OBJECT,     ProtocolAssets.INGREDIENT_TEXT,         new float[]{0.573f,0.204f},   new float[]{0.48f, 0.2f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    BLACK_LIQUID        (ProtocolAssets.BLACK_LIQUID_CARD,          ProtocolAssets.BLACK_LIQUID_OBJECT,         ProtocolAssets.LIQUID_TEXT,             new float[]{0.57f,0.07f},     new float[]{0.5f, 0.09f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    BLUE_LIQUID         (ProtocolAssets.BLUE_LIQUID_CARD,           ProtocolAssets.BLUE_LIQUID_OBJECT,          ProtocolAssets.LIQUID_TEXT,             new float[]{0.57f,0.07f},     new float[]{0.52f, 0.1f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    BOOK_IN_STAND       (ProtocolAssets.BOOK_IN_STAND_CARD,         ProtocolAssets.BOOK_IN_STAND_OBJECT,        ProtocolAssets.BOOK_TEXT,               new float[]{0.294f,0.148f},   new float[]{0.68f, 0.18f},    null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    BOOKS               (ProtocolAssets.BOOKS_CARD,                 ProtocolAssets.BOOKS_OBJECT,                ProtocolAssets.BOOKS_TEXT,              new float[]{0.6f,0.135f},     new float[]{0.62f, 0.36f},    null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    DIARY               (ProtocolAssets.DIARY_CARD,                 ProtocolAssets.DIARY_OBJECT,                ProtocolAssets.DIARY_TEXT,              new float[]{0.51f,0.14f},     new float[]{0.5f, 0.5f},      null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    GRAY_INGREDIENT     (ProtocolAssets.GRAY_INGREDIENT_CARD,       ProtocolAssets.GRAY_INGREDIENT_OBJECT,      ProtocolAssets.INGREDIENT_TEXT,         new float[]{0.51f,0.148f},    new float[]{0.5f, 0.45f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    GREEN_LIQUID        (ProtocolAssets.GREEN_LIQUID_CARD,          ProtocolAssets.GREEN_LIQUID_OBJECT,         ProtocolAssets.LIQUID_TEXT,             new float[]{0.565f,0.148f},   new float[]{0.52f, 0.14f},    null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    LIGHT_BURNER        (ProtocolAssets.LIGHT_BURNER_CARD,          ProtocolAssets.LIGHT_BURNER_OBJECT,         ProtocolAssets.BURNER_TEXT,             new float[]{0.64f,0.07f},     new float[]{0.59f, 0.1f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    LIGHT_OFF_BURNER    (ProtocolAssets.LIGHT_OFF_BURNER_CARD,      ProtocolAssets.LIGHT_OFF_BURNER_OBJECT,     ProtocolAssets.BURNER_TEXT,             new float[]{0.61f,0.104f},    new float[]{0.52f, 0.18f},    null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    LIGHT_TUBE          (ProtocolAssets.LIGHT_TUBE_CARD,            ProtocolAssets.LIGHT_TUBE_OBJECT,           ProtocolAssets.TUBE_TEXT,               new float[]{0.38f,0.069f},    new float[]{0.38f,0.069f},    null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    OBLONG_FLASK        (ProtocolAssets.OBLONG_FLASK_CARD,          ProtocolAssets.OBLONG_FLASK_OBJECT,         ProtocolAssets.FLASK_TEXT,              new float[]{0.619f,0.085f},   new float[]{0.52f, 0.09f},    null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    OPEN_BOOK           (ProtocolAssets.OPEN_BOOK_CARD,             ProtocolAssets.OPEN_BOOK_OBJECT,            ProtocolAssets.BOOK_TEXT,               new float[]{0.66f,0.14f},     new float[]{0.5f, 0.5f},      null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    ORANGE_LIQUID       (ProtocolAssets.ORANGE_LIQUID_CARD,         ProtocolAssets.ORANGE_LIQUID_OBJECT,        ProtocolAssets.LIQUID_TEXT,             new float[]{0.53f,0.08f},     new float[]{0.52f, 0.08f},    null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    PAPER_BOX           (ProtocolAssets.PAPER_BOX_CARD,             ProtocolAssets.PAPER_BOX_OBJECT,            ProtocolAssets.BOX_TEXT,                new float[]{0.49f,0.075f},    new float[]{0.5f, 0.25f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    PEN                 (ProtocolAssets.PEN_CARD,                   ProtocolAssets.PEN_OBJECT,                  ProtocolAssets.PEN_TEXT,                new float[]{0.4f,0.16f},      new float[]{0.5f, 0.5f},      null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    PILE_OF_BOXES       (ProtocolAssets.PILE_OF_BOXES_CARD,         ProtocolAssets.PILE_OF_BOXES_OBJECT,        ProtocolAssets.PILE_TEXT,               new float[]{0.44f,0.047f},    null,                         null,  new RoomPosition[]{RoomPosition.FLOOR}),
    ROUND_FLASK         (ProtocolAssets.ROUND_FLASK_CARD,           ProtocolAssets.ROUND_FLASK_OBJECT,          ProtocolAssets.FLASK_TEXT,              new float[]{0.58f,0.136f},    new float[]{0.5f, 0.13f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    SMALL_BOX           (ProtocolAssets.SMALL_BOX_CARD,             ProtocolAssets.SMALL_BOX_OBJECT,            ProtocolAssets.BOX_S_TEXT,                new float[]{0.54f,0.17f},     new float[]{0.5f, 0.42f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    SMOKE_TUBE          (ProtocolAssets.SMOKE_TUBE_CARD,            ProtocolAssets.SMOKE_TUBE_OBJECT,           ProtocolAssets.TUBE_TEXT,               new float[]{0.358f,0.089f},   new float[]{0.358f,0.089f},   null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    TUBE                (ProtocolAssets.TUBE_CARD,                  ProtocolAssets.TUBE_OBJECT,                 ProtocolAssets.TUBE_TEXT,               new float[]{0.42f,0.12f},     new float[]{0.42f,0.12f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    TUBES_IN_STAND      (ProtocolAssets.TUBES_IN_STAND_CARD,        ProtocolAssets.TUBES_IN_STAND_OBJECT,       ProtocolAssets.TUBES_TEXT,              new float[]{0.44f,0.07f},     new float[]{0.52f, 0.23f},    null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    WHITE_INGREDIENT    (ProtocolAssets.WHITE_INGREDIENT_CARD,      ProtocolAssets.WHITE_INGREDIENT_OBJECT,     ProtocolAssets.INGREDIENT_TEXT,         new float[]{0.57f,0.14f},     new float[]{0.52f, 0.14f},    null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    YELLOW_LIQUID       (ProtocolAssets.YELLOW_LIQUID_CARD,         ProtocolAssets.YELLOW_LIQUID_OBJECT,        ProtocolAssets.LIQUID_TEXT,             new float[]{0.55f,0.104f},    new float[]{0.5f, 0.14f},     null,  new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    //BONUS
    BAG                 (ProtocolAssets.BAG_CARD,                   ProtocolAssets.BAG_OBJECT,                  ProtocolAssets.BAG_TEXT,                    new float[]{0.44f, 0.07f},  new float[]{0.62f, 0.29f},  null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    BIG_BOOK            (ProtocolAssets.BIG_BOOK_CARD,              ProtocolAssets.BIG_BOOK_OBJECT,             ProtocolAssets.BOOK_TEXT,                   new float[]{0.44f, 0.08f},  new float[]{0.5f, 0.5f},    null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    BIG_GLOBE           (ProtocolAssets.BIG_GLOBE_CARD,             ProtocolAssets.BIG_GLOBE_OBJECT,            ProtocolAssets.GLOBE_TEXT,                  new float[]{0.5f, 0.04f},   null,                       null,                    new RoomPosition[]{RoomPosition.FLOOR}),
    BROWN_BOOK          (ProtocolAssets.BROWN_BOOK_CARD,            ProtocolAssets.BROWN_BOOK_OBJECT,           ProtocolAssets.BOOK_TEXT,                   new float[]{0.62f, 0.06f},  new float[]{0.5f, 0.58f},   null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    ELEPHANT            (ProtocolAssets.ELEPHANT_CARD,              ProtocolAssets.ELEPHANT_OBJECT,             ProtocolAssets.STATUETTE_ELEPHANT_TEXT,     new float[]{0.45f, 0.1f},   new float[]{0.4f, 0.25f},   null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    GIRAFFE             (ProtocolAssets.GIRAFFE_CARD,               ProtocolAssets.GIRAFFE_OBJECT,              ProtocolAssets.STATUETTE_GIRAFFE_TEXT,      new float[]{0.5f, 0.06f},   new float[]{0.5f, 0.2f},    null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    GRAY_BOOK           (ProtocolAssets.GRAY_BOOK_CARD,             ProtocolAssets.GRAY_BOOK_OBJECT,            ProtocolAssets.BOOK_TEXT,                   new float[]{0.44f, 0.12f},  new float[]{0.5f, 0.5f},    null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    KABUKI              (ProtocolAssets.KABUKI_CARD,                ProtocolAssets.KABUKI_OBJECT,               ProtocolAssets.KABUKI_MASK_TEXT,            new float[]{0.63f, 0.35f},  null,                       new float[]{0.5f, 0.5f}, new RoomPosition[]{RoomPosition.WALL}),
    KIMONO              (ProtocolAssets.KIMONO_CARD,                ProtocolAssets.KIMONO_OBJECT,               ProtocolAssets.KIMONO_TEXT,                 new float[]{0.45f, 0.02f},  null,                       null,                    new RoomPosition[]{RoomPosition.FLOOR}),
    LETTERS             (ProtocolAssets.LETTERS_CARD,               ProtocolAssets.LETTERS_OBJECT,              ProtocolAssets.LETTER_TEXT,                 new float[]{0.31f, 0.12f},  new float[]{0.45f, 0.5f},   null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    LION                (ProtocolAssets.LION_CARD,                  ProtocolAssets.LION_OBJECT,                 ProtocolAssets.STATUETTE_LION_TEXT,         new float[]{0.5f, 0.1f},    new float[]{0.5f, 0.3f},    null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    MASK_AFRICA         (ProtocolAssets.MASK_AFRICA_CARD,           ProtocolAssets.MASK_AFRICA_OBJECT,          ProtocolAssets.MASK_TEXT,                   new float[]{0.6f, 0.45f},   null,                       new float[]{0.5f, 0.5f}, new RoomPosition[]{RoomPosition.WALL}),
    SHELLS              (ProtocolAssets.SHELLS_CARD,                ProtocolAssets.SHELLS_OBJECT,               ProtocolAssets.BOX_TEXT,                    new float[]{0.42f, 0.07f},  new float[]{0.5f, 0.5f},    null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    SMALL_BOOK          (ProtocolAssets.SMALL_BOOK_CARD,            ProtocolAssets.SMALL_BOOK_OBJECT,           ProtocolAssets.BOOK_TEXT,                   new float[]{0.57f, 0.1f},   new float[]{0.5f, 0.5f},    null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    SMALL_GLOBE         (ProtocolAssets.SMALL_GLOBE_CARD,           ProtocolAssets.SMALL_GLOBE_OBJECT,          ProtocolAssets.GLOBE_TEXT,                  new float[]{0.58f, 0.1f},   new float[]{0.56f, 0.12f},  null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    PILE_OF_BOOKS       (ProtocolAssets.PILE_OF_BOOKS_CARD,         ProtocolAssets.PILE_OF_BOOKS_OBJECT,        ProtocolAssets.PILE_B_TEXT,                 new float[]{0.47f, 0.08f},  new float[]{0.45f, 0.3f},   null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    STAND_FOR_POSTER    (ProtocolAssets.STAND_FOR_POSTER_CARD,      ProtocolAssets.STAND_FOR_POSTER_OBJECT,     ProtocolAssets.STAND_TEXT,                  new float[]{0.54f, 0.04f},  new float[]{0.52f, 0.08f},  null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    TABLE_WITH_MAPS     (ProtocolAssets.TABLE_WITH_MAPS_CARD,       ProtocolAssets.TABLE_WITH_MAPS_OBJECT,      ProtocolAssets.TABLE_B_TEXT,                new float[]{0.5f, 0.04f},   null,                       null,                    new RoomPosition[]{RoomPosition.FLOOR}),
    CHINESE_VASE        (ProtocolAssets.CHINESE_VASE_CARD,          ProtocolAssets.CHINESE_VASE_OBJECT,         ProtocolAssets.VASE_TEXT,                   new float[]{0.5f, 0.06f},   new float[]{0.5f, 0.08f},   null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    COMPASS             (ProtocolAssets.COMPASS_CARD,               ProtocolAssets.COMPASS_OBJECT,              ProtocolAssets.COMPASS_TEXT,                new float[]{0.5f, 0.1f},    new float[]{0.48f, 0.48f},  null,                    new RoomPosition[]{RoomPosition.FLOOR, RoomPosition.FURNITURE}),
    TRAVEL_HAT          (ProtocolAssets.TRAVEL_HAT_CARD,            ProtocolAssets.TRAVEL_HAT_OBJECT,           ProtocolAssets.HAT_TEXT,                    new float[]{0.62f, 0.41f},  null,                       new float[]{0.62f, 0.41f}, new RoomPosition[]{RoomPosition.WALL});

    private String          cardTexturePath;
    private String          objectTexturePath;
    private String          textPath;

    private float[]         centerRatio;
    private float[]         putRatio;
    private float[]         wallRatio;

    private RoomPosition[]  canDragPosition;

    GameObjectType(String cardTexturePath, String objectTexturePath, String textPath, float[] centerRatio, float[] putRatio,float[] wallRatio ,RoomPosition[] canDragPosition){

        this.cardTexturePath = cardTexturePath;
        this.objectTexturePath = objectTexturePath;
        this.textPath = textPath;
        this.centerRatio = centerRatio;
        this.putRatio = putRatio;
        this.wallRatio = wallRatio;
        this.canDragPosition = canDragPosition;

    }

    public String getCardTexturePath() {
        return cardTexturePath;
    }

    public String getObjectTexturePath() {
        return objectTexturePath;
    }

    public String getTextPath() {
        return textPath;
    }

    public float[] getCenterRatio() {
        return centerRatio;
    }

    public float[] getPutRatio() {
        return putRatio;
    }

    public float[] getWallRatio() {
        return wallRatio;
    }

    public RoomPosition chooseFurnitureOrWallPosition(TablexiaRandom random){
        int i = random.nextInt(1);
        return i == 0 ? RoomPosition.WALL : RoomPosition.FURNITURE;
    }

    public boolean checkPosition(RoomPosition position){
        for (RoomPosition can : canDragPosition){
            if(can == position)
                return true;
        }

        return false;
    }


    @Override
    public int getOriginType() {
        return ordinal();
    }
}
