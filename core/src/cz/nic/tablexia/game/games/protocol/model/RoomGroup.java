/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.protocol.ProtocolGame;
import cz.nic.tablexia.game.games.protocol.controller.RoomPosition;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObject;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.ObjectModel;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.EdgePoints;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.WallObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.Furniture;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.FurnitureType;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;
import cz.nic.tablexia.game.games.protocol.utils.ObjectsPositionComparator;
import cz.nic.tablexia.game.games.protocol.utils.PositionTransfer;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.util.Log;

/**
 * Created by lmarik on 9.5.17.
 */

public class RoomGroup extends Group {

    public static final int    MIN_OBJECTS_DISTANCE    = 30;
    private static final float  WALL_PADDING            = 10f;

    private int countObjects = 0;
    private ProtocolGame protocolGame;

    private Map<RoomPosition, List<ObjectModel>> roomObjects;
    private Map<RoomPosition, List<ObjectModel>> roomFurniture;

    private TablexiaSettings.BuildType buildType;

    public RoomGroup(ProtocolGame protocolGame) {
        this.protocolGame = protocolGame;

        roomObjects = new HashMap<>();
        roomFurniture = new HashMap<>();

        buildType = TablexiaSettings.getInstance().getBuildType();

        initEmptyMap();
    }


/////////////////////////////// ROOM ACTION

    public void addFurniture(Furniture furniture, RoomPosition roomPosition, float xPosition, float yPosition) {
        addActor(furniture);

        roomFurniture.get(roomPosition).add(furniture);
        furniture.setPosition(xPosition, yPosition);
        furniture.setLastPosition(new Vector2(xPosition, yPosition));
    }

    public void addObject(GameObject gameObject, float xPosition, float yPosition, PositionTransfer positionTransfer) {
        countObjects++;
        RoomPosition roomPosition = positionTransfer.getRoomPosition();
        FurnitureType furnitureType = positionTransfer.getFurnitureType();
        WallType wallType = positionTransfer.getWallType();

        if (roomPosition == RoomPosition.FURNITURE && furnitureType != null) {
            Furniture furniture = getFurnitureByType(furnitureType);

            if (furniture == null) {
                Log.err(getClass(), "Add object failed!");
                return;
            }

            xPosition -= furniture.getX();
            yPosition -= furniture.getY();

            furniture.addObjectOnFurniture(gameObject, xPosition, yPosition);
            gameObject.setFurniture(furniture);

            controlRotateOnFurniture(gameObject, furnitureType);

        } else if(roomPosition == RoomPosition.WALL && wallType != null){

            addObjectToWall(gameObject, wallType, roomPosition, xPosition, yPosition);


        }else {
            gameObject.setPosition(xPosition, yPosition);
            gameObject.setLastPosition(new Vector2(xPosition, yPosition));
            gameObject.setWallPositionType(null);

            addActor(gameObject);
            roomObjects.get(roomPosition).add(gameObject);
        }

        gameObject.setRoomPosition(roomPosition);
        controlObjectsPosition(gameObject);

        protocolGame.setDropEvent(gameObject.getType());
    }

    private void addObjectToWall(GameObject gameObject, WallType wallType, RoomPosition roomPosition, float xPosition, float yPosition) {

        WallObjectDescriptor descriptor = WallObjectDescriptor.getDescriptorByType(gameObject.getType());
        WallType objectOrientation = descriptor.getWallOrientation();

        gameObject.middleTexture(wallType == WallType.MIDDLE_WALL);
        gameObject.rotateObject(objectOrientation != wallType && wallType != WallType.MIDDLE_WALL);

        gameObject.setPosition(xPosition, yPosition);
        gameObject.setLastPosition(new Vector2(xPosition, yPosition));
        gameObject.setWallPositionType(wallType);

        addActor(gameObject);
        roomObjects.get(roomPosition).add(gameObject);

        controlWallPosition(gameObject, wallType);
    }

    public void removeObject(GameObject gameObject) {
        countObjects--;
        if (gameObject.getRoomPosition() == RoomPosition.FURNITURE) {
            Furniture furniture = gameObject.getFurniture();

            if (furniture == null) {
                Log.err(getClass(), "Remove object failed!");
                return;
            }

            furniture.removeObjectFromFurniture(gameObject);
        } else {
            removeActor(gameObject);
            roomObjects.get(gameObject.getRoomPosition()).remove(gameObject);
        }

        gameObject.setFurniture(null);
        gameObject.setRoomPosition(null);
        gameObject.setWallPositionType(null);
        gameObject.middleTexture(false);
        gameObject.rotateObject(false);
    }

    public void resetData() {
        countObjects = 0;

        roomObjects.get(RoomPosition.WALL).clear();
        roomObjects.get(RoomPosition.FLOOR).clear();

        clearObjectsOnFurniture(roomFurniture.get(RoomPosition.WALL));
        clearObjectsOnFurniture(roomFurniture.get(RoomPosition.FLOOR));

        for (Actor actor : getChildren().begin()) {
            if (actor instanceof GameObject)
                removeActor(actor);

        }
    }

    private void clearObjectsOnFurniture(List<ObjectModel> furnitures) {
        for (ObjectModel furniture : furnitures) {
            furniture.clearObjects();
        }
    }


    public void resetActor(GameObject gameObject) {
        if (gameObject.getRoomPosition() == RoomPosition.FURNITURE) {
            Furniture furniture = gameObject.getFurniture();

            float xPosition = gameObject.getX() - furniture.getX();
            float yPosition = gameObject.getY() - furniture.getY();

            gameObject.setPosition(xPosition, yPosition);
            gameObject.setLastPosition(new Vector2(xPosition, yPosition));
            furniture.addActor(gameObject);
        } else {
            addActor(gameObject);
        }

        controlObjectsPosition(gameObject);
    }

    private void initEmptyMap() {
        roomObjects.put(RoomPosition.WALL, new ArrayList<>());
        roomObjects.put(RoomPosition.FLOOR, new ArrayList<>());

        roomFurniture.put(RoomPosition.WALL, new ArrayList<>());
        roomFurniture.put(RoomPosition.FLOOR, new ArrayList<>());
    }

/////////////////////////////// CONTROL POSITION

    public void objectPositionChanges(GameObject changedObject, PositionTransfer positionTransfer) {
        RoomPosition position = positionTransfer.getRoomPosition();
        RoomPosition previousPosition = changedObject.getRoomPosition();

        if ((position == RoomPosition.FLOOR || position == RoomPosition.WALL) && previousPosition == RoomPosition.FURNITURE) {

            transferObjectFromFurnitureToRoomGroup(changedObject, position);

        } else if (position == RoomPosition.FURNITURE && previousPosition == RoomPosition.FURNITURE && positionTransfer.getFurnitureType() != changedObject.getFurniture().getType()) {

            transferObjectBetweenFurniture(changedObject, positionTransfer.getFurnitureType());

        } else if (position == RoomPosition.FURNITURE && (previousPosition == RoomPosition.FLOOR || previousPosition == RoomPosition.WALL)) {

            transferObjectFromRoomGroupToFurniture(changedObject, positionTransfer.getFurnitureType());

        } else if (position == RoomPosition.WALL && previousPosition == RoomPosition.FLOOR) {

            roomObjects.get(RoomPosition.FLOOR).remove(changedObject);
            roomObjects.get(RoomPosition.WALL).add(changedObject);
            changedObject.setLastPosition(new Vector2(changedObject.getX(), changedObject.getY()));
            addActor(changedObject);

        } else if (position == RoomPosition.FLOOR && previousPosition == RoomPosition.WALL) {

            roomObjects.get(RoomPosition.WALL).remove(changedObject);
            roomObjects.get(RoomPosition.FLOOR).add(changedObject);
            changedObject.setLastPosition(new Vector2(changedObject.getX(), changedObject.getY()));
            addActor(changedObject);

        } else if (position == changedObject.getRoomPosition()) {

            if (previousPosition == RoomPosition.FURNITURE) {
                Furniture furniture = changedObject.getFurniture();

                float xPosition = changedObject.getX() - furniture.getX();
                float yPosition = changedObject.getY() - furniture.getY();

                changedObject.setPosition(xPosition, yPosition);
                changedObject.setLastPosition(new Vector2(xPosition, yPosition));
                furniture.addActor(changedObject);

            } else {
                addActor(changedObject);
                changedObject.setLastPosition(new Vector2(changedObject.getX(), changedObject.getY()));
            }

        }

        WallType wallType = positionTransfer.getWallType();

        if (position == RoomPosition.WALL && wallType != changedObject.getWallPositionType()) {
            WallObjectDescriptor descriptor = WallObjectDescriptor.getDescriptorByType(changedObject.getType());
            changedObject.middleTexture(wallType == WallType.MIDDLE_WALL);
            changedObject.rotateObject((descriptor.getWallOrientation() != wallType && wallType != WallType.MIDDLE_WALL));
        } else if (position == RoomPosition.FURNITURE) {
            changedObject.middleTexture(false);
            controlRotateOnFurniture(changedObject, positionTransfer.getFurnitureType());
        } else if (position == RoomPosition.FLOOR) {
            changedObject.middleTexture(false);
            changedObject.rotateObject(false);
        }


        if (position == RoomPosition.WALL)
            controlWallPosition(changedObject, wallType);


        changedObject.setRoomPosition(position);
        changedObject.setWallPositionType(wallType);
        controlObjectsPosition(changedObject);
    }

    private void controlRotateOnFurniture(GameObject gameObject, FurnitureType furnitureType){
        if(!gameObject.canPutOnPosition(RoomPosition.WALL))
            return;

        WallType objectWallOrientation = WallObjectDescriptor.getDescriptorByType(gameObject.getType()).getWallOrientation();
        WallType furnitureWallOrientation = furnitureType.getWallOrientation();
        gameObject.rotateObject(objectWallOrientation != furnitureWallOrientation);
    }

    private void controlWallPosition(GameObject gameObject, WallType wallType) {
        Vector2 lBObject = new Vector2(gameObject.getX() + gameObject.getlBPoint().x, gameObject.getY() + gameObject.getlBPoint().y);
        Vector2 lTObject = new Vector2(gameObject.getX() + gameObject.getlTPoint().x, gameObject.getY() + gameObject.getlTPoint().y);
        Vector2 rBObject = new Vector2(gameObject.getX() + gameObject.getrBPoint().x, gameObject.getY() + gameObject.getrBPoint().y);
        Vector2 rTObject = new Vector2(gameObject.getX() + gameObject.getrTPoint().x, gameObject.getY() + gameObject.getrTPoint().y);

        float roomWidth = getWidth();
        float roomHeight = getHeight();

        EdgePoints wallEdgePoints = wallType.getEdgePointsByDifficulty(protocolGame.getGameDifficulty());
        Vector2 lBWall = wallEdgePoints.getlBPoint(roomWidth, roomHeight);
        Vector2 lTWall = wallEdgePoints.getlTPoint(roomWidth, roomHeight);
        Vector2 rBWall = wallEdgePoints.getrBPoint(roomWidth, roomHeight);
        Vector2 rTWall = wallEdgePoints.getrTPoint(roomWidth, roomHeight);

        boolean checkLBPoints = checkPointBetweenTwoLines(rBWall, lBWall, lTWall, lBObject);
        boolean checkLTPoints = checkPointBetweenTwoLines(rTWall, lTWall, lBWall, lTObject);
        boolean checkRBPoints = checkPointBetweenTwoLines(rTWall, rBWall, lBWall, rBObject);
        boolean checkRTPoints = checkPointBetweenTwoLines(lTWall, rTWall, rBWall, rTObject);


        if (!(checkLBPoints && checkLTPoints && checkRBPoints && checkRTPoints)) {

            int pointsCNT = (checkLBPoints ? 0 : 1) + (checkLTPoints ? 0 : 1) + (checkRBPoints ? 0 : 1) + (checkRTPoints ? 0 : 1);

            if (pointsCNT == 3) {

                boolean useLB = !checkLBPoints && checkRTPoints;
                boolean useLT = !checkLTPoints && checkRBPoints;
                boolean useRB = !checkRBPoints && checkLTPoints;
                boolean useRT = !checkRTPoints && checkLBPoints;


                Vector2 wall = useLB ? lBWall : useLT ? lTWall : useRB ? rBWall : rTWall;
                Vector2 object = useLB ? lBObject : useLT ? lTObject : useRB ? rBObject : rTObject;

                float correctX = gameObject.getX() + (wall.x - object.x) + WALL_PADDING * ((useRT || useRB) ? -1 : 1);
                float correctY = gameObject.getY() + (wall.y - object.y) + WALL_PADDING * ((useLT || useRT) ? -1 : 1);

                gameObject.moveToPosition(true, correctX, correctY, new Runnable() {
                    @Override
                    public void run() {
                        changeOutlinePosition(gameObject);
                    }
                });
            } else if (pointsCNT == 2) {

                boolean bottom = checkLTPoints && checkRTPoints;
                boolean right = checkLBPoints && checkLTPoints;
                boolean left = checkRBPoints && checkRTPoints;
                boolean top = checkLBPoints && checkRBPoints;

                Vector2[] wallPoints = new Vector2[2];
                Vector2[] objectPoints = new Vector2[2];
                float paddingX = 0.f;
                float paddingY = 0.f;

                wallPoints[0] = top ? lTWall : right ? rBWall : lBWall;
                wallPoints[1] = bottom ? rBWall : left ? lTWall : rTWall;
                Vector2 check;

                if (top) {
                    objectPoints[0] = wallType == WallType.LEFT_WALL ? rTObject : lTObject;
                    objectPoints[1] = wallType == WallType.LEFT_WALL ? rBObject : lBObject;

                    paddingY -= WALL_PADDING;
                    check = wallType == WallType.LEFT_WALL ? rTObject : lTObject;

                } else if (bottom) {
                    objectPoints[0] = wallType == WallType.LEFT_WALL ? rTObject : lTObject;
                    objectPoints[1] = wallType == WallType.LEFT_WALL ? rBObject : lBObject;

                    paddingY += WALL_PADDING;
                    check = wallType == WallType.LEFT_WALL ? rBObject : lBObject;
                } else if (right) {
                    objectPoints[0] = wallType == WallType.RIGHT_WALL ? lTObject : lBObject;
                    objectPoints[1] = wallType == WallType.RIGHT_WALL ? rTObject : rBObject;

                    paddingX -= WALL_PADDING / 2;
                    check = wallType == WallType.RIGHT_WALL ? rTObject : rBObject;
                } else {
                    objectPoints[0] = wallType == WallType.LEFT_WALL ? lTObject : lBObject;
                    objectPoints[1] = wallType == WallType.LEFT_WALL ? rTObject : rBObject;

                    paddingX += WALL_PADDING / 2;
                    check = wallType == WallType.LEFT_WALL ? lTObject : lBObject;
                }

                Vector2 intersect = intersectVerticalLines(wallPoints[0], wallPoints[1], objectPoints[0], objectPoints[1]);
                if (intersect == null)
                    return;

                float correctX = gameObject.getX() - (check.x - intersect.x) + paddingX;
                float correctY = gameObject.getY() - (check.y - intersect.y) + paddingY;

                gameObject.moveToPosition(true, correctX, correctY, new Runnable() {
                    @Override
                    public void run() {
                        changeOutlinePosition(gameObject);
                    }
                });
            }

        }

    }

    private Vector2 intersectVerticalLines(Vector2 p1, Vector2 p2, Vector2 q1, Vector2 q2) {
        float f1 = -(p2.y - p1.y) / (p1.x - p2.x);
        float f2 = -(q2.y - q1.y) / (q1.x - q2.x);
        float g1 = p1.y - f1 * p1.x;
        float g2 = q1.y - f2 * q1.x;

        if (f1 == f2)
            return null;

        if (p1.x == p2.x) return new Vector2(p1.x, f2 * p1.x + g2);
        else if (q1.x == q2.x) return new Vector2(q1.x, f1 * q1.x + g1);
        else {
            Vector2 inter = new Vector2((g2 - g1) / (f1 - f2), 0);
            inter.y = f1 * inter.x + g1;
            return inter;
        }
    }


    private boolean checkPointBetweenTwoLines(Vector2 A, Vector2 B, Vector2 C, Vector2 point) {
        float[] u = new float[]{-B.y + A.y, B.x - A.x, 0}; //general form line AB
        float[] v = new float[]{-C.y + B.y, C.x - B.x, 0}; //general form line BX

        u[2] = -u[0] * A.x - u[1] * A.y;
        v[2] = -v[0] * B.x - v[1] * B.y;

        return checkPointsInHalfSpace(u[0], u[1], u[2], C, point) && checkPointsInHalfSpace(v[0], v[1], v[2], A, point);
    }

    private boolean checkPointsInHalfSpace(float u1, float u2, float u3, Vector2 point1, Vector2 point2) {
        return ((u1 * point1.x + u2 * point1.y + u3) * (u1 * point2.x + u2 * point2.y + u3) > 0);
    }

    private void transferObjectBetweenFurniture(GameObject changedObject, FurnitureType furnitureType) {
        Furniture oldFurniture = changedObject.getFurniture();
        Furniture newFurniture = getFurnitureByType(furnitureType);

        if (oldFurniture == null || newFurniture == null) {
            Log.err(getClass(), "Transfer object failed!");
            return;
        }

        float xPosition = changedObject.getX() - newFurniture.getX();
        float yPosition = changedObject.getY() - newFurniture.getY();

        oldFurniture.removeObjectFromFurniture(changedObject);
        newFurniture.addObjectOnFurniture(changedObject, xPosition, yPosition);

        changedObject.setFurniture(newFurniture);
    }

    private void transferObjectFromFurnitureToRoomGroup(GameObject changedObject, RoomPosition position) {
        Furniture furniture = changedObject.getFurniture();

        if (furniture == null) {
            Log.err(getClass(), "Transfer object failed!");
            return;
        }

        furniture.removeObjectFromFurniture(changedObject);
        changedObject.setFurniture(null);
        changedObject.setWallPositionType(null);
        changedObject.setLastPosition(new Vector2(changedObject.getX(), changedObject.getY()));

        addActor(changedObject);
        roomObjects.get(position).add(changedObject);
    }

    private void transferObjectFromRoomGroupToFurniture(GameObject changedObject, FurnitureType furnitureType) {
        removeActor(changedObject);
        roomObjects.get(changedObject.getRoomPosition()).remove(changedObject);

        Furniture furniture = getFurnitureByType(furnitureType);

        if (furniture == null) {
            Log.err(getClass(), "Transfer object failed!");
            return;
        }

        float xPosition = changedObject.getX() - furniture.getX();
        float yPosition = changedObject.getY() - furniture.getY();

        furniture.addObjectOnFurniture(changedObject, xPosition, yPosition);
        changedObject.setFurniture(furniture);
    }

    /**
     * Control minimum object position and set objects Zindex by position on picture
     *
     * @param controlled
     */
    private void controlObjectsPosition(GameObject controlled) {

        GameObjectType bounceObjectType = null;
        if (controlled.getRoomPosition() == RoomPosition.FLOOR && buildType != TablexiaSettings.BuildType.ITEST)
            bounceObjectType = bounceObjectByMinDistance(controlled);


        //Set ZIndex
        List<ObjectModel> objectOnTheWall = roomObjects.get(RoomPosition.WALL);
        List<ObjectModel> furnitureOnTheWall = roomFurniture.get(RoomPosition.WALL);

        List<ObjectModel> objectOnTheFloor = new ArrayList<>();
        objectOnTheFloor.addAll(roomFurniture.get(RoomPosition.FLOOR));
        objectOnTheFloor.addAll(roomObjects.get(RoomPosition.FLOOR));

        Collections.sort(objectOnTheWall, new ObjectsPositionComparator());
        Collections.sort(objectOnTheFloor, new ObjectsPositionComparator());
        Collections.sort(furnitureOnTheWall, new ObjectsPositionComparator());

        int zIndex = 0;

        setZIndexForList(objectOnTheWall, zIndex);
        zIndex += objectOnTheWall.size();

        setZIndexForList(furnitureOnTheWall, zIndex);
        zIndex += furnitureOnTheWall.size();

        setZIndexForList(objectOnTheFloor, zIndex);

        if(!controlled.hasActions() && (bounceObjectType == null || bounceObjectType != controlled.getType())){
            changeOutlinePosition(controlled);
        }

    }


    private void setZIndexForList(List<ObjectModel> objectModels, int zIndex) {
        for (ObjectModel objectModel : objectModels) {
            objectModel.setZIndex(zIndex);
            objectModel.zIndexChanged();
            zIndex++;
        }
    }

    private GameObjectType bounceObjectByMinDistance(GameObject controlled) {
        ObjectModel collisionObject;

        if ((collisionObject = findMinCollisionObject(controlled, roomObjects.get(RoomPosition.FLOOR))) != null) {

            Vector2 controlledMiddle = controlled.getCenterScreenPosition();
            Vector2 collisionMiddle = collisionObject.getCenterScreenPosition();

            double dist = computeDistance(controlledMiddle, collisionMiddle);
            double angle = computeAngle(collisionMiddle, controlledMiddle);

            float dif = MIN_OBJECTS_DISTANCE - (float) dist;

            float newX = controlled.getX() + (float) (dif * Math.sin(Math.toRadians(360 - angle)));
            float newY = controlled.getY() + (float) (dif * Math.cos(Math.toRadians(360 - angle)));

            Vector2 centerPosition = new Vector2(newX + controlled.getCenterPosition().x, newY + controlled.getCenterPosition().y);
            Vector2 putPoint = controlled.getPutPoint() != null ? new Vector2(newX + controlled.getPutPoint().x, newY + controlled.getPutPoint().y) : null;
            Vector2 wallPoint = controlled.getWallPoint() != null ? new Vector2(newX + controlled.getWallPoint().x, newY + controlled.getWallPoint().y) : null;

            PositionTransfer positionTransfer = protocolGame.getRoomPosition(centerPosition, putPoint, wallPoint);
            if (positionTransfer != null && positionTransfer.getRoomPosition() == RoomPosition.FLOOR) {
                bounceObject(controlled,newX,newY);
                return controlled.getType();
            } else if (collisionObject instanceof GameObject) {
                newX = collisionObject.getX() + (float) (dif * Math.sin(Math.toRadians(180 - angle)));
                newY = collisionObject.getY() + (float) (dif * Math.cos(Math.toRadians(180 - angle)));
                bounceObject((GameObject) collisionObject, newX, newY);
                return ((GameObject) collisionObject).getType();
            }
        }

        return null;
    }

    private void changeOutlinePosition(GameObject gameObject){
        protocolGame.addObjectForOutline(gameObject, true);
    }

    private void bounceObject(GameObject object, float xPosition, float yPosition){
        object.moveToPosition(true, xPosition, yPosition, new Runnable() {
            @Override
            public void run() {
                changeOutlinePosition(object);
            }
        });
    }

    private ObjectModel findMinCollisionObject(GameObject controlled, List<ObjectModel> objects) {

        int index = -1;
        Double minDis = null;

        for (int i = 0; i < objects.size(); i++) {

            ObjectModel object = objects.get(i);

            if (!controlled.equals(object) && object instanceof GameObject) {
                Vector2 middleFirst = controlled.getCenterScreenPosition();
                Vector2 middleSecond = object.getCenterScreenPosition();
                double dis = Math.abs(computeDistance(middleFirst, middleSecond));
                if (dis < MIN_OBJECTS_DISTANCE) {

                    if (minDis == null || dis < minDis) {
                        index = i;
                        minDis = dis;
                    }

                }
            }

        }


        return index == -1 ? null : objects.get(index);
    }

    private double computeDistance(Vector2 first, Vector2 second) {
        return Math.sqrt(Math.pow(second.y - first.y, 2) + Math.pow(second.x - first.x, 2));
    }

    private double computeAngle(Vector2 first, Vector2 second) {
        return 180.0 / Math.PI * Math.atan2(first.x - second.x, second.y - first.y);
    }

/////////////////////////////// GETTERS

    public Furniture getFurnitureByType(FurnitureType type) {
        for (ObjectModel furniture : roomFurniture.get(RoomPosition.WALL)) {
            if (furniture.getType() == type && furniture instanceof Furniture) {
                return (Furniture) furniture;
            }
        }

        for (ObjectModel furniture : roomFurniture.get(RoomPosition.FLOOR)) {
            if (furniture.getType() == type && furniture instanceof Furniture) {
                return (Furniture) furniture;
            }
        }

        return null;
    }

    public int getCountObjects() {
        return countObjects;
    }
}
