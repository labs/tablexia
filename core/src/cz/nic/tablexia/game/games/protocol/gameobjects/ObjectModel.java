/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.protocol.controller.RoomPosition;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;

/**
 * Created by lmarik on 14.8.17.
 */

public abstract class ObjectModel extends Group {
    protected static final float         DEFAULT_SCALE_NONE               = 1f;
    protected static final float         DEFAULT_SCALE_DESKTOP_HD         = 0.45f;

    public abstract TypeObjectDescriptor getType();
    public abstract TextureRegion getTexture();
    public abstract float getImageWidth();
    public abstract float getImageHeight();

    protected Vector2      lastPosition;
    protected Vector2      centerPosition;      //check room position
    protected Vector2      putPoint;            //check furniture position
    protected Vector2      wallPoint;           //check wall position
    protected RoomPosition roomPosition;
    protected float        scaleFactor;

    public ObjectModel(){
        scaleFactor = TablexiaSettings.getInstance().isUseHdAssets() ?  DEFAULT_SCALE_DESKTOP_HD : DEFAULT_SCALE_NONE;
    }

    public Vector2 getCenterScreenPosition() {
        return new Vector2(getX() + centerPosition.x, getY() + centerPosition.y);
    }

    protected Image createPoint(Vector2 position, Color color){
        Image point = new Image(ApplicationAtlasManager.getInstance().getColorTexture(color));
        point.setBounds(position.x - 2.5f, position.y - 2.5f, 5f, 5f);
        addActor(point);
        return point;
    }

    public Vector2 getPutPointScreenPosition() {
        return null;
    }

    public Vector2 getWallPointScreenPosition() { return null;}

    public Vector2 getLastPosition() {
        return lastPosition;
    }

    public void setLastPosition(Vector2 lastPosition) {
        this.lastPosition = lastPosition;
    }

    public Vector2 getScreenPosition(){
        return new Vector2(getX(),getY());
    }

    public Vector2 getCenterPosition() {
        return centerPosition;
    }

    public Vector2 getPutPoint() {
        return putPoint;
    }

    public Vector2 getWallPoint() {
        return wallPoint;
    }

    public RoomPosition getRoomPosition() {
        return roomPosition;
    }

    public TypeObjectDescriptor getTypePosition(){
        return null;
    }

    public void setRoomPosition(RoomPosition roomPosition) {
        this.roomPosition = roomPosition;
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    public void zIndexChanged(){

    }

    public void clearObjects(){

    }

}
