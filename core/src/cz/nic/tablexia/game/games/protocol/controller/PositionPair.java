/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.controller;


import cz.nic.tablexia.game.games.protocol.gameobjects.TypeObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;

/**
 * Created by lmarik on 17.5.17.
 */

public class PositionPair {

    private Preposition             preposition;
    private TypeObjectDescriptor    objectType;

    public PositionPair(Preposition preposition,TypeObjectDescriptor objectType){
        this.preposition = preposition;
        this.objectType = objectType;
    }

    public Preposition getPreposition() {
        return preposition;
    }

    public TypeObjectDescriptor getObjectType() {
        return objectType;
    }

    @Override
    public String toString() {
        return "PRE: " + preposition + " -> " + objectType;
    }
}
