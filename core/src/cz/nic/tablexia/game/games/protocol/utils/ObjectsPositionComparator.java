/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.utils;

import java.util.Comparator;

import cz.nic.tablexia.game.games.protocol.gameobjects.ObjectModel;

/**
 * Created by lmarik on 15.8.17.
 */

public class ObjectsPositionComparator implements Comparator<ObjectModel> {

    @Override
    public int compare(ObjectModel o1, ObjectModel o2) {
        return ((o2.getLastPosition().y + o2.getCenterPosition().y) > (o1.getLastPosition().y + o1.getCenterPosition().y) ? 1 : -1);
    }
}
