/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.nic.tablexia.game.games.protocol.ProtocolGame;
import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObject;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.button.TablexiaButton;


/**
 * Created by lmarik on 5.5.17.
 */

public class CardsWidget extends Group {

    public static final String  UP_BUTTON   = "up button";
    public static final String  DOWN_BUTTON = "down button";

    private static final int    CARD_MARGIN             = 10;
    private static final float  UP_DOWN_SIZE          = 50f;
    private static final int    BUTTON_MARGIN           = 5;
    public  static final int    VISIBLE_CARDS           = 3;


    private ProtocolGame protocolGame;

    private List<Actor>             cards           = new ArrayList<>();
    private List<GameObjectType>    visibleTypes    = new ArrayList<>();

    private int         countCards          = 0;
    private int         startCardIndex      = 0;
    private int         lastCollisionIndex   = -1;

    private TablexiaButton upButton;
    private TablexiaButton downButton;

    public CardsWidget(ProtocolGame protocolGame) {
        this.protocolGame = protocolGame;
        initUpDownButton();
    }

/////////////////////////////// INIT MENU COMPONENT

    private void initUpDownButton() {

        upButton = createButton(ProtocolAssets.ARROW_UP, ProtocolAssets.ARROW_UP_PRESSED, ProtocolAssets.ARROW_UP_DISABLED);
        upButton.setName(UP_BUTTON);
        upButton.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                upWidget();
            }
        });

        downButton = createButton(ProtocolAssets.ARROW_DOWN, ProtocolAssets.ARROW_DOWN_PRESSED, ProtocolAssets.ARROW_DOWN_DISABLED);
        downButton.setName(DOWN_BUTTON);
        downButton.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                downWidget();

            }
        });

        upButton.setEnabled(false);
        downButton.setEnabled(false);

        addActor(upButton);
        addActor(downButton);
    }

    private TablexiaButton createButton(String texture, String texturePressed, String disableTexture) {
        TablexiaButton button = new TablexiaButton(null, false, protocolGame.getScreenTextureRegion(texture), protocolGame.getScreenTextureRegion(texturePressed), null, protocolGame.getScreenTextureRegion(disableTexture));
        button.adaptiveSizePositionFix(false);
        button.setEnabled();

        return button;
    }


    public void screenResize() {
        upButton.setBounds(getWidth() / 2 - UP_DOWN_SIZE / 2, getHeight() - (BUTTON_MARGIN + UP_DOWN_SIZE), UP_DOWN_SIZE, UP_DOWN_SIZE);
        downButton.setBounds(getWidth() / 2 - UP_DOWN_SIZE / 2, BUTTON_MARGIN, UP_DOWN_SIZE, UP_DOWN_SIZE);
        resetCardActors();
    }


/////////////////////////////// BUTTONS ACTION

    private void upWidget() {
        startCardIndex--;
        resetCardActors();
    }

    private void downWidget() {
        startCardIndex++;
        resetCardActors();
    }

    private void initTouchableButton() {
        downButton.setEnabled((countCards - startCardIndex) > VISIBLE_CARDS);
        upButton.setEnabled(startCardIndex > 0);
    }

    public void setEnableButton(boolean enableButton) {
        if (!enableButton) {
            upButton.setEnabled(enableButton);
            downButton.setEnabled(enableButton);
            return;
        }

        initTouchableButton();
    }

/////////////////////////////// MENU ACTION

    public void addGameObject(GameObject gameObject) {
        addCard(gameObject, lastCollisionIndex == -1 ? countCards : lastCollisionIndex + startCardIndex);
        countCards++;
        resetCardActors();

        Log.info(getClass(), gameObject.getType() + " added to menu");
    }

    private void addCard(Actor card, int position) {

        if (card instanceof GameObject) {
            ((GameObject) card).setInRoom(false);
        }

        if (position > cards.size()) {
            cards.add(card);
        } else {
            cards.add(position, card);
        }
        if (lastCollisionIndex == 3)
            downWidget();
    }

    public void removeCard(GameObject object) {
        if (startCardIndex != 0) {
            startCardIndex--;
        }
        Log.info(getClass(), object.getType() + " remove from menu");

        cards.remove(object);
        countCards--;

        resetCardActors();
    }


    private void resetCardActors() {

        initTouchableButton();
        resetAnimDropCards();
        visibleTypes.clear();
        removeChildren();
        int iteration = 0;
        int listIndex = startCardIndex;

        while (iteration < VISIBLE_CARDS) {

            float xPosition = getWidth() / 2 - ProtocolGame.CARD_SMALL_SIZE / 2;
            float yPosition = getYPositionByIndex(iteration);

            if (listIndex < cards.size()) {
                Actor card = cards.get(listIndex);

                if (card instanceof GameObject) {
                    GameObject gameObject = (GameObject) card;
                    if (!gameObject.isBiggerScale()) {
                        gameObject.setLastPosition(new Vector2(xPosition, yPosition));
                        gameObject.setMenuPosition(iteration);
                        visibleTypes.add(((GameObject) card).getType());
                        gameObject.setPosition(xPosition, yPosition);
                        addActor(gameObject);
                    }
                } else {
                    card.setPosition(xPosition, yPosition);
                    addActor(card);
                }
            } else {
                Actor empty = new EmptyCard(protocolGame);
                empty.setPosition(xPosition, yPosition);
                addActor(empty);
            }

            iteration++;
            listIndex++;
        }
    }

    private void removeChildren() {
        for (Actor actor : getChildren().begin()) {
            if (actor instanceof EmptyCard || actor instanceof GameObject)
                removeActor(actor);

        }
    }

    public void resetData() {
        cards.clear();
        removeChildren();
    }

/////////////////////////////// DROP ANIMATION

    public void prepareDropAnimCards(float dragYPosition) {

        int collisionIndex = getAnimIndexCards(dragYPosition);

        if (collisionIndex == -1) {
            resetAnimDropCards();
            lastCollisionIndex = -1;
            return;
        }

        if (collisionIndex == lastCollisionIndex) {
            return;
        }

        moveUp(collisionIndex);
        moveDown(collisionIndex);

        lastCollisionIndex = collisionIndex;
    }

    private int getAnimIndexCards(float dragYPosition) {
        Actor firstCard = getCardByIndex(startCardIndex);
        Actor secondCard = getCardByIndex(startCardIndex + 1);
        Actor thirdCard = getCardByIndex(startCardIndex + 2);

        if (firstCard != null && dragYPosition > firstCard.getY() + ProtocolGame.CARD_SMALL_SIZE / 2)
            return 0;
        else if (firstCard != null && secondCard != null && dragYPosition < firstCard.getY() + ProtocolGame.CARD_SMALL_SIZE / 2 && dragYPosition >= secondCard.getY() + ProtocolGame.CARD_SMALL_SIZE / 2)
            return 1;
        else if (secondCard != null && thirdCard != null && dragYPosition < secondCard.getY() + ProtocolGame.CARD_SMALL_SIZE / 2 && dragYPosition >= thirdCard.getY() + ProtocolGame.CARD_SMALL_SIZE / 2)
            return 2;
        else if (thirdCard != null && dragYPosition < thirdCard.getY() + +ProtocolGame.CARD_SMALL_SIZE / 2)
            return 3;

        return -1;
    }

    public void resetAnimDropCards() {
        int iteration = 0;

        while (iteration < VISIBLE_CARDS) {
            resetAnimDrop(iteration + startCardIndex);
            iteration++;
        }

        lastCollisionIndex = -1;
    }

    private void resetAnimDrop(int index) {
        if (index > cards.size() - 1 || !(cards.get(index) instanceof GameObject))
            return;

        GameObject gameObject = (GameObject) cards.get(index);

        gameObject.clearActions();

        if (gameObject.getLastPosition() != null && !gameObject.isPositionOnLast()) {
            gameObject.returnObjectOnLastPosition(false, null);
        }

    }

    public void resetCard(int cardIndex) {
        if (cardIndex == -1)
            return;

        float xPosition = getWidth() / 2 - ProtocolGame.CARD_SMALL_SIZE / 2;
        float yPosition = getYPositionByIndex(cardIndex);

        Actor card = cards.get(cardIndex + startCardIndex);

        if (card instanceof GameObject) {
            GameObject object = (GameObject) card;

            object.setLastPosition(new Vector2(xPosition, yPosition));
            object.setMenuPosition(cardIndex);
            card.setPosition(xPosition, yPosition);

            addActor(card);
        }
    }


/////////////////////////////// MENU CARDS ACTION

    private void moveDown(int collisionIndex) {
        int iteration = collisionIndex;

        int cardIndex = startCardIndex + collisionIndex;

        while (iteration < VISIBLE_CARDS) {

            if (cardIndex > cards.size() - 1) {
                break;
            }

            if (cards.get(cardIndex) instanceof GameObject) {
                cards.get(cardIndex).setZIndex(VISIBLE_CARDS + 2);
                ((GameObject) cards.get(cardIndex)).moveToDown();
            }

            cardIndex++;
            iteration++;
        }
    }

    private void moveUp(int collisionIndex) {
        int iteration = collisionIndex;
        int cardIndex = collisionIndex + startCardIndex;

        while (iteration > 0) {
            iteration--;
            cardIndex--;

            if (cardIndex > cards.size() - 1) {
                break;
            }

            if (cards.get(cardIndex) instanceof GameObject) {
                cards.get(cardIndex).setZIndex(VISIBLE_CARDS + 2);
                ((GameObject) cards.get(cardIndex)).moveToUp();
            }

        }
    }


/////////////////////////////// GETTERS

    private Actor getCardByIndex(int index) {
        if (index >= cards.size())
            return null;

        return cards.get(index);
    }

    private float getYPositionByIndex(int index) {
        return upButton.getY() - ((index + 1) * (ProtocolGame.CARD_SMALL_SIZE + CARD_MARGIN));
    }

////////////////////////////// TEST

    public List<Actor> getVisibleCards() {
        return Arrays.asList(
                getCardByIndex(startCardIndex),
                getCardByIndex(startCardIndex + 1),
                getCardByIndex(startCardIndex + 2)
        );
    }

    public int getCountCards() {
        return countCards;
    }

    public boolean isGameObjectVisible(GameObjectType gameObjectType) {
        return visibleTypes.contains(gameObjectType);
    }

}
