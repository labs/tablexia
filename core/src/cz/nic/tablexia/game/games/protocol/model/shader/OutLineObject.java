/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.model.shader;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;

import cz.nic.tablexia.game.games.protocol.gameobjects.ObjectModel;

/**
 * Created by lmarik on 12.3.18.
 */

public class OutLineObject extends Group {

    public enum OutLineObjectType{
        FURNITURE, GAMEOBJECT
    }

    private OutLineObjectType       outLineObjectType;
    private ObjectModel             parent;
    private OutlineObjectDescriptor descriptor;
    private ShaderProgram           shaderProgram;
    private float                   atlasWidth;
    private float                   atlasHeight;

    public OutLineObject(OutLineObjectType outLineObjectType,ObjectModel parent,OutlineObjectDescriptor descriptor, ShaderProgram shaderProgram) {
        this.outLineObjectType = outLineObjectType;
        this.parent = parent;
        this.descriptor = descriptor;
        this.shaderProgram = shaderProgram;

        atlasWidth = descriptor.getTextureRegion().getTexture().getWidth() * parent.getScaleFactor();
        atlasHeight = descriptor.getTextureRegion().getTexture().getHeight()* parent.getScaleFactor();

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();
        batch.begin();

        batch.end();

        shaderProgram.begin();
        shaderProgram.setUniformf("u_viewportInverse", new Vector2(1f / atlasWidth, 1f / atlasHeight));
        shaderProgram.setUniformf("u_color", Shader.LINE_COLOR);
        shaderProgram.end();
        batch.setShader(shaderProgram);
        batch.begin();

        batch.draw(descriptor.getTextureRegion(), getX() + descriptor.getScreenPosition().x, getY() + descriptor.getScreenPosition().y, parent.getImageWidth(), parent.getImageHeight());

        batch.end();
        batch.setShader(null);
        batch.begin();
    }

    @Override
    public ObjectModel getParent() {
        return parent;
    }

    public OutLineObjectType getOutLineObjectType() {
        return outLineObjectType;
    }

    public OutlineObjectDescriptor getDescriptor() {
        return descriptor;
    }
}
