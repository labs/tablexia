/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.model.gameprotocol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;
import cz.nic.tablexia.game.games.protocol.controller.ObjectsController;
import cz.nic.tablexia.game.games.protocol.controller.PositionCounter;
import cz.nic.tablexia.game.games.protocol.controller.RoomPosition;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.TypeObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.AbstractObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.AbstractObjectDescriptor.Verb;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.FurnitureType;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;
import cz.nic.tablexia.game.games.protocol.model.ProtocolGameState;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;

/**
 * Created by lmarik on 12.5.17.
 */
public abstract class ProtocolGenerator {

    private     static final String                 NEXT_BIG_CHAR_PATTERN               = "<BIG>";
    public      static final String                 EMPTY_CHAR                          = "<EMPTY>";
    private     static final int                    MAX_FURNITURE                       = 2;

    public abstract String getWallTextWithAdjective(AbstractTablexiaScreen screen, AbstractObjectDescriptor descriptor, Preposition preposition);
    public abstract String getObjectCardText(AbstractTablexiaScreen screen,GameObjectType objectType);
    public abstract String getObjectTextWithAdjective(AbstractTablexiaScreen screen, GameObjectType objectType, Preposition preposition, AbstractObjectDescriptor descriptor);
    public abstract String getFurnitureTextWithAdjective(AbstractTablexiaScreen screen, FurnitureType furnitureType,Preposition preposition, AbstractObjectDescriptor descriptor);
    public abstract AbstractObjectDescriptor getObjectDescriptorByObjectType(GameObjectType objectType);
    public abstract AbstractObjectDescriptor getFurnitureDescriptorByType(FurnitureType furnitureType);
    public abstract AbstractObjectDescriptor getWallDescriptorByType(WallType wallType);

    protected interface TextDescriptor{
        List <SentenceDescriptor> getSentenceDescriptors();
        int getSentenceCnt();
        Integer[] getIndexDescriptorBySentect(Sentence sentence);
    }

/////////////////////////////// DESCRIPTORS

    public enum ProtocolText {

        TEXT_2_1(2,    1, false, new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_3_1(3,    2, false, new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_4_1(4,    3, false, new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_4_2(4,    2, true,  new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR}),
        TEXT_4_3(4,    1, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL}),
        TEXT_4_4(4,    0, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL}),
        TEXT_5_1(5,    4, false, new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_5_2(5,    3, true,  new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_5_3(5,    2, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL}),
        TEXT_5_4(5,    1, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL,SentencePosition.FURNITURE_WALL,SentencePosition.FLOOR,SentencePosition.FLOOR,SentencePosition.FURNITURE_WALL}),
        TEXT_5_5(5,    0, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL}),
        TEXT_6_1(6,    4, false, new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_6_2(6,    4, true,  new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_6_3(6,    3, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL,SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_6_4(6,    2, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL,SentencePosition.FLOOR,SentencePosition.FLOOR,SentencePosition.FLOOR,SentencePosition.FURNITURE_WALL,SentencePosition.FURNITURE_WALL}),
        TEXT_6_5(6,    1, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL,SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL}),
        TEXT_6_6(6,    0, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL,SentencePosition.FURNITURE_WALL,SentencePosition.FURNITURE_WALL,SentencePosition.FURNITURE_WALL,SentencePosition.FURNITURE_WALL,SentencePosition.FURNITURE_WALL}),
        TEXT_7_1(7,    5, false, new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_7_2(7,    4, true,  new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR,SentencePosition.FLOOR,SentencePosition.FLOOR}),
        TEXT_7_3(7,    4, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL}),
        TEXT_7_4(7,    3, true,  new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL}),
        TEXT_7_5(7,    2, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL} ),
        TEXT_7_6(7,    1, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL}),
        TEXT_7_7(7,    0, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL}),
        TEXT_8_1(8,    7, false, new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_8_2(8,    6, true,  new SentencePosition[]{SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_8_3(8,    5, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL}),
        TEXT_8_4(8,    4, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL}),
        TEXT_8_5(8,    3, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR}),
        TEXT_8_6(8,    2, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL}),
        TEXT_8_7(8,    1, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FLOOR, SentencePosition.FLOOR, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL}),
        TEXT_8_8(8,    0, true,  new SentencePosition[]{SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL, SentencePosition.FURNITURE_WALL});

        private int                 objectsCount;
        private int                 prepositionsCount;
        private boolean             haveFurniturePosition;
        private SentencePosition[]  sentencePosition;

        ProtocolText(int objectsCount, int prepositionsCount, boolean haveFurniturePosition, SentencePosition[] objectsPosition) {
            this.objectsCount = objectsCount;
            this.prepositionsCount = prepositionsCount;
            this.haveFurniturePosition = haveFurniturePosition;
            this.sentencePosition = objectsPosition;
        }


        public int getPrepositionsCount() {
            return prepositionsCount;
        }


        public SentencePosition[] getSentencePosition() {
            return sentencePosition;
        }

        public static ProtocolText getSentence(int objectsCount, int prepositionsMinimum , TablexiaRandom random, GameDifficulty difficulty) {
            List<ProtocolText> protocolTexts = new ArrayList<>();
            for (ProtocolText protocolText : values()) {
                if (protocolText.objectsCount == objectsCount && protocolText.prepositionsCount >= prepositionsMinimum &&
                        ((difficulty == GameDifficulty.EASY && !protocolText.haveFurniturePosition) || (difficulty != GameDifficulty.EASY))) {
                    protocolTexts.add(protocolText);
                }
            }

            Collections.shuffle(protocolTexts, random);

            return protocolTexts.get(0);
        }

    }

    public enum SentencePosition {
        FLOOR,
        FURNITURE_WALL
    }

    protected enum TextType{
        Object,
        Preposition,
        Pronoun,
        Verb,
        Furniture_Wall
    }

    private enum CZSKTextDescriptor implements TextDescriptor{
        /**
         * Příklady:
         *      TEXT_2_1: Sestav místo činu, kde jsou razítka mezi barevným míčem a modrým autem.
         *      TEXT_3_1: Sestav místo činu, kde jsou razítka před barevným míčem, který je nalevo od modrého auta.
         *      TEXT_4_1: Sestav místo činu, kde je krajina nalevo od porcelánové vázy. Čajový set leží nalevo od porcelánového talíře, který je za krajinou.
         *      TEXT_4_2: Sestav místo činu, kde leží dalekohled před krajinou. Hodinky jsou na velké levé polici. Dále na místě činu leží čajový set před dalekohledem.
         *      TEXT_4_3: Na místě činu leží fotoaparát na levé komodě. Krajina je za globusem a židle leží na velké pravé polici.
         *      TEXT_4_4: Fotoaparát je na místě činu na malé pravé polici. Na levé komodě leží zrcadlo. Dále je na místě činu dáma s pandou na velké levé polici a skleněná váza leží na pravé komodě.
         *      TEXT_5_1: Na místě činu jsou razítka za káčou, která je před domkem pro panenky. Domek pro panenky je před prakem, který je napravo od kostek.
         *      TEXT_5_2: Sestav místo činu, kde je hromada krabic za knihou ve stojanu. Na velké pravé polici leží malá krabice. Otevřená kniha je napravo od velké kulaté baňky, která leží nalevo od hromady krabic.
         *      TEXT_5_3: Na místě činu leží šedá přísada na velkém stole. Pero je za zhasnutým kahanem, který leží před hořícím kahanem. Dále na místě činu je hořící zkumavka na malém stolku.
         *      TEXT_5_4: Sestav místo činu, kde leží dýmící zkumavka na velké pravé polici a zápisník je na malé levé polici. Knihy leží za perem. Na pravé skříňce jsou zkumavky ve stojanu.
         *      TEXT_5_5: Sestav místo činu, kde je otevřená kniha na malé pravé polici. Zhasnutý kahan leží na malém stolku a hořící kahan je na malé pravé polici. Dále na místě činu leží na malé pravé polici podlouhlá baňka a pero je na malém stolku.
         *      TEXT_6_1: Dřevěný koník je na místě činu nalevo od hadrové panenky, která je nalevo od pejska. Tahací kačer je u bubínku. Dále na místě činu je káča u pejska.
         *      TEXT_6_2: Na místě činu je zhasnutý kahan nalevo od dýmící zkumavky, která leží za oranžovou tekutinou. Na malé pravé polici je kniha ve stojanu. Zkumavky ve stojanu leží nalevo od hořící zkumavky, která je napravo od oranžové tekutiny.
         *      TEXT_6_3: Sestav místo činu, kde je zhasnutý kahan na malém stolku a kniha ve stojanu leží na pravé skříňce. Hořící zkumavka je nalevo od černé přísady. Podlouhlá baňka leží nalevo od oranžové tekutiny, která je za černou přísadou.
         *      TEXT_6_4: Šedá přísada je na místě činu na velkém stole. Otevřená kniha leží nalevo od knihy ve stojanu, která je napravo od zkumavek ve stojanu. Dále na místě činu leží na levé skříňce zápisník a malá krabice je na velkém stole.
         *      TEXT_6_5: Sestav místo činu, kde jsou zkumavky ve stojanu na malé pravé polici a hořící kahan leží napravo od hromady krabic. Na velké levé polici je zhasnutý kahan. Žlutá tekutina leží na pravé skříňce a dýmící zkumavka je na velkém stole.
         *      TEXT_6_6: Otevřená kniha je na místě činu na velkém stole a kniha ve stojanu leží na velké pravé polici. Na velkém stole je černá tekutina a na velké pravé polici leží černá přísada. Dále na místě činu je papírová krabice na velké pravé polici a knihy leží na velké levé polici.
         *      TEXT_7_1: Sestav místo činu, kde je domek pro panenky nalevo od malé panenky a papírový drak je napravo od malé panenky, která je u modrého auta. Dále na místě činu je anděl před pytlíčkem kuliček, který je u kohoutka.
         *      TEXT_7_2: Sestav místo činu, kde je oranžová tekutina za velkou krabicí, která leží za zkumavkami ve stojanu. Na velké levé polici je otevřená kniha. Hromada krabic leží před papírovou krabicí a zkumavka je za papírovou krabicí.
         *      TEXT_7_3: Na místě činu leží modrá tekutina na velké pravé polici a kniha ve stojanu je napravo od hořícího kahanu, který leží za hořící zkumavkou. Zelená tekutina je před velkou krabicí, která leží napravo od hořící zkumavky. Na velkém stole je bílá přísada.
         *      TEXT_7_4: Zkumavky ve stojanu leží na místě činu napravo od papírové krabice, která je nalevo od modré tekutiny. Hořící kahan leží na malém stolku a šedá přísada je na velkém stole. Velká krabice leží za papírovou krabicí a na malé levé polici je malá krabice.
         *      TEXT_7_5: Sestav místo činu, kde je velká kulatá baňka na malém stolku. Kulatá baňka leží na malém stolku a zkumavka je na velkém stole. Dále na místě činu leží malá krabice napravo od oranžové tekutiny, která je nalevo od černé přísady. Na malé levé polici leží otevřená kniha.
         *      TEXT_7_6: Na místě činu leží papírová krabice na levé skříňce a pero je na levé skříňce. Oranžová tekutina leží nalevo od otevřené knihy. Na malé levé polici je modrá tekutina. Šedá přísada leží na malém stolku a kniha ve stojanu je na malém stolku.
         *      TEXT_7_7: Pero je na místě činu na malé levé polici a šedá přísada leží na levé skříňce. Na malé levé polici je bílá přísada. Modrá tekutina leží na malé pravé polici a zápisník je na velké pravé polici. Dále na místě činu leží na malém stolku velká kulatá baňka a zhasnutý kahan je na velkém stole.
         *
         */
        TEXT_2_1(1, Collections.singletonList(
                new SentenceDescriptor(Sentence.S2_FL_S_1, new Integer[]{0, 0, 0, 1})
        )),
        TEXT_3_1(1, Collections.singletonList(
                new SentenceDescriptor(Sentence.S3_FL_C_1, new Integer[]{0, 0, 0, 1, 1, 1, 1, 2})
        )),
        TEXT_4_1(2, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FL_S_1, new Integer[]{0, 0, 0, 1}),
                new SentenceDescriptor(Sentence.A3_FL_C_1,new Integer[]{2, 2, 2, 3, 3, 3, 3, 0})
        )),
        TEXT_4_2(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FL_S_1, new Integer[]{0, 0, 0, 1}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{2, 2, 2}),
                new SentenceDescriptor(Sentence.A2_FL_S_1, new Integer[]{3, 3, 3, 0})
        )),
        TEXT_4_3(2, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_1, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A3_FF_C_1, new Integer[]{1, 1, 1, 2, 3, 3, 3})
        )),
        TEXT_4_4(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_2, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{1, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{2, 2, 2, 3, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{2, 2, 3, 2})
        )),
        TEXT_5_1(2, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_2, new Integer[]{0, 0, 0, 1, 1, 1, 1, 2}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{2, 2, 2, 3, 3, 3, 3, 4})
        )),
        TEXT_5_2(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FL_S_1, new Integer[]{0, 0, 0, 1}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{2, 2, 2}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{3, 3, 3, 4, 4, 4, 4, 0})
        )),
        TEXT_5_3(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_1, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{1, 1, 1, 2, 2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A1_FW_S_2, new Integer[]{4, 4, 4})
        )),
        TEXT_5_4(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_1, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{4, 4, 4}),
                new SentenceDescriptor(Sentence.S2_FW_C_1_A, new Integer[]{0, 0, 1, 0})
        )),
        TEXT_5_5(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_3, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{1, 1, 1, 2, 2, 2}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{3, 3, 3, 4, 4}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{3, 3, 4, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{1, 2, 1, 1})
        )),
        TEXT_6_1(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_3, new Integer[]{0, 0, 0, 1, 1, 1, 1, 2}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{3, 3, 3, 4}),
                new SentenceDescriptor(Sentence.A2_FL_S_1, new Integer[]{5, 5, 5, 2})
        )),
        TEXT_6_2(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_2, new Integer[]{0, 0, 0, 1, 1, 1, 1, 2}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{3, 3, 3}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{4, 4, 4, 5, 5, 5, 5, 2})
        )),
        TEXT_6_3(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_1, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{4, 4, 4, 5, 5, 5, 5, 3}),
                new SentenceDescriptor(Sentence.S2_FW_C_1_A, new Integer[]{0, 0, 1, 0})
        ) ),
        TEXT_6_4(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_2, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{1, 1, 1, 2, 2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{4, 4, 4, 5, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{4, 4, 5, 4})
        )),
        TEXT_6_5(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FF_C_1, new Integer[]{0, 0, 0, 1, 1, 1, 2}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{3, 3, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{4, 4, 4, 5, 5, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{4, 5, 4, 4})
        )),
        TEXT_6_6(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_3, new Integer[]{0, 0, 0, 1, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{2, 2, 2, 3, 3, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{4, 4, 4, 5, 5}),
                new SentenceDescriptor(Sentence.S2_FW_C_3_A, new Integer[]{0, 1, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{4, 4, 5, 4}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{2, 3, 2, 2})
        )),
        TEXT_7_1(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FL_S_1, new Integer[]{0, 0, 0, 1}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{2, 2, 2, 1, 1, 1, 1, 3}),
                new SentenceDescriptor(Sentence.A3_FL_C_2, new Integer[]{4, 4, 4, 5, 5, 5, 5, 6})
        )),
        TEXT_7_2(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_1, new Integer[]{0, 0, 0, 1, 1, 1, 1, 2}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{3, 3, 3}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{4, 4, 4, 5, 5, 5, 5, 6})
        )),
        TEXT_7_3(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S4_FF_C_1, new Integer[]{0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{4, 4, 4, 5, 5, 5, 5, 3}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{6, 6, 6})
        )),
        TEXT_7_4(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_3, new Integer[]{0, 0, 0, 1, 1, 1, 1, 2}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{3, 3, 3, 4, 4, 4}),
                new SentenceDescriptor(Sentence.A3_FF_C_1, new Integer[]{5, 5, 5, 1, 6, 6, 6}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{3, 4, 3, 3})
        )),
        TEXT_7_5(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_3, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{1, 1, 1, 2, 2, 2}),
                new SentenceDescriptor(Sentence.A3_FL_C_2, new Integer[]{3, 3, 3, 4, 4, 4, 4, 5}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{6, 6, 6}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{1, 2, 1, 1})
        )),
        TEXT_7_6(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_2, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{4, 4, 4}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{5, 5, 5, 6, 6, 6}),
                new SentenceDescriptor(Sentence.S2_FW_C_2_A, new Integer[]{0, 0, 1, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{5, 6, 5, 5})
        )),
        TEXT_7_7(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_3, new Integer[]{0, 0, 0, 1, 1, 1}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{2, 2, 2}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{3, 3, 3, 4, 4, 4}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{5, 5, 5, 6, 6}),
                new SentenceDescriptor(Sentence.S2_FW_C_3_A, new Integer[]{0, 1, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{5, 5, 6, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{3, 4, 3, 3})
        )),
        TEXT_8_1(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_1, new Integer[]{0, 0, 0, 1, 1, 1, 1, 2}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{3, 3, 3, 4}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{5, 5, 5, 2, 2, 2, 2, 6}),
                new SentenceDescriptor(Sentence.A3_FL_C_2, new Integer[]{7, 7, 7, 6, 6, 6, 6, 3})
        )),
        TEXT_8_2(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_2, new Integer[]{0, 0, 0, 1, 1, 1, 1, 2}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{3, 3, 3, 4, 4, 4, 4, 7}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{5, 5, 5}),
                new SentenceDescriptor(Sentence.A3_FL_C_2, new Integer[]{6, 6, 6, 2, 2, 2, 2, 7})
        ) ),
        TEXT_8_3(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S4_FF_C_1, new Integer[]{0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{4, 4, 4, 5}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{6, 6, 6, 5, 5, 5, 5, 1}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{7, 7, 7})
        )),
        TEXT_8_4(5, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_1, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{4, 4, 4, 5, 5, 5, 5, 6}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{6, 6, 6, 3}),
                new SentenceDescriptor(Sentence.A1_FW_S_2, new Integer[]{7, 7, 7}),
                new SentenceDescriptor(Sentence.S2_FW_C_1_A, new Integer[]{0, 0, 1, 0})
        )),
        TEXT_8_5(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_3, new Integer[]{0, 0, 0, 1, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{4, 4, 4, 5, 5, 5}),
                new SentenceDescriptor(Sentence.A3_FL_C_2, new Integer[]{6, 6, 6, 3, 3, 3, 3, 7}),
                new SentenceDescriptor(Sentence.S2_FW_C_3_A, new Integer[]{0, 1, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{4, 5, 4, 4})
        )),
        TEXT_8_6(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_2, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{2, 2, 2, 3, 3, 3, 3, 4}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{5, 5, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{6, 6, 6, 7, 7}),
                new SentenceDescriptor(Sentence.S2_FW_C_2_A, new Integer[]{0, 0, 1, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{6, 6, 7, 6})
        )),
        TEXT_8_7(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FF_C_1, new Integer[]{0, 0, 0, 1, 1, 1, 2}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{3, 3, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{4, 4, 4, 5, 5, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{6, 6, 6, 7, 7}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{4, 5, 4, 4}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{6, 6, 7, 6})
        )),
        TEXT_8_8(5, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_1, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{2, 2, 2}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{3, 3, 3, 4, 4, 4}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{5, 5, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{6, 6, 6, 7, 7}),
                new SentenceDescriptor(Sentence.S2_FW_C_1_A, new Integer[]{0, 0, 1, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{3, 4, 3, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{6, 6, 7, 6})
        ));


        private List<SentenceDescriptor> sentenceDescriptors;
        private int sentenceCnt;
        CZSKTextDescriptor(int sentenceCnt , List<SentenceDescriptor> sentenceDescriptors){
            this.sentenceCnt = sentenceCnt;
            this.sentenceDescriptors = sentenceDescriptors;
        }


        @Override
        public List<SentenceDescriptor> getSentenceDescriptors() {
            return sentenceDescriptors;
        }

        @Override
        public int getSentenceCnt(){
            return sentenceCnt;
        }

        @Override
        public Integer[] getIndexDescriptorBySentect(Sentence sentence) {

            for(SentenceDescriptor sentenceDescriptor: sentenceDescriptors){
                if(sentenceDescriptor.getSentence() == sentence)
                    return sentenceDescriptor.getDescriptorIndex();
            }

            throw new RuntimeException("Cannot get index descriptor array for sentence: " + sentence);
        }

    }

    protected static class SentenceDescriptor{

        private Sentence sentence;
        private Integer[] descriptorIndex;

        public SentenceDescriptor(Sentence sentence, Integer[] descriptorIndex){
            this.sentence = sentence;
            this.descriptorIndex = descriptorIndex;
        }

        public Sentence getSentence() {
            return sentence;
        }

        public Integer[] getDescriptorIndex() {
            return descriptorIndex;
        }
    }

    protected enum Sentence{
        /**
         *      S = Start / A = Another
         *     /
         *    /  Count of objects
         *   / /      index
         *  / /      /
         *  S1_FW_S_1 (_A alternative)
         *      \ \
         *      \ \
         *      \  S = simple sentence / C = Compound sentence
         *      \
         *       Objects position: FL(Floor), FW(Furniture or Wall), FF (Floor and Furniture or Wall)
         */

        //Start sentences
        S1_FW_S_1   ("game_protocol_sentence_s1_fw_s_1",    1, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall}),
        S1_FW_S_2   ("game_protocol_sentence_s1_fw_s_2",    1, false, new TextType[]{TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        S1_FW_S_3   ("game_protocol_sentence_s1_fw_s_3",    1, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall}),
        S2_FL_S_1   ("game_protocol_sentence_s2_fl_s_1",    0, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Preposition, TextType.Object}),
        S2_FW_C_1   ("game_protocol_sentence_s2_fw_c_1",    2, true,  new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall, TextType.Object, TextType.Furniture_Wall}),
        S2_FW_C_1_A ("game_protocol_sentence_s2_fw_c_1_a",  1, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Object, TextType.Furniture_Wall}),
        S2_FW_C_2   ("game_protocol_sentence_s2_fw_c_2",    2, true,  new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall, TextType.Object, TextType.Furniture_Wall}),
        S2_FW_C_2_A ("game_protocol_sentence_s2_fw_c_2_a",  1, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Object, TextType.Furniture_Wall}),
        S2_FW_C_3   ("game_protocol_sentence_s2_fw_c_3",    2, true,  new TextType[]{TextType.Object, TextType.Verb, TextType.Furniture_Wall, TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        S2_FW_C_3_A ("game_protocol_sentence_s2_fw_c_3_a",  1, false, new TextType[]{TextType.Object, TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        S3_FL_C_1   ("game_protocol_sentence_s3_fl_c_1",    0, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Verb, TextType.Preposition, TextType.Object}),
        S3_FL_C_2   ("game_protocol_sentence_s3_fl_c_2",    0, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Verb, TextType.Preposition, TextType.Object}),
        S3_FF_C_1   ("game_protocol_sentence_s3_ff_c_1",    1, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall, TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object}),
        S3_FL_C_3   ("game_protocol_sentence_s3_fl_c_3",    0, false, new TextType[]{TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Verb, TextType.Preposition, TextType.Object}),
        S4_FF_C_1   ("game_protocol_sentence_s4_ff_c_1",    1, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall, TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Verb, TextType.Preposition, TextType.Object}),
        //Another sentences
        A1_FW_S_1   ("game_protocol_sentence_a1_fw_s_1",    1, false, new TextType[]{TextType.Furniture_Wall, TextType.Verb, TextType.Object}),
        A1_FW_S_2   ("game_protocol_sentence_a1_fw_s_2",    1, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall}),
        A2_FL_S_1   ("game_protocol_sentence_a2_fl_s_1",    0, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Preposition, TextType.Object}),
        A2_FL_S_2   ("game_protocol_sentence_a2_fl_s_2",    0, false, new TextType[]{TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object}),
        A2_FW_C_1   ("game_protocol_sentence_a2_fw_c_1",    2, true,  new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall, TextType.Object, TextType.Furniture_Wall}),
        A2_FW_C_1_A ("game_protocol_sentence_a2_fw_c_1_a",  1, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Object, TextType.Furniture_Wall}),
        A2_FW_C_2   ("game_protocol_sentence_a2_fw_c_2",    2, true,  new TextType[]{TextType.Object, TextType.Verb, TextType.Furniture_Wall, TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        A2_FW_C_2_A ("game_protocol_sentence_a2_fw_c_2_a",  1, false, new TextType[]{TextType.Object, TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        A3_FL_C_1   ("game_protocol_sentence_a3_fl_c_1",    0, false, new TextType[]{TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Verb, TextType.Preposition, TextType.Object}),
        A3_FF_C_1   ("game_protocol_sentence_a3_ff_c_1",    1, false, new TextType[]{TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object, TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        A3_FL_C_2   ("game_protocol_sentence_a3_fl_c_2",    0, false, new TextType[]{TextType.Verb, TextType.Object, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Verb, TextType.Preposition, TextType.Object});

        private String      resourcePath;
        private TextType[]  textTypes;

        private boolean     canBeMerge;
        private int         furnitureCnt;

        Sentence(String resourcePath, int furnitureCnt, boolean canBeMerge, TextType[] textTypes){
            this.resourcePath = resourcePath;
            this.furnitureCnt = furnitureCnt;
            this.canBeMerge = canBeMerge;
            this.textTypes = textTypes;
        }

        public static Sentence getAlternativeSentence(Sentence sentence){
            return Sentence.values()[sentence.ordinal() + 1];
        }
    }


    private   ProtocolText protocolText;
    private   List<GameObjectType>  objectTypeList = new ArrayList<>();
    protected TablexiaRandom        random;
    private   ObjectsController     controller;
    private   PositionCounter       positionCounter;
    protected ProtocolGameState     gameState;
    protected GameDifficulty        difficulty;
    private   int                   startIndexVerb;

    private HashMap<GameObjectType,List<String>> objectsText = new HashMap<>();

    public ProtocolGenerator(TablexiaRandom random, GameDifficulty difficulty, ProtocolGameState gameState) {

        this.random = random;
        this.difficulty = difficulty;
        this.gameState = gameState;

        controller = new ObjectsController();
        positionCounter = new PositionCounter();

    }

/////////////////////////////// PROTOCOL GENERATE


    public void setSentenceByObjectsCnt(int objectCount, int prepositionsMinimum, TablexiaRandom random) {
        protocolText = ProtocolText.getSentence(objectCount, prepositionsMinimum, random, difficulty);
    }

    public void clearData() {
        controller.clearSolution();
        objectsText.clear();
        positionCounter.clearData();
    }

    private GameObjectType getRandomNextGameObjectType() {
        List<GameObjectType> another = gameState.getAnotherObjectTypes();
        Collections.shuffle(another, random);

        for (GameObjectType objectType : another) {
            if (objectType.checkPosition(RoomPosition.FLOOR)) {
                gameState.removeObjectFromAnotherList(objectType);
                return objectType;
            }
        }

        Log.err(getClass(), "Cannot find another object, which can put on the floor!");
        return null;
    }


    private void addNewObjectsText(GameObjectType type, String textObject) {
        if (objectsText.containsKey(type)) {
            objectsText.get(type).add(textObject);
            return;
        }

        objectsText.put(type, new ArrayList<>(Collections.singletonList(textObject)));
    }

/////////////////////////////// PROTOCOL TEXT

    public String getProtocolMessage(AbstractTablexiaScreen screen) {
        objectTypeList = gameState.getProtocolObjectTypes();
        positionCounter.initRoots(objectTypeList);

        TextDescriptor textDescriptor = getTextDescriptor(protocolText);
        List<SentenceDescriptor> sentenceDescriptors = textDescriptor.getSentenceDescriptors();
        List<FurnitureType> furnitureTypes = FurnitureType.getFurnitureByDifficulty(difficulty);
        List<WallType> wallTypes = new LinkedList<>(Arrays.asList(WallType.values()));

        int sentenceCnt = 0;
        startIndexVerb = random.nextInt(2);

        String message = "[BLACK]";
        Iterator<SentenceDescriptor> it = sentenceDescriptors.iterator();
        while (it.hasNext() && sentenceCnt != textDescriptor.getSentenceCnt()) {
            SentenceDescriptor sentenceDescriptor = it.next();
            Sentence sentence = sentenceDescriptor.getSentence();
            Integer[] descriptorIndex = sentenceDescriptor.getDescriptorIndex();
            TextType[] textTypes = getTextTypesBySentence(sentence);
            List<TypeObjectDescriptor> typePositions = null;
            boolean needAlternative = false;

            if (sentence.furnitureCnt != 0) {
                typePositions = new ArrayList<>();
                needAlternative = generateFurnitureForSentence(typePositions, furnitureTypes, wallTypes, textTypes, descriptorIndex, sentence.canBeMerge);

                if (needAlternative) {
                    sentence = Sentence.getAlternativeSentence(sentence);
                    textTypes = getTextTypesBySentence(sentence);
                    descriptorIndex = textDescriptor.getIndexDescriptorBySentect(sentence);
                }

            }

            message += screen.getFormattedText(sentence.resourcePath, (Object[]) prepareProtocolObjectsText(screen, textTypes, typePositions, descriptorIndex, needAlternative));

            if (sentenceCnt != textDescriptor.getSentenceCnt() - 1)
                message += " ";

            sentenceCnt++;
        }
        message += "[]";

        int index = -1;

        //Check <BIG>
        while ((index = message.indexOf(NEXT_BIG_CHAR_PATTERN, index + 1)) != -1) {
            if (index + NEXT_BIG_CHAR_PATTERN.length() < message.length()) {
                String charToReplace = String.valueOf(message.charAt(index + NEXT_BIG_CHAR_PATTERN.length()));
                message = message.replaceFirst(NEXT_BIG_CHAR_PATTERN + charToReplace, charToReplace.toUpperCase());
            }
        }

        return message;
    }

    private boolean generateFurnitureForSentence(List<TypeObjectDescriptor> typePositions, List<FurnitureType> furnitureTypes, List<WallType> wallTypes, TextType[] textTypes, Integer[] descriptorIndex, boolean canBeMerge) {
        for (int i = 0; i < textTypes.length; i++) {

            if (textTypes[i] == TextType.Furniture_Wall) {
                RoomPosition position = RoomPosition.FURNITURE;
                GameObjectType objectType = objectTypeList.get(descriptorIndex[i]);

                if (objectType.checkPosition(RoomPosition.WALL) && !objectType.checkPosition(RoomPosition.FURNITURE)) {
                    //object can put only on wall
                    position = RoomPosition.WALL;
                } else if (objectType.checkPosition(RoomPosition.WALL) && objectType.checkPosition(RoomPosition.FURNITURE)) {
                    //choose wall or furniture position
                    position = objectType.chooseFurnitureOrWallPosition(random);
                }

                if (position == RoomPosition.FURNITURE) {

                    //Check if furniture is not empty.
                    if (furnitureTypes == null || furnitureTypes.isEmpty())
                        furnitureTypes = FurnitureType.getFurnitureByDifficulty(difficulty);

                    int rnd = random.nextInt(furnitureTypes.size());
                    FurnitureType furnitureType = furnitureTypes.get(rnd);
                    controller.addPositionToSolution(objectType, Preposition.ON, furnitureType);
                    typePositions.add(furnitureType);
                } else {
                    if(wallTypes == null || wallTypes.isEmpty())
                        wallTypes = Arrays.asList(WallType.values());

                    int rnd = random.nextInt(wallTypes.size());
                    WallType wallType = wallTypes.get(rnd);
                    controller.addPositionToSolution(objectType, Preposition.ON, wallType);
                    typePositions.add(wallType);
                }
            }
        }

        //Remove used furniture type and wall type
        for (TypeObjectDescriptor position : typePositions) {
            if (position instanceof FurnitureType && furnitureTypes.contains(position)) {
                furnitureTypes.remove(position);
            }else if (position instanceof WallType && wallTypes.contains(position)){
                wallTypes.remove(position);
            }
        }


        //Check position types, who can merged
        if (canBeMerge && typePositions.size() == MAX_FURNITURE) {

            if (typePositions.get(0) == typePositions.get(1)) {
                typePositions.remove(1);
                return true;
            }

        }

        return false;
    }


    private String[] prepareProtocolObjectsText(AbstractTablexiaScreen screen, TextType[] textTypes, List<TypeObjectDescriptor> typePositions, Integer[] descriptorIndex, boolean alternative) {
        String[] texts = new String[textTypes.length];

        Preposition lastPreposition = Preposition.NONE;

        int furnitureIndex = 0;

        for (int i = 0; i < textTypes.length; i++) {
            TextType textType = textTypes[i];
            GameObjectType objectType = objectTypeList.get(descriptorIndex[i]);
            AbstractObjectDescriptor descriptor = getObjectDescriptorByObjectType(objectType);
            switch (textType) {
                case Object:
                    texts[i] = prepareObject(screen, objectType, lastPreposition, descriptor);
                    lastPreposition = Preposition.NONE;
                    break;
                case Preposition:
                    lastPreposition = gameState.getNextPreposition();
                    texts[i] = preparePreposition(screen, objectType, lastPreposition, descriptorIndex, i);
                    break;
                case Pronoun:
                    texts[i] = preparePronoun(screen, descriptor);
                    break;
                case Verb:
                    boolean wall = controller.isObjectOnWall(objectType);
                    texts[i] = prepareVerb(screen, descriptor.getNounType(), startIndexVerb, alternative, wall);
                    startIndexVerb = (startIndexVerb + 1) % 2;
                    break;
                case Furniture_Wall:
                    if (typePositions.get(furnitureIndex) instanceof FurnitureType) {
                        FurnitureType furnitureType = (FurnitureType) typePositions.get(furnitureIndex);
                        AbstractObjectDescriptor furnitureDescriptor = getFurnitureDescriptorByType(furnitureType);
                        texts[i] = prepareFurniture(screen, furnitureType, furnitureDescriptor, objectType);
                    } else {
                        WallType wallType = (WallType) typePositions.get(furnitureIndex);
                        AbstractObjectDescriptor wallDescriptor = getWallDescriptorByType(wallType);
                        texts[i] = prepareWall(screen, wallType, wallDescriptor, objectType);
                    }

                    furnitureIndex++;
                    break;

            }
        }

        return texts;
    }

    private String prepareObject(AbstractTablexiaScreen screen, GameObjectType objectType, Preposition preposition, AbstractObjectDescriptor descriptor) {
        String objectText = getObjectTextWithAdjective(screen, objectType, preposition, descriptor);
        addNewObjectsText(objectType, objectText);
        controller.addGameObjectTypeToSolution(objectType);
        return objectText;
    }

    private String preparePreposition(AbstractTablexiaScreen screen, GameObjectType objectType, Preposition preposition, Integer[] descriptorIndex, int index) {

        String prepositionText = preposition.getPrepositionText(screen);
        controller.addPositionToSolution(objectType, preposition, objectTypeList.get(descriptorIndex[index + 1]));
        GameObjectType newObject = null;

        if (preposition == Preposition.BETWEEN) {
            newObject = getRandomNextGameObjectType();

            gameState.addObject(newObject);
            positionCounter.addRoot(newObject);

            AbstractObjectDescriptor descriptor = getObjectDescriptorByObjectType(newObject);
            String objectText = getObjectTextWithAdjective(screen, newObject, preposition, descriptor);

            prepositionText += " " + objectText + " " + screen.getText(ProtocolAssets.TEXT_AND);
            addNewObjectsText(newObject, objectText);
            controller.addPositionToSolution(objectType, preposition, newObject);
        }
        positionCounter.computeShift(preposition, objectType, objectTypeList.get(descriptorIndex[index + 1]), newObject, gameState.getHorizontalPreposition(), gameState.getVerticalPreposition());
        return prepositionText;
    }

    private String preparePronoun(AbstractTablexiaScreen screen, AbstractObjectDescriptor descriptor) {
        return descriptor.getPronounInflection(screen);
    }

    private String prepareVerb(AbstractTablexiaScreen screen, AbstractObjectDescriptor.NounType nounType, int startIndexVerb, boolean alternative, boolean wall) {
        Verb verb = Verb.getVerb(startIndexVerb, wall);
        return alternative ? verb.getPluralVerbText(screen) : verb.getVerbText(screen, nounType);
    }

    private String prepareFurniture(AbstractTablexiaScreen screen, FurnitureType furnitureType, AbstractObjectDescriptor descriptor, GameObjectType objectType) {
        String furnitureText = getFurnitureTextWithAdjective(screen, furnitureType, Preposition.ON, descriptor);
        String preposition = Preposition.ON.getPrepositionText(screen);
        return preposition + " " + furnitureText;
    }

    private String prepareWall(AbstractTablexiaScreen screen, WallType wallType, AbstractObjectDescriptor descriptor, GameObjectType objectType) {
        String wallText = getWallTextWithAdjective(screen, descriptor, Preposition.ON);
        String preposition = getWallPreposition(screen);
        return preposition + " " + wallText;
    }

    protected String getWallPreposition(AbstractTablexiaScreen screen) {
        return Preposition.ON.getPrepositionText(screen);
    }


    protected TextType[] getTextTypesBySentence(Sentence sentence) {
        return sentence.textTypes;
    }

    protected TextDescriptor getTextDescriptor(ProtocolText protocolText) {
        return CZSKTextDescriptor.values()[protocolText.ordinal()];
    }

    /**
     * Check last character in string s is "ň" or "ť" for change first character in inflection Exp" Kůň - U koně, Před koněm
     *
     * @param s
     * @return
     */
    protected boolean changeLastCharacterInflection(String s) {
        return s.matches(".*[ň |ť]$");
    }

/////////////////////////////// GETTERS


    public ProtocolText getProtocolText() {
        return protocolText;
    }

    public PositionCounter getPositionCounter() {
        return positionCounter;
    }

    public ObjectsController getController() {
        return controller;
    }

    public HashMap<GameObjectType, List<String>> getObjectsText() {
        return objectsText;
    }
}
