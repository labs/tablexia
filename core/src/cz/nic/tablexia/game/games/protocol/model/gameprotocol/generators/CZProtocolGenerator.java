/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.model.gameprotocol.generators;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.AbstractObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.AbstractObjectDescriptor.CaseType;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.CZObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.FurnitureType;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;
import cz.nic.tablexia.game.games.protocol.model.ProtocolGameState;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.ProtocolGenerator;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by lmarik on 12.5.17.
 */
public class CZProtocolGenerator extends ProtocolGenerator {

/////////////////////////////// DESCRIPTORS

    private enum ObjectDescriptor {
        // EASY
        ANGEL               (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.PAN,  null)),
        BABY_CARRIAGE       (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, null)),
        BAG_OF_MARBLES      (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, ProtocolAssets.MARBLES)),
        BEAR                (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.PAN,  null)),
        BIG_DOLL            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                           CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        BLUE_CAR            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLUE},                                          CZObjectDescriptor.AdjectiveInflection.NM, CZObjectDescriptor.InflectionType.MESTO,null)),
        BOWLING             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        COLOR_BALL          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.COLOR},                                         CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.MUZ,  null)),
        CUBES_TOY           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        DEVIL               (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.PAN,  null)),
        DOG                 (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.PAN,  null)),
        DOLLHOUSE           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, ProtocolAssets.FOR_DOLLS)),
        DRUM                (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, null)),
        FOOTBALL            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.FOOTBALL},                                      CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.MUZ,  null)),
        HEN                 (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        INK_STAMPS          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.MESTO,null)),
        NICHOLAS            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.MUZ,  null)),
        SOLDIER             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.PAN,  null)),
        PAPER_DRAKE         (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PAPER},                                         CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.PAN,  null)),
        PULL_DUCK           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PULL},                                          CZObjectDescriptor.AdjectiveInflection.MJ, CZObjectDescriptor.InflectionType.PAN,  null)),
        RAG_DOLL            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RAG},                                           CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        RED_CAR             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RED},                                           CZObjectDescriptor.AdjectiveInflection.NM, CZObjectDescriptor.InflectionType.MESTO,null)),
        ROCK_HORSE          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ROCK},                                          CZObjectDescriptor.AdjectiveInflection.MJ, CZObjectDescriptor.InflectionType.MUZ,  null)),
        ROOSTER             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.PAN,  null)),
        SLINGSHOT           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, null)),
        SMALL_DOLL          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        TRAIN               (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, null)),
        WHIPPING_TOP        (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        WHISTLE             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        WOODEN_HORSE        (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.WOODEN},                                        CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.PAN,  null)),
        // MEDIUM
        BIG_LAMP            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        BOWL                (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        BOX_WITH_JEWELERY   (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.RUZE, ProtocolAssets.JEWELERY)),
        CAMERA              (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, null)),
        CHAIR               (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.MESTO,null)),
        CHESS_TABLE         (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.CHESS},                                         CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.HRAD, null)),
        COLLECTION_OF_COINS (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, ProtocolAssets.COINS)),
        CUCKOO_CLOCK        (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.PLURAL,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.CUCKOO},                                        CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        GLASS_VASE          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GLASS},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        GLOBUS              (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, null)),
        LADY_AND_PANDA      (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, ProtocolAssets.PANDA)),
        LANDSCAPE           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        MIRROR              (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.MESTO,null)),
        PEARLS              (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        PENDULUM_CLOCK      (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.PLURAL,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PENDULUM},                                      CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        PORCELAIN_MUG       (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN},                                     CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.HRAD, null)),
        PORCELAIN_PLATE     (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN},                                     CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.MUZ,  null)),
        PORCELAIN_POTTY     (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN},                                     CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.HRAD, null)),
        PORCELAIN_STATUETTE (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN},                                     CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        PORCELAIN_VASE      (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN},                                     CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        RED_KETTLE          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RED},                                           CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.RUZE, null)),
        SEAT                (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.RUZE, null)),
        SMALL_LAMP          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        TABLE               (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, null)),
        TEA_SET             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.TEA},                                           CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.HRAD, null)),
        TELESCOPE           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, null)),
        TIN_PLATE           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.TIN},                                           CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.MUZ,  null)),
        UMBRELLA            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.KURE, null)),
        WATCH               (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        // HARD
        BIG_BOX             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                           CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.RUZE, null)),
        BIG_ROUND_FLASK     (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.ROUND}, CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        BLACK_INGREDIENT    (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLACK},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA ,null)),
        BLACK_LIQUID        (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLACK},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        BLUE_LIQUID         (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLUE},                                          CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        BOOK_IN_STAND       (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, ProtocolAssets.IN_STAND)),
        BOOKS               (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        DIARY               (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.HRAD, null)),
        GRAY_INGREDIENT     (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GRAY},                                          CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        GREEN_LIQUID        (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GREEN},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        LIGHT_BURNER        (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LIGHT},                                         CZObjectDescriptor.AdjectiveInflection.MJ, CZObjectDescriptor.InflectionType.HRAD, null)),
        LIGHT_OFF_BURNER    (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LIGHT_OFF},                                     CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.HRAD, null)),
        LIGHT_TUBE          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LIGHT},                                         CZObjectDescriptor.AdjectiveInflection.FJ, CZObjectDescriptor.InflectionType.ZENA, null)),
        OBLONG_FLASK        (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.OBLONG},                                        CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        OPEN_BOOK           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.OPEN},                                          CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        ORANGE_LIQUID       (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ORANGE},                                        CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        PAPER_BOX           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PAPER},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.RUZE, null)),
        PEN                 (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.MESTO,null)),
        PILE_OF_BOXES       (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, ProtocolAssets.OF_BOXES)),
        ROUND_FLASK         (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ROUND},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        SMALL_BOX           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        SMOKE_TUBE          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMOKE},                                         CZObjectDescriptor.AdjectiveInflection.FJ, CZObjectDescriptor.InflectionType.ZENA, null)),
        TUBE                (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, null)),
        TUBES_IN_STAND      (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                                                       null,               CZObjectDescriptor.InflectionType.ZENA, ProtocolAssets.IN_STAND)),
        WHITE_INGREDIENT    (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.WHITE},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        YELLOW_LIQUID       (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.YELLOW},                                        CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        //BONUS
        BAG                 (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.ZENA, null)),
        BIG_BOOK            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                           CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        BIG_GLOBE           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                           CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.HRAD, null)),
        BROWN_BOOK          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BROWN},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        ELEPHANT            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.ZENA, ProtocolAssets.ELEPHANT)),
        GIRAFFE             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.ZENA, ProtocolAssets.GIRAFFE)),
        GRAY_BOOK           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GRAY},                                          CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        KABUKI              (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.KABUKI},                                        CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        KIMONO              (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.MESTO,null)),
        LETTERS             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.HRAD, ProtocolAssets.IN_PACKAGES)),
        LION                (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.ZENA, ProtocolAssets.LION)),
        MASK_AFRICA         (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.AFRICA},                                        CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        SHELLS              (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.RUZE, ProtocolAssets.WITH_SHELLS)),
        SMALL_BOOK          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                         CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        SMALL_GLOBE         (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                         CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.HRAD, null)),
        PILE_OF_BOOKS       (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.ZENA, ProtocolAssets.OF_BOOKS)),
        STAND_FOR_POSTER    (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.HRAD, ProtocolAssets.WITH_POSTERS)),
        TABLE_WITH_MAPS     (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.HRAD, ProtocolAssets.WITH_MAPS)),
        CHINESE_VASE        (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.CHINESE},                                       CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.ZENA, null)),
        COMPASS             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                              null,                        CZObjectDescriptor.InflectionType.HRAD, null)),
        TRAVEL_HAT          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.TRAVEL},                                        CZObjectDescriptor.AdjectiveInflection.MM, CZObjectDescriptor.InflectionType.HRAD, null));


        private CZObjectDescriptor descriptor;
        ObjectDescriptor(CZObjectDescriptor descriptor){
            this.descriptor = descriptor;
        }

        public CZObjectDescriptor getDescriptor() {
            return descriptor;
        }
    }

    private enum FurnitureDescriptor {
        //MEDIUM
        MEDIUM_BIG_SHELF_LEFT           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.LEFT},      CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        MEDIUM_BIG_SHELF_RIGHT          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.RIGHT},     CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        MEDIUM_CHEST_OF_DRAWERS_LEFT    (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                              CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.ZENA, null)),
        MEDIUM_CHEST_OF_DRAWERS_RIGHT   (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                             CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.ZENA, null)),
        MEDIUM_SMALL_SHELF_LEFT         (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.LEFT},    CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        MEDIUM_SMALL_SHELF_RIGHT        (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.RIGHT},   CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        //HARD
        HARD_BIG_SHELF_LEFT             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.LEFT},      CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        HARD_BIG_SHELF_RIGHT            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.RIGHT},     CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        HARD_BIG_TABLE                  (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                               CZObjectDescriptor.AdjectiveInflection.MM,  CZObjectDescriptor.InflectionType.HRAD, null)),
        HARD_CHEST_OF_DRAWERS_LEFT      (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                              CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.ZENA, null)),
        HARD_CHEST_OF_DRAWERS_RIGHT     (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                             CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.ZENA, null)),
        HARD_SMALL_SHELF_LEFT           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.LEFT},    CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        HARD_SMALL_SHELF_RIGHT          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.RIGHT},   CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        HARD_SMALL_TABLE                (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                             CZObjectDescriptor.AdjectiveInflection.MM,  CZObjectDescriptor.InflectionType.HRAD, null)),
        //BONUS
        BONUS_BIG_SHELF_LEFT            (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.LEFT},      CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        BONUS_BIG_SHELF_RIGHT           (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.RIGHT},     CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        BONUS_BIG_TABLE                 (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                               CZObjectDescriptor.AdjectiveInflection.MM,  CZObjectDescriptor.InflectionType.HRAD, null)),
        BONUS_CABINET_LEFT              (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                              CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.ZENA, null)),
        BONUS_CHEST_OF_DRAWERS_LEFT     (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                              CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.ZENA, null)),
        BONUS_CHEST_OF_DRAWERS_RIGHT    (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                             CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.ZENA, null)),
        BONUS_CABINET_RIGHT             (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                             CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.ZENA, null)),
        BONUS_ROUND_TABLE               (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ROUND},                                             CZObjectDescriptor.AdjectiveInflection.MM,  CZObjectDescriptor.InflectionType.HRAD, null)),
        BONUS_SMALL_SHELF_LEFT          (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.LEFT},    CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        BONUS_SMALL_SHELF_RIGHT         (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.RIGHT},   CZObjectDescriptor.AdjectiveInflection.FM,  CZObjectDescriptor.InflectionType.RUZE, null)),
        BONUS_SMALL_TABLE               (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                             CZObjectDescriptor.AdjectiveInflection.MM,  CZObjectDescriptor.InflectionType.HRAD, null));

        private CZObjectDescriptor descriptor;
        FurnitureDescriptor(CZObjectDescriptor descriptor){
            this.descriptor = descriptor;
        }

        public CZObjectDescriptor getDescriptor() {
            return descriptor;
        }
    }

    private enum WallDescriptor{
        LEFT_WALL   (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},    CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.KOST, null)),
        MIDDLE_WALL (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.MIDDLE},  CZObjectDescriptor.AdjectiveInflection.FJ, CZObjectDescriptor.InflectionType.KOST, null)),
        RIGHT_WALL  (new CZObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},   CZObjectDescriptor.AdjectiveInflection.FM, CZObjectDescriptor.InflectionType.KOST, null));

        CZObjectDescriptor descriptor;
        WallDescriptor(CZObjectDescriptor descriptor){
            this.descriptor = descriptor;
        }

        public CZObjectDescriptor getDescriptor() {
            return descriptor;
        }

    }

    private final String  END_EK_SUFFIX          = "ek";
    private final String  END_K_SUFFIX           = "k";

    public CZProtocolGenerator(TablexiaRandom random, GameDifficulty difficulty, ProtocolGameState gameState) {
        super(random, difficulty,gameState);
    }

/////////////////////////////// OBJECT INFLECTION TEXT

    @Override
    public String getObjectCardText(AbstractTablexiaScreen screen,GameObjectType objectType) {
        return getObjectTextWithAdjective(screen,objectType,Preposition.NONE,getObjectDescriptorByObjectType(objectType));
    }

    @Override
    public String getObjectTextWithAdjective(AbstractTablexiaScreen screen, GameObjectType objectType,Preposition preposition, AbstractObjectDescriptor descriptor){
        CZObjectDescriptor czObjectDescriptor = (CZObjectDescriptor) descriptor;
        return czObjectDescriptor.getAdjectiveTextByPreposition(screen,preposition) + getObjectTextByPreposition(screen,objectType.getTextPath(),preposition,czObjectDescriptor) + descriptor.getNextDescriptionText(screen);
    }

    @Override
    public String getFurnitureTextWithAdjective(AbstractTablexiaScreen screen, FurnitureType furnitureType,Preposition preposition, AbstractObjectDescriptor descriptor){
        CZObjectDescriptor czObjectDescriptor = (CZObjectDescriptor) descriptor;
        return czObjectDescriptor.getAdjectiveTextByPreposition(screen,preposition) + getObjectTextByPreposition(screen,furnitureType.getTextResource(),preposition,czObjectDescriptor);
    }

    @Override
    public String getWallTextWithAdjective(AbstractTablexiaScreen screen, AbstractObjectDescriptor descriptor, Preposition preposition) {
        CZObjectDescriptor czObjectDescriptor = (CZObjectDescriptor) descriptor;
        return czObjectDescriptor.getAdjectiveTextByPreposition(screen,preposition) + screen.getText(ProtocolAssets.WALL_TEXT);
    }

    private String getObjectTextByPreposition(AbstractTablexiaScreen screen, String textPath, Preposition preposition, CZObjectDescriptor descriptor){
        String objectText = screen.getText(textPath);

        CaseType caseType = descriptor.getCaseTypeByPreposition(preposition);
        if((caseType == CaseType.GENETIV) && (descriptor.getVoice() == AbstractObjectDescriptor.Voice.F || descriptor.getVoice() == AbstractObjectDescriptor.Voice.N) && descriptor.getNounType() == CZObjectDescriptor.NounType.PLURAL){
            return screen.getText(textPath + caseType.getPathResource());
        }

        boolean finishedOnConsonantDiacritic = changeLastCharacterInflection(objectText);


        if(caseType != CaseType.NOMINATIV) {

            //Check -ek ending and remove -e last character for infleciton. = Pejsek - u Pejska
            if (objectText.endsWith(END_EK_SUFFIX)) {
                objectText = objectText.substring(0, objectText.length() - 2) + objectText.substring(objectText.length() - 1);
            }

            //Check -ů(ň/ť) ending. Change "ů" to "o" and remove diacritic = Kůň - před Koněm
            if (objectText.length() >= 2 && objectText.toCharArray()[objectText.length() - 2] == 'ů'){
                char[] chars = objectText.toCharArray();
                chars[objectText.length()-2] = 'o';
                chars[objectText.length()-1] = removeDiacritic(chars[objectText.length() -1]);
                objectText = String.valueOf(chars);
            }
        }

        //Check furniture ending
        if(caseType == CaseType.LOKAL ){

            //Add "ce" ending for female void. Skříňka - Skříňce.
            if(objectText.endsWith(END_K_SUFFIX) && descriptor.getVoice() == AbstractObjectDescriptor.Voice.F){
                objectText = objectText.substring(0, objectText.length() - 1) + "ce";
                return objectText;
            }

            //Check -k ending for Hrad inflection (Stolek - na stolku). Use second inflection ending.
            if(objectText.endsWith(END_K_SUFFIX) && descriptor.getInflectionType() == CZObjectDescriptor.InflectionType.HRAD){
                return objectText + descriptor.getNounInflection(screen,preposition,finishedOnConsonantDiacritic,true);
            }
        }

        return objectText + descriptor.getNounInflection(screen,preposition,finishedOnConsonantDiacritic,false);
    }

    private char removeDiacritic(char c){
        switch (c){
            case 'ň':
                return 'n';
            case 'ť':
                return 't';
            default:
                return c;
        }
    }

/////////////////////////////// DESCRIPTOR

    @Override
    public CZObjectDescriptor getObjectDescriptorByObjectType(GameObjectType type){
        return ObjectDescriptor.values()[type.ordinal()].getDescriptor();
    }

    @Override
    public CZObjectDescriptor getFurnitureDescriptorByType(FurnitureType furnitureType) {
        return FurnitureDescriptor.values()[furnitureType.ordinal()].getDescriptor();
    }

    @Override
    public AbstractObjectDescriptor getWallDescriptorByType(WallType wallType) {
        return WallDescriptor.values()[wallType.ordinal()].getDescriptor();
    }
}
