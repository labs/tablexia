/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;

/**
 * Created by lmarik on 9.5.17.
 */

public class LevelDefinition {

    private static Map<Level,List<GameObjectType>> levels = new HashMap<>();

    public enum DifficultyDefinition{
        EASY    (GameDifficulty.EASY,   new int[]{3,4,5},   new int[]{1,0,1}),
        MEDIUM  (GameDifficulty.MEDIUM, new int[]{4,5,6},   new int[]{1,1,2}),
        HARD    (GameDifficulty.HARD,   new int[]{5,6,7},   new int[]{1,1,2}),
        BONUS   (GameDifficulty.BONUS,  new int[]{6,7,8},   new int[]{0,1,2});


        GameDifficulty difficulty;
        int[] objectsInRounds;
        int[] maxBetweenInRounds;
        DifficultyDefinition(GameDifficulty difficulty, int[] objectsInRounds ,int[] maxBetweenInRounds){
            this.difficulty = difficulty;
            this.objectsInRounds = objectsInRounds;
            this.maxBetweenInRounds = maxBetweenInRounds;
        }

        public static DifficultyDefinition getDefinitionByDifficulty(GameDifficulty difficulty){
            for(DifficultyDefinition definition: values()){
                if(difficulty == definition.difficulty){
                    return definition;
                }
            }

            return null;
        }

        public int getObjectsCountByRound(int round){
            return objectsInRounds[round];
        }

        public int getMaxBetweenPrepositionByRound( int round){
           return maxBetweenInRounds[round];
        }

        public int getSumObjectCount(){
            int sum = 0;

            for (int cnt : objectsInRounds)
                sum += cnt;

            return sum;
        }
    }

    public enum Level{

        TOY_STORE       (GameDifficulty.EASY,    ProtocolAssets.TOY_STORE_BACKGROUND,     new float[]{0.5f,0.3f}),
        JUNK_SHOP       (GameDifficulty.MEDIUM,  ProtocolAssets.JUNK_SHOP_BACKGROUND,     new float[]{0.5f,0.3f}),
        LABORATORY      (GameDifficulty.HARD,    ProtocolAssets.LABORATORY_BACKGROUND,    new float[]{0.5f,0.3f}),
        TRAVELER_ROOM   (GameDifficulty.BONUS,   ProtocolAssets.TRAVELER_ROOM_BACKGROUND, new float[]{0.5f,0.3f});

        private GameDifficulty  difficulty;
        private String          roomTexturePath;
        private float[]         middleRatio;

        Level(GameDifficulty difficulty,String roomTexturePath, float[] middleRatio){
            this.difficulty = difficulty;
            this.roomTexturePath = roomTexturePath;
            this.middleRatio = middleRatio;
        }


        public GameDifficulty getDifficulty() {
            return difficulty;
        }

        public String getRoomTexturePath() {
            return roomTexturePath;
        }

        public float[] getMiddleRatio() {
            return middleRatio;
        }

        public static int getLevelIndexByDifficulty(GameDifficulty difficulty){
            for (Level level:values()){
                if(level.getDifficulty() == difficulty)
                    return level.ordinal();
            }

            return -1;
        }
    }

    static {

        //EASY
        levels.put(Level.TOY_STORE, Arrays.asList(
                GameObjectType.ANGEL,
                GameObjectType.BABY_CARRIAGE,
                GameObjectType.BAG_OF_MARBLES,
                GameObjectType.BEAR,
                GameObjectType.BIG_DOLL,
                GameObjectType.BLUE_CAR,
                GameObjectType.BOWLING,
                GameObjectType.COLOR_BALL,
                GameObjectType.CUBES_TOY,
                GameObjectType.DEVIL,
                GameObjectType.DOG,
                GameObjectType.DOLLHOUSE,
                GameObjectType.DRUM,
                GameObjectType.FOOTBALL,
                GameObjectType.HEN,
                GameObjectType.INK_STAMPS,
                GameObjectType.NICHOLAS,
                GameObjectType.SOLDIER,
                GameObjectType.PAPER_DRAKE,
                GameObjectType.PULL_DUCK,
                GameObjectType.RAG_DOLL,
                GameObjectType.RED_CAR,
                GameObjectType.ROCK_HORSE,
                GameObjectType.ROOSTER,
                GameObjectType.SLINGSHOT,
                GameObjectType.SMALL_DOLL,
                GameObjectType.TRAIN,
                GameObjectType.WHIPPING_TOP,
                GameObjectType.WHISTLE,
                GameObjectType.WOODEN_HORSE
        ));

        //MEDIUM
        levels.put(Level.JUNK_SHOP, Arrays.asList(
                GameObjectType.BIG_LAMP,
                GameObjectType.BOWL,
                GameObjectType.BOX_WITH_JEWELERY,
                GameObjectType.CAMERA,
                GameObjectType.CHAIR,
                GameObjectType.CHESS_TABLE,
                GameObjectType.COLLECTION_OF_COINS,
                GameObjectType.CUCKOO_CLOCK,
                GameObjectType.GLASS_VASE,
                GameObjectType.GLOBUS,
                GameObjectType.LADY_AND_PANDA,
                GameObjectType.LANDSCAPE,
                GameObjectType.MIRROR,
                GameObjectType.PEARLS,
                GameObjectType.PENDULUM_CLOCK,
                GameObjectType.PORCELAIN_MUG,
                GameObjectType.PORCELAIN_PLATE,
                GameObjectType.PORCELAIN_POTTY,
                GameObjectType.PORCELAIN_STATUETTE,
                GameObjectType.PORCELAIN_VASE,
                GameObjectType.RED_KETTLE,
                GameObjectType.SEAT,
                GameObjectType.SMALL_LAMP,
                GameObjectType.TABLE,
                GameObjectType.TEA_SET,
                GameObjectType.TELESCOPE,
                GameObjectType.TIN_PLATE,
                GameObjectType.UMBRELLA,
                GameObjectType.WATCH
        ));

        //HARD
        levels.put(Level.LABORATORY, Arrays.asList(
                GameObjectType.BIG_BOX,
                GameObjectType.BIG_ROUND_FLASK,
                GameObjectType.BLACK_INGREDIENT,
                GameObjectType.BLACK_LIQUID,
                GameObjectType.BLUE_LIQUID,
                GameObjectType.BOOK_IN_STAND,
                GameObjectType.BOOKS,
                GameObjectType.DIARY,
                GameObjectType.GRAY_INGREDIENT,
                GameObjectType.GREEN_LIQUID,
                GameObjectType.LIGHT_BURNER,
                GameObjectType.LIGHT_OFF_BURNER,
                GameObjectType.LIGHT_TUBE,
                GameObjectType.OBLONG_FLASK,
                GameObjectType.OPEN_BOOK,
                GameObjectType.ORANGE_LIQUID,
                GameObjectType.PAPER_BOX,
                GameObjectType.PEN,
                GameObjectType.PILE_OF_BOXES,
                GameObjectType.ROUND_FLASK,
                GameObjectType.SMALL_BOX,
                GameObjectType.SMOKE_TUBE,
                GameObjectType.TUBE,
                GameObjectType.TUBES_IN_STAND,
                GameObjectType.WHITE_INGREDIENT,
                GameObjectType.YELLOW_LIQUID

        ));

        //BONUS
        levels.put(Level.TRAVELER_ROOM, Arrays.asList(
                GameObjectType.BAG,
                GameObjectType.BIG_BOOK,
                GameObjectType.BIG_GLOBE,
                GameObjectType.BROWN_BOOK,
                GameObjectType.CHESS_TABLE,
                GameObjectType.ELEPHANT,
                GameObjectType.GIRAFFE,
                GameObjectType.GRAY_BOOK,
                GameObjectType.KABUKI,
                GameObjectType.KIMONO,
                GameObjectType.LETTERS,
                GameObjectType.LION,
                GameObjectType.MASK_AFRICA,
                GameObjectType.OPEN_BOOK,
                GameObjectType.SHELLS,
                GameObjectType.SMALL_BOOK,
                GameObjectType.SMALL_GLOBE,
                GameObjectType.PILE_OF_BOOKS,
                GameObjectType.STAND_FOR_POSTER,
                GameObjectType.TABLE_WITH_MAPS,
                GameObjectType.CHINESE_VASE,
                GameObjectType.COMPASS,
                GameObjectType.TRAVEL_HAT
        ));
    }

    static List<GameObjectType> getObjectsByDifficulty(GameDifficulty difficulty){
        int index = Level.getLevelIndexByDifficulty(difficulty);
        List<GameObjectType> types = levels.get(Level.values()[index]);
        return types;
    }

    static Level getLevelByDifficulty(GameDifficulty difficulty){
        int index = Level.getLevelIndexByDifficulty(difficulty);
        return Level.values()[index];
    }



}
