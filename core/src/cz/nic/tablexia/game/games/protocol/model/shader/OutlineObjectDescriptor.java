/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.model.shader;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import cz.nic.tablexia.game.games.protocol.gameobjects.TypeObjectDescriptor;

/**
 * Created by lmarik on 12.3.18.
 */

public class OutlineObjectDescriptor {

    private TextureRegion textureRegion;
    private Vector2 screenPosition;
    private float width;
    private float height;
    private float yMiddlePosition;
    private Vector2 lastPosition;
    private float[] rectDescriptor;
    private TypeObjectDescriptor typePosition; // null = on the floor. not null == object is on the furniture
    boolean needControl;

    OutlineObjectDescriptor(TextureRegion textureRegion, Vector2 screenPosition, TypeObjectDescriptor typePosition, float width, float height, float yMiddlePosition, float[] rectDescriptor, boolean needControl) {
        this.textureRegion = textureRegion;
        this.needControl = needControl;
        this.screenPosition = screenPosition;
        this.typePosition = typePosition;
        this.width = width;
        this.height = height;
        this.yMiddlePosition = yMiddlePosition;
        this.rectDescriptor = rectDescriptor;
        lastPosition = screenPosition;
    }

    public void updateData(Vector2 screenPosition, TypeObjectDescriptor typePosition, float yMiddlePosition) {
        this.screenPosition = screenPosition;
        this.typePosition = typePosition;
        this.yMiddlePosition = yMiddlePosition;

        needControl = (screenPosition.x != lastPosition.x || screenPosition.y != lastPosition.y);

    }

    public Rectangle getObjectRect() {
        return new Rectangle(screenPosition.x + width * rectDescriptor[0], screenPosition.y + height * rectDescriptor[1], width * (1f - rectDescriptor[0]), height * (1f - rectDescriptor[1]));
    }


    public TextureRegion getTextureRegion() {
        return textureRegion;
    }

    public Vector2 getScreenPosition() {
        return screenPosition;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getyMiddlePosition() {
        return yMiddlePosition;
    }

    public Vector2 getLastPosition() {
        return lastPosition;
    }

    public TypeObjectDescriptor getTypePosition() {
        return typePosition;
    }

    @Override
    public String toString() {
        return ": SCREEN POSITION: " + screenPosition.toString() + " MIDDLE: " + yMiddlePosition + " LAST POSITION: " + (lastPosition != null ? lastPosition.toString() : " is null");
    }
}
