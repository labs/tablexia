/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.gameobjects.descriptors;

import com.badlogic.gdx.math.Vector2;

public class EdgePoints {

    private float[] lTRatio;
    private float[] lBRatio;
    private float[] rTRatio;
    private float[] rBRatio;

    public EdgePoints(float[] lTRatio, float[] lBRatio, float[] rTRatio, float[] rBRatio){
        this.lTRatio = lTRatio;
        this.lBRatio = lBRatio;
        this.rTRatio = rTRatio;
        this.rBRatio = rBRatio;
    }

    public float[] getlTRatio() {
        return lTRatio;
    }

    public float[] getlBRatio() {
        return lBRatio;
    }

    public float[] getrTRatio() {
        return rTRatio;
    }

    public float[] getrBRatio() {
        return rBRatio;
    }

    public Vector2 getlBPoint(float w, float h) {
        return new Vector2(w * lBRatio[0], h * lBRatio[1]);
    }

    public Vector2 getrBPoint(float w, float h) {
        return new Vector2(w * rBRatio[0], h * rBRatio[1]);
    }

    public Vector2 getlTPoint(float w, float h) {
        return new Vector2(w * lTRatio[0], h * lTRatio[1]);
    }

    public Vector2 getrTPoint(float w, float h) {
        return new Vector2(w * rTRatio[0], h * rTRatio[1]);
    }
}
