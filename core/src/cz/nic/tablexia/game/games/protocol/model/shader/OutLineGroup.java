/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.model.shader;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import cz.nic.tablexia.game.games.protocol.controller.RoomPosition;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObject;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.ObjectModel;
import cz.nic.tablexia.game.games.protocol.gameobjects.TypeObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.FurnitureType;
import cz.nic.tablexia.util.Log;


/**
 * Created by lmarik on 29.1.18.
 */

public class OutLineGroup extends Group {
    private static final float OVER_PERCENT = 90.0f;

    private Map<TypeObjectDescriptor, OutLineObject> objectScreenPosition;
    private List<TypeObjectDescriptor> textureMap;
    private Stack<TypeObjectDescriptor> updatedStack;
    private ShaderProgram shaderProgram;

    public OutLineGroup() {
        textureMap = new ArrayList<>();
        objectScreenPosition = new HashMap<>();
        updatedStack = new Stack<>();

        shaderProgram = new ShaderProgram(Shader.VERTEX_SHADER, Shader.FRAGMENT_SHADER);
        if (!shaderProgram.isCompiled()) {
            throw new GdxRuntimeException("Couldn't compile shader: " + shaderProgram.getLog());
        } else {
            Log.info(getClass(), "Shader compiled");
        }
    }


    public void updateMap() {
        TypeObjectDescriptor update;
        boolean collision;

        updatedStack.addAll(textureMap);

        while (!updatedStack.isEmpty()) {
            collision = false;
            update = updatedStack.pop();
            OutLineObject updateObject = objectScreenPosition.get(update);
            OutlineObjectDescriptor updateDescriptor = updateObject.getDescriptor();

            for (TypeObjectDescriptor control : objectScreenPosition.keySet()) {
                if (control != update) {
                    OutLineObject controlObject = objectScreenPosition.get(control);
                    OutlineObjectDescriptor controlDescriptor = controlObject.getDescriptor();

                    Rectangle updateRect = updateDescriptor.getObjectRect();
                    Rectangle controlRect = controlDescriptor.getObjectRect();

                    if (updateDescriptor.getTypePosition() != control) {
                        if (isBehindRendered(updateObject,controlObject) && checkCollision(updateRect, controlRect)) {
                            addTypeToMap(update);
                            addActor(updateObject);
                            collision = true;
                        } else if (isBehindRendered(controlObject, updateObject) && checkCollision(controlRect, updateRect)) {
                            addTypeToMap(control);
                            addActor(controlObject);
                        }
                    }

                }
            }

            if (!collision && textureMap.contains(update)) {
                textureMap.remove(update);
                removeActor(updateObject);
            }
        }

    }

    private boolean checkCollision(Rectangle firstR, Rectangle secondR) {
        if (computeArea(firstR.getWidth(), firstR.getHeight()) > computeArea(secondR.getWidth(), secondR.getHeight()) || !firstR.overlaps(secondR))
            return false;

        float xOver = Math.max(firstR.getX(), secondR.getX());
        float yOver = Math.max(firstR.getY(), secondR.getY());

        float wOver = Math.min(firstR.getX() + firstR.getWidth(), secondR.getX() + secondR.getWidth());
        float hOver = Math.min(firstR.getY() + firstR.getHeight(), secondR.getY() + secondR.getHeight());


        float diffArea = computeArea(wOver - xOver, hOver - yOver);
        float control = computeArea(firstR.getWidth(), firstR.getHeight());
        float percentOver = (diffArea * 100 / control);

        return percentOver >= OVER_PERCENT;
    }

    private boolean isBehindRendered(OutLineObject first, OutLineObject second){
        if(first.getOutLineObjectType() == OutLineObject.OutLineObjectType.GAMEOBJECT){

            GameObject firstObject = (GameObject) first.getParent();

            if(firstObject.getRoomPosition() == RoomPosition.FURNITURE && second.getParent().getRoomPosition() != RoomPosition.FURNITURE)
                return firstObject.getFurniture().getZIndex() < second.getParent().getZIndex();

        }


        return first.getParent().getZIndex() < second.getParent().getZIndex();
    }

    private float computeArea(float w, float h) {
        return w * h;
    }

    private void addTypeToMap(TypeObjectDescriptor type) {
        if (!textureMap.contains(type)) {
            textureMap.add(type);
        }
    }

    public void disposeShader() {
        Log.info(getClass(), "Shader dispose");
        shaderProgram.dispose();
    }

    public void addObject(ObjectModel objectModel, TypeObjectDescriptor type, TextureRegion textureRegion, TypeObjectDescriptor typePosition, float xPosition, float yPosition, float width, float height, float yMiddlePosition) {
        if (!objectScreenPosition.containsKey(type)) {
            OutLineObject.OutLineObjectType outLineObjectType = type instanceof GameObjectType ? OutLineObject.OutLineObjectType.GAMEOBJECT : OutLineObject.OutLineObjectType.FURNITURE;
            OutlineObjectDescriptor descriptor = new OutlineObjectDescriptor(
                    textureRegion,
                    new Vector2(xPosition, yPosition),
                    typePosition,
                    width,
                    height,
                    yMiddlePosition,
                    getRectDescriptor(type),
                    (type instanceof GameObjectType));
            objectScreenPosition.put(type, new OutLineObject(outLineObjectType, objectModel, descriptor, shaderProgram));
        } else {
            objectScreenPosition.get(type).getDescriptor().updateData(new Vector2(xPosition, yPosition), typePosition, yMiddlePosition);
        }

        if (objectScreenPosition.get(type).getOutLineObjectType() == OutLineObject.OutLineObjectType.GAMEOBJECT) {
            updatedStack.add(type);
        }
    }

    private float[] getRectDescriptor(TypeObjectDescriptor type) {
        if (type.getOriginType() == FurnitureType.HARD_SMALL_TABLE.getOriginType() ||
                type.getOriginType() == FurnitureType.HARD_BIG_TABLE.getOriginType() ||
                type.getOriginType() == FurnitureType.BONUS_BIG_TABLE.getOriginType() ||
                type.getOriginType() == FurnitureType.BONUS_SMALL_TABLE.getOriginType()) {
            return new float[]{0.0f, 0.4f};
        }else if(type.getOriginType() == GameObjectType.CHESS_TABLE.getOriginType() ||
                type.getOriginType() == GameObjectType.TABLE.getOriginType()){
            return new float[]{0.0f, 0.58f};
        }

        return new float[]{0.0f, 0.0f};
    }

    public void removeObject(TypeObjectDescriptor type) {
        if (!objectScreenPosition.containsKey(type))
            return;

        removeTextureFromMap(type);
        objectScreenPosition.remove(type);
    }

    public void removeTextureFromMap(TypeObjectDescriptor type) {
        if (textureMap.contains(type)) {
            textureMap.remove(type);
            removeActor(objectScreenPosition.get(type));
        }
    }


    public void clearData() {
        for (Iterator<TypeObjectDescriptor> it = objectScreenPosition.keySet().iterator(); it.hasNext(); ) {
            if (it.next() instanceof GameObjectType) {
                it.remove();
            }
        }

        clearChildren();

        textureMap.clear();
        updatedStack.clear();
    }
}
