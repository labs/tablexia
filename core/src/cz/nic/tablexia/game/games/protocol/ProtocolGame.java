/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.StringBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;
import cz.nic.tablexia.game.games.protocol.controller.PositionPair;
import cz.nic.tablexia.game.games.protocol.controller.RoomPosition;
import cz.nic.tablexia.game.games.protocol.controller.ObjectsController;
import cz.nic.tablexia.game.games.protocol.controller.PositionCounter;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObject;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.ObjectModel;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.WallObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;
import cz.nic.tablexia.game.games.protocol.model.CardsWidget;
import cz.nic.tablexia.game.games.protocol.model.RoomGroup;
import cz.nic.tablexia.game.games.protocol.model.ProtocolGameState;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.Furniture;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.FurnitureType;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.ProtocolGenerator;
import cz.nic.tablexia.game.games.protocol.model.shader.OutLineGroup;
import cz.nic.tablexia.game.games.protocol.utils.PositionTransfer;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaTextureManager;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.resolvers.ProtocolScoreResolver;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Utility;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.button.GameImageTablexiaButton;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

/**
 * Created by lmarik on 3.5.17.
 */

public class ProtocolGame extends AbstractTablexiaGame<ProtocolGameState> {

    public static final String ROOM_GROUP           = "room group";
    public static final String CARDS_WIDGET_GROUP   = "cards widget group";
    public static final String FIXED_OBJECT         = "fixed object";
    public static final String OBJECT               = "object ";
    public static final String FURNITURE            = "furniture ";
    public static final String ROUND_BUTTON         = "round button";

    public static final String BIGGER_SCALE_EVENT   = "bigger scale event: ";
    public static final String SMALLER_SCALE_EVENT  = "smaller scale event: ";
    public static final String NEW_ROUND_EVENT      = "new round event";
    public static final String FINISH_CONTROL_EVENT = "finish control event";
    public static final String DROP_EVENT           = "drop event: ";


    private enum ResultMapping {

        NO_STAR_TEXT	(GameResult.NO_STAR,    ProtocolAssets.VICTORY_NO_STAR_TEXT, 		ProtocolAssets.VICTORY_NO_STAR_SOUND),
        ONE_STAR_TEXT	(GameResult.ONE_STAR, 	ProtocolAssets.VICTORY_ONE_STAR_TEXT, 		ProtocolAssets.VICTORY_ONE_STAR_SOUND),
        TWO_STAR_TEXT	(GameResult.TWO_STAR, 	ProtocolAssets.VICTORY_TWO_STAR_TEXT, 		ProtocolAssets.VICTORY_TWO_STAR_SOUND),
        THREE_STAR_TEXT	(GameResult.THREE_STAR, ProtocolAssets.VICTORY_THREE_STAR_TEXT,	    ProtocolAssets.VICTORY_THREE_STAR_SOUND);

        private final GameResult gameResult;
        private final String textKey;
        private final String soundName;

        ResultMapping(GameResult gameResult, String textKey, String soundName) {
            this.gameResult = gameResult;
            this.textKey = textKey;
            this.soundName = soundName;
        }

        public String getTextKey() {
            return textKey;
        }

        public String getSoundName() {
            return soundName;
        }

        public static ProtocolGame.ResultMapping getResultTextMappingForGameResult(GameResult gameResult) {
            for (ProtocolGame.ResultMapping resultMapping : ProtocolGame.ResultMapping.values()) {
                if (resultMapping.gameResult.equals(gameResult)) {
                    return resultMapping;
                }
            }
            return null;
        }
    }

    public  static final int           MAX_ROUNDS                      = 3;
    private static final float         ANIMATION_DURATION              = 0.3f;
    private static final float         DIMMER_ANIMATION_DURATION       = 1.2f;
    private static final float         COLOR_CHANGE_DELAY              = 0.5f;
    private static final float         NEXT_ROUND_DELAY                = 5.5f;
    public  static final float         PANEL_WIDTH                     = 175f;

    private static final float         PROTOCOL_GROUP_HEIGHT           = 0.22f;
    private static final float         PROTOCOL_TEXT_PADDING           = 5f;
    private static final float         COLOR_MAP_HEIGHT_RATIO          = 0.594f;
    private static final float         ROOM_WIDTH                      = 660f;
    private static final float         ROOM_HEIGHT                     = 485f;

    private static final float         ROOM_WIDTH_RATIO               = 0.8f;

    public  static final float           CARD_SMALL_SIZE                 = 110f;
    public  static final float           CARD_BIG_SIZE                   = 225f;

    private static final int           FINISH_BUTTON_THRESHOLD_WIDTH   = 70;
    private static final int           FINISH_BUTTON_WIDTH             = 145;
    private static final int           FINISH_BUTTON_HEIGHT            = 72;
    private static final int           FINIS_BOTTOM_PADDING            = 10;

    private float                      difSizePercent                  = 100;

    private static final ApplicationFontManager.FontType     TEXT_FONT       = ApplicationFontManager.FontType.REGULAR_18;
    private static final Color                               LABEL_COLOR     = Color.GRAY;

    private Pixmap                      colorMapPixmap;
    private int                         colorMapWidth;
    private int                         colorMapHeight;

    private ObjectsController           controller;
    private PositionCounter             positionCounter;

    private TablexiaLabel               protocolLabel;

    private OutLineGroup                outLineGroup;
    private Group                       draggedLayout;
    private GameObject                  draggedObject;

    private RoomGroup                   roomGroup;
    private CardsWidget                 cardsWidget;
    private Image                       dimmerScreen;

    private Image                       background;
    private Image                       protocolBackground;
    private Image                       cardsWidgetBackground;

    private GameImageTablexiaButton     roundButton;

    private HashMap<GameObjectType,GameObject> objects = new HashMap<>();

    private long    roundTime                   = 0;
    private long    startPauseTime              = 0;
    private long    sumPauseTime                = 0;
    private int     objectControlIndex          = 0;

    private boolean dragDisable                 = false;
    private boolean paused                      = false;
    private boolean buttonNextRound             = false;
    private boolean controlPosition             = false;
    private boolean actionColorFinished         = false;
    private boolean mistakeInRound              = false;
    private float   secondsElapsed;

    private TablexiaTextureManager textureManager;


/////////////////////////////// SCREEN LOADERS

    @Override
    protected ProtocolGameState prepareGameData(Map<String, String> gameState) {
        setGameScore(ProtocolScoreResolver.SCORE_COUNT, 0);
        setGameScore(ProtocolScoreResolver.SCORE_CONTROLLED, 0);
        setGameScore(ProtocolScoreResolver.SCORE_MISTAKE_ROUND, 0);
        setGameScore(ProtocolScoreResolver.FINISHED_ROUNDS, 0);

        ProtocolGameState state = ProtocolGameState.ProtocolGameStateFactory.createInstance(getGameDifficulty(), getRandom(), ProtocolGame.this);

        String clickMapName = prepareScreenAssetsPath(prepareScreenName())+ "excluded/clickmap_" + getGameDifficulty().name().toLowerCase() + ".png";
        final Object loaderLock = new Object();
        synchronized (loaderLock) {

            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    synchronized (loaderLock) {
                        tryToDisposeTextureManager();
                        textureManager = new TablexiaTextureManager(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL);
                        textureManager.loadTexture(clickMapName);
                        textureManager.finishLoading();
                        loaderLock.notify();
                    }
                }
            });

            try {
                loaderLock.wait();
            } catch (InterruptedException e) {
                Log.err(getClass(), "Error while waiting to texture loader!", e);
            }
        }

        Texture clickMap = textureManager.getTexture(clickMapName);
        if (!clickMap.getTextureData().isPrepared()) {
            clickMap.getTextureData().prepare();
        }

        colorMapWidth = clickMap.getWidth();
        colorMapHeight = clickMap.getHeight();

        colorMapPixmap = clickMap.getTextureData().consumePixmap();

        return state;
    }

    private void tryToDisposeTextureManager() {
        if (textureManager != null) {
            textureManager.dispose();
            textureManager = null;
        }
    }


/////////////////////////////// SCREEN LIFECYCLE

    @Override
    protected void gameLoaded(Map<String, String> gameState) {
        if (!objects.isEmpty())
            objects.clear();

        //Room Group
        roomGroup = new RoomGroup(this);
        roomGroup.setName(ROOM_GROUP);

        background = new Image(getScreenTextureRegion(getData().getRoomBackground()));
        getStage().addActor(background);
        draggedLayout = new Group();
        draggedLayout.setTouchable(Touchable.childrenOnly);
        draggedObject = null;
        dragDisable = false;

        outLineGroup = new OutLineGroup();
        outLineGroup.setTouchable(Touchable.disabled);

        //Protocol Label
        initProtocolTitle();

        //Menu
        cardsWidget = new CardsWidget(this);
        cardsWidget.setName(CARDS_WIDGET_GROUP);
        cardsWidgetBackground = new Image(getScreenTextureRegion(ProtocolAssets.MENU_BACKGROUND));

        //Round button
        initRoundButton();

        //dimmer
        dimmerScreen = new Image(ApplicationAtlasManager.getInstance().getColorTexture(Color.BLACK));
        dimmerScreen.setVisible(false);


        getStage().addActor(protocolBackground);
        getStage().addActor(cardsWidgetBackground);
        getStage().addActor(protocolLabel);
        getStage().addActor(roomGroup);
        getStage().addActor(outLineGroup);
        getStage().addActor(cardsWidget);
        getStage().addActor(roundButton);
        getStage().addActor(dimmerScreen);

        setComponentsPositionAndSize();

        initFurniture();
        createGameObjects();

        getStage().addActor(draggedLayout);

        getStage().addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if(draggedObject != null && draggedObject.isBiggerScale() && !draggedObject.isScaling()){
                    setDragDisable(draggedObject.isBiggerScale());
                    draggedObject.scaleCard();
                }
            }
        });


        roundTime = System.currentTimeMillis();
        buttonNextRound = false;
    }

    @Override
    public void screenResized(int width, int height) {
        setComponentsPositionAndSize();
    }

    @Override
    protected void gamePaused(Map<String, String> gameState) {
        prepareObjectsForPause();
        paused = true;
    }

    @Override
    protected void gameResumed() {
        if (paused) {
            resetObjectsAfterResume();
        }

        paused = false;
    }

    @Override
    protected void gameDisposed() {
        if(outLineGroup != null){
            outLineGroup.disposeShader();
        }
        tryToDisposeTextureManager();
    }

    @Override
    protected void gameEnd() {
        if (dimmerScreen != null) {
            dimmerScreen.clearActions();
        }
        getStage().clear();
    }

    private void gameFinished() {
        long time = System.currentTimeMillis() - roundTime - sumPauseTime;
        setGameScore(String.format(ProtocolScoreResolver.ROUND_TIME_GAME_SCORE_KEY, getData().getPlayedRound() + 1), time);
    }

/////////////////////////////// INIT SCREEN COMPONENT

    private void initRoundButton() {
        int buttonWidth = (ComponentScaleUtil.isUnderThreshold() ? FINISH_BUTTON_THRESHOLD_WIDTH : FINISH_BUTTON_WIDTH);

        float xPosition = getViewportWidth() - PANEL_WIDTH - buttonWidth;
        float yPosition = getViewportBottomY() + FINIS_BOTTOM_PADDING;

        roundButton = createButton(ProtocolAssets.COMPLETE_TEXT, ApplicationInternalTextureManager.BUTTON_YES_ICON, xPosition, yPosition);
        roundButton.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {

                if (buttonNextRound) {

                    if (!controlPosition)
                        return;

                    if (objectControlIndex <= getData().getProtocolObjectTypes().size() - 1) {
                        GameObjectType type = getData().getProtocolObjectTypes().get(objectControlIndex);
                        objects.get(type).clearActions();
                    }

                    startNextRound();

                } else {

                    gameFinished();

                    roomGroup.setTouchable(Touchable.disabled);
                    roundButton.setEnabled(false);
                    controller.controlPositions(objects);
                    enableControl(true);
                }
            }
        });
        roundButton.setName(ROUND_BUTTON);

    }

    private GameImageTablexiaButton createButton(String textPath, String imagePath, float x, float y) {
        GameImageTablexiaButton button = new GameImageTablexiaButton(getText(textPath), new Image(ApplicationInternalTextureManager.getInstance().getTexture(imagePath)));
        button.setBounds(
                x,
                y,
                FINISH_BUTTON_WIDTH,
                FINISH_BUTTON_HEIGHT
        );
        button.onlyOnClick(true);
        button.setEnabled(false);

        return button;
    }

    private void initProtocolTitle() {
        protocolBackground = new Image(getScreenTextureRegion(ProtocolAssets.RULE_BACKGROUND));

        protocolLabel = new TablexiaLabel("", new TablexiaLabel.TablexiaLabelStyle(TEXT_FONT, LABEL_COLOR), true);
        protocolLabel.setAlignment(Align.center);
        protocolLabel.setWrap(true);

        addProtocolText();
    }

    private void addProtocolText() {
        Log.info(getClass(), getData().getProtocolText());
        protocolLabel.setText(getData().getProtocolText());
    }

    private void setComponentsPositionAndSize() {
        float labelHeight = getViewportHeight() * PROTOCOL_GROUP_HEIGHT;

        //Room group
        float groupWidth = getViewportWidth() - PANEL_WIDTH;
        float groupHeight = getViewportHeight() - labelHeight;


        float roomWidth = groupWidth * ROOM_WIDTH_RATIO;
        difSizePercent = (roomWidth * 100) / ROOM_WIDTH;
        float roomHeight = (ROOM_HEIGHT * difSizePercent) /100;

        float xRoom = getViewportLeftX() + groupWidth/2 - roomWidth/2;
        float yRoom = getViewportBottomY() + groupHeight/2 - roomHeight/2;

        roomGroup.setBounds(xRoom, yRoom, roomWidth, roomHeight);
        background.setBounds(xRoom, yRoom, roomWidth, roomHeight);
        draggedLayout.setBounds(xRoom, yRoom, roomWidth, roomHeight);
        outLineGroup.setBounds(xRoom,yRoom,roomWidth,roomHeight);

        //Protocol Label
        protocolBackground.setBounds(getViewportLeftX(), getViewportTopY() - labelHeight, getViewportWidth(), labelHeight);
        protocolLabel.setBounds(getViewportLeftX() + AbstractMenu.MAX_OPEN_CLOSE_BUTTON_WIDTH, getViewportTopY() - labelHeight + PROTOCOL_TEXT_PADDING, getViewportWidth() - AbstractMenu.MAX_OPEN_CLOSE_BUTTON_WIDTH
                - PANEL_WIDTH, labelHeight - PROTOCOL_TEXT_PADDING);

        //Menu
        cardsWidgetBackground.setBounds(getViewportWidth() - PANEL_WIDTH, getViewportBottomY(), PANEL_WIDTH, getViewportHeight());
        cardsWidget.setBounds(getViewportWidth() - PANEL_WIDTH, getSceneInnerBottomY(), PANEL_WIDTH, getSceneInnerHeight());
        cardsWidget.screenResize();

        //Round button
        int buttonWidth = (ComponentScaleUtil.isUnderThreshold() ? FINISH_BUTTON_THRESHOLD_WIDTH : FINISH_BUTTON_WIDTH);

        float xPosition = getViewportWidth() - PANEL_WIDTH - buttonWidth;
        float yPosition = getViewportBottomY() + FINIS_BOTTOM_PADDING;
        roundButton.setPosition(xPosition, yPosition);

        //dimmer
        dimmerScreen.setBounds(0, getViewportBottomY(), getViewportWidth(), getViewportHeight());
    }

//////////////////////////////// INIT FURNITURE

    private void initFurniture() {
        List<FurnitureType> furnitureTypes = getData().getFurniture();
        if (furnitureTypes == null)
            return;

        for (FurnitureType type : furnitureTypes) {
            float[] ratio = type.getPositionRation();

            float xPosition = ROOM_WIDTH * ratio[0];
            float yPosition = ROOM_HEIGHT * ratio[1];
            xPosition = (xPosition * difSizePercent) /100;
            yPosition = (yPosition * difSizePercent) /100;

            Furniture furniture = new Furniture(type, getScreenTextureRegion(type.getPathResource()),difSizePercent);
            furniture.setPosition(xPosition, yPosition);
            furniture.setName(FURNITURE + type);
            roomGroup.addFurniture(furniture, type.getMapPosition(), xPosition, yPosition);
            addObjectForOutline(furniture, false);
        }
    }

/////////////////////////////// INIT PROTOCOL OBJECTS

    private void createGameObjects() {

        ProtocolGenerator generator = getData().getProtocolGenerator();

        controller = generator.getController();
        positionCounter = generator.getPositionCounter();
        //Log.info(getClass(), controller.toString());

        List<GameObjectType> types = getData().getProtocolObjectTypes();

        //create objects map
        for (GameObjectType type : types) {
            addNewObjectByType(type, generator);
        }

        //Add objects to gamescene by random
        List<GameObjectType> randomListType = new ArrayList<>(objects.keySet());
        Collections.shuffle(randomListType, getRandom());

        //first object is in the room
        fixObjectPosition(randomListType.get(0), controller.getPositionDescriptionByGameObjectType(randomListType.get(0)));

        //another objects are in the menu
        for (int i = 1; i < randomListType.size(); i++) {
            GameObjectType type = randomListType.get(i);
            objects.get(type).setInRoom(false);
            objects.get(type).setName(OBJECT + type);
            addGameObjectToMenu(objects.get(type));
        }

    }

    private void fixObjectPosition(GameObjectType objectType, List<PositionPair> positionPairs) {
        GameObject object = objects.get(objectType);

        if (positionPairs == null || positionPairs.isEmpty() || positionPairs.get(0).getObjectType() instanceof GameObjectType) {
            float[] middleRatio = getData().getMiddleRatioInMap();

            float xPosition = roomGroup.getWidth() * middleRatio[0] - object.getCenterPosition().x;
            float yPosition = roomGroup.getHeight() * middleRatio[1] - object.getCenterPosition().y;

            float[] middleShift = positionCounter.countMiddleShifts(objectType);

            xPosition += middleShift[0];
            yPosition += middleShift[1];

            object.setRoomPosition(RoomPosition.FLOOR);
            addGameObjectToRoom(object, xPosition, yPosition, new PositionTransfer(RoomPosition.FLOOR, null, null));
        } else if (positionPairs.get(0).getObjectType() instanceof FurnitureType) {
            FurnitureType furnitureType = (FurnitureType) positionPairs.get(0).getObjectType();
            Furniture furniture = roomGroup.getFurnitureByType(furnitureType);
            float[] fixRatio = furnitureType.getFixPositionRatio();

            float xPosition = furniture.getX() + (furniture.getFurnitureImage().getWidth() * fixRatio[0]) - object.getPutPoint().x;
            float yPosition = furniture.getY() + (furniture.getFurnitureImage().getHeight() * fixRatio[1]) - object.getPutPoint().y;

            object.setRoomPosition(RoomPosition.FURNITURE);
            object.setFurniture(furniture);

            addGameObjectToRoom(object, xPosition, yPosition, new PositionTransfer(RoomPosition.FURNITURE, furnitureType, null));
        } else if (positionPairs.get(0).getObjectType() instanceof WallType) {
            WallType wallType = (WallType) positionPairs.get(0).getObjectType();
            WallObjectDescriptor descriptor = WallObjectDescriptor.getDescriptorByType(objectType);

            float[] fixRatio = descriptor.getFixRatio(wallType);

            float xPosition = roomGroup.getWidth() * fixRatio[0] - object.getWallPoint().x;
            float yPosition = roomGroup.getHeight() * fixRatio[1] - object.getWallPoint().y;

            object.setRoomPosition(RoomPosition.WALL);
            object.setWallPositionType(wallType);

            addGameObjectToRoom(object, xPosition, yPosition, new PositionTransfer(RoomPosition.WALL, null, wallType));
        }

        object.setName(FIXED_OBJECT);
        object.setFixed(true);
        object.destroyListener();
        object.setTouchable(Touchable.disabled);
        addObjectForOutline(object,false);
    }

    private void addNewObjectByType(GameObjectType type, ProtocolGenerator generator) {
        String cardText = generator.getObjectCardText(this, type);
        objects.put(type, new GameObject(type, this, cardText, true));
    }

/////////////////////////////// PROTOCOL OBJECTS ACTION

    public void addGameObjectToRoom(GameObject object, float xPosition, float yPosition, PositionTransfer positionTransfer) {
        removeGameObjectFromMenu(object);
        object.setInRoom(true);
        object.setSelected(false);
        roomGroup.addObject(object, xPosition, yPosition, positionTransfer);
        checkLayoutAfterAction();
    }

    public void addGameObjectToMenu(GameObject object) {
        removeGameObjectFromPicture(object);
        removeObjectFromOutline(object);
        object.setInRoom(false);
        object.setSelected(false);
        cardsWidget.addGameObject(object);
        checkLayoutAfterAction();
    }

    public void addObjectToDragLayout(GameObject object, float xPosition, float yPosition) {
        if (object == draggedObject) {
            return;
        }

        outLineGroup.removeTextureFromMap(object.getType());
        draggedLayout.addActor(object);
        object.setPosition(xPosition, yPosition);
        object.setLastPosition(new Vector2(xPosition, yPosition));
        draggedObject = object;
        removeObjectFromOutline(object);
        updateOutline();
    }


    public boolean isDragHaveAnotherObject(GameObject gameObject){
        return draggedObject != null && draggedObject != gameObject;
    }

    public boolean isObjectInDraggedLayout(GameObject object){
        return object == draggedObject;
    }

    public void resetDragLayoutObject() {
        if (draggedObject == null || draggedObject.isBiggerScale() || draggedObject.isScaling()) return;

        if (draggedObject.getRoomPosition() == null) {
            cardsWidget.resetCard(draggedObject.getMenuPosition());
        } else {
            roomGroup.resetActor(draggedObject);
        }

        checkLayoutAfterAction();
    }

    public void checkLayoutAfterAction() {
        clearDraggedObject();
        checkRoomObjectsCount();
    }

    private void clearDraggedObject(){
        if(draggedObject != null) {
            draggedLayout.clearChildren();
            draggedObject = null;
        }
    }

    private void checkRoomObjectsCount(){
        setFinishButtonEnable(roomGroup.getCountObjects() == objects.size());
    }

    private void removeGameObjectFromPicture(GameObject object) {
        if (!object.isInRoom())
            return;

        roomGroup.removeObject(object);
    }

    private void removeGameObjectFromMenu(GameObject object) {
        if (object.isInRoom())
            return;

        cardsWidget.removeCard(object);
    }

//////////////////////////// OUTLINE

    private void updateOutline(){
        outLineGroup.updateMap();
    }

    private void removeObjectFromOutline(ObjectModel objectModel){
        outLineGroup.removeObject(objectModel.getType());
    }

    public void addObjectForOutline(ObjectModel objectModel, boolean needUpdate){
        Vector2 screenPosition = objectModel.getScreenPosition();
        outLineGroup.addObject(
                objectModel,
                objectModel.getType(),
                objectModel.getTexture(),
                objectModel.getTypePosition(),
                screenPosition.x,
                screenPosition.y,
                objectModel.getImageWidth(),
                objectModel.getImageHeight(),
                objectModel.getCenterScreenPosition().y);

        if(needUpdate) {
            updateOutline();
        }
    }

//////////////////////////// GAME SCORE

    private void addMistake() {
        getData().addMistakePosition();
        mistakeInRound = true;
    }

    private void addCorrect() {
        getData().addCorrectPosition();
    }

//////////////////////////// PROTOCOL GAME LIFECYCLE

    private void controlObjectAction(int index, float delay, Runnable finishAction) {
        changeProtocolTextColor(index);
        GameObjectType type = getData().getProtocolObjectTypes().get(index);
        objects.get(type).setControlColorAction(delay, finishAction);

        if (objects.get(type).isCorrect()) {
            addCorrect();
        } else {
            addMistake();
        }
    }

    public void controlObjectPosition(GameObject changedObject, PositionTransfer positionTransfer) {
        roomGroup.objectPositionChanges(changedObject, positionTransfer);
    }

    public void cardsWidgetAnimDropCards(float dragYPosition) {
        cardsWidget.prepareDropAnimCards(dragYPosition);
    }

    public void resetAnimDropCards() {
        cardsWidget.resetAnimDropCards();
    }

    private void prepareObjectsForPause() {

        startPauseTime = System.currentTimeMillis();

        for (GameObject object : objects.values()) {
            object.destroyListener();
            object.clearActions();
            object.setSelected(false);
        }
    }

    private void resetObjectsAfterResume() {
        sumPauseTime += System.currentTimeMillis() - startPauseTime;
        startPauseTime = 0;

        for (GameObject object : objects.values()) {

            object.initDragListener();

            if (object.getLastPosition() != null && !object.isPositionOnLast() && !object.isBiggerScale()) {
                object.returnObjectOnLastPosition(false, null);
            }
        }
        resetDragLayoutObject();
    }

    private void changeProtocolTextColor(int objectIndex) {
        GameObjectType type = getData().getProtocolObjectTypes().get(objectIndex);
        List<String> objectsText = getData().getProtocolGenerator().getObjectsText().get(type);

        String color = getColorNameByCorrect(objects.get(type).isCorrect());

        StringBuilder text = protocolLabel.getText();
        int startObjectText = 0;

        for (String objectText : objectsText) {
            startObjectText = text.toString().toLowerCase().indexOf(objectText.toLowerCase(), startObjectText);

            if (startObjectText == -1 || startObjectText >= text.length()) {
                Log.err(getClass(), "Cannot change label text color in object: " + type);
                return;
            }

            text.insert(startObjectText, color);
            text.insert(startObjectText + objectText.length() + color.length(), "[]");

            startObjectText += objectText.length() + color.length();
        }

        protocolLabel.setText(text.toString());
    }

    private String getColorNameByCorrect(boolean correct) {
        return correct ? "[GREEN]" : "[RED]";
    }

    private void enableControl(boolean enable) {
        controlPosition = enable;
        secondsElapsed = 0;
        actionColorFinished = enable;
        objectControlIndex = 0;
    }

    private void prepareScreenForNextRound() {
        cardsWidget.resetData();
        roomGroup.resetData();
        objects.clear();
        outLineGroup.clearData();

        buttonNextRound = false;
        roundButton.setEnabled(false);
    }

    private void startNextRound() {
        enableControl(false);

        if (mistakeInRound)
            getData().addMistakeRound();

        getData().addPlayedRound();

        setGameScore(ProtocolScoreResolver.SCORE_COUNT, getData().getSumObjectsCount() - getData().getMistakePosition());
        setGameScore(ProtocolScoreResolver.SCORE_CONTROLLED, getData().getObjectsCnt());
        setGameScore(ProtocolScoreResolver.SCORE_MISTAKE_ROUND, getData().getMistakeRound());
        setGameScore(ProtocolScoreResolver.FINISHED_ROUNDS, getData().getPlayedRound());

        if (getData().getPlayedRound() < MAX_ROUNDS) {
            mistakeInRound = false;

            dimmerScreen.setVisible(true);
            dimmerScreen.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(DIMMER_ANIMATION_DURATION), Actions.run(new Runnable() {
                @Override
                public void run() {
                    prepareScreenForNextRound();
                    getData().setNewRound();
                    addProtocolText();
                    createGameObjects();
                    roomGroup.setTouchable(Touchable.enabled);
                    dimmerScreen.addAction(Actions.sequence(Actions.alpha(1), Actions.fadeOut(DIMMER_ANIMATION_DURATION), Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            dimmerScreen.setVisible(false);
                            triggerScenarioStepEvent(NEW_ROUND_EVENT);
                        }
                    })));
                }
            })));

        } else {
            gameComplete();
        }
    }


    @Override
    protected void gameAct(float delta) {
        if (controlPosition) {

            if (actionColorFinished && secondsElapsed > ANIMATION_DURATION) {
                secondsElapsed = 0;
                actionColorFinished = false;
                controlObjectAction(objectControlIndex, getControlDelay(), new Runnable() {
                    @Override
                    public void run() {
                        if (!controlPosition) {
                            return;
                        }

                        if (objectControlIndex == getData().getProtocolObjectTypes().size() - 2) {
                            buttonNextRound = true;
                            roundButton.setEnabled(true);
                            triggerScenarioStepEvent(FINISH_CONTROL_EVENT);
                        }

                        if (objectControlIndex == getData().getProtocolObjectTypes().size() - 1) {
                            startNextRound();
                            return;
                        }

                        objectControlIndex++;
                        actionColorFinished = true;
                    }
                });

            } else {
                secondsElapsed = secondsElapsed + delta;
            }
        }
    }

    private float getControlDelay() {
        return (objectControlIndex < getData().getProtocolObjectTypes().size() - 1) ? COLOR_CHANGE_DELAY : NEXT_ROUND_DELAY;
    }

    public void gameComplete() {
        endGame();
        showGameResultDialog();
    }

/////////////////////////////// COLOR MAP

    public PositionTransfer getRoomPosition(Vector2 middleControl, Vector2 putPoint, Vector2 wallPoint) {
        Color centerColor = getColorByClick(middleControl.x, middleControl.y);

        FurnitureType furnitureType = null;
        WallType wallType = null;

        if(putPoint != null) {
            furnitureType = isAtFurniture(putPoint);
        }

        if(wallPoint != null) {
            wallType = isAtWall(wallPoint);
        }

        //1. Check furniture position
        if(furnitureType != null){
            return new PositionTransfer(RoomPosition.FURNITURE, furnitureType, null);
        }

        //2. Check floor position
        if(isAtFloor(centerColor)){
            return new PositionTransfer(RoomPosition.FLOOR, null, null);
        }

        //3. Check wall position
        if(wallType != null){
            return new PositionTransfer(RoomPosition.WALL, null, wallType);
        }

        return FurnitureType.canPutObjectUnderFurnitures(centerColor, getGameDifficulty()) ? new PositionTransfer(RoomPosition.FLOOR, null, null) : null;
    }

    private boolean isAtFloor(Color touchColor) {
        return (touchColor.equals(Color.BLUE) ||
                touchColor.equals(Color.YELLOW) ||
                touchColor.equals(Color.GREEN));
    }

    private FurnitureType isAtFurniture(Vector2 checkPoint) {
        Color touchColor = getColorByClick(checkPoint.x, checkPoint.y);
        return FurnitureType.getFurnitureTypeByColor(touchColor, getGameDifficulty());
    }

    private WallType isAtWall(Vector2 checkPoint) {
        Color touchColor = getColorByClick(checkPoint.x, checkPoint.y);
        WallType wallType = WallType.getWallTypeByColor(touchColor);

        if(wallType == null)
                wallType = isAtWallBehindFurniture(touchColor, checkPoint);

        return wallType;
    }

    private WallType isAtWallBehindFurniture(Color touchColor, Vector2 objectPoint){
        return FurnitureType.getFurnitureWallByColor(touchColor, objectPoint , roomGroup.getWidth(), roomGroup.getHeight(), getGameDifficulty());
    }

    private Color getColorByClick(float x, float y) {
        int clickX = (int) ((x / (roomGroup.getWidth()) * colorMapWidth));
        int clickY = (int) (colorMapHeight - y / (roomGroup.getHeight() * getColorMapRatio()) * colorMapHeight);

        if (clickX >= colorMapWidth ||
                clickY >= colorMapHeight ||
                clickX < 0 || clickY < 0) {
            return Color.BLACK;
        }
        return new Color(colorMapPixmap.getPixel(clickX, clickY));
    }

    private float getColorMapRatio() {
        return getGameDifficulty() == GameDifficulty.EASY ? COLOR_MAP_HEIGHT_RATIO : 1f;
    }

/////////////////////////////// GETTERS SETTERS


    public RoomGroup getRoomGroup() {
        return roomGroup;
    }

    public boolean isDragDisable() {
        return dragDisable;
    }

    public void setFinishButtonEnable(boolean enable) {
        roundButton.setEnabled(enable);
    }


    public void setDragDisable(boolean biggerScaleAction) {
        this.dragDisable = !biggerScaleAction;
        cardsWidget.setEnableButton(biggerScaleAction);
    }

//////////////////////////// TEST

    public void setBiggerScaleEvent(GameObjectType objectType){
        triggerScenarioStepEvent(BIGGER_SCALE_EVENT + objectType);
    }

    public void setSmallerScaleEvent(GameObjectType objectType){
        triggerScenarioStepEvent(SMALLER_SCALE_EVENT + objectType);
    }

    public void setDropEvent(GameObjectType objectType){
        triggerScenarioStepEvent(DROP_EVENT + objectType);
    }


//////////////////////////// GAME RESULT

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(new SummaryMessage(SummaryImage.STATS,
                getFormattedText(ProtocolAssets.VICTORY_SUMMARY_TEXT, getData().getCorrectPosition(), getData().getSumObjectsCount())));
    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        return ResultMapping.getResultTextMappingForGameResult(gameResult).getTextKey();
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        return ResultMapping.getResultTextMappingForGameResult(gameResult).getSoundName();
    }


//////////////////////////// PRELOADER

    private static final String     PRELOADER_ANIM_IMAGE                = "preloader_anim";
    private static final int        PRELOADER_ANIM_FRAMES               = 9;
    private static final float      PRELOADER_ANIM_FRAME_DURATION       = 0.5f;
    private static final String     PRELOADER_TEXT_KEY                  = ApplicationTextManager.ApplicationTextsAssets.GAME_PROTOCOL_PRELOADER;

    private static final Scaling    PRELOADER_IMAGE_SCALING             = Scaling.fit;
    private static final int        PRELOADER_TEXT_ALIGN 			    = Align.left;
    private static final float      PRELOADER_TEXT_PADDING 			    = 10f;
    private static final float 	    PRELOADER_LEFT_COLUMN_WIDTH_RATIO 	= 1f / 4;
    private static final float 	    PRELOADER_RIGHT_COLUMN_RATIO 		= 5f / 8;
    private static final float 	    PRELOADER_ROW_HEIGHT 				= 1f / 3;

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        AnimatedImage preloaderImage = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_IMAGE, PRELOADER_ANIM_FRAMES, PRELOADER_ANIM_FRAME_DURATION), false);
        preloaderImage.startAnimationLoop();
        String preloaderText = preloaderAssetsManager.getText(PRELOADER_TEXT_KEY);
        components.add(new TwoColumnContentDialogComponent(
                new AnimatedImageContentDialogComponent(preloaderImage, PRELOADER_IMAGE_SCALING),
                new TextContentDialogComponent(preloaderText, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_ROW_HEIGHT));
    }
}
