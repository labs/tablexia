/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.gameobjects.wall;

import com.badlogic.gdx.graphics.Color;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.protocol.gameobjects.TypeObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.EdgePoints;

/**
 * Created by lmarik on 18.8.17.
 */

public enum WallType implements TypeObjectDescriptor{

    LEFT_WALL   (new Color(0xff0000ff),
            ImmutableMap.<GameDifficulty, EdgePoints>builder()
                    .put(GameDifficulty.MEDIUM, new EdgePoints(new float[]{0.018f, 0.709f}, new float[]{0.018f, 0.31f},new float[]{0.353f, 0.98f}, new float[]{0.354f, 0.586f}))
                    .put(GameDifficulty.BONUS, new EdgePoints(new float[]{0.018f, 0.709f}, new float[]{0.018f, 0.49f},new float[]{0.353f, 0.98f}, new float[]{0.354f, 0.775f}))
                    .build()),
    MIDDLE_WALL (new Color(0xff00ffff),
            ImmutableMap.<GameDifficulty, EdgePoints>builder()
                    .put(GameDifficulty.MEDIUM, new EdgePoints(new float[]{0.353f, 0.98f}, new float[]{0.354f, 0.586f}, new float[]{0.65f, 0.98f}, new float[]{0.65f, 0.586f}))
                    .put(GameDifficulty.BONUS, new EdgePoints(new float[]{0.353f, 0.98f}, new float[]{0.354f, 0.775f}, new float[]{0.65f, 0.98f}, new float[]{0.65f, 0.775f}))
                    .build()),
    RIGHT_WALL  (new Color(0x00ffffff),
            ImmutableMap.<GameDifficulty, EdgePoints>builder()
                    .put(GameDifficulty.MEDIUM, new EdgePoints(new float[]{0.65f, 0.98f}, new float[]{0.65f, 0.586f}, new float[]{0.98f, 0.709f}, new float[]{0.98f, 0.31f}))
                    .put(GameDifficulty.BONUS, new EdgePoints(new float[]{0.65f, 0.98f}, new float[]{0.65f, 0.775f}, new float[]{0.98f, 0.709f}, new float[]{0.98f, 0.49f}))
                    .build());

    private Color wallColor;

    private Map<GameDifficulty, EdgePoints> edgePointsMap;

    WallType(Color wallColor, Map<GameDifficulty, EdgePoints> edgePointsMap){
        this.wallColor = wallColor;
        this.edgePointsMap = edgePointsMap;
    }

    public static WallType getWallTypeByColor(Color color){
        for (WallType type: values()){
            if(type.wallColor.equals(color))
                return type;
        }

        return null;
    }

    public EdgePoints getEdgePointsByDifficulty(GameDifficulty difficulty){
        return edgePointsMap.get(difficulty);
    }

    @Override
    public int getOriginType() {
        return ordinal();
    }
}

