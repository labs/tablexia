/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.protocol.ProtocolGame;
import cz.nic.tablexia.game.games.protocol.controller.RoomPosition;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.FurnitureType;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.generators.DEProtocolGenerator;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.ProtocolGenerator;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.generators.CZProtocolGenerator;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.generators.SKProtocolGenerator;
import cz.nic.tablexia.util.Log;

/**
 * Created by lmarik on 9.5.17.
 */

public class ProtocolGameState {

    enum LocaleGenerator {
        CS(TablexiaSettings.LocaleDefinition.cs_CZ.getLocale()) {
            @Override
            public ProtocolGenerator createGenerator(TablexiaRandom random, GameDifficulty difficulty, ProtocolGameState gameState) {
                return new CZProtocolGenerator(random, difficulty, gameState);
            }
        },
        SK(TablexiaSettings.LocaleDefinition.sk_SK.getLocale()) {
            @Override
            public ProtocolGenerator createGenerator(TablexiaRandom random, GameDifficulty difficulty, ProtocolGameState gameState) {
                return new SKProtocolGenerator(random, difficulty, gameState);
            }
        },
        DE(TablexiaSettings.LocaleDefinition.de_DE.getLocale()) {
            @Override
            public ProtocolGenerator createGenerator(TablexiaRandom random, GameDifficulty difficulty, ProtocolGameState gameState) {
                return new DEProtocolGenerator(random, difficulty, gameState);
            }
        };

        private final Locale locale;

        LocaleGenerator(Locale locale) {
            this.locale = locale;
        }

        public static ProtocolGenerator getGeneratorForLocale(Locale locale, TablexiaRandom random, GameDifficulty difficulty, ProtocolGameState gameState) {
            for (LocaleGenerator current : values()) {
                if (current.locale == locale)
                    return current.createGenerator(random, difficulty, gameState);
            }

            throw new RuntimeException("Couldn't get a generator for locale " + locale);
        }

        public ProtocolGenerator createGenerator(TablexiaRandom random, GameDifficulty difficulty, ProtocolGameState gameState) {
            throw new RuntimeException("Method for creating the generator is not implemented!");
        }

    }

    private ProtocolGame            protocolGame;
    private GameDifficulty          difficulty;
    private List<GameObjectType>    protocolObjectTypes;
    private List<GameObjectType>    anotherObjectTypes;
    private Queue<Preposition>      prepositions;
    private TablexiaRandom          random;
    private ProtocolGenerator       protocolGenerator;

    private String protocolText;

    private int mistakeRound    = 0;
    private int sumObjectsCount = 0;
    private int correctPosition = 0;
    private int mistakePosition = 0;
    private int playedRound     = 0;

    private int objectsCnt;
    private int betweenCnt;

    private int horizontalPreposition = 0;
    private int verticalPreposition = 0;

    private ProtocolGameState(GameDifficulty difficulty, TablexiaRandom random, ProtocolGame protocolGame) {
        this.difficulty = difficulty;
        this.random = random;
        this.protocolGame = protocolGame;

        protocolGenerator = initGenerator();
        setNewRound();
    }

/////////////////////////////// INIT GENERATOR

    private ProtocolGenerator initGenerator() {
        return LocaleGenerator.getGeneratorForLocale(TablexiaSettings.getInstance().getLocale(), random, difficulty, this);
    }

/////////////////////////////// LEVEL GENERATE

    public void setNewRound() {
        LevelDefinition.DifficultyDefinition definition = LevelDefinition.DifficultyDefinition.getDefinitionByDifficulty(difficulty);
        if (definition == null) {
            Log.err(getClass(), "Cannot find difficulty definition diff: " + difficulty);
            return;
        }

        prepositions = new LinkedList<>();

        objectsCnt = definition.getObjectsCountByRound(playedRound);
        protocolGenerator.setSentenceByObjectsCnt(objectsCnt, 0, random);

        //set prepostions and objects
        int prepositionsCnt = protocolGenerator.getProtocolText().getPrepositionsCount();
        betweenCnt = setPrepositions(prepositionsCnt, definition.getMaxBetweenPrepositionByRound(playedRound));

        //check betweenCnt
        if (betweenCnt != 0) {
            objectsCnt -= betweenCnt;
            protocolGenerator.setSentenceByObjectsCnt(objectsCnt, betweenCnt, random);

            prepositionsCnt = protocolGenerator.getProtocolText().getPrepositionsCount();

            if (prepositions.size() < prepositionsCnt) {
                //generate next preposition
                setPrepositions(prepositionsCnt - prepositions.size(), 0);

            } else if (prepositions.size() > prepositionsCnt) {
                //remove unnecessary elements
                Iterator<Preposition> it = prepositions.iterator();
                while (prepositions.size() != prepositionsCnt && it.hasNext()) {
                    Preposition preposition = it.next();
                    if (preposition != Preposition.BETWEEN)
                        it.remove();
                }
            }


        }

        setObjects(protocolGenerator.getProtocolText().getSentencePosition());

        //set generator
        protocolGenerator.clearData();

        protocolText = protocolGenerator.getProtocolMessage(protocolGame);
        sumObjectsCount += protocolObjectTypes.size();
    }

    private int setPrepositions(int prepositionsCount, int maxBetween) {

        int i = 0;
        int betweenCnt = 0;
        boolean needAll = betweenCnt != maxBetween;

        while (i < prepositionsCount) {
            Preposition preposition = Preposition.getRandomPreposition(random, needAll);

            if (preposition == Preposition.BETWEEN) {
                betweenCnt++;
                if (betweenCnt == maxBetween) needAll = false;
            }else {
                addTypePrepositionCount(preposition);
            }

            prepositions.add(preposition);
            i++;
        }
        return betweenCnt;
    }

    private void addTypePrepositionCount(Preposition preposition){
        if(preposition == Preposition.LEFT || preposition == Preposition.RIGHT) horizontalPreposition++;
        else verticalPreposition++;

    }

    private void setObjects(ProtocolGenerator.SentencePosition[] sentencePositions) {
        protocolObjectTypes = new ArrayList<>();
        anotherObjectTypes = new ArrayList<>();

        anotherObjectTypes.addAll(LevelDefinition.getObjectsByDifficulty(difficulty));
        Collections.shuffle(anotherObjectTypes, random);

        Iterator<GameObjectType> iterator = anotherObjectTypes.iterator();
        int objectIndex = 0;

        while (protocolObjectTypes.size() != objectsCnt && iterator.hasNext()){
            GameObjectType type = iterator.next();

            if((sentencePositions[objectIndex] == ProtocolGenerator.SentencePosition.FLOOR && type.checkPosition(RoomPosition.FLOOR)) ||
                    (sentencePositions[objectIndex] == ProtocolGenerator.SentencePosition.FURNITURE_WALL && type.checkPosition(RoomPosition.WALL) || type.checkPosition(RoomPosition.FURNITURE))){
                protocolObjectTypes.add(type);
                iterator.remove();
                objectIndex++;
            }
        }

        if(protocolObjectTypes.size() != objectsCnt){
            Log.err(getClass(),"Cannot find all objects by sentence position");
        }
    }

    public void addObject(GameObjectType objectType) {
        protocolObjectTypes.add(objectType);
    }

    public void removeObjectFromAnotherList(GameObjectType objectType) {
        anotherObjectTypes.remove(objectType);
    }

    public Preposition getNextPreposition() {
        return prepositions.remove();
    }

/////////////////////////////// GAME SCORE

    public void addCorrectPosition() {
        correctPosition++;
    }

    public void addMistakePosition() {
        mistakePosition++;
    }

    public void addPlayedRound() {
        playedRound++;
    }

    public void addMistakeRound() {
        mistakeRound++;
    }

/////////////////////////////// BACKGROUND, COLOR MAP, FURNITURE

public String getRoomBackground() {
    return LevelDefinition.getLevelByDifficulty(difficulty).getRoomTexturePath();
}

public float[] getMiddleRatioInMap(){
    return LevelDefinition.getLevelByDifficulty(difficulty).getMiddleRatio();
}

public List<FurnitureType> getFurniture(){
    return FurnitureType.getFurnitureTypesByDifficulty(difficulty);
}

///////////////////////////////  GETTERS

    public GameDifficulty getDifficulty() {
        return difficulty;
    }

    public List<GameObjectType> getProtocolObjectTypes() {
        return protocolObjectTypes;
    }

    public List<GameObjectType> getAnotherObjectTypes() {
        return anotherObjectTypes;
    }

    public String getProtocolText() {
        return protocolText;
    }

    public int getCorrectPosition() {
        return correctPosition;
    }

    public int getMistakePosition() {
        return mistakePosition;
    }

    public int getMistakeRound() {
        return mistakeRound;
    }

    public int getObjectsCnt() {
        return objectsCnt;
    }

    public int getPlayedRound() {
        return playedRound;
    }

    public ProtocolGenerator getProtocolGenerator() {
        return protocolGenerator;
    }

    public int getSumObjectsCount() {
        return sumObjectsCount;
    }

    public int getBetweenCnt() {
        return betweenCnt;
    }

    public int getHorizontalPreposition() {
        return horizontalPreposition;
    }

    public int getVerticalPreposition() {
        return verticalPreposition;
    }

    /////////////////////////////// GAME STATE FACTORY

    public static class ProtocolGameStateFactory {
        public static ProtocolGameState createInstance(GameDifficulty difficulty, TablexiaRandom random, ProtocolGame protocolGame) {
            return new ProtocolGameState(difficulty, random, protocolGame);
        }
    }
}
