/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.model.gameprotocol.generators;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.AbstractObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.AbstractObjectDescriptor.CaseType;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.SKObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.FurnitureType;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;
import cz.nic.tablexia.game.games.protocol.model.ProtocolGameState;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.ProtocolGenerator;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by lmarik on 5.6.17.
 */
public class SKProtocolGenerator extends ProtocolGenerator {

/////////////////////////////// DESCRIPTORS

    private enum ObjectDescriptor{
        //EASY
        ANGEL               (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.CHLAP,    null)),
        BABY_CARRIAGE       (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.DUB,      null)),
        BAG_OF_MARBLES      (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.STROM,    ProtocolAssets.MARBLES)),
        BEAR                (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.CHLAP,    null)),
        BIG_DOLL            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},               SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        BLUE_CAR            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLUE},              SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.MESTO,    null)),
        BOWLING             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.STROM,    null)),
        COLOR_BALL          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.COLOR},             SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        CUBES_TOY           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,       null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        DEVIL               (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.CHLAP,    null)),
        DOG                 (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.CHLAP,    null)),
        DOLLHOUSE           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.STROM,    ProtocolAssets.FOR_DOLLS)),
        DRUM                (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.DUB,      null)),
        FOOTBALL            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.FOOTBALL},          SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        HEN                 (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        INK_STAMPS          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,       null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        NICHOLAS            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.CHLAP,    null)),
        SOLDIER             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.CHLAP,    null)),
        PAPER_DRAKE         (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PAPER},             SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.CHLAP,    null)),
        PULL_DUCK           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PULL},              SKObjectDescriptor.AdjectiveInflection.CUDZI,   SKObjectDescriptor.InflectionType.CHLAP,    null)),
        RAG_DOLL            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RAG},               SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        RED_CAR             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RED},               SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.MESTO,    null)),
        ROCK_HORSE          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ROCK},              SKObjectDescriptor.AdjectiveInflection.CUDZI,   SKObjectDescriptor.InflectionType.CHLAP,    null)),
        ROOSTER             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.CHLAP,    null)),
        SLINGSHOT           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.STROM,    null)),
        SMALL_DOLL          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},             SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        TRAIN               (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.DUB,      null)),
        WHIPPING_TOP        (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        WHISTLE             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        WOODEN_HORSE        (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.WOODEN},            SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.CHLAP,    null)),
        //MEDIUM
        BIG_LAMP            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        BOWL                (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        BOX_WITH_JEWELERY   (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ULICA,    null)),
        CAMERA              (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.STROM,    null)),
        CHAIR               (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.MESTO,    null)),
        CHESS_TABLE         (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.CHESS},             SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.DUB,      null)),
        COLLECTION_OF_COINS (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     ProtocolAssets.COINS)),
        CUCKOO_CLOCK        (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,       new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.CUCKOO},            SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        GLASS_VASE          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GLASS},             SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        GLOBUS              (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.STROM,    null)),
        LADY_AND_PANDA      (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     ProtocolAssets.PANDA)),
        LANDSCAPE           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        MIRROR              (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.MESTO,    null)),
        PEARLS              (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,       null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        PENDULUM_CLOCK      (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PENDULUM},          SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        PORCELAIN_MUG       (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN},         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.DUB,      null)),
        PORCELAIN_PLATE     (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN},         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.DUB,      null)),
        PORCELAIN_POTTY     (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN},         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.DUB,      null)),
        PORCELAIN_STATUETTE (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN},         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        PORCELAIN_VASE      (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN},         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        RED_KETTLE          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RED},               SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ULICA,    null)),
        SEAT                (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        SMALL_LAMP          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        TABLE               (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.DUB,      null)),
        TEA_SET             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.TEA},               SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.DUB,      null)),
        TELESCOPE           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.STROM,    null)),
        TIN_PLATE           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.TIN},               SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.DUB,      null)),
        UMBRELLA            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.DUB,      null)),
        WATCH               (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,       null,                                                                                           null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        // HARD
        BIG_BOX             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                           SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ULICA,     null)),
        BIG_ROUND_FLASK     (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.ROUND}, SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        BLACK_INGREDIENT    (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLACK},                                         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        BLACK_LIQUID        (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLACK},                                         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        BLUE_LIQUID         (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLUE},                                          SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        BOOK_IN_STAND       (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                                       null,                                           SKObjectDescriptor.InflectionType.ZENA,     ProtocolAssets.IN_STAND)),
        BOOKS               (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,       null,                                                                                                                       null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        DIARY               (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                                       null,                                           SKObjectDescriptor.InflectionType.DUB,      null)),
        GRAY_INGREDIENT     (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GRAY},                                          SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        GREEN_LIQUID        (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GREEN},                                         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        LIGHT_BURNER        (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LIGHT},                                         SKObjectDescriptor.AdjectiveInflection.PAVI_R,  SKObjectDescriptor.InflectionType.DUB,      null)),
        LIGHT_OFF_BURNER    (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LIGHT_OFF},                                     SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.DUB,      null)),
        LIGHT_TUBE          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LIGHT},                                         SKObjectDescriptor.AdjectiveInflection.MATKIN,  SKObjectDescriptor.InflectionType.ZENA,     null)),
        OBLONG_FLASK        (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.OBLONG} ,                                       SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        OPEN_BOOK           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.OPEN},                                          SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        ORANGE_LIQUID       (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ORANGE},                                        SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        PAPER_BOX           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PAPER},                                         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ULICA,     null)),
        PEN                 (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                                       null,                                           SKObjectDescriptor.InflectionType.MESTO,    null)),
        PILE_OF_BOXES       (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                                       null,                                           SKObjectDescriptor.InflectionType.ZENA,     ProtocolAssets.OF_BOXES)),
        ROUND_FLASK         (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ROUND},                                         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        SMALL_BOX           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        SMOKE_TUBE          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                                       null,                                           SKObjectDescriptor.InflectionType.ZENA,     ProtocolAssets.SMOKE)),
        TUBE                (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                                       null,                                           SKObjectDescriptor.InflectionType.ZENA,     null)),
        TUBES_IN_STAND      (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,       null,                                                                                                                       null,                                           SKObjectDescriptor.InflectionType.ZENA,     ProtocolAssets.IN_STAND)),
        WHITE_INGREDIENT    (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.WHITE},                                         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        YELLOW_LIQUID       (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.YELLOW},                                        SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,     null)),
        //BONUS
        BAG                 (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                              null,                             SKObjectDescriptor.InflectionType.ZENA,      null)),
        BIG_BOOK            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG,},                                          SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,      null)),
        BIG_GLOBE           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                           SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.STROM,      null)),
        BROWN_BOOK          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BROWN},                                         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,      null)),
        ELEPHANT            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                              null,                             SKObjectDescriptor.InflectionType.ZENA,       ProtocolAssets.ELEPHANT)),
        GIRAFFE             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                              null,                             SKObjectDescriptor.InflectionType.ZENA,       ProtocolAssets.GIRAFFE)),
        GRAY_BOOK           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GRAY},                                          SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,       null)),
        KABUKI              (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.KABUKI},                                        SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,       null)),
        KIMONO              (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                              null,                             SKObjectDescriptor.InflectionType.MESTO,      null)),
        LETTERS             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.PLURAL,       null,                                                                                                              null,                             SKObjectDescriptor.InflectionType.DUB,        ProtocolAssets.IN_PACKAGES)),
        LION                (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                              null,                             SKObjectDescriptor.InflectionType.ZENA,       ProtocolAssets.LION)),
        MASK_AFRICA         (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.AFRICA},                                        SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,       null)),
        SHELLS              (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                              null,                             SKObjectDescriptor.InflectionType.ULICA,      ProtocolAssets.WITH_SHELLS)),
        SMALL_BOOK          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,       null)),
        SMALL_GLOBE         (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                         SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.STROM,      null)),
        PILE_OF_BOOKS       (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                               null,                            SKObjectDescriptor.InflectionType.ZENA,       ProtocolAssets.OF_BOOKS)),
        STAND_FOR_POSTER    (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                              null,                             SKObjectDescriptor.InflectionType.DUB,        ProtocolAssets.WITH_POSTERS)),
        TABLE_WITH_MAPS     (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                               null,                            SKObjectDescriptor.InflectionType.DUB,        ProtocolAssets.WITH_MAPS)),
        CHINESE_VASE        (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.CHINESE},                                       SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.ZENA,       null)),
        COMPASS             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     null,                                                                                                               null,                            SKObjectDescriptor.InflectionType.STROM,      null)),
        TRAVEL_HAT          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,     new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.TRAVEL},                                        SKObjectDescriptor.AdjectiveInflection.PEKNY,   SKObjectDescriptor.InflectionType.DUB,        null));


        private SKObjectDescriptor descriptor;
        ObjectDescriptor(SKObjectDescriptor descriptor){
            this.descriptor = descriptor;
        }

        public SKObjectDescriptor getDescriptor() {
            return descriptor;
        }
    }

    private enum FurnitureDescriptor {
        //MEDIUM
        MEDIUM_BIG_SHELF_LEFT           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.LEFT},    SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        MEDIUM_BIG_SHELF_RIGHT          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.RIGHT},   SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        MEDIUM_CHEST_OF_DRAWERS_LEFT    (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                            SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ZENA,  null)),
        MEDIUM_CHEST_OF_DRAWERS_RIGHT   (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                           SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ZENA,  null)),
        MEDIUM_SMALL_SHELF_LEFT         (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.LEFT},  SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        MEDIUM_SMALL_SHELF_RIGHT        (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.RIGHT}, SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        //HARD
        HARD_BIG_SHELF_LEFT             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.LEFT},    SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        HARD_BIG_SHELF_RIGHT            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.RIGHT},   SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        HARD_BIG_TABLE                  (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                             SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.DUB,   null)),
        HARD_CHEST_OF_DRAWERS_LEFT      (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                            SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ZENA,  null)),
        HARD_CHEST_OF_DRAWERS_RIGHT     (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                           SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ZENA,  null)),
        HARD_SMALL_SHELF_LEFT           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.LEFT},  SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        HARD_SMALL_SHELF_RIGHT          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.RIGHT}, SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        HARD_SMALL_TABLE                (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                           SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.DUB,   null)),
        //BONUS
        BONUS_BIG_SHELF_LEFT            (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.LEFT},    SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        BONUS_BIG_SHELF_RIGHT           (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.RIGHT},   SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        BONUS_BIG_TABLE                 (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                             SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.DUB,   null)),
        BONUS_CABINET_LEFT              (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                            SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ZENA,  null)),
        BONUS_CHEST_OF_DRAWERS_LEFT     (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                            SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ZENA,  null)),
        BONUS_CHEST_OF_DRAWERS_RIGHT    (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                           SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ZENA,  null)),
        BONUS_CABINET_RIGHT             (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                           SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ZENA,  null)),
        BONUS_ROUND_TABLE               (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ROUND},                                           SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.DUB,   null)),
        BONUS_SMALL_SHELF_LEFT          (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.LEFT},  SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        BONUS_SMALL_SHELF_RIGHT         (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.RIGHT}, SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        BONUS_SMALL_TABLE               (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                           SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.DUB,   null));

        private SKObjectDescriptor descriptor;
        FurnitureDescriptor(SKObjectDescriptor descriptor){
            this.descriptor = descriptor;
        }

        public SKObjectDescriptor getDescriptor() {
            return descriptor;
        }
    }

    private enum WallDescriptor{
        LEFT_WALL   (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT}, SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        MIDDLE_WALL (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.MIDDLE}, SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null)),
        RIGHT_WALL  (new SKObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT}, SKObjectDescriptor.AdjectiveInflection.PEKNY, SKObjectDescriptor.InflectionType.ULICA, null));

        private SKObjectDescriptor descriptor;
        WallDescriptor(SKObjectDescriptor descriptor){
            this.descriptor = descriptor;
        }

        public SKObjectDescriptor getDescriptor() {
            return descriptor;
        }
    }

    private final String END_OK_SUFFIX          = "ok";

    public SKProtocolGenerator(TablexiaRandom random, GameDifficulty difficulty, ProtocolGameState gameState) {
        super(random, difficulty,gameState);
    }


/////////////////////////////// OBJECT INFLECTION TEXT

    @Override
    public String getObjectCardText(AbstractTablexiaScreen screen, GameObjectType objectType) {
        return getObjectTextWithAdjective(screen,objectType,Preposition.NONE,getObjectDescriptorByObjectType(objectType));
    }

    @Override
    public String getObjectTextWithAdjective(AbstractTablexiaScreen screen, GameObjectType objectType, Preposition preposition, AbstractObjectDescriptor descriptor){
        SKObjectDescriptor skObjectDescriptor = (SKObjectDescriptor) descriptor;
        return skObjectDescriptor.getAdjectiveText(screen,preposition) + getObjectTextByPreposition(screen,objectType.getTextPath(),skObjectDescriptor,preposition) + descriptor.getNextDescriptionText(screen);
    }

    @Override
    public String getFurnitureTextWithAdjective(AbstractTablexiaScreen screen, FurnitureType furnitureType, Preposition preposition, AbstractObjectDescriptor descriptor) {
        SKObjectDescriptor skObjectDescriptor = (SKObjectDescriptor) descriptor;
        return skObjectDescriptor.getAdjectiveText(screen,preposition) + getObjectTextByPreposition(screen,furnitureType.getTextResource(),skObjectDescriptor,preposition);
    }

    @Override
    public String getWallTextWithAdjective(AbstractTablexiaScreen screen, AbstractObjectDescriptor descriptor, Preposition preposition) {
        SKObjectDescriptor skObjectDescriptor = (SKObjectDescriptor) descriptor;
        return skObjectDescriptor.getAdjectiveText(screen,preposition) + screen.getText(ProtocolAssets.WALL_TEXT);
    }

    private String getObjectTextByPreposition(AbstractTablexiaScreen screen, String textPath, SKObjectDescriptor descriptor, Preposition preposition){

        String objectText = screen.getText(textPath);

        CaseType caseType = descriptor.getCaseTypeByPreposition(preposition);
        if(caseType == CaseType.GENETIV && (descriptor.getVoice() == AbstractObjectDescriptor.Voice.F || descriptor.getVoice() == AbstractObjectDescriptor.Voice.N) && descriptor.getNounType() == AbstractObjectDescriptor.NounType.PLURAL){
            return screen.getText(textPath + caseType.getPathResource());
        }

        if(caseType != CaseType.NOMINATIV){

            //Check -ok ending and remove -o last character for infleciton. bubienok - pred bubienkom
            if(objectText.endsWith(END_OK_SUFFIX) && descriptor.getSyllableCount(objectText) > 1){
                objectText = objectText.substring(0, objectText.length() -2) + objectText.substring(objectText.length()-1);
            }

            //Check -ô(ň/ť) ending. Change "ô" to "o"  Kôň - před Koňom
            if(objectText.length() >= 2 && objectText.toCharArray()[objectText.length() - 2] == 'ô'){
                char[] chars = objectText.toCharArray();
                chars[objectText.length()-2] = 'o';
                objectText = String.valueOf(chars);
            }
        }


        return objectText + descriptor.getNounInflection(screen,preposition);
    }

/////////////////////////////// DESCRIPTOR

    @Override
    public SKObjectDescriptor getObjectDescriptorByObjectType(GameObjectType type){
        return ObjectDescriptor.values()[type.ordinal()].getDescriptor();
    }

    @Override
    public SKObjectDescriptor getFurnitureDescriptorByType(FurnitureType furnitureType) {
        return FurnitureDescriptor.values()[furnitureType.ordinal()].getDescriptor();
    }

    @Override
    public AbstractObjectDescriptor getWallDescriptorByType(WallType wallType) {
        return WallDescriptor.values()[wallType.ordinal()].getDescriptor();
    }
}
