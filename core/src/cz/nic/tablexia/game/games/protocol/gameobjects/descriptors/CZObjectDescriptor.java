/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.gameobjects.descriptors;

import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.ProtocolGenerator;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by lmarik on 11.5.17.
 */

public class CZObjectDescriptor extends AbstractObjectDescriptor {

    /**
     * MM - mužský mladý
     * MJ - mužský jarní
     * FM - ženský mladý
     * FJ - ženský jarní
     * NM  - střední mladý
     */
    public enum AdjectiveInflection {

        MM  ("_mm"),
        MJ  ("_mj"),
        FM  ("_fm"),
        FJ  ("_fj"),
        NM  ("_nm");

        String pathResource;
        AdjectiveInflection(String pathResource){
            this.pathResource = pathResource;
        }

        /**
         *                                 adjectiveInflection path resource
         *                                /
         * MM:  game_protocol_descriptor_mh_s_g
         *                                  \  \
         *                                  \  case type
         *                                  \
         *                                   singular
         *
         * @param screen
         * @param caseType
         * @return
         */
        public String getAdjectiveInflectionTextByPreposition(AbstractTablexiaScreen screen, CaseType caseType, NounType nounType){
            return screen.getText(STRING_DESCRIPTOR_PREFIX +pathResource+nounType.pathResource+caseType.pathResource);
        }
    }

    public enum InflectionType{
        PAN("_p"),
        MUZ("_m"),
        HRAD("_h"),

        ZENA("_z"),
        RUZE("_r"),
        KOST("_k"),

        MESTO("_m"),
        KURE("_ku");


        String pathResource;
        InflectionType(String pathResource){
            this.pathResource = pathResource;
        }
    }




    private AdjectiveInflection     adjectiveInflection;
    private InflectionType          inflectionType;


    public CZObjectDescriptor(Voice voice,NounType nounType,Adjective[] adjectives, AdjectiveInflection adjectiveInflection, InflectionType inflectionType, String nextDescriptionPath){
        super(voice,nounType,adjectives,nextDescriptionPath);
        this.adjectiveInflection = adjectiveInflection;
        this.nounType = nounType;
        this.inflectionType = inflectionType;

    }

    public String getAdjectiveTextByPreposition(AbstractTablexiaScreen screen,Preposition preposition){
        if(adjectives == null)
            return "";

        StringBuilder text = new StringBuilder();

        for(Adjective adjective : adjectives){
            text.append(adjective.isInflect() ? adjective.getAdjectiveTypeText(screen) + adjectiveInflection.getAdjectiveInflectionTextByPreposition(screen, getCaseTypeByPreposition(preposition), nounType) + " " : adjective.getAdjectiveTypeText(screen) + " ");
        }

        return text.toString();
    }

    /**
     * For pronoun : která, které, kteří ....
     *
     *
     *                                         male voice
     *                                        /
     *  M : game_protocol_descriptor_pronoun_m_l_s
     *                                         \ \
     *                                         \  plural = p / singular = s
     *                                         \
     *                                          live = l /none_live = n
     *
     *                                        female voice
     *                                       /
     *  FM: game_protocol_descriptor_pronoun_f_s
     *                                        \
     *                                        \
     *                                         plural = p / singular = s
     * @return pronoun with inflection
     */
    @Override
    public String getPronounInflection(AbstractTablexiaScreen screen){
        return screen.getText(STRING_DESCRIPTOR_PREFIX + STRING_RESOURCE_PRONOUN + getPronounSuffix());
    }


    /**
     *
     * Noun inflection suffix by preposition
     *                                            adjectiveInflection (m/f/n)
     *                                             /
     *                                            / infleciton type (pan = p,hrad = h ...)
     *                                           / /
     *  M:  game_protocol_descriptor_inflection_m_p_s_g
     *                                              \  \
     *                                              \   case type
     *                                              \
     *                                              plural = p / singular = s
     * @param screen
     * @param preposition
     * @param hasDiacritics
     * @return
     */
    public String getNounInflection(AbstractTablexiaScreen screen, Preposition preposition, boolean hasDiacritics, boolean secondInflection){
        String path = STRING_DESCRIPTOR_PREFIX + STRING_RESOURCE_INFLECTION + voice.pathResource+ inflectionType.pathResource + nounType.pathResource + getCaseTypeByPreposition(preposition).pathResource;

        if(secondInflection) path += "_2";

        String inflection = screen.getText(path);


        if(hasDiacritics && inflection.length() > 0){
            char[] chars = inflection.toCharArray();
            chars[0] = getCharWithDiacritics(chars[0]);
            inflection = String.valueOf(chars);
        }

        return inflection.equals(ProtocolGenerator.EMPTY_CHAR) ? "" : inflection;
    }


    private char getCharWithDiacritics(char t){
        switch (t){
            case 'e':
                return  'ě';
        }

        return t;
    }


    private String getPronounSuffix(){
        switch (voice){
            case F:
                return "_f" + nounType.pathResource;
            case N:
                return "_n" + nounType.pathResource;
            default:
                return "_m" + (inflectionType == InflectionType.HRAD ? "_n" : "_l") + nounType.pathResource;
        }
    }

    public NounType getNounType() {
        return nounType;
    }

    public InflectionType getInflectionType() {
        return inflectionType;
    }
}
