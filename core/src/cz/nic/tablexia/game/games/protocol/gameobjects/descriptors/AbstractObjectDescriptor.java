/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.gameobjects.descriptors;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by lmarik on 5.6.17.
 */

public abstract class AbstractObjectDescriptor {

    public abstract String getPronounInflection(AbstractTablexiaScreen screen);

    private  static  final String REGEX_SYLLABLE_COUNT        = "(?i)([bcčdďfghjklĺľmnňpqrŕřsštťvwxzžß]*[aáeěéiíoôóőuúűůyýäöü]+[bcčdďfghjklĺľmnňpqrŕřsštťvwxzžß]*)";
    protected static final String STRING_ADJECTIVE_PREFIX     = "game_protocol_adjective";
    protected static final String STRING_DESCRIPTOR_PREFIX    = "game_protocol_descriptor";
    protected static final String STRING_RESOURCE_VERB        = "_verb";
    public    static final String STRING_RESOURCE_INFLECTION  = "_inflection";
    protected static final String STRING_RESOURCE_PRONOUN     = "_pronoun";


    public enum CaseType{

        NOMINATIV       ("_n"),
        GENETIV         ("_g"),
        LOKAL           ("_l"),
        INSTRUMENTAL    ("_i");

        String pathResource;
        CaseType(String pathResource){
            this.pathResource = pathResource;
        }

        public String getPathResource() {
            return pathResource;
        }
    }

    public enum Adjective {
        BLUE        ("_blue",       true),
        COLOR       ("_color",      true),
        BIG         ("_big",        true),
        WOODEN      ("_wooden",     true),
        PAPER       ("_paper",      true),
        FOOTBALL    ("_football",   true),
        PULL        ("_pull",       true),
        RAG         ("_rag",        true),
        RED         ("_red",        true),
        ROCK        ("_rock",       true),
        SMALL       ("_small",      true),
        FOR_DOLL    ("_for_doll",   true),
        CHESS       ("_chess",      true),
        CUCKOO      ("_cuckoo",     true),
        PENDULUM    ("_pendulum",   true),
        GLASS       ("_glass",      true),
        PORCELAIN   ("_porcelain",  true),
        TEA         ("_tea",        true),
        TIN         ("_tin",        true),
        ROUND       ("_round",      true),
        BLACK       ("_black",      true),
        GRAY        ("_gray",       true),
        GREEN       ("_green",      true),
        LIGHT       ("_light",      true),
        LIGHT_OFF   ("_light_off",  true),
        OBLONG      ("_oblong",     true),
        OPEN        ("_open",       true),
        ORANGE      ("_orange",     true),
        SMOKE       ("_smoke",      true),
        WHITE       ("_white",      true),
        YELLOW      ("_yellow",     true),
        COINS       ("_coins",      true),
        LEFT        ("_left",       true),
        RIGHT       ("_right",      true),
        MIDDLE      ("_middle",     true),
        BROWN       ("_brown",      true),
        KABUKI      ("_kabuki",     false),
        AFRICA      ("_africa",     true),
        CHINESE     ("_chinese",    true),
        TRAVEL      ("_travel",     true);

        String pathResource;
        boolean inflect;

        Adjective(String pathResource, boolean inflect){
            this.pathResource = pathResource;
            this.inflect = inflect;
        }

        public String getAdjectiveTypeText(AbstractTablexiaScreen screen) {
            return screen.getText(STRING_DESCRIPTOR_PREFIX +pathResource);
        }

        public boolean isInflect() {
            return inflect;
        }
    }

    public enum Voice{
        M("_m"),  //male
        F("_f"),  //female
        N("_n");  //no-live


        String pathResource;
        Voice(String pathResource){
            this.pathResource = pathResource;
        }
    }

    public enum NounType{
        SINGULAR("_s"),
        PLURAL("_p");

        String pathResource;
        NounType(String pathResource){
            this.pathResource = pathResource;
        }
    }

    public enum Verb{
        IS  ("_is"),
        LIE ("_lie"),
        HANG("_hang");

        String pathResource;
        Verb(String pathResource){
            this.pathResource = pathResource;
        }

        public String getVerbText(AbstractTablexiaScreen screen, NounType nounType){
            return screen.getText(STRING_DESCRIPTOR_PREFIX + STRING_RESOURCE_VERB + pathResource + nounType.pathResource);
        }

        public String getPluralVerbText(AbstractTablexiaScreen screen){
            return getVerbText(screen, NounType.PLURAL);
        }

        public static Verb getVerb(int index, boolean wall){

            return index == 0 ? IS : wall ? HANG : LIE;
        }

    }


    protected Adjective[]     adjectives;
    protected NounType        nounType;
    protected Voice           voice;
    protected String          nextDescriptionPath;

    public AbstractObjectDescriptor(Voice voice,NounType nounType,Adjective[] adjectives, String nextDescriptionPath){
        this.voice = voice;
        this.nounType = nounType;
        this.adjectives = adjectives;
        this.nextDescriptionPath = nextDescriptionPath;
    }

    public String getNextDescriptionText(AbstractTablexiaScreen screen){
        return nextDescriptionPath == null ? "" : " " + screen.getText(nextDescriptionPath);
    }

    public CaseType getCaseTypeByPreposition(Preposition preposition){
        if(preposition == Preposition.NONE){
            return CaseType.NOMINATIV;
        }else if (preposition == Preposition.LEFT || preposition == Preposition.RIGHT){
            return CaseType.GENETIV;
        }else if (preposition == Preposition.ON){
            return CaseType.LOKAL;
        }else  {
            return CaseType.INSTRUMENTAL;
        }
    }

    public int getSyllableCount(String objectText){
        int count = 0;

        Pattern pattern = Pattern.compile(REGEX_SYLLABLE_COUNT);
        Matcher matcher = pattern.matcher(objectText);

        while (matcher.find()){
            count++;
        }
        return count;
    }

    public Adjective[] getAdjectives() {
        return adjectives;
    }

    public NounType getNounType() {
        return nounType;
    }

    public Voice getVoice() {
        return voice;
    }
}
