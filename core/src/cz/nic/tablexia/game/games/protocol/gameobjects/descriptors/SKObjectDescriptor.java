/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.gameobjects.descriptors;

import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.ProtocolGenerator;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by lmarik on 5.6.17.
 */

public class SKObjectDescriptor extends AbstractObjectDescriptor {

    //TODO promozat nepouzivajici
    public enum AdjectiveInflection {

        PEKNY   ("_pe"),
        CUDZI   ("_cu"),
        PAVI    ("_pa"),
        PAVI_R  ("_par"),
        OTCOV   ("_ot"),
        MATKIN  ("_ma");

        String pathResource;
        AdjectiveInflection(String pathResource){
            this.pathResource = pathResource;
        }
    }

    //TODO promozat nepouzivajici
    public enum InflectionType{
        CHLAP           ("_ch"),
        HRDINA          ("_hr"),
        DUB             ("_du"),
        STROM           ("_st"),

        ZENA            ("_ze"),
        ULICA           ("_ul"),
        DLAN            ("_dl"),
        KOST            ("_ko"),
        GAZDINA         ("_ga"),

        MESTO           ("_me"),
        SRDCE           ("_sr"),
        VYSVEDCENIE     ("_vy"),
        DIEVCA          ("_di");

        String pathResource;
        InflectionType(String pathResource){
            this.pathResource = pathResource;
        }
    }

    private AdjectiveInflection     adjectiveInflection;
    private InflectionType          inflectionType;

    public SKObjectDescriptor(Voice voice, NounType nounType, Adjective[] adjectives,AdjectiveInflection adjectiveInflection, InflectionType inflectionType,String nextDescriptionPath) {
        super(voice, nounType, adjectives,nextDescriptionPath);

        this.adjectiveInflection = adjectiveInflection;
        this.inflectionType = inflectionType;
    }


    public String getAdjectiveText(AbstractTablexiaScreen screen,Preposition preposition){
        if(adjectives == null)
            return "";

        StringBuilder text = new StringBuilder();

        for(Adjective adjective: adjectives){
            text.append(adjective.isInflect() ? adjective.getAdjectiveTypeText(screen) + getAdjectiveInflection(screen, preposition) + " " : adjective.getAdjectiveTypeText(screen) + " ");
        }

        return text.toString();
    }

    /**
     *
     * Singular:
     *                                         voice type
     *                                       /
     *                                      /  adjective inflection type
     *                                     / /
     * game_protocol_adjective_inflection_m_pe_s_g
     *                                         \ \
     *                                         \  case type
     *                                         \
     *                                          singular
     *
     *
     * Plular:
     *                                        add "_m" / "_fn", if case type is NOMINATIV
     *                                       /
     *                                      /  adjective inflection type
     *                                     /  /
     * game_protocol_adjective_inflection_?_pe_p_g
     *                                         \  \
     *                                         \   case type
     *                                         \
     *                                          plular
     *
     *
     * @param screen
     * @param preposition
     * @return
     */
    private String getAdjectiveInflection(AbstractTablexiaScreen screen, Preposition preposition){
        CaseType caseType = getCaseTypeByPreposition(preposition);

        if(nounType == NounType.SINGULAR){
            return screen.getText(STRING_ADJECTIVE_PREFIX + STRING_RESOURCE_INFLECTION + voice.pathResource + adjectiveInflection.pathResource + nounType.pathResource + caseType.pathResource);
        }

        String voiceText = "";
        if(caseType == CaseType.NOMINATIV){
            voiceText = (voice == Voice.M ? "_m" : "_fn");
        }

        return screen.getText(STRING_ADJECTIVE_PREFIX + STRING_RESOURCE_INFLECTION + voiceText + adjectiveInflection.pathResource + nounType.pathResource + caseType.pathResource);
    }

    /**
     *
     * Singular:
     *                                   voice type
     *                                  /
     * game_protocol_descriptor_pronoun_m_s
     *                                    \
     *                                     singular
     * Plular:
     *
     *                                    voice == male => add "_m"
     *                                   /
     * game_protocol_descriptor_pronoun_?_p
     *                                   \
     *                                    plular
     * @param screen
     * @return
     */
    @Override
    public String getPronounInflection(AbstractTablexiaScreen screen){
       if(nounType == NounType.SINGULAR){
           return screen.getText(STRING_DESCRIPTOR_PREFIX + STRING_RESOURCE_PRONOUN + voice.pathResource + nounType.pathResource);
       }

       return screen.getText(STRING_DESCRIPTOR_PREFIX + STRING_RESOURCE_PRONOUN + (voice == Voice.M ? "_m" : "" ) + nounType.pathResource);
    }

    /**
     *                                          voice type
     *                                        /
     *                                       /  inflection type
     *                                      / /
     * game_protocol_descriptor_inflection_m_ch_s_g
     *                                          \ \
     *                                          \  case type
     *                                          \
     *                                           noun type
     * @param screen
     * @param preposition
     * @return
     */
    public String getNounInflection(AbstractTablexiaScreen screen, Preposition preposition){
        String inflection = screen.getText(STRING_DESCRIPTOR_PREFIX + STRING_RESOURCE_INFLECTION + voice.pathResource + inflectionType.pathResource +nounType.pathResource + getCaseTypeByPreposition(preposition).pathResource);
        return inflection.equals(ProtocolGenerator.EMPTY_CHAR) ? "" : inflection;
    }

}
