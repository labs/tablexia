/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.protocol.gameobjects.furniture;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;
import cz.nic.tablexia.game.games.protocol.controller.RoomPosition;
import cz.nic.tablexia.game.games.protocol.gameobjects.TypeObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;

/**
 * Created by lmarik on 9.8.17.
 */

public enum FurnitureType implements TypeObjectDescriptor {

    //MEDIUM
    MEDIUM_BIG_SHELF_LEFT           (ProtocolAssets.BIG_SHELF_LEFT,         ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.MEDIUM, RoomPosition.WALL,   WallType.LEFT_WALL,     new float[]{0.14f,0.64f},   new float[]{0.82f,0.802f},   new float[]{0.5f,0.52f},   new Color(0x5900b3ff)),
    MEDIUM_BIG_SHELF_RIGHT          (ProtocolAssets.BIG_SHELF_RIGHT,        ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.MEDIUM, RoomPosition.WALL,   WallType.RIGHT_WALL,    new float[]{0.62f,0.66f},   new float[]{0.15f,0.76f},    new float[]{0.5f,0.52f},   new Color(0xff6600ff)),
    MEDIUM_CHEST_OF_DRAWERS_LEFT    (ProtocolAssets.CHEST_OF_DRAWERS_LEFT,  ProtocolAssets.F_CHEST_OF_DRAWERS_TEXT, GameDifficulty.MEDIUM, RoomPosition.FLOOR,  WallType.LEFT_WALL,     new float[]{0.05f,0.3f},    new float[]{0.89f,0.201f},   new float[]{0.5f,0.82f},   new Color(0x996600ff)),
    MEDIUM_CHEST_OF_DRAWERS_RIGHT   (ProtocolAssets.CHEST_OF_DRAWERS_RIGHT, ProtocolAssets.F_CHEST_OF_DRAWERS_TEXT, GameDifficulty.MEDIUM, RoomPosition.FLOOR,  WallType.RIGHT_WALL,    new float[]{0.825f,0.27f},  new float[]{0.12f,0.228f},   new float[]{0.5f,0.82f},   new Color(0x7f0000ff)),
    MEDIUM_SMALL_SHELF_LEFT         (ProtocolAssets.SMALL_SHELF_LEFT,       ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.MEDIUM, RoomPosition.WALL,   WallType.LEFT_WALL,     new float[]{0.22f,0.52f},   new float[]{0.76f,0.74f},    new float[]{0.5f,0.54f},   new Color(0x003300ff)),
    MEDIUM_SMALL_SHELF_RIGHT        (ProtocolAssets.SMALL_SHELF_RIGHT,      ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.MEDIUM, RoomPosition.WALL,   WallType.RIGHT_WALL,    new float[]{0.62f,0.55f},   new float[]{0.267f,0.598f},  new float[]{0.5f,0.52f},   new Color(0x5c5c8aff)),
    //HARD
    HARD_BIG_SHELF_LEFT             (ProtocolAssets.BIG_SHELF_LEFT,         ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.HARD,   RoomPosition.WALL,   WallType.LEFT_WALL,     new float[]{0.026f,0.578f}, new float[]{0.29f,0.064f},    new float[]{0.55f,0.56f}, new Color(0x5900b3ff)),
    HARD_BIG_SHELF_RIGHT            (ProtocolAssets.BIG_SHELF_RIGHT,        ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.HARD,   RoomPosition.WALL,   WallType.RIGHT_WALL,    new float[]{0.585f,0.694f}, new float[]{0.714f,0.123f},   new float[]{0.5f,0.55f},  new Color(0xff6600ff)),
    HARD_BIG_TABLE                  (ProtocolAssets.BIG_TABLE,              ProtocolAssets.F_TABLE_B_TEXT,          GameDifficulty.HARD,   RoomPosition.FLOOR,  null,                   new float[]{0.141f,0.118f}, new float[]{0.546f,0.05f},    new float[]{0.48f,0.75f}, new Color(0x000080ff)),
    HARD_CHEST_OF_DRAWERS_LEFT      (ProtocolAssets.CHEST_OF_DRAWERS_LEFT,  ProtocolAssets.F_CABINET_TEXT,          GameDifficulty.HARD,   RoomPosition.FLOOR,  WallType.LEFT_WALL,     new float[]{0.268f,0.481f}, new float[]{0.937f,0.204f},   new float[]{0.52f,0.83f}, new Color(0x996600ff)),
    HARD_CHEST_OF_DRAWERS_RIGHT     (ProtocolAssets.CHEST_OF_DRAWERS_RIGHT, ProtocolAssets.F_CABINET_TEXT,          GameDifficulty.HARD,   RoomPosition.FLOOR,  WallType.RIGHT_WALL,    new float[]{0.723f,0.384f}, new float[]{0.082f,0.215f},   new float[]{0.5f,0.81f},  new Color(0x7f0000ff)),
    HARD_SMALL_SHELF_LEFT           (ProtocolAssets.SMALL_SHELF_LEFT,       ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.HARD,   RoomPosition.WALL,   WallType.LEFT_WALL,     new float[]{0.047f,0.452f}, new float[]{0.327f,0.107f},   new float[]{0.52f,0.54f}, new Color(0x003300ff)),
    HARD_SMALL_SHELF_RIGHT          (ProtocolAssets.SMALL_SHELF_RIGHT,      ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.HARD,   RoomPosition.WALL,   WallType.RIGHT_WALL,    new float[]{0.841f,0.607f}, new float[]{0.667f,0.097f},   new float[]{0.54f,0.52f}, new Color(0x5c5c8aff)),
    HARD_SMALL_TABLE                (ProtocolAssets.SMALL_TABLE,            ProtocolAssets.F_TABLE_S_TEXT,          GameDifficulty.HARD,   RoomPosition.FLOOR,  null,                   new float[]{0.5f,0.483f},   new float[]{0.562f,0.065f},   new float[]{0.5f,0.8f},   new Color(0x004466ff)),
    //BONUS
    BONUS_BIG_SHELF_LEFT            (ProtocolAssets.BIG_SHELF_LEFT,         ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.BONUS,  RoomPosition.WALL,   WallType.LEFT_WALL,     new float[]{0.2106f, 0.7402f},  new float[]{0.29f,0.064f},   new float[]{0.55f,0.56f},  new Color(0x5900b3ff)),
    BONUS_BIG_SHELF_RIGHT           (ProtocolAssets.BIG_SHELF_RIGHT,        ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.BONUS,  RoomPosition.WALL,   WallType.RIGHT_WALL,    new float[]{0.5909f,0.7402f},   new float[]{0.714f,0.123f},  new float[]{0.5f,0.55f},   new Color(0xff6600ff)),
    BONUS_BIG_TABLE                 (ProtocolAssets.BIG_TABLE,              ProtocolAssets.F_TABLE_B_TEXT,          GameDifficulty.BONUS,  RoomPosition.FLOOR,  null,                   new float[]{0.5258f,0.0268f},   new float[]{0.41f, 0.1f},    new float[]{0.6f, 0.76f},  new Color(0x000080ff)),
    BONUS_CABINET_LEFT              (ProtocolAssets.CABINET_LEFT,           ProtocolAssets.F_CABINET_TEXT,          GameDifficulty.BONUS,  RoomPosition.FLOOR,  WallType.LEFT_WALL,     new float[]{0.2636f,0.4784f},   new float[]{0.94f, 0.24f},   new float[]{0.6f, 0.84f},  new Color(0x996600ff)),
    BONUS_CHEST_OF_DRAWERS_LEFT     (ProtocolAssets.CHEST_OF_DRAWERS_LEFT,  ProtocolAssets.F_CHEST_OF_DRAWERS_TEXT, GameDifficulty.BONUS,  RoomPosition.FLOOR,  WallType.LEFT_WALL,     new float[]{0.0182f, 0.2619f},  new float[]{0.97f, 0.29f},   new float[]{0.35f,0.72f},  new Color(0x009965ff)),
    BONUS_CHEST_OF_DRAWERS_RIGHT    (ProtocolAssets.CHEST_OF_DRAWERS_RIGHT, ProtocolAssets.F_CHEST_OF_DRAWERS_TEXT, GameDifficulty.BONUS,  RoomPosition.FLOOR,  WallType.RIGHT_WALL,    new float[]{0.6909f,0.3278f},   new float[]{0.034f,0.29f},   new float[]{0.48f,0.8f},   new Color(0x570061ff)),
    BONUS_CABINET_RIGHT             (ProtocolAssets.CABINET_RIGHT,          ProtocolAssets.F_CABINET_TEXT,          GameDifficulty.BONUS,  RoomPosition.FLOOR,  WallType.RIGHT_WALL,    new float[]{0.8576f,0.2887f},   new float[]{0.05f, 0.24f},   new float[]{0.5f, 0.82f},  new Color(0x7f0000ff)),
    BONUS_ROUND_TABLE               (ProtocolAssets.ROUND_TABLE,            ProtocolAssets.F_TABLE_S_TEXT,          GameDifficulty.BONUS,  RoomPosition.FLOOR,  null,                   new float[]{0.4394f,0.4845f},   new float[]{0.52f, 0.02f},   new float[]{0.42f, 0.78f}, new Color(0x212022ff)),
    BONUS_SMALL_SHELF_LEFT          (ProtocolAssets.SMALL_SHELF_LEFT,       ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.BONUS,  RoomPosition.WALL,   WallType.LEFT_WALL,     new float[]{0.1152f,0.6598f},   new float[]{0.327f,0.107f},  new float[]{0.54f,0.52f},  new Color(0x003300ff)),
    BONUS_SMALL_SHELF_RIGHT         (ProtocolAssets.SMALL_SHELF_RIGHT,      ProtocolAssets.F_SHELF_TEXT,            GameDifficulty.BONUS,  RoomPosition.WALL,   WallType.RIGHT_WALL,    new float[]{0.8379f,0.6144f},   new float[]{0.667f,0.097f},  new float[]{0.54f,0.52f},  new Color(0x5c5c8aff)),
    BONUS_SMALL_TABLE               (ProtocolAssets.SMALL_TABLE,            ProtocolAssets.F_TABLE_S_TEXT,          GameDifficulty.BONUS,  RoomPosition.FLOOR,  null,                   new float[]{0.1742f,0.2268f},   new float[]{0.5f, 0.06f},    new float[]{0.45f, 0.74f}, new Color(0x004466ff));

    private static final int MEDIUM_RANGE_MIN  = 0;
    private static final int MEDIUM_RANGE_MAX  = 6;
    private static final int HARD_RANGE_MIN    = 6;
    private static final int HARD_RANGE_MAX    = 14;
    private static final int BONUS_RANGE_MIN   = 14;
    private static final int BONUS_RANGE_MAX   = 25;


    private String          pathResource;
    private String          textResource;
    private GameDifficulty  difficulty;
    private RoomPosition    mapPosition;
    private WallType        wallOrientation;
    private float[]         positionRation;
    private float[]         middleRatio;
    private float[]         fixPositionRatio;
    private Color           colorInMap;

    FurnitureType(String pathResource, String textResource, GameDifficulty difficulty, RoomPosition mapPosition, WallType wallOrientation, float[] positionRatio, float[] middleRatio, float[] fixPositionRatio, Color colorInMap) {
        this.pathResource = pathResource;
        this.textResource = textResource;
        this.difficulty = difficulty;
        this.mapPosition = mapPosition;
        this.wallOrientation = wallOrientation;
        this.positionRation = positionRatio;
        this.middleRatio = middleRatio;
        this.fixPositionRatio = fixPositionRatio;
        this.colorInMap = colorInMap;
    }

    public Color getColorInMap() {
        return colorInMap;
    }

    public String getTextResource() {
        return textResource;
    }

    public String getPathResource() {
        return pathResource;
    }

    public float[] getPositionRation() {
        return positionRation;
    }

    public RoomPosition getMapPosition() {return  mapPosition;}

    public float[] getMiddleRatio() {
        return middleRatio;
    }

    public float[] getFixPositionRatio() {
        return fixPositionRatio;
    }

    public WallType getWallOrientation() {
        return wallOrientation;
    }

    public static List<FurnitureType> getFurnitureTypesByDifficulty(GameDifficulty difficulty){
        List<FurnitureType> types = new ArrayList<>();
        for (FurnitureType type: values()){
            if(type.difficulty == difficulty)
                types.add(type);
        }

        return types;
    }

    public static FurnitureType getFurnitureTypeByColor(Color color, GameDifficulty difficulty){
        for (FurnitureType type: values()){
            if(type.difficulty == difficulty && type.colorInMap.equals(color))
                return type;
        }

        return null;
    }

    public static WallType getFurnitureWallByColor(Color color, Vector2 objectPoint, float w, float h, GameDifficulty difficulty){
        List<FurnitureType> leftFurniture;
        List<FurnitureType> rightFurniture;

        switch (difficulty){
            case MEDIUM:
                leftFurniture = Arrays.asList(MEDIUM_BIG_SHELF_LEFT, MEDIUM_CHEST_OF_DRAWERS_LEFT, MEDIUM_SMALL_SHELF_LEFT);
                rightFurniture = Arrays.asList(MEDIUM_BIG_SHELF_RIGHT, MEDIUM_CHEST_OF_DRAWERS_RIGHT, MEDIUM_SMALL_SHELF_RIGHT);
                break;
            case BONUS:
                leftFurniture = Arrays.asList(BONUS_BIG_SHELF_LEFT, BONUS_SMALL_SHELF_LEFT);
                rightFurniture = Arrays.asList(BONUS_BIG_SHELF_RIGHT, BONUS_SMALL_SHELF_RIGHT);
                break;
            default:
                return null;
        }

        for(FurnitureType furniture: leftFurniture){
            if(furniture.colorInMap.equals(color)) {
                Vector2 wallPoint = WallType.LEFT_WALL.getEdgePointsByDifficulty(difficulty).getrTPoint(w,h);
                return wallPoint.x >= objectPoint.x ? WallType.LEFT_WALL : WallType.MIDDLE_WALL;
            }
        }

        for(FurnitureType furniture: rightFurniture){
            if(furniture.colorInMap.equals(color)) {
                Vector2 wallPoint = WallType.RIGHT_WALL.getEdgePointsByDifficulty(difficulty).getlTPoint(w,h);
                return wallPoint.x <= objectPoint.x ? WallType.RIGHT_WALL : WallType.MIDDLE_WALL;
            }
        }

        return null;
    }

    public static boolean canPutObjectUnderFurnitures(Color color, GameDifficulty difficulty){
       List<FurnitureType> floorFurniture;

       switch (difficulty){
           case HARD:
               floorFurniture = Collections.singletonList(HARD_BIG_TABLE);
               break;
           case BONUS:
               floorFurniture = Arrays.asList(BONUS_BIG_TABLE, BONUS_SMALL_TABLE);
               break;
           default:
                return false;
       }

        for(FurnitureType furnitureType: floorFurniture){
           if(furnitureType.colorInMap.equals(color))
               return true;
        }

        return false;
    }

    public static List<FurnitureType> getFurnitureByDifficulty(GameDifficulty difficulty){
        if(difficulty == GameDifficulty.EASY)
            return null;

        int min = difficulty == GameDifficulty.MEDIUM ? MEDIUM_RANGE_MIN : difficulty == GameDifficulty.HARD ? HARD_RANGE_MIN : BONUS_RANGE_MIN;
        int max = difficulty == GameDifficulty.MEDIUM ? MEDIUM_RANGE_MAX : difficulty == GameDifficulty.HARD ? HARD_RANGE_MAX : BONUS_RANGE_MAX;

        return new ArrayList<>(Arrays.asList(values())).subList(min,max);
    }

    @Override
    public int getOriginType() {
        return ordinal();
    }
}
