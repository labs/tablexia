/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.gameobjects.descriptors;

import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by lmarik on 5.6.17.
 */

public class DEObjectDescriptor extends AbstractObjectDescriptor {

    public enum InflectionType {
        WEAK    (new String[]{"n","en"}),
        STRONG  (new String[]{"s","es","n"});

        private final String    REGEX_WEAK_TEST                 = ".*(?i)[aeiouyäöü]";
        private final String    REGEX_STRONG_TEST_PLURAL        = ".*(?i)[ns]";
        private final String    REGEX_STRONG_TEST_SINGULAR      = ".*(?i)[szßx]";

        String[] verbalEndings;
        InflectionType(String[] verbalEndings){
            this.verbalEndings = verbalEndings;
        }

        public String getVerbalEndingText(String objectText,int caseInt, Voice voice, NounType nounType){
            return this == WEAK ? getWeakVerbalEnding(objectText,caseInt,voice,nounType) : getStrongVerbalEnding(objectText,caseInt,nounType);
        }

        private String getWeakVerbalEnding(String objectText,int caseInt, Voice voice, NounType nounType){
            if(caseInt == 1 || voice != Voice.M){
                return "";
            }

            //Words end up with a vowel has -n ending
            if(objectText.matches(REGEX_WEAK_TEST) || nounType == NounType.PLURAL){
                return verbalEndings[0];
            }

            // -en ending
            return verbalEndings[1];
        }

        private String getStrongVerbalEnding(String objectText,int caseInt, NounType nounType){

            //Plural object in 3.case have -n ending
            if(nounType == NounType.PLURAL){
                if(caseInt == 3 && !objectText.matches(REGEX_STRONG_TEST_PLURAL)){
                    return verbalEndings[2];
                }

                return "";
            }


            return "";
           /* if(voice == Voice.F || caseInt !=2){
                return "";
            }

           //Monoslabic and words ending with S, Z, ß or X have -es ending
            if(objectText.matches(REGEX_STRONG_TEST_SINGULAR)){
               return getSyllableCount(objectText) == 1 ? verbalEndings[1] : "";
            }

            // -e ending
            return verbalEndings[0];*/
        }

    }


    private enum  Clause {

        CM  ("_m_d"),
        CF  ("_f_d"),
        CN  ("_n_d"),
        CP  ("_p_d");

        private static final String RESOURCE_PATH   = "game_protocol_clause";

        private String definitePath;
        Clause(String definitePath){
            this.definitePath = definitePath;
        }

        public String getResourcePathByDefinite(){
            return definitePath;
        }

        public static Clause getClauseByVoice(AbstractObjectDescriptor.Voice voice){

            return values()[voice.ordinal()];
        }

        public static String getClauseText(AbstractTablexiaScreen screen, Clause clause,String casePath){
            return screen.getText(RESOURCE_PATH+clause.getResourcePathByDefinite() + casePath);
        }

    }

    private final String SPLIT_PATH_RESOURCE    = "_split";

    private InflectionType inflectionType;
    private boolean komposita;
    public DEObjectDescriptor(Voice voice, NounType nounType, Adjective[] adjectives, InflectionType inflectionType, String nextDescriptionPath, boolean komposita) {
        super(voice, nounType, adjectives,nextDescriptionPath);
        this.inflectionType = inflectionType;
        this.komposita = komposita;
    }


    public String getAdjectiveTextByPreposition(AbstractTablexiaScreen screen, Preposition preposition){
        if(adjectives == null)
            return "";

        String text = "";
        for(Adjective adjective: adjectives){
            text += adjective.getAdjectiveTypeText(screen) + getAdjectiveInflection(screen,preposition) + " ";
        }

        return text;
    }

    /**
     *                               male adjectiveInflection
     *                              /
     * M: game_protocol_descriptor_m_d_2
     *                                \ \
     *                                 \ 2. case
     *                                  \
     *                                   define clause
     *
     * @param screen
     * @param preposition
     * @return
     */
    private String getAdjectiveInflection(AbstractTablexiaScreen screen,Preposition preposition){
        Clause clause = Clause.getClauseByVoice(voice);
        return screen.getText(STRING_DESCRIPTOR_PREFIX + clause.getResourcePathByDefinite() + getCasePathResource(preposition));
    }

    private String getCasePathResource(Preposition preposition){
       return "_" + String.valueOf(getCaseByPreposition(preposition));
    }

    private int getCaseByPreposition(Preposition preposition){
        if(preposition == null || preposition == Preposition.NONE){
            return 1;
        }else {
            return 3;
        }
    }

    /**
     * Return clause by voice with inflection: der, die das
     *
     * When preposition is LEFT or RIGHT, add "from text" before clause : von der, von die.
     *      - > if voice is MALE or NOLIVE merge from with clause : von dem = vom
     *
     *
     * @param screen
     * @param preposition
     * @return
     */
    public String getClauseText(AbstractTablexiaScreen screen,Preposition preposition){
        Clause clause = (nounType == NounType.SINGULAR ? Clause.getClauseByVoice(voice) : Clause.CP);
        int cause = getCaseByPreposition(preposition);

        if(preposition == Preposition.LEFT || preposition == Preposition.RIGHT) {

            // "von dem" change to "vom"
            if (nounType != NounType.PLURAL && cause == 3 && (voice == Voice.M || voice == Voice.N)) {
                return getFromText(screen,true);
            }else {
                return getFromText(screen,false) + " " + Clause.getClauseText(screen,clause, getCasePathResource(preposition));
            }
        }


        return Clause.getClauseText(screen,clause, getCasePathResource(preposition));
    }

    private String getFromText(AbstractTablexiaScreen screen, boolean splitWithClause){
        return screen.getText(ProtocolAssets.TEXT_FROM + (splitWithClause ? SPLIT_PATH_RESOURCE : ""));
    }

    public String getVerbalEnding(String objectText,Preposition preposition){
        return inflectionType.getVerbalEndingText(objectText,getCaseByPreposition(preposition),voice, nounType);
    }

    @Override
    public String getPronounInflection(AbstractTablexiaScreen screen) {
        return getClauseText(screen,Preposition.NONE);
    }

    public boolean isKomposita() {
        return komposita;
    }
}
