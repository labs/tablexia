/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.model.gameprotocol.generators;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.protocol.assets.ProtocolAssets;
import cz.nic.tablexia.game.games.protocol.gameobjects.GameObjectType;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.AbstractObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.descriptors.DEObjectDescriptor;
import cz.nic.tablexia.game.games.protocol.gameobjects.furniture.FurnitureType;
import cz.nic.tablexia.game.games.protocol.gameobjects.wall.WallType;
import cz.nic.tablexia.game.games.protocol.model.ProtocolGameState;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.Preposition;
import cz.nic.tablexia.game.games.protocol.model.gameprotocol.ProtocolGenerator;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by lmarik on 5.6.17.
 */
public class DEProtocolGenerator extends ProtocolGenerator {

/////////////////////////////// DESCRIPTORS



    private enum DETextDescriptor implements TextDescriptor{
        TEXT_2_1(1, Collections.singletonList(
                new SentenceDescriptor(Sentence.S2_FL_S_1, new Integer[]{0, 0, 1, 0})
        )),
        TEXT_3_1(1, Collections.singletonList(
                new SentenceDescriptor(Sentence.S3_FL_C_1, new Integer[]{0, 0, 1, 1, 1, 2, 1, 0})
        )),
        TEXT_4_1(2, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FL_S_1, new Integer[]{0, 0, 1, 0}),
                new SentenceDescriptor(Sentence.A3_FL_C_1,new Integer[]{2, 2, 2, 3, 3, 3, 0, 3})
        )),
        TEXT_4_2(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FL_S_1, new Integer[]{0, 0, 1, 0}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{2, 2, 2}),
                new SentenceDescriptor(Sentence.A2_FL_S_1, new Integer[]{3, 3, 3, 0})
        )),
        TEXT_4_3(2, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_1, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A3_FF_C_1, new Integer[]{1, 1, 1, 2, 3, 3, 3})
        )),
        TEXT_4_4(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_2, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{1, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{2, 2, 2, 3, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{2, 2, 3, 2})
        )),
        TEXT_5_1(2, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_2, new Integer[]{0, 0, 0, 1, 1, 1, 2, 1}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{2, 2, 2, 3, 3, 3, 4, 3})
        )),
        TEXT_5_2(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FL_S_1, new Integer[]{0, 0, 1, 0}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{2, 2, 2}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{3, 3, 3, 4, 4, 4, 0, 4})
        )),
        TEXT_5_3(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_1, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{1, 1, 1, 2, 2, 2, 3, 2}),
                new SentenceDescriptor(Sentence.A1_FW_S_2, new Integer[]{4, 4, 4})
        )),
        TEXT_5_4(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_1, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{4, 4, 4}),
                new SentenceDescriptor(Sentence.S2_FW_C_1_A, new Integer[]{0, 1, 0, 0})
        )),
        TEXT_5_5(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_3, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{1, 1, 1, 2, 2, 2}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{3, 3, 3, 4, 4}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{3, 3, 4, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{1, 2, 1, 1})
        )),
        TEXT_6_1(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_3, new Integer[]{0, 0, 0, 1, 1, 1, 2, 1}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{3, 3, 3, 4}),
                new SentenceDescriptor(Sentence.A2_FL_S_1, new Integer[]{5, 5, 5, 2})
        )),
        TEXT_6_2(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_2, new Integer[]{0, 0, 0, 1, 1, 1, 2, 1}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{3, 3, 3}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{4, 4, 4, 5, 5, 5, 2, 5})
        )),
        TEXT_6_3(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_1, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{4, 4, 4, 5, 5, 5, 3, 5}),
                new SentenceDescriptor(Sentence.S2_FW_C_1_A, new Integer[]{0, 1, 0, 0})
        )),
        TEXT_6_4(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_2, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{1, 1, 1, 2, 2, 2, 3, 2}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{4, 4, 4, 5, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{4, 4, 5, 4})
        )),
        TEXT_6_5(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FF_C_1, new Integer[]{0, 0, 0, 1, 1, 2, 1}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{3, 3, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{4, 4, 4, 5, 5, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{4, 5, 4, 4})
        )),
        TEXT_6_6(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_3, new Integer[]{0, 0, 0, 1, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{2, 2, 2, 3, 3, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{4, 4, 4, 5, 5}),
                new SentenceDescriptor(Sentence.S2_FW_C_3_A, new Integer[]{0, 1, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{4, 4, 5, 4}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{2, 3, 2, 2})
        )),
        TEXT_7_1(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FL_S_1, new Integer[]{0, 0, 1, 0}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{2, 2, 2, 1, 1, 1, 3, 1}),
                new SentenceDescriptor(Sentence.A3_FL_C_2, new Integer[]{4, 4, 4, 5, 5, 5, 6, 5})
        )),
        TEXT_7_2(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_1, new Integer[]{0, 0, 1, 1, 1, 2, 1, 0}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{3, 3, 3}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{4, 4, 4, 5, 5, 5, 6, 5})
        )),
        TEXT_7_3(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S4_FF_C_1, new Integer[]{0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 2}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{4, 4, 4, 5, 5, 5, 3, 5}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{6, 6, 6})
        )),
        TEXT_7_4(3, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_3, new Integer[]{0, 0, 0, 1, 1, 1, 2, 1}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{3, 3, 3, 4, 4, 4}),
                new SentenceDescriptor(Sentence.A3_FF_C_1, new Integer[]{5, 5, 5, 1, 6, 6, 6}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{3, 4, 3, 3})
        )),
        TEXT_7_5(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S1_FW_S_3, new Integer[]{0, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{1, 1, 1, 2, 2, 2}),
                new SentenceDescriptor(Sentence.A3_FL_C_2, new Integer[]{3, 3, 3, 4, 4, 4, 5, 4}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{6, 6, 6}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{1, 2, 1, 1})
        )),
        TEXT_7_6(4, Arrays.asList(
               new SentenceDescriptor(Sentence.S2_FW_C_2, new Integer[]{0, 0, 0, 1, 1}),
               new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{2, 2, 2, 3}),
               new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{4, 4, 4}),
               new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{5, 5, 5, 6, 6, 6}),
               new SentenceDescriptor(Sentence.S2_FW_C_2_A, new Integer[]{0, 0, 1, 0}),
               new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{5, 6, 5, 5})
        )),
        TEXT_7_7(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_3, new Integer[]{0, 0, 0, 1, 1, 1}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{2, 2, 2}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{3, 3, 3, 4, 4, 4}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{5, 5, 5, 6, 6}),
                new SentenceDescriptor(Sentence.S2_FW_C_3_A, new Integer[]{0, 1, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{5, 5, 6, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{3, 4, 3, 3})
        )),
        TEXT_8_1(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_1, new Integer[]{0, 0, 1, 1, 1, 2, 1, 0}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{3, 3, 3, 4}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{5, 5, 5, 2, 2, 2, 6, 2}),
                new SentenceDescriptor(Sentence.A3_FL_C_2, new Integer[]{7, 7, 7, 6, 6, 6, 3, 6})
        )),
        TEXT_8_2(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FL_C_2, new Integer[]{0, 0, 0, 1, 1, 1, 2, 1}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{3, 3, 3, 4, 4, 4, 7, 4}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{5, 5, 5}),
                new SentenceDescriptor(Sentence.A3_FL_C_2, new Integer[]{6, 6, 6, 2, 2, 2, 7, 2})
        ) ),
        TEXT_8_3(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S4_FF_C_1, new Integer[]{0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 2}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{4, 4, 4, 5}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{6, 6, 6, 5, 5, 5, 1, 5}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{7, 7, 7})
        )),
        TEXT_8_4(5, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_1, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{4, 4, 4, 5, 5, 5, 6, 5}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{6, 6, 6, 3}),
                new SentenceDescriptor(Sentence.A1_FW_S_2, new Integer[]{7, 7, 7}),
                new SentenceDescriptor(Sentence.S2_FW_C_1_A, new Integer[]{0, 1, 0, 0})
        )),
        TEXT_8_5(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_3, new Integer[]{0, 0, 0, 1, 1, 1}),
                new SentenceDescriptor(Sentence.A2_FL_S_2, new Integer[]{2, 2, 2, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{4, 4, 4, 5, 5, 5}),
                new SentenceDescriptor(Sentence.A3_FL_C_2, new Integer[]{6, 6, 6, 3, 3, 3, 7, 3}),
                new SentenceDescriptor(Sentence.S2_FW_C_3_A, new Integer[]{0, 1, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{4, 5, 4, 4})
        )),
        TEXT_8_6(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_2, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A3_FL_C_1, new Integer[]{2, 2, 2, 3, 3, 3, 4, 3}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{5, 5, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{6, 6, 6, 7, 7}),
                new SentenceDescriptor(Sentence.S2_FW_C_2_A, new Integer[]{0, 0, 1, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{6, 6, 7, 6})
        )),
        TEXT_8_7(4, Arrays.asList(
                new SentenceDescriptor(Sentence.S3_FF_C_1, new Integer[]{0, 0, 0, 1, 1, 2, 1}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{3, 3, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{4, 4, 4, 5, 5, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{6, 6, 6, 7, 7}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{4, 5, 4, 4}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{6, 6, 7, 6})
        )),
        TEXT_8_8(5, Arrays.asList(
                new SentenceDescriptor(Sentence.S2_FW_C_1, new Integer[]{0, 0, 0, 1, 1}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{2, 2, 2}),
                new SentenceDescriptor(Sentence.A2_FW_C_2, new Integer[]{3, 3, 3, 4, 4, 4}),
                new SentenceDescriptor(Sentence.A1_FW_S_1, new Integer[]{5, 5, 5}),
                new SentenceDescriptor(Sentence.A2_FW_C_1, new Integer[]{6, 6, 6, 7, 7}),
                new SentenceDescriptor(Sentence.S2_FW_C_1_A, new Integer[]{0, 1, 0, 0}),
                new SentenceDescriptor(Sentence.A2_FW_C_2_A, new Integer[]{3, 4, 3, 3}),
                new SentenceDescriptor(Sentence.A2_FW_C_1_A, new Integer[]{6, 6, 7, 6})
        ));

        private int                      sentenceCnt;
        private List<SentenceDescriptor> sentenceDescriptors;

        DETextDescriptor(int sentenceCnt, List<SentenceDescriptor> sentenceDescriptors){
            this.sentenceCnt = sentenceCnt;
            this.sentenceDescriptors = sentenceDescriptors;
        }


        @Override
        public List<SentenceDescriptor> getSentenceDescriptors() {
            return sentenceDescriptors;
        }

        @Override
        public int getSentenceCnt() {
            return sentenceCnt;
        }

        @Override
        public Integer[] getIndexDescriptorBySentect(Sentence sentence) {
            for(SentenceDescriptor descriptor: sentenceDescriptors){
                if(descriptor.getSentence() == sentence)
                    return descriptor.getDescriptorIndex();
            }

            return null;
        }
    }

    private enum DESentenceDescriptor {
        //Start sentences
        S1_FW_S_1   (new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall}),
        S1_FW_S_2   (new TextType[]{TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        S1_FW_S_3   (new TextType[]{TextType.Object, TextType.Furniture_Wall, TextType.Verb}),
        S2_FL_S_1   (new TextType[]{TextType.Object, TextType.Preposition, TextType.Object, TextType.Verb}),
        S2_FW_C_1   (new TextType[]{TextType.Object, TextType.Furniture_Wall, TextType.Verb, TextType.Object, TextType.Furniture_Wall}),
        S2_FW_C_1_A (new TextType[]{TextType.Object, TextType.Object, TextType.Furniture_Wall, TextType.Verb}),
        S2_FW_C_2   (new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall, TextType.Object, TextType.Furniture_Wall}),
        S2_FW_C_2_A (new TextType[]{TextType.Verb, TextType.Object, TextType.Object, TextType.Furniture_Wall}),
        S2_FW_C_3   (new TextType[]{TextType.Object, TextType.Verb, TextType.Furniture_Wall, TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        S2_FW_C_3_A (new TextType[]{TextType.Object, TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        S3_FL_C_1   (new TextType[]{TextType.Object, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Preposition, TextType.Object, TextType.Verb, TextType.Verb}),
        S3_FL_C_2   (new TextType[]{TextType.Verb, TextType.Object, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Preposition, TextType.Object, TextType.Verb}),
        S3_FF_C_1   (new TextType[]{TextType.Object, TextType.Furniture_Wall, TextType.Verb, TextType.Object, TextType.Preposition, TextType.Object,TextType.Verb}),
        S3_FL_C_3   (new TextType[]{TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Preposition, TextType.Object, TextType.Verb}),
        S4_FF_C_1   (new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall, TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Preposition, TextType.Object, TextType.Verb}),
        //Another sentences
        A1_FW_S_1   (new TextType[]{TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        A1_FW_S_2   (new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall}),
        A2_FL_S_1   (new TextType[]{TextType.Verb, TextType.Object, TextType.Preposition, TextType.Object}),
        A2_FL_S_2   (new TextType[]{TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object}),
        A2_FW_C_1   (new TextType[]{TextType.Verb, TextType.Object, TextType.Furniture_Wall, TextType.Object, TextType.Furniture_Wall}),
        A2_FW_C_1_A (new TextType[]{TextType.Verb, TextType.Object, TextType.Object, TextType.Furniture_Wall}),
        A2_FW_C_2   (new TextType[]{TextType.Object, TextType.Verb, TextType.Furniture_Wall, TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        A2_FW_C_2_A (new TextType[]{TextType.Object, TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        A3_FL_C_1   (new TextType[]{TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Preposition, TextType.Object, TextType.Verb}),
        A3_FF_C_1   (new TextType[]{TextType.Object, TextType.Verb, TextType.Preposition, TextType.Object, TextType.Object, TextType.Verb, TextType.Furniture_Wall}),
        A3_FL_C_2   (new TextType[]{TextType.Verb, TextType.Object, TextType.Preposition, TextType.Object, TextType.Pronoun, TextType.Preposition, TextType.Object, TextType.Verb});

        private TextType[] textTypes;

        DESentenceDescriptor(TextType[] textTypes){
            this.textTypes = textTypes;
        }

        public TextType[] getTextTypes() {
            return textTypes;
        }
    }

    private enum ObjectDescriptor{
        //EASY
        ANGEL               (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        BABY_CARRIAGE       (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.FOR_DOLL},  DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        BAG_OF_MARBLES      (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        BEAR                (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        BIG_DOLL            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},       DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        BLUE_CAR            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLUE},      DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        BOWLING             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        COLOR_BALL          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.COLOR},     DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        CUBES_TOY           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        DEVIL               (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        DOG                 (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        DOLLHOUSE           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.FOR_DOLL},  DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        DRUM                (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        FOOTBALL            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.FOOTBALL},  DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        HEN                 (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        INK_STAMPS          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        NICHOLAS            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        SOLDIER             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.WEAK,       null,                 false)),
        PAPER_DRAKE         (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PAPER},     DEObjectDescriptor.InflectionType.WEAK,       null,                 true)),
        PULL_DUCK           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PULL},      DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        RAG_DOLL            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RAG},       DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        RED_CAR             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RED},       DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        ROCK_HORSE          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ROCK},      DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        ROOSTER             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        SLINGSHOT           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        SMALL_DOLL          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},     DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        TRAIN               (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        WHIPPING_TOP        (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        WHISTLE             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        WOODEN_HORSE        (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.WOODEN},    DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        //MEDIUM
        BIG_LAMP            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        BOWL                (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        BOX_WITH_JEWELERY   (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        CAMERA              (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        CHAIR               (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        CHESS_TABLE         (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.CHESS},     DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        COLLECTION_OF_COINS (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.COINS},     DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        CUCKOO_CLOCK        (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.CUCKOO},    DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        GLASS_VASE          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GLASS},     DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        GLOBUS              (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        LADY_AND_PANDA      (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     ProtocolAssets.PANDA, false)),
        LANDSCAPE           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        MIRROR              (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        PEARLS              (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        PENDULUM_CLOCK      (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PENDULUM},  DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        PORCELAIN_MUG       (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN}, DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        PORCELAIN_PLATE     (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN}, DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        PORCELAIN_POTTY     (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN}, DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        PORCELAIN_STATUETTE (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN}, DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        PORCELAIN_VASE      (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PORCELAIN}, DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        RED_KETTLE          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RED},       DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        SEAT                (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        SMALL_LAMP          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        TABLE               (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        TEA_SET             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.TEA},       DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        TELESCOPE           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        TIN_PLATE           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.TIN},       DEObjectDescriptor.InflectionType.STRONG,     null,                 true)),
        UMBRELLA            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        WATCH               (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                 false)),
        // HARD
        BIG_BOX             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                                     DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        BIG_ROUND_FLASK     (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.ROUND},           DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        BLACK_INGREDIENT    (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLACK},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        BLACK_LIQUID        (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLACK},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        BLUE_LIQUID         (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BLUE},                                                    DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        BOOK_IN_STAND       (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                                 DEObjectDescriptor.InflectionType.STRONG,     ProtocolAssets.IN_STAND, false)),
        BOOKS               (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                                                                 DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        DIARY               (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   null ,                                                                                                                                DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        GRAY_INGREDIENT     (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GRAY},                                                    DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        GREEN_LIQUID        (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GREEN},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        LIGHT_BURNER        (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LIGHT},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        LIGHT_OFF_BURNER    (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LIGHT_OFF},                                               DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        LIGHT_TUBE          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LIGHT},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        OBLONG_FLASK        (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.OBLONG},                                                  DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        OPEN_BOOK           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.OPEN},                                                    DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        ORANGE_LIQUID       (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ORANGE},                                                  DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        PAPER_BOX           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.PAPER},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    true)),
        PEN                 (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                                 DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        PILE_OF_BOXES       (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                                 DEObjectDescriptor.InflectionType.STRONG,     ProtocolAssets.OF_BOXES, false)),
        ROUND_FLASK         (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ROUND},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        SMALL_BOX           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        SMOKE_TUBE          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMOKE},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        TUBE                (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                                 DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        TUBES_IN_STAND      (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.PLURAL,     null,                                                                                                                                 DEObjectDescriptor.InflectionType.STRONG,     ProtocolAssets.IN_STAND, false)),
        WHITE_INGREDIENT    (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.WHITE},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        YELLOW_LIQUID       (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.YELLOW},                                                  DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        //BONUS
        BAG                 (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        BIG_BOOK            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                                     DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        BIG_GLOBE           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                                     DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        BROWN_BOOK          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BROWN},                                                   DEObjectDescriptor.InflectionType.STRONG,     null,                    false)),
        ELEPHANT            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,      null,                   false)),
        GIRAFFE             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,      null,                   false)),
        GRAY_BOOK           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.GRAY},                                                    DEObjectDescriptor.InflectionType.STRONG,      null,                   false)),
        KABUKI              (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,      null,                   false)),
        KIMONO              (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,      null,                   false)),
        LETTERS             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.PLURAL,       null,                                                                                                                      DEObjectDescriptor.InflectionType.STRONG,        ProtocolAssets.IN_PACKAGES,             false)),
        LION                (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,      null,                   false)),
        MASK_AFRICA         (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.AFRICA},                                                  DEObjectDescriptor.InflectionType.STRONG,      null,                   false)),
        SHELLS              (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,      ProtocolAssets.WITH_SHELLS,             false)),
        SMALL_BOOK          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                                   DEObjectDescriptor.InflectionType.STRONG,      null,                   false)),
        SMALL_GLOBE         (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                                   DEObjectDescriptor.InflectionType.STRONG,      null,                   false)),
        PILE_OF_BOOKS       (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,       ProtocolAssets.OF_BOOKS,               false)),
        STAND_FOR_POSTER    (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,       null,                  false)),
        TABLE_WITH_MAPS     (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,       ProtocolAssets.WITH_MAPS,              false)),
        CHINESE_VASE        (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.CHINESE},                                                 DEObjectDescriptor.InflectionType.STRONG,       null,                  false)),
        COMPASS             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   null,                                                                                                                        DEObjectDescriptor.InflectionType.STRONG,       null,                  false)),
        TRAVEL_HAT          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M,   AbstractObjectDescriptor.NounType.SINGULAR,   new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.TRAVEL},                                                  DEObjectDescriptor.InflectionType.STRONG,       null,                  true));

        private DEObjectDescriptor deObjectDescriptor;

        ObjectDescriptor(DEObjectDescriptor descriptor){
            this.deObjectDescriptor = descriptor;
        }

        public DEObjectDescriptor getDeObjectDescriptor() {
            return deObjectDescriptor;
        }
    }

    private enum FurnitureDescriptor {
        //MEDIUM
        MEDIUM_BIG_SHELF_LEFT           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.LEFT},    DEObjectDescriptor.InflectionType.STRONG, null, false)),
        MEDIUM_BIG_SHELF_RIGHT          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.RIGHT},   DEObjectDescriptor.InflectionType.STRONG, null, false)),
        MEDIUM_CHEST_OF_DRAWERS_LEFT    (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                            DEObjectDescriptor.InflectionType.STRONG, null, false)),
        MEDIUM_CHEST_OF_DRAWERS_RIGHT   (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                           DEObjectDescriptor.InflectionType.STRONG, null, false)),
        MEDIUM_SMALL_SHELF_LEFT         (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.LEFT},  DEObjectDescriptor.InflectionType.STRONG, null, false)),
        MEDIUM_SMALL_SHELF_RIGHT        (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.RIGHT}, DEObjectDescriptor.InflectionType.STRONG, null, false)),
        //HARD
        HARD_BIG_SHELF_LEFT             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.LEFT},    DEObjectDescriptor.InflectionType.STRONG, null, false)),
        HARD_BIG_SHELF_RIGHT            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.RIGHT},   DEObjectDescriptor.InflectionType.STRONG, null, false)),
        HARD_BIG_TABLE                  (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                             DEObjectDescriptor.InflectionType.STRONG, null, false)),
        HARD_CHEST_OF_DRAWERS_LEFT      (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                            DEObjectDescriptor.InflectionType.STRONG, null, false)),
        HARD_CHEST_OF_DRAWERS_RIGHT     (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                           DEObjectDescriptor.InflectionType.STRONG, null, false)),
        HARD_SMALL_SHELF_LEFT           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.LEFT},  DEObjectDescriptor.InflectionType.STRONG, null, false)),
        HARD_SMALL_SHELF_RIGHT          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.RIGHT}, DEObjectDescriptor.InflectionType.STRONG, null, false)),
        HARD_SMALL_TABLE                (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                           DEObjectDescriptor.InflectionType.STRONG, null, false)),
        //BONUS
        BONUS_BIG_SHELF_LEFT            (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.LEFT},    DEObjectDescriptor.InflectionType.STRONG, null, false)),
        BONUS_BIG_SHELF_RIGHT           (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG, AbstractObjectDescriptor.Adjective.RIGHT},   DEObjectDescriptor.InflectionType.STRONG, null, false)),
        BONUS_BIG_TABLE                 (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.BIG},                                             DEObjectDescriptor.InflectionType.STRONG, null, false)),
        BONUS_CABINET_LEFT              (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                            DEObjectDescriptor.InflectionType.STRONG, null, false)),
        BONUS_CHEST_OF_DRAWERS_LEFT     (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},                                            DEObjectDescriptor.InflectionType.STRONG, null, false)),
        BONUS_CHEST_OF_DRAWERS_RIGHT    (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                           DEObjectDescriptor.InflectionType.STRONG, null, false)),
        BONUS_CABINET_RIGHT             (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},                                           DEObjectDescriptor.InflectionType.STRONG, null, false)),
        BONUS_ROUND_TABLE               (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.ROUND},                                           DEObjectDescriptor.InflectionType.STRONG, null, false)),
        BONUS_SMALL_SHELF_LEFT          (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.LEFT},  DEObjectDescriptor.InflectionType.STRONG, null, false)),
        BONUS_SMALL_SHELF_RIGHT         (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.N, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL, AbstractObjectDescriptor.Adjective.RIGHT}, DEObjectDescriptor.InflectionType.STRONG, null, false)),
        BONUS_SMALL_TABLE               (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.M, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.SMALL},                                           DEObjectDescriptor.InflectionType.STRONG, null, false));


        private DEObjectDescriptor descriptor;
        FurnitureDescriptor(DEObjectDescriptor descriptor){
            this.descriptor = descriptor;
        }

        public DEObjectDescriptor getDescriptor() {
            return descriptor;
        }
    }

    private enum WallDescriptor{
        LEFT_WALL   (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.LEFT},   DEObjectDescriptor.InflectionType.STRONG, null, false)),
        MIDDLE_WALL (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.MIDDLE}, DEObjectDescriptor.InflectionType.STRONG, null, false)),
        RIGHT_WALL  (new DEObjectDescriptor(AbstractObjectDescriptor.Voice.F, AbstractObjectDescriptor.NounType.SINGULAR, new AbstractObjectDescriptor.Adjective[]{AbstractObjectDescriptor.Adjective.RIGHT},  DEObjectDescriptor.InflectionType.STRONG, null, false));

        DEObjectDescriptor descriptor;
        WallDescriptor(DEObjectDescriptor descriptor){
            this.descriptor = descriptor;
        }

        public DEObjectDescriptor getDescriptor() {
            return descriptor;
        }
    }

    public DEProtocolGenerator(TablexiaRandom random, GameDifficulty difficulty, ProtocolGameState gameState) {
        super(random, difficulty,gameState);
    }

/////////////////////////////// OBJECT INFLECTION TEXT

    @Override
    public String getObjectCardText(AbstractTablexiaScreen screen, GameObjectType objectType) {
        DEObjectDescriptor descriptor = getObjectDescriptorByObjectType(objectType);
        return descriptor.getClauseText(screen,Preposition.NONE) + " " + getInflectionObjectTextWithAdjective(screen,objectType.getTextPath(),descriptor,Preposition.NONE);
    }

    @Override
    public String getFurnitureTextWithAdjective(AbstractTablexiaScreen screen, FurnitureType furnitureType, Preposition preposition, AbstractObjectDescriptor descriptor) {
        DEObjectDescriptor deObjectDescriptor = (DEObjectDescriptor) descriptor;
        return deObjectDescriptor.getClauseText(screen, preposition) + " " + getInflectionObjectTextWithAdjective(screen, furnitureType.getTextResource(), deObjectDescriptor, preposition);
    }

    @Override
    public String getWallTextWithAdjective(AbstractTablexiaScreen screen, AbstractObjectDescriptor descriptor, Preposition preposition) {
        DEObjectDescriptor deObjectDescriptor = (DEObjectDescriptor) descriptor;
        return deObjectDescriptor.getClauseText(screen,preposition) + " " +deObjectDescriptor.getAdjectiveTextByPreposition(screen, preposition) + screen.getText(ProtocolAssets.WALL_TEXT);
    }

    @Override
    protected String getWallPreposition(AbstractTablexiaScreen screen) {
        return screen.getText(Preposition.ON.getResourcePath() + "_2");
    }

    @Override
    public String getObjectTextWithAdjective(AbstractTablexiaScreen screen, GameObjectType objectType, Preposition preposition, AbstractObjectDescriptor descriptor) {
        DEObjectDescriptor deObjectDescriptor = (DEObjectDescriptor) descriptor;
        return ((DEObjectDescriptor) descriptor).getClauseText(screen,preposition) + " " +getInflectionObjectTextWithAdjective(screen,objectType.getTextPath(),deObjectDescriptor,preposition);
    }

    private String getInflectionObjectTextWithAdjective(AbstractTablexiaScreen screen, String textPath, DEObjectDescriptor descriptor, Preposition preposition){
        String objectText = screen.getText(textPath);
        String verbalEnd =  descriptor.getVerbalEnding(objectText,preposition);

        if(descriptor.isKomposita()){
             return getKompositaObjectText(screen,objectText,descriptor) + verbalEnd + descriptor.getNextDescriptionText(screen);
        }

        return descriptor.getAdjectiveTextByPreposition(screen,preposition) + objectText + verbalEnd + descriptor.getNextDescriptionText(screen);
    }

    /**
     * Create german komposita -  Ajdective: fuß Object: Ball = Fußball
     * @param screen
     * @param objectText
     * @param descriptor
     * @return
     */
    private String getKompositaObjectText(AbstractTablexiaScreen screen, String objectText, DEObjectDescriptor descriptor){
        String adjectiveText = descriptor.getAdjectives()[0].getAdjectiveTypeText(screen);
        adjectiveText = adjectiveText.substring(0,1).toUpperCase() + adjectiveText.substring(1);

        objectText = objectText.substring(0,1).toLowerCase() + objectText.substring(1);

        return adjectiveText + objectText;
    }

/////////////////////////////// DESCRIPTOR


    @Override
    protected TextDescriptor getTextDescriptor(ProtocolText protocolText) {
        return DETextDescriptor.values()[protocolText.ordinal()];
    }

    @Override
    public DEObjectDescriptor getObjectDescriptorByObjectType(GameObjectType type){
        return ObjectDescriptor.values()[type.ordinal()].getDeObjectDescriptor();
    }

    @Override
    public DEObjectDescriptor getFurnitureDescriptorByType(FurnitureType furnitureType) {
        return FurnitureDescriptor.values()[furnitureType.ordinal()].getDescriptor();
    }

    @Override
    public AbstractObjectDescriptor getWallDescriptorByType(WallType wallType) {
        return WallDescriptor.values()[wallType.ordinal()].getDescriptor();
    }

    @Override
    protected TextType[] getTextTypesBySentence(Sentence sentence) {
        return DESentenceDescriptor.values()[sentence.ordinal()].getTextTypes();
    }
}
