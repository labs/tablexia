/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.protocol.model.gameprotocol;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by lmarik on 10.5.17.
 */

public enum Preposition {

    NONE    (null),
    BEHIND  ("_behind"),
    FRONT   ("_front"),
    RIGHT   ("_right"),
    LEFT    ("_left"),
    BETWEEN ("_between"),
    ON      ("_on");


    private static final String RESOURCE_PATH = "game_protocol_preposition";

    private String textPath;
    Preposition(String textPath){
        this.textPath = textPath;
    }

    public String getPrepositionText(AbstractTablexiaScreen screen){
        return textPath == null ? "" : screen.getText(getResourcePath());
    }

    public String getResourcePath(){
        return RESOURCE_PATH + textPath;
    }

    /**
     * Return preposition for description position between two objects
     * @param random -tablexia random
     * @param all   - with between pre
     * @return - preposition
     */
    public static Preposition getRandomPreposition(TablexiaRandom random, boolean all){
        int it = all ? 2 : 3;

        int index = random.nextInt(values().length - it) + 1;
        return values()[index];
    }

}

