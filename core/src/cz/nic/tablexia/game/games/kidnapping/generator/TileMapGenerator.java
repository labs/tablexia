/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.generator;


import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import cz.nic.tablexia.util.ui.tilemapgenerator.Position;

/**
 * @author lhoracek
 *
 */
public class TileMapGenerator {

    Random random;

    public TileMapGenerator(Random random) {
       this.random = random;
    }


    public Map<Position, cz.nic.tablexia.game.games.kidnapping.media.assets.TileType> generate(int width, int height) {
        boolean[][] freePositions = new boolean[width][height];

        // oznacime vsechna mista za prazdna
        setAllPositionFree(freePositions, width, height);

        // objekty budou razene v mape podle pozice - po radcich
        Map<Position, cz.nic.tablexia.game.games.kidnapping.media.assets.TileType> tiles = new TreeMap<Position, cz.nic.tablexia.game.games.kidnapping.media.assets.TileType>();
        // zacnemem generovat dlazdice
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {

                Position position = new Position(x, y);
                // pokud mame dlazdici prazdnou
                if (freePositions[x][y]) {
                    cz.nic.tablexia.game.games.kidnapping.media.assets.TileType tile = cz.nic.tablexia.game.games.kidnapping.media.assets.TileType.getRandomTile(random);
                    // zkontrolovat, jestli se tile vejde
                    while (!fits(position, tile.getSwSize(), tile.getSwSize(), freePositions)) {
                        tile = cz.nic.tablexia.game.games.kidnapping.media.assets.TileType.getRandomTile(random);
                    }

                    // oznacit pozice za obsazene podle velikosti dlazdice
                    markOccupiend(position, tile.getSwSize(), tile.getSwSize(), freePositions);

                    // pridat dlazdici do vysledne mapy na danou pozici
                    tiles.put(position, tile);
                }
            }
        }
        return tiles;
    }

    /**
     * Set all possible position available to use
     * @param freePositions
     * @param width
     * @param height
     */
    
    private void setAllPositionFree(boolean[][] freePositions, int width, int height) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                freePositions[x][y] = true;
            }
        }
    }
    
    /**
     * Zkontroluje, ze dana dlazdice se vejde do seznamu pozic pri sve velikosti
     *
     * @param position
     * @param freePositions
     * @return
     */
    private boolean fits(Position position, int tileSwSize, int tileSeSize, boolean[][] freePositions) {
        Position swPosition = position;
        for (int sw = 0; sw < tileSwSize; sw++) {
            Position sePosition = swPosition;
            for (int se = 0; se < tileSeSize; se++) {
                // check AIOOB
                if ((sePosition.getX() >= freePositions.length) || (sePosition.getX() < 0) || (sePosition.getY() >= freePositions[0].length) || (sePosition.getY() < 0)) {
                    return false;
                }
                if (!freePositions[sePosition.getX()][sePosition.getY()]) {
                    return false;
                }
                sePosition = sePosition.getSouthEast();
            }
            swPosition = swPosition.getSouthWest();
        }
        return true;
    }

    /**
     * Oznaci prislusne pole za obsazene, aby nebylo mozne tam stavet dalsi dlazdice
     *
     * @param position
     * @param freePositions
     * @return
     */
    private void markOccupiend(Position position, int tileSwSize, int tileSeSize, boolean[][] freePositions) {
        Position swPosition = position;
        for (int sw = 0; sw < tileSwSize; sw++) {
            Position sePosition = swPosition;
            for (int se = 0; se < tileSeSize; se++) {
                freePositions[sePosition.getX()][sePosition.getY()] = false;
                // Log.d(TAG, "Marked occupied " + sePosition + " by " + tile);
                sePosition = sePosition.getSouthEast();
            }
            swPosition = swPosition.getSouthWest();
        }
    }
}
