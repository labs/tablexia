/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.media.assets;

import cz.nic.tablexia.game.common.media.AssetDescription;

/**
 * @author lhoracek
 */
public enum SoundType implements AssetDescription {

    SOUND_CORRECT("correct", SoundClass.SOUND, true),
    SOUND_ERROR("error", SoundClass.SOUND, true);


    public enum SoundClass {
        SOUND, MUSIC;
    }

    private SoundType(String resource, SoundClass soundClass, boolean eager) {
        this.resource = resource;
    }

    private final String resource;

    public String getResource() {
        return resource;
    }
}
