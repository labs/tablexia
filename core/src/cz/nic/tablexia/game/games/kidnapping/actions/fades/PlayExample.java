/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actions.fades;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import cz.nic.tablexia.util.ui.PlayMusicAction;

/**
 * Created by lhoracek on 5/25/15.
 */
public class PlayExample extends SequenceAction {

    public PlayExample(Music music) {
        addAction(EarAlphaAction.fadeIn());
        addAction(Actions.delay(0.5f));
        addAction(PlayMusicAction.getInstance(music));
        addAction(Actions.delay(0.5f));
        addAction(EarAlphaAction.fadeOut());
        addAction(Actions.delay(0.5f));
    }
}
