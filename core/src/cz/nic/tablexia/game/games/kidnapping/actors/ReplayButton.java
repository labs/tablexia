/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actors;


import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.games.kidnapping.KidnappingGame;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.util.ui.button.GameImageTablexiaButton;

/**
 * Created by lhoracek on 5/22/15.
 */
public class ReplayButton extends GameImageTablexiaButton {

    private static final String REPLAY_BUTTON_TEXT_KEY 	= "game_kidnapping_replay";
    private static final int    WIDTH 					= 220;
    private static final int    HEIGHT 					= 80;

	public ReplayButton(KidnappingGame screen) {
        super(screen.getText(REPLAY_BUTTON_TEXT_KEY), new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_REPEAT_ICON)));
        setButtonSize(WIDTH, HEIGHT);
    }
}

