/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by lhoracek on 5/19/15.
 */
public class EarOverlay extends Group {

    private static final ApplicationFontManager.FontType        FONT_TYPE   = ApplicationFontManager.FontType.REGULAR_30;
    private static final Color                                  FONT_COLOR  = Color.WHITE;

    private Image ear, overlay;
    private TablexiaLabel text;

    public EarOverlay(String text, Texture overlayTexture, TextureRegion earTexture) {
        this.addActor(overlay = new Image(overlayTexture));
        this.addActor(ear = new Image(earTexture));
        this.addActor(this.text = new TablexiaLabel(text, new TablexiaLabel.TablexiaLabelStyle(FONT_TYPE, FONT_COLOR)));
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        overlay.setSize(getWidth(), getHeight());
        ear.setPosition(getWidth() / 2 - ear.getWidth() / 2, getHeight() / 2 - ear.getHeight() / 2);
        text.setPosition(getWidth() / 2 - text.getWidth() / 2, getHeight() / 5 - text.getHeight() / 2);
    }

    public Label getText() {
        return text;
    }
}
