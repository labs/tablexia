/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actions;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;

import cz.nic.tablexia.game.games.kidnapping.actors.KidnappingTile;
import cz.nic.tablexia.util.ui.tilemapgenerator.Map;

/**
 * Created by lhoracek on 5/20/15.
 */
public class CenterTileAction extends MoveToAction {
    private KidnappingTile tile;

    public KidnappingTile getTile() {
        return tile;
    }

    public void setTile(KidnappingTile tile) {
        this.tile = tile;
    }

    @Override
    public void setActor(Actor actor) {
        super.setActor(actor);
        if (actor != null) {
            if (!(actor instanceof Map)) {
                throw new IllegalArgumentException("Needs Tile actor instead " + actor.getClass().getName());
            }
            Map map = (Map) actor;
            float x = -tile.getCenterX() * map.getMapScale() + map.getWidth() / 2;
            float y = -tile.getCenterY() * map.getMapScale() + map.getHeight() / 2;
            setPosition(x, y);
            setTarget(map.getTiles());
        }
    }

    public static CenterTileAction getInstance(KidnappingTile tile) {
     return getInstance(tile, 0);
    }

    public static CenterTileAction getInstance(KidnappingTile tile, float duration) {
        CenterTileAction action = Actions.action(CenterTileAction.class);
        action.setDuration(duration);
        action.setTile(tile);
        return action;
    }
}
