/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.HashMap;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.common.ui.health_bar.HealthBar;
import cz.nic.tablexia.game.games.kidnapping.KidnappingGame;
import cz.nic.tablexia.game.games.kidnapping.Properties;
import cz.nic.tablexia.game.games.kidnapping.media.assets.KidnappingTextAssets;
import cz.nic.tablexia.game.games.kidnapping.media.assets.TextureTypes;
import cz.nic.tablexia.game.games.kidnapping.model.Direction;
import cz.nic.tablexia.game.games.kidnapping.model.GameState;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.ui.tilemapgenerator.ParentCullingGroup;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;

/**
 * Created by danilov on 20.4.16.
 */
public class KidnappingMap extends cz.nic.tablexia.util.ui.tilemapgenerator.Map {
    private static final int LIFE_OFFSET    = 10;
    private static final int HEALTH_COUNT = 2;

    private java.util.Map<Position, KidnappingTile> tileMap = new HashMap<Position, KidnappingTile>();
    private EarOverlay   overlay;
    private ReplayButton replayButton;
    private Image correct, wrong;
    private HealthBar healthBar;
    private TextureRegion flagTextureRegion, pathNorthEastTextureRegion, pathNorthWestTextureRegion;

    public KidnappingMap(KidnappingGame screen, GfxLibrary library) {
        super(new ParentCullingGroup(Properties.TILE_BASE_WIDTH, Properties.TILE_BASE_HEIGHT));
        addActor(replayButton = new ReplayButton(screen));
        addActor(healthBar = new HealthBar(screen.getGameGlobalTextureRegion(AbstractTablexiaGame.HEART_FULL),
                screen.getGameGlobalTextureRegion(AbstractTablexiaGame.HEART_BROKEN),
                screen.getColorTextureRegion(HealthBar.BACKGROUND_COLOR),
                HEALTH_COUNT));

        replayButton.setVisible(false);
        addActor(overlay = new EarOverlay(screen.getText(KidnappingTextAssets.INSTRUCTIONS_PAY_ATTENTION), ApplicationAtlasManager.getInstance().getColorTexture(Color.BLACK), screen.getScreenTextureRegion(screen.getGfxLibrary().get(TextureTypes.EAR))));
        addActor(correct = new Image(library.getTextureRegion(TextureTypes.CORRECT)));
        addActor(wrong = new Image(library.getTextureRegion(TextureTypes.WRONG)));
        flagTextureRegion = library.getTextureRegion(TextureTypes.POSITION_CURRENT);
        pathNorthEastTextureRegion = library.getTextureRegion(TextureTypes.POSITION_LINE_NESW);
        pathNorthWestTextureRegion = library.getTextureRegion(TextureTypes.POSITION_LINE_NWSE);
        wrong.setVisible(false);
        correct.setVisible(false);
    }

    public KidnappingTile getTile(Position position) {
        return tileMap.get(position);
    }

    public EarOverlay getOverlay() {
        return overlay;
    }

    public Image getCorrect() {
        return correct;
    }

    public Image getWrong() {
        return wrong;
    }

    public ReplayButton getReplayButton() {
        return replayButton;
    }


    public void addTile(Position position, KidnappingTile tile) {
        int baseX = getBaseX(position);
        int baseY = getBaseY(position) + (tile.getTileType().getYOffset() / 2);
        tile.setPosition(baseX, baseY);
        tileMap.put(position, tile);
        tiles.addActor(tile);
    }

    private static int getBaseX(Position position) {
        return (position.getX() * (Properties.TILE_BASE_WIDTH + Properties.ROAD_WIDTH)) / 2;
    }

    private static int getBaseY(Position position) {
        return -1 * ((position.getY() * (Properties.TILE_BASE_HEIGHT + Properties.ROAD_HEIGHT)) - ((position.getX() % 2) * ((Properties.TILE_BASE_HEIGHT + Properties.ROAD_HEIGHT) / 2)));
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        overlay.setSize(getWidth(), getHeight());
        replayButton.setPosition(getWidth() - (replayButton.getWidth() + 50), 20);

        healthBar.setPosition(getWidth() - healthBar.getWidth() - LIFE_OFFSET, getHeight() - healthBar.getHeight() - LIFE_OFFSET);

        correct.setSize(getWidth() / 4, ScaleUtil.getHeight(correct.getWidth(), correct.getHeight(), getWidth() / 4));
        wrong.setSize(getWidth() / 4, ScaleUtil.getHeight(wrong.getWidth(), wrong.getHeight(), getWidth() / 4));
        correct.setPosition(getWidth() / 2 - correct.getWidth() / 2, getHeight() / 2 - correct.getHeight() / 2);
        wrong.setPosition(getWidth() / 2 - wrong.getWidth() / 2, getHeight() / 2 - wrong.getHeight() / 2);
    }

    public void draw(Batch batch, float parentAlpha) {
        clipBegin();
        super.draw(batch, parentAlpha);
        clipEnd();
    }

    public void setMapScale(float scaleXY) {
        tiles.setScale(scaleXY);
    }

    public float getMapScale() {
        if (tiles.getScaleY() != tiles.getScaleX()) {
            throw new IllegalStateException("Different scale");
        }
        return tiles.getScaleX();
    }

    public Group getTiles() {
        return tiles;
    }

    public Arrow getDirectionArrows(Position position, Direction direction, GfxLibrary gfxLibrary) {
        KidnappingTile tile = getTile(position);
        KidnappingTile actualTile;
        switch (direction) {
            case NW:
                if(tile.northWestOut != null) return tile.northWestOut;

                TextureRegion arrowNW = gfxLibrary.getTextureRegion(TextureTypes.POSITION_NEXT_NW);
                TextureRegion arrowNWPressed = gfxLibrary.getTextureRegion(TextureTypes.POSITION_NEXT_NW_PRESSED);
                tile.addActor(tile.northWestOut = new Arrow(arrowNW, arrowNWPressed));
                tile.northWestOut.setSize(tile.northWestOut.getWidth()/2f, tile.northWestOut.getHeight()/2f);
                tile.northWestOut.setPosition(KidnappingTile.ARROW_WEST_OUT_X, KidnappingTile.ARROW_WEST_OUT_Y);
                tile.northWestOut.setVisible(false);
                return tile.northWestOut;
            case NE:
                if(tile.northEastOut != null) return tile.northEastOut;

                TextureRegion arrowNE = gfxLibrary.getTextureRegion(TextureTypes.POSITION_NEXT_NE);
                TextureRegion arrowNEPressed = gfxLibrary.getTextureRegion(TextureTypes.POSITION_NEXT_NE_PRESSED);
                tile.addActor(tile.northEastOut = new Arrow(arrowNE, arrowNEPressed));
                tile.northEastOut.setSize(tile.northEastOut.getWidth()/2f, tile.northEastOut.getHeight()/2f);
                tile.northEastOut.setPosition(KidnappingTile.ARROW_EAST_OUT_X, KidnappingTile.ARROW_EAST_OUT_Y);
                tile.northEastOut.setVisible(false);
                return tile.northEastOut;
            case SE:
                actualTile = getTile(position.getSouthEast());

                if(actualTile.northWestIn != null) return actualTile.northWestIn;

                TextureRegion arrowSE = gfxLibrary.getTextureRegion(TextureTypes.POSITION_NEXT_SE);
                TextureRegion arrowSEPressed = gfxLibrary.getTextureRegion(TextureTypes.POSITION_NEXT_SE_PRESSED);
                actualTile.addActor(actualTile.northWestIn = new Arrow(arrowSE, arrowSEPressed));
                actualTile.northWestIn.setSize(actualTile.northWestIn.getWidth()/2f, actualTile.northWestIn.getHeight()/2f);
                actualTile.northWestIn.setPosition(KidnappingTile.ARROW_WEST_IN_X, KidnappingTile.ARROW_WEST_IN_Y);
                actualTile.northWestIn.setVisible(false);
                return actualTile.northWestIn;
            case SW:
                actualTile = getTile(position.getSouthWest());

                if(actualTile.northEastIn != null) return actualTile.northEastIn;

                TextureRegion arrowSW = gfxLibrary.getTextureRegion(TextureTypes.POSITION_NEXT_SW);
                TextureRegion arrowSWPressed = gfxLibrary.getTextureRegion(TextureTypes.POSITION_NEXT_SW_PRESSED);
                actualTile.addActor(actualTile.northEastIn = new Arrow(arrowSW, arrowSWPressed));
                actualTile.northEastIn.setSize(actualTile.northEastIn.getWidth()/2f, actualTile.northEastIn.getHeight()/2f);
                actualTile.northEastIn.setPosition(KidnappingTile.ARROW_EAST_IN_X, KidnappingTile.ARROW_EAST_IN_Y);
                actualTile.northEastIn.setVisible(false);
                return actualTile.northEastIn;
        }

        //Something went wrong
        Log.info(getClass(), "Tried to get direction arrow for unknown direction!");
        return null;
    }

    public Arrow[] getDirectionArrows(Position position, Direction[] directions, GfxLibrary gfxLibrary) {
        Arrow[] arrows = new Arrow[directions.length];
        for (int i = 0; i < directions.length; i++) {
            arrows[i] = getDirectionArrows(position, directions[i], gfxLibrary);
        }
        return arrows;
    }
    
    public void addCurrentPositionFlag(Position position){
        KidnappingTile tile = getTile(position);
        Image flag = new Image(flagTextureRegion);
        tile.addActor(tile.flag = flag);
        flag.setPosition(KidnappingTile.PATH_FLAG_X, KidnappingTile.PATH_FLAG_Y);
        flag.setVisible(false);
    }
    
    public Actor addRoad(Position position, Direction direction){
        KidnappingTile tile = getTile(position);
        Image road;
        if (direction.equals(Direction.NE)){
            road = new Image(pathNorthEastTextureRegion);
            tile.pathNorthEast = road; 
        }else {
            road = new Image(pathNorthWestTextureRegion);
            tile.pathNorthWest = road;
        }
        
        road.setVisible(false);
        road.setPosition(direction.getX(),direction.getY());
        tile.addActor(road);
        return road;
    }

    public void hideLife() {
        healthBar.hide();
    }

    public static class KidnappingMapFactory {
        public static KidnappingMap createInstance(KidnappingGame game, GfxLibrary gfxLibrary, GameState gameState) {
            KidnappingMap map = new KidnappingMap(game, gfxLibrary);
            for (Position position : gameState.getMap().keySet()) {
                map.addTile(position, KidnappingTile.TileFactory.createInstance(gfxLibrary, gameState.getMap().get(position)));
            }
            map.getOverlay().setVisible(false);
            return map;
        }
    }
}
