/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actions.fades;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;

import cz.nic.tablexia.game.games.kidnapping.actors.Arrow;
import cz.nic.tablexia.util.ui.tilemapgenerator.Map;

/**
 * Created by lhoracek on 5/25/15.
 */
public class ShowArrowAction extends SubActorFadeAction<Map> {
    private Arrow arrow;

    public Arrow getArrow() {
        return arrow;
    }

    public void setArrow(Arrow arrow) {
        this.arrow = arrow;
    }

    @Override
    public Actor getTargetActor(Map map) {
        return arrow;
    }

    public static ShowArrowAction fadeIn(Arrow arrow) {
        ShowArrowAction sfa = getFade(ShowArrowAction.class, DEFAULT_DURATION, 1);
        sfa.setArrow(arrow);
        arrow.toFront();
        return sfa;
    }

    public static ShowArrowAction fadeOut(Arrow arrow) {
        ShowArrowAction sfa = getFade(ShowArrowAction.class, DEFAULT_DURATION, 0);
        sfa.setArrow(arrow);
        return sfa;
    }

    public static Action arrowsFadeIn(Arrow[] arrows) {
        ParallelAction pa = Actions.parallel();
        for (Arrow a : arrows) {
            pa.addAction(ShowArrowAction.fadeIn(a));
        }
        return pa;
    }

    public static Action arrowsFadeOut(Arrow[] arrows) {
        ParallelAction pa = Actions.parallel();
        for (Arrow a : arrows) {
            pa.addAction(ShowArrowAction.fadeOut(a));
        }
        return pa;
    }
}
