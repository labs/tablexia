/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.kidnapping.generator.PathGenerator;
import cz.nic.tablexia.game.games.kidnapping.generator.SoundPathGenerator;
import cz.nic.tablexia.shared.model.resolvers.KidnappingScoreResolver;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;
import cz.nic.tablexia.game.games.kidnapping.generator.TileMapGenerator;
import cz.nic.tablexia.game.games.kidnapping.media.DirectionSounds;
import cz.nic.tablexia.game.games.kidnapping.media.assets.TileType;
import cz.nic.tablexia.game.games.kidnapping.util.DirectionsHelper;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.shared.model.Game;

/**
 * Created by lhoracek on 5/15/15.
 */
public class GameState {
    public static final String GAME_MISSES          = KidnappingScoreResolver.GAME_MISSES;
    public static final String GAME_REPLAYS         = KidnappingScoreResolver.GAME_REPLAYS;
    public static final String GAME_STEP            = KidnappingScoreResolver.GAME_STEP;
    public static final String GAME_ROUNDS_CORRECT  = KidnappingScoreResolver.GAME_ROUNDS_CORRECT;


    private final Map<Position, TileType>         map;
    private final List<Position>                  path;
    private final List<DirectionSounds.SoundPack> sounds;
    private final Direction                       startDirection, endDirection;
    private Game game;

    private GameState(Game game, Map<Position, TileType> map, List<Position> path, List<DirectionSounds.SoundPack> sounds, Direction startDirection, Direction endDirection) {
        this.map = map;
        this.path = path;
        this.sounds = sounds;
        this.startDirection = startDirection;
        this.endDirection = endDirection;
        this.game = game;
    }

    public Map<Position, TileType> getMap() {
        return map;
    }

    public List<Position> getPath() {
        return path;
    }

    Integer step = null; // needs caching because it saves asynchronously
    public int getStep() {
        if(step != null){
            return step;
        }
        return step = Integer.parseInt(game.getGameScore(GAME_STEP, "0"));
    }

    public void setStep(int step) {
        if (game != null) {
            GameDAO.setGameScore(game, GAME_STEP, String.valueOf(step));
        }
        this.step = step;
    }

    public Position getCurrentPosition() {
        return path.get(getStep());
    }

    public Position getLastPosition() {
        return path.get(getStep() - 1);
    }

    public void addReplay() {
        GameDAO.setGameScore(game, GAME_REPLAYS, String.valueOf(getReplays() + 1));
    }

    public void addMiss() {
        GameDAO.setGameScore(game, GAME_MISSES, String.valueOf(getMisses() + 1));
    }

	public void resetMisses() {
		GameDAO.setGameScore(game, GAME_MISSES, String.valueOf(0));
	}

	public void resetReplays() {
		GameDAO.setGameScore(game, GAME_REPLAYS, String.valueOf(0));
	}

    public void resetRoundsCorrect() {
        GameDAO.setGameScore(game, GAME_ROUNDS_CORRECT, String.valueOf(0));
    }

    public void addedCorrectRound() {
        GameDAO.setGameScore(game, GAME_ROUNDS_CORRECT, String.valueOf(getCorrectRounds() + 1));
    }

    public int getMisses() {
        return Integer.parseInt(game.getGameScore(GAME_MISSES, "0"));
    }

    public int getReplays() {
        return Integer.parseInt(game.getGameScore(GAME_REPLAYS, "0"));
    }

    public int getCorrectRounds() {
        return Integer.parseInt(game.getGameScore(GAME_ROUNDS_CORRECT, "0"));
    }

    public List<DirectionSounds.SoundPack> getSounds() {
        return sounds;
    }

    @Override
    public String toString() {
        List<Direction> nextDirs = new ArrayList<Direction>();
        List<Direction> lastDirs = new ArrayList<Direction>();
        for (int i = 0; i < path.size(); i++) {
            nextDirs.add(DirectionsHelper.getNextDirection(path, i));
            lastDirs.add(DirectionsHelper.getLastDirectionFrom(path, i));
        }
        return "Kidnapping[" + map.size() + " tiles][" + path.size() + " steps][" + sounds.size() + " soundPacks][path: " + Arrays.toString(path.toArray()) + "]\n" +
                "[dirsNext: " + Arrays.toString(nextDirs.toArray()) + "]" + "[dirsLast: " + Arrays.toString(lastDirs.toArray()) + "][startDir:" + startDirection.name() + " endDir:" + endDirection.name() + "]";
    }

    public Direction getNextDirection() {
        return (getStep() == getPath().size() - 1) ? endDirection : DirectionsHelper.getNextDirection(path, getStep());
    }

    public Direction getLastDirectionFrom() {
        return getStep() == 0 ? startDirection : DirectionsHelper.getLastDirectionFrom(path, getStep());
    }

    public static class GameStateFactory {
        public static GameState createInstance(Game game, Random random, GameDifficulty gameDifficulty, Locale locale, int width, int height, int stops, int startX, int startY) {
            TileMapGenerator mapGenerator = new TileMapGenerator(random);
            PathGenerator pathGenerator = new PathGenerator(random);
            SoundPathGenerator soundPathGenerator = new SoundPathGenerator(random, gameDifficulty);
            Map<Position, TileType> map = mapGenerator.generate(width, height);
            List<Position> path = pathGenerator.generatePath(new Position(startX, startY), stops, map);
            List<DirectionSounds.SoundPack> sounds = soundPathGenerator.generatePath(path, locale);
            Direction startDirectionFrom = pathGenerator.getRandomDirection(DirectionsHelper.getNextDirection(path, 0));
            Direction endDirection = pathGenerator.getRandomDirection(DirectionsHelper.getLastDirectionFrom(path, path.size() - 1));
            return new GameState(game, map, path, sounds, startDirectionFrom, endDirection);
        }
    }
}
