/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping;

/**
 * Created by lhoracek on 5/15/15.
 */
public class Properties {

    public static final int TILE_BASE_WIDTH  = 528/2;
    public static final int TILE_BASE_HEIGHT = 264/2;

    public static final int ROAD_WIDTH  = 264/2;
    public static final int ROAD_HEIGHT = 132/2;

    public static final int GAME_STOPS = 5; // game always has +1 steps -> 5 = 6 stops

    public static final int MAP_WIDTH  = Math.max((3 * GAME_STOPS) + 2, 10);
    public static final int MAP_HEIGHT = Math.max((2 * GAME_STOPS) + 2, 5);

    private static final String MFX_FOLDER_PATH = "common/mfx/";
    private static final String RULE_PATH = MFX_FOLDER_PATH + "help/";
    private static final String RULE_BASE_FILE_NAME = "rulehelp_";
    private static final String RULE_FILE_EXTENSION = ".mp3";

    public static final String RULE_EASY = RULE_PATH  + RULE_BASE_FILE_NAME + "easy" + RULE_FILE_EXTENSION;
    public static final String RULE_MEDIUM = RULE_PATH  + RULE_BASE_FILE_NAME + "medium" + RULE_FILE_EXTENSION;
    public static final String RULE_HARD = RULE_PATH  + RULE_BASE_FILE_NAME + "hard" + RULE_FILE_EXTENSION;
    public static final String RULE_BONUS = RULE_PATH  + RULE_BASE_FILE_NAME + "bonus" + RULE_FILE_EXTENSION;

    static final String[] RESULT_SOUNDS        = new String[]{"common/mfx/result/result_0.mp3", "common/mfx/result/result_1.mp3", "common/mfx/result/result_2.mp3", "common/mfx/result/result_3.mp3"};
    static final String[] RESULT_TEXT          = new String[]{"game_kidnapping_result_0", "game_kidnapping_result_1", "game_kidnapping_result_2", "game_kidnapping_result_3"};

    static final String RESULT_TEXT_SUMMARY = "game_kidnapping_stats";
}
