/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.media.assets;

/**
 * Created by Drahomir Karchnak on 30/12/15.
 */
public class KidnappingTextAssets {
	public static final String INSTRUCTIONS_PAY_ATTENTION = "game_kidnapping_instructions_pay_attention";
	public static final String RULE_MESSAGE_UNDERSTAND	  = "game_kidnapping_rulemessage_understand";

	public static final String RULE_EASY	= "game_kidnapping_rule_easy";
	public static final String RULE_MEDIUM	= "game_kidnapping_rule_medium";
	public static final String RULE_HARD	= "game_kidnapping_rule_hardcore";
	public static final String RULE_BONUS	= "game_kidnapping_rule_bonus";
}