/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.media.assets;

import java.util.Random;

import cz.nic.tablexia.game.common.media.AssetDescription;

/**
 * Created by lhoracek on 5/15/15.
 */
public enum TileType implements AssetDescription {


    HOUSE1(1, 1, "house1", 78), //
    HOUSE2(1, 1, "house2", 78), //
    HOUSE3(1, 1, "house3", 78), //
    HOUSE4(1, 1, "house4", 78), //
    HOUSE5(1, 1, "house5", 78), //
    HOUSE6(1, 1, "house6", 78), //
    HOUSE7(1, 1, "house7", 78), //
    HOUSE8(1, 1, "house8", 78), //
    PLAYGOUND(1, 1, "playground", 78), //
    PARK(1, 1, "park", 78), //
    POST(1, 1, "post", 78), //
    SCHOOL(1, 1, "school", 78), //
    VILLA1(1, 1, "villa1", 78), //
    VILLA2(1, 1, "villa2", 78), //
    VILLA3(1, 1, "villa3", 78), //
    VILLA4(1, 1, "villa4", 78); //

    public static final String BASE_DIR = "tiles/";
    private final int swSize, seSize;
    private final String name;
    private final int    yOffset;

    TileType(int sw, int se, String name, int yOffset) {
        this.swSize = sw;
        this.seSize = se;
        this.name = name;
        this.yOffset = yOffset;
    }

    public int getSwSize() {
        return swSize;
    }

    public int getSeSize() {
        return seSize;
    }

    public String getResource() {
        return BASE_DIR + name;
    }

    public int getYOffset() {
        return yOffset;
    }

    public static TileType getRandomTile(Random random) {
        return TileType.values()[random.nextInt(TileType.values().length)];
    }

}
