/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.model;

import cz.nic.tablexia.game.games.kidnapping.actors.KidnappingTile;
import cz.nic.tablexia.game.games.kidnapping.media.assets.TextureTypes;

/**
 * Created by lhoracek on 5/21/15.
 */
public enum Direction {
    NW(TextureTypes.POSITION_LINE_NWSE, KidnappingTile.PATH_NORTH_WEST_X, KidnappingTile.PATH_NORTH_WEST_Y),
    NE(TextureTypes.POSITION_LINE_NESW, KidnappingTile.PATH_NORTH_EAST_X, KidnappingTile.PATH_NORTH_EAST_Y),
    SE(TextureTypes.POSITION_LINE_NWSE, KidnappingTile.PATH_NORTH_EAST_X, KidnappingTile.PATH_NORTH_EAST_Y),
    SW(TextureTypes.POSITION_LINE_NESW, KidnappingTile.PATH_NORTH_WEST_X, KidnappingTile.PATH_NORTH_WEST_Y);
    
    private TextureTypes roadType;
    private float        x,y;

    Direction(TextureTypes roadType, float x, float y) {
        this.roadType = roadType;
        this.x = x;
        this.y = y;
    }

    public TextureTypes getRoadType() {
        return roadType;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public Direction getOppositeDirection() {
        switch (this) {
            case NE:
                return Direction.SW;
            case NW:
                return Direction.SE;
            case SE:
                return Direction.NW;
            case SW:
                return Direction.NE;
            default:
                throw new IllegalArgumentException("Wrong direction argument");
        }
    }
}
