/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Timer;
import com.google.common.collect.ObjectArrays;

import java.util.Arrays;
import java.util.List;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.common.media.AssetDescription;
import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.common.media.SfxLibrary;
import cz.nic.tablexia.game.games.kidnapping.actions.ArrowMusicAction;
import cz.nic.tablexia.game.games.kidnapping.actions.CenterTileAction;
import cz.nic.tablexia.game.games.kidnapping.actions.fades.CorrectAlphaAction;
import cz.nic.tablexia.game.games.kidnapping.actions.fades.PlayExample;
import cz.nic.tablexia.game.games.kidnapping.actions.fades.ReplayAlphaAction;
import cz.nic.tablexia.game.games.kidnapping.actions.fades.ShowArrowAction;
import cz.nic.tablexia.game.games.kidnapping.actions.fades.ShowFlagAction;
import cz.nic.tablexia.game.games.kidnapping.actions.fades.ShowRoadAction;
import cz.nic.tablexia.game.games.kidnapping.actions.fades.WrongAlphaAction;
import cz.nic.tablexia.game.games.kidnapping.actors.Arrow;
import cz.nic.tablexia.game.games.kidnapping.actors.KidnappingMap;
import cz.nic.tablexia.game.games.kidnapping.media.DirectionSounds;
import cz.nic.tablexia.game.games.kidnapping.media.assets.KidnappingTextAssets;
import cz.nic.tablexia.game.games.kidnapping.media.assets.SoundType;
import cz.nic.tablexia.game.games.kidnapping.media.assets.TextureTypes;
import cz.nic.tablexia.game.games.kidnapping.media.assets.TileType;
import cz.nic.tablexia.game.games.kidnapping.model.Direction;
import cz.nic.tablexia.game.games.kidnapping.model.GameState;
import cz.nic.tablexia.game.games.kidnapping.util.DirectionsHelper;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.util.MusicUtil;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;

/**
 * Created by lhoracek on 6.3.15.
 * Edited by Drahomir Karchnak
 */
public class KidnappingGame extends AbstractTablexiaGame<GameState> {
    public  static final String RULE_BUTTON                     = "rule button";
    public  static final String MAP                             = "map";

    public  static final String EVENT_EXAMPLE_PLAYED            = "example played";
    public  static final String EVENT_SOUNDS_PLAYED             = "sounds played";
    public  static final String EVENT_RULE_SHOWED               = "rule showed";
    public  static final String EVENT_START_ROUND               = "start round";
    public  static final String EVENT_CORRECT                   = "correct direction";
    public  static final String EVENT_ANIMATION_WRONG_FINISHED  = "animation wrogn finished";

	private static final ApplicationFontManager.FontType        HELP_OVERLAY_FONT 		= ApplicationFontManager.FontType.BOLD_18;
	private static final Color 									HELP_OVERLAY_FONT_COLOR = Color.BLACK;

	private static final float RULE_MESSAGE_PAPER_WIDTH  =	0.6f;
	private static final float RULE_MESSAGE_TEXT_PADDING =  0.1f;

	private static final float RULE_SOUND_DELAY 		  = 1.0f;
	private static final float RULE_SOUND_FADE_OUT_TIME   =	0.5f;
	private static final float RULE_MESSAGE_FADE_OUT_TIME =	0.5f;
	private static final float HELP_OVERLAY_FADE_IN_TIME  =	0.5f;
	private static final float MAP_FADE_IN_TIME			  =	0.5f;
	private static final float GAME_DELAY				  = 0.5f;
	private static final float GAME_PLAY_SOUNDS_DELAY	  = 0.5f;

	private static final float MAP_SCALE =	1f;
	private static final float CENTER_MAP_TIME = 0.7f;
	private static final float BUTTON_BOTTOM_OFFSET = 0.2f;

	private GfxLibrary gfxLibrary = new GfxLibrary(this, ObjectArrays.concat(TextureTypes.values(), TileType.values(), AssetDescription.class));
	private SfxLibrary sfxLibrary = new SfxLibrary(this, SoundType.values());

    private KidnappingMap map;
	private Group ruleMessage;
	private boolean ruleMessageShown = false;
	private StandardTablexiaButton ruleButton;
    private boolean isBackButtonAllowed;

    private DirectionSounds.SoundPack actualSoundPack;
    private int mistakesCount = 0;
    private boolean didMistakeInThisRound = false;

	@Override
    protected void prepareGameSoundAssetNames(List<String> soundsFileNames) {
        soundsFileNames.addAll(sfxLibrary.values());
    }

    @Override
    protected GameState prepareGameData(java.util.Map<String, String> gameState) {
        GameState gs = GameState.GameStateFactory.createInstance(getGame(), getRandom(), getGameDifficulty(), TablexiaSettings.getInstance().getLocale(), Properties.MAP_WIDTH, Properties.MAP_HEIGHT, Properties.GAME_STOPS, Properties.MAP_WIDTH / 2, Properties.MAP_HEIGHT / 2);
        return gs;
    }

    @Override
    protected void gameLoaded(final java.util.Map<String, String> gameState) {
        mistakesCount = 0;
        isBackButtonAllowed = false;
		getStage().clear();
		getStage().addActor(map = KidnappingMap.KidnappingMapFactory.createInstance(this, gfxLibrary, getData()));
		map.setMapScale(MAP_SCALE);

		// center first tile and start game
		map.addAction(CenterTileAction.getInstance(map.getTile(getData().getCurrentPosition())));
		map.getReplayButton().setInputListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                playSounds();
                getData().addReplay();
            }
        });

		map.addAction(Actions.alpha(0f));
		map.addAction(Actions.visible(false));
        map.setName(MAP);

		prepareRuleMessage();

		//Resets stats to 0
		getData().resetMisses();
		getData().resetReplays();
        getData().resetRoundsCorrect();

		screenResized(0, 0);
	}

    @Override
    public boolean isSoundMandatory() {
        return true;
    }

    private void prepareRuleMessage() {
		ruleMessage = new Group();

		//Adds Backgrounds
		Image background = new TablexiaNoBlendingImage(getApplicationTextureRegion(ApplicationAtlasManager.BACKGROUND_WOODEN));
		setActorToFullScene(background);

		ruleMessage.addActor(background);

		//Adds Paper
		Image image = new Image(getGameGlobalTextureRegion(GAME_RULE_BACKGROUND));
		image.setSize(getSceneWidth() * RULE_MESSAGE_PAPER_WIDTH, getSceneInnerHeight());
		image.setPosition(getSceneLeftX() + getSceneWidth() / 2 - image.getWidth() / 2, getSceneInnerBottomY());

		ruleMessage.addActor(image);

		//Adds 'understand' button
		ruleButton = new StandardTablexiaButton(getText(KidnappingTextAssets.RULE_MESSAGE_UNDERSTAND), StandardTablexiaButton.TablexiaButtonType.GREEN);
        ruleButton.setName(RULE_BUTTON);
		ruleButton.setPosition(getSceneLeftX() + getSceneWidth() / 2 - ruleButton.getWidth() / 2, getSceneInnerBottomY() + getSceneInnerHeight() * BUTTON_BOTTOM_OFFSET);
		ruleButton.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);

                ruleMessage.addAction(new SequenceAction(Actions.fadeOut(RULE_MESSAGE_FADE_OUT_TIME), Actions.visible(false)));
                stopRuleSound(RULE_SOUND_FADE_OUT_TIME);

                map.addAction(Actions.sequence(Actions.delay(RULE_MESSAGE_FADE_OUT_TIME + GAME_DELAY),
                        Actions.run(new Runnable() {
                            public void run() {
                                startRound();
                            }
                        })));
            }
		});
        ruleButton.setVisible(false);
		ruleMessage.addActor(ruleButton);

		//Decide which text to use
		String helpText = getText(KidnappingDifficultyDefinition.getKidnappingDifficultyDefinitionForGameDifficulty(getGameDifficulty()).getRuleDetailsDefinition());

		//Actual Message
		TablexiaLabel.TablexiaLabelStyle ruleMessageStyle = new TablexiaLabel.TablexiaLabelStyle(HELP_OVERLAY_FONT, HELP_OVERLAY_FONT_COLOR);
		TablexiaLabel ruleMessageText = new TablexiaLabel(helpText, ruleMessageStyle);

		ruleMessageText.setWrap(true);
		ruleMessageText.setAlignment(Align.center);
		ruleMessageText.setWidth(getSceneWidth() * RULE_MESSAGE_PAPER_WIDTH - (2 * (getSceneWidth() * RULE_MESSAGE_TEXT_PADDING)));

		ruleMessageText.setPosition(getSceneLeftX() + getSceneWidth() / 2 - ruleMessageText.getWidth() / 2,
				getSceneInnerBottomY() + getSceneInnerHeight() / 2 - ruleMessageText.getHeight() / 2);

		ruleMessage.addActor(ruleMessageText);

		//Makes sure ruleMessage is not visible at first
		ruleMessage.addAction(Actions.visible(false));
		ruleMessage.addAction(Actions.alpha(0));

		getStage().addActor(ruleMessage);
	}

	private void showRuleMessage() {
		ruleMessage.addAction(new SequenceAction(Actions.visible(true), Actions.fadeIn(HELP_OVERLAY_FADE_IN_TIME)));
		ruleMessageShown = true;
	}

    @Override
    public void backButtonPressed() {
        if(isBackButtonAllowed) super.backButtonPressed();
    }

    private void playRuleSound(float delay) {
        Timer.schedule(new Timer.Task(){
            @Override
            public void run() {
                ruleButton.setVisible(true);
                getRuleSound().play();
                triggerScenarioStepEvent(EVENT_RULE_SHOWED);
            }
        }, delay);
	}

	private void stopRuleSound(float fadeoutTime) {
		if (getRuleSound().isPlaying()) {
			MusicUtil.fadeOut(getRuleSound(), fadeoutTime);
		}
	}

    @Override
    protected void gameVisible() {
		// center first tile and start game
        triggerScenarioStepEvent(EVENT_GAME_READY);
		if (!ruleMessageShown) {
			showRuleMessage();
			playRuleSound(RULE_SOUND_DELAY);
		}
		else {
			map.addAction(Actions.sequence(Actions.delay(RULE_MESSAGE_FADE_OUT_TIME  + GAME_DELAY),
					Actions.run(new Runnable() {
						public void run() {
							startRound();
						}
					})));
		}
        isBackButtonAllowed = true;
	}

    @Override
    protected void screenResized(int width, int height) {
        super.screenResized(width, height);
        map.setBounds(0, getStage().getCamera().position.y - getStage().getHeight() / 2, getStage().getWidth(), getStage().getHeight()); // scaling viewport camera y-position adjustment
    }

    /**
     * Plays example sound and all sounds for directions, then shows the replay button
     */
    private void startRound() {
		map.addAction(	Actions.sequence(	Actions.visible(true),
											Actions.fadeIn(MAP_FADE_IN_TIME),
											ReplayAlphaAction.fadeOut(),
											Actions.parallel(	CenterTileAction.getInstance(map.getTile(getData().getCurrentPosition()), CENTER_MAP_TIME),
																getData().getStep() != 0 ? ShowRoadAction.fadeIn(getData().getLastPosition(), getData().getLastDirectionFrom().getOppositeDirection()) : Actions.delay(0)),
																ShowFlagAction.fadeIn(getData().getCurrentPosition()),
																Actions.delay(GAME_PLAY_SOUNDS_DELAY),
																Actions.run(new Runnable() {
            public void run() {
				playSounds();
                triggerScenarioStepEvent(EVENT_START_ROUND);
            }
        })));
    }

    /**
     * Plays sound for one round while animating arrows
     */
    private void playSounds() {
        didMistakeInThisRound = false;
        final Position position = getData().getCurrentPosition();
        actualSoundPack = getData().getSounds().get(getData().getStep());
        final Direction[] directions = DirectionsHelper.getNextDirections(getData().getLastDirectionFrom());
		final Arrow[] arrows = map.getDirectionArrows(position, directions, gfxLibrary);
		for (final Arrow a : arrows) {
            a.setDisabled(true);
            a.clearListeners();
		}

        map.addAction(Actions.sequence(ReplayAlphaAction.fadeOut(), new PlayExample(actualSoundPack.getExample(this)), Actions.run(new Runnable() {
            @Override
            public void run() {
                Music[] sounds = actualSoundPack.getSoundsInOrder(KidnappingGame.this, directions, getData().getNextDirection());
                triggerScenarioStepEvent(EVENT_EXAMPLE_PLAYED);
                map.addAction(Actions.sequence(ShowArrowAction.arrowsFadeIn(arrows), Actions.delay(0.5f), ArrowMusicAction.arrowSequence(sounds, arrows), ReplayAlphaAction.fadeIn(), Actions.run(new Runnable() {
                    public void run() {

						final Arrow correct = arrows[DirectionsHelper.getNextDirectionIndex(directions, getData().getNextDirection())];
						for (final Arrow a : arrows) {
							a.setDisabled(false);
                            a.addListener(new ClickListener() {
                                @Override
                                public void clicked(InputEvent event, float x, float y) {
                                    if(a.isDisabled()) return;
									a.setDisabled(true);

									if (event.getTarget() == correct) {
										//Disables all arrows
										for (final Arrow arr : arrows) {
											arr.setDisabled(true);
										}
                                        sfxLibrary.getSound(SoundType.SOUND_CORRECT).play();
                                        map.getReplayButton().setVisible(false);
                                        map.addAction(Actions.sequence(CorrectAlphaAction.fadeIn(), CorrectAlphaAction.fadeOut(), ShowArrowAction.arrowsFadeOut(arrows)));
                                        correct();
									} else {
										sfxLibrary.getSound(SoundType.SOUND_ERROR).play();
										map.addAction(Actions.sequence(WrongAlphaAction.fadeIn(), WrongAlphaAction.fadeOut(),Actions.run(new Runnable() {
                                            @Override
                                            public void run() {
                                                triggerScenarioStepEvent(EVENT_ANIMATION_WRONG_FINISHED);
                                            }
                                        })));
                                        mistakeInThisRound();
									}
                                }
                            });
                        }
                        triggerScenarioStepEvent(EVENT_SOUNDS_PLAYED);
                    }
                })));
            }
        })));
    }

    private void mistakeInThisRound() {
        wrong();
        if (!didMistakeInThisRound) {
            mistakesCount++;
            map.hideLife();
            didMistakeInThisRound = true;
            if (mistakesCount > 1){
                gameComplete();
            }
        }
    }

    public void correct() {
        if (!didMistakeInThisRound) getData().addedCorrectRound();
        triggerScenarioStepEvent(EVENT_CORRECT);
        if (actualSoundPack != null) {
            actualSoundPack.disposeMusic();
        }
        if (getData().getStep() == getData().getPath().size() - 1) {
            gameComplete();
        } else {
            getData().setStep(getData().getStep() + 1);
            startRound();
        }
    }

    public void wrong() {
        getData().addMiss();
    }

    public GfxLibrary getGfxLibrary() {
        return gfxLibrary;
    }

    public SfxLibrary getSfxLibrary() {
        return sfxLibrary;
    }

    private Music getRuleSound() {
		return getMusic(KidnappingDifficultyDefinition.getKidnappingDifficultyDefinitionForGameDifficulty(getGameDifficulty()).getRuleSoundPath());
    }

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(new SummaryMessage(SummaryImage.STATS, getFormattedText(Properties.RESULT_TEXT_SUMMARY, game.getGameScore(GameState.GAME_ROUNDS_CORRECT, "0"))));
    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        return Properties.RESULT_TEXT[gameResult.getStarCount()];
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        return Properties.RESULT_SOUNDS[gameResult.getStarCount()];
    }


//////////////////////////////////////////// PRELOADER

    private static final String                             PRELOADER_INFO_IMAGE            = "preloader_info";
    private static final String                             PRELOADER_ANIM1_IMAGE           = "preloader_anim1";
    private static final int                                PRELOADER_ANIM1_FRAMES          = 5;
    private static final String                             PRELOADER_ANIM2_IMAGE           = "preloader_anim2";
    private static final int                                PRELOADER_ANIM2_FRAMES          = 6;
    private static final float                              PRELOADER_ANIM1_FRAME_DURATION  = 0.7f;
    private static final float                              PRELOADER_ANIM2_FRAME_DURATION  = 0.7f;
    private static final String                             PRELOADER_TEXT1_KEY             = ApplicationTextManager.ApplicationTextsAssets.GAME_KIDNAPPING_PRELOADER_TEXT1;
    private static final String                             PRELOADER_TEXT2_KEY             = ApplicationTextManager.ApplicationTextsAssets.GAME_KIDNAPPING_PRELOADER_TEXT2;
    private static final String                             PRELOADER_TEXT3_KEY             = ApplicationTextManager.ApplicationTextsAssets.GAME_KIDNAPPING_PRELOADER_TEXT3;

    private static final Scaling                            PRELOADER_IMAGE_SCALING             = Scaling.fit;
    private static final int 			                    PRELOADER_TEXT_ALIGN 				= Align.left;
    private static final float 			                    PRELOADER_TEXT_PADDING 				= 10f;
    private static final float 			                    PRELOADER_LEFT_COLUMN_WIDTH_RATIO 	= 1f / 4;
    private static final float 			                    PRELOADER_RIGHT_COLUMN_RATIO 		= 5f / 8;
    private static final float 			                    PRELOADER_ROW_HEIGHT 				= 1f / 5;

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        AnimatedImage preloaderImage1 = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM1_IMAGE, PRELOADER_ANIM1_FRAMES, PRELOADER_ANIM1_FRAME_DURATION), false);
        preloaderImage1.startAnimationLoop();
        String preloaderText1 = preloaderAssetsManager.getText(PRELOADER_TEXT1_KEY);
        components.add(new TwoColumnContentDialogComponent(
                new AnimatedImageContentDialogComponent(preloaderImage1, PRELOADER_IMAGE_SCALING),
                new TextContentDialogComponent(preloaderText1, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_ROW_HEIGHT));

        components.add(new FixedSpaceContentDialogComponent());

        AnimatedImage preloaderImage2 = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM2_IMAGE, PRELOADER_ANIM2_FRAMES, PRELOADER_ANIM2_FRAME_DURATION), false);
        preloaderImage2.startAnimationLoop();
        String preloaderText2 = preloaderAssetsManager.getText(PRELOADER_TEXT2_KEY);
        components.add(new TwoColumnContentDialogComponent(
                new TextContentDialogComponent(preloaderText2, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                new AnimatedImageContentDialogComponent(preloaderImage2, PRELOADER_IMAGE_SCALING),
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_ROW_HEIGHT));

        components.add(new FixedSpaceContentDialogComponent());

        Image preloaderImage3 = new Image(preloaderAssetsManager.getTextureRegion(PRELOADER_INFO_IMAGE));
        String preloaderText3 = preloaderAssetsManager.getText(PRELOADER_TEXT3_KEY);
        components.add(new TwoColumnContentDialogComponent(
                new ImageContentDialogComponent(preloaderImage3, PRELOADER_IMAGE_SCALING),
                new TextContentDialogComponent(preloaderText3, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_ROW_HEIGHT));
    }
}