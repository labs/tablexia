/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actions;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;

/**
 * Created by lhoracek on 5/20/15.
 */
public class WhileAction extends ParallelAction {
    final Action whileAction, repeatAction;

    public WhileAction(Action whileAction, Action repeatAction) {
        super(whileAction, repeatAction);
        this.whileAction = whileAction;
        this.repeatAction = repeatAction;
    }

    @Override
    public boolean act(float delta) {
        boolean running = !whileAction.act(delta);
        boolean restart = repeatAction.act(delta);
        if (running && restart) {
            repeatAction.restart();
        }
        return !running && restart;
    }
}
