/*
 *
 *  * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.kidnapping;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.kidnapping.media.assets.KidnappingTextAssets;

public enum KidnappingDifficultyDefinition {

    EASY(GameDifficulty.EASY, Properties.RULE_EASY, KidnappingTextAssets.RULE_EASY),
    MEDIUM(GameDifficulty.MEDIUM, Properties.RULE_MEDIUM, KidnappingTextAssets.RULE_MEDIUM),
    HARD(GameDifficulty.HARD, Properties.RULE_HARD, KidnappingTextAssets.RULE_HARD),
    BONUS(GameDifficulty.BONUS, Properties.RULE_BONUS, KidnappingTextAssets.RULE_BONUS);


    private GameDifficulty gameDifficulty;
    private String ruleSoundPath;
    private String ruleDetailsDefinition;

    KidnappingDifficultyDefinition(GameDifficulty gameDifficulty, String ruleSoundPath, String ruleDetailsDefinition) {
        this.gameDifficulty = gameDifficulty;
        this.ruleSoundPath = ruleSoundPath;
        this.ruleDetailsDefinition = ruleDetailsDefinition;
    }


    public GameDifficulty getGameDifficulty() {
        return gameDifficulty;
    }

    public String getRuleSoundPath() {
        return ruleSoundPath;
    }

    public String getRuleDetailsDefinition() {
        return ruleDetailsDefinition;
    }

    public static KidnappingDifficultyDefinition getKidnappingDifficultyDefinitionForGameDifficulty(GameDifficulty gameDifficulty){
        for(KidnappingDifficultyDefinition difficultyDefinition : KidnappingDifficultyDefinition.values()){
            if (difficultyDefinition.getGameDifficulty().equals(gameDifficulty)) return difficultyDefinition;
        }
        throw new RuntimeException("Unsupported difficulty");
    }
}
