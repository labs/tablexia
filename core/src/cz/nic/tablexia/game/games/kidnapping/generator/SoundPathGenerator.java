/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.generator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.kidnapping.media.DirectionSounds;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;
import cz.nic.tablexia.game.games.kidnapping.util.DirectionsHelper;

/**
 * Created by lhoracek on 5/15/15.
 */
public class SoundPathGenerator {
    private final Random         random;
    private final GameDifficulty gameDifficulty;

    public SoundPathGenerator(Random random, GameDifficulty gameDifficulty) {
        this.random = random;
        this.gameDifficulty = gameDifficulty;
    }

    public List<DirectionSounds.SoundPack> generatePath(List<Position> path, Locale locale) {
        Set<Integer> usedSounds = new HashSet<Integer>();
        List<DirectionSounds.SoundPack> sounds = new ArrayList<DirectionSounds.SoundPack>();
        for (int i = 0; i < path.size(); i++) {
            DirectionSounds.SoundPack nextSoundPack = null;
            do {
                nextSoundPack = DirectionSounds.KidnappingSoundPackDefinition.getSoundPackByGameDifficulty(gameDifficulty).getRandomSoundPack(random, locale);
            } while (usedSounds.contains(nextSoundPack.getSoundNumber()));
            sounds.add(nextSoundPack);
            usedSounds.add(nextSoundPack.getSoundNumber());
        }
        return sounds;
    }
}
