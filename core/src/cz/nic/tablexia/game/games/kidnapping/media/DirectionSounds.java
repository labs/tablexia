/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.media;

import com.badlogic.gdx.audio.Music;

import java.io.Serializable;
import java.util.Locale;
import java.util.Random;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.kidnapping.KidnappingGame;
import cz.nic.tablexia.game.games.kidnapping.model.Direction;
import cz.nic.tablexia.util.Log;

/**
 * @author lhoracek
 */
public class DirectionSounds {

    public static class SoundPack implements Serializable {

        private static final long serialVersionUID = -4529735716574493739L;

        private Music[] musics;
        private Music exampleMusic;

        public SoundPack(int soundNumber, String example, String correct, String wrong1, String wrong2) {
            this.soundNumber = soundNumber;
            this.example = example;
            this.correct = correct;
            this.wrong1 = wrong1;
            this.wrong2 = wrong2;
        }

        private Integer soundNumber;

        private String example, correct, wrong1, wrong2;

        public String getCorrectName() {
            return correct;
        }

        public String getExampleName() {
            return example;
        }

        public String getWrong1Name() {
            return wrong1;
        }

        public String getWrong2Name() {
            return wrong2;
        }

        public Music getCorrect(KidnappingGame kidnappingGame) {
            return getMusic(kidnappingGame, correct);
        }

        public Music getExample(KidnappingGame kidnappingGame) {
            exampleMusic = getMusic(kidnappingGame, example);
            return exampleMusic;
        }

        public Music getWrong1(KidnappingGame kidnappingGame) {
            return getMusic(kidnappingGame, wrong1);
        }

        public Music getWrong2(KidnappingGame kidnappingGame) {
            return getMusic(kidnappingGame, wrong2);
        }

        private Music getMusic(KidnappingGame kidnappingGame, String music) {
            return kidnappingGame.getMusic("common/mfx." + music + ".mp3");
        }

        public Music[] getSoundsInOrder(KidnappingGame kidnappingGame, Direction[] directions, Direction next) {
            String[] sounds = getSoundNamesInOrder(directions, next);
            musics = new Music[3];
            for (int i = 0; i < 3; i++) {
                musics[i] = getMusic(kidnappingGame, sounds[i]);
            }
            return musics;
        }

        public String[] getSoundNamesInOrder(Direction[] directions, Direction next) {
            String[] sounds = new String[3];
            sounds[0] = (directions[0] == next) ? correct : wrong1;
            sounds[1] = (directions[0] == next) ? wrong1 : ((directions[1] == next) ? correct : wrong2);
            sounds[2] = (directions[2] == next) ? correct : wrong2;

            return sounds;
        }

        public Integer getSoundNumber() {
            return soundNumber;
        }

        public void disposeMusic() {
            if (musics != null) {
                for (Music music : musics) {
                    music.dispose();
                }
            }

            if (exampleMusic != null) {
                exampleMusic.dispose();
            }
        }

        @Override
        public String toString() {
            return "SoundPack{" +
                    "example='" + example + '\'' +
                    ", correct='" + correct + '\'' +
                    ", wrong1='" + wrong1 + '\'' +
                    ", wrong2='" + wrong2 + '\'' +
                    ", soundNumber=" + soundNumber +
                    '}';
        }
    }

    private enum LocaleSoundsCount {
        cs_CZ(TablexiaSettings.LocaleDefinition.cs_CZ.getLocale(), new int[]{20, 19, 31, 18}),
        sk_SK(TablexiaSettings.LocaleDefinition.sk_SK.getLocale(), new int[]{20, 19, 20, 18}),
        de_DE(TablexiaSettings.LocaleDefinition.de_DE.getLocale(), new int[]{20, 19, 20, 17});

        private Locale locale;
        private int[] soundsCount;

        LocaleSoundsCount(Locale locale, int[] soundsCount) {
            this.locale = locale;
            this.soundsCount = soundsCount;
        }

        public int getSoundsCountForDifficulty(GameDifficulty gameDifficulty) {
            return soundsCount[gameDifficulty.getDifficultyNumber() - 1];
        }

        private static LocaleSoundsCount getLocaleSoundCount(Locale locale) {
            for (LocaleSoundsCount localeSoundsCount : values()) {
                if (localeSoundsCount.locale.equals(locale)) {
                    return localeSoundsCount;
                }
            }
            Log.err(KidnappingGame.class, ": Currently chosen language can't be find in LocaleSoundCount Enum!");
            return null;
        }
    }
    
    public enum KidnappingSoundPackDefinition {
        
        EASY(GameDifficulty.EASY, "level1/", null, null, null, false),
        MEDIUM(GameDifficulty.MEDIUM, "level2/", null, null, "-HL", true),
        HARD(GameDifficulty.HARD, "level3/", "VZ", "A", null, false),
        BONUS(GameDifficulty.BONUS, "level4/", "VZ", "A", null, false);
        
        GameDifficulty gameDifficulty;
        String prefix;
        String exampleSuffix;
        String correctSuffix;
        String additionSuffix;
        boolean enableInverting;

        KidnappingSoundPackDefinition(GameDifficulty gameDifficulty, String prefix, String exampleSuffix, String correctSuffix, String additionSuffix, boolean enableInverting) {
            this.gameDifficulty = gameDifficulty;
            this.prefix = prefix;
            this.exampleSuffix = exampleSuffix;
            this.correctSuffix = correctSuffix;
            this.additionSuffix = additionSuffix == null ? "" : additionSuffix;
            this.enableInverting = enableInverting;
        }

        public SoundPack getRandomSoundPack(Random random, Locale locale) {

            int num = random.nextInt(LocaleSoundsCount.getLocaleSoundCount(locale).getSoundsCountForDifficulty(gameDifficulty)) + 1;

            String correct = prefix + num + "-" + (correctSuffix == null ? getRandomLetter(random) : correctSuffix);
            String example = exampleSuffix == null ? correct : prefix + num + "-" + exampleSuffix;

            String wrong1 = null;
            do {
                wrong1 = prefix + num + "-" + getRandomLetter(random);
            } while (wrong1.equals(correct));

            String wrong2 = null;
            do {
                wrong2 = prefix + num + "-" + getRandomLetter(random);
            } while (wrong2.equals(correct) || wrong2.equals(wrong1));

            // swap speech mode on example and direction sounds if inversion is enabled and random is true
            if (enableInverting && random.nextBoolean()) {
                return new SoundPack(num, example + additionSuffix, correct, wrong1, wrong2);
            }
            return new SoundPack(num, example, correct + additionSuffix, wrong1 + additionSuffix, wrong2 + additionSuffix);

        }

        public GameDifficulty getGameDifficulty() {
            return gameDifficulty;
        }

        public static KidnappingSoundPackDefinition getSoundPackByGameDifficulty(GameDifficulty gameDifficulty){
            for(KidnappingSoundPackDefinition kidnappingSoundPackDefinition : KidnappingSoundPackDefinition.values()){
                if (kidnappingSoundPackDefinition.getGameDifficulty().equals(gameDifficulty)){
                    return kidnappingSoundPackDefinition;
                }
            }
            Log.err(DirectionSounds.class.getName(),"Wrong difficulty definition");
            return null;
        }
    }

    protected static String getRandomLetter(Random random) {
        int rand = random.nextInt(3);
        switch (rand) {
            case 1:
                return "A";
            case 2:
                return "B";
            default:
                return "C";
        }
    }

}
