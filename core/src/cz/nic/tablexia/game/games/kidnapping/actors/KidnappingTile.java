/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.games.kidnapping.media.assets.TileType;

/**
 * Created by danilov on 20.4.16.
 */
public class KidnappingTile extends Group {

    public static final int ARROW_EAST_OUT_X     = 265;
    public static final int ARROW_EAST_OUT_Y     = 55;
    public static final int ARROW_EAST_IN_X      = 257;
    public static final int ARROW_EAST_IN_Y      = 53;
    public static final int ARROW_WEST_OUT_X     = 63;
    public static final int ARROW_WEST_OUT_Y     = 56;
    public static final int ARROW_WEST_IN_X      = 68;
    public static final int ARROW_WEST_IN_Y      = 56;

    public static final int PATH_FLAG_X          = 114;
    public static final int PATH_FLAG_Y          = 6;
    
    public static final int PATH_NORTH_EAST_X    = 230;
    public static final int PATH_NORTH_EAST_Y    = 43;
    public static final int PATH_NORTH_WEST_X    = 29;
    public static final int PATH_NORTH_WEST_Y    = 44;

    private static final int TILE_WIDTH          = 399;
    private static final int TILE_HEIGHT         = 237;

    private TileType tileType;
    private Image    street;

    Arrow    northWestIn, northEastIn, northWestOut, northEastOut;
    Image    flag, pathNorthWest, pathNorthEast;

    public Image getStreet() {
        return street;
    }

    public float getCenterX() {
        return (getX() + getStreet().getWidth() / 2);
    }

    public float getCenterY() {
        return (getY() + getStreet().getHeight() / 4);
    }

    public Image getFlag() {
        return flag;
    }

    public Image getPathNorthWest() {
        return pathNorthWest;
    }

    public Image getPathNorthEast() {
        return pathNorthEast;

    }

    public TileType getTileType() {
        return tileType;
    }

    public static class TileFactory {
        public static KidnappingTile createInstance(GfxLibrary gfxLibrary, TileType tileType) {
            KidnappingTile tile = new KidnappingTile();
            tile.tileType = tileType;

            TextureRegion tileTexture = gfxLibrary.getTextureRegion(tileType);

            tile.street = new Image(tileTexture);
            tile.street.setSize(TILE_WIDTH, TILE_HEIGHT);
            tile.addActor(tile.street);
            tile.street.setTouchable(Touchable.disabled);

            return tile;
        }
    }

    //GETS FOR TESTING

    public Arrow getNorthWestIn() {
        return northWestIn;
    }

    public Arrow getNorthEastIn() {
        return northEastIn;
    }

    public Arrow getNorthWestOut() {
        return northWestOut;
    }

    public Arrow getNorthEastOut() {
        return northEastOut;
    }
}
