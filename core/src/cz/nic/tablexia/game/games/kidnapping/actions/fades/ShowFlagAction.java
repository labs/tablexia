/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actions.fades;

import com.badlogic.gdx.scenes.scene2d.Actor;

import cz.nic.tablexia.game.games.kidnapping.actors.KidnappingMap;
import cz.nic.tablexia.game.games.kidnapping.actors.KidnappingTile;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;

/**
 * Created by lhoracek on 5/19/15.
 */
public class ShowFlagAction extends SubActorFadeAction<KidnappingMap> {
    Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public Actor getTargetActor(KidnappingMap map) {
        KidnappingTile tile = map.getTile(position);
        if (tile.getFlag() == null){
            map.addCurrentPositionFlag(position);
        }
        return map.getTile(position).getFlag();
    }


    public static ShowFlagAction fadeIn(Position position) {
        ShowFlagAction sfa = getFade(ShowFlagAction.class, DEFAULT_DURATION, 1);
        sfa.setPosition(position);
        return sfa;
    }
}

