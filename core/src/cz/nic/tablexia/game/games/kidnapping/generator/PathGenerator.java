/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.generator;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import cz.nic.tablexia.game.games.kidnapping.media.assets.TileType;
import cz.nic.tablexia.game.games.kidnapping.model.Direction;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;
import cz.nic.tablexia.game.games.kidnapping.util.DirectionsHelper;


/**
 * Generator cesty městem
 *
 * @author lhoracek
 */
public class PathGenerator {
    private static final String TAG = PathGenerator.class.getSimpleName();
    private Random random;

    public PathGenerator(Random random) {
        this.random = random;
    }

    public List<Position> generatePath(Position startingPosition, int pathLength, Map<Position, TileType> map) {
        List<Position> path = new ArrayList<Position>();
        path.add(startingPosition);
        Position lastPosition = startingPosition;
        Direction lastDirection = null;
        for (int i = 0; i < pathLength; i++) {
            Direction newDirection = getRandomDirection(lastDirection == null ? null : lastDirection.getOppositeDirection());
            Position newPosition = getPositionInDirection(newDirection, lastPosition);
            path.add(newPosition);
            // Log.d(TAG, "Next step " + newDirection + " " + newPosition);

            // kdyz ve vygenerovanem smeru je null, koukneme jetli na SE a S je dlazdece,
            // pokud ano, tak je tu krizovatka a muzeme to rozjet, jinak musime generovat cestu dal
            // do mapy pridame bod, ale dame false, protoze nebude zastavkou

            // kdyz uz jsme na dalsi krizovatce jednou jeli? zatim ponechame

            lastDirection = newDirection;
            lastPosition = newPosition;
        }

        return path;
    }


    /**
     * Return random direction that is diffrent from the fromDiretion
     * @param fromDirection
     * @return
     */
    public Direction getRandomDirection(Direction fromDirection) {
        if (fromDirection == null) {
            Direction[] allDirections = Direction.values();
            fromDirection = allDirections[random.nextInt(4)];
        }
        Direction[] directions = DirectionsHelper.getNextDirections(fromDirection);
        // nastaveni pravdepodobnosti jednotlivých směrů - 20% - 60% - 20%
        float randomIndex = random.nextFloat();
        if (randomIndex < 0.2f) {
            return directions[0];
        } else if (randomIndex < 0.6f) {
            return directions[1];
        } else {
            return directions[2];
        }
    }

    private Position getPositionInDirection(Direction direction, Position currentPosition) {
        switch (direction) {
            case NE:
                return currentPosition.getNorthEast();
            case NW:
                return currentPosition.getNorthWest();
            case SE:
                return currentPosition.getSouthEast();
            case SW:
                return currentPosition.getSouthWest();
            default:
                throw new IllegalArgumentException("Wrong direction argument");
        }
    }
}
