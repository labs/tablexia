/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actions.fades;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;

/**
 * Created by lhoracek on 5/25/15.
 */
public abstract class SubActorFadeAction<T extends Actor> extends AlphaAction {
    public static final float DEFAULT_DURATION = 0.5f;

    public SubActorFadeAction() {
    }

    @Override
    public void setActor(Actor actor) {
        super.setActor(actor);
        if (actor != null) {
            setTarget(getTargetActor(getActorClass(actor)));
        }
    }

    private T getActorClass(Actor actor){
        return (T) actor;
    }

    public abstract Actor getTargetActor(T map);

    @Override
    protected void begin() {
        getTarget().getColor().a = getTarget().isVisible() ? 1 : 0;
        getTarget().setVisible(true);
        super.begin();
    }

    @Override
    public void end() {
        super.end();
        if(getTarget().getColor().a == 0f){
            getTarget().setVisible(false);
        }
    }

    protected static <T extends SubActorFadeAction> T  getFade(Class<T> clazz, float duration, float targetAlpha) {
        T action = Actions.action(clazz);
        action.setAlpha(targetAlpha);
        action.setDuration(duration);
        action.setInterpolation(null);
        return action;
    }
}