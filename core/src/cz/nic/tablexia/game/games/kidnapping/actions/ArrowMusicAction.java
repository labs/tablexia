/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actions;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelegateAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import cz.nic.tablexia.game.games.kidnapping.actions.fades.BlinkAction;
import cz.nic.tablexia.game.games.kidnapping.actors.Arrow;
import cz.nic.tablexia.util.ui.PlayMusicAction;

/**
 * Created by lhoracek on 5/22/15.
 */
public class ArrowMusicAction extends DelegateAction {


    public ArrowMusicAction(Arrow arrow, Music music) {
        setAction(new WhileAction(Actions.sequence(Actions.delay(1f), PlayMusicAction.getInstance(music), Actions.delay(1f)), new BlinkAction(arrow)));
    }

    @Override
    protected boolean delegate(float delta) {
        return getAction().act(delta);
    }

    public static Action arrowSequence(Music[] sounds, Arrow[] arrows){
        SequenceAction sa = Actions.sequence();
        for(int i = 0; i< sounds.length; i++){
            sa.addAction(new ArrowMusicAction(arrows[i], sounds[i]));
        }
        return sa;
    }
}
