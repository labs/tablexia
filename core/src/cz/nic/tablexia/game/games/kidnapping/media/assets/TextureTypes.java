/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.media.assets;

import cz.nic.tablexia.game.common.media.AssetDescription;

/**
 * @author lhoracek
 */
public enum TextureTypes implements AssetDescription {

    POSITION_CURRENT("position-current"), //
    OVERLAY("overlay"), //
    EAR("ear"), //
    POSITION_LINE_NWSE("lineNWSE"), //
    POSITION_LINE_NESW("lineNESW"), //

    WRONG("wrong"), //
    CORRECT("correct"), //

    POSITION_NEXT_NW("arrow-left-top-unpressed"), //
    POSITION_NEXT_NE("arrow-right-top-unpressed"), //
    POSITION_NEXT_SW("arrow-left-down-unpressed"), //
    POSITION_NEXT_SE("arrow-right-down-unpressed"), //

    POSITION_NEXT_NW_PRESSED("arrow-left-top-pressed"), //
    POSITION_NEXT_NE_PRESSED("arrow-right-top-pressed"), //
    POSITION_NEXT_SW_PRESSED("arrow-left-down-pressed"), //
    POSITION_NEXT_SE_PRESSED("arrow-right-down-pressed"), //

    PAPER("empty_card");

    private final String resource;


    private TextureTypes(String resource) {
        this.resource = resource;
    }

    public String getResource() {
        return resource;
    }
}
