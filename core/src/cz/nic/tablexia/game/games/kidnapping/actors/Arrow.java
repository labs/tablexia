/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;

/**
 * @author lhoracek
 */
public class Arrow extends Stack {
    private Image normal, pressed;
    private boolean disabled = false;
    private final Color COLOR_ENABLED = new Color(0x71B714ff);
    private final Color COLOR_DISABLED = Color.GRAY;

    public Arrow(TextureRegion arrow, TextureRegion arrowPressed) {
        addActor(pressed = new Image(arrowPressed));
        addActor(normal = new Image(arrow));
        reset();
    }

    public void reset() {
        pressed.setVisible(false);
        normal.setVisible(true);
    }


    public void setDisabled(boolean val) {
        if (!val) {
            normal.setColor(COLOR_ENABLED);
            pressed.setColor(COLOR_ENABLED);
        } else {
            normal.setColor(COLOR_DISABLED);
            pressed.setColor(COLOR_DISABLED);
        }
        this.disabled = val;
    }

    public boolean isDisabled() {
        return disabled;
    }

    @Override
    public Actor hit(float x, float y, boolean touchable) {
        return super.hit(x, y, touchable) == null ? null : this;
    }
}
