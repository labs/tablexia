/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.kidnapping.util;

import java.util.Arrays;
import java.util.List;

import cz.nic.tablexia.game.games.kidnapping.model.Direction;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;

/**
 * Created by lhoracek on 5/14/15.
 */
public class DirectionsHelper {

    /**
     * Brátí hodnotu enumu pro příští smer z cesty
     *
     * @param position
     * @return
     */
    public static Direction getNextDirection(List<Position> path, int position) {
        return getDirection(1, path, position);
    }

    /**
     * Brátí hodnotu enumu pro předchozí směr z cesty. bere se z aktuální pozice směrem na předchozí.
     *
     * @param position
     * @return
     */
    public static Direction getLastDirectionFrom(List<Position> path, int position) {
        Direction direction = getDirection(-1, path, position);
        return direction == null ? null : direction.getOppositeDirection();
    }

    private static Direction getDirection(int step, List<Position> path, int position) {
        Direction fromDirection = null;
        if (((step < 0) && (position > (-step) - 1)) || ((step > 0) && (position < ((path.size() - step))))) {
            boolean preNE = path.get(position).getNorthEast().equals(path.get(position + step));
            boolean preNW = path.get(position).getNorthWest().equals(path.get(position + step));
            boolean preSE = path.get(position).getSouthEast().equals(path.get(position + step));
            boolean preSW = path.get(position).getSouthWest().equals(path.get(position + step));

            if (preSW) {
                fromDirection = Direction.SW;
            } else if (preSE) {
                fromDirection = Direction.SE;
            } else if (preNW) {
                fromDirection = Direction.NW;
            } else if (preNE) {
                fromDirection = Direction.NE;
            } else {
                throw new IllegalArgumentException("Could not tell previous direction");
            }
            return step < 0 ? fromDirection.getOppositeDirection() : fromDirection;
        }
        return null;
    }

    public static Direction[] getNextDirections(Direction fromDirection) {
        Direction[] allDirections = Direction.values();
        int lastDirectionIndex = Arrays.asList(allDirections).indexOf(fromDirection);
        return new Direction[]{allDirections[(lastDirectionIndex + 1) % Direction.values().length], allDirections[(lastDirectionIndex + 2) % Direction.values().length], allDirections[(lastDirectionIndex + 3) % Direction.values().length]};
    }

    public static int getNextDirectionIndex(Direction[] directions, Direction direction) {
        for (int i = 0; i < directions.length; i++) {
            if (directions[i] == direction) {
                return i;
            }
        }
        throw new IllegalStateException("Direction " + direction + " not found in " + Arrays.toString(directions));
    }
}
