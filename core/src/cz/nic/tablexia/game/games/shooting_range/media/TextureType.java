/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.media;

import java.util.Arrays;
import java.util.List;

import cz.nic.tablexia.game.common.media.AssetDescription;
import cz.nic.tablexia.game.difficulty.GameDifficulty;

/**
 * Created by lhoracek on 6/22/15.
 */
public enum TextureType implements AssetDescription {

    WATCH("watch"), //
    WATCH_HAND("watch_hand"), //

    CAROUSEL("carousel"), //

    BACKGROUND("background"), //
    FRAME("frame"), //

    BOX_BAD("box_bad"), //
    BOX_GOOD("box_good"), //

    WAVE_1("plan2"), //
    WAVE_2("plan1"), //
    WAVE_3("plan3"), //

    FLOWER_1("flowers/1"), //
    FLOWER_2("flowers/2"), //
    FLOWER_3("flowers/3"), //
    FLOWER_4("flowers/4"), //
    FLOWER_5("flowers/5"), //
    FLOWER_6("flowers/6"), //
    FLOWER_7("flowers/7"), //
    FLOWER_8("flowers/8"), //
    FLOWER_9("flowers/9"), //
    FLOWER_10("flowers/10"), //
    FLOWER_11("flowers/11"), //
    FLOWER_12("flowers/12"), //
    FLOWER_13("flowers/13"), //
    FLOWER_14("flowers/14"), //
    FLOWER_15("flowers/15"), //
    FLOWER_16("flowers/16"), //
    FLOWER_17("flowers/17"), //
    FLOWER_18("flowers/18"), //
    FLOWER_19("flowers/19"), //
    FLOWER_20("flowers/20"), //
    FLOWER_21("flowers/21"), //
    FLOWER_22("flowers/22"), //
    FLOWER_23("flowers/23"), //
    FLOWER_24("flowers/24"), //

    FLOWER_1_B("flowers/bonus/1"),
    FLOWER_2_B("flowers/bonus/2"),
    FLOWER_3_B("flowers/bonus/3"),
    FLOWER_4_B("flowers/bonus/4"),
    FLOWER_5_B("flowers/bonus/5"),
    FLOWER_6_B("flowers/bonus/7"),
    FLOWER_8_B("flowers/bonus/8"),
    FLOWER_9_B("flowers/bonus/9"),
    FLOWER_10_B("flowers/bonus/10"),
    FLOWER_1_BM("flowers/bonus/1", true),
    FLOWER_2_BM("flowers/bonus/2", true),
    FLOWER_3_BM("flowers/bonus/3", true),
    FLOWER_4_BM("flowers/bonus/4", true),
    FLOWER_5_BM("flowers/bonus/5", true),
    FLOWER_6_BM("flowers/bonus/6", true),
    FLOWER_7_BM("flowers/bonus/7", true),
    FLOWER_8_BM("flowers/bonus/8", true),
    FLOWER_9_BM("flowers/bonus/9", true),
    FLOWER_10_BM("flowers/bonus/10", true),

    BOX_SMOKE_1("effects/1smoke"), //
    BOX_SMOKE_2("effects/2smoke"), //
    BOX_SMOKE_3("effects/3smoke"), //
    BOX_SMOKE_4("effects/4smoke"), //
    BOX_SMOKE_5("effects/5smoke"), //
    BOX_HIT("effects/good_plus1"), //
    BOX_TIME_MINUS_7("effects/bad_minus7"), //
    BOX_TIME_MINUS_5("effects/bad_minus5"), //
    BOX_TIME_PLUS_5("effects/good_plus5"), //
    BOX_TIME_PLUS_7("effects/good_plus7"), //
    BOX_SPEED_UP("effects/bad_fast"), //
    BOX_SPEED_DOWN("effects/good_slow"), //

    ; //

    public static final TextureType[] BOXES                = {BOX_BAD, BOX_GOOD};
    public static final TextureType[] EFFECTS              = {BOX_SMOKE_1, BOX_SMOKE_2, BOX_SMOKE_3, BOX_SMOKE_4, BOX_SMOKE_5, BOX_TIME_MINUS_5, BOX_TIME_MINUS_7, BOX_TIME_PLUS_5, BOX_TIME_PLUS_7, BOX_SPEED_UP, BOX_SPEED_DOWN};
    public static final TextureType[] FLOWERS_LEVEL_EASY   = {FLOWER_1, FLOWER_6, FLOWER_9, FLOWER_13, FLOWER_17, FLOWER_21};
    public static final TextureType[] FLOWERS_LEVEL_MEDIUM = {FLOWER_2, FLOWER_4, FLOWER_5, FLOWER_7, FLOWER_11, FLOWER_12, FLOWER_14, FLOWER_15, FLOWER_17, FLOWER_19, FLOWER_21, FLOWER_22};
    public static final TextureType[] FLOWERS_LEVEL_HARD   = {FLOWER_1, FLOWER_2, FLOWER_3, FLOWER_4, FLOWER_5, FLOWER_6, FLOWER_7, FLOWER_8, FLOWER_9, FLOWER_10, FLOWER_11, FLOWER_12, FLOWER_13, FLOWER_14, FLOWER_15, FLOWER_16, FLOWER_17, FLOWER_18, FLOWER_19, FLOWER_20, FLOWER_21, FLOWER_22, FLOWER_23, FLOWER_24};
    public static final TextureType[] FLOWERS_LEVEL_BONUS   = {FLOWER_1_B, FLOWER_1_BM, FLOWER_4_B, FLOWER_4_BM, FLOWER_5_B, FLOWER_5_BM, FLOWER_8_B, FLOWER_8_BM};

    private final String resource;
    private boolean flipped;

    private TextureType(String resource) {
        this.resource = resource;
        this.flipped = false;
    }

    TextureType(String resource, boolean flipped) {
        this.resource = resource;
        this.flipped = flipped;
    }

    public String getResource() {
        return resource;
    }

    public boolean isFlipped() {
        return flipped;
    }

    public static List<TextureType> getTargetPack(GameDifficulty gameDifficulty) {
        switch (gameDifficulty) {
            case EASY:
                return Arrays.asList(TextureType.FLOWERS_LEVEL_EASY);
            case MEDIUM:
                return Arrays.asList(TextureType.FLOWERS_LEVEL_MEDIUM);
            case HARD:
                return Arrays.asList(TextureType.FLOWERS_LEVEL_HARD);
            case BONUS:
                return Arrays.asList(TextureType.FLOWERS_LEVEL_BONUS);
            default:
                throw new IllegalStateException("Unknown difficulty");
        }
    }
}