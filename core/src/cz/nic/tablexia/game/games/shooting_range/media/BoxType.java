/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.media;

/**
 * @author lhoracek
 */
public enum BoxType {

    TIME_ADD(TextureType.BOX_BAD), //
    TIME_SUB(TextureType.BOX_GOOD), //
    SLOW_DOWN(TextureType.BOX_GOOD), //
    SPEED_UP(TextureType.BOX_BAD), //
    HIT(TextureType.BOX_GOOD), //
    CLOUD(TextureType.BOX_BAD);

    public static final BoxType[] GOOD_BOXES = new BoxType[]{HIT, TIME_SUB, SLOW_DOWN};
    public static final BoxType[] BAD_BOXES  = new BoxType[]{TIME_ADD, SPEED_UP, CLOUD};

    private final TextureType textureType;

    private BoxType(TextureType textureType) {
        this.textureType = textureType;
    }

    public TextureType getTextures() {
        return textureType;
    }

}
