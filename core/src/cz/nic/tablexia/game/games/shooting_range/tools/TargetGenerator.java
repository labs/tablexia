/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.tools;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.shooting_range.Properties;
import cz.nic.tablexia.game.games.shooting_range.actors.Row;
import cz.nic.tablexia.game.games.shooting_range.actors.Target;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;

/**
 * Created by lhoracek on 6/22/15.
 */
public class TargetGenerator {
    private static final String TAG = TargetGenerator.class.getSimpleName();


    private final GameDifficulty gameDifficulty;
    private final Random         random;
    private final GfxLibrary     gfxLibrary;
    private final InputListener  clickListener;
    private final HitEvaluator   hitEvaluator;
    private final List<TextureType> textureTypeBag;
    private final HashMap<TextureType, TextureRegion> additionalTextureRegions;

    public TargetGenerator(GameDifficulty gameDifficulty, Random random, GfxLibrary gfxLibrary, InputListener clickListener, HitEvaluator hitEvaluator) {
        this.random = random;
        this.gameDifficulty = gameDifficulty;
        this.gfxLibrary = gfxLibrary;
        this.clickListener = clickListener;
        this.hitEvaluator = hitEvaluator;
        this.textureTypeBag = TextureType.getTargetPack(gameDifficulty);
        this.additionalTextureRegions = new HashMap<>();
    }

    public TextureType getRandomFlowerType() {
        return textureTypeBag.get(random.nextInt(textureTypeBag.size()));
    }

    private Map<Row, Target> lastTargetsForRow = new HashMap<Row, Target>();

    public Target getLastTargetInRow(Row row) {
        return lastTargetsForRow.get(row);
    }

    public Target addNewTargetToRow(Row row, TextureType requiredTextureType) {
        Target prevLastTarget = getLastTargetInRow(row);
        float targetPeriod = row.getWave().getRunningTime() / row.getWave().getFlowersOnScreen();
        return addNewTargetToRow(row, requiredTextureType, prevLastTarget.getStartTime() + targetPeriod);
    }

    public Target addNewTargetToRow(Row row, TextureType requiredTextureType, float startTime) {
        return addNewTargetToRow(row, requiredTextureType, startTime, true);
    }

    public Target addNewTargetToRow(Row row, TextureType requiredTextureType, float startTime, boolean asLast) {
        List<TextureType> textureTypeBag = new ArrayList<TextureType>(requiredTextureType == null ? getTextureTypeBag() : Arrays.asList((new TextureType[]{requiredTextureType})));
        Target target = getRandomTarget(textureTypeBag, startTime, row);
        if (asLast) {
            lastTargetsForRow.put(row, target);
        }
        return target;
    }

    /*
     * Init row on start
     */
    public List<Target> generateTargetRow(Row row) {
        List<Target> flowers = new ArrayList<Target>();
        List<TextureType> textureTypeBag = new ArrayList<TextureType>(getTextureTypeBag());

        int flowerNumber = row.getWave().getFlowersOnScreen();
        float targetPeriod = row.getWave().getRunningTime() / row.getWave().getFlowersOnScreen();

        // add one more target off the screen
        for (int i = -flowerNumber; i <= 0; i++) {
            Target target = getRandomTarget(textureTypeBag, targetPeriod * i, row);
            flowers.add(target);
            if (i == 0) {
                lastTargetsForRow.put(row, target);
            }
        }
        return flowers;
    }

    private TextureType getRandomBoxType() {
        float boxTypeIndex = random.nextFloat();
        if ((gameDifficulty == GameDifficulty.EASY) && (boxTypeIndex > Properties.EASY_GOOD_PROBABILITY)) {
            return TextureType.BOX_BAD;
        } else if ((gameDifficulty == GameDifficulty.MEDIUM) && (boxTypeIndex > Properties.HARD_GOOD_PROBABILITY)) {
            return TextureType.BOX_BAD;
        } else if ((gameDifficulty == GameDifficulty.HARD) && (boxTypeIndex > Properties.HARD_GOOD_PROBABILITY)) {
            return TextureType.BOX_BAD;
        }

        return TextureType.BOX_GOOD;
    }

    private Target getRandomTarget(List<TextureType> textureTypeBag, float startTime, Row row) {
        float boxIndex = random.nextFloat();
        TextureType textureType = getRandomBoxType();
        if (boxIndex > getBoxProbability()) {
            int random = (int) (Math.random() * textureTypeBag.size());
            textureType = textureTypeBag.get(random);
        }

        TextureRegion targetTextureRegion;

        if (textureType.isFlipped()){
            targetTextureRegion = additionalTextureRegions.get(textureType);
            if (targetTextureRegion == null) {
                targetTextureRegion = new TextureRegion(gfxLibrary.getTextureRegion(textureType));
                targetTextureRegion.flip(textureType.isFlipped(), false);
                additionalTextureRegions.put(textureType, targetTextureRegion);
            }
        }else {
            targetTextureRegion = gfxLibrary.getTextureRegion(textureType);
        }
        return new Target(targetTextureRegion, startTime, row, textureType, clickListener, hitEvaluator, textureType.isFlipped());
    }

    private float getBoxProbability() {
        switch (gameDifficulty) {
            case EASY:
                return Properties.EASY_BOX_PROBABILITY;
            case MEDIUM:
                return Properties.MEDIUM_BOX_PROBABILITY;
            case HARD:
                return Properties.HARD_BOX_PROBABILITY;
            case BONUS:
                return Properties.MEDIUM_BOX_PROBABILITY;
            default:
                throw new IllegalStateException("Unknown difficulty");
        }
    }

    private List<TextureType> getTextureTypeBag() {
        return textureTypeBag;
    }
}