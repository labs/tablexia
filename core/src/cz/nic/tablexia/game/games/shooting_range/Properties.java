/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range;

import cz.nic.tablexia.shared.model.resolvers.ShootingRangeScoreResolver;

/**
 * Created by lhoracek on 6/22/15.
 */
public class Properties {

    public static final String SCORE_TOTAL      = ShootingRangeScoreResolver.SCORE_TOTAL;
    public static final String SCORE_HITS       = ShootingRangeScoreResolver.SCORE_HITS;
    public static final String SCORE_MISSES     = ShootingRangeScoreResolver.SCORE_MISSES;
    public static final String SCORE_ERRORS     = ShootingRangeScoreResolver.SCORE_ERRORS;
    public static final String SCORE_BAD_BOXES  = ShootingRangeScoreResolver.SCORE_BAD_BOXES;
    public static final String SCORE_GOOD_BOXES = ShootingRangeScoreResolver.SCORE_GOOD_BOXES;

    // GAME SPEED AND TIME
    public static final float ADD_TIME = 5f;
    public static final float SUB_TIME = -5f;

    public static final float GAME_SPEED_FAST    = 2.0f;
    public static final float GAME_SPEED_NORMAL  = 1f;
    public static final float GAME_SPEED_SLOW    = 0.5f;
    public static final long  GAME_SPEED_TIMEOUT = 5000;

     // TODO fix expiration of game speed change

    public static final int GAME_TIME    = 60;
    public static final int WARNING_TIME = 5;

    // TARGET GENERATOR
    public static final float EASY_BOX_PROBABILITY   = 0.05f;                                // 5% of targets are boxes
    public static final float MEDIUM_BOX_PROBABILITY = 0.05f;                                // 5% of targets are boxes
    public static final float HARD_BOX_PROBABILITY   = 0.1f;                                 //10% of targets are boxes

    public static final float EASY_GOOD_PROBABILITY   = 0.7f;
    public static final float MEDIUM_GOOD_PROBABILITY = 0.5f; // TODO check
    public static final float HARD_GOOD_PROBABILITY   = 0.3f;

    public static final String MFX_PATH = "common/mfx/";

    static final String[] RESULT_SOUNDS = new String[]{ MFX_PATH + "result0.mp3",
                                                        MFX_PATH + "result1.mp3",
                                                        MFX_PATH + "result2.mp3",
                                                        MFX_PATH + "result3.mp3"};

    static final String[] RESULT_TEXT   = new String[]{ "game_shootingrange_result_0",
                                                        "game_shootingrange_result_1",
                                                        "game_shootingrange_result_2",
                                                        "game_shootingrange_result_3"};

    static final String RESULT_TEXT_SUMMARY = "game_shootingrange_stats";
}
