/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.actors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.HashMap;

import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;

/**
 * Created by lhoracek on 6/23/15.
 */
public class Carousel extends Group {
    private Image      carouselBase;
    private Image      flower;// TODO change type to Flower
    private GfxLibrary gfxLibrary;
    private TextureType currentTarget;
    private HashMap<TextureType, TextureRegion> additionalRegions;

    public Carousel(GfxLibrary gfxLibrary) {
        addActor(carouselBase = new Image(gfxLibrary.getTextureRegion(TextureType.CAROUSEL)));
        setSize(carouselBase.getWidth(), carouselBase.getHeight());
        setTouchable(Touchable.disabled);
        setSize(carouselBase.getWidth(), carouselBase.getHeight());
        setOrigin(getWidth() / 2, getHeight() / 2);
        this.gfxLibrary = gfxLibrary;
        this.additionalRegions = new HashMap<>();
    }

    public void showNextFlower(final TextureType textureType) { // TODO change type to Flower
        final Image oldFlower = this.flower;

        TextureRegion flowerTextureRegion;

        if (textureType.isFlipped()){
            flowerTextureRegion = additionalRegions.get(textureType);
            if (flowerTextureRegion == null){
                flowerTextureRegion = new TextureRegion(gfxLibrary.getTextureRegion(textureType));
                flowerTextureRegion.flip(textureType.isFlipped(), false);
                additionalRegions.put(textureType, flowerTextureRegion);
            }
        }else {
            flowerTextureRegion = gfxLibrary.getTextureRegion(textureType);
        }

        this.flower = new Image(flowerTextureRegion);
        addActor(flower);
        flower.setOrigin(getWidth() / 10 * 3, getHeight() / 10 * 4);
        flower.setX(getWidth() / 10 * 2);
        flower.setY(getHeight() / 10);
        flower.setRotation(-getRotation() + 90);

        this.currentTarget = textureType;

        addAction(Actions.sequence(Actions.rotateBy(-90, 0.5f, Interpolation.elasticOut), (oldFlower == null ? Actions.delay(0) : Actions.removeActor(oldFlower))));
    }

    public TextureType getCurrentTarget() {
        return currentTarget;
    }
}
