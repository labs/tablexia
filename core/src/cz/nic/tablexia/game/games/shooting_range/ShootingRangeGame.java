/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.common.media.SfxLibrary;
import cz.nic.tablexia.game.games.shooting_range.actors.Carousel;
import cz.nic.tablexia.game.games.shooting_range.actors.Effect;
import cz.nic.tablexia.game.games.shooting_range.actors.Row;
import cz.nic.tablexia.game.games.shooting_range.actors.Scene;
import cz.nic.tablexia.game.games.shooting_range.actors.Smoke;
import cz.nic.tablexia.game.games.shooting_range.actors.Target;
import cz.nic.tablexia.game.games.shooting_range.actors.Watch;
import cz.nic.tablexia.game.games.shooting_range.media.BoxType;
import cz.nic.tablexia.game.games.shooting_range.media.SoundType;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;
import cz.nic.tablexia.game.games.shooting_range.model.GameState;
import cz.nic.tablexia.game.games.shooting_range.model.Wave;
import cz.nic.tablexia.game.games.shooting_range.tools.HitEvaluator;
import cz.nic.tablexia.game.games.shooting_range.tools.PixelPerfectHitEvaluator;
import cz.nic.tablexia.game.games.shooting_range.tools.TargetGenerator;
import cz.nic.tablexia.game.games.shooting_range.tools.TargetPositionController;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

/**
 * Created by lhoracek
 */
public class ShootingRangeGame extends AbstractTablexiaGame<GameState> {
    public static final String GAME_SCENE = "game_scene";
    public static final String CAROUSEL = "carousel";


    public static final String LABEL_SCORE = "label score";



    private static final ApplicationFontManager.FontType        SCORE_COUNTER_TEXT_FONT             = ApplicationFontManager.FontType.BOLD_30;
    private static final Color                                  SCORE_COUNTER_TEXT_COLOR            = Color.WHITE;
    private static final Color                                  ANIMATION_COLOR                     = Color.RED;
    private static final float                                  ANIMATION_DURATION                  = 0.1f;
    private static final int                                    SCORE_BOTTOM_PAD                    = 10;
    private static final int                                    SCORE_HORIZONTAL_PAD                = 50;
    private static final int							        ONE_SECOND_DELAY		   			= 1;
    private static final float							        FLOWER_ANIMATION_DURATION			= 0.35f;
    private static final float							        WATCH_AND_CAROUSEL_HIDE_DURATION	= 0.5f;
    private static final float                                  WATCH_PADDING_LEFT                  = 0.09f;

    private GfxLibrary gfxLibrary = new GfxLibrary(this, TextureType.values());
    private SfxLibrary sfxLibrary = new SfxLibrary(this, SoundType.values());

    private Scene                    scene;
    private Carousel                 carousel;
    private Watch                    watch;
    private TablexiaLabel            score;
    private TargetGenerator          targetGenerator;
    private TargetPositionController targetPositionController;
    private HitEvaluator             hitEvaluator;

    private enum GameEffect{
        SLOW_DOWN,
        SPEED_UP,
        NO_EFFECT
    }

    private GameEffect               currentEffect  = GameEffect.NO_EFFECT;
    private long                     effectEntry    = 0;
    private long                     effectDuration = 0;
    private boolean                  effectInterrupted = false;

    /**
     * Most important listener deciding every hit on target
     */
    private InputListener clickListener = new InputListener() {
        /**
         * Decide on touchDown for fast response
         */
        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if(button == Input.Buttons.RIGHT) return false;
            Target target = (Target) event.getTarget();
            if (!target.isShot()) { // skip if target already hit and let miss listener to catch
                if (target.getTextureType().equals(getData().getCurrentTarget())) {
                    sfxLibrary.getSound(SoundType.HITS[getRandom().nextInt(SoundType.HITS.length)]).play();
                    target.setShot(true);
                    target.addAction(Actions.parallel(Actions.scaleTo(1, 0f, 0.1f)));
                    getData().addHit();
                    setGameScore(Properties.SCORE_HITS, getData().getHits());
                    if (getData().getHitLimit() == 0) {
                        newTarget();
                    }
                } else if (target.getTextureType().equals(TextureType.BOX_BAD)) {
                    target.setShot(true);
                    getData().addBoxBad();
                    setGameScore(Properties.SCORE_BAD_BOXES, getData().getBoxesBadHit());
                    target.addAction(Actions.scaleTo(1, 0, 0.1f));
                    switch (BoxType.BAD_BOXES[getRandom().nextInt(BoxType.BAD_BOXES.length)]) {
                        case SPEED_UP:
                            if (currentEffect == GameEffect.SPEED_UP) {
                                effectDuration += Properties.GAME_SPEED_TIMEOUT;
                            } else {
                                currentEffect = GameEffect.SPEED_UP;
                                effectEntry = TimeUtils.millis();
                                effectDuration = Properties.GAME_SPEED_TIMEOUT;
                            }
                            getData().setGameSpeed(Properties.GAME_SPEED_FAST);
                            sfxLibrary.getSound(SoundType.BOX_FAST).play();
                            scene.addActor(new Effect(gfxLibrary.getTextureRegion(TextureType.BOX_SPEED_UP), target.getX(), target.getY()));
                            break;
                        case TIME_ADD:
                            getData().addTime(Properties.ADD_TIME);
                            sfxLibrary.getSound(SoundType.BELL).play();
                            scene.addActor(new Effect(gfxLibrary.getTextureRegion(TextureType.BOX_TIME_MINUS_5), target.getX(), target.getY()));
                            break;
                        case CLOUD:
                            scene.addActor(new Smoke(gfxLibrary, target.getX(), target.getY()));
                            sfxLibrary.getSound(SoundType.BOX_SMOKE).play();
                            break;
                        default:
                    }
                } else if (target.getTextureType().equals(TextureType.BOX_GOOD)) {
                    target.setShot(true);
                    getData().addBoxGood();
                    setGameScore(Properties.SCORE_GOOD_BOXES, getData().getBoxesGoodHit());
                    target.addAction(Actions.scaleTo(1, 0, 0.1f));
                    switch (BoxType.GOOD_BOXES[getRandom().nextInt(BoxType.GOOD_BOXES.length)]) {
                        case SLOW_DOWN:
                            if (currentEffect == GameEffect.SLOW_DOWN) {
                                effectDuration = +Properties.GAME_SPEED_TIMEOUT;
                            } else {
                                currentEffect = GameEffect.SLOW_DOWN;
                                effectDuration = Properties.GAME_SPEED_TIMEOUT;
                                effectEntry = TimeUtils.millis();
                            }
                            getData().setGameSpeed(Properties.GAME_SPEED_SLOW);
                            scene.addActor(new Effect(gfxLibrary.getTextureRegion(TextureType.BOX_SPEED_DOWN), target.getX(), target.getY()));
                            sfxLibrary.getSound(SoundType.BOX_SLOW).play();
                            break;
                        case TIME_SUB:
                            getData().addTime(Properties.SUB_TIME);
                            sfxLibrary.getSound(SoundType.BELL).play();
                            scene.addActor(new Effect(gfxLibrary.getTextureRegion(TextureType.BOX_TIME_PLUS_5), target.getX(), target.getY()));
                            break;
                        case HIT:
                            scene.addActor(new Effect(gfxLibrary.getTextureRegion(TextureType.BOX_HIT), target.getX(), target.getY()));
                            sfxLibrary.getSound(SoundType.HITS[getRandom().nextInt(SoundType.HITS.length)]).play();
                            getData().addScore(2);
                            break;
                        default:
                            throw new IllegalStateException("Unknown state");
                    }
                } else {
                    if (!target.hasActions()) {
                        target.addAction(Actions.sequence(Actions.color(ANIMATION_COLOR, ANIMATION_DURATION), Actions.color(Color.WHITE, ANIMATION_DURATION), Actions.color(ANIMATION_COLOR, ANIMATION_DURATION),
                                Actions.color(Color.WHITE, ANIMATION_DURATION), Actions.color(ANIMATION_COLOR, ANIMATION_DURATION), Actions.color(Color.WHITE, ANIMATION_DURATION)));
                        target.addAction(Actions.after(Actions.color(Color.WHITE)));
                    }
                    sfxLibrary.getSound(SoundType.WRONG).play();
                    getData().addWrongTarget();
                    setGameScore(Properties.SCORE_ERRORS, getData().getWrongTargets());
                }
            }
            setGameScore(Properties.SCORE_TOTAL, getData().getScore());
            return super.touchDown(event, x, y, pointer, button);
        }
    };

    private InputListener missInputListener = new InputListener() {
        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if(button == Input.Buttons.RIGHT) return false;
            sfxLibrary.getSound(SoundType.MISS).play();
            getData().addMiss();
            setGameScore(Properties.SCORE_MISSES, getData().getMisses());
            setGameScore(Properties.SCORE_TOTAL, getData().getScore());
            return super.touchDown(event, x, y, pointer, button);
        }
    };

    @Override
    protected void prepareGameSoundAssetNames(List<String> soundsFileNames) {
        soundsFileNames.addAll(sfxLibrary.values());
    }

    @Override
    protected GameState prepareGameData(Map<String, String> gameState) {
        return new GameState();
    }

    @Override
    protected void gameLoaded(Map<String, String> gameState) {
        getStage().setDebugAll(TablexiaSettings.getInstance().isShowBoundingBoxes());
        initScene();

        // Create new hit evaluator
        hitEvaluator = new PixelPerfectHitEvaluator(gfxLibrary, TextureType.getTargetPack(getGameDifficulty())); // need to be done here - getGameDifficulty returns null anywhere before
        // TODO add boxes - hit evaluator is initialized with flowers, but not boxes and could case glitch when evaluating box hit
        addDisposable(hitEvaluator);


        targetGenerator = new TargetGenerator(getGameDifficulty(), getRandom(), gfxLibrary, clickListener, hitEvaluator);
        targetPositionController = new TargetPositionController(scene, targetGenerator, getRandom(), getGameDifficulty(), getStage().getWidth());
        for (Row row : scene.getRows()) {         // init each row with starting flowers
            row.setWave(Wave.getWave(getGameDifficulty(), row.getRowIndex()));
            row.addTargets(targetGenerator.generateTargetRow(row));
        }
        targetPositionController.onUpdate(0); // init flower position
        scene.getBackground().addListener(missInputListener);
    }

    @Override
    protected void gameVisible() {
        scene.addAction(Actions.sequence(Actions.delay(ONE_SECOND_DELAY), Actions.run(new Runnable() {
			@Override
			public void run() {
				for (Row row : scene.getRows()) {
					row.addActionToFlowers(Actions.sequence(Actions.show(), Actions.scaleTo(1, 1, FLOWER_ANIMATION_DURATION)));
				}
                float carouselX = getSceneRightX()-carousel.getWidth()/2;
                float watchX = getViewportWidth() * WATCH_PADDING_LEFT;
                carousel.addAction(Actions.sequence(Actions.moveTo(carouselX, getViewportTopY()+carousel.getHeight()/2), Actions.show(),Actions.moveTo(carouselX, getViewportTopY()-carousel.getHeight()/2, WATCH_AND_CAROUSEL_HIDE_DURATION)));
                watch.addAction(Actions.sequence(Actions.moveTo(watchX, getViewportBottomY()-watch.getWatchHeight()), Actions.show(), Actions.moveTo(watchX,getViewportBottomY(),WATCH_AND_CAROUSEL_HIDE_DURATION)));
			}
		}), Actions.delay(ONE_SECOND_DELAY), Actions.run(new Runnable() {
			@Override
			public void run() {
				scene.setTouchable(Touchable.enabled);
				newTarget();
				getData().setRunning(true);
			}
		})));

        AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_GAME_READY);
    }

    @Override
    protected void gamePaused(Map<String, String> gameState) {
        //save remaining effect time. if any
        if (currentEffect != GameEffect.NO_EFFECT || effectDuration > TimeUtils.timeSinceMillis(effectEntry)) {
            effectDuration = effectDuration - TimeUtils.timeSinceMillis(effectEntry);
            effectInterrupted = true;
        }
        super.gamePaused(gameState);
    }

    @Override
    protected void gameResumed() {
        //restore remaining effect time, if any
        if (currentEffect != GameEffect.NO_EFFECT) {
            effectEntry = TimeUtils.millis();
            effectInterrupted = false;
        }
        super.gameResumed();
    }

    private void repositionUIElements() {
        scene.setBounds(0, getViewportBottomY(), getViewportWidth(), getViewportHeight()); // scaling viewport camera y-position adjustment
        watch.setPosition(getViewportWidth() * WATCH_PADDING_LEFT, getViewportBottomY());
        carousel.setPosition(getViewportWidth() - (carousel.getWidth() / 2), getViewportTopY() - (carousel.getHeight() / 2));
        score.setPosition(getViewportWidth() - score.getWidth() - SCORE_HORIZONTAL_PAD, getViewportBottomY() + SCORE_BOTTOM_PAD);
    }

    /**
     * Based on game difficulty create random hit limit for current target
     * Current it 1-4 hit for all difficulties
     *
     * @return hitLimit
     */
    private int getNewHitLimit() {
        return getRandom().nextInt(4) + 1;
    }

    /**
     * Generate new target and hit limit a show it in carousel
     */
    private void newTarget() {
        getData().setHitLimit(getNewHitLimit());
        getData().setCurrentTarget(getRandomFlower(getData().getCurrentTarget()));
        carousel.showNextFlower(getData().getCurrentTarget());
        targetPositionController.setTargetType(getData().getCurrentTarget());
    }

    /**
     * Get random flower from target bag.
     *
     * @return TextureType of flower
     */
    private TextureType getRandomFlower(final TextureType previousTextureType) {
		Set<TextureType> flowers = targetPositionController.getAvailableTypes();

		flowers.remove(previousTextureType);
        if (flowers.size() > 0) {
            TextureType[] types = flowers.toArray(new TextureType[0]);
            return types[getRandom().nextInt(types.length)];
        }
        return targetGenerator.getRandomFlowerType();
    }

    /**
     * Called in each tick of rendering engine. Updates position of targets and passed game time
     *
     * @param delta delta time
     */
    @Override
    protected void gameRender(float delta) {
        if (!isScreenPaused()) {
            if (getData().isRunning()) {
                if (getData().getTime() >= Properties.GAME_TIME) {
                    finishGame();
                }
                getData().addTime(delta);
                targetPositionController.onUpdate(delta * getData().getGameSpeed());
                if (currentEffect != GameEffect.NO_EFFECT && TimeUtils.timeSinceMillis(effectEntry) > effectDuration && !effectInterrupted) {
                    //back to normal
                    currentEffect = GameEffect.NO_EFFECT;
                    effectDuration = 0;
                    effectEntry = 0;
                    getData().setGameSpeed(Properties.GAME_SPEED_NORMAL);
                }
            }
            watch.setTime((int) getData().getTime());
            score.setText(String.valueOf(getData().getScore()));
        }
    }

    private void finishGame() {
        getData().setRunning(false);
        scene.setTouchable(Touchable.disabled);
        sfxLibrary.getSound(SoundType.BELL).play();

		// elements hide animations
		for (Row row : scene.getRows()) {
			row.addActionToFlowers(Actions.sequence(Actions.scaleTo(1, 0, FLOWER_ANIMATION_DURATION), Actions.removeActor()));
		}
		carousel.addAction(Actions.sequence(Actions.moveBy(0, carousel.getHeight(), WATCH_AND_CAROUSEL_HIDE_DURATION), Actions.removeActor()));
		watch.addAction(Actions.sequence(Actions.delay(WATCH_AND_CAROUSEL_HIDE_DURATION), Actions.moveBy(0, -watch.getWatchHeight(), WATCH_AND_CAROUSEL_HIDE_DURATION), new RunnableAction() {
			@Override
			public void run() {
				gameComplete();
			}
		}));

    }

    /**
     * Create scene with watch, carousel and background
     */
    private void initScene() {
        getStage().addActor(scene = new Scene(gfxLibrary));
        getStage().addActor(watch = new Watch(gfxLibrary));
        watch.setScale(0.4f);
        getStage().addActor(carousel = new Carousel(gfxLibrary));
        carousel.setScale(0.6f);
        TablexiaLabel.TablexiaLabelStyle labelStyle = new TablexiaLabel.TablexiaLabelStyle(SCORE_COUNTER_TEXT_FONT, SCORE_COUNTER_TEXT_COLOR);
        getStage().addActor(score = new TablexiaLabel("0", labelStyle));
        repositionUIElements();
        carousel.setVisible(false);
        watch.setVisible(false);
        scene.setName(GAME_SCENE);
        score.setName(LABEL_SCORE);
        carousel.setName(CAROUSEL);
    }

    /**
     * Clears libgdx boxes with targets
     */
    private void resetBoxes() {
        if (scene != null && scene.getRows() != null)
            for(int i = 0; i < scene.getRows().length; i++) {
                scene.getRows()[i].clear();
            }
    }

    /**
     * Recompute carousel and watch position on screen resize
     *
     * @param width
     * @param height
     */
    @Override
    protected void screenResized(int width, int height) {
        super.screenResized(width, height);
        repositionUIElements();
        targetPositionController.updateWidth();
    }

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(new SummaryMessage(SummaryImage.STATS, getFormattedText(Properties.RESULT_TEXT_SUMMARY, game.getGameScore(Properties.SCORE_TOTAL, "0"))));

    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        return Properties.RESULT_TEXT[gameResult.getStarCount()];
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        return Properties.RESULT_SOUNDS[gameResult.getStarCount()];
    }


//////////////////////////////////////////// PRELOADER

    private static final String                             PRELOADER_INFO_IMAGE            = "preloader_info";
    private static final String                             PRELOADER_ANIM_IMAGE            = "preloader_anim";
    private static final int                                PRELOADER_ANIM_FRAMES           = 8;
    private static final float                              PRELOADER_ANIM_FRAME_DURATION   = 0.5f;
    private static final String                             PRELOADER_TEXT1_KEY             = ApplicationTextManager.ApplicationTextsAssets.GAME_SHOOTING_RANGE_PRELOADER_TEXT1;
    private static final String                             PRELOADER_TEXT2_KEY             = ApplicationTextManager.ApplicationTextsAssets.GAME_SHOOTING_RANGE_PRELOADER_TEXT2;

    private static final Scaling                            PRELOADER_IMAGE_SCALING             = Scaling.fit;
    private static final int 			                    PRELOADER_TEXT_ALIGN 				= Align.left;
    private static final float 			                    PRELOADER_TEXT_PADDING 				= 10f;
    private static final float 			                    PRELOADER_LEFT_COLUMN_WIDTH_RATIO 	= 1f / 4;
    private static final float 			                    PRELOADER_RIGHT_COLUMN_RATIO 		= 5f / 8;
    private static final float 			                    PRELOADER_ROW_HEIGHT 				= 1f / 3;

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        AnimatedImage preloaderImage1 = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_IMAGE, PRELOADER_ANIM_FRAMES, PRELOADER_ANIM_FRAME_DURATION), false);
        preloaderImage1.startAnimationLoop();
        String preloaderText1 = preloaderAssetsManager.getText(PRELOADER_TEXT1_KEY);
        components.add(new TwoColumnContentDialogComponent(
                new AnimatedImageContentDialogComponent(preloaderImage1, PRELOADER_IMAGE_SCALING),
                new TextContentDialogComponent(preloaderText1, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_ROW_HEIGHT));

        components.add(new FixedSpaceContentDialogComponent());

        Image preloaderImage2 = new Image(preloaderAssetsManager.getTextureRegion(PRELOADER_INFO_IMAGE));
        String preloaderText2 = preloaderAssetsManager.getText(PRELOADER_TEXT2_KEY);
        components.add(new TwoColumnContentDialogComponent(
                new TextContentDialogComponent(preloaderText2, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                new ImageContentDialogComponent(preloaderImage2, PRELOADER_IMAGE_SCALING),
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_ROW_HEIGHT));
    }

    //////////////////////////////////////////// DEBUG - FORCE GAME END

    @Override
    protected void onForceGameEnd(GameResultDefinition gameResult) {
        finishGame();
    }


}
