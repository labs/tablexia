/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.actors;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;
import cz.nic.tablexia.game.games.shooting_range.model.Wave;

/**
 * Created by lhoracek on 6/23/15.
 */
public class Row extends Group {

	private static final float INITIAL_ROW_SCALE = 0.1f;
    private static final float ROW_OFFSET_Y = 50;

    private Image waveImage;
    private Group targetGroup;
    private List<Target> targets = new CopyOnWriteArrayList<Target>();
    private Wave wave;
    private int  rowIndex;

    public Row(GfxLibrary gfxLibrary, int index, TextureType texture) {
        addActor(targetGroup = new Group());
        addActor(waveImage = new Image(gfxLibrary.getTextureRegion(texture)));
        rowIndex = index;
        setTouchable(Touchable.childrenOnly);
        targetGroup.setTouchable(Touchable.childrenOnly);
        waveImage.setTouchable(Touchable.disabled);

		// we start with stretched flowers to be able to run pop up animation on them
		targetGroup.setScaleY(INITIAL_ROW_SCALE);
    }

    public void setWave(Wave wave) {
        this.wave = wave;
    }

    public Wave getWave() {
        return wave;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void addTargets(Collection<Target> targets) {
        for (Target target : targets) {
            addTarget(target, 0);
        }
    }

    public void addTarget(Target target, float elapsedTime) {
        targets.add(target);
        float newX = getWave().getX(elapsedTime - target.getStartTime(), getWidth());
        float newY = getHeight() - ROW_OFFSET_Y + getWave().getY(elapsedTime - target.getStartTime(), getHeight());
        target.setPosition(newX, newY);
        targetGroup.addActor(target);

    }

    public void removeTarget(Target target) {
        targets.remove(target);
        targetGroup.removeActor(target);
    }

    public List<Target> getTargets() {
        return targets;
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        waveImage.setSize(getWidth(), getHeight());
        targetGroup.setSize(getWidth(), getHeight());
    }

	public void addActionToFlowers(Action action) {
		targetGroup.addAction(action);
	}
}
