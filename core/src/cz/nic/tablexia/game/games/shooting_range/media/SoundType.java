/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.media;

import cz.nic.tablexia.game.common.media.AssetDescription;

/**
 * @author lhoracek
 */
public enum SoundType implements AssetDescription {

    BELL("bell"), //
    CAROUSEL_1("carousel_1"), //
    CAROUSEL_2("carousel_2"), //
    SWOOSH("swoosh"), //
    BOX_FAST("box_fast"),//
    BOX_SLOW("box_slow"), //
    BOX_SMOKE("box_smoke"), //
    HIT_1("hit_1"), //
    HIT_2("hit_2"), //
    HIT_3("hit_3"), //
    HIT_4("hit_4"), //
    MISS("miss"), //
    WRONG("wrong"); //

    public static final SoundType[] CAROUSELS = {CAROUSEL_1, CAROUSEL_2};
    public static final SoundType[] HITS      = {HIT_1, HIT_2, HIT_3, HIT_4};

    private SoundType(String resource) {
        this.resource = resource;
    }

    private final String resource;

    public String getResource() {
        return resource;
    }
}
