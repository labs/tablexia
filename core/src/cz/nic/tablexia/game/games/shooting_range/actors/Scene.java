/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.actors;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;

/**
 * Created by lhoracek on 6/23/15.
 */
public class Scene extends Group {
    private final Image frame;
    private final Image background;
    private       Row   row1, row2, row3;
    private Row[] rows;


    public Scene(GfxLibrary gfxLibrary) {
        addActor(background = new TablexiaNoBlendingImage(gfxLibrary.getTextureRegion(TextureType.BACKGROUND)));

        addActor(row1 = new Row(gfxLibrary, 0, TextureType.WAVE_1));
        addActor(row2 = new Row(gfxLibrary, 1, TextureType.WAVE_2));
        addActor(row3 = new Row(gfxLibrary, 2, TextureType.WAVE_3));
        rows = new Row[]{row1, row2, row3};

        addActor(frame = new Image(gfxLibrary.getTextureRegion(TextureType.FRAME)));
        frame.setTouchable(Touchable.disabled);
        setTouchable(Touchable.disabled);
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        background.setSize(getWidth(), getHeight());
        frame.setSize(getWidth(), getHeight());

        row1.setSize(getWidth(), getHeight() / 10 * 7);
        row2.setSize(getWidth(), getHeight() / 10 * 5);
        row3.setSize(getWidth(), getHeight() / 10 * 3);
    }

    public Row[] getRows() {
        return rows;
    }

    public Image getBackground() {
        return background;
    }
}
