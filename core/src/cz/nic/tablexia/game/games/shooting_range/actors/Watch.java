/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.actors;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;

/**
 * Created by lhoracek on 6/23/15.
 */
public class Watch extends Group {
    private Image watch, watchHand;

    public Watch(GfxLibrary gfxLibrary) {
        addActor(watch = new Image(gfxLibrary.getTextureRegion(TextureType.WATCH)));
        addActor(watchHand = new Image(gfxLibrary.getTextureRegion(TextureType.WATCH_HAND)));
        watchHand.setPosition(254, 166);
        watchHand.setOrigin(watchHand.getWidth() / 2, 15);
        setTouchable(Touchable.disabled);
    }

    public void setTime(int time) {
        int newRotation = (time % 60) * -6;
        watchHand.addAction(Actions.rotateTo(newRotation, Math.abs(watch.getRotation() - newRotation) * 0.002f, Interpolation.elasticOut));
    }

	public float getWatchHeight() {
		return watch.getHeight();
	}
}
