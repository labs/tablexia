/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.actors;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;

/**
 * Created by lhoracek on 6/23/15.
 */
public class Smoke extends Effect {

    private static final TextureType[] textures = new TextureType[]{TextureType.BOX_SMOKE_1, TextureType.BOX_SMOKE_2, TextureType.BOX_SMOKE_3, TextureType.BOX_SMOKE_4, TextureType.BOX_SMOKE_5};

    public Smoke(GfxLibrary gfxLibrary, float x, float y) {
        init();
        for (int i = 0; i < textures.length; i++) {
            Image image = new Image(gfxLibrary.getTextureRegion(textures[i]));
            setSize(Math.max(getWidth(), image.getWidth()), Math.max(getHeight(), image.getHeight()));
            addActor(image);
            image.setVisible(false);
            image.addAction(Actions.sequence(Actions.delay(0.6f * i), Actions.visible(true), Actions.alpha(0), Actions.fadeIn(0.2f), Actions.delay(0.3f), Actions.fadeOut(0.2f)));
        }
        addAction(Actions.sequence(Actions.delay(5), Actions.removeActor()));
        setPosition(x - getWidth() / 2, y);
    }

    @Override
    protected void init() {
        // nothing
    }
}
