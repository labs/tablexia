/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.actors;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.games.shooting_range.media.TextureType;
import cz.nic.tablexia.game.games.shooting_range.tools.HitEvaluator;

/**
 * Created by lhoracek on 6/22/15.
 */
public class Target extends Image {
    private final float       startTime;
    private       TextureType textureType;
    private boolean shot = false;
    private Sprite       effect;
    private Row          row;
    private HitEvaluator hitEvaluator;
    private boolean flipped;


    public static interface PositionProvider {
        public float getY(float elapseTime, float maxHeight);

        public static final PositionProvider PROVIDER_FLAT = new PositionProvider() {
            @Override
            public float getY(float elapsedTime, float maxHeight) {
                return 0;
            }
        };
    }
    public Target(TextureRegion texture, Float startTime, Row row, TextureType textureType, InputListener listener, HitEvaluator hitEvaluator) {
        this(texture, startTime, row, textureType, listener, hitEvaluator, false);
    }

    public Target(TextureRegion texture, Float startTime, Row row, TextureType textureType, InputListener listener, HitEvaluator hitEvaluator, boolean flipped) {
        super(texture);
        this.startTime = startTime;
        this.textureType = textureType;
        this.row = row;
        addListener(listener);
        this.hitEvaluator = hitEvaluator;
        this.flipped = flipped;
    }

    public float getStartTime() {
        return startTime;
    }

    public void setTextureType(TextureType textureType) {
        this.textureType = textureType;
    }

    public TextureType getTextureType() {
        return textureType;
    }

    public Row getRow() {
        return row;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + Float.floatToIntBits(startTime);
        result = (prime * result) + ((textureType == null) ? 0 : textureType.hashCode());
        result = (prime * result) + ((row == null) ? 0 : row.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Target other = (Target) obj;
        if (Float.floatToIntBits(startTime) != Float.floatToIntBits(other.startTime)) {
            return false;
        }
        if (textureType != other.textureType) {
            return false;
        }
        if (row != other.row) {
            return false;
        }
        return true;
    }

    public boolean isShot() {
        return shot;
    }

    public void setShot(boolean shot) {
        this.shot = shot;
    }

    public Sprite getEffect() {
        return effect;
    }

    public void setEffect(Sprite effect) {
        this.effect = effect;
    }


    @Override
    public String toString() {
        return "Target{" +
                "textureType=" + textureType +
                ", shot=" + shot +
                '}';
    }

}
