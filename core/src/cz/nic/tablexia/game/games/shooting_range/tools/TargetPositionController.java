/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.tools;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.shooting_range.actors.Row;
import cz.nic.tablexia.game.games.shooting_range.actors.Scene;
import cz.nic.tablexia.game.games.shooting_range.actors.Target;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;
import cz.nic.tablexia.game.games.shooting_range.model.Direction;

/**
 * Created by lhoracek on 7/2/15.
 */
public class TargetPositionController {

    private float elapsedTime = 0;
    private final TargetGenerator targetGenerator;
    private final Random          random;
    private float                 width;
    private final Scene           scene;
    private       TextureType     targetType;
    private       GameDifficulty  gameDifficulty;
    private static final float ROW_OFFSET_Y = 50;
    Set<TextureType> availableTypes = new HashSet<TextureType>();

    public TargetPositionController(Scene scene, TargetGenerator targetGenerator, Random random, GameDifficulty gameDifficulty, float width) {
        super();
        this.scene = scene;
        this.targetGenerator = targetGenerator;
        this.random = random;
        this.width = width;
        this.gameDifficulty = gameDifficulty;
    }


    public void setTargetType(TextureType targetType) {
        this.targetType = targetType;
    }

    public Set<TextureType> getAvailableTypes() {
        return availableTypes;
    }

    /*
             * (non-Javadoc)
             * @see org.andengine.engine.handler.IUpdateHandler#onUpdate(float)
             */
    public void onUpdate(float pSecondsElapsed) {
        elapsedTime += pSecondsElapsed;
        Set<TextureType> availableTypes = new HashSet<TextureType>();
        List<Target> invisibles = new ArrayList<Target>();
        for (Row row : scene.getRows()) {
            for (Target target : row.getTargets()) {
                float x = row.getWave().getX(elapsedTime - target.getStartTime(), width);
                // is target in scene?
                if (((x + target.getWidth()) > 0) && (x < width)) {
                    // is last target in row
                    if (targetGenerator.getLastTargetInRow(row).equals(target)) {
                        // last invisible target is on scene - add new one
                        List<TextureType> textures = TextureType.getTargetPack(gameDifficulty);
                        Target newTarget = targetGenerator.addNewTargetToRow(row, textures.get(random.nextInt(textures.size())));
                        // init newly added targets position
                        row.addTarget(newTarget, elapsedTime);

                    }
                    target.setX(x);
                    target.setY(row.getHeight() - target.getHeight()/4 - ROW_OFFSET_Y + row.getWave().getY(elapsedTime - target.getStartTime(), row.getHeight()));

                    // is target in first half of its way? to get available target types not immediately leaving screen
                    if ((x > (Direction.RIGHT == row.getWave().getDirection() ? 0 : width / 2)) && (x < (Direction.RIGHT == row.getWave().getDirection() ? width / 2 : width))) {
                        // only not hit
                        if (!target.isShot()) {
                            availableTypes.add(target.getTextureType());
                        }
                    }
                    // target is not on scene
                } else {
                    if (((row.getWave().getDirection() == Direction.LEFT) && (x < 0)) || ((row.getWave().getDirection() == Direction.RIGHT) && (x > width))) {
                        // gone out of screen
                        row.removeTarget(target);
                    } else {
                        // coming to the screen
                        invisibles.add(target);
                        availableTypes.add(target.getTextureType()); // TODO count available to make them more available or select tartgetType, tha has multiple occurences
                    }
                }
            }
        }

        if ((targetType != null) && !availableTypes.contains(targetType)) {
            if (invisibles.size() > 0) {
                int index = random.nextInt(invisibles.size());
                Target swap = invisibles.get(index);
                Target swapTo = targetGenerator.addNewTargetToRow(swap.getRow(), targetType, swap.getStartTime(), targetGenerator.getLastTargetInRow(swap.getRow()).equals(swap));
                swap.getRow().addTarget(swapTo, elapsedTime);
                swap.getRow().removeTarget(swap);
            } else {
                Row row = scene.getRows()[random.nextInt(scene.getRows().length)];
                row.addTarget(targetGenerator.addNewTargetToRow(row, targetType), elapsedTime);
            }
        }

        for (TextureType box : TextureType.BOXES) {
            availableTypes.remove(box);
        }

        this.availableTypes = availableTypes;
    }

    public void updateWidth() {
        this.width = scene.getWidth();
        onUpdate(0);
    }
}
