/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.actions;

import com.badlogic.gdx.scenes.scene2d.actions.RelativeTemporalAction;

/**
 * Created by lhoracek on 6/26/15.
 */
public class HitAction extends RelativeTemporalAction {
    private static final float HIT_Y = 50;

    protected void updateRelative(float percentDelta) {
        target.setOrigin(target.getOriginX(), target.getOriginY() + HIT_Y * percentDelta);
    }

}
