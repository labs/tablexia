/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.games.shooting_range.actors.Target;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;

/**
 * Created by lhoracek on 7/14/15.
 */
public class PixelPerfectHitEvaluator implements HitEvaluator {
    private Map<Texture, Pixmap> pixmapCache = new HashMap<Texture, Pixmap>();

    @Override
    public void dispose() {
        for (Pixmap pixmap : pixmapCache.values()) {
            pixmap.dispose();
        }
    }

    public PixelPerfectHitEvaluator(GfxLibrary gfxLibrary, List<TextureType> targetTypes) {
        init(gfxLibrary, targetTypes);
    }

    private void init(GfxLibrary gfxLibrary, List<TextureType> targetTypes) {
        for (TextureType targetType : targetTypes) {
            getPixmap(gfxLibrary.getTextureRegion(targetType).getTexture());
        }
    }

    private Pixmap getPixmap(Texture texture) {
        if (pixmapCache.get(texture) == null) {
            texture.getTextureData().prepare();
            Pixmap pixmap = texture.getTextureData().consumePixmap();
            pixmapCache.put(texture, pixmap);
        }
        return pixmapCache.get(texture);
    }

    @Override
    public boolean hit(Target target, float x, float y) {
        // Pixel perfect hit decision
        if (!target.isShot()) {
            TextureRegion textureRegion = ((TextureRegionDrawable) target.getDrawable()).getRegion();
            Texture texture = textureRegion.getTexture();
            Pixmap pixmap = getPixmap(texture);
            int pixel = pixmap.getPixel(textureRegion.getRegionX() + (int) x, textureRegion.getRegionY() + textureRegion.getRegionHeight() - (int) y);
            if (new Color(pixel).a != 0) {
                return true;
            }
        }
        return false;
    }
}
