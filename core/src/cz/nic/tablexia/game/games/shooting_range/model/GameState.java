/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.shooting_range.model;

import cz.nic.tablexia.game.games.shooting_range.Properties;
import cz.nic.tablexia.game.games.shooting_range.media.TextureType;

/**
 * Created by lhoracek on 6/22/15.
 */
public class GameState {

    private boolean running = false;

    private int   score        = 0;
    private int   hits         = 0;
    private int   misses       = 0;
    private int   wrongTargets = 0;
    private int   boxesBadHit  = 0;
    private int   boxesGoodHit = 0;
    private float time         = 0f;
    private TextureType currentTarget;
    private int   hitLimit  = 0;
    private float gameSpeed = Properties.GAME_SPEED_NORMAL;

    public int getHitLimit() {
        return hitLimit;
    }

    public void setGameSpeed(float gameSpeed) {
        this.gameSpeed = gameSpeed;
    }

    public void setHitLimit(int limit) {
        this.hitLimit = limit;
    }

    public int getScore() {
        return score;
    }

    public void addScore(int diff) {
        score += diff;
    }

    public void addMiss() {
        misses++;
        addScore(-1);
    }

    public void addWrongTarget() {
        wrongTargets++;
        addScore(-1);
    }

    public void addBoxGood() {
        boxesGoodHit++;
    }

    public void addBoxBad() {
        boxesBadHit++;
    }

    public int getHits() {
        return hits;
    }

    public void addHit() {
        this.hits++;
        addScore(2);
        hitLimit--;
    }

    public int getMisses() {
        return misses;
    }

    public int getWrongTargets() {
        return wrongTargets;
    }

    public int getBoxesBadHit() {
        return boxesBadHit;
    }

    public int getBoxesGoodHit() {
        return boxesGoodHit;
    }

    public float getTime() {
        return time;
    }

    public float getGameSpeed() {
        return gameSpeed;
    }

    public void addTime(float delta) {
        time = Math.max(0, Math.min(Properties.GAME_TIME, time + (delta * gameSpeed)));
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public TextureType getCurrentTarget() {
        return currentTarget;
    }

    public void setCurrentTarget(TextureType currentTarget) {
        this.currentTarget = currentTarget;
    }
}
