/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.model;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Václav Tarantík on 11.8.15.
 */
public class Vehicle extends RotatedAndDraggedImage {
    public Vehicle(TextureRegion textureRegion,  cz.nic.tablexia.util.Point position, cz.nic.tablexia.util.Point size) {
        super(textureRegion);
        setSize(size.x, size.y);
        setPosition(position.x, position.y);
        setOrigin(size.x / 2, size.y / 2);
    }
}
