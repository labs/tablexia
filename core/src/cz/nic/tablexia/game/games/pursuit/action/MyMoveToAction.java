/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.action;

import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;

import cz.nic.tablexia.util.Log;

/**
 * Created by Václav Tarantík on 26.10.15.
 */
public class MyMoveToAction extends MoveToAction implements Interruptable {
    private boolean interrupted;

    public MyMoveToAction(float x, float y, float duration){
        setPosition(x, y);
        setDuration(duration);
    }
    @Override
    protected void update(float percent) {
        if(!interrupted){
            super.update(percent);
        }
    }

    private void setInterrupted(boolean interrupted) {
        this.interrupted = interrupted;
    }

    @Override
    public void interrupt() {
        Log.info(getClass(),"Interrupting action");
        setInterrupted(true);
    }
}
