/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.assets;

/**
 * Created by Václav Tarantík on 18.6.15.
 */
public class PursuitAssets {

    public static final String SOUNDS_PATH      = "common/sfx/";
    public static final String MP3_EXTENSION    = ".mp3";
    public static final String MAPS_DIR         = "excluded/";
    public static final String VEHICLES_DIR     = "vehicles/";
    public static final String BACKGROUND       = "background2";

    public static final String FINISH_FLAG = VEHICLES_DIR + "flag";
    public static final String EMPTY_PUZZLE = "empty_puzzle";

    //PIECES SOUNDS
    public static final String SOUND_ROTATION = SOUNDS_PATH+"rotate"+MP3_EXTENSION;
    public static final String SOUND_SWITCH = SOUNDS_PATH+"switch"+MP3_EXTENSION;
    public static final String SOUND_LIFT = SOUNDS_PATH+"lift"+MP3_EXTENSION;

    //VEHICLE SOUNDS
    public static final String SOUND_CAR = SOUNDS_PATH + "car" + MP3_EXTENSION;
    public static final String SOUND_TRAIN = SOUNDS_PATH + "train" + MP3_EXTENSION;
    public static final String SOUND_BOAT = SOUNDS_PATH + "boat" + MP3_EXTENSION;
    public static final String SOUND_MOTORCYCLE = SOUNDS_PATH + "motorcycle" + MP3_EXTENSION;

	//OTHER SOUNDS
    public static final String SOUND_HELP =  SOUNDS_PATH+"help"+MP3_EXTENSION;
    public static final String VICTORYSPEECH_NOSTAR =  SOUNDS_PATH+"result_0"+MP3_EXTENSION;
    public static final String VICTORYSPEECH_ONESTAR =  SOUNDS_PATH+"result_1"+MP3_EXTENSION;
    public static final String VICTORYSPEECH_TWOSTAR =  SOUNDS_PATH+"result_2"+MP3_EXTENSION;
    public static final String VICTORYSPEECH_THREESTAR =  SOUNDS_PATH+"result_3"+MP3_EXTENSION;

    //TEXT+
    public static final String TEXT_HELP = "game_pronasledovani_loadingtext";
    public static final String VICTORYTEXT_NOSTAR = "game_pronasledovani_victorytext_gameover";
    public static final String VICTORYTEXT_ONESTAR = "game_pronasledovani_victorytext_onestar";
    public static final String VICTORYTEXT_TWOSTARS = "game_pronasledovani_victorytext_twostars";
    public static final String VICTORYTEXT_THREESTARS = "game_pronasledovani_victorytext_threestars";

}
