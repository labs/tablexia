/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.helper;

import java.util.Random;

import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Point;

/**
 * Created by Václav Tarantík on 2.7.15.
 */
public class ArithmeticsHelper {
    private static final float SUFFICIENT_ROTATION_AMOUNT_DEGREES = 15;
    public static final int CARDINAL_POINTS_COUNT = 4;

    public static double getAngleBetweenTwoPoints(Point firstPoint, Point secondPoint) {
        double degrees = Math.atan2(secondPoint.x - firstPoint.x, secondPoint.y - firstPoint.y);
        degrees = (degrees > 0 ? degrees : (2 * Math.PI + degrees)) * 360 / (2 * Math.PI);
        double angleInsideThreeSixty = (float) degrees % 360;
        double recalcAngle = 360 - angleInsideThreeSixty;
        return recalcAngle;
    }

    /*
    calculates the angle according to rotation direction, if rotation of piece exceeds the
    SUFFICIENT_ROTATION_AMOUNT_DEGREES, piece will be rotated to closest right angle after drop
     */
    public static float getAngleInRotationDirection(float originalRotation, float currentAngle) {
        float rotationAmount = currentAngle - originalRotation;
        int numOfNinetyDegreesPassed = (int) (rotationAmount / 90);
        //original angle added with number of ninety degrees which we rotated piece over
        float baseAngle = originalRotation + (numOfNinetyDegreesPassed * 90);
        if ((Math.abs(rotationAmount) % 90) > SUFFICIENT_ROTATION_AMOUNT_DEGREES) {
            baseAngle+= (getRotationDirection(originalRotation, currentAngle) * 90);
        }
        return baseAngle;
    }

    public static float getClosestRightAngle(float currentAngle) {
        int numOfNinetyDegreesInAngle = (int)(currentAngle/90);
        float angleModule = currentAngle % 90;
        float highestLowerRightAngle =  numOfNinetyDegreesInAngle*90;
        if(currentAngle<0){
            return angleModule<-45?highestLowerRightAngle-90:highestLowerRightAngle;
        }else{
            return angleModule>45?highestLowerRightAngle+90:highestLowerRightAngle;
        }
    }

    //returns right angle inside 360 degrees
    public static float getRandomRightAngle(Random random) {
        return 90 * (random.nextInt(CARDINAL_POINTS_COUNT));
    }

    // (xi-x)^2 + (yi-y)^2 <= d ^2
    public static boolean isPointWithinTheCircle(Point p,Point actorCenter, int circleRadius) {
        if ((Math.pow(p.x-actorCenter.x, 2) + Math.pow(p.y-actorCenter.y, 2)) <= Math.pow(circleRadius, 2)) {
            return true;
        }
        return false;
    }
    
    public static Point getPointOnCircle(Point circleCenter, float circleRadius, float degrees){
        return new Point((float) (circleCenter.x + circleRadius * Math.cos(Math.toRadians(degrees))), (float) (circleCenter.y + circleRadius * Math.sin(Math.toRadians(degrees))));
    }

    /*
 * Translation of coordinates on the square area according to the area rotation
 * (x'-xc) = Kc*(x-xc) - Ks*(y-yc)
 * (y'-yc) = Ks*(x-xc) + Kc*(y-yc)
 */
    public static Point translatePositionAccordingToRotation(Point zeroRotationPosition, float rotation, Point rotationCenter) {
        //rotation = 360 - (rotation % 360);
        rotation = (float) Math.toRadians(rotation);

        float x = (float) (Math.cos(rotation) * (zeroRotationPosition.x - rotationCenter.x)) - (float) (Math.sin(rotation) * (zeroRotationPosition.y - rotationCenter.y));
        x += rotationCenter.x;
        float y = (float) (Math.sin(rotation) * (zeroRotationPosition.x - rotationCenter.x)) + (float) (Math.cos(rotation) * (zeroRotationPosition.y - rotationCenter.y));
        y += rotationCenter.y;

        return new Point(x, y);
    }

    public static int getRotationDirection(float originalRotation, float currentRotation){
        return (currentRotation<originalRotation)?-1:1;
    }

}
