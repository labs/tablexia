/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.model;

import cz.nic.tablexia.game.games.pursuit.helper.PursuitPositionDefinition;
import cz.nic.tablexia.shared.model.definitions.INumberedDefinition;
import cz.nic.tablexia.util.Point;

/**
 * Created by Václav Tarantík on 10.8.15.
 */
public enum Maps implements INumberedDefinition{
    MAP1(0, 8, "map1", new Point(0.866f, 0.1f), new Point(0.26f, 0.85f), PursuitPositionDefinition.MAP_1, VehicleType.CAR),
    MAP2(1, 14, "map2", new Point(0.64f, 0.11f), new Point(0.22f, 0.75f), PursuitPositionDefinition.MAP_2, VehicleType.BOAT),
    MAP3(2, 1, "map3", new Point(0.08f, 0.41f), new Point(0.79f, 0.71f), PursuitPositionDefinition.MAP_3, VehicleType.MOTORCYCLE),
    MAP4(3, 11, "map4", new Point(0.95f, 0.33f), new Point(0.18f, 0.57f), PursuitPositionDefinition.MAP_4, VehicleType.CAR),
    MAP5(4, 6, "map5",new Point(0.94f, 0.12f), new Point(0.86f, 0.82f), PursuitPositionDefinition.MAP_5, VehicleType.CAR),
    MAP6(5, 5, "map6",new Point(0.95f, 0.17f), new Point(0.45f, 0.32f), PursuitPositionDefinition.MAP_6, VehicleType.MOTORCYCLE),
    MAP7(6, 10, "map7",new Point(0.56f, 0.07f), new Point(0.36f, 0.83f), PursuitPositionDefinition.MAP_7, VehicleType.TRAIN),
    MAP8(7, 13, "map8",new Point(0.16f, 0.19f), new Point(0.83f, 0.79f), PursuitPositionDefinition.MAP_8, VehicleType.MOTORCYCLE),
    MAP9(8, 4, "map9",new Point(0.15f, 0.11f), new Point(0.81f, 0.83f), PursuitPositionDefinition.MAP_9, VehicleType.MOTORCYCLE),
    MAP10(9, 7, "map10",new Point(0.22f, 0.07f), new Point(0.76f, 0.84f), PursuitPositionDefinition.MAP_10, VehicleType.CAR),
    MAP11(10, 3, "map11",new Point(0.61f, 0.11f), new Point(0.21f, 0.69f), PursuitPositionDefinition.MAP_11, VehicleType.CAR),
    MAP12(11, 2, "map12",new Point(0.89f, 0.17f), new Point(0.2f, 0.85f), PursuitPositionDefinition.MAP_12, VehicleType.MOTORCYCLE),
    MAP13(12, 12, "map13", new Point(0.08f, 0.06f), new Point(0.88f, 0.73f), PursuitPositionDefinition.MAP_13, VehicleType.BOAT),
    MAP14(13, 9, "map14",new Point(0.18f, 0.08f), new Point(0.32f, 0.84f), PursuitPositionDefinition.MAP_14, VehicleType.CAR),
    MAP15(14, 0, "map15",new Point(0.6f, 0.9f), new Point(0.3f, 0.2f), PursuitPositionDefinition.MAP_15, VehicleType.BOAT);

    private int id;
    private int mapOrder;
    private String textureRegionName;
    //relatives position of checkpoints on map
    private Point startPointCoords;
    private Point endPointCoords;
    private VehicleType vehicleType;

    private PursuitPositionDefinition pursuitPositionDefinition;

    Maps(int id, int mapOrder, String textureRegionName, Point startPointCoords, Point endPointCoords, PursuitPositionDefinition positionDefinition, VehicleType vehicleType){
        this.id = id;
        this.mapOrder = mapOrder;
        this.textureRegionName = textureRegionName;
        this.startPointCoords = startPointCoords;
        this.endPointCoords = endPointCoords;
        this.pursuitPositionDefinition = positionDefinition;
        this.vehicleType = vehicleType;
    }

    @Override
    public int number() {
        return id;
    }

    public int getMapOrder() {
        return mapOrder;
    }

    public String getTextureRegionName(){
        return textureRegionName;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public Point getStartPointCoords(){
        return startPointCoords;
    }

    public Point getEndPointCoords(){
        return endPointCoords;
    }

    public PursuitPositionDefinition getPursuitPositionDefinition() {
        return pursuitPositionDefinition;
    }

    public static Maps getMapById(int id) {
        for(int i = 0; i < Maps.values().length; i++) {
            if (id == Maps.values()[i].id) return Maps.values()[i];
        }
        return null;
    }

    public static Maps getNextMap(int order) {
        while(order < Maps.values().length) {
            order++;
            for(int i = 0; i < Maps.values().length; i++) {
                if (order == Maps.values()[i].mapOrder) return Maps.values()[i];
            }
        }
        return getFirstMap();
    }

    public static Maps getFirstMap() {
        Maps map = Maps.values()[0];
        for(int i = 0; i < Maps.values().length; i++) {
            if (Maps.values()[i].getMapOrder() < map.getMapOrder()) map = Maps.values()[i];
        }
        return map;
    }
}
