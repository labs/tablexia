/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.helper;

import cz.nic.tablexia.game.games.pursuit.assets.PursuitAssets;
import cz.nic.tablexia.game.games.pursuit.model.Maps;
import cz.nic.tablexia.util.Point;

/**
 * Created by Václav Tarantík on 18.6.15.
 */
public class TextureHelper {

    public static final  Point  FLAG_BLACK_DOT_RELATIVE_POSITION = new Point(0.295f,0.129f);
    public static final  Point  FINISH_FLAG_SIZE                 = new Point(75, 60);
    private static final String PNG_SUFFIX                       = ".png";


    public static String getMapRegionName(Maps map) {
        return PursuitAssets.MAPS_DIR + map.getTextureRegionName() + PNG_SUFFIX;
    }

    public static String getBgTexturePath() {
        return PursuitAssets.BACKGROUND;
    }

    public static String getFinishFlagTextureName(){
        return PursuitAssets.FINISH_FLAG;
    }

    public static Point getVehicleStartPosition(float parentGroupWidth, Maps currentMap){
        Point relativeStartPosition = currentMap.getStartPointCoords();
        return new Point(parentGroupWidth * relativeStartPosition.x, parentGroupWidth * relativeStartPosition.y);
    }

    public static Point getFinishFlagPosition(float parentGroupWidth, Maps currentMap){
        Point relativeFinishPosition = currentMap.getEndPointCoords();
        return new Point(parentGroupWidth * relativeFinishPosition.x, parentGroupWidth * relativeFinishPosition.y);
    }

}
