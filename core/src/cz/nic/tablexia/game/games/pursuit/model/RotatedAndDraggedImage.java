/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.model;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by Václav Tarantík on 11.8.15.
 */
public class RotatedAndDraggedImage extends Image {

    public RotatedAndDraggedImage(TextureRegion textureRegion){
        super(textureRegion);
    }

    //rotation independent calculation
    //Overriden so there is no need to recalculate touch point after rotating the piece
    @Override
    public Vector2 parentToLocalCoordinates(Vector2 parentCoords) {
        final float childX = getX();
        final float childY = getY();
        parentCoords.x -= childX;
        parentCoords.y -= childY;
        return parentCoords;
    }

    @Override
    public Vector2 localToParentCoordinates(Vector2 localCoords) {
        localCoords.x += getX();
        localCoords.y += getY();
        return localCoords;
    }

    public boolean intersectsWith(Actor actorToIntersectWith){
        Rectangle draggedActorRectangle = new Rectangle(getX(), getY(), getWidth(),getHeight());
        Rectangle actorToIntersectWithRectangle = new Rectangle(actorToIntersectWith.getX(),actorToIntersectWith.getY(),actorToIntersectWith.getWidth(),actorToIntersectWith.getHeight());
        Rectangle intersectionRectangle = new Rectangle();
        if (Intersector.intersectRectangles(draggedActorRectangle, actorToIntersectWithRectangle, intersectionRectangle)) {
            return true;
        }
        return false;
    }
}
