/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.action;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.math.Path;
import com.badlogic.gdx.math.Vector2;
import cz.nic.tablexia.util.actions.MoveAlongAction;

/**
 * Created by Vitaliy Vashchenko on 24.6.16.
 */
public class VehicleMoveAction extends MoveAlongAction {

    private float parentSize;
    private Path<Vector2> path;
    private Music vehicleMusic;
    private boolean acting = true;
    
    public VehicleMoveAction(Path<Vector2> path, float duration, float parentSize, Music vehicleMusic) {
        super(path, duration);
        setRotate(true);
        this.path = path;
        this.parentSize = parentSize;
        this.vehicleMusic = vehicleMusic;
        setDuration(duration);
    }


    @Override
    protected void update(float percent) {
        Vector2 value = new Vector2();
        path.valueAt(value, percent);
        actor.setPosition(parentSize*value.x-actor.getWidth()/2,parentSize*value.y - actor.getHeight()/2);
        actor.setRotation(path.derivativeAt(value, percent).angle() - 90);
    }

    @Override
    public boolean act(float delta) {
        if (acting) return super.act(delta);
        else return false;
    }

    @Override
    protected void begin() {
        vehicleMusic.setVolume(1);
        vehicleMusic.setLooping(true);
        vehicleMusic.play();
        super.begin();
    }

    @Override
    protected void end() {
        vehicleMusic.stop();
        super.end();
        acting = false;
    }
    
    public void pause(){
        acting = false;
        vehicleMusic.pause();
    }
    
    public void resume(){
        acting = true;
        vehicleMusic.play();
    }

}
