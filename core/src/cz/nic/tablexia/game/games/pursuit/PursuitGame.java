/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.CatmullRomSpline;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.pursuit.action.RotateAndMovePieceInPosition;
import cz.nic.tablexia.game.games.pursuit.action.RotatePieceToClosestAngle;
import cz.nic.tablexia.game.games.pursuit.action.VehicleMoveAction;
import cz.nic.tablexia.game.games.pursuit.assets.PursuitAssets;
import cz.nic.tablexia.game.games.pursuit.helper.ArithmeticsHelper;
import cz.nic.tablexia.game.games.pursuit.helper.GameRulesHelper;
import cz.nic.tablexia.game.games.pursuit.helper.PursuitPositionDefinition;
import cz.nic.tablexia.game.games.pursuit.helper.TextureHelper;
import cz.nic.tablexia.game.games.pursuit.model.Grid;
import cz.nic.tablexia.game.games.pursuit.model.Maps;
import cz.nic.tablexia.game.games.pursuit.model.PuzzlePiece;
import cz.nic.tablexia.game.games.pursuit.model.Vehicle;
import cz.nic.tablexia.game.games.pursuit.model.VehicleType;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;
import cz.nic.tablexia.shared.model.resolvers.PursuitScoreResolver;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.MusicUtil;
import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.listener.DragAndRotateActorListener;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

/**
 * Created by Václav Tarantík on 6.3.15.
 */
public class PursuitGame extends AbstractTablexiaGame<Void> {
    private enum ResultMapping {

        NO_STAR_TEXT(GameResult.NO_STAR, PursuitAssets.VICTORYTEXT_NOSTAR, PursuitAssets.VICTORYSPEECH_NOSTAR),
        ONE_STAR_TEXT(GameResult.ONE_STAR, PursuitAssets.VICTORYTEXT_ONESTAR, PursuitAssets.VICTORYSPEECH_ONESTAR),
        TWO_STAR_TEXT(GameResult.TWO_STAR, PursuitAssets.VICTORYTEXT_TWOSTARS, PursuitAssets.VICTORYSPEECH_TWOSTAR),
        THREE_STAR_TEXT(GameResult.THREE_STAR, PursuitAssets.VICTORYTEXT_THREESTARS, PursuitAssets.VICTORYSPEECH_THREESTAR);

        private final GameResult gameResult;
        private final String textKey;
        private final String soundName;

        ResultMapping(GameResult gameResult, String textKey, String soundName) {
            this.gameResult = gameResult;
            this.textKey = textKey;
            this.soundName = soundName;
        }

        public String getTextKey() {
            return textKey;
        }

        public String getSoundName() {
            return soundName;
        }

        public static ResultMapping getResultTextMappingForGameResult(GameResult gameResult) {
            for (ResultMapping resultMapping : ResultMapping.values()) {
                if (resultMapping.gameResult.equals(gameResult)) {
                    return resultMapping;
                }
            }
            return null;
        }
    }


    private static final int GRID_PADDING = 10;

    public  static final String GRID                        = "grid group";
    public  static final String EVENT_PIECE_DROPPED         = "piece dropped";

    private static final String SUMMARY_TEXT_TIME_HOURS     = "victory_text_hours";
    private static final String SUMMARY_TEXT_TIME_MINUTES   = "victory_text_minutes";
    private static final String SUMMARY_TEXT_TIME_SECONDS   = "victory_text_seconds";
    private static final String SUMMARY_TEXT_MOVE_COUNT     = "victory_text_moves";
    private static final String SUMMARY_TEXT_TIME           = "victory_text_time";

    private static final String SCORE_KEY_MOVE_COUNT        = PursuitScoreResolver.SCORE_KEY_MOVE_COUNT;
	private static final String SCORE_KEY_LAST_MAP          = PursuitScoreResolver.SCORE_KEY_LAST_MAP;

	private static final float OVERLAP_PIECE_SCALE	        = 1.2f;

	private static final float NEXT_PATH_POINT = 0.01f;
    
	private static final float ANIMATION_MOVING_DURATION    = 5f;
	private static final float ANIMATION_DELAY_DURATION     = 1.5f;

	private static final int   MIN_ANGLE_TO_PLAY_ROTATE_SOUND   = 45;  //degrees
	public static final int   ROTATE_SOUND_MIN_INTERVAL        = 420; //ms

    private Group contentGroup;

    private Grid grid;
    private int gridColumnCount;
    private int overlappedPieceZIndex;
    private TablexiaNoBlendingImage backgroundImage;
    private DragAndRotateActorListener dragAndRotateActorListener;

	private int lastCirclePart = 0;
	private long lastTimeRotateSoundPlayed;

    private int movesCounter;
    private Maps                actualMap;

    private AtomicBoolean gameFinished = new AtomicBoolean(false);

    private Vehicle vehicle;
    private Image finishFlag;
    
    private TablexiaTextureManager textureManager;
    private String                 mapTextureName;
    
    private VehicleMoveAction vehicleMoveAction;

    private Long testCountDroppedEvent = 0l;

    @Override
    protected Viewport createViewport() {
        return new ExtendViewport(0, TablexiaSettings.getMinWorldHeight());
    }

	@Override
	protected void prepareGameSoundAssetNames(List<String> soundsFileNames) {
        //Car Sound
        soundsFileNames.add(PursuitAssets.SOUND_CAR);
        //Train Sound
        soundsFileNames.add(PursuitAssets.SOUND_TRAIN);
        //Boat Sound
        soundsFileNames.add(PursuitAssets.SOUND_BOAT);
        //Motorcycle Sound
        soundsFileNames.add(PursuitAssets.SOUND_MOTORCYCLE);
		//Pieces Sounds
		soundsFileNames.add(PursuitAssets.SOUND_LIFT);
		soundsFileNames.add(PursuitAssets.SOUND_ROTATION);
		soundsFileNames.add(PursuitAssets.SOUND_SWITCH);
	}

    @Override
    protected Void prepareGameData(Map<String, String> gameState) {
        final Object loaderLock = new Object();
        updateMapIndex();
        mapTextureName = prepareScreenAssetsPath(prepareScreenName()) + TextureHelper.getMapRegionName(actualMap);

        synchronized (loaderLock) {

            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    synchronized (loaderLock) {
                        tryToDisposeTextureManager();
                        textureManager = new TablexiaTextureManager(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL);
                        textureManager.loadTexture(mapTextureName);
                        textureManager.finishLoading();
                        loaderLock.notify();
                    }
                }
            });

            try {
                loaderLock.wait();
            } catch (InterruptedException e) {
                Log.err(getClass(), "Error while waiting to texture loader!", e);
            }
        }
        return null;
    }

    private void tryToDisposeTextureManager() {
        if (textureManager != null) {
            textureManager.dispose();
            textureManager = null;
        }
    }

    @Override
    protected void screenResized(int width, int height) {
        ScaleUtil.setFullScreen(contentGroup, getStage());
        float minSide = Math.min(getViewportWidth(), getViewportHeight());
        grid.setSize(minSide - 2 * GRID_PADDING, minSide - 2 * GRID_PADDING);
        grid.setPosition(getViewportWidth() / 2 - grid.getWidth() / 2, (getViewportHeight() / 2 - grid.getHeight() / 2));
        grid.scaleGrid();

    }

    @Override
    protected void gameLoaded(Map<String, String> gameState) {
        gameFinished.set(false);
        movesCounter = 0;
        gridColumnCount = GameRulesHelper.getNumberOfColumns(getGameDifficulty());
        overlappedPieceZIndex = (int) Math.pow(gridColumnCount, 2) - 1;
        contentGroup = new Group();
        ScaleUtil.setFullScreen(contentGroup, getStage());
        getStage().addActor(contentGroup);

		prepareBackground();
		prepareGrid();
		prepareFinishFlag();
		prepareVehicle();

        dragAndRotateActorListener = new DragAndRotateActorListener(new DragAndRotateActorListener.IOnRotationFinished() {
            @Override
            public void onDragAndRotationFinished() {
                dragAndRotateActorListener.setPieceAnimationDone(true);
                setGameScore(SCORE_KEY_MOVE_COUNT, movesCounter++);
            }
        }) {
            @Override
            public void selected(float x, float y) {
                if (dragAndRotateActorListener != null) {
                    if (dragAndRotateActorListener.getDraggedActor() == null) {
                        dragAndRotateActorListener.setDraggedActor(grid.pieceAtPoint(x, y));
                        if (dragAndRotateActorListener.getDraggedActor()!=null){
                            getSound(PursuitAssets.SOUND_LIFT).play();

                        }
                    }
                }
            }

            @Override
            public void moved(float x, float y, Actor draggedPiece) {
                if (dragAndRotateActorListener != null) {
                    PuzzlePiece puzzlePiece = (PuzzlePiece) draggedPiece;
                    PuzzlePiece overlappedPiece = grid.pieceIntersectingWithDragged(puzzlePiece.getId());
                    if (overlappedPiece != null) {
                        overlappedPiece.setZIndex(overlappedPieceZIndex);
                        overlappedPiece.setScale(OVERLAP_PIECE_SCALE);
                        grid.unscalePieces(puzzlePiece.getId(), overlappedPiece.getId());
                    } else {
                        grid.unscalePieces(puzzlePiece.getId(), -1);
                    }
                }
            }

			@Override
			public void rotated(float angle, Actor rotatedPiece) {
				int circlePart = (int)(angle) / MIN_ANGLE_TO_PLAY_ROTATE_SOUND;

				if (lastCirclePart != circlePart && System.currentTimeMillis() - lastTimeRotateSoundPlayed > ROTATE_SOUND_MIN_INTERVAL) {
					getSound(PursuitAssets.SOUND_ROTATION).play();
					lastTimeRotateSoundPlayed = System.currentTimeMillis();
					lastCirclePart = circlePart;
				}
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
			}

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				lastCirclePart = 0;
				return super.touchDown(event, x, y, pointer, button);
			}

			@Override
            public void dropped(float x, float y, final Actor draggedPiece) {

                if (dragAndRotateActorListener != null) {
                    if (draggedPiece != null) {
                        dragAndRotateActorListener.setPerformingAction(true);
                        final PuzzlePiece puzzlePiece = (PuzzlePiece) draggedPiece;
                        final PuzzlePiece overlappedPiece = grid.pieceIntersectingWithDragged(puzzlePiece.getId());

                        Action onPiecesSwitchFinishedAction = new Action() {
                            @Override
                            public boolean act(float delta) {

                                setGameScore(SCORE_KEY_MOVE_COUNT, movesCounter++);
                                dragAndRotateActorListener.setDraggedActor(null);
                                dragAndRotateActorListener.setPerformingAction(false);
                                grid.unscaleAllPieces();
                                //cancelling all actions on actor that would take place after piece was moved in correct position
                                Array<Action> gridActions = grid.getActions();
                                if (gridActions.size > 0) {
                                    for (Action action : gridActions) {
                                        if (action instanceof RotatePieceToClosestAngle || action instanceof RotateAndMovePieceInPosition) {
                                            grid.removeAction(action);
                                        }
                                    }
                                    //in case we canceled rotation animation, rotate piece to correct angle
                                    if (!isRotatedWithDoubleTap()) { //in case of double tap next action will rotate piece back to the ange before the rotation
                                        grid.addAction(Actions.sequence(new RotatePieceToClosestAngle((PuzzlePiece) draggedPiece), Actions.run(new Runnable() {
                                            @Override
                                            public void run() {
                                                // check if puzzle is solved after possible rotation animation
                                                checkEndGameCondition();
                                                triggerScenarioStepEvent(EVENT_PIECE_DROPPED + testCountDroppedEvent);
                                                testCountDroppedEvent++;
                                            }
                                        })));
                                    }
                                }

                                //counter for bad trigger same event behind
								checkEndGameCondition();
                                if(getGameDifficulty().equals(GameDifficulty.BONUS))
                                    grid.checkNextPuzzlePieces(puzzlePiece, true);
                                return true;
                            }
                        };
                        if (grid.switchPieces(puzzlePiece, overlappedPiece, onPiecesSwitchFinishedAction)) {
							getSound(PursuitAssets.SOUND_SWITCH).play();
                            if(getGameDifficulty().equals(GameDifficulty.BONUS))
                                grid.checkNextPuzzlePieces(overlappedPiece, true);
						}
                    }
                }
            }
        };
        dragAndRotateActorListener.setParentActor(grid);
        backgroundImage.addListener(dragAndRotateActorListener);


    }

	private void checkEndGameCondition() {
		if (grid.allPiecesInCorrectPositions() && !gameFinished.get()) {
			backgroundImage.removeListener(dragAndRotateActorListener);
			showCheckpoints();
            gameFinished.set(true);
		}
	}

    @Override
    protected void gameVisible() {
        AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_GAME_READY);
    }

    @Override
    protected void gameDisposed() {
        tryToDisposeTextureManager();
        super.gameDisposed();
    }

	private void updateMapIndex() {
		String lastMap = GameDAO.getLastGameScoreValueForGameAndKey(GameDefinition.PURSUIT.getGameNumber(), SCORE_KEY_LAST_MAP);

		//couldn't find the last map
		if (lastMap == null) {
            actualMap = Maps.getFirstMap();
		} else {
            actualMap = Maps.getNextMap(Maps.getMapById(Integer.parseInt(lastMap)).getMapOrder());
		}
		//Update last map id in db
		setGameScore(SCORE_KEY_LAST_MAP, actualMap.number());
	}

    private void prepareBackground() {
        backgroundImage = new TablexiaNoBlendingImage(getScreenTextureRegion(TextureHelper.getBgTexturePath()));
        backgroundImage.setFillParent(true);
        contentGroup.addActor(backgroundImage);
    }

    private void prepareGrid() {
        Texture mapTextureRegion = textureManager.getTexture(mapTextureName);
        mapTextureRegion.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        float minSide = Math.min(getViewportWidth(),getViewportHeight());
        grid = new Grid(((getViewportWidth() / 2) - minSide / 2) + GRID_PADDING,(getViewportHeight() / 2 - minSide / 2) + GRID_PADDING, minSide- 2 * GRID_PADDING, gridColumnCount, mapTextureRegion, getRandom(), this);
        grid.setName(GRID);
        contentGroup.addActor(grid);
    }

    private void prepareVehicle() {
        VehicleType vehicleType = actualMap.getVehicleType();
        Point vehiclePosition = new Point(0, 0);
        Point vehicleSize = new Point(vehicleType.getVehicleSize().x, vehicleType.getVehicleSize().y);
        vehicle = new Vehicle(getScreenTextureRegion(vehicleType.getTextureName()), vehiclePosition, vehicleSize);
        vehicle.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
        vehicle.setVisible(false);
        grid.addActor(vehicle);
	}

	private void prepareFinishFlag() {
        finishFlag = new Image(getScreenTextureRegion(TextureHelper.getFinishFlagTextureName()));
		finishFlag.setSize(TextureHelper.FINISH_FLAG_SIZE.x, TextureHelper.FINISH_FLAG_SIZE.y);
        finishFlag.setOrigin(finishFlag.getWidth() / 2, finishFlag.getHeight() / 2);
        finishFlag.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
        finishFlag.setVisible(false);
        grid.addActor(finishFlag);

    }

	/**
	 * Returns sound (instances of Music) for vehicleType
	 * @param vehicleType
	 * @return Music vehicle sound
	 */
	private Music getVehicleSound(VehicleType vehicleType) {
		//Using music, so i can use OnCompletionListener
		String soundsToLoad;

		switch (vehicleType) {
			case CAR:
				soundsToLoad = PursuitAssets.SOUND_CAR;
				break;
			case MOTORCYCLE:
				soundsToLoad = PursuitAssets.SOUND_MOTORCYCLE;
				break;
			case TRAIN:
				soundsToLoad = PursuitAssets.SOUND_TRAIN;
				break;
			case BOAT:
				soundsToLoad = PursuitAssets.SOUND_BOAT;
				break;
			default:
				throw new IllegalArgumentException(this.getClass().getSimpleName() + ": Can't get sounds for vehicleType: " + vehicleType);
		}

		return getMusic(soundsToLoad);
	}

	//sets the position according to completed map rotation
    private void showCheckpoints() {
        Point gridRotationCenter = new Point(grid.getWidth() / 2, grid.getHeight() / 2);
        //flag
        Point flagPosition = TextureHelper.getFinishFlagPosition(grid.getWidth(), actualMap);
        Point flagPositonConsideringMapRotation = ArithmeticsHelper.translatePositionAccordingToRotation(flagPosition, grid.getRotation(), gridRotationCenter);
        //positioning both flag and vehicle by center
        //flag is not rotated
        Point flagPoleOrigin = new Point(finishFlag.getWidth() * TextureHelper.FLAG_BLACK_DOT_RELATIVE_POSITION.x, finishFlag.getHeight() * TextureHelper.FLAG_BLACK_DOT_RELATIVE_POSITION.y);
        finishFlag.setPosition(flagPositonConsideringMapRotation.x - flagPoleOrigin.x, flagPositonConsideringMapRotation.y - flagPoleOrigin.y);

        //vehicle
        Point vehiclePosition = TextureHelper.getVehicleStartPosition(grid.getWidth(), actualMap);
        Point vehiclePositionConsideringRotation = ArithmeticsHelper.translatePositionAccordingToRotation(vehiclePosition, grid.getRotation(), gridRotationCenter);
        vehicle.setPosition(vehiclePositionConsideringRotation.x - vehicle.getWidth() / 2, vehiclePositionConsideringRotation.y - vehicle.getHeight() / 2);

        finishFlag.setVisible(true);

        final Music vehicleSound = getVehicleSound(actualMap.getVehicleType());
        final CatmullRomSpline<Vector2> path = new CatmullRomSpline<Vector2>(PursuitPositionDefinition.getPointsWithRotation(actualMap.getPursuitPositionDefinition(), grid.getRotation()), false);

        Vector2 value = new Vector2();
        path.valueAt(value, NEXT_PATH_POINT);
        vehicle.setRotation(path.derivativeAt(value, NEXT_PATH_POINT).angle() - 90);

        vehicleMoveAction = new VehicleMoveAction(path, ANIMATION_MOVING_DURATION, grid.getWidth(), vehicleSound);
        vehicle.addAction(Actions.sequence(Actions.show(),Actions.delay(ANIMATION_DELAY_DURATION),vehicleMoveAction,new RunnableAction() {
            @Override
            public void run() {
                MusicUtil.fadeIn(vehicleSound,MUSIC_FADE_OUT_DURATION);
                endGame();
                showGameResultDialog();
                vehicle.clear();
                vehicle.remove();
                finishFlag.clear();
                finishFlag.remove();


            }
        }));
    }

    @Override
    protected void gamePaused(Map<String, String> gameState) {
        super.gamePaused(gameState);
        if (vehicleMoveAction!=null) vehicleMoveAction.pause();
    }

    @Override
    protected void gameResumed() {
        super.gameResumed();
        if (vehicleMoveAction!=null) vehicleMoveAction.resume();
    }

    @Override
    protected void gameEnd() {
        vehicleMoveAction = null;
        super.gameEnd();
    }

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(new SummaryMessage(SummaryImage.TIME, getFormattedText(SUMMARY_TEXT_TIME, getOverallTimeText(GameDAO.getGameDuration(game)))),
                new SummaryMessage(SummaryImage.STATS, getFormattedText(SUMMARY_TEXT_MOVE_COUNT, game.getGameScore(SCORE_KEY_MOVE_COUNT, "0"))));
    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        return ResultMapping.getResultTextMappingForGameResult(gameResult).getTextKey();
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        return ResultMapping.getResultTextMappingForGameResult(gameResult).getSoundName();
    }

    private String getOverallTimeText(float millis) {
        StringBuilder sb = new StringBuilder();
        int seconds = (int) (millis / 1000) % 60 ;
        int minutes = (int) ((millis / (1000*60)) % 60);
        int hours   = (int) ((millis / (1000*60*60)) % 24);
        Log.info(getClass(), "TIME: " + hours + "h, " + minutes + "m, " + seconds + "s");
        if (hours != 0) {
            Log.info(getClass(), "HOURS");
            sb.append(getFormattedText(SUMMARY_TEXT_TIME_HOURS, hours));
        }
        if (minutes != 0) {
            Log.info(getClass(), "MINUTES");
            sb.append(getFormattedText(SUMMARY_TEXT_TIME_MINUTES, minutes));
        }
        if (seconds != 0) {
            Log.info(getClass(), "SECONDS");
            sb.append(getFormattedText(SUMMARY_TEXT_TIME_SECONDS, seconds));
        }
        return sb.toString();
    }


//////////////////////////////////////////// PRELOADER

    private static final String                             PRELOADER_ANIM_IMAGE            = "preloader_anim";
    private static final int                                PRELOADER_ANIM_FRAMES           = 14;
    private static final float                              PRELOADER_ANIM_FRAME_DURATION   = 0.5f;
    private static final String                             PRELOADER_TEXT_KEY             	= ApplicationTextManager.ApplicationTextsAssets.GAME_PURSUIT_PRELOADER_TEXT;
    private static final String                             PRELOADER_DESKTOP_TEXT_KEY      = ApplicationTextManager.ApplicationTextsAssets.GAME_PURSUIT_PRELOADER_DESKTOP_TEXT;

    private static final Scaling        PRELOADER_IMAGE_SCALING 		    = Scaling.fit;
    private static final int 			PRELOADER_TEXT_ALIGN 				= Align.left;
    private static final float 			PRELOADER_TEXT_PADDING 				= 10f;
    private static final float 			PRELOADER_LEFT_COLUMN_WIDTH_RATIO 	= 1f / 4;
    private static final float 			PRELOADER_RIGHT_COLUMN_RATIO 		= 5f / 8;
    private static final float 			PRELOADER_ROW_HEIGHT 				= 1f / 3;

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        AnimatedImage preloaderImage = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_IMAGE, PRELOADER_ANIM_FRAMES, PRELOADER_ANIM_FRAME_DURATION), false);
        preloaderImage.startAnimationLoop();
        String preloaderText = TablexiaSettings.getInstance().isRunningOnMobileDevice() ? preloaderAssetsManager.getText(PRELOADER_TEXT_KEY) : preloaderAssetsManager.getText(PRELOADER_DESKTOP_TEXT_KEY);
        components.add(new TwoColumnContentDialogComponent(
                new AnimatedImageContentDialogComponent(preloaderImage, PRELOADER_IMAGE_SCALING),
                new TextContentDialogComponent(preloaderText, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_ROW_HEIGHT));
    }

    @Override
    protected String preparePreloaderSpeechFileBaseName() {
        return (TablexiaSettings.getInstance().getPlatform() == TablexiaSettings.Platform.DESKTOP) ? super.preparePreloaderSpeechFileBaseName() + "_desktop" : super.preparePreloaderSpeechFileBaseName();
    }

    //////////////////////////////////////////// DEBUG - FORCE GAME END

    @Override
    protected void onForceGameEnd(GameResultDefinition gameResult) {
        backgroundImage.removeListener(dragAndRotateActorListener);

        long duration = GameRulesHelper.getDurationForStars(GameDifficulty.getByGame(getGame()), gameResult.getCupsCount());
        GameDAO.startGame(getGame(), TimeUtils.millis() - duration);
        waitForActualGameScore();
        endGame();
        gameFinished.set(true);

        showGameResultDialog();
    }
}
