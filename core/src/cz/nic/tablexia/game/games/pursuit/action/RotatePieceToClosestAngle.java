/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.action;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import cz.nic.tablexia.game.games.pursuit.helper.ArithmeticsHelper;
import cz.nic.tablexia.game.games.pursuit.model.PuzzlePiece;
import cz.nic.tablexia.util.listener.DragAndRotateActorListener;

/**
 * Created by Václav Tarantík on 3.8.15.
 */
public class RotatePieceToClosestAngle extends SequenceAction {
    public RotatePieceToClosestAngle(PuzzlePiece draggedPiece){
        setTarget(draggedPiece);
        setActor(draggedPiece);
        addAction(Actions.rotateTo(ArithmeticsHelper.getClosestRightAngle(draggedPiece.getRotation()), DragAndRotateActorListener.ROTATION_ANIMATION_DURATION));
    }
    public RotatePieceToClosestAngle(PuzzlePiece draggedPiece,float originalRotation){
        setTarget(draggedPiece);
        setActor(draggedPiece);
        addAction(Actions.rotateTo(ArithmeticsHelper.getAngleInRotationDirection(originalRotation,draggedPiece.getRotation()), DragAndRotateActorListener.ROTATION_ANIMATION_DURATION));
    }

    public RotatePieceToClosestAngle(PuzzlePiece draggedPiece, float originalRotation,final DragAndRotateActorListener.IOnRotationFinished iOnRotationFinished){
        this(draggedPiece,originalRotation);
        Action afterAction = new Action() {
            @Override
            public boolean act(float delta) {
                iOnRotationFinished.onDragAndRotationFinished();
                return true;
            }
        };
        addAction(afterAction);
    }
}
