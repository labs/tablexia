/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.model;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import cz.nic.tablexia.game.games.pursuit.PursuitGame;
import cz.nic.tablexia.game.games.pursuit.assets.PursuitAssets;

/**
 * Created by Václav Tarantík on 17.6.15.
 */
public class PuzzlePiece extends RotatedAndDraggedImage {
    private int id;
    private boolean dragged;
    private int correctPosition;
    private int actualPosition;
    private TextureRegion texture;
    private boolean covered;
    private boolean pieceVisible;
    private TextureRegionDrawable coverTexture;

    public PuzzlePiece(final int id, TextureRegion texture) {
        super(texture);
        setOrigin(getWidth() / 2, getHeight() / 2);
        this.id = id;
        this.texture = texture;
        setTouchable(Touchable.disabled);
        covered = false;
        pieceVisible = true;
        coverTexture = null;
    }

    public int getId() {
        return id;
    }

    public boolean isCovered() {
        return covered;
    }

    @Override
    public String toString() {
        return "Piece[id: " + id + "]";
    }

    public int getCorrectPosition() {
        return correctPosition;
    }

    public void setCorrectPosition(int correctPosition) {
        this.correctPosition = correctPosition;
    }

    public int getActualPosition() {
        return actualPosition;
    }

    public void setActualPosition(int actualPosition) {
        this.actualPosition = actualPosition;
    }

    public void coverPuzzlePiece(PursuitGame pursuitGame) {
        if (pieceVisible == true) {
            if (coverTexture == null)
                coverTexture = new TextureRegionDrawable(pursuitGame.getScreenTextureRegion(PursuitAssets.EMPTY_PUZZLE));
            this.setDrawable(coverTexture);
            covered = true;
            pieceVisible = false;
        }
    }

    public void showPuzzlePiece() {
        this.setDrawable(new TextureRegionDrawable(texture));
        pieceVisible = true;
    }

    public void blickPuzzlePiece() {
        if (!pieceVisible) {
            PuzzlePiece puzzlePiece = this;
            addAction(Actions.sequence(Actions.run(new Runnable() {
                @Override
                public void run() {
                    puzzlePiece.setDrawable(new TextureRegionDrawable(texture));
                }
            }), Actions.delay(1f), Actions.run(new Runnable() {
                @Override
                public void run() {
                    puzzlePiece.setDrawable(coverTexture);
                }
            })));
        }
    }
}
