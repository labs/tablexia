/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.helper;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.shared.model.resolvers.PursuitScoreResolver;

/**
 * Created by Václav Tarantík on 27.7.15.
 */
public class GameRulesHelper {
    private static final int EASY_ONE_STAR_DURATION         = PursuitScoreResolver.EASY_ONE_STAR_DURATION;
    private static final int EASY_TWO_STARS_DURATION        = PursuitScoreResolver.EASY_TWO_STARS_DURATION;
    private static final int EASY_THREE_STARS_DURATION      = PursuitScoreResolver.EASY_THREE_STARS_DURATION;

    private static final int MEDIUM_ONE_STAR_DURATION       = PursuitScoreResolver.MEDIUM_ONE_STAR_DURATION;
    private static final int MEDIUM_TWO_STARS_DURATION      = PursuitScoreResolver.MEDIUM_TWO_STARS_DURATION;
    private static final int MEDIUM_THREE_STARS_DURATION    = PursuitScoreResolver.MEDIUM_THREE_STARS_DURATION;

    private static final int HARD_ONE_STAR_DURATION         = PursuitScoreResolver.HARD_ONE_STAR_DURATION;
    private static final int HARD_TWO_STARS_DURATION        = PursuitScoreResolver.HARD_TWO_STARS_DURATION;
    private static final int HARD_THREE_STARS_DURATION      = PursuitScoreResolver.HARD_THREE_STARS_DURATION;

    public static final int[] COLUMNS_FOR_DIFFICULTY        = new int[] {3, 4, 5, 5};

    public static int getNumberOfColumns(GameDifficulty difficulty){
        return difficulty != GameDifficulty.TUTORIAL ? COLUMNS_FOR_DIFFICULTY[difficulty.ordinal() - 1] : 3;
    }

    /**
     * This method is used for force game end debug button.
     * This method returns time in milliseconds, which is supposed to be saved in a database to get the exact star count for difficulty
     * @param difficulty difficulty of the game
     * @param stars star count you wish to get
     * @return duration of the game in milliseconds
     */
    public static long getDurationForStars(GameDifficulty difficulty, int stars) {
        int durationSeconds = 0;

        switch (difficulty) {
            case EASY:
                if (stars == 0) {
                    durationSeconds = EASY_ONE_STAR_DURATION + 1;
                } else if (stars == 1) {
                    durationSeconds = EASY_TWO_STARS_DURATION + 1;
                } else if (stars == 2) {
                    durationSeconds = EASY_THREE_STARS_DURATION + 1;
                } else {
                    durationSeconds = EASY_THREE_STARS_DURATION - 1;
                }
                break;
            case MEDIUM:
                if (stars == 0) {
                    durationSeconds = MEDIUM_ONE_STAR_DURATION + 1;
                } else if (stars == 1) {
                    durationSeconds = MEDIUM_TWO_STARS_DURATION + 1;
                } else if (stars == 2) {
                    durationSeconds = MEDIUM_THREE_STARS_DURATION + 1;
                } else {
                    durationSeconds = MEDIUM_THREE_STARS_DURATION - 1;
                }
                break;
            case HARD:
                if (stars == 0) {
                    durationSeconds = HARD_ONE_STAR_DURATION + 1;
                } else if (stars == 1) {
                    durationSeconds = HARD_TWO_STARS_DURATION + 1;
                } else if (stars == 2) {
                    durationSeconds = HARD_THREE_STARS_DURATION + 1;
                } else {
                    durationSeconds = HARD_THREE_STARS_DURATION - 1;
                }
                break;
            case BONUS:
                if (stars == 0) {
                    durationSeconds = HARD_ONE_STAR_DURATION + 1;
                } else if (stars == 1) {
                    durationSeconds = HARD_TWO_STARS_DURATION + 1;
                } else if (stars == 2) {
                    durationSeconds = HARD_THREE_STARS_DURATION + 1;
                } else {
                    durationSeconds = HARD_THREE_STARS_DURATION - 1;
                }
                break;
        }

        return durationSeconds * 1000;
    }

    public static AbstractTablexiaGame.GameResult getNumberOfStarsForTime(GameDifficulty difficulty, Long millis){
        if (millis == null) {
            return AbstractTablexiaGame.GameResult.NO_STAR;
        }
        float seconds = millis / 1000;
        switch (difficulty) {
            case EASY:
                if (seconds > EASY_ONE_STAR_DURATION) {
                    return AbstractTablexiaGame.GameResult.NO_STAR;
                } else if (seconds > EASY_TWO_STARS_DURATION) {
                    return AbstractTablexiaGame.GameResult.ONE_STAR;
                } else if (seconds > EASY_THREE_STARS_DURATION) {
                    return AbstractTablexiaGame.GameResult.TWO_STAR;
                } else {
                    return AbstractTablexiaGame.GameResult.THREE_STAR;
                }
            case MEDIUM:
                if (seconds > MEDIUM_ONE_STAR_DURATION) {
                    return AbstractTablexiaGame.GameResult.NO_STAR;
                } else if (seconds > MEDIUM_TWO_STARS_DURATION) {
                    return AbstractTablexiaGame.GameResult.ONE_STAR;
                } else if (seconds > MEDIUM_THREE_STARS_DURATION) {
                    return AbstractTablexiaGame.GameResult.TWO_STAR;
                } else {
                    return AbstractTablexiaGame.GameResult.THREE_STAR;
                }
            case HARD:
                if (seconds > HARD_ONE_STAR_DURATION) {
                    return AbstractTablexiaGame.GameResult.NO_STAR;
                } else if (seconds > HARD_TWO_STARS_DURATION) {
                    return AbstractTablexiaGame.GameResult.ONE_STAR;
                } else if (seconds > HARD_THREE_STARS_DURATION) {
                    return AbstractTablexiaGame.GameResult.TWO_STAR;
                } else {
                    return AbstractTablexiaGame.GameResult.THREE_STAR;
                }
            case BONUS:
                if (seconds > HARD_ONE_STAR_DURATION) {
                    return AbstractTablexiaGame.GameResult.NO_STAR;
                } else if (seconds > HARD_TWO_STARS_DURATION) {
                    return AbstractTablexiaGame.GameResult.ONE_STAR;
                } else if (seconds > HARD_THREE_STARS_DURATION) {
                    return AbstractTablexiaGame.GameResult.TWO_STAR;
                } else {
                    return AbstractTablexiaGame.GameResult.THREE_STAR;
                }
        }
        throw new IllegalStateException("Unknown difficulty value");
    }
}
