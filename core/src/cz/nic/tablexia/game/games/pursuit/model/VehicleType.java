/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.model;

import cz.nic.tablexia.game.games.pursuit.assets.PursuitAssets;

/**
 * Created by Václav Tarantík on 10.8.15.
 */
public enum VehicleType {
    CAR(new cz.nic.tablexia.util.Point(29, 50)),
    MOTORCYCLE(new cz.nic.tablexia.util.Point(18,50)),
    TRAIN(new cz.nic.tablexia.util.Point(29, 70)),
    BOAT(new cz.nic.tablexia.util.Point(23, 60));

    private String textureName;
    private cz.nic.tablexia.util.Point vehicleSize;

    VehicleType(cz.nic.tablexia.util.Point vehicleSize) {
        textureName = PursuitAssets.VEHICLES_DIR + name().toLowerCase();
        this.vehicleSize = vehicleSize;
    }

    public String getTextureName() {
        return textureName;
    }

    public cz.nic.tablexia.util.Point getVehicleSize() {
        return vehicleSize;
    }
}
