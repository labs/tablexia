/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.action;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;

import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.game.games.pursuit.model.PuzzlePiece;
import cz.nic.tablexia.util.listener.DragAndRotateActorListener;

/**
 * Created by Václav Tarantík on 3.8.15.
 */
public class RotateAndMovePieceInPosition extends SequenceAction implements Interruptable{
    public RotateAndMovePieceInPosition(PuzzlePiece draggedPiece,float originalRotation, Point position, final DragAndRotateActorListener.IOnRotationFinished iOnRotationFinished){
        setActor(draggedPiece);
        setTarget(draggedPiece);
        Action rotationFinishedAction = new Action() {
            @Override
            public boolean act(float delta) {
                iOnRotationFinished.onDragAndRotationFinished();
                return true;
            }
        };

        addAction(Actions.parallel(new RotatePieceToClosestAngle(draggedPiece,originalRotation),new MovePieceToPosition(draggedPiece,position)));
        addAction(rotationFinishedAction);
    }

    public void interrupt(){
        for(Action a: getActions()){
            if(a instanceof Interruptable){
                ((Interruptable)a).interrupt();
            }
        }
    }
}
