/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.helper;

import com.badlogic.gdx.math.Vector2;

import cz.nic.tablexia.util.Point;

/**
 * Created by Vitaliy Vashchenko on 22.8.16.
 */
public enum PursuitPositionDefinition {
    /**
     * Points for creating a path for move animation
     */
    MAP_1(new Point(0.866f, 0.1f),new Point(0.866f, 0.1f), new Point(0.859f, 0.211f), new Point(0.824f, 0.276f), new Point(0.792f, 0.312f), new Point(0.719f, 0.268f), new Point(0.666f, 0.241f), new Point(0.608f, 0.221f), new Point(0.534f, 0.203f), new Point(0.448f, 0.194f), new Point(0.374f, 0.224f), new Point(0.328f, 0.286f), new Point(0.293f, 0.37f), new Point(0.265f, 0.469f), new Point(0.237f, 0.575f), new Point(0.215f, 0.671f), new Point(0.228f, 0.772f), new Point(0.258f, 0.846f), new Point(0.26f, 0.85f)),
    MAP_2(new Point(0.64f, 0.11f),new Point(0.64f, 0.11f), new Point(0.68f, 0.214f), new Point(0.738f, 0.308f), new Point(0.777f, 0.395f), new Point(0.683f, 0.449f), new Point(0.613f, 0.509f), new Point(0.54f, 0.573f), new Point(0.473f, 0.658f), new Point(0.4f, 0.728f), new Point(0.322f, 0.711f), new Point(0.26f, 0.666f), new Point(0.208f, 0.757f), new Point(0.22f, 0.75f)),
    MAP_3(new Point(0.08f, 0.41f),new Point(0.08f, 0.41f), new Point(0.182f, 0.461f), new Point(0.271f, 0.466f), new Point(0.368f, 0.475f), new Point(0.376f, 0.599f), new Point(0.387f, 0.698f), new Point(0.398f, 0.755f), new Point(0.361f, 0.824f), new Point(0.394f, 0.868f), new Point(0.461f, 0.877f), new Point(0.531f, 0.85f), new Point(0.627f, 0.897f), new Point(0.691f, 0.899f), new Point(0.77f, 0.903f), new Point(0.776f, 0.855f), new Point(0.758f, 0.811f), new Point(0.776f, 0.754f), new Point(0.79f, 0.71f)),
    MAP_4(new Point(0.95f, 0.33f),new Point(0.95f, 0.33f), new Point(0.866f, 0.307f), new Point(0.766f, 0.342f), new Point(0.709f, 0.391f), new Point(0.588f, 0.452f), new Point(0.512f, 0.497f), new Point(0.425f, 0.588f), new Point(0.355f, 0.667f), new Point(0.278f, 0.595f), new Point(0.19f, 0.574f), new Point(0.18f, 0.57f)),
    MAP_5(new Point(0.94f, 0.12f),new Point(0.94f, 0.12f), new Point(0.928f, 0.333f), new Point(0.784f, 0.373f), new Point(0.678f, 0.412f), new Point(0.645f, 0.525f), new Point(0.578f, 0.582f), new Point(0.549f, 0.649f), new Point(0.597f, 0.733f), new Point(0.68f, 0.77f), new Point(0.784f, 0.793f), new Point(0.863f, 0.843f), new Point(0.86f, 0.82f)),
    MAP_6(new Point(0.95f, 0.17f),new Point(0.95f, 0.17f), new Point(0.85f, 0.18f), new Point(0.763f, 0.171f), new Point(0.68f, 0.136f), new Point(0.602f, 0.092f), new Point(0.538f, 0.036f), new Point(0.509f, 0.081f), new Point(0.438f, 0.077f), new Point(0.392f, 0.142f), new Point(0.369f, 0.199f), new Point(0.377f, 0.251f), new Point(0.436f, 0.306f), new Point(0.45f, 0.32f)),
    MAP_7(new Point(0.56f, 0.07f),new Point(0.56f, 0.07f), new Point(0.43f, 0.16f), new Point(0.347f, 0.246f), new Point(0.373f, 0.36f), new Point(0.443f, 0.465f), new Point(0.499f, 0.58f), new Point(0.431f, 0.657f), new Point(0.379f, 0.738f), new Point(0.356f, 0.825f), new Point(0.36f, 0.83f)),
    MAP_8(new Point(0.16f, 0.19f),new Point(0.16f, 0.19f), new Point(0.237f, 0.255f), new Point(0.287f, 0.316f), new Point(0.351f, 0.354f), new Point(0.285f, 0.44f), new Point(0.212f, 0.521f), new Point(0.134f, 0.597f), new Point(0.071f, 0.679f), new Point(0.118f, 0.718f), new Point(0.204f, 0.754f), new Point(0.272f, 0.733f), new Point(0.343f, 0.798f), new Point(0.425f, 0.856f), new Point(0.496f, 0.919f), new Point(0.596f, 0.853f), new Point(0.672f, 0.823f), new Point(0.759f, 0.82f), new Point(0.818f, 0.828f), new Point(0.825f, 0.801f), new Point(0.83f, 0.79f)),
    MAP_9(new Point(0.15f, 0.11f),new Point(0.15f, 0.11f), new Point(0.193f, 0.179f), new Point(0.255f, 0.19f), new Point(0.164f, 0.426f), new Point(0.234f, 0.494f), new Point(0.307f, 0.661f), new Point(0.356f, 0.723f), new Point(0.435f, 0.776f), new Point(0.509f, 0.805f), new Point(0.622f, 0.831f), new Point(0.719f, 0.837f), new Point(0.808f, 0.834f), new Point(0.81f, 0.83f)),
    MAP_10(new Point(0.22f, 0.07f),new Point(0.22f, 0.07f), new Point(0.214f, 0.293f), new Point(0.204f, 0.421f), new Point(0.449f, 0.422f), new Point(0.574f, 0.561f), new Point(0.678f, 0.49f), new Point(0.806f, 0.75f), new Point(0.761f, 0.838f), new Point(0.76f, 0.84f)),
    MAP_11(new Point(0.61f, 0.11f),new Point(0.61f, 0.11f), new Point(0.582f, 0.258f), new Point(0.571f, 0.364f), new Point(0.599f, 0.442f), new Point(0.641f, 0.499f), new Point(0.646f, 0.584f), new Point(0.702f, 0.626f), new Point(0.81f, 0.644f), new Point(0.801f, 0.693f), new Point(0.678f, 0.683f), new Point(0.553f, 0.688f), new Point(0.366f, 0.697f), new Point(0.302f, 0.688f), new Point(0.214f, 0.697f), new Point(0.21f, 0.69f)),
    MAP_12(new Point(0.89f, 0.17f),new Point(0.89f, 0.17f), new Point(0.806f, 0.118f), new Point(0.565f, 0.111f), new Point(0.43f, 0.102f), new Point(0.385f, 0.164f), new Point(0.436f, 0.294f), new Point(0.473f, 0.442f), new Point(0.492f, 0.562f), new Point(0.444f, 0.676f), new Point(0.385f, 0.792f), new Point(0.278f, 0.803f), new Point(0.223f, 0.843f), new Point(0.208f, 0.868f), new Point(0.2f, 0.85f)),
    MAP_13(new Point(0.08f, 0.06f),new Point(0.08f, 0.06f), new Point(0.189f, 0.227f), new Point(0.414f, 0.316f), new Point(0.665f, 0.51f), new Point(0.797f, 0.632f), new Point(0.88f, 0.735f), new Point(0.88f, 0.73f)),
    MAP_14(new Point(0.18f, 0.08f),new Point(0.18f, 0.08f), new Point(0.212f, 0.239f), new Point(0.29f, 0.321f), new Point(0.399f, 0.382f), new Point(0.342f, 0.503f), new Point(0.311f, 0.659f), new Point(0.308f, 0.773f), new Point(0.324f, 0.843f), new Point(0.32f, 0.84f)),
    MAP_15(new Point(0.6f, 0.9f), new Point(0.6f, 0.9f), new Point(0.6f, 0.8f), new Point(0.6f, 0.5f), new Point(0.75f, 0.35f), new Point(0.7f, 0.235f), new Point(0.5f, 0.135f), new Point(0.35f, 0.2f), new Point(0.35f, 0.2f));

    private Point[] points;

    PursuitPositionDefinition(Point... points) {
        this.points = points;
    }

    public Point[] getPoints() {
        return points;
    }

    public static Vector2[] getPointsWithRotation(PursuitPositionDefinition positionDefinition, float rotation) {
        Vector2[] rotatedPositions = new Vector2[positionDefinition.getPoints().length];
        int rotationDefinition = getRotationDirection(rotation);
        for (int i = 0; i < rotatedPositions.length; i++) {
            rotatedPositions[i] = getRotatedPosition(rotationDefinition, positionDefinition.getPoints()[i]);
        }

        return rotatedPositions;
    }

    private static Vector2 getRotatedPosition(int rotationDirection, Point position) {
        switch (rotationDirection) {
            case 1:
                //x=1-y, y=x
                return new Vector2(1 - position.y, position.x);
            case 2:
                //x = 1-x,y=1-y
                return new Vector2(1 - position.x, 1 - position.y);
            case 3:
                //x = y, y = 1-x
                return new Vector2(position.y, 1 - position.x);
            default:
                return new Vector2(position.x, position.y);
        }
    }

    /**
     *  Get rotation direction from float value
     * @param rotation rotation in float
     * @return piece rotation: 0 - UP, 1 - RIGHT, 2 - DOWN, 3 - LEFT
     */
    public static int getRotationDirection(float rotation) {
        return (int) getPieceRotation(rotation) / 90;

    }

    public static float getPieceRotation(float rotation) {
        float firstPieceRotation = rotation % 360;
        if (firstPieceRotation < 0) {
            firstPieceRotation = 360 + firstPieceRotation;
        }
        return firstPieceRotation;
    }



}