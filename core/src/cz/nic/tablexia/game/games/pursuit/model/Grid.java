/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Stack;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.pursuit.PursuitGame;
import cz.nic.tablexia.game.games.pursuit.action.MovePieceToPosition;
import cz.nic.tablexia.game.games.pursuit.action.SwitchPiecesAction;
import cz.nic.tablexia.game.games.pursuit.helper.PursuitPositionDefinition;
import cz.nic.tablexia.util.Point;

/**
 * Created by Václav Tarantík on 17.6.15.
 */
public class Grid extends Group {

    private int numberOfColumns;//number of rows respectively columns
    private Map<Integer, PuzzlePiece> piecesMap;
    private Texture texture;
    private Random random;
    private PursuitGame pursuitGame;

    public Grid(float x, float y, float size, int numberOfColumns, Texture texture, Random random, PursuitGame pursuitGame) {
        super();
        setPosition(x, y);
        setSize(size, size);

        setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());

        this.texture = texture;
        this.numberOfColumns = numberOfColumns;
        this.random = random;
        this.pursuitGame = pursuitGame;

        setTouchable(Touchable.childrenOnly);

        piecesMap = new LinkedHashMap<Integer, PuzzlePiece>();
        placePieces();

    }

    //creates pieces texture regions and places them in the grid
    private void placePieces() {
        float pieceSize = getWidth() / numberOfColumns;
        TextureRegion[][] split = new TextureRegion(texture).split(texture.getWidth() / numberOfColumns, texture.getHeight() / numberOfColumns);
        int i = 0;
        for (TextureRegion[] splitX : split) {
            for (TextureRegion splitXY : splitX) {

                PuzzlePiece puzzlePiece = new PuzzlePiece(i, splitXY);
                puzzlePiece.setSize(pieceSize, pieceSize);
                puzzlePiece.setOrigin(pieceSize / 2, pieceSize / 2);
                puzzlePiece.setZIndex(1);

                //in initial state, all pieces are at the correct positions
                puzzlePiece.setCorrectPosition(i);
                puzzlePiece.setActualPosition(i);
                i++;

                Point pieceInitialPosition = getPieceGridPositionInPixels(i);
                puzzlePiece.setPosition(pieceInitialPosition.x, pieceInitialPosition.y);

                piecesMap.put(piecesMap.size(), puzzlePiece);
                addActor(puzzlePiece);
            }
        }
        shufflePieces(random);
        if(pursuitGame.getGameDifficulty().equals(GameDifficulty.BONUS)){
            int pieceToCoverId = random.nextInt(i);
            piecesMap.get(pieceToCoverId).coverPuzzlePiece(pursuitGame);
        }
    }

    public void scaleGrid() {
        float pieceSize = getWidth() / (float) numberOfColumns;
        for (PuzzlePiece piece : piecesMap.values()) {
            piece.setSize(pieceSize, pieceSize);
            piece.setOrigin(pieceSize / 2, pieceSize / 2);
        }
        placePiecesInGrid();
    }

    //method shuffles pieces inside grid and sets its rotation randomly
    private void shufflePieces(Random random) {
        //random rotation
        for (PuzzlePiece piece : piecesMap.values()) {
            piece.setRotation(90 * (random.nextInt(4)));
        }
        //random position
        setRandomPositionsForPieces(random);
        placePiecesInGrid();
    }

    //finds piece at specified point that does not have the tabuId
    public PuzzlePiece pieceAtPoint(float x, float y, int tabuId) {
        Rectangle point = new Rectangle(x, y, 1, 1);
        for (PuzzlePiece piece : piecesMap.values()) {
            if (piece.getId() != tabuId) {
                Vector2 recalculatedCoords = new Vector2(piece.getX(), piece.getY());
                localToStageCoordinates(recalculatedCoords);
                Rectangle currentPieceRectangle = new Rectangle(recalculatedCoords.x, recalculatedCoords.y, piece.getWidth(), piece.getHeight());
                Rectangle intersectionRectangle = new Rectangle();
                if (Intersector.intersectRectangles(point, currentPieceRectangle, intersectionRectangle)) {
                    return piece;
                }
            }
        }
        return null;
    }

    public PuzzlePiece pieceAtPoint(float x, float y) {
        return pieceAtPoint(x, y, -1);
    }

    public PuzzlePiece pieceIntersectingWithDragged(int draggedPieceId) {
        PuzzlePiece draggedPiece = piecesMap.get(draggedPieceId);
        Vector2 pointRecalculatedPoint = new Vector2(draggedPiece.getX(), draggedPiece.getY());
        localToStageCoordinates(pointRecalculatedPoint);
        return pieceAtPoint(pointRecalculatedPoint.x + draggedPiece.getWidth() / 2, pointRecalculatedPoint.y + draggedPiece.getHeight() / 2, draggedPieceId);
    }

    public void unscalePieces(int draggedPieceId, int pieceNotToUnscaleId) {
        for (PuzzlePiece piece : piecesMap.values()) {
            if (piece.getId() != pieceNotToUnscaleId && piece.getId() != draggedPieceId) {
                piece.setScale(1);
                piece.setZIndex(1);
            }
        }
    }

    public void unscaleAllPieces() {
        for (PuzzlePiece piece : piecesMap.values()) {
            piece.setZIndex(1);
            piece.setScale(1f);
        }
    }

    public boolean switchPieces(PuzzlePiece draggedPiece, PuzzlePiece overlappedPiece, Action moveAction) {
        if (draggedPiece != null) {
            int draggedPieceActualPosition = draggedPiece.getActualPosition();
			Point draggedPiecePositionInPixels = getPieceGridPositionInPixels(draggedPieceActualPosition);

            //Piece was released above the empty area
            if (overlappedPiece == null) {
                addAction(new MovePieceToPosition(draggedPiece, draggedPiecePositionInPixels, moveAction));

            } else {
                int overlappedPieceActualPosition = overlappedPiece.getActualPosition();
                Point overlappedPiecePositionInPixels = getPieceGridPositionInPixels(overlappedPieceActualPosition);
                addAction(new SwitchPiecesAction(draggedPiece, draggedPiecePositionInPixels, overlappedPiece, overlappedPiecePositionInPixels, moveAction));
				return true;
			}
        }
		return false;
    }

    //returns the position of piece inside grid according to piece's actual position
    public Point getPieceGridPositionInPixels(int actualPositionInGrid) {
        float pieceSize = getWidth() / (float) numberOfColumns;
        Point pieceOrderNumbers = getPieceOrderNumbers(actualPositionInGrid);

        return new Point(pieceOrderNumbers.x * pieceSize, getHeight() - ((1 + pieceOrderNumbers.y) * pieceSize));
    }

    //Pieces are placed from upper left corner, method returns relative positions according to number of rows and columns
    private Point getPieceRelativePosition(int actualPositionInGrid) {
        Point pieceOrderNumbers = getPieceOrderNumbers(actualPositionInGrid);
        float relativeXPosition = pieceOrderNumbers.x / (float) numberOfColumns;
        float relativeYPosition = pieceOrderNumbers.y / (float) numberOfColumns;

        return new Point(relativeXPosition,relativeYPosition);
    }

    private Point getPieceOrderNumbers(int actualPositionInGrid) {
        int xOrderCoord = (actualPositionInGrid % numberOfColumns);
        int yOrderCoord = (actualPositionInGrid / numberOfColumns);

        return new Point(xOrderCoord,yOrderCoord);
    }

    private void placePiecesInGrid() {
        for (PuzzlePiece piece: piecesMap.values()) {
            Point piecePosition = getPieceGridPositionInPixels(piece.getActualPosition());
            piece.setPosition(piecePosition.x, piecePosition.y);
        }
    }

    //Method sets unique random currentPosition values to pieces
    private void setRandomPositionsForPieces(Random random){
        //Generating random sequence
        Stack possibleCardPositions = new Stack();
        for (int i = 0; i < numberOfColumns*numberOfColumns; i++) {
            possibleCardPositions.add(i);
        }
        //using random seed to shuffle sequence collection
        Collections.shuffle(possibleCardPositions, random);

        for (PuzzlePiece piece: piecesMap.values()) {
            piece.setActualPosition((Integer)possibleCardPositions.pop());
        }
    }

    public boolean allPiecesInCorrectPositions() {
        ArrayList<Integer> listToBeTransposed = new ArrayList<Integer>();
        //fills the arraylist with values for default piece rotation (0)
        for (PuzzlePiece piece: piecesMap.values()) {
            listToBeTransposed.add(piece.getCorrectPosition());
        }
        float firstPieceRotation = PursuitPositionDefinition.getPieceRotation(((PuzzlePiece)piecesMap.values().toArray()[0]).getRotation());
        //direction according to the piece rotation: 0 - UP, 1 - RIGHT, 2 - DOWN, 3 - LEFT
        int firstPieceRotationDirection = (int)firstPieceRotation/90;
        for (int i = 0; i < firstPieceRotationDirection; i++) {
            listToBeTransposed = transposeList(listToBeTransposed);
        }
        for (int i = 0; i < piecesMap.size(); i++) {
            PuzzlePiece piece = (PuzzlePiece)piecesMap.values().toArray()[i];
            float currentPieceRotation = piece.getRotation() % 360;
            //recalculating piece rotation in positive range of values
            if (currentPieceRotation < 0) {
                currentPieceRotation = 360 + currentPieceRotation;
            }
            if (piece.getActualPosition() != listToBeTransposed.get(i) || (currentPieceRotation != firstPieceRotation)) {
                return false;
            }
        }
        for (PuzzlePiece piece: piecesMap.values()) {
            if(piece.isCovered()) piece.showPuzzlePiece();
        }
        return true;
    }

    public int checkNextPuzzlePieces(PuzzlePiece puzzlePiece, boolean allowBlicking) {
        PuzzlePiece puzzleForBlicking = null;
        float middlePieceRotation = PursuitPositionDefinition.getPieceRotation(puzzlePiece.getRotation());
        //direction according to the piece rotation: 0 - UP, 1 - RIGHT, 2 - DOWN, 3 - LEFT
        int middlePieceRotationDirection = (int) middlePieceRotation / 90;
        int middlePuzzleActualPos = puzzlePiece.getActualPosition();
        int middlePuzzleCorrectPos = puzzlePiece.getCorrectPosition();

        int correctPos = 0;

        //up
        int nextCorrectPos = middlePuzzleCorrectPos - numberOfColumns;
        int following = checkIfNextIsFollowing((middlePuzzleCorrectPos / numberOfColumns) > 0, 0, nextCorrectPos, middlePuzzleActualPos, middlePieceRotationDirection, middlePieceRotation);
        correctPos += addIfFollows(following);
        if (following == 1 && puzzleForBlicking == null)
            puzzleForBlicking = ifCoveredFindPuzzlePiece(puzzlePiece, nextCorrectPos);

        //right
        nextCorrectPos = middlePuzzleCorrectPos + 1;
        following = checkIfNextIsFollowing(middlePuzzleCorrectPos % numberOfColumns < (numberOfColumns - 1), 3, nextCorrectPos, middlePuzzleActualPos, middlePieceRotationDirection, middlePieceRotation);
        correctPos += addIfFollows(following);
        if (following == 1 && puzzleForBlicking == null)
            puzzleForBlicking = ifCoveredFindPuzzlePiece(puzzlePiece, nextCorrectPos);

        //down
        nextCorrectPos = middlePuzzleCorrectPos + numberOfColumns;
        following = checkIfNextIsFollowing((middlePuzzleCorrectPos / numberOfColumns) < (numberOfColumns - 1), 2, nextCorrectPos, middlePuzzleActualPos, middlePieceRotationDirection, middlePieceRotation);
        correctPos += addIfFollows(following);
        if (following == 1 && puzzleForBlicking == null)
            puzzleForBlicking = ifCoveredFindPuzzlePiece(puzzlePiece, nextCorrectPos);

        //left
        nextCorrectPos = middlePuzzleCorrectPos - 1;
        following = checkIfNextIsFollowing((middlePuzzleCorrectPos % numberOfColumns) > 0, 1, nextCorrectPos, middlePuzzleActualPos, middlePieceRotationDirection, middlePieceRotation);
        correctPos += addIfFollows(following);
        if (following == 1 && puzzleForBlicking == null)
            puzzleForBlicking = ifCoveredFindPuzzlePiece(puzzlePiece, nextCorrectPos);

        if (allowBlicking) checkCoveredPuzzle();
        if (allowBlicking && puzzleForBlicking != null) puzzleForBlicking.blickPuzzlePiece();

        return correctPos;
    }

    private int addIfFollows(int following) {
        if (following == 1 || following == (-1)) return 1;
        return 0;
    }

    /**
     * @param condition - condition if next puzzle in this direction exists
     * @param direction - 0 up, 1 left, 2 down, 3 right
     * @param nextCorrectPos
     * @param middlePuzzleActualPos
     * @param middlePieceRotationDirection
     * @param middlePieceRotation
     * @return   1 if nextCorrectPos follows up to middlePuzzleActualPos
     *          -1 if middlePuzzleActualPos does not have following puzzle in this direction
     *           0 if nextCorrectPos does not follow up to middlePuzzleActualPos
     */
    private int checkIfNextIsFollowing(boolean condition, int direction, int nextCorrectPos, int middlePuzzleActualPos, int middlePieceRotationDirection, float middlePieceRotation) {
        if (condition) {
            if (findAndCheckNext(direction, nextCorrectPos, middlePuzzleActualPos, middlePieceRotationDirection, middlePieceRotation))
                return 1;
            else return 0;
        }
        return (-1);
    }

    /**
     * Check if covered puzzle has all it's next following puzzles
     */
    private void checkCoveredPuzzle() {
        PuzzlePiece coveredPuzzle = null;
        for (PuzzlePiece piece : piecesMap.values()) {
            if (piece.isCovered()) {
                coveredPuzzle = piece;
                break;
            }
        }

        if (coveredPuzzle != null) {
            int correctPos = checkNextPuzzlePieces(coveredPuzzle, false);
            if (correctPos == 4) coveredPuzzle.showPuzzlePiece();
            else coveredPuzzle.coverPuzzlePiece(pursuitGame);
        }
    }

    private PuzzlePiece ifCoveredFindPuzzlePiece(PuzzlePiece middlePuzzlePiece, int puzzleCorrectPosition) {
        if (middlePuzzlePiece.isCovered()) return middlePuzzlePiece;
        PuzzlePiece puzzlePiece = piecesMap.get(puzzleCorrectPosition);
        if (puzzlePiece.isCovered()) return puzzlePiece;
        return null;
    }

    /**
     * Find puzzle that should be next to middlePuzzleActualPos and check if it is next to it rotated the same correct direction
     *
     * @param direction
     * @param nextCorrectPos
     * @param middlePuzzleActualPos
     * @param middlePieceRotationDirection
     * @param middlePieceRotation
     * @return true if nextCorrectPos follows up to middlePuzzleActualPos
     */
    private boolean findAndCheckNext(int direction, int nextCorrectPos, int middlePuzzleActualPos, int middlePieceRotationDirection, float middlePieceRotation) {
        PuzzlePiece pieceNext = piecesMap.get(nextCorrectPos); //piece that should be next

        int correctActualNextPos = getNextCorrectPos(middlePieceRotationDirection, middlePuzzleActualPos, direction);
        if (correctActualNextPos == pieceNext.getActualPosition() && PursuitPositionDefinition.getPieceRotation(pieceNext.getRotation()) == middlePieceRotation)
            return true;
        return false;
    }

    /**
     * Find puzzle that lies next to middlePuzzleActualPos in specified directon
     *
     * @param middlePieceRotationDirection
     * @param middlePuzzleActualPos
     * @param nextPos
     * @return position of next puzzle piece to middlePuzzleActualPos in direction middlePieceRotationDirection
     */
    private int getNextCorrectPos(int middlePieceRotationDirection, int middlePuzzleActualPos, int nextPos) {
        int nextAndRotation = (middlePieceRotationDirection + nextPos) % 4;
        if (nextAndRotation == 0) {
            if (middlePuzzleActualPos / numberOfColumns > 0)
                return middlePuzzleActualPos - numberOfColumns;
        } else if (nextAndRotation == 1) {
            if (middlePuzzleActualPos % numberOfColumns > 0) return middlePuzzleActualPos - 1;
        } else if (nextAndRotation == 2) {
            if (middlePuzzleActualPos / numberOfColumns < (numberOfColumns - 1))
                return middlePuzzleActualPos + numberOfColumns;
        } else {
            if (middlePuzzleActualPos % numberOfColumns < (numberOfColumns - 1))
                return middlePuzzleActualPos + 1;
        }
        return (-1); //no position (edge)
    }


    //Method transposes the matrix to check whether the result shape is correctly folded with rotation
    /*  One usage of this method on values in list does the following:
        |0|1|2|      |6|3|0|
        |3|4|5|  ->  |7|4|1|
        |6|7|8|      |8|5|2|
     */

    public ArrayList<Integer> transposeList(ArrayList<Integer> listToBeTransposed) {
        ArrayList<Integer> listToBeReturned = new ArrayList<Integer>(9);
        int repetitionCounter = numberOfColumns - 1;
        for (int i = repetitionCounter; i >= 0; i--) {
            for (int j = 0; j < numberOfColumns; j++) {
                listToBeReturned.add(listToBeTransposed.get(listToBeTransposed.size() - 1 - ((numberOfColumns * j) + repetitionCounter)));
            }
            repetitionCounter--;
        }
        return listToBeReturned;
    }

    //returns its rotation according to the rotation of the first piece inside grid
    @Override
    public float getRotation() {
        return piecesMap.get(piecesMap.keySet().toArray()[0]).getRotation();
    }

    //Gets for testing

    public Map<Integer, PuzzlePiece> getPiecesMap() {
        return piecesMap;
    }

    public int getNumberOfColumns() {
        return numberOfColumns;
    }
}
