/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.pursuit.action;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import cz.nic.tablexia.util.Point;
import cz.nic.tablexia.game.games.pursuit.model.PuzzlePiece;

/**
 * Created by Václav Tarantík on 3.8.15.
 */
public class SwitchPiecesAction extends SequenceAction {
    public SwitchPiecesAction(final PuzzlePiece draggedPiece, Point draggedPieceRelativePosition, final PuzzlePiece overlappedPiece, Point overlappedPieceRelativePosition, Action onSwitchFinishedAction){
        addAction(Actions.parallel(new MovePieceToPosition(draggedPiece, overlappedPieceRelativePosition),
                new MovePieceToPosition(overlappedPiece, draggedPieceRelativePosition)));
        addAction(onSwitchFinishedAction);

        int overlappedPieceActualPosition = overlappedPiece.getActualPosition();
        int draggedPieceActualPosition = draggedPiece.getActualPosition();

        draggedPiece.setActualPosition(overlappedPieceActualPosition);
        overlappedPiece.setActualPosition(draggedPieceActualPosition);
    }
}
