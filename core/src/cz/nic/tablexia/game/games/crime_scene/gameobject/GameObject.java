/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.gameobject;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.crime_scene.CrimeSceneGame;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.ui.actionwidget.Action;
import cz.nic.tablexia.util.ui.actionwidget.ActionsStripWidget;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;

/**
 * Clickable object placed in Crime scene map
 *
 * Created by danilov on 18.4.16.
 */
public class GameObject extends Group implements Action.ActionListener {
    public static final String EVENT_DROP_DOWN = "drop down card";

    private ActionsStripWidget actionsStripWidget;
    private CrimeSceneGame crimeSceneGame;
    private GameObjectType gameObjectType;
    private Action action;
    private Position mapPosition;
    private boolean dragging;
    private boolean highlighted;

    public GameObject(ActionsStripWidget actionsStripWidget, CrimeSceneGame crimeSceneGame, GameObjectType gameObjectType, int actionCardSize, Position mapPosition) {
        this.actionsStripWidget = actionsStripWidget;
        this.crimeSceneGame = crimeSceneGame;
        this.gameObjectType = gameObjectType;
        this.mapPosition = mapPosition;

        //TODO change to the objects real size, set X,Y by GameObjectType attributes
        createGameObject(actionCardSize);
    }

    private void createGameObject(final int actionCardSize) {
        action = TablexiaSettings.getInstance().isRunningOnMobileDevice() ? new CrimeSceneAction(gameObjectType.getResource(), 0, actionCardSize, crimeSceneGame, gameObjectType.getActionNumber(), this) :
                new DesktopCrimeSceneAction(gameObjectType.getResource(), 0, actionCardSize, crimeSceneGame, gameObjectType.getActionNumber(), this);
        action.getListeners().removeAll(action.getListeners(), false);
        action.setDragListener();
        action.addActionListener(this);
        action.setActionsStripWidget(actionsStripWidget);
        action.setClickable();
        action.setPosition(mapPosition.getX(), mapPosition.getY());
        addActor(action);
    }

    @Override
    public void onActionDrag(Action lastAction) {
        changeToCardDrawable();
    }

    @Override
    public void onActionDoubleTap(Action action) {
    }

    @Override
    public void onActionDrop(Action action, int collidesWithNumber) {
        AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_DROP_DOWN);
        if (collidesWithNumber != Action.NO_COLLISION_NUMBER) {
            // FIXME: 10/30/17 why to store action number if using ordinal?
            if (!crimeSceneGame.isActionAdded(action)) actionsStripWidget.addSelectedAction(GameObjectType.values()[action.getActionNumber()].getActionTexturePath(), collidesWithNumber, crimeSceneGame, action.getActionNumber());
            if (!actionsStripWidget.isNotFull()) crimeSceneGame.getStartButton().setEnabled(true);

            crimeSceneGame.setDimmerVisible(false);

        } else {
            if (dragging) { // highlight object
                GameObject obj = crimeSceneGame.createGameObject(crimeSceneGame.getHighlightedGameObject().getGameObjectType());
                obj.changeToCardDrawable();
                crimeSceneGame.highlightGameObject(obj);
            } else { // recreate object in former area
                crimeSceneGame.addGameObject(crimeSceneGame.createGameObject(GameObjectType.getGameObjectTypeForActionNumber(action.getActionNumber())));
                crimeSceneGame.setDimmerVisible(false);
            }

            if (actionsStripWidget.isNotFull()) crimeSceneGame.getStartButton().setEnabled(false);
        }

    }

    public void changeToCardDrawable() {
        action.setDrawable(new TextureRegionDrawable(crimeSceneGame.getScreenTextureRegion(gameObjectType.getActionTexturePath())));
        action.setSize(CrimeSceneGame.ACTION_CARD_DRAG_SIZE, CrimeSceneGame.ACTION_CARD_DRAG_SIZE);
    }
    
    public void resetPosition(){
        crimeSceneGame.setHighlightedGameObject(this);
        crimeSceneGame.highlightGameObject(this);
        onActionDrop(action, Action.NO_COLLISION_NUMBER);
    }

    public Action getAction() {
        return action;
    }

    public GameObjectType getGameObjectType() {
        return gameObjectType;
    }

    public boolean isDragging() {
        return dragging;
    }

    public void setDragging(boolean dragging) {
        this.dragging = dragging;
    }

    public boolean isHighlighted() {
        return highlighted;
    }

    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
    }

    //Gets for testinng

    public Position getMapPosition() {
        return mapPosition;
    }

}