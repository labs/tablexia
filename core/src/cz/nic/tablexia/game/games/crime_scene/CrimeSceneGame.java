/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.crime_scene.actors.PlaySoundScreen;
import cz.nic.tablexia.game.games.crime_scene.assets.CrimeSceneAssets;
import cz.nic.tablexia.game.games.crime_scene.gameobject.GameObject;
import cz.nic.tablexia.game.games.crime_scene.gameobject.GameObjectType;
import cz.nic.tablexia.game.games.crime_scene.listeners.CrimeSceneDragListener;
import cz.nic.tablexia.game.games.crime_scene.listeners.DesktopCrimeSceneDragListener;
import cz.nic.tablexia.game.games.crime_scene.model.CrimeSceneDifficulty;
import cz.nic.tablexia.game.games.crime_scene.model.CrimeSceneGameState;
import cz.nic.tablexia.game.games.crime_scene.model.LevelDefinition;
import cz.nic.tablexia.game.games.crime_scene.model.ResultMapping;
import cz.nic.tablexia.game.games.crime_scene.model.bonus.BonusAssetsManager;
import cz.nic.tablexia.game.games.crime_scene.model.bonus.BonusSoundsSetting;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.resolvers.CrimeSceneScoreResolver;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.actionwidget.Action;
import cz.nic.tablexia.util.ui.actionwidget.ActionContainer;
import cz.nic.tablexia.util.ui.actionwidget.ActionsStripWidget;
import cz.nic.tablexia.util.ui.button.GameImageTablexiaButton;
import cz.nic.tablexia.util.ui.button.TablexiaButton;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

/**
 * Created by danilov on 11.4.16.
 */
public class CrimeSceneGame extends AbstractTablexiaGame<CrimeSceneGameState> {
    public  static final String SOUND_SCREEN                = "sound screen group";
    public  static final String STRIP_WIDGET                = "action strip widget";
    public  static final String BUTTON_REPLAY               = "replay button";
    public  static final String BUTTON_START                = "start button";
    public  static final String OBJECTS_LAYER               = "objects layer";
    public  static final String GAME_MAP                    = "game map group";

    public  static final String EVENT_STOP_PLAYED           = "event stop played";
    public  static final String EVENT_START_PLAY            = "event start play";
    public  static final String EVENT_FINISH_SOUND          = "event finish sound index: ";
    public  static final String EVENT_ACTION_CONTROLLED     = "event control action: ";
    public  static final String EVENT_HIGHLIGHTED_GO        = "higlighted object shown";


    public  static final int    ACTION_CARD_DRAG_SIZE       = 100;
    private static final int    ACTIONS_PANEL_WIDTH         = 155;
    private static final int    START_BUTTON_WIDTH          = 135;
    private static final int    START_BUTTON_HEIGHT         = 72;
    private static final int    REPLAY_BUTTON_WIDTH         = 187;
    private static final int    REPLAY_BUTTON_HEIGHT        = 72;
    private static final int    START_BUTTON_OFFSET_X       = 30;
    private static final int    BOTTOM_OFFSET_Y             = 10;
    private static final int    GAME_MAP_PADDING            = 25;
    private static final int    GAME_MAP_SIZE               = 625;
    private static final int    ACTION_CARD_EXTRA_LARGE     = 2 * ACTION_CARD_DRAG_SIZE;

    private static final String SCORE_KEY_COUNT             = CrimeSceneScoreResolver.SCORE_KEY_COUNT;
    private static final String RESULT_SCORE_COUNT          = CrimeSceneScoreResolver.RESULT_SCORE_COUNT;
    private static final String REPLAY_COUNT                = CrimeSceneScoreResolver.REPLAY_COUNT;
    private static final String FINISHED_ROUNDS             = CrimeSceneScoreResolver.FINISHED_ROUNDS;

    public  static final float  ANIMATION_DURATION          = 0.3f;
    private static final float  FADE_DURATION               = 1f;
    private static final float  DIMMER_ALPHA                = 2 / 3f;

    private static final int    MAX_TASK_REPEAT             = 1;
    public  static final int    MAX_ROUNDS                  = 4;
    public  static final int    MAX_WRONG_ROUNDS            = 2;

    public static final int    ANIMATION_DELAY             = 1000;
    public static final int    ANIMATION_DELAY_MULTIPLIER  = 3;

    private static int              correctSounds = 0;

    private Group                   contentGroup;
    private Group                   gameMap;
    private Group                   objectsLayer;
    private Group                   highlightedGameObjectLayer;
    private Image                   dimmer;
    private Image                   gameMapTexture;
    private Image                   stripWidgetBackground;
    private ActionsStripWidget      actionsStripWidget;

    private GameImageTablexiaButton startButton, replayButton;
    private PlaySoundScreen         playSoundScreen;
    private BonusAssetsManager bonusAssetsManager;
    private GameObject              highlightedGameObject;
    private Music                   actualPlayingMusic;
    private List<ActionContainer>   selectedActions;
    private boolean                 actionFinished;
    private boolean                 sequenceStarted = false;
    private float                   secondsElapsed;
    private int 	                selectedActionPosition = 0;
    private int                     repeatTaskCount = 0;
    private int                     actualNumberOfGameObjects;
    private int                     formerNumberOfGameObjects;
    private int                     errorsCount = 0;
    private int                     level = 1;
    private int                     wrongRounds = 0;

    @Override
    protected void gameLoaded(java.util.Map gameState) {
        setUpActionsStripWidget();

        highlightedGameObjectLayer = new Group();
        setUpDimmer();

        contentGroup = new Group();
        getStage().addActor(contentGroup);

        stripWidgetBackground = new Image(getScreenTextureRegion(CrimeSceneAssets.STRIP_WIDGET_BACKGROUND));
        stripWidgetBackground.setPosition(getViewportWidth() - ACTIONS_PANEL_WIDTH, getViewportBottomY());

        gameMap = new Group();
        gameMap.setName(GAME_MAP);
        setUpGameMap();

        objectsLayer = new Group();
        objectsLayer.setName(OBJECTS_LAYER);

        generateGameObjects();

        gameMap.addActor(objectsLayer);
        contentGroup.addActor(gameMap);
        contentGroup.addActor(dimmer);
        contentGroup.addActor(stripWidgetBackground);
        contentGroup.addActor(actionsStripWidget);
        contentGroup.addActor(highlightedGameObjectLayer);

        createStartButton();
        createReplayButton();

        setUpSizeAndPositionOfComponents();
        preparePlaySoundScreen(false, false);
    }

    @Override
    protected void gameVisible() {
        if(playSoundScreen!=null)
            playSoundScreen.playSounds();
    }

    @Override
    protected void gameResumed() {
        if(playSoundScreen != null && playSoundScreen.isSequencePlaying())
            playSoundScreen.resumeBonus();
    }

    @Override
    protected void gamePaused(java.util.Map gameState) {
        if(playSoundScreen != null && playSoundScreen.isSequencePlaying())
            playSoundScreen.pauseBonus();
    }

    @Override
    protected void gameDisposed() {
        disposeBonusAssets();
    }

    @Override
    protected void gameEnd() {
        clearBonusAssets();
        getStage().clear();
    }

    @Override
    public boolean isSoundMandatory() {
        return true;
    }

    @Override
    protected void screenResized(int width, int height) {
       setUpSizeAndPositionOfComponents();
    }

    @Override
    public void setScrollableLayoutFocus() {
        if(actionsStripWidget != null)
            actionsStripWidget.setScrollFocus();
    }

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(new SummaryMessage(SummaryImage.STATS,
                getFormattedText(RESULT_SCORE_COUNT,
                        game.getGameScore(SCORE_KEY_COUNT, String.valueOf(correctSounds)) + " / " + getMaxScore())));
    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        return ResultMapping.getResultMappingForGameResult(gameResult).getTextKey();
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        return ResultMapping.getResultMappingForGameResult(gameResult).getSoundName();
    }

    @Override
    protected CrimeSceneGameState prepareGameData(java.util.Map gameState) {
        CrimeSceneDifficulty crimeSceneDifficulty = CrimeSceneDifficulty.getCrimeSceneDifficultyForGameDifficulty(getGameDifficulty());
        actualNumberOfGameObjects = crimeSceneDifficulty.getNumberOfObjects();

        CrimeSceneGameState crimeSceneGameState = CrimeSceneGameState.CrimeSceneGameStateFactory.createInstance(getRandom(), crimeSceneDifficulty);

        if (getGameDifficulty().equals(GameDifficulty.BONUS)) {
            bonusAssetsManager = new BonusAssetsManager();
            loadBonusAtlas(crimeSceneGameState.getUsedLevel());
        }
        return crimeSceneGameState;
    }

    private void loadBonusAtlas(LevelDefinition.Level used) {
        if (bonusAssetsManager == null)
            return;

        bonusAssetsManager.loadAtlas(getGameDifficultyAtlasPath(used.getDifficulty()));
    }

    @Override
    protected void prepareScreenAtlases(List<String> atlasesNames) {
        super.prepareScreenAtlases(atlasesNames);
    }

    @Override
    public TextureRegion getScreenTextureRegion(String regionName) {
        TextureRegion textureRegion = null;
        if(bonusAssetsManager != null && bonusAssetsManager.containsRegion(regionName))
            textureRegion = bonusAssetsManager.getTextureRegion(regionName);

        if(textureRegion != null)
            return textureRegion;

        GameDifficulty usedDifficulty = getData().getUsedLevel().getDifficulty();
        textureRegion = getTextureRegionForAtlas(getGameDifficultyAtlasPath(usedDifficulty), regionName, null, false);

        if(textureRegion == null){
            return super.getScreenTextureRegion(regionName);
        }

        return textureRegion;
    }

    @Override
    protected void gameAct(float delta) {
        if (sequenceStarted) {
            if (actionFinished && (secondsElapsed > ANIMATION_DURATION)) {
                secondsElapsed = 0;
                if (selectedActionPosition < selectedActions.size()) {
                    actionFinished = false;
                    actionsStripWidget.displayProcessedActions(selectedActionPosition,
                            false,
                            new Runnable() {
                                @Override
                                public void run() {
                                    int selectedActionNumber = actionsStripWidget.getSelectedActions().get(selectedActionPosition).getAction().getActionNumber();
                                    int correctActionNumber = getData().getSolution().get(selectedActionPosition).getActionNumber();

                                    if (selectedActionNumber == correctActionNumber) {
                                        actionsStripWidget.getSelectedActions().get(selectedActionPosition).getAction().addAction(Actions.color(Color.GREEN));
                                        correctSounds++;
                                    } else {
                                        actionsStripWidget.getSelectedActions().get(selectedActionPosition).getAction().addAction(Actions.color(Color.FIREBRICK));
                                        errorsCount++;
                                    }

                                    setActionControlledEvent(selectedActionNumber);

                                    if(selectedActionPosition == selectedActions.size() -1) restartGameForNextRound();
                                    selectedActionPosition++;
                                    actionFinished = true;
                                }
                            });
                } else {
                    sequenceStarted = false;
                    selectedActionPosition = 0;
                }
            } else {
                secondsElapsed = secondsElapsed + delta;
            }
        }
    }

////////////////////// SET UP COMPONENTS

    private void setUpGameMap() {
        gameMap.removeActor(gameMapTexture);
        gameMapTexture = ScaleUtil.createImageToHeight(getScreenTextureRegion(getData().getUsedLevel().getTexturePath()), GAME_MAP_SIZE);
        gameMap.addActor(gameMapTexture);
        gameMapTexture.setZIndex(0);
        setUpSizeAndPositionOfComponents();
    }

    private void setUpSizeAndPositionOfComponents() {
        if (playSoundScreen != null) playSoundScreen.setBounds(0, getViewportBottomY(), getViewportWidth(), getViewportHeight());

        if(startButton != null) startButton.setPosition(getViewportWidth() - actionsStripWidget.getWidth() - startButton.getWidth() - START_BUTTON_OFFSET_X,
                getViewportBottomY() + BOTTOM_OFFSET_Y);

        if(startButton != null && replayButton != null) replayButton.setPosition(getViewportWidth() - actionsStripWidget.getWidth() - startButton.getWidth() - replayButton.getWidth() - START_BUTTON_OFFSET_X * 2,
                getViewportBottomY() + BOTTOM_OFFSET_Y);

        actionsStripWidget.setBounds(getViewportWidth() - ACTIONS_PANEL_WIDTH, getViewportBottomY(), ACTIONS_PANEL_WIDTH, getViewportHeight());
        stripWidgetBackground.setY(getViewportBottomY());

        if(gameMap != null && gameMapTexture != null){
            gameMap.setBounds(((getViewportWidth() - stripWidgetBackground.getWidth()) - gameMapTexture.getWidth()) / 2, getViewportBottomY() + (getViewportHeight() - gameMapTexture.getHeight()) / 2 + GAME_MAP_PADDING, getViewportWidth(), getViewportHeight());
        }

        dimmer.setBounds(getViewportLeftX(), getViewportBottomY(), getViewportWidth(), getViewportHeight());
        highlightedGameObjectLayer.setBounds(getViewportLeftX(), getViewportBottomY(), getViewportWidth(), getViewportHeight());
        if(getHighlightedGameObject() != null) highlightedGameObject.setPosition(getViewportWidth() / 2 - ACTION_CARD_DRAG_SIZE / 2 - stripWidgetBackground.getWidth(), getViewportHeight() / 2 - ACTION_CARD_DRAG_SIZE / 2);
    }

    private void setUpActionsStripWidget() {
        actionsStripWidget = new ActionsStripWidget(0f, null, this, ACTION_CARD_DRAG_SIZE, ACTION_CARD_DRAG_SIZE, ANIMATION_DURATION, CrimeSceneAssets.CONTROL_ACTUAL, CrimeSceneAssets.CONTROL_NEXT, getData().getSolution().size(), ActionsStripWidget.StartButtonControl.IS_FULL) {
            @Override
            public void onActionDrop(Action action, int collidesWithNumber) {
                //add to stripwidget or recreate in-game object
                if (collidesWithNumber != Action.NO_COLLISION_NUMBER) {
                    addSelectedAction(action.getTexturePath(), collidesWithNumber, CrimeSceneGame.this, action.getActionNumber());
                    if(!actionsStripWidget.isNotFull()) startButton.setEnabled(true);
                } else {
                    addGameObject(createGameObject(GameObjectType.getGameObjectTypeForActionNumber(action.getActionNumber())));
                    if(actionsStripWidget.isNotFull()) startButton.setEnabled(false);
                }
            }

            @Override
            protected void prepareSelectedActions(String selectedActionsString, AbstractTablexiaGame tablexiaGame) {
                if (selectedActionsString != null) {
                    String[] actionStrings = selectedActionsString.split(ActionsStripWidget.SELECTED_ACTIONS_STRING_SEPARATOR);
                    for (int i = 0; i < actionStrings.length; i++) {
                        int actionNumber = Integer.valueOf(actionStrings[i]);
                        addSelectedAction(GameObjectType.getGameObjectTypeForActionNumber(actionNumber).getActionTexturePath(), i, tablexiaGame, actionNumber);
                    }
                }
            }

            @Override
            public void onActionDoubleTap(Action action) {
                if (!TablexiaSettings.getInstance().isRunningOnMobileDevice() && containsAction(action)) {
                    removeActionAndUpdateView(action.getOrderNumber());
                    addGameObject(createGameObject(GameObjectType.getGameObjectTypeForActionNumber(action.getActionNumber())));
                    if (actionsStripWidget.isNotFull()) startButton.setEnabled(false);
                }
            }
        };
        actionsStripWidget.setPosition(getViewportWidth() - ACTIONS_PANEL_WIDTH, getViewportBottomY());
        actionsStripWidget.setSize(ACTIONS_PANEL_WIDTH, getViewportHeight());
        actionsStripWidget.getActualPointer().setVisible(false);
        actionsStripWidget.enableControl();
        actionsStripWidget.setName(STRIP_WIDGET);
    }

    private void createStartButton() {
        startButton = new GameImageTablexiaButton(
            getText(CrimeSceneAssets.DONE_TEXT),
            new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_YES_ICON))
        );
        startButton.setButtonSize(START_BUTTON_WIDTH, START_BUTTON_HEIGHT)
                .setButtonPosition(getViewportWidth() - actionsStripWidget.getWidth() - START_BUTTON_WIDTH - START_BUTTON_OFFSET_X,
                        getViewportBottomY() + BOTTOM_OFFSET_Y)
                .setDisabled()
                .setInputListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        startButton.setDisabled();
                        replayButton.setDisabled();
                        prepareValuesForValidation();
                    }
                });

        startButton.setName(BUTTON_START);
        contentGroup.addActor(startButton);
        actionsStripWidget.setStartButton(startButton);
        actionsStripWidget.enableStartButtonControl(true);
    }

    private void createReplayButton() {
        replayButton = new GameImageTablexiaButton(
            getText(CrimeSceneAssets.REPLAY),
            new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_REPEAT_ICON))
        );
        replayButton.setButtonSize(REPLAY_BUTTON_WIDTH, REPLAY_BUTTON_HEIGHT)
                .setButtonPosition(getViewportWidth() - actionsStripWidget.getWidth() - START_BUTTON_WIDTH - REPLAY_BUTTON_WIDTH - START_BUTTON_OFFSET_X * 2,
                        getViewportBottomY() + BOTTOM_OFFSET_Y)
                .setEnabled()
                .setInputListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        hideLastTouchedAction();
                        playAllSounds(true);
                        repeatTaskCount++;
                        setGameScore(REPLAY_COUNT, repeatTaskCount);
                        if(getReplayCountLeft() == 0) replayButton.setDisabled();
                    }
                });

        replayButton.setName(BUTTON_REPLAY);
        contentGroup.addActor(replayButton);
    }

    private void setUpDimmer() {
        dimmer = new Image(ApplicationAtlasManager.getInstance().getColorTexture(Color.BLACK));
        Color color = dimmer.getColor();
        dimmer.setColor(color.r, color.g, color.b, color.a * DIMMER_ALPHA);
        dimmer.setBounds(0, getViewportBottomY(), getViewportWidth(), getViewportHeight());

        highlightedGameObjectLayer.addListener(new InputListener() {
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                return true;
            }

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                hideLastTouchedAction();
            }
        });

        setDimmerVisible(false);
    }

    private void prepareValuesForValidation() {
        errorsCount = 0;
        selectedActionPosition = 0;
        repeatTaskCount = 0;
        sequenceStarted = true;
        actionFinished = true;
        selectedActions = actionsStripWidget.getSelectedActions();
        actionsStripWidget.disableControl();
        actionsStripWidget.getActualPointer().setVisible(true);
    }

    private void preparePlaySoundScreen(boolean useFadeIn, boolean repeat) {
        TextureRegion actionCardTexture = getScreenTextureRegion(CrimeSceneAssets.BLANK_ACTION_CARD);

        List<Music> musicList = new ArrayList<>();
        for(GameObjectType gameObjectType : getData().getSolution()) {
            Log.info(getClass(), "Music: " + gameObjectType.getSoundPath());
            Music music = getMusic(gameObjectType.getSoundPath());
            musicList.add(music);
        }

        Music bonusBack = null;
        float bonusVolume = 1.0f;
        if(getGameDifficulty().equals(GameDifficulty.BONUS)){
            BonusSoundsSetting setting = getData().getBonusSetting();
            Log.info(getClass()," Bonus: " + getData().getBonusSetting());
            bonusBack = getMusic(setting.getSoundPath());
            bonusVolume = setting.getVolume();
        }


        playSoundScreen = new PlaySoundScreen(this, ApplicationAtlasManager.getInstance().getColorTexture(Color.BLACK), actionCardTexture, musicList, bonusBack, bonusVolume);
        playSoundScreen.setBounds(0, getViewportBottomY(), getViewportWidth(), getViewportHeight());
        playSoundScreen.setName(SOUND_SCREEN);
        if(useFadeIn) playSoundScreen.addAction(new SequenceAction(Actions.alpha(0), Actions.fadeIn(FADE_DURATION)));
        getStage().addActor(playSoundScreen);
    }

    private void setHighLightGameObjectListener(GameObject gameObject) {
        gameObject.addListener(TablexiaSettings.getInstance().isRunningOnMobileDevice() ?
                new CrimeSceneDragListener(gameObject, this) :
                new DesktopCrimeSceneDragListener(gameObject, this));
    }

    public void resolveClickOnGameObject(GameObject gameObject, float clickX, float clickY) {
        highlightedGameObjectLayer.clearChildren();
        highlightedGameObjectLayer.addActor(gameObject);
        setDimmerVisible(true);
        setHighlightedGameObject(gameObject);
        centerGameObject(gameObject, clickX, clickY);
    }

    public void highlightGameObject(GameObject gameObject) {
        if(gameObject.isHighlighted())
            return;

        gameObject.setHighlighted(true);
        removeDuplicateObject(gameObject);
        highlightedGameObjectLayer.addActor(gameObject);

        gameObject.getAction().setBounds(0, 0, ACTION_CARD_EXTRA_LARGE, ACTION_CARD_EXTRA_LARGE);
        gameObject.setPosition(getViewportWidth() / 2 - gameObject.getAction().getWidth() / 2 - actionsStripWidget.getWidth() / 2,
                getViewportHeight() / 2 - gameObject.getAction().getHeight() / 2);

        setDimmerVisible(true);
        replayMusic(gameObject);
    }

    public void resetHighLightLayer(){
        if(highlightedGameObjectLayer.hasChildren()) {
            highlightedGameObjectLayer.clearChildren();
            setDimmerVisible(false);
        }
    }

    private void centerGameObject(GameObject gameObject, float clickX, float clickY) {
        Vector2 stageCoordinate = gameObject.localToParentCoordinates(new Vector2(clickX, clickY));
        if(gameObject.isHighlighted()) {
            gameObject.setPosition(stageCoordinate.x - ACTION_CARD_DRAG_SIZE / 2,
                    stageCoordinate.y - ACTION_CARD_DRAG_SIZE / 2 );
        } else {
            gameObject.setPosition(stageCoordinate.x - ACTION_CARD_DRAG_SIZE / 2 + stripWidgetBackground.getWidth() / 2,
                    stageCoordinate.y - ACTION_CARD_DRAG_SIZE / 2 + (gameMap.getY() - getViewportBottomY()));
        }
        gameObject.getAction().setPosition(0,0);
    }

    private void hideLastTouchedAction() {
        setDimmerVisible(false);
        if(actualPlayingMusic != null) {
            actualPlayingMusic.stop();
            actualPlayingMusic.dispose();
        }

        if(getHighlightedGameObject() != null && !isActionAdded(getHighlightedGameObject().getAction()) && !isObjectInObjectsLayer(getHighlightedGameObject())) {
            objectsLayer.addActor(createGameObject(getHighlightedGameObject().getGameObjectType()));
        }
        highlightedGameObjectLayer.clearChildren();
    }

    public boolean isActionAdded(Action action) {
        for(ActionContainer actionContainer : actionsStripWidget.getSelectedActions()) {
            if(actionContainer.getAction().getActionNumber() == action.getActionNumber()) return true;
        }
        return false;
    }
    ////////////////////// CONTROL OF GAME

    public void gameComplete() {
        endGame();
        showGameResultDialog();
    }

    private void restartGameForNextRound() {
        formerNumberOfGameObjects = actualNumberOfGameObjects;

        if (errorsCount > 0) {
            wrongRounds++;
            if (actualNumberOfGameObjects > CrimeSceneDifficulty.getCrimeSceneDifficultyForGameDifficulty(getGameDifficulty()).getNumberOfObjects()) {
                actualNumberOfGameObjects--;
            }
        } else {
            wrongRounds = 0;
            actualNumberOfGameObjects++;
        }

        setGameScore(FINISHED_ROUNDS, level);
        setGameScore(SCORE_KEY_COUNT, correctSounds);

        if (level < MAX_ROUNDS && wrongRounds < MAX_WRONG_ROUNDS) {
            getData().setSolution(getGameDifficulty(), ++level, actualNumberOfGameObjects);

            //Timer for leaving last answer visible for a second
            //Need to use two consecutive tasks, because of flashing task screen
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    playAllSounds(false);
                }
            }, ANIMATION_DELAY);

            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    resetActionsStripWidget();
                    loadBonusAtlas(getData().getUsedLevel());
                    generateGameObjects();
                    setHighlightedGameObject(null);
                    setUpGameMap();
                    if (getReplayCountLeft() > 0) replayButton.setEnabled();
                }
            }, ANIMATION_DELAY * ANIMATION_DELAY_MULTIPLIER);

        } else {
            gameComplete();
            errorsCount = 0;
            wrongRounds = 0;
            correctSounds = 0;
            level = 1;
            setHighlightedGameObject(null);
        }
    }

    private void resetActionsStripWidget() {
         for(int i = formerNumberOfGameObjects - 1; i >= 0; i--) {
            actionsStripWidget.removeAction(i);
        }
        actionsStripWidget.setMaximumActionsCount(actualNumberOfGameObjects);

        sequenceStarted = false;
        actionFinished = false;
        actionsStripWidget.getActualPointer().setVisible(false);
        actionsStripWidget.enableControl();
    }

    private void generateGameObjects() {
        for(GameObjectType gameObjectType : getData().getSolution()) {
            addGameObject(createGameObject(gameObjectType));
        }
    }

    public GameObject createGameObject(GameObjectType gameObjectType) {
        LevelDefinition.Level usedLevel = getData().getUsedLevel();

        GameObject gameObject =  new GameObject(actionsStripWidget, this, gameObjectType, ACTION_CARD_DRAG_SIZE, LevelDefinition.getGameObjectsPosition(usedLevel, gameObjectType));
        gameObject.setPosition(gameObject.getX() + gameMapTexture.getX(), gameObject.getY() + gameMapTexture.getY());
        setHighLightGameObjectListener(gameObject);
        return gameObject;
    }

    public boolean isObjectInObjectsLayer(GameObject gameObject) {
        for(Actor actor : objectsLayer.getChildren()) {
            if(((GameObject) actor).getGameObjectType() == gameObject.getGameObjectType()) return true;
        }
        return false;
    }

    public void addGameObject(GameObject gameObject) {
        if(!isObjectInObjectsLayer(gameObject)) objectsLayer.addActor(gameObject);
    }

    private void removeDuplicateObject(GameObject gameObject) {
       if(isObjectInObjectsLayer(gameObject)) objectsLayer.removeActor(gameObject);
    }

    private boolean isNumberOfObjectsCorrect() {
        return actionsStripWidget != null && objectsLayer != null &&
                actionsStripWidget.getSelectedActions().size() + objectsLayer.getChildren().size == getData().getSolution().size();
    }

    private static int getMaxScoreForDifficulty(GameDifficulty gameDifficulty) {
        int maxScore = 0;
        for(int i = 0; i < MAX_ROUNDS; i++) {
            maxScore += CrimeSceneDifficulty.getCrimeSceneDifficultyForGameDifficulty(gameDifficulty).getNumberOfObjects() + i;
        }
        return maxScore;
    }

    private int getMaxScore() {
        return getMaxScoreForDifficulty(getGameDifficulty());
    }

    private void playAllSounds(boolean repeat) {
        preparePlaySoundScreen(true, repeat);
        playSoundScreen.playSounds();
    }

    private void replayMusic(GameObject gameObject) {
        setActualPlayingMusic(getMusic(gameObject.getGameObjectType().getSoundPath()));
        actualPlayingMusic.play();
    }

    public void disposeBonusMusic() {
        if(bonusAssetsManager != null && playSoundScreen != null){
            playSoundScreen.disposeBonus();
        }
    }

    private void disposeBonusAssets(){
        if(bonusAssetsManager != null){
            bonusAssetsManager.disposeData();
        }
    }

    private void clearBonusAssets(){
        if(bonusAssetsManager != null){
            bonusAssetsManager.clearData();
        }
    }


    public void addGameObjectToStrip(GameObject gameObject){
        removeDuplicateObject(gameObject);
        gameObject.changeToCardDrawable();
        actionsStripWidget.addSelectedAction(GameObjectType.getGameObjectTypeForActionNumber(gameObject.getAction().getActionNumber()).getActionTexturePath(), actionsStripWidget.getSelectedActions().size(), this, gameObject.getAction().getActionNumber());
        startButton.setEnabled(!actionsStripWidget.isNotFull());
    }

    ////////////////////// SET EVENT FOR TESTING
    public void setPlayStopEvent(){
        triggerScenarioStepEvent(EVENT_STOP_PLAYED);
    }

    public void setPlayStarEvent(){
        triggerScenarioStepEvent(EVENT_START_PLAY);
    }

    public void setActionControlledEvent(int actionNumber){
        triggerScenarioStepEvent(EVENT_ACTION_CONTROLLED + actionNumber);
    }

    public void setSoundFinishEvent(int soundNumber){
        triggerScenarioStepEvent(EVENT_FINISH_SOUND + soundNumber);
    }

    ////////////////////// GETTERS AND SETTERS

    public void setDimmerVisible(boolean visible) {
        //condition preventing to set dimmer visible when dropped action on stripWidget with multitouch
        if(visible && isNumberOfObjectsCorrect()) return;
        dimmer.setVisible(visible);
        highlightedGameObjectLayer.setVisible(visible);
    }

    public TablexiaButton getStartButton() {
        return startButton;
    }

    private int getReplayCountLeft() {
        return MAX_TASK_REPEAT - repeatTaskCount;
    }

    private String getReplayCountText() {
        return "(" + (MAX_TASK_REPEAT - repeatTaskCount) + ")";
    }

    private void setActualPlayingMusic(Music actualPlayingMusic) {
        this.actualPlayingMusic = actualPlayingMusic;
    }

    public GameObject getHighlightedGameObject() {
        return highlightedGameObject;
    }

    public void setHighlightedGameObject(GameObject highlightedGameObject) {
        this.highlightedGameObject = highlightedGameObject;
        if(highlightedGameObject!=null) AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_HIGHLIGHTED_GO);
    }


////////////////////// PRELOAD MESSAGE

    private static final String                             PRELOADER_ANIM_IMAGE             = "preloader_anim";

    private static final String                             PRELOADER_ANIM_1_IMAGE           = PRELOADER_ANIM_IMAGE + "1";
    private static final int                                PRELOADER_ANIM_1_FRAMES          = 4;
    private static final float                              PRELOADER_ANIM_1_FRAME_DURATION  = 1f;

    private static final String                             PRELOADER_ANIM_2_IMAGE           = PRELOADER_ANIM_IMAGE + "2";
    private static final int                                PRELOADER_ANIM_2_FRAMES          = 18;
    private static final float                              PRELOADER_ANIM_2_FRAME_DURATION  = 2 / 5f;

    private static final String                             PRELOADER_TEXT_KEY_1         = ApplicationTextManager.ApplicationTextsAssets.GAME_CRIME_SCENE_PRELOADER_TEXT_1;
    private static final String                             PRELOADER_TEXT_KEY_2         = ApplicationTextManager.ApplicationTextsAssets.GAME_CRIME_SCENE_PRELOADER_TEXT_2;
    private static final String                             PRELOADER_TEXT_KEY_2_DESKTOP = ApplicationTextManager.ApplicationTextsAssets.GAME_CRIME_SCENE_PRELOADER_TEXT_2_DESKTOP;

    private static final Scaling                            PRELOADER_IMAGE_SCALING             = Scaling.fit;
    private static final int 			                    PRELOADER_TEXT_ALIGN 				= Align.left;
    private static final float 			                    PRELOADER_TEXT_PADDING 				= 10f;
    private static final float                              PRELOADER_IMAGE_COLUMN_WIDTH_RATIO  = 1f / 4;
    private static final float                              PRELOADER_TEXT_COLUMN_WIDTH_RATIO   = 5f / 8;
    private static final float 			                    PRELOADER_ROW_HEIGHT 				= 1f / 3;

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        AnimatedImage preloaderImage = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_1_IMAGE, PRELOADER_ANIM_1_FRAMES, PRELOADER_ANIM_1_FRAME_DURATION), false);
        preloaderImage.startAnimationLoop();
        String preloaderText = preloaderAssetsManager.getText(PRELOADER_TEXT_KEY_1);
        components.add(
                new TwoColumnContentDialogComponent(
                    new AnimatedImageContentDialogComponent(preloaderImage, PRELOADER_IMAGE_SCALING),
                    new TextContentDialogComponent(preloaderText, PRELOADER_TEXT_ALIGN, PRELOADER_TEXT_PADDING),
                        PRELOADER_IMAGE_COLUMN_WIDTH_RATIO,
                        PRELOADER_TEXT_COLUMN_WIDTH_RATIO,
                    PRELOADER_ROW_HEIGHT
                )
        );

        components.add(new FixedSpaceContentDialogComponent());

        AnimatedImage preloaderImage2 = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_2_IMAGE, PRELOADER_ANIM_2_FRAMES, PRELOADER_ANIM_2_FRAME_DURATION), false);
        preloaderImage2.startAnimationLoop();
        String preloaderText2 = TablexiaSettings.getInstance().isRunningOnMobileDevice() ? preloaderAssetsManager.getText(PRELOADER_TEXT_KEY_2) : preloaderAssetsManager.getText(PRELOADER_TEXT_KEY_2_DESKTOP);
        components.add(new TwoColumnContentDialogComponent(
                new TextContentDialogComponent(preloaderText2, PRELOADER_TEXT_ALIGN, PRELOADER_TEXT_PADDING),
                new AnimatedImageContentDialogComponent(preloaderImage2, PRELOADER_IMAGE_SCALING),
                PRELOADER_TEXT_COLUMN_WIDTH_RATIO,
                PRELOADER_IMAGE_COLUMN_WIDTH_RATIO,
                PRELOADER_ROW_HEIGHT));
    }

}