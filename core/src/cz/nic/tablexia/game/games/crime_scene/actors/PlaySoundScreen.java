/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.actors;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.List;

import cz.nic.tablexia.game.games.crime_scene.CrimeSceneGame;
import cz.nic.tablexia.game.games.crime_scene.assets.CrimeSceneAssets;
import cz.nic.tablexia.util.ui.PlayMusicAction;

/**
 * Created by danilov on 20.5.16.
 */
public class PlaySoundScreen extends Group {
    private static final float  INTRO_DURATION                              = 3f;
    private static final float  MOVE_DURATION                               = 0.5f;
    private static final float  PAUSE_DURATION                              = 0.5f;
    private static final int    ACTION_CARD_SIZE                            = 200;

    private static final int    NUMBER_IMAGE_MAX_HEIGHT                     = 150;

    private Image actionCard, overlay;
    private Group numberGroup = new Group();
    private List<Music> musicList;
    private Music bonusBack;
    private CrimeSceneGame crimeSceneGame;

    private boolean sequencePlaying = false;
    private int finishSounds = 0;

    private float bonusVolume = 1.0f;
    private long bonusID;

    public PlaySoundScreen(CrimeSceneGame crimeSceneGame, Texture overlayTexture, TextureRegion actionCardTexture, List<Music> musicList, Music bonusBack, float bonusVolume) {
        this.musicList = musicList;
        this.bonusBack = bonusBack;
        this.bonusVolume = bonusVolume;
        this.crimeSceneGame = crimeSceneGame;

        addActor(overlay = new Image(overlayTexture));
        addActor(actionCard = new Image(actionCardTexture));
        addActor(numberGroup);
        actionCard.setSize(ACTION_CARD_SIZE, ACTION_CARD_SIZE);
        actionCard.setPosition(-ACTION_CARD_SIZE, 0);
    }

    public enum Numbers {
        ZERO(CrimeSceneAssets.ZERO),
        ONE(CrimeSceneAssets.ONE),
        TWO(CrimeSceneAssets.TWO),
        THREE(CrimeSceneAssets.THREE),
        FOUR(CrimeSceneAssets.FOUR),
        FIVE(CrimeSceneAssets.FIVE),
        SIX(CrimeSceneAssets.SIX),
        SEVEN(CrimeSceneAssets.SEVEN),
        EIGHT(CrimeSceneAssets.EIGHT),
        NINE(CrimeSceneAssets.NINE);

        private String texturePath;

        Numbers(String texturePath) {
            this.texturePath = texturePath;
        }

        public static String[] getNumberTexture(int number) {
            if (number < 0) return null;

            if (number < 10) {
                String[] textures = new String[1];
                textures[0] = Numbers.values()[number].texturePath;
                return textures;
            } else if (number < 100) {
                String[] textures = new String[2];
                textures[0] = Numbers.values()[number / 10].texturePath;
                textures[1] = Numbers.values()[number % 10].texturePath;
                return textures;
            }
            return null;
        }

    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        overlay.setSize(getWidth(), getHeight());
    }

    private Group getNumberGroup() {
        return numberGroup;
    }

    public void playSounds() {
        playBonus();
        sequencePlaying = true;

        finishSounds = 0;

        actionCard.setVisible(true);

        crimeSceneGame.setPlayStarEvent();

        SequenceAction actionCardSequence = new SequenceAction(Actions.moveTo(crimeSceneGame.getViewportWidth() / 2 - actionCard.getWidth() / 2,
                crimeSceneGame.getViewportWidth()));
        actionCardSequence.addAction(Actions.delay(INTRO_DURATION));

        int i = 1;
        for (Music music : musicList) {
            PlayMusicAction musicAction = PlayMusicAction.getInstance(music);
            musicAction.reset();
            actionCardSequence = setCardsMovement(actionCardSequence, musicAction, i++);
        }
        actionCardSequence.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                PlaySoundScreen.this.addAction(Actions.fadeOut(MOVE_DURATION));
                PlaySoundScreen.this.addAction(Actions.after(Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        stopBonus();
                        sequencePlaying = false;
                        PlaySoundScreen.this.remove();
                        crimeSceneGame.disposeBonusMusic();
                        crimeSceneGame.setPlayStopEvent();
                    }
                })));
            }
        }));

        actionCard.addAction(actionCardSequence);
    }

    private SequenceAction setCardsMovement(SequenceAction actionCardSequence, PlayMusicAction musicAction, final int cardOrder) {

        final int numberOfNumerals = Numbers.getNumberTexture(cardOrder).length;
        final Image[] numberImages = new Image[numberOfNumerals];

        for (int i = 0; i < numberOfNumerals; i++) {
            numberImages[i] = new Image(crimeSceneGame.getScreenTextureRegion(Numbers.getNumberTexture(cardOrder)[i]));
            float imgWidth = NUMBER_IMAGE_MAX_HEIGHT * (numberImages[i].getWidth() / numberImages[i].getHeight());
            numberImages[i].setSize(imgWidth, NUMBER_IMAGE_MAX_HEIGHT);
            numberImages[i].setY(crimeSceneGame.getViewportHeight() / 2 - numberImages[0].getHeight() / 2);
        }

        if (numberOfNumerals == 1) {
            numberImages[0].setX(crimeSceneGame.getViewportWidth() / 2 - numberImages[0].getWidth() / 2);
        } else if (numberOfNumerals == 2) {
            numberImages[0].setX(crimeSceneGame.getViewportWidth() / 2 - (numberImages[0].getWidth() + numberImages[1].getWidth()) / 2);
            numberImages[1].setX(crimeSceneGame.getViewportWidth() / 2 - (numberImages[0].getWidth() + numberImages[1].getWidth()) / 2 + numberImages[0].getWidth());
        }

        actionCardSequence.addAction(Actions.alpha(0));
        actionCardSequence.addAction(Actions.moveTo(crimeSceneGame.getViewportWidth() / 2 - actionCard.getWidth() / 2, crimeSceneGame.getViewportWidth()));
        actionCardSequence.addAction(new ParallelAction(Actions.moveTo(getWidth() / 2 - actionCard.getWidth() / 2,
                getHeight() / 2 - actionCard.getHeight() / 2, MOVE_DURATION), Actions.fadeIn(MOVE_DURATION)));

        actionCardSequence.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < numberOfNumerals; i++) {
                    getNumberGroup().addActor(numberImages[i]);
                }
            }
        }));

        actionCardSequence.addAction(musicAction);
        actionCardSequence.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                musicAction.getMusic().dispose();
                getNumberGroup().clearChildren();
                finishSounds++;
                crimeSceneGame.setSoundFinishEvent(finishSounds);
            }
        }));

        actionCardSequence.addAction(new ParallelAction(Actions.moveTo(getWidth() / 2 - actionCard.getWidth() / 2,
                -actionCard.getHeight(), MOVE_DURATION), Actions.fadeOut(MOVE_DURATION)));
        actionCardSequence.addAction(Actions.delay(PAUSE_DURATION));

        return actionCardSequence;
    }

/////////////////////////////// BONUS

    private void playBonus() {
        if (bonusBack != null && !bonusBack.isPlaying()) {
            bonusBack.play();
            bonusBack.setVolume(bonusVolume);
            bonusBack.setLooping(true);

        }
    }

    public void resumeBonus() {
        if (bonusBack != null && !bonusBack.isPlaying()) {
            bonusBack.play();
            bonusBack.setVolume(bonusVolume);
        }
    }

    public void pauseBonus() {
        if (bonusBack != null) {
            bonusBack.pause();
        }
    }

    private void stopBonus() {
        if (bonusBack != null) {
            bonusBack.stop();
        }
    }

    public void disposeBonus() {
        if (bonusBack != null) {
            bonusBack.dispose();
        }
    }

    public List<Music> getMusicList() {
        return musicList;
    }

    public boolean isSequencePlaying() {
        return sequencePlaying;
    }
}
