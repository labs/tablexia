/*
 *
 *  * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  
 */

package cz.nic.tablexia.game.games.crime_scene.gameobject;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.Timer;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.util.entity.Touch;

class DesktopCrimeSceneAction extends CrimeSceneAction {

    private static final float DELAY = 0.15f;
    private static final float DEFAULT_DESKTOP_DOWNSCALE = 0.45f;

    private Touch   lastRecordedTouchDown;
    private boolean lastTapValid;
    private Touch   lastRecordedTap;
    private Timer   timer;

    private boolean delayedActionStarted = false;

    public DesktopCrimeSceneAction(String texturePath, int orderNumber, int actionSize, AbstractTablexiaGame tablexiaGame, int actionNumber, GameObject gameObject) {
        super(texturePath, orderNumber, actionSize, tablexiaGame, actionNumber, gameObject);
        if (TablexiaSettings.getInstance().isUseHdAssets()){
            setSize(getWidth() * DEFAULT_DESKTOP_DOWNSCALE, getHeight() * DEFAULT_DESKTOP_DOWNSCALE);
        }
        timer = new Timer();
    }

    @Override
    public void setDragListener() {
        addListener(new DragListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                lastRecordedTouchDown = new Touch(event.getListenerActor(), System.currentTimeMillis());
                setDelayedActionStarted(false);
                timer.scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        setDelayedActionStarted(true);
                        downAction(x, y, true);
                    }
                }, DELAY);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                timer.clear();
                Touch touchUp = new Touch(event.getListenerActor(), System.currentTimeMillis());
                lastTapValid = Touch.isTap(lastRecordedTouchDown, touchUp);
                if (lastTapValid) {
                    if (lastRecordedTap == null) {
                        lastRecordedTap = touchUp;
                    } else {
                        if (Touch.isDoubleTap(lastRecordedTap, touchUp)) {
                            timer.clear();
                            lastRecordedTap = null;
                        } else {
                            lastRecordedTap = touchUp;
                            resetDelayedUpAction();
                        }
                    }
                } else {
                    if (gameObject.isDragging()) {
                        if (isDelayedActionStarted()) {
                            upAction(false);
                        } else {
                            gameObject.resetPosition();
                        }
                    }
                }
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                dragAction(x, y);
            }
        });
    }

    private void resetDelayedUpAction() {
        timer.clear();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                upAction(false);
            }
        }, DELAY);
    }

    private synchronized void setDelayedActionStarted(boolean value) {
        delayedActionStarted = value;
    }

    private synchronized boolean isDelayedActionStarted() {
        return delayedActionStarted;
    }

}