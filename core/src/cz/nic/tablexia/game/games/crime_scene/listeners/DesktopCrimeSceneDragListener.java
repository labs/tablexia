/*
 *
 *  * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  
 */

package cz.nic.tablexia.game.games.crime_scene.listeners;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.utils.Timer;

import cz.nic.tablexia.game.games.crime_scene.CrimeSceneGame;
import cz.nic.tablexia.game.games.crime_scene.gameobject.GameObject;
import cz.nic.tablexia.util.entity.Touch;

public class DesktopCrimeSceneDragListener extends CrimeSceneDragListener {
    private Timer          timer;
    private static final float DELAY = 0.15f;
    Touch   lastRecordedTouchDown;
    boolean lastTapValid;
    Touch   lastRecordedTap;
    
    public DesktopCrimeSceneDragListener(GameObject gameObject, CrimeSceneGame crimeSceneGame) {
        super(gameObject, crimeSceneGame);
        timer = new Timer();
    }
    
    @Override
    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        if(button == Input.Buttons.RIGHT) return false;
        lastRecordedTouchDown = new Touch(event.getListenerActor(), System.currentTimeMillis());
        timer.clear();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                crimeSceneGame.resolveClickOnGameObject(gameObject, x, y);
            }
        }, DELAY);
        return true;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        timer.clear();
        Touch touchUp = new Touch(event.getListenerActor(), System.currentTimeMillis());
        lastTapValid = Touch.isTap(lastRecordedTouchDown, touchUp);
        if (lastTapValid) {
            if (lastRecordedTap == null) {
                lastRecordedTap = touchUp;
                timer.clear();
                resetHighlightTask();
            } else {
                if (Touch.isDoubleTap(lastRecordedTap, touchUp)) {
                    timer.clear();
                    crimeSceneGame.addGameObjectToStrip(gameObject);
                    crimeSceneGame.resetHighLightLayer();
                    lastRecordedTap = null;
                } else {
                    lastRecordedTap = touchUp;
                    resetHighlightTask();
                }
            }

        } else {
            if (!gameObject.isDragging()) crimeSceneGame.highlightGameObject(gameObject);
            gameObject.setDragging(false);
            resetDragDeltaTime();
        }
    }

    @Override
    public void touchDragged (InputEvent event, float x, float y, int pointer) {
        increaseDragDeltaTime(gameObject);
    }


    private void resetHighlightTask(){
        timer.clear();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                gameObject.changeToCardDrawable();
                crimeSceneGame.setHighlightedGameObject(gameObject);
                crimeSceneGame.highlightGameObject(gameObject);
            }
        }, DELAY);
    }
    
}
