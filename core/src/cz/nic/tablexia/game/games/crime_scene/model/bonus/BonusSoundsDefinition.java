/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.model.bonus;

import cz.nic.tablexia.game.common.TablexiaRandom;

/**
 * Created by lmarik on 19.1.18.
 */

public enum BonusSoundsDefinition {

    INTERIOR    (new BonusSoundsSetting[]{BonusSoundsSetting.BANK, BonusSoundsSetting.COFFEE, BonusSoundsSetting.TOWN,BonusSoundsSetting.MENU_TOWN, BonusSoundsSetting.NIGHTWATCH_DAY, BonusSoundsSetting.SANDGLASS}),
    EXTERIOR    (new BonusSoundsSetting[]{BonusSoundsSetting.WAY_OFFICE, BonusSoundsSetting.CLOCKWORK, BonusSoundsSetting.MENU_TOWN, BonusSoundsSetting.NIGHTWATCH_NIGHT, BonusSoundsSetting.TRAIN_TRACKING}),
    UNDERGROUND (new BonusSoundsSetting[]{BonusSoundsSetting.CLOCKWORK, BonusSoundsSetting.NIGHTWATCH_NIGHT, BonusSoundsSetting.SANDGLASS});

    private BonusSoundsSetting[] soundsPath;

    BonusSoundsDefinition(BonusSoundsSetting[] soundsPath) {
        this.soundsPath = soundsPath;
    }

    public BonusSoundsSetting getRandomSoundPath(TablexiaRandom random) {
        return soundsPath[random.nextInt(soundsPath.length)];
    }

}
