/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.model.bonus;

import cz.nic.tablexia.game.games.crime_scene.assets.CrimeSceneAssets;

/**
 * Created by lmarik on 9.2.18.
 */

public enum BonusSoundsSetting {


    BANK                (CrimeSceneAssets.BANK_BONUS_BACK,              0.8f),
    COFFEE              (CrimeSceneAssets.COFFEE_BONUS_BACK,            0.8f),
    TOWN                (CrimeSceneAssets.TOWN_BONUS_BACK,              0.8f),
    MENU_TOWN           (CrimeSceneAssets.MENU_TOWN_BONUS_BACK,         0.8f),
    NIGHTWATCH_DAY      (CrimeSceneAssets.NIGHTWATCH_DAY_BONUS_BACK,    0.8f),
    SANDGLASS           (CrimeSceneAssets.SANDGLASS_BONUS_BACK,         0.8f),
    WAY_OFFICE          (CrimeSceneAssets.WAY_OFFICE_BONUS_BACK,        0.5f),
    CLOCKWORK           (CrimeSceneAssets.CLOCKWORK_BONUS_BACK,         0.8f),
    NIGHTWATCH_NIGHT    (CrimeSceneAssets.NIGHTWATCH_NIGHT_BONUS_BACK,  0.8f),
    TRAIN_TRACKING      (CrimeSceneAssets.TRAIN_TRACKING_BONUS_BACK,    0.3f);

    private String soundPath;
    private float  volume;

    BonusSoundsSetting(String soundPath, float volume){
        this.soundPath = soundPath;
        this.volume = volume;
    }

    public String getSoundPath() {
        return soundPath;
    }

    public float getVolume() {
        return volume;
    }
}
