/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.model;


import cz.nic.tablexia.game.difficulty.GameDifficulty;

/**
 * Created by danilov on 11.5.16.
 */
public enum CrimeSceneDifficulty {

    EASY(GameDifficulty.EASY, 3),
    MEDIUM(GameDifficulty.MEDIUM, 5),
    HARD(GameDifficulty.HARD, 7),
    BONUS(GameDifficulty.BONUS, 7);

    private GameDifficulty gameDifficulty;
    private int numberOfObjects;

    CrimeSceneDifficulty(GameDifficulty gameDifficulty, int numberOfObjects) {
        this.gameDifficulty = gameDifficulty;
        this.numberOfObjects = numberOfObjects;
    }

    public static CrimeSceneDifficulty getCrimeSceneDifficultyForGameDifficulty(GameDifficulty gameDifficulty) {
        for (CrimeSceneDifficulty crimeSceneDifficulty : CrimeSceneDifficulty.values()) {
            if (crimeSceneDifficulty.gameDifficulty == gameDifficulty) {
                return crimeSceneDifficulty;
            }
        }
        return null;
    }

    public GameDifficulty getGameDifficulty() {
        return gameDifficulty;
    }

    public int getNumberOfObjects() {
        return numberOfObjects;
    }

}
