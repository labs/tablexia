/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.gameobject;

import cz.nic.tablexia.game.common.media.AssetDescription;
import cz.nic.tablexia.game.games.crime_scene.assets.CrimeSceneAssets;

/**
 * Type of clickable object in game Crime scene map
 * <p/>
 * Created by danilov on 11.4.16.
 */
public enum GameObjectType implements AssetDescription {

    // ===== EASY ====
    BOOK_BROWSE(0, CrimeSceneAssets.BOOK_BROWSE_OBJECT, CrimeSceneAssets.BOOK_BROWSE_ACTION, CrimeSceneAssets.BOOK_BROWSE_SOUND),
    BOOK_FALL_1(1, CrimeSceneAssets.BOOK_FALL_1_OBJECT, CrimeSceneAssets.BOOK_FALL_1_ACTION, CrimeSceneAssets.BOOK_FALL_SOUND),
    BUDGIES(2, CrimeSceneAssets.BUDGIES_OBJECT, CrimeSceneAssets.BUDGIES_ACTION, CrimeSceneAssets.BUDGIES_SOUND),
    CLOCK_1(3, CrimeSceneAssets.CLOCK_1_OBJECT, CrimeSceneAssets.CLOCK_1_ACTION, CrimeSceneAssets.CLOCK_1_3_SOUND),
    CLOCK_CUCKOO(4, CrimeSceneAssets.CLOCK_CUCKOO_OBJECT, CrimeSceneAssets.CLOCK_CUCKOO_ACTION, CrimeSceneAssets.CLOCK_CUCKOO_SOUND),
    CREAK_FLOOR(5, CrimeSceneAssets.CREAK_FLOOR_OBJECT, CrimeSceneAssets.CREAK_FLOOR_ACTION, CrimeSceneAssets.CREAK_FLOOR_SOUND),
    DRUM(6, CrimeSceneAssets.DRUM_OBJECT, CrimeSceneAssets.DRUM_ACTION, CrimeSceneAssets.DRUM_SOUND),
    FLUTE(7, CrimeSceneAssets.FLUTE_OBJECT, CrimeSceneAssets.FLUTE_ACTION, CrimeSceneAssets.FLUTE_SOUND),
    GRAMOPHONE(8, CrimeSceneAssets.GRAMOPHONE_OBJECT, CrimeSceneAssets.GRAMOPHONE_ACTION, CrimeSceneAssets.GRAMOPHONE_SOUND),
    GUITAR(9, CrimeSceneAssets.GUITAR_OBJECT, CrimeSceneAssets.GUITAR_ACTION, CrimeSceneAssets.GUITAR_SOUND),
    PARROT(10, CrimeSceneAssets.PARROT_OBJECT, CrimeSceneAssets.PARROT_ACTION, CrimeSceneAssets.PARROT_SOUND),
    PIANO(11, CrimeSceneAssets.PIANO_OBJECT, CrimeSceneAssets.PIANO_ACTION, CrimeSceneAssets.PIANO_SOUND),
    RADIO_1(12, CrimeSceneAssets.RADIO_1_OBJECT, CrimeSceneAssets.RADIO_1_ACTION, CrimeSceneAssets.RADIO_1_SOUND),
    SAFE_1(13, CrimeSceneAssets.SAFE_1_OBJECT, CrimeSceneAssets.SAFE_1_ACTION, CrimeSceneAssets.SAFE_SOUND),
    TELEPHONE(14, CrimeSceneAssets.TELEPHONE_OBJECT, CrimeSceneAssets.TELEPHONE_ACTION, CrimeSceneAssets.TELEPHONE_SOUND),
    VIOLIN(15, CrimeSceneAssets.VIOLIN_OBJECT, CrimeSceneAssets.VIOLIN_ACTION, CrimeSceneAssets.VIOLIN_SOUND),
    BROOM(16, CrimeSceneAssets.BROOM_OBJECT, CrimeSceneAssets.BROOM_ACTION, CrimeSceneAssets.BROOM_SOUND),
    CORK(17, CrimeSceneAssets.CORK_OBJECT, CrimeSceneAssets.CORK_ACTION, CrimeSceneAssets.CORK_SOUND),
    EGG(18, CrimeSceneAssets.EGG_OBJECT, CrimeSceneAssets.EGG_ACTION, CrimeSceneAssets.EGG_SOUND),
    FRY(19, CrimeSceneAssets.FRY_OBJECT, CrimeSceneAssets.FRY_ACTION, CrimeSceneAssets.FRY_SOUND),
    GLASS(20, CrimeSceneAssets.GLASS_OBJECT, CrimeSceneAssets.GLASS_ACTION, CrimeSceneAssets.GLASS_SOUND),
    KETTLE(21, CrimeSceneAssets.KETTLE_OBJECT, CrimeSceneAssets.KETTLE_ACTION, CrimeSceneAssets.KETTLE_SOUND),
    PAN(22, CrimeSceneAssets.PAN_OBJECT, CrimeSceneAssets.PAN_ACTION, CrimeSceneAssets.PAN_SOUND),
    PLATE(23, CrimeSceneAssets.PLATE_OBJECT, CrimeSceneAssets.PLATE_ACTION, CrimeSceneAssets.PLATE_SOUND),
    SHARPENING_KNIFE(24, CrimeSceneAssets.SHARPENING_KNIFE_OBJECT, CrimeSceneAssets.SHARPENING_KNIFE_ACTION, CrimeSceneAssets.SHARPENING_KNIFE_SOUND),
    SLICING(25, CrimeSceneAssets.SLICING_OBJECT, CrimeSceneAssets.SLICING_ACTION, CrimeSceneAssets.SLICING_SOUND),
    SOUP(26, CrimeSceneAssets.SOUP_OBJECT, CrimeSceneAssets.SOUP_ACTION, CrimeSceneAssets.SOUP_SOUND),
    SIDEBOARD(27, CrimeSceneAssets.SIDEBOARD_OBJECT, CrimeSceneAssets.SIDEBOARD_ACTION, CrimeSceneAssets.SIDEBOARD_SOUND),
    CUCUMBERS_JAR(28, CrimeSceneAssets.CUCUMBERS_JAR_OBJECT, CrimeSceneAssets.CUCUMBERS_JAR_ACTION, CrimeSceneAssets.CUCUMBERS_JAR_SOUND),
    POURING_RICE(29, CrimeSceneAssets.POURING_RICE_OBJECT, CrimeSceneAssets.POURING_RICE_ACTION, CrimeSceneAssets.POURING_RICE_SOUND),
    TAP_1(30, CrimeSceneAssets.TAP_1_OBJECT, CrimeSceneAssets.TAP_1_ACTION, CrimeSceneAssets.TAP_SOUND),
    STOVE(31, CrimeSceneAssets.STOVE_OBJECT, CrimeSceneAssets.STOVE_ACTION, CrimeSceneAssets.STOVE_SOUND),
    BUCKET_1(32, CrimeSceneAssets.BUCKET_1_OBJECT, CrimeSceneAssets.BUCKET_1_ACTION, CrimeSceneAssets.BUCKET_SOUND),
    DRAWER(33, CrimeSceneAssets.DRAWER_OBJECT, CrimeSceneAssets.DRAWER_ACTION, CrimeSceneAssets.DRAWER_SOUND),
    TAP_2(34, CrimeSceneAssets.TAP_2_OBJECT, CrimeSceneAssets.TAP_2_ACTION, CrimeSceneAssets.TAP_SOUND),
    LAMP_1(35, CrimeSceneAssets.LAMP_1_OBJECT, CrimeSceneAssets.LAMP_1_ACTION, CrimeSceneAssets.LAMP_SOUND),
    MIRROR(36, CrimeSceneAssets.MIRROR_OBJECT, CrimeSceneAssets.MIRROR_ACTION, CrimeSceneAssets.MIRROR_SOUND),
    MUSIC_BOX(37, CrimeSceneAssets.MUSIC_BOX_OBJECT, CrimeSceneAssets.MUSIC_BOX_ACTION, CrimeSceneAssets.MUSIC_BOX_SOUND),
    PAPER_1(38, CrimeSceneAssets.PAPER_1_OBJECT, CrimeSceneAssets.PAPER_1_ACTION, CrimeSceneAssets.PAPER_SOUND),
    PEN_1(39, CrimeSceneAssets.PEN_1_OBJECT, CrimeSceneAssets.PEN_1_ACTION, CrimeSceneAssets.PEN_SOUND),
    PILLOW(40, CrimeSceneAssets.PILLOW_OBJECT, CrimeSceneAssets.PILLOW_ACTION, CrimeSceneAssets.PILLOW_SOUND),
    SHOWER(41, CrimeSceneAssets.SHOWER_OBJECT, CrimeSceneAssets.SHOWER_ACTION, CrimeSceneAssets.SHOWER_SOUND),
    TEETH(42, CrimeSceneAssets.TEETH_OBJECT, CrimeSceneAssets.TEETH_ACTION, CrimeSceneAssets.TEETH_SOUND),
    TOILET(43, CrimeSceneAssets.TOILET_OBJECT, CrimeSceneAssets.TOILET_ACTION, CrimeSceneAssets.TOILET_SOUND),
    WARDROBE(44, CrimeSceneAssets.WARDROBE_OBJECT, CrimeSceneAssets.WARDROBE_ACTION, CrimeSceneAssets.WARDROBE_SOUND),
    BOOK_FALL_2(45, CrimeSceneAssets.BOOK_FALL_2_OBJECT, CrimeSceneAssets.BOOK_FALL_2_ACTION, CrimeSceneAssets.BOOK_FALL_SOUND),
    BOX(46, CrimeSceneAssets.BOX_OBJECT, CrimeSceneAssets.BOX_ACTION, CrimeSceneAssets.BOX_SOUND),
    BROKEN_GLASS(47, CrimeSceneAssets.BROKEN_GLASS_OBJECT, CrimeSceneAssets.BROKEN_GLASS_ACTION, CrimeSceneAssets.BROKEN_GLASS_SOUND),
    CREAK_FLOOR_2(48, CrimeSceneAssets.CREAK_FLOOR_2_OBJECT, CrimeSceneAssets.CREAK_FLOOR_2_ACTION, CrimeSceneAssets.CREAK_FLOOR_2_SOUND),
    MARBLE(49, CrimeSceneAssets.MARBLE_OBJECT, CrimeSceneAssets.MARBLE_ACTION, CrimeSceneAssets.MARBLE_SOUND),
    MOUSE_1(50, CrimeSceneAssets.MOUSE_1_2_OBJECT, CrimeSceneAssets.MOUSE_1_2_3_ACTION, CrimeSceneAssets.MOUSE_1_2_SOUND),
    ROCKING_CHAIR(51, CrimeSceneAssets.ROCKING_CHAIR_OBJECT, CrimeSceneAssets.ROCKING_CHAIR_ACTION, CrimeSceneAssets.ROCKING_CHAIR_SOUND),
    CAT_HISS(52, CrimeSceneAssets.CAT_HISS_OBJECT, CrimeSceneAssets.CAT_HISS_ACTION, CrimeSceneAssets.CAT_HISS_SOUND),
    CAT_MEOW(53, CrimeSceneAssets.CAT_MEOW_OBJECT, CrimeSceneAssets.CAT_MEOW_ACTION, CrimeSceneAssets.CAT_MEOW_SOUND),
    CAT_PURR(54, CrimeSceneAssets.CAT_PURR_OBJECT, CrimeSceneAssets.CAT_PURR_ACTION, CrimeSceneAssets.CAT_PURR_SOUND),
    CURTAIN_1(55, CrimeSceneAssets.CURTAIN_1_OBJECT, CrimeSceneAssets.CURTAIN_1_ACTION, CrimeSceneAssets.CURTAIN_SOUND),
    DOOR_1(56, CrimeSceneAssets.DOOR_1_OBJECT, CrimeSceneAssets.DOOR_1_ACTION, CrimeSceneAssets.DOOR_1_2_SOUND),
    STAIRS_1(57, CrimeSceneAssets.STAIRS_1_OBJECT, CrimeSceneAssets.STAIRS_1_ACTION, CrimeSceneAssets.STAIRS_1_SOUND),
    WINDOW_1(58, CrimeSceneAssets.WINDOW_1_OBJECT, CrimeSceneAssets.WINDOW_1_ACTION, CrimeSceneAssets.WINDOW_SOUND),
    DOG_FRIENDLY(59, CrimeSceneAssets.DOG_FRIENDLY_OBJECT, CrimeSceneAssets.DOG_FRIENDLY_ACTION, CrimeSceneAssets.DOG_FRIENDLY_SOUND),
    DOG_GROWL(60, CrimeSceneAssets.DOG_GROWL_OBJECT, CrimeSceneAssets.DOG_GROWL_ACTION, CrimeSceneAssets.DOG_GROWL_SOUND),
    DOG_HOWL(61, CrimeSceneAssets.DOG_HOWL_OBJECT, CrimeSceneAssets.DOG_HOWL_ACTION, CrimeSceneAssets.DOG_HOWL_SOUND),
    DOG_THREAT(62, CrimeSceneAssets.DOG_THREAT_OBJECT, CrimeSceneAssets.DOG_THREAT_ACTION, CrimeSceneAssets.DOG_THREAT_SOUND),
    DRIPPING_1(63, CrimeSceneAssets.DRIPPING_1_OBJECT, CrimeSceneAssets.DRIPPING_1_ACTION, CrimeSceneAssets.DRIPPING_1_SOUND),
    TOY_SQUEAKY(64, CrimeSceneAssets.TOY_SQUEAKY_OBJECT, CrimeSceneAssets.TOY_SQUEAKY_ACTION, CrimeSceneAssets.TOY_SQUEAKY_SOUND),
    STONE(65, CrimeSceneAssets.STONE_OBJECT,CrimeSceneAssets.STONE_ACTION,CrimeSceneAssets.STONE_SOUND),

    // ===== MEDIUM ====

    APPLE_1(66, CrimeSceneAssets.APPLE_1_OBJECT,CrimeSceneAssets.APPLE_1_ACTION,CrimeSceneAssets.APPLE_1_SOUND),
    APPLE_2(67, CrimeSceneAssets.APPLE_2_OBJECT, CrimeSceneAssets.APPLE_2_ACTION, CrimeSceneAssets.APPLE_2_SOUND),
    BALL_KICK(68, CrimeSceneAssets.BALL_KICK_OBJECT, CrimeSceneAssets.BALL_KICK_ACTION, CrimeSceneAssets.BALL_KICK_SOUND),
    BIRD_TRILL(69, CrimeSceneAssets.BIRD_TRILL_OBJECT, CrimeSceneAssets.BIRD_TRILL_ACTION, CrimeSceneAssets.BIRD_TRILL_SOUND),
    BIRD_WHISTLE(70, CrimeSceneAssets.BIRD_WHISTLE_OBJECT, CrimeSceneAssets.BIRD_WHISTLE_ACTION, CrimeSceneAssets.BIRD_WHISTLE_SOUND),
    CHICKEN_CLUCK(71, CrimeSceneAssets.CHICKEN_CLUCK_OBJECT, CrimeSceneAssets.CHICKEN_CLUCK_ACTION, CrimeSceneAssets.CHICKEN_CLUCK_SOUND),
    FIRE_FLAME(72, CrimeSceneAssets.FIRE_FLAME_OBJECT, CrimeSceneAssets.FIRE_FLAME_ACTION, CrimeSceneAssets.FIRE_FLAME_SOUND),
    GATE_GROAN(73, CrimeSceneAssets.GATE_GROAN_OBJECT, CrimeSceneAssets.GATE_GROAN_ACTION, CrimeSceneAssets.GATE_GROAN_SOUND),
    KENNEL_CHAIN(74, CrimeSceneAssets.KENNEL_CHAIN_OBJECT, CrimeSceneAssets.KENNEL_CHAIN_ACTION, CrimeSceneAssets.KENNEL_CHAIN_SOUND),
    ROOSTER_SQUAWK(75, CrimeSceneAssets.ROOSTER_SQUAWK_OBJECT, CrimeSceneAssets.ROOSTER_SQUAWK_ACTION, CrimeSceneAssets.ROOSTER_SQUAWK_SOUND),
    SHOVEL_DIGGING(76, CrimeSceneAssets.SHOVEL_DIGGING_OBJECT, CrimeSceneAssets.SHOVEL_DIGGING_ACTION, CrimeSceneAssets.SHOVEL_DIGGING_SOUND),
    WALK_GRAVEL(77, CrimeSceneAssets.WALK_GRAVEL_OBJECT, CrimeSceneAssets.WALK_GRAVEL_ACTION, CrimeSceneAssets.WALK_GRAVEL_SOUND),
    WOOD_CHOP(78, CrimeSceneAssets.WOOD_CHOP_OBJECT, CrimeSceneAssets.WOOD_CHOP_ACTION, CrimeSceneAssets.WOOD_CHOP_SOUND),
    FROG(79, CrimeSceneAssets.FROG_OBJECT,CrimeSceneAssets.FROG_ACTION,CrimeSceneAssets.FROG_SOUND),
    COINS_1(80, CrimeSceneAssets.COINS_1_OBJECT, CrimeSceneAssets.COINS_1_ACTION, CrimeSceneAssets.COINS_1_SOUND),
    DUSTBIN_COVER(81, CrimeSceneAssets.DUSTBIN_COVER_OBJECT, CrimeSceneAssets.DUSTBIN_COVER_ACTION, CrimeSceneAssets.DUSTBIN_COVER_SOUND),
    FLOWERPOT_BROKEN(82, CrimeSceneAssets.FLOWERPOT_BROKEN_OBJECT, CrimeSceneAssets.FLOWERPOT_BROKEN_ACTION, CrimeSceneAssets.FLOWERPOT_BROKEN_SOUND),
    MOUSE_2(83,CrimeSceneAssets.MOUSE_1_2_OBJECT,CrimeSceneAssets.MOUSE_1_2_3_ACTION,CrimeSceneAssets.MOUSE_1_2_SOUND),
    RADIO_2(84, CrimeSceneAssets.RADIO_2_OBJECT, CrimeSceneAssets.RADIO_2_ACTION, CrimeSceneAssets.RADIO_2_SOUND),
    WALK_HEELS(85, CrimeSceneAssets.WALK_HEELS_OBJECT, CrimeSceneAssets.WALK_HEELS_ACTION, CrimeSceneAssets.WALK_HEELS_SOUND),
    WALK_WATER(86, CrimeSceneAssets.WALK_WATER_OBJECT, CrimeSceneAssets.WALK_WATER_ACTION, CrimeSceneAssets.WALK_WATER_SOUND),
    WINDOW_2(87, CrimeSceneAssets.WINDOW_2_OBJECT, CrimeSceneAssets.WINDOW_2_ACTION, CrimeSceneAssets.WINDOW_SOUND),
    SHARDS(88, CrimeSceneAssets.SHARDS_OBJECT, CrimeSceneAssets.SHARDS_ACTION, CrimeSceneAssets.SHARDS_SOUND),

    // ===== HARD ====

    ALARM(89,CrimeSceneAssets.ALARM_OBJECT,CrimeSceneAssets.ALARM_ACTION,CrimeSceneAssets.ALARM_SOUND),
    STEPS_1(90,CrimeSceneAssets.STEPS_1_OBJECT,CrimeSceneAssets.STEPS_1_ACTION,CrimeSceneAssets.STEPS_SOUND),
    INKPOT(91,CrimeSceneAssets.INKPOT_OBJECT,CrimeSceneAssets.INKPOT_ACTION,CrimeSceneAssets.INKPOT_SOUND),
    PHONE_HANGUP(92,CrimeSceneAssets.PHONE_HANGUP_OBJECT,CrimeSceneAssets.PHONE_HANGUP_ACTION,CrimeSceneAssets.PHONE_HANGUP_SOUND),
    PHONE_TALK(93,CrimeSceneAssets.PHONE_TALK_OBJECT,CrimeSceneAssets.PHONE_TALK_ACTION,CrimeSceneAssets.PHONE_TALK_SOUND),
    PHONE_RING(94, CrimeSceneAssets.PHONE_RING_OBJECT,CrimeSceneAssets.PHONE_RING_ACTION,CrimeSceneAssets.PHONE_RING_SOUND),
    STAMP(95,CrimeSceneAssets.STAMP_OBJECT,CrimeSceneAssets.STAMP_ACTION,CrimeSceneAssets.STAMP_SOUND),
    WINDOW_3(96,CrimeSceneAssets.WINDOW_3_OBJECT,CrimeSceneAssets.WINDOW_3_ACTION,CrimeSceneAssets.WINDOW_SOUND),
    BUCKET_2(97, CrimeSceneAssets.BUCKET_2_OBJECT, CrimeSceneAssets.BUCKET_2_ACTION, CrimeSceneAssets.BUCKET_SOUND),
    DOOR_2(98, CrimeSceneAssets.DOOR_2_OBJECT, CrimeSceneAssets.DOOR_2_ACTION, CrimeSceneAssets.DOOR_1_2_SOUND),
    LAMP_2(99, CrimeSceneAssets.LAMP_2_OBJECT, CrimeSceneAssets.LAMP_2_ACTION, CrimeSceneAssets.LAMP_SOUND),
    CLOCK_2(100, CrimeSceneAssets.CLOCK_2_OBJECT, CrimeSceneAssets.CLOCK_2_ACTION, CrimeSceneAssets.CLOCK_2_SOUND),
    PAPER_2(101, CrimeSceneAssets.PAPER_2_OBJECT, CrimeSceneAssets.PAPER_2_ACTION, CrimeSceneAssets.PAPER_SOUND),
    PEN_2(102, CrimeSceneAssets.PEN_2_OBJECT, CrimeSceneAssets.PEN_2_ACTION, CrimeSceneAssets.PEN_SOUND),
    BOOKS(103,CrimeSceneAssets.BOOKS_OBJECT,CrimeSceneAssets.BOOKS_ACTION,CrimeSceneAssets.BOOKS_SOUND),
    TYPEWRITER_TYPING_ALT(104,CrimeSceneAssets.TYPEWRITER_TYPING_ALT_OBJECT,CrimeSceneAssets.TYPEWRITER_TYPING_ALT_ACTION,CrimeSceneAssets.TYPEWRITER_TYPING_ALT_SOUND),
    TYPEWRITER_TYPING_FAST(105,CrimeSceneAssets.TYPEWRITER_TYPING_FAST_OBJECT,CrimeSceneAssets.TYPEWRITER_TYPING_FAST_ACTION,CrimeSceneAssets.TYPEWRITER_TYPING_FAST_SOUND),
    TYPEWRITER_TYPING_NORMAL(106,CrimeSceneAssets.TYPEWRITER_TYPING_NORMAL_OBJECT,CrimeSceneAssets.TYPEWRITER_TYPING_NORMAL_ACTION,CrimeSceneAssets.TYPEWRITER_TYPING_NORMAL_SOUND),
    CURTAIN_2(107, CrimeSceneAssets.CURTAIN_2_OBJECT, CrimeSceneAssets.CURTAIN_2_ACTION, CrimeSceneAssets.CURTAIN_SOUND),
    SAFE_2(108, CrimeSceneAssets.SAFE_2_OBJECT, CrimeSceneAssets.SAFE_2_ACTION, CrimeSceneAssets.SAFE_SOUND),
    SMALL_WINDOW(109, CrimeSceneAssets.SMALL_WINDOW_OBJECT,CrimeSceneAssets.SMALL_WINDOW_ACTION,CrimeSceneAssets.SMALL_WINDOW_SOUND),
    DOOR_3(110,CrimeSceneAssets.DOOR_3_OBJECT,CrimeSceneAssets.DOOR_3_ACTION,CrimeSceneAssets.DOOR_3_SOUND),
    STAIRS_2(111, CrimeSceneAssets.STAIRS_2_OBJECT, CrimeSceneAssets.STAIRS_2_ACTION, CrimeSceneAssets.STAIRS_2_SOUND),
    COINS_3(112,CrimeSceneAssets.COINS_3_OBJECT,CrimeSceneAssets.COINS_3_ACTION,CrimeSceneAssets.COINS_3_SOUND),
    STEPS_2(113, CrimeSceneAssets.STEPS_2_OBJECT,CrimeSceneAssets.STEPS_2_ACTION,CrimeSceneAssets.STEPS_SOUND),
    CREAKING_CHAIR(114, CrimeSceneAssets.CREAKING_CHAIR_OBJECT, CrimeSceneAssets.CREAKING_CHAIR_ACTION, CrimeSceneAssets.CREAKING_CHAIR_SOUND),
    CLOCK_3(115, CrimeSceneAssets.CLOCK_3_OBJECT, CrimeSceneAssets.CLOCK_3_ACTION, CrimeSceneAssets.CLOCK_1_3_SOUND),
    DRIPPING_2(116,CrimeSceneAssets.DRIPPING_2_OBJECT,CrimeSceneAssets.DRIPPING_2_ACTION,CrimeSceneAssets.DRIPPING_2_SOUND),
    MOUSE_3(117,CrimeSceneAssets.MOUSE_3_OBJECT,CrimeSceneAssets.MOUSE_1_2_3_ACTION,CrimeSceneAssets.MOUSE_3_SOUND),

    //added later
    TINKLE_TOY(118, CrimeSceneAssets.TINKLE_TOY_OBJECT, CrimeSceneAssets.TINKLE_TOY_ACTION, CrimeSceneAssets.TINKLE_TOY_SOUND),
    KEY_IN_LOCK(119, CrimeSceneAssets.KEY_IN_LOCK_OBJECT, CrimeSceneAssets.KEY_IN_LOCK_ACTION, CrimeSceneAssets.KEY_IN_LOCK_SOUND),
    KEYS(120, CrimeSceneAssets.KEYS_OBJECT, CrimeSceneAssets.KEYS_ACTION, CrimeSceneAssets.KEYS_SOUND),
    SAFE_CLOSE(121, CrimeSceneAssets.SAFE_CLOSE_OBJECT, CrimeSceneAssets.SAFE_CLOSE_ACTION, CrimeSceneAssets.SAFE_CLOSE_SOUND),
    ROD(122, CrimeSceneAssets.ROD_OBJECT, CrimeSceneAssets.ROD_ACTION, CrimeSceneAssets.ROD_SOUND);

    //TODO add sounds
    //    COINS_2(76,CrimeSceneAssets.COINS_2_OBJECT,CrimeSceneAssets.COINS_2_ACTION,CrimeSceneAssets.COINS_2_SOUND),
    //    BARS, BENCH, BRICK, SAFE_CLICK, SAFE_CREAK

    private int actionNumber;
    private String gameObjectTexturePath;
    private String actionTexturePath;
    private String soundPath;

    GameObjectType(int actionNumber, String texturePath, String actionTexturePath, String soundPath) {
        this.actionNumber = actionNumber;
        this.gameObjectTexturePath = texturePath;
        this.actionTexturePath = actionTexturePath;
        this.soundPath = soundPath;
    }

    public int getActionNumber() {return actionNumber; }

    private String getGameObjectTexturePath() {
        return gameObjectTexturePath;
    }

    public String getActionTexturePath() {
        return actionTexturePath;
    }

    public String getSoundPath() {
        return soundPath;
    }

    @Override
    public String getResource() {
        return getGameObjectTexturePath();
    }

    public static GameObjectType getGameObjectTypeForActionNumber(int actionNumber) {
        for (GameObjectType gameObjectType : GameObjectType.values()) {
            if (gameObjectType.actionNumber == actionNumber) {
                return gameObjectType;
            }
        }
        return null;
    }
}
