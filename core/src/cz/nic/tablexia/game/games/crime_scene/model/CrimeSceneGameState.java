/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.crime_scene.gameobject.GameObjectType;
import cz.nic.tablexia.game.games.crime_scene.model.LevelDefinition.Level;
import cz.nic.tablexia.game.games.crime_scene.model.bonus.BonusSoundsSetting;
import cz.nic.tablexia.util.Log;

/**
 * Created by danilov on 27.4.16.
 */
public class CrimeSceneGameState {
    private List<GameObjectType> solution;
    private TablexiaRandom random;
    private Level usedLevel;
    private BonusSoundsSetting bonusSetting;

    private CrimeSceneGameState(CrimeSceneDifficulty gameDifficulty, TablexiaRandom random) {
        this.random = random;
        setSolution(gameDifficulty.getGameDifficulty(), 1, gameDifficulty.getNumberOfObjects());
    }

    public List<GameObjectType> getSolution() {
        return solution;
    }

    public Level getUsedLevel() {
        return usedLevel;
    }

    public void setSolution(GameDifficulty gameDifficulty, int levelNumber, int numberOfGameObjects) {
        if (gameDifficulty.equals(GameDifficulty.BONUS)) {
            usedLevel = Level.getRandomLevelForBonus(random);
            bonusSetting = usedLevel.getBonusSoundsDefinition().getRandomSoundPath(random);
        } else {
            usedLevel = Level.getLevelByDifficulty(gameDifficulty, levelNumber);
        }

        Log.info(getClass(), "Used level: " + usedLevel);

        List<GameObjectType> allGameObjects = new ArrayList<>(LevelDefinition.getGameObjectsByLevel(usedLevel));

        Collections.shuffle(allGameObjects, random);
        solution = new ArrayList<GameObjectType>(allGameObjects.subList(0, numberOfGameObjects));
    }

    public BonusSoundsSetting getBonusSetting() {
        return bonusSetting;
    }

    public static class CrimeSceneGameStateFactory {
        public static CrimeSceneGameState createInstance(TablexiaRandom random, CrimeSceneDifficulty gameDifficulty) {
            return new CrimeSceneGameState(gameDifficulty, random);
        }
    }
}
