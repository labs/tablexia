/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.model;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.crime_scene.assets.CrimeSceneAssets;
import cz.nic.tablexia.game.games.crime_scene.gameobject.GameObjectType;
import cz.nic.tablexia.game.games.crime_scene.model.bonus.BonusSoundsDefinition;
import cz.nic.tablexia.util.ui.tilemapgenerator.Position;

/**
 * Created by danilov on 25.4.16.
 */
public class LevelDefinition {

   //TODO find out size of y array
   public static Position[][] levelsDefinition = new Position[Level.values().length][500];

   public enum Level {
      EASY_1(GameDifficulty.EASY, 1, CrimeSceneAssets.EASY_GAME_MAP_1, BonusSoundsDefinition.INTERIOR),
      EASY_2(GameDifficulty.EASY, 2, CrimeSceneAssets.EASY_GAME_MAP_2, BonusSoundsDefinition.INTERIOR),
      EASY_3(GameDifficulty.EASY, 3, CrimeSceneAssets.EASY_GAME_MAP_3, BonusSoundsDefinition.INTERIOR),
      EASY_4(GameDifficulty.EASY, 4, CrimeSceneAssets.EASY_GAME_MAP_4, BonusSoundsDefinition.INTERIOR),
      MEDIUM_1(GameDifficulty.MEDIUM, 1, CrimeSceneAssets.MEDIUM_GAME_MAP_1_2, BonusSoundsDefinition.EXTERIOR),
      MEDIUM_2(GameDifficulty.MEDIUM, 2, CrimeSceneAssets.MEDIUM_GAME_MAP_1_2, BonusSoundsDefinition.EXTERIOR),
      MEDIUM_3(GameDifficulty.MEDIUM, 3, CrimeSceneAssets.MEDIUM_GAME_MAP_3_4, BonusSoundsDefinition.EXTERIOR),
      MEDIUM_4(GameDifficulty.MEDIUM, 4, CrimeSceneAssets.MEDIUM_GAME_MAP_3_4, BonusSoundsDefinition.EXTERIOR),
      HARD_1(GameDifficulty.HARD, 1, CrimeSceneAssets.HARD_GAME_MAP_1, BonusSoundsDefinition.INTERIOR),
      HARD_2(GameDifficulty.HARD, 2, CrimeSceneAssets.HARD_GAME_MAP_2, BonusSoundsDefinition.INTERIOR),
      HARD_3(GameDifficulty.HARD, 3, CrimeSceneAssets.HARD_GAME_MAP_3, BonusSoundsDefinition.UNDERGROUND),
      HARD_4(GameDifficulty.HARD, 4, CrimeSceneAssets.HARD_GAME_MAP_4, BonusSoundsDefinition.UNDERGROUND);

      private GameDifficulty difficulty;
      private int levelNumber;
      private String texturePath;
      private BonusSoundsDefinition bonusSoundsDefinition;

      Level(GameDifficulty difficulty, int level, String texturePath, BonusSoundsDefinition bonusSoundsDefinition) {
         this.difficulty = difficulty;
         this.levelNumber = level;
         this.texturePath = texturePath;
         this.bonusSoundsDefinition = bonusSoundsDefinition;
      }

      public GameDifficulty getDifficulty() {
         return difficulty;
      }

      public int getLevelNumber() {
         return levelNumber;
      }

      public String getTexturePath() {
         return texturePath;
      }

      public BonusSoundsDefinition getBonusSoundsDefinition() {
         return bonusSoundsDefinition;
      }

      public static Level getLevelByDifficulty(GameDifficulty difficulty, int levelNumber) {
         for (Level level : values()) {
            if (level.getDifficulty() == difficulty && level.getLevelNumber() == levelNumber) {
               return level;
            }
         }

         return null;
      }

      public static Level getRandomLevelForBonus(TablexiaRandom random) {
         return values()[random.nextInt(values().length)];
      }
   }

    static {
       //TODO optimize code (add objects to arrays only when necessary)

       //EASY 1
       levelsDefinition[0][GameObjectType.BOOK_BROWSE.getActionNumber()] = new Position(70,120);
       levelsDefinition[0][GameObjectType.BOOK_FALL_1.getActionNumber()] = new Position(260,15);
       levelsDefinition[0][GameObjectType.BUDGIES.getActionNumber()] = new Position(250,50);
       levelsDefinition[0][GameObjectType.CLOCK_1.getActionNumber()] = new Position(30,380);
       levelsDefinition[0][GameObjectType.CLOCK_CUCKOO.getActionNumber()] = new Position(75,330);
       levelsDefinition[0][GameObjectType.CREAK_FLOOR.getActionNumber()] = new Position(140,80);
       levelsDefinition[0][GameObjectType.DRUM.getActionNumber()] = new Position(360,80);
       levelsDefinition[0][GameObjectType.FLUTE.getActionNumber()] = new Position(240,190);
       levelsDefinition[0][GameObjectType.GRAMOPHONE.getActionNumber()] = new Position(360,120);
       levelsDefinition[0][GameObjectType.GUITAR.getActionNumber()] = new Position(20,145);
       levelsDefinition[0][GameObjectType.PARROT.getActionNumber()] = new Position(300,285);
       levelsDefinition[0][GameObjectType.PIANO.getActionNumber()] = new Position(85,180);
       levelsDefinition[0][GameObjectType.RADIO_1.getActionNumber()] = new Position(520,165);
       levelsDefinition[0][GameObjectType.SAFE_1.getActionNumber()] = new Position(200,220);
       levelsDefinition[0][GameObjectType.TELEPHONE.getActionNumber()] = new Position(180,65);
       levelsDefinition[0][GameObjectType.VIOLIN.getActionNumber()] = new Position(310,250);
       //common
       levelsDefinition[0][GameObjectType.CAT_HISS.getActionNumber()] = new Position(300,150);
       levelsDefinition[0][GameObjectType.CAT_MEOW.getActionNumber()] = new Position(475,150);
       levelsDefinition[0][GameObjectType.CAT_PURR.getActionNumber()] = new Position(300,75);
       levelsDefinition[0][GameObjectType.CURTAIN_1.getActionNumber()] = new Position(200,375);
       levelsDefinition[0][GameObjectType.DOOR_1.getActionNumber()] = new Position(450,240);
       levelsDefinition[0][GameObjectType.STAIRS_1.getActionNumber()] = new Position(360,290);
       levelsDefinition[0][GameObjectType.WINDOW_1.getActionNumber()] = new Position(140,345);

       //EASY 2
       levelsDefinition[1][GameObjectType.BROOM.getActionNumber()] = new Position(300,330);
       levelsDefinition[1][GameObjectType.CORK.getActionNumber()] = new Position(150,150);
       levelsDefinition[1][GameObjectType.EGG.getActionNumber()] = new Position(400,165);
       levelsDefinition[1][GameObjectType.FRY.getActionNumber()] = new Position(450,280);
       levelsDefinition[1][GameObjectType.GLASS.getActionNumber()] = new Position(200,100);
       levelsDefinition[1][GameObjectType.KETTLE.getActionNumber()] = new Position(200,150);
       levelsDefinition[1][GameObjectType.PAN.getActionNumber()] = new Position(200,200);
       levelsDefinition[1][GameObjectType.PLATE.getActionNumber()] = new Position(350,250);
       levelsDefinition[1][GameObjectType.SHARPENING_KNIFE.getActionNumber()] = new Position(200,300);
       levelsDefinition[1][GameObjectType.SLICING.getActionNumber()] = new Position(400,100);
       levelsDefinition[1][GameObjectType.SOUP.getActionNumber()] = new Position(550,200);
       levelsDefinition[1][GameObjectType.SIDEBOARD.getActionNumber()] = new Position(70,220);
       levelsDefinition[1][GameObjectType.CUCUMBERS_JAR.getActionNumber()] = new Position(450,220);
       levelsDefinition[1][GameObjectType.POURING_RICE.getActionNumber()] = new Position(300,240);
       levelsDefinition[1][GameObjectType.TAP_1.getActionNumber()] = new Position(20,250);
       levelsDefinition[1][GameObjectType.STOVE.getActionNumber()] = new Position(405,305);
       //common
       levelsDefinition[1][GameObjectType.CAT_HISS.getActionNumber()] = new Position(300,150);
       levelsDefinition[1][GameObjectType.CAT_MEOW.getActionNumber()] = new Position(475,150);
       levelsDefinition[1][GameObjectType.CAT_PURR.getActionNumber()] = new Position(300,75);
       levelsDefinition[1][GameObjectType.CURTAIN_1.getActionNumber()] = new Position(200,375);
       levelsDefinition[1][GameObjectType.DOOR_1.getActionNumber()] = new Position(630,239);
       levelsDefinition[1][GameObjectType.STAIRS_1.getActionNumber()] = new Position(550,282);
       levelsDefinition[1][GameObjectType.WINDOW_1.getActionNumber()] = new Position(140,345);

       //EASY 3
       levelsDefinition[2][GameObjectType.BUCKET_1.getActionNumber()] = new Position(255,200);
       levelsDefinition[2][GameObjectType.DRAWER.getActionNumber()] = new Position(130,250);
       levelsDefinition[2][GameObjectType.TAP_2.getActionNumber()] = new Position(440,350);
       levelsDefinition[2][GameObjectType.LAMP_1.getActionNumber()] = new Position(540,150);
       levelsDefinition[2][GameObjectType.MIRROR.getActionNumber()] = new Position(450,400);
       levelsDefinition[2][GameObjectType.MUSIC_BOX.getActionNumber()] = new Position(340,90);
       levelsDefinition[2][GameObjectType.PAPER_1.getActionNumber()] = new Position(380,120);
       levelsDefinition[2][GameObjectType.PEN_1.getActionNumber()] = new Position(380,210);
       levelsDefinition[2][GameObjectType.PILLOW.getActionNumber()] = new Position(140,150);
       levelsDefinition[2][GameObjectType.SHOWER.getActionNumber()] = new Position(590,180);
       levelsDefinition[2][GameObjectType.TEETH.getActionNumber()] = new Position(500,270);
       levelsDefinition[2][GameObjectType.TOILET.getActionNumber()] =   new Position(520,330);
       levelsDefinition[2][GameObjectType.WARDROBE.getActionNumber()] = new Position(20,210);
       levelsDefinition[2][GameObjectType.BOOK_FALL_2.getActionNumber()] = new Position(260,280);
       //common
       levelsDefinition[2][GameObjectType.CAT_HISS.getActionNumber()] = new Position(300,150);
       levelsDefinition[2][GameObjectType.CAT_MEOW.getActionNumber()] = new Position(475,150);
       levelsDefinition[2][GameObjectType.CAT_PURR.getActionNumber()] = new Position(300,75);
       levelsDefinition[2][GameObjectType.CURTAIN_1.getActionNumber()] = new Position(200,430);
       levelsDefinition[2][GameObjectType.DOOR_1.getActionNumber()] = new Position(700,255);
       levelsDefinition[2][GameObjectType.STAIRS_1.getActionNumber()] = new Position(315,330);
       levelsDefinition[2][GameObjectType.WINDOW_1.getActionNumber()] = new Position(130,410);

       //EASY 4
       levelsDefinition[3][GameObjectType.BOX.getActionNumber()] = new Position(150,150);
       levelsDefinition[3][GameObjectType.BROKEN_GLASS.getActionNumber()] = new Position(300,250);
       levelsDefinition[3][GameObjectType.CREAK_FLOOR_2.getActionNumber()] = new Position(600,200);
       levelsDefinition[3][GameObjectType.MARBLE.getActionNumber()] = new Position(400,200);
       levelsDefinition[3][GameObjectType.MOUSE_1.getActionNumber()] = new Position(50,230);
       levelsDefinition[3][GameObjectType.ROCKING_CHAIR.getActionNumber()] = new Position(400,300);
       levelsDefinition[3][GameObjectType.TINKLE_TOY.getActionNumber()] = new Position(200,250);
       //common
       levelsDefinition[3][GameObjectType.CAT_HISS.getActionNumber()] = new Position(300,150);
       levelsDefinition[3][GameObjectType.CAT_MEOW.getActionNumber()] = new Position(475,150);
       levelsDefinition[3][GameObjectType.CAT_PURR.getActionNumber()] = new Position(300,75);
       levelsDefinition[3][GameObjectType.CURTAIN_1.getActionNumber()] = new Position(200,375);
       levelsDefinition[3][GameObjectType.DOOR_1.getActionNumber()] = new Position(650,270);
       levelsDefinition[3][GameObjectType.STAIRS_1.getActionNumber()] = new Position(560,320);
       levelsDefinition[3][GameObjectType.WINDOW_1.getActionNumber()] = new Position(100,330);

       //MEDIUM 1
       levelsDefinition[4][GameObjectType.APPLE_1.getActionNumber()] = new Position(220,120);
       levelsDefinition[4][GameObjectType.APPLE_2.getActionNumber()] = new Position(320,20);
       levelsDefinition[4][GameObjectType.BALL_KICK.getActionNumber()] = new Position(450,300);
       levelsDefinition[4][GameObjectType.BIRD_TRILL.getActionNumber()] = new Position(430,150);
       levelsDefinition[4][GameObjectType.BIRD_WHISTLE.getActionNumber()] = new Position(260,210);
       levelsDefinition[4][GameObjectType.CHICKEN_CLUCK.getActionNumber()] = new Position(250,60);
       levelsDefinition[4][GameObjectType.FIRE_FLAME.getActionNumber()] = new Position(350,100);
       levelsDefinition[4][GameObjectType.GATE_GROAN.getActionNumber()] = new Position(80,240);
       levelsDefinition[4][GameObjectType.KENNEL_CHAIN.getActionNumber()] = new Position(500,300);
       levelsDefinition[4][GameObjectType.ROOSTER_SQUAWK.getActionNumber()] = new Position(620,290);
       levelsDefinition[4][GameObjectType.SHOVEL_DIGGING.getActionNumber()] = new Position(550,200);
       levelsDefinition[4][GameObjectType.WALK_GRAVEL.getActionNumber()] = new Position(420,200);
       levelsDefinition[4][GameObjectType.WOOD_CHOP.getActionNumber()] = new Position(500,230);
       levelsDefinition[4][GameObjectType.FROG.getActionNumber()] = new Position(170,250);
       //common
       levelsDefinition[4][GameObjectType.CAT_HISS.getActionNumber()] = new Position(300,150);
       levelsDefinition[4][GameObjectType.CAT_MEOW.getActionNumber()] = new Position(475,150);
       levelsDefinition[4][GameObjectType.CAT_PURR.getActionNumber()] = new Position(300,75);
       levelsDefinition[4][GameObjectType.DOG_GROWL.getActionNumber()] = new Position(180,170);
       levelsDefinition[4][GameObjectType.DOG_FRIENDLY.getActionNumber()] = new Position(100,150);
       levelsDefinition[4][GameObjectType.DOG_HOWL.getActionNumber()] = new Position(490,100);
       levelsDefinition[4][GameObjectType.DOG_THREAT.getActionNumber()] = new Position(320,250);
       levelsDefinition[4][GameObjectType.DRIPPING_1.getActionNumber()] = new Position(650,190);
       levelsDefinition[4][GameObjectType.TOY_SQUEAKY.getActionNumber()] = new Position(250,150);
       levelsDefinition[4][GameObjectType.STONE.getActionNumber()] = new Position(70,200);

       //MEDIUM 2
       levelsDefinition[5][GameObjectType.APPLE_1.getActionNumber()] = new Position(220,120);
       levelsDefinition[5][GameObjectType.APPLE_2.getActionNumber()] = new Position(320,20);
       levelsDefinition[5][GameObjectType.BALL_KICK.getActionNumber()] = new Position(450,300);
       levelsDefinition[5][GameObjectType.BIRD_TRILL.getActionNumber()] = new Position(430,150);
       levelsDefinition[5][GameObjectType.BIRD_WHISTLE.getActionNumber()] = new Position(260,210);
       levelsDefinition[5][GameObjectType.CHICKEN_CLUCK.getActionNumber()] = new Position(250,60);
       levelsDefinition[5][GameObjectType.FIRE_FLAME.getActionNumber()] = new Position(350,100);
       levelsDefinition[5][GameObjectType.GATE_GROAN.getActionNumber()] = new Position(80,240);
       levelsDefinition[5][GameObjectType.KENNEL_CHAIN.getActionNumber()] = new Position(500,300);
       levelsDefinition[5][GameObjectType.ROOSTER_SQUAWK.getActionNumber()] = new Position(620,290);
       levelsDefinition[5][GameObjectType.SHOVEL_DIGGING.getActionNumber()] = new Position(550,200);
       levelsDefinition[5][GameObjectType.WALK_GRAVEL.getActionNumber()] = new Position(420,200);
       levelsDefinition[5][GameObjectType.WOOD_CHOP.getActionNumber()] = new Position(500,230);
       levelsDefinition[5][GameObjectType.FROG.getActionNumber()] = new Position(170,250);
       //common
       levelsDefinition[5][GameObjectType.CAT_HISS.getActionNumber()] = new Position(300,150);
       levelsDefinition[5][GameObjectType.CAT_MEOW.getActionNumber()] = new Position(475,150);
       levelsDefinition[5][GameObjectType.CAT_PURR.getActionNumber()] = new Position(300,75);
       levelsDefinition[5][GameObjectType.DOG_GROWL.getActionNumber()] = new Position(180,170);
       levelsDefinition[5][GameObjectType.DOG_FRIENDLY.getActionNumber()] = new Position(100,150);
       levelsDefinition[5][GameObjectType.DOG_HOWL.getActionNumber()] = new Position(490,100);
       levelsDefinition[5][GameObjectType.DOG_THREAT.getActionNumber()] = new Position(320,250);
       levelsDefinition[5][GameObjectType.DRIPPING_1.getActionNumber()] = new Position(650,190);
       levelsDefinition[5][GameObjectType.TOY_SQUEAKY.getActionNumber()] = new Position(250,150);
       levelsDefinition[5][GameObjectType.STONE.getActionNumber()] = new Position(70,200);

       //MEDIUM 3
       levelsDefinition[6][GameObjectType.COINS_1.getActionNumber()] = new Position(45,230);
       levelsDefinition[6][GameObjectType.DUSTBIN_COVER.getActionNumber()] = new Position(600,250);
       levelsDefinition[6][GameObjectType.FLOWERPOT_BROKEN.getActionNumber()] = new Position(530,150);
       levelsDefinition[6][GameObjectType.MOUSE_2.getActionNumber()] = new Position(110,275);
       levelsDefinition[6][GameObjectType.RADIO_2.getActionNumber()] = new Position(25,280);
       levelsDefinition[6][GameObjectType.WALK_HEELS.getActionNumber()] = new Position(330,250);
       levelsDefinition[6][GameObjectType.WALK_WATER.getActionNumber()] = new Position(400,180);
       levelsDefinition[6][GameObjectType.WINDOW_2.getActionNumber()] = new Position(150,360);
       levelsDefinition[6][GameObjectType.SHARDS.getActionNumber()] = new Position(600,150);
       //common
       levelsDefinition[6][GameObjectType.CAT_HISS.getActionNumber()] = new Position(300,150);
       levelsDefinition[6][GameObjectType.CAT_MEOW.getActionNumber()] = new Position(475,100);
       levelsDefinition[6][GameObjectType.CAT_PURR.getActionNumber()] = new Position(300,75);
       levelsDefinition[6][GameObjectType.DOG_GROWL.getActionNumber()] = new Position(150,250);
       levelsDefinition[6][GameObjectType.DOG_FRIENDLY.getActionNumber()] = new Position(120,155);
       levelsDefinition[6][GameObjectType.DOG_HOWL.getActionNumber()] = new Position(230,250);
       levelsDefinition[6][GameObjectType.DOG_THREAT.getActionNumber()] = new Position(450,250);
       levelsDefinition[6][GameObjectType.DRIPPING_1.getActionNumber()] = new Position(700,220);
       levelsDefinition[6][GameObjectType.TOY_SQUEAKY.getActionNumber()] = new Position(250,150);
       levelsDefinition[6][GameObjectType.STONE.getActionNumber()] = new Position(400,100);

       //MEDIUM 4
       levelsDefinition[7][GameObjectType.COINS_1.getActionNumber()] = new Position(45,230);
       levelsDefinition[7][GameObjectType.DUSTBIN_COVER.getActionNumber()] = new Position(600,250);
       levelsDefinition[7][GameObjectType.FLOWERPOT_BROKEN.getActionNumber()] = new Position(530,150);
       levelsDefinition[7][GameObjectType.MOUSE_2.getActionNumber()] = new Position(110,275);
       levelsDefinition[7][GameObjectType.RADIO_2.getActionNumber()] = new Position(25,280);
       levelsDefinition[7][GameObjectType.WALK_HEELS.getActionNumber()] = new Position(330,250);
       levelsDefinition[7][GameObjectType.WALK_WATER.getActionNumber()] = new Position(400,180);
       levelsDefinition[7][GameObjectType.WINDOW_2.getActionNumber()] = new Position(150,360);
       levelsDefinition[7][GameObjectType.SHARDS.getActionNumber()] = new Position(600,150);
       //common
       levelsDefinition[7][GameObjectType.CAT_HISS.getActionNumber()] = new Position(300,150);
       levelsDefinition[7][GameObjectType.CAT_MEOW.getActionNumber()] = new Position(475,100);
       levelsDefinition[7][GameObjectType.CAT_PURR.getActionNumber()] = new Position(300,75);
       levelsDefinition[7][GameObjectType.DOG_GROWL.getActionNumber()] = new Position(150,250);
       levelsDefinition[7][GameObjectType.DOG_FRIENDLY.getActionNumber()] = new Position(120,155);
       levelsDefinition[7][GameObjectType.DOG_HOWL.getActionNumber()] = new Position(230,250);
       levelsDefinition[7][GameObjectType.DOG_THREAT.getActionNumber()] = new Position(450,250);
       levelsDefinition[7][GameObjectType.DRIPPING_1.getActionNumber()] = new Position(700,220);
       levelsDefinition[7][GameObjectType.TOY_SQUEAKY.getActionNumber()] = new Position(250,150);
       levelsDefinition[7][GameObjectType.STONE.getActionNumber()] = new Position(400,100);

       //HARD 1
       levelsDefinition[8][GameObjectType.ALARM.getActionNumber()] = new Position(50,400);
       levelsDefinition[8][GameObjectType.STEPS_1.getActionNumber()] = new Position(450,120);
       //common 1,2
       levelsDefinition[8][GameObjectType.INKPOT.getActionNumber()] = new Position(530,170);
       levelsDefinition[8][GameObjectType.PHONE_HANGUP.getActionNumber()] = new Position(100,200);
       levelsDefinition[8][GameObjectType.PHONE_TALK.getActionNumber()] = new Position(200,200);
       levelsDefinition[8][GameObjectType.PHONE_RING.getActionNumber()] = new Position(300,200);
       levelsDefinition[8][GameObjectType.STAMP.getActionNumber()] = new Position(250,100);
       levelsDefinition[8][GameObjectType.CLOCK_2.getActionNumber()] = new Position(710,245);
       levelsDefinition[8][GameObjectType.WINDOW_3.getActionNumber()] = new Position(600,400);
       levelsDefinition[8][GameObjectType.BUCKET_2.getActionNumber()] = new Position(500,300);
       levelsDefinition[8][GameObjectType.DOOR_2.getActionNumber()] = new Position(100,278);
       levelsDefinition[8][GameObjectType.LAMP_2.getActionNumber()] = new Position(600,250);
       levelsDefinition[8][GameObjectType.PAPER_2.getActionNumber()] = new Position(380,120);
       levelsDefinition[8][GameObjectType.PEN_2.getActionNumber()] = new Position(380,200);
       levelsDefinition[8][GameObjectType.STAIRS_2.getActionNumber()] = new Position(300,332);

       //HARD 2
       levelsDefinition[9][GameObjectType.BOOKS.getActionNumber()] = new Position(620,205);
       levelsDefinition[9][GameObjectType.TYPEWRITER_TYPING_ALT.getActionNumber()] = new Position(200,100);
       levelsDefinition[9][GameObjectType.TYPEWRITER_TYPING_FAST.getActionNumber()] = new Position(650,100);
       levelsDefinition[9][GameObjectType.TYPEWRITER_TYPING_NORMAL.getActionNumber()] = new Position(300,300);
       levelsDefinition[9][GameObjectType.CURTAIN_2.getActionNumber()] = new Position(200,375);
       levelsDefinition[9][GameObjectType.SAFE_2.getActionNumber()] = new Position(760,135);
       //common 1,2
       levelsDefinition[9][GameObjectType.INKPOT.getActionNumber()] = new Position(530,170);
       levelsDefinition[9][GameObjectType.PHONE_HANGUP.getActionNumber()] = new Position(100,200);
       levelsDefinition[9][GameObjectType.PHONE_TALK.getActionNumber()] = new Position(200,200);
       levelsDefinition[9][GameObjectType.PHONE_RING.getActionNumber()] = new Position(300,200);
       levelsDefinition[9][GameObjectType.STAMP.getActionNumber()] = new Position(350,50);
       levelsDefinition[9][GameObjectType.CLOCK_2.getActionNumber()] = new Position(860,205);
       levelsDefinition[9][GameObjectType.WINDOW_3.getActionNumber()] = new Position(800,350);
       levelsDefinition[9][GameObjectType.BUCKET_2.getActionNumber()] = new Position(450,300);
       levelsDefinition[9][GameObjectType.DOOR_2.getActionNumber()] = new Position(100,258);
       levelsDefinition[9][GameObjectType.LAMP_2.getActionNumber()] = new Position(550,250);
       levelsDefinition[9][GameObjectType.PAPER_2.getActionNumber()] = new Position(380,120);
       levelsDefinition[9][GameObjectType.PEN_2.getActionNumber()] = new Position(380,200);
       levelsDefinition[9][GameObjectType.STAIRS_2.getActionNumber()] = new Position(700,320);

       //HARD 3
       levelsDefinition[10][GameObjectType.SMALL_WINDOW.getActionNumber()] = new Position(450,450);
       //common 3,4
       levelsDefinition[10][GameObjectType.DOOR_3.getActionNumber()] = new Position(150,300);
       levelsDefinition[10][GameObjectType.STAIRS_2.getActionNumber()] = new Position(310,297);
       levelsDefinition[10][GameObjectType.COINS_3.getActionNumber()] = new Position(600,200);
       levelsDefinition[10][GameObjectType.STEPS_2.getActionNumber()] = new Position(450,120);
       levelsDefinition[10][GameObjectType.CREAKING_CHAIR.getActionNumber()] = new Position(300,50);
       levelsDefinition[10][GameObjectType.CLOCK_3.getActionNumber()] = new Position(530,480);
       levelsDefinition[10][GameObjectType.KEYS.getActionNumber()] = new Position(400,230);
       levelsDefinition[10][GameObjectType.KEY_IN_LOCK.getActionNumber()] = new Position(50,330);
       levelsDefinition[10][GameObjectType.ROD.getActionNumber()] = new Position(200,200);
       levelsDefinition[10][GameObjectType.SAFE_CLOSE.getActionNumber()] = new Position(437,299);

       //HARD 4
       levelsDefinition[11][GameObjectType.DRIPPING_2.getActionNumber()] = new Position(200,200);
       levelsDefinition[11][GameObjectType.MOUSE_3.getActionNumber()] = new Position(150,225);
       //common 3,4
       levelsDefinition[11][GameObjectType.DOOR_3.getActionNumber()] = new Position(120,285);
       levelsDefinition[11][GameObjectType.STAIRS_2.getActionNumber()] = new Position(290,265);
       levelsDefinition[11][GameObjectType.COINS_3.getActionNumber()] = new Position(670,200);
       levelsDefinition[11][GameObjectType.STEPS_2.getActionNumber()] = new Position(450,120);
       levelsDefinition[11][GameObjectType.CREAKING_CHAIR.getActionNumber()] = new Position(300,50);
       levelsDefinition[11][GameObjectType.CLOCK_3.getActionNumber()] = new Position(530,480);
       levelsDefinition[11][GameObjectType.KEYS.getActionNumber()] = new Position(400,210);
       levelsDefinition[11][GameObjectType.KEY_IN_LOCK.getActionNumber()] = new Position(30,330);
       levelsDefinition[11][GameObjectType.ROD.getActionNumber()] = new Position(570,250);
       levelsDefinition[11][GameObjectType.SAFE_CLOSE.getActionNumber()] = new Position(437,298);
    }


   public static List<GameObjectType> getGameObjectsByLevel(Level level){
      return getGameObjectsByLevelIndex(level.ordinal());
   }

   private static List<GameObjectType> getGameObjectsByLevelIndex(int absoluteLevelIndex){
      ArrayList<GameObjectType> gameObjects = new ArrayList<GameObjectType>();
      for(int i = 0; i < levelsDefinition[absoluteLevelIndex].length; i++) {
         if(levelsDefinition[absoluteLevelIndex][i] != null) gameObjects.add(GameObjectType.getGameObjectTypeForActionNumber(i));
      }
      return gameObjects;
   }

   public static Position getGameObjectsPosition(Level level, GameObjectType gameObjectType) {
      return levelsDefinition[level.ordinal()][gameObjectType.getActionNumber()];
   }
}
