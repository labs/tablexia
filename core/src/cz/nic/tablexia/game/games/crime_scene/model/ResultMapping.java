/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.model;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.games.crime_scene.assets.CrimeSceneAssets;

/**
 * Created by danilov on 13.7.16.
 */
public enum ResultMapping {
    NO_STAR(AbstractTablexiaGame.GameResult.NO_STAR, CrimeSceneAssets.SCORE0_TEXT_KEY, CrimeSceneAssets.SCORE_RATING_0),
    ONE_STAR(AbstractTablexiaGame.GameResult.ONE_STAR, CrimeSceneAssets.SCORE1_TEXT_KEY, CrimeSceneAssets.SCORE_RATING_1),
    TWO_STAR(AbstractTablexiaGame.GameResult.TWO_STAR, CrimeSceneAssets.SCORE2_TEXT_KEY, CrimeSceneAssets.SCORE_RATING_2),
    THREE_STAR(AbstractTablexiaGame.GameResult.THREE_STAR, CrimeSceneAssets.SCORE3_TEXT_KEY, CrimeSceneAssets.SCORE_RATING_3);

    private AbstractTablexiaGame.GameResult gameResult;
    private String textKey;
    private String soundName;

    ResultMapping(AbstractTablexiaGame.GameResult gameResult, String textKey, String soundName) {
        this.gameResult = gameResult;
        this.textKey = textKey;
        this.soundName = soundName;
    }

    public static ResultMapping getResultMappingForGameResult(AbstractTablexiaGame.GameResult gameResult) {
        for(ResultMapping resultMapping : ResultMapping.values()) {
            if(resultMapping.gameResult == gameResult) {
                return resultMapping;
            }
        }
        return null;
    }

    public String getTextKey() {
        return textKey;
    }

    public String getSoundName() {
        return soundName;
    }
}
