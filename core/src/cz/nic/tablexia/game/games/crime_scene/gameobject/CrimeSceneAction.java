/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.gameobject;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.util.ui.actionwidget.Action;

/**
 * Created by danilov on 13.7.16.
 */
class CrimeSceneAction extends Action {

    protected final GameObject  gameObject;
    protected final int         actionCardSize;

    public CrimeSceneAction(String texturePath, int orderNumber, int actionSize, AbstractTablexiaGame tablexiaGame, int actionNumber, GameObject gameObject) {
        super(texturePath, orderNumber, actionSize, 0, 0, true, tablexiaGame, actionNumber);
        this.gameObject = gameObject;
        this.actionCardSize = actionSize;
    }

    @Override
    public void onCollision(int orderNumber, int actionSizeBigger, int actionSizeSmaller) {
        super.onCollision(orderNumber, actionCardSize, actionCardSize);
    }

    @Override
    public void setDragListener() {
        addListener(new DragListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return downAction(x, y, true);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if(gameObject.isDragging()) upAction(false);
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                dragAction(x, y);
            }
        });
    }

    @Override
    public void setBounds (float x, float y, float width, float height) {
        super.setBounds(x,y,width,height);
        super.setSize(super.getDrawable().getMinWidth(), super.getDrawable().getMinHeight());
    }
}
