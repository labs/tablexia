/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.listeners;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;

import cz.nic.tablexia.game.games.crime_scene.CrimeSceneGame;
import cz.nic.tablexia.game.games.crime_scene.gameobject.GameObject;

/**
 * Created by danilov on 13.7.16.
 */
public class CrimeSceneDragListener extends DragListener {

    private static final int    DRAG_THRESHOLD      = 10;
    private int                 dragDeltaTime       = 0;
    protected GameObject          gameObject;
    protected CrimeSceneGame crimeSceneGame;

    public CrimeSceneDragListener(GameObject gameObject, CrimeSceneGame crimeSceneGame) {
        this.gameObject = gameObject;
        this.crimeSceneGame = crimeSceneGame;
    }

    @Override
    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        crimeSceneGame.resolveClickOnGameObject(gameObject, x, y);
        return true;
    }

    @Override
    public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
        if(!gameObject.isDragging()) crimeSceneGame.highlightGameObject(gameObject);
        gameObject.setDragging(false);
        resetDragDeltaTime();
    }

    @Override
    public void touchDragged (InputEvent event, float x, float y, int pointer) {
        increaseDragDeltaTime(gameObject);
    }

    /**
     * Every time touchDragged() method in DragListener is called, dragDeltaTime is increased in order to better differentiate
     * users touch or drag on screen
     * @param gameObject dragged game object
     */
    protected void increaseDragDeltaTime(GameObject gameObject) {
        if(dragDeltaTime++ == DRAG_THRESHOLD)  {
            gameObject.setDragging(true);
        }
    }

    protected void resetDragDeltaTime() {
        dragDeltaTime = 0;
    }
}
