/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.model;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.difficulty.GameDifficulty;

/**
 * Created by danilov on 13.7.16.
 */
public enum ResultStars {

    THREE_STAR(AbstractTablexiaGame.GameResult.THREE_STAR, 12, 20, 28),
    TWO_STAR(AbstractTablexiaGame.GameResult.TWO_STAR, 9, 16, 23),
    ONE_STAR(AbstractTablexiaGame.GameResult.ONE_STAR, 7, 13, 19),
    NO_STAR(AbstractTablexiaGame.GameResult.NO_STAR, -1, -1, -1);

    private AbstractTablexiaGame.GameResult gameResult;
    private int         easyScore;
    private int         mediumScore;
    private int         hardScore;

    ResultStars(AbstractTablexiaGame.GameResult gameResult, int easyScore, int mediumScore, int hardScore) {
        this.gameResult = gameResult;
        this.easyScore = easyScore;
        this.mediumScore = mediumScore;
        this.hardScore = hardScore;
    }

    public static AbstractTablexiaGame.GameResult getStarCountForDifficultyAndErrors(GameDifficulty gameDifficulty, int score) {
        for(ResultStars star : ResultStars.values()) {
            if(score >= star.getScoreForDifficulty(gameDifficulty)) {
                return star.gameResult;
            }
        }
        return AbstractTablexiaGame.GameResult.NO_STAR;
    }

    public static int getScoreForGameResultAndDifficulty(AbstractTablexiaGame.GameResult result, GameDifficulty gameDifficulty) {
        for(ResultStars star : ResultStars.values()) {
            if(star.gameResult == result) {
                switch (gameDifficulty) {
                    case EASY:   return star.easyScore   + 1;
                    case MEDIUM: return star.mediumScore + 1;
                    case HARD:   return star.hardScore   + 1;
                    default: return 0;
                }
            }
        }
        return 0;
    }

    public int getScoreForDifficulty(GameDifficulty gameDifficulty) {
        switch(gameDifficulty) {
            case EASY:
                return easyScore;
            case MEDIUM:
                return mediumScore;
            case HARD:
                return hardScore;
            default:
                return -1;
        }
    }
}
