/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.model.bonus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaAtlasManager;
import cz.nic.tablexia.util.Log;

/**
 * Created by lmarik on 5.3.18.
 */

public class BonusAssetsManager {
    private TablexiaAtlasManager atlasManager;

    private List<String> regions;
    private List<String> atlases;

    protected String atlasPath;

    public BonusAssetsManager() {
        atlasManager = new TablexiaAtlasManager(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL);
        regions = new ArrayList<>();
        atlases = new ArrayList<>();
    }

    public void loadAtlas(String atlasPath) {
        this.atlasPath = atlasPath;

        if (!atlases.contains(atlasPath)) {
            Log.info(getClass(), "Need load atlas " + atlasPath);
            final CountDownLatch countDownLatch = new CountDownLatch(1);

            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    atlasManager.loadAtlas(atlasPath);
                    atlasManager.finishLoading();
                    countDownLatch.countDown();
                }
            });

            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                Log.err(getClass(), "Cannot wait for loading bonus assets!", e);
                return;
            }

            TextureAtlas atlas = atlasManager.getAsset(atlasPath, TextureAtlas.class);
            addRegionName(atlas.getRegions());
            atlases.add(atlasPath);

            Log.info(getClass(), "Atlas " + atlasPath + " loaded");
        } else {
            Log.info(getClass(), "Atlas " + atlasPath + " is already loaded");
        }
    }

    private void addRegionName(Array<TextureAtlas.AtlasRegion> atlasRegions) {
        for (TextureAtlas.AtlasRegion atlasRegion : atlasRegions) {
            regions.add(atlasRegion.name);
        }
    }

    public TextureRegion getTextureRegion(String regionName) {
        return atlasManager.getTextureRegionFromAtlas(atlasPath, regionName, null);
    }

    public boolean containsRegion(String regionName) {
        return regions.contains(regionName);
    }

    public void clearData() {
        Log.info(getClass(), "Clear Data");
        regions.clear();
        atlases.clear();
        atlasManager.clear();
    }

    public void disposeData(){
        Log.info(getClass(),"Dispose");
        atlasManager.dispose();
    }

}
