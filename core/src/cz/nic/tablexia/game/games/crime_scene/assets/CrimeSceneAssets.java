/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.crime_scene.assets;

/**
 * Created by danilov on 11.4.16.
 */
public class CrimeSceneAssets {

    private static final String GFX_PATH             = "gfx/";
    private static final String SFX_PATH             = "sfx/";
    private static final String ACTION_CARDS         = "actioncards/";
    private static final String GAME_OBJECTS         = "gameobjects/";
    private static final String NUMBERS              = "numbers/";
    private static final String BACKGROUND           = "background/";
    private static final String CONTROLS_PATH        = GFX_PATH + "control/";
    private static final String LEVEL_1              = "level1/";
    private static final String LEVEL_2              = "level2/";
    private static final String LEVEL_3              = "level3/";
    private static final String LEVEL_4              = "level4/";
    private static final String LEVEL_1_2            = "level1_2/";
    private static final String LEVEL_3_4            = "level3_4/";
    private static final String BONUS_PATH           = "bonus/";

    public static final String STRIP_WIDGET_BACKGROUND  = GFX_PATH + BACKGROUND + "strip_widget_background";
    public static final String EASY_GAME_MAP_1          = GFX_PATH + BACKGROUND + "easy_game_map_one";
    public static final String EASY_GAME_MAP_2          = GFX_PATH + BACKGROUND + "easy_game_map_two";
    public static final String EASY_GAME_MAP_3          = GFX_PATH + BACKGROUND + "easy_game_map_three";
    public static final String EASY_GAME_MAP_4          = GFX_PATH + BACKGROUND + "easy_game_map_four";
    public static final String MEDIUM_GAME_MAP_1_2      = GFX_PATH + BACKGROUND + "medium_game_map_one_two";
    public static final String MEDIUM_GAME_MAP_3_4      = GFX_PATH + BACKGROUND + "medium_game_map_three_four";
    public static final String HARD_GAME_MAP_1          = GFX_PATH + BACKGROUND + "hard_game_map_one";
    public static final String HARD_GAME_MAP_2          = GFX_PATH + BACKGROUND + "hard_game_map_two";
    public static final String HARD_GAME_MAP_3          = GFX_PATH + BACKGROUND + "hard_game_map_three";
    public static final String HARD_GAME_MAP_4          = GFX_PATH + BACKGROUND + "hard_game_map_four";

    public static final String CONTROL_ACTUAL 		= CONTROLS_PATH + "actual";
    public static final String CONTROL_NEXT	 		= CONTROLS_PATH + "next";


    //======== EASY LEVEL =============

    //== actions ==

    public static final String BOOK_BROWSE_ACTION       = GFX_PATH + ACTION_CARDS + LEVEL_1 + "book_browse";
    public static final String BOOK_FALL_1_ACTION       = GFX_PATH + ACTION_CARDS + LEVEL_1 + "book_fall_one";
    public static final String BUDGIES_ACTION           = GFX_PATH + ACTION_CARDS + LEVEL_1 + "budgies";
    public static final String CLOCK_CUCKOO_ACTION      = GFX_PATH + ACTION_CARDS + LEVEL_1 + "clock_cuckoo";
    public static final String CREAK_FLOOR_ACTION       = GFX_PATH + ACTION_CARDS + LEVEL_1 + "creak_floor";
    public static final String DRUM_ACTION              = GFX_PATH + ACTION_CARDS + LEVEL_1 + "drum";
    public static final String FLUTE_ACTION             = GFX_PATH + ACTION_CARDS + LEVEL_1 + "flute";
    public static final String GRAMOPHONE_ACTION        = GFX_PATH + ACTION_CARDS + LEVEL_1 + "gramophone";
    public static final String GUITAR_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_1 + "guitar";
    public static final String PARROT_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_1 + "parrot";
    public static final String PIANO_ACTION             = GFX_PATH + ACTION_CARDS + LEVEL_1 + "piano";
    public static final String RADIO_1_ACTION           = GFX_PATH + ACTION_CARDS + LEVEL_1 + "radio_one";
    public static final String SAFE_1_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_1 + "safe_one";
    public static final String TELEPHONE_ACTION         = GFX_PATH + ACTION_CARDS + LEVEL_1 + "telephone";
    public static final String VIOLIN_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_1 + "violin";
    
    public static final String BROOM_ACTION             = GFX_PATH + ACTION_CARDS + LEVEL_2 + "broom";
    public static final String CORK_ACTION              = GFX_PATH + ACTION_CARDS + LEVEL_2 + "cork";
    public static final String EGG_ACTION               = GFX_PATH + ACTION_CARDS + LEVEL_2 + "egg";
    public static final String FRY_ACTION               = GFX_PATH + ACTION_CARDS + LEVEL_2 + "fry";
    public static final String GLASS_ACTION             = GFX_PATH + ACTION_CARDS + LEVEL_2 + "glass";
    public static final String KETTLE_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_2 + "kettle";
    public static final String PAN_ACTION               = GFX_PATH + ACTION_CARDS + LEVEL_2 + "pan";
    public static final String PLATE_ACTION             = GFX_PATH + ACTION_CARDS + LEVEL_2 + "plate";
    public static final String SHARPENING_KNIFE_ACTION  = GFX_PATH + ACTION_CARDS + LEVEL_2 + "sharpening_knife";
    public static final String SLICING_ACTION           = GFX_PATH + ACTION_CARDS + LEVEL_2 + "slicing";
    public static final String SOUP_ACTION              = GFX_PATH + ACTION_CARDS + LEVEL_2 + "soup";
    public static final String STOVE_ACTION             = GFX_PATH + ACTION_CARDS + LEVEL_2 + "stove";
    public static final String TAP_1_ACTION             = GFX_PATH + ACTION_CARDS + LEVEL_2 + "tap_one";
    public static final String CUCUMBERS_JAR_ACTION     = GFX_PATH + ACTION_CARDS + LEVEL_2 + "cucumbers_jar";
    public static final String POURING_RICE_ACTION      = GFX_PATH + ACTION_CARDS + LEVEL_2 + "pouring_rice";
    public static final String SIDEBOARD_ACTION         = GFX_PATH + ACTION_CARDS + LEVEL_2 + "sideboard";

    public static final String BOOK_FALL_2_ACTION       = GFX_PATH + ACTION_CARDS + LEVEL_3 + "book_fall_two";
    public static final String BUCKET_1_ACTION          = GFX_PATH + ACTION_CARDS + LEVEL_3 + "bucket_one";
    public static final String DRAWER_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_3 + "drawer";
    public static final String TAP_2_ACTION             = GFX_PATH + ACTION_CARDS + LEVEL_3 + "tap_two";
    public static final String LAMP_1_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_3 + "lamp_one";
    public static final String MIRROR_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_3 + "mirror";
    public static final String MUSIC_BOX_ACTION         = GFX_PATH + ACTION_CARDS + LEVEL_3 + "music_box";
    public static final String PAPER_1_ACTION           = GFX_PATH + ACTION_CARDS + LEVEL_3 + "paper_one";
    public static final String PEN_1_ACTION             = GFX_PATH + ACTION_CARDS + LEVEL_3 + "pen_one";
    public static final String PILLOW_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_3 + "pillow";
    public static final String SHOWER_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_3 + "shower";
    public static final String TEETH_ACTION             = GFX_PATH + ACTION_CARDS + LEVEL_3 + "teeth";
    public static final String TOILET_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_3 + "toilet";
    public static final String WARDROBE_ACTION          = GFX_PATH + ACTION_CARDS + LEVEL_3 + "wardrobe";
    
    public static final String BOX_ACTION               = GFX_PATH + ACTION_CARDS + LEVEL_4 + "box";
    public static final String BROKEN_GLASS_ACTION      = GFX_PATH + ACTION_CARDS + LEVEL_4 + "broken_glass";
    public static final String CREAK_FLOOR_2_ACTION     = GFX_PATH + ACTION_CARDS + LEVEL_4 + "creak_floor_two";
    public static final String MARBLE_ACTION            = GFX_PATH + ACTION_CARDS + LEVEL_4 + "marble";
    public static final String ROCKING_CHAIR_ACTION     = GFX_PATH + ACTION_CARDS + LEVEL_4 + "rocking_chair";
    public static final String TINKLE_TOY_ACTION        = GFX_PATH + ACTION_CARDS + LEVEL_4 + "tinkle_toy";
    public static final String CLOCK_1_ACTION           = GFX_PATH + ACTION_CARDS + LEVEL_4 + "clock_one";


    public static final String CURTAIN_1_ACTION         = GFX_PATH + ACTION_CARDS + "curtain_one";
    public static final String STAIRS_1_ACTION          = GFX_PATH + ACTION_CARDS + "stairs_one";
    public static final String WINDOW_1_ACTION          = GFX_PATH + ACTION_CARDS + "window_one";
    public static final String DOOR_1_ACTION            = GFX_PATH + ACTION_CARDS + "door_one";

    //== objects ==

    public static final String BOOK_BROWSE_OBJECT       =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "book_browse";
    public static final String BOOK_FALL_1_OBJECT       =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "book_fall_one";
    public static final String BUDGIES_OBJECT           =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "budgies";
    public static final String CLOCK_CUCKOO_OBJECT      =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "clock_cuckoo";
    public static final String CREAK_FLOOR_OBJECT       =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "creak_floor";
    public static final String DRUM_OBJECT              =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "drum";
    public static final String FLUTE_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "flute";
    public static final String GRAMOPHONE_OBJECT        =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "gramophone";
    public static final String GUITAR_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "guitar";
    public static final String PARROT_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "parrot";
    public static final String PIANO_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "piano";
    public static final String RADIO_1_OBJECT           =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "radio_one";
    public static final String SAFE_1_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "safe_one";
    public static final String TELEPHONE_OBJECT         =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "telephone";
    public static final String VIOLIN_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "violin";

    public static final String BROOM_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "broom";
    public static final String CORK_OBJECT              =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "cork";
    public static final String EGG_OBJECT               =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "egg";
    public static final String FRY_OBJECT               =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "fry";
    public static final String GLASS_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "glass";
    public static final String KETTLE_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "kettle";
    public static final String PAN_OBJECT               =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "pan";
    public static final String PLATE_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "plate";
    public static final String SHARPENING_KNIFE_OBJECT  =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "sharpening_knife";
    public static final String SLICING_OBJECT           =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "slicing";
    public static final String SOUP_OBJECT              =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "soup";
    public static final String STOVE_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "stove";
    public static final String TAP_1_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "tap_one";
    public static final String CUCUMBERS_JAR_OBJECT     =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "cucumbers_jar";
    public static final String POURING_RICE_OBJECT      =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "pouring_rice";
    public static final String SIDEBOARD_OBJECT         =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "sideboard";

    public static final String BOOK_FALL_2_OBJECT       =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "book_fall_two";
    public static final String BUCKET_1_OBJECT          =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "bucket_one";
    public static final String DRAWER_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "drawer";
    public static final String TAP_2_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "tap_two";
    public static final String LAMP_1_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "lamp_one";
    public static final String MIRROR_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "mirror";
    public static final String MUSIC_BOX_OBJECT         =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "music_box";
    public static final String PAPER_1_OBJECT           =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "paper_one";
    public static final String PEN_1_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "pen_one";
    public static final String PILLOW_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "pillow";
    public static final String SHOWER_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "shower";
    public static final String TEETH_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "teeth";
    public static final String TOILET_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "toilet";
    public static final String WARDROBE_OBJECT          =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "wardrobe";

    public static final String BOX_OBJECT               =  GFX_PATH + GAME_OBJECTS + LEVEL_4 + "box";
    public static final String BROKEN_GLASS_OBJECT      =  GFX_PATH + GAME_OBJECTS + LEVEL_4 + "broken_glass";
    public static final String CREAK_FLOOR_2_OBJECT     =  GFX_PATH + GAME_OBJECTS + LEVEL_4 + "creak_floor_two";
    public static final String MARBLE_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_4 + "marble";
    public static final String ROCKING_CHAIR_OBJECT     =  GFX_PATH + GAME_OBJECTS + LEVEL_4 + "rocking_chair";
    public static final String TINKLE_TOY_OBJECT        =  GFX_PATH + GAME_OBJECTS + LEVEL_4 + "tinkle_toy";
    public static final String CLOCK_1_OBJECT           =  GFX_PATH + GAME_OBJECTS + LEVEL_4 + "clock_one";

    public static final String CURTAIN_1_OBJECT         =  GFX_PATH + GAME_OBJECTS + "curtain_one";
    public static final String STAIRS_1_OBJECT          =  GFX_PATH + GAME_OBJECTS + "stairs_one";
    public static final String WINDOW_1_OBJECT          =  GFX_PATH + GAME_OBJECTS + "window_one";
    public static final String DOOR_1_OBJECT            =  GFX_PATH + GAME_OBJECTS + "door_one";


    //== sounds ==

    public static final String BOOK_BROWSE_SOUND        = SFX_PATH + LEVEL_1 + "book_browse.mp3";
    public static final String BUDGIES_SOUND            = SFX_PATH + LEVEL_1 + "budgies.mp3";
    public static final String CLOCK_CUCKOO_SOUND       = SFX_PATH + LEVEL_1 + "clock_cuckoo.mp3";
    public static final String CREAK_FLOOR_SOUND        = SFX_PATH + LEVEL_1 + "creak_floor.mp3";
    public static final String DRUM_SOUND               = SFX_PATH + LEVEL_1 + "drum.mp3";
    public static final String FLUTE_SOUND              = SFX_PATH + LEVEL_1 + "flute.mp3";
    public static final String GRAMOPHONE_SOUND         = SFX_PATH + LEVEL_1 + "gramophone.mp3";
    public static final String GUITAR_SOUND             = SFX_PATH + LEVEL_1 + "guitar.mp3";
    public static final String PARROT_SOUND             = SFX_PATH + LEVEL_1 + "parrot.mp3";
    public static final String PIANO_SOUND              = SFX_PATH + LEVEL_1 + "piano.mp3";
    public static final String RADIO_1_SOUND            = SFX_PATH + LEVEL_1 + "radio_one.mp3";
    public static final String TELEPHONE_SOUND          = SFX_PATH + LEVEL_1 + "telephone.mp3";
    public static final String VIOLIN_SOUND             = SFX_PATH + LEVEL_1 + "violin.mp3";

    public static final String BROOM_SOUND              = SFX_PATH + LEVEL_2 + "broom.mp3";
    public static final String CORK_SOUND               = SFX_PATH + LEVEL_2 + "cork.mp3";
    public static final String EGG_SOUND                = SFX_PATH + LEVEL_2 + "egg.mp3";
    public static final String FRY_SOUND                = SFX_PATH + LEVEL_2 + "fry.mp3";
    public static final String GLASS_SOUND              = SFX_PATH + LEVEL_2 + "glass.mp3";
    public static final String KETTLE_SOUND             = SFX_PATH + LEVEL_2 + "kettle.mp3";
    public static final String PAN_SOUND                = SFX_PATH + LEVEL_2 + "pan.mp3";
    public static final String PLATE_SOUND              = SFX_PATH + LEVEL_2 + "plate.mp3";
    public static final String SHARPENING_KNIFE_SOUND   = SFX_PATH + LEVEL_2 + "sharpening_knife.mp3";
    public static final String SLICING_SOUND            = SFX_PATH + LEVEL_2 + "slicing.mp3";
    public static final String SOUP_SOUND               = SFX_PATH + LEVEL_2 + "soup.mp3";
    public static final String STOVE_SOUND              = SFX_PATH + LEVEL_2 + "stove.mp3";

    public static final String CUCUMBERS_JAR_SOUND      = SFX_PATH + LEVEL_2 + "cucumbers_jar.mp3";
    public static final String POURING_RICE_SOUND       = SFX_PATH + LEVEL_2 + "pouring_rice.mp3";
    public static final String SIDEBOARD_SOUND          = SFX_PATH + LEVEL_2 + "sideboard.mp3";

    public static final String DRAWER_SOUND             = SFX_PATH + LEVEL_3 + "drawer.mp3";
    public static final String MIRROR_SOUND             = SFX_PATH + LEVEL_3 + "mirror.mp3";
    public static final String MUSIC_BOX_SOUND          = SFX_PATH + LEVEL_3 + "music_box.mp3";

    public static final String PILLOW_SOUND             = SFX_PATH + LEVEL_3 + "pillow.mp3";
    public static final String SHOWER_SOUND             = SFX_PATH + LEVEL_3 + "shower.mp3";
    public static final String TEETH_SOUND              = SFX_PATH + LEVEL_3 + "teeth.mp3";
    public static final String TOILET_SOUND             = SFX_PATH + LEVEL_3 + "toilet.mp3";
    public static final String WARDROBE_SOUND           = SFX_PATH + LEVEL_3 + "wardrobe.mp3";

    public static final String BOX_SOUND                = SFX_PATH + LEVEL_4 + "box.mp3";
    public static final String BROKEN_GLASS_SOUND       = SFX_PATH + LEVEL_4 + "broken_glass.mp3";
    public static final String CREAK_FLOOR_2_SOUND      = SFX_PATH + LEVEL_4 + "creak_floor_two.mp3";
    public static final String MARBLE_SOUND             = SFX_PATH + LEVEL_4 + "marble.mp3";
    public static final String ROCKING_CHAIR_SOUND      = SFX_PATH + LEVEL_4 + "rocking_chair.mp3";
    public static final String TINKLE_TOY_SOUND         = SFX_PATH + LEVEL_4 + "tinkle_toy.mp3";

    public static final String STAIRS_1_SOUND           = SFX_PATH + "stairs_one.mp3";
    public static final String BOOK_FALL_SOUND          = SFX_PATH + "book_fall.mp3";
    public static final String TAP_SOUND                = SFX_PATH + "tap.mp3";

    //======== MEDIUM =============

    //===actions===
        
    public static final String APPLE_1_ACTION                  =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "apple_one";
    public static final String APPLE_2_ACTION                  =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "apple_two";
    public static final String BALL_KICK_ACTION                =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "ball_kick";
    public static final String BIRD_TRILL_ACTION               =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "bird_trill";
    public static final String BIRD_WHISTLE_ACTION             =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "bird_whistle";
    public static final String CHICKEN_CLUCK_ACTION            =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "chicken_cluck";
    public static final String FIRE_FLAME_ACTION               =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "fire_flame";
    public static final String FROG_ACTION                     =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "frog";
    public static final String GATE_GROAN_ACTION               =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "gate_groan";
    public static final String KENNEL_CHAIN_ACTION             =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "kennel_chain";
    public static final String ROOSTER_SQUAWK_ACTION           =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "rooster_squawk";
    public static final String SHOVEL_DIGGING_ACTION           =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "shovel_digging";
    public static final String WALK_GRAVEL_ACTION              =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "walk_gravel";
    public static final String WOOD_CHOP_ACTION                =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "wood_chop";

    public static final String DUSTBIN_COVER_ACTION            =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "dustbin_cover";
    public static final String FLOWERPOT_BROKEN_ACTION         =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "flowerpot_broken";
    public static final String RADIO_2_ACTION                  =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "radio_two";
    public static final String SHARDS_ACTION                   =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "shards";
    public static final String WALK_HEELS_ACTION               =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "walk_heels";
    public static final String WALK_WATER_ACTION               =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "walk_water";
    public static final String WINDOW_2_ACTION                 =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "window_two";

    public static final String DOG_FRIENDLY_ACTION              =  GFX_PATH + ACTION_CARDS + "dog_friendly";
    public static final String DOG_GROWL_ACTION                 =  GFX_PATH + ACTION_CARDS + "dog_growl";
    public static final String DOG_HOWL_ACTION                  =  GFX_PATH + ACTION_CARDS + "dog_howl";
    public static final String DOG_THREAT_ACTION                =  GFX_PATH + ACTION_CARDS + "dog_threat";
    public static final String DRIPPING_1_ACTION                =  GFX_PATH + ACTION_CARDS + "dripping_one";
    public static final String STONE_ACTION                     =  GFX_PATH + ACTION_CARDS + "stone";
    public static final String TOY_SQUEAKY_ACTION               =  GFX_PATH + ACTION_CARDS + "toy_squeaky";
    
    //===objects===
    
    public static final String APPLE_1_OBJECT                  =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "apple_one";
    public static final String APPLE_2_OBJECT                  =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "apple_two";
    public static final String BALL_KICK_OBJECT                =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "ball_kick";
    public static final String BIRD_TRILL_OBJECT               =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "bird_trill";
    public static final String BIRD_WHISTLE_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "bird_whistle";
    public static final String CHICKEN_CLUCK_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "chicken_cluck";
    public static final String FIRE_FLAME_OBJECT               =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "fire_flame";
    public static final String FROG_OBJECT                     =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "frog";
    public static final String GATE_GROAN_OBJECT               =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "gate_groan";
    public static final String KENNEL_CHAIN_OBJECT             =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "kennel_chain";
    public static final String ROOSTER_SQUAWK_OBJECT           =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "rooster_squawk";
    public static final String SHOVEL_DIGGING_OBJECT           =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "shovel_digging";
    public static final String WALK_GRAVEL_OBJECT              =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "walk_gravel";
    public static final String WOOD_CHOP_OBJECT                =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "wood_chop";

    public static final String DUSTBIN_COVER_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "dustbin_cover";
    public static final String FLOWERPOT_BROKEN_OBJECT         =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "flowerpot_broken";
    public static final String RADIO_2_OBJECT                  =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "radio_two";
    public static final String SHARDS_OBJECT                   =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "shards";
    public static final String WALK_HEELS_OBJECT               =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "walk_heels";
    public static final String WALK_WATER_OBJECT               =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "walk_water";
    public static final String WINDOW_2_OBJECT                 =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "window_two";

    public static final String DOG_FRIENDLY_OBJECT              =  GFX_PATH + GAME_OBJECTS + "dog_friendly";
    public static final String DOG_GROWL_OBJECT                 =  GFX_PATH + GAME_OBJECTS + "dog_growl";
    public static final String DOG_HOWL_OBJECT                  =  GFX_PATH + GAME_OBJECTS + "dog_howl";
    public static final String DOG_THREAT_OBJECT                =  GFX_PATH + GAME_OBJECTS + "dog_threat";
    public static final String DRIPPING_1_OBJECT                =  GFX_PATH + GAME_OBJECTS + "dripping_one";
    public static final String STONE_OBJECT                     =  GFX_PATH + GAME_OBJECTS + "stone";
    public static final String TOY_SQUEAKY_OBJECT               =  GFX_PATH + GAME_OBJECTS + "toy_squeaky";

    //===sounds===
    
    public static final String APPLE_1_SOUND                  =  SFX_PATH + LEVEL_1_2 + "apple_one.mp3";
    public static final String APPLE_2_SOUND                  =  SFX_PATH + LEVEL_1_2 + "apple_two.mp3";
    public static final String BALL_KICK_SOUND                =  SFX_PATH + LEVEL_1_2 + "ball_kick.mp3";
    public static final String BIRD_TRILL_SOUND               =  SFX_PATH + LEVEL_1_2 + "bird_trill.mp3";
    public static final String BIRD_WHISTLE_SOUND             =  SFX_PATH + LEVEL_1_2 + "bird_whistle.mp3";
    public static final String CHICKEN_CLUCK_SOUND            =  SFX_PATH + LEVEL_1_2 + "chicken_cluck.mp3";
    public static final String FIRE_FLAME_SOUND               =  SFX_PATH + LEVEL_1_2 + "fire_flame.mp3";
    public static final String FROG_SOUND                     =  SFX_PATH + LEVEL_1_2 + "frog.mp3";
    public static final String GATE_GROAN_SOUND               =  SFX_PATH + LEVEL_1_2 + "gate_groan.mp3";
    public static final String KENNEL_CHAIN_SOUND             =  SFX_PATH + LEVEL_1_2 + "kennel_chain.mp3";
    public static final String ROOSTER_SQUAWK_SOUND           =  SFX_PATH + LEVEL_1_2 + "rooster_squawk.mp3";
    public static final String SHOVEL_DIGGING_SOUND           =  SFX_PATH + LEVEL_1_2 + "shovel_digging.mp3";
    public static final String WALK_GRAVEL_SOUND              =  SFX_PATH + LEVEL_1_2 + "walk_gravel.mp3";
    public static final String WOOD_CHOP_SOUND                =  SFX_PATH + LEVEL_1_2 + "wood_chop.mp3";

    public static final String DUSTBIN_COVER_SOUND            =  SFX_PATH + LEVEL_3_4 + "dustbin_cover.mp3";
    public static final String FLOWERPOT_BROKEN_SOUND         =  SFX_PATH + LEVEL_3_4 + "flowerpot_broken.mp3";
    public static final String RADIO_2_SOUND                  =  SFX_PATH + LEVEL_3_4 + "radio_two.mp3";
    public static final String SHARDS_SOUND                   =  SFX_PATH + LEVEL_3_4 + "shards.mp3";
    public static final String WALK_HEELS_SOUND               =  SFX_PATH + LEVEL_3_4 + "walk_heels.mp3";
    public static final String WALK_WATER_SOUND               =  SFX_PATH + LEVEL_3_4 + "walk_water.mp3";

    public static final String DOG_FRIENDLY_SOUND              =  SFX_PATH + "dog_friendly.mp3";
    public static final String DOG_GROWL_SOUND                 =  SFX_PATH + "dog_growl.mp3";
    public static final String DOG_HOWL_SOUND                  =  SFX_PATH + "dog_howl.mp3";
    public static final String DOG_THREAT_SOUND                =  SFX_PATH + "dog_threat.mp3";
    public static final String DRIPPING_1_SOUND                =  SFX_PATH + "dripping_one.mp3";
    public static final String STONE_SOUND                     =  SFX_PATH + "stone.mp3";
    public static final String TOY_SQUEAKY_SOUND               =  SFX_PATH + "toy_squeaky.mp3";

    //======== HARD =============

    //===actions===

    public static final String ALARM_ACTION                     =  GFX_PATH + ACTION_CARDS + LEVEL_1 + "alarm";
    public static final String BUCKET_2_ACTION                  =  GFX_PATH + ACTION_CARDS + LEVEL_1 + "bucket_two";
    public static final String STEPS_1_ACTION                   =  GFX_PATH + ACTION_CARDS + LEVEL_1 + "steps_one";

    public static final String INKPOT_ACTION                    =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "inkpot";
    public static final String LAMP_2_ACTION                    =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "lamp_two";
    public static final String PEN_2_ACTION                     =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "pen_two";
    public static final String PHONE_HANGUP_ACTION              =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "phone_hangup";
    public static final String PHONE_RING_ACTION                =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "phone_ring";
    public static final String PHONE_TALK_ACTION                =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "phone_talk";
    public static final String STAMP_ACTION                     =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "stamp";
    public static final String WINDOW_3_ACTION                  =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "window_three";
    public static final String CLOCK_2_ACTION                   =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "clock_two";
    public static final String DOOR_2_ACTION                    =  GFX_PATH + ACTION_CARDS + LEVEL_1_2 + "door_two";

    public static final String BOOKS_ACTION                     =  GFX_PATH + ACTION_CARDS + LEVEL_2 + "books";
    public static final String CURTAIN_2_ACTION                 =  GFX_PATH + ACTION_CARDS + LEVEL_2 + "curtain_two";
    public static final String PAPER_2_ACTION                   =  GFX_PATH + ACTION_CARDS + LEVEL_2 + "paper_two";
    public static final String SAFE_2_ACTION                    =  GFX_PATH + ACTION_CARDS + LEVEL_2 + "safe_two";
    public static final String TYPEWRITER_TYPING_ALT_ACTION     =  GFX_PATH + ACTION_CARDS + LEVEL_2 + "typewriter_typing_alt";
    public static final String TYPEWRITER_TYPING_FAST_ACTION    =  GFX_PATH + ACTION_CARDS + LEVEL_2 + "typewriter_typing_fast";
    public static final String TYPEWRITER_TYPING_NORMAL_ACTION  =  GFX_PATH + ACTION_CARDS + LEVEL_2 + "typewriter_typing_normal";

    public static final String SMALL_WINDOW_ACTION              =  GFX_PATH + ACTION_CARDS + LEVEL_3 + "small_window";

//    public static final String BAR_ACTION                       =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "bar";
//    public static final String BRICK_ACTION                     =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "brick";
    public static final String CREAKING_CHAIR_ACTION            =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "creaking_chair";
//    public static final String COINS_2_ACTION                   =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "coins_two";
    public static final String KEY_IN_LOCK_ACTION               =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "key_in_lock";
    public static final String KEYS_ACTION                      =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "keys";
    public static final String ROD_ACTION                       =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "rod";
//    public static final String SAFE_CLICK_ACTION                =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "safe_click";
    public static final String SAFE_CLOSE_ACTION                =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "safe_close";
//    public static final String SAFE_CREAK_ACTION                =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "safe_creak";
    public static final String STEPS_2_ACTION                   =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "steps_two";
    public static final String CLOCK_3_ACTION                   =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "clock_three";
    public static final String DOOR_3_ACTION                    =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "door_three";
    public static final String COINS_3_ACTION                   =  GFX_PATH + ACTION_CARDS + LEVEL_3_4 + "coins_three";

    public static final String DRIPPING_2_ACTION                =  GFX_PATH + ACTION_CARDS + LEVEL_4 + "dripping_two";

    public static final String STAIRS_2_ACTION                  =  GFX_PATH + ACTION_CARDS + "stairs_two";

    //===objects===

    public static final String ALARM_OBJECT                     =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "alarm";
    public static final String BUCKET_2_OBJECT                  =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "bucket_two";
    public static final String STEPS_1_OBJECT                   =  GFX_PATH + GAME_OBJECTS + LEVEL_1 + "steps_one";

    public static final String INKPOT_OBJECT                    =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "inkpot";
    public static final String LAMP_2_OBJECT                    =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "lamp_two";
    public static final String PEN_2_OBJECT                     =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "pen_two";
    public static final String PHONE_HANGUP_OBJECT              =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "phone_hangup";
    public static final String PHONE_RING_OBJECT                =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "phone_ring";
    public static final String PHONE_TALK_OBJECT                =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "phone_talk";
    public static final String STAMP_OBJECT                     =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "stamp";
    public static final String WINDOW_3_OBJECT                  =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "window_three";
    public static final String CLOCK_2_OBJECT                   =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "clock_two";
    public static final String DOOR_2_OBJECT                    =  GFX_PATH + GAME_OBJECTS + LEVEL_1_2 + "door_two";

    public static final String BOOKS_OBJECT                     =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "books";
    public static final String CURTAIN_2_OBJECT                 =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "curtain_two";
    public static final String PAPER_2_OBJECT                   =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "paper_two";
    public static final String SAFE_2_OBJECT                    =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "safe_two";
    public static final String TYPEWRITER_TYPING_ALT_OBJECT     =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "typewriter_typing_alt";
    public static final String TYPEWRITER_TYPING_FAST_OBJECT    =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "typewriter_typing_fast";
    public static final String TYPEWRITER_TYPING_NORMAL_OBJECT  =  GFX_PATH + GAME_OBJECTS + LEVEL_2 + "typewriter_typing_normal";

    public static final String SMALL_WINDOW_OBJECT              =  GFX_PATH + GAME_OBJECTS + LEVEL_3 + "small_window";

    public static final String CREAKING_CHAIR_OBJECT            =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "creaking_chair";
    public static final String KEY_IN_LOCK_OBJECT               =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "key_in_lock";
    public static final String KEYS_OBJECT                      =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "keys";
    public static final String ROD_OBJECT                       =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "rod";
    public static final String SAFE_CLOSE_OBJECT                =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "safe_close";
    public static final String STEPS_2_OBJECT                   =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "steps_two";
    public static final String CLOCK_3_OBJECT                   =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "clock_three";
    public static final String DOOR_3_OBJECT                    =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "door_three";
    public static final String COINS_3_OBJECT                   =  GFX_PATH + GAME_OBJECTS + LEVEL_3_4 + "coins_three";

    public static final String DRIPPING_2_OBJECT                =  GFX_PATH + GAME_OBJECTS + LEVEL_4 + "dripping_two";
    public static final String MOUSE_3_OBJECT                   =  GFX_PATH + GAME_OBJECTS + LEVEL_4 + "mouse_three";

    public static final String STAIRS_2_OBJECT                  =  GFX_PATH + GAME_OBJECTS + "stairs_two";

    //===sounds===

    public static final String ALARM_SOUND                     =  SFX_PATH + LEVEL_1 + "alarm.mp3";

    public static final String INKPOT_SOUND                    =  SFX_PATH + LEVEL_1_2 + "inkpot.mp3";
    public static final String PHONE_HANGUP_SOUND              =  SFX_PATH + LEVEL_1_2 + "phone_hangup.mp3";
    public static final String PHONE_RING_SOUND                =  SFX_PATH + LEVEL_1_2 + "phone_ring.mp3";
    public static final String PHONE_TALK_SOUND                =  SFX_PATH + LEVEL_1_2 + "phone_talk.mp3";
    public static final String STAMP_SOUND                     =  SFX_PATH + LEVEL_1_2 + "stamp.mp3";
    public static final String CLOCK_2_SOUND                   =  SFX_PATH + LEVEL_1_2 + "clock_two.mp3";

    public static final String BOOKS_SOUND                     =  SFX_PATH + LEVEL_2 + "books.mp3";
    public static final String TYPEWRITER_TYPING_ALT_SOUND     =  SFX_PATH + LEVEL_2 + "typewriter_typing_alt.mp3";
    public static final String TYPEWRITER_TYPING_FAST_SOUND    =  SFX_PATH + LEVEL_2 + "typewriter_typing_fast.mp3";
    public static final String TYPEWRITER_TYPING_NORMAL_SOUND  =  SFX_PATH + LEVEL_2 + "typewriter_typing_normal.mp3";

    public static final String SMALL_WINDOW_SOUND              =  SFX_PATH + LEVEL_3 + "small_window.mp3";

    public static final String CREAKING_CHAIR_SOUND            =  SFX_PATH + LEVEL_3_4 + "creaking_chair.mp3";
    public static final String COINS_3_SOUND                   =  SFX_PATH + LEVEL_3_4 + "coins_three.mp3";
    public static final String KEY_IN_LOCK_SOUND               =  SFX_PATH + LEVEL_3_4 + "key_in_lock.mp3";
    public static final String KEYS_SOUND                      =  SFX_PATH + LEVEL_3_4 + "keys.mp3";
    public static final String ROD_SOUND                       =  SFX_PATH + LEVEL_3_4 + "rod.mp3";
    public static final String SAFE_CLOSE_SOUND                =  SFX_PATH + LEVEL_3_4 + "safe_close.mp3";
    public static final String DOOR_3_SOUND                    =  SFX_PATH + LEVEL_3_4 + "door_three.mp3";

    public static final String DRIPPING_2_SOUND                =  SFX_PATH + LEVEL_4 + "dripping_two.mp3";
    public static final String MOUSE_3_SOUND                   =  SFX_PATH + LEVEL_4 + "mouse_three.mp3";

    public static final String STAIRS_2_SOUND                  =  SFX_PATH + "stairs_two.mp3";
    public static final String STEPS_SOUND                     =  SFX_PATH + "steps.mp3";

    // === COMMON ===

    public static final String CAT_HISS_OBJECT          =  GFX_PATH + GAME_OBJECTS + "cat_hiss";
    public static final String CAT_MEOW_OBJECT          =  GFX_PATH + GAME_OBJECTS + "cat_meow";
    public static final String CAT_PURR_OBJECT          =  GFX_PATH + GAME_OBJECTS + "cat_purr";
    public static final String COINS_1_OBJECT           =  GFX_PATH + GAME_OBJECTS + "coins_one";
    public static final String MOUSE_1_2_OBJECT         =  GFX_PATH + GAME_OBJECTS + "mouse_one_two";

    public static final String CAT_HISS_ACTION          = GFX_PATH + ACTION_CARDS + "cat_hiss";
    public static final String CAT_MEOW_ACTION          = GFX_PATH + ACTION_CARDS + "cat_meow";
    public static final String CAT_PURR_ACTION          = GFX_PATH + ACTION_CARDS + "cat_purr";
    public static final String COINS_1_ACTION           = GFX_PATH + ACTION_CARDS + "coins_one";
    public static final String MOUSE_1_2_3_ACTION       = GFX_PATH + ACTION_CARDS + "mouse_one_two_three";

    public static final String CAT_HISS_SOUND           = SFX_PATH + "cat_hiss.mp3";
    public static final String CAT_MEOW_SOUND           = SFX_PATH + "cat_meow.mp3";
    public static final String CAT_PURR_SOUND           = SFX_PATH + "cat_purr.mp3";
    public static final String BUCKET_SOUND             = SFX_PATH + "bucket.mp3";
    public static final String CLOCK_1_3_SOUND          = SFX_PATH + "clock_one_three.mp3";
    public static final String COINS_1_SOUND            = SFX_PATH + "coins_one.mp3";
    public static final String CURTAIN_SOUND            = SFX_PATH + "curtain.mp3";
    public static final String DOOR_1_2_SOUND           = SFX_PATH + "door_one_two.mp3";
    public static final String LAMP_SOUND               = SFX_PATH + "lamp.mp3";
    public static final String MOUSE_1_2_SOUND          = SFX_PATH + "mouse_one_two.mp3";
    public static final String PAPER_SOUND              = SFX_PATH + "paper.mp3";
    public static final String PEN_SOUND                = SFX_PATH + "pen.mp3";
    public static final String SAFE_SOUND               = SFX_PATH + "safe.mp3";
    public static final String WINDOW_SOUND             = SFX_PATH + "window.mp3";

    //======== NUMBERS =============
    
    public static final String ZERO                 = GFX_PATH + NUMBERS + "zero";
    public static final String ONE                  = GFX_PATH + NUMBERS + "one";
    public static final String TWO                  = GFX_PATH + NUMBERS + "two";
    public static final String THREE                = GFX_PATH + NUMBERS + "three";
    public static final String FOUR                 = GFX_PATH + NUMBERS + "four";
    public static final String FIVE                 = GFX_PATH + NUMBERS + "five";
    public static final String SIX                  = GFX_PATH + NUMBERS + "six";
    public static final String SEVEN                = GFX_PATH + NUMBERS + "seven";
    public static final String EIGHT                = GFX_PATH + NUMBERS + "eight";
    public static final String NINE                 = GFX_PATH + NUMBERS + "nine";

    //======== TEXTS =============
    
    public static final String REPLAY               = "game_crimescene_replay";
    public static final String SCORE0_TEXT_KEY      = "score_0";
    public static final String SCORE1_TEXT_KEY      = "score_1";
    public static final String SCORE2_TEXT_KEY      = "score_2";
    public static final String SCORE3_TEXT_KEY      = "score_3";
    public static final String DONE_TEXT            = "complete";

    //======== OTHER =============

    public static final String BLANK_ACTION_CARD    = GFX_PATH + ACTION_CARDS + "empty_card";

    public static final String SCORE_RATING_0       = SFX_PATH + "score_rating_0.mp3";
    public static final String SCORE_RATING_1       = SFX_PATH + "score_rating_1.mp3";
    public static final String SCORE_RATING_2       = SFX_PATH + "score_rating_2.mp3";
    public static final String SCORE_RATING_3       = SFX_PATH + "score_rating_3.mp3";

    //======== BONUS LEVEL =============

    //== distractor sounds ==
    public static final String BANK_BONUS_BACK              = BONUS_PATH + "bank_bonus_back.mp3";
    public static final String CLOCKWORK_BONUS_BACK         = BONUS_PATH + "clockwork_bonus_back.mp3";
    public static final String COFFEE_BONUS_BACK            = BONUS_PATH + "coffee_bonus_back.mp3";
    public static final String MENU_TOWN_BONUS_BACK         = BONUS_PATH + "menu_town_bonus_back.mp3";
    public static final String NIGHTWATCH_DAY_BONUS_BACK    = BONUS_PATH + "nightwatch_day_bonus_back.mp3";
    public static final String NIGHTWATCH_NIGHT_BONUS_BACK  = BONUS_PATH + "nightwatch_night_bonus_back.mp3";
    public static final String SANDGLASS_BONUS_BACK         = BONUS_PATH + "sandglass_bonus_back.mp3";
    public static final String TOWN_BONUS_BACK              = BONUS_PATH + "town_bonus_back.mp3";
    public static final String TRAIN_TRACKING_BONUS_BACK    = BONUS_PATH + "train_tracking_bonus_back.mp3";
    public static final String WAY_OFFICE_BONUS_BACK        = BONUS_PATH + "way_office_bonus_back.mp3";

}