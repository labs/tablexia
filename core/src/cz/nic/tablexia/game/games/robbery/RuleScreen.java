/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.robbery;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.Map;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.games.robbery.assets.RobberyAssets;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

public class RuleScreen extends AbstractRobberyScreen {
    public static final     String  BUTTON_OK = "understand button";

    private static final    float   PAPER_HEIGHT_RATIO              = 19f/20;

    private static final    float   LABEL_Y_POSITION_RATIO          = 23f/40;
    private static final 	Color   LABEL_TEXT_COLOR                = Color.GRAY;

    private static final 	float   TITLE_PAPER_POSITION_X_OFFSET   = 1f/20;
    private static final    float   TITLE_PAPER_Y_POSITION_RATIO    = 3f/5;
    private static final 	float   TITLE_WIDTH_RATIO               = 3f/5;
    private static final 	float   TITLE_HEIGHT_RATIO              = 1f/5;

    private static final    float   BUTTON_WIDTH                    = 170;
    private static final    float   BUTTON_HEIGHT                   = 80;
    private static final    float   BUTTON_X_POSITION_RATIO         = 1f/2;
    private static final    float   BUTTON_Y_POSITION_RATIO         = 9f/40;
    private static final    String  BUTTON_TEXT_KEY                 = "game_robbery_rulemessage_understand";

    private Image background;
    private TextureRegion paperTextureRegion;

    public RuleScreen(RobberyGame robberyGame) {
        super(robberyGame);
        paperTextureRegion = robberyGame.getGameGlobalTextureRegion(AbstractTablexiaGame.GAME_RULE_BACKGROUND);
    }


//////////////////////////// SCREEN LIFECYCLE
	
	@Override
	protected void screenLoaded(Map<String, String> screenState) {
        prepareBackground();

        Image           paper   = preparePaper();
        Image           title   = prepareTitle(paper.getX(), paper.getY(), paper.getWidth(), paper.getHeight());
		TablexiaLabel   label   = prepareLabel(paper.getX(), paper.getY(), paper.getWidth(), paper.getHeight());
        TablexiaButton  button  = prepareButton(paper.getX(), paper.getY(), paper.getWidth(), paper.getHeight());
        button.setName(BUTTON_OK);
		getStage().addActor(background);
		getStage().addActor(paper);
        getStage().addActor(title);
		getStage().addActor(label);
        getStage().addActor(button);


	}

    @Override
    protected void screenResized(int width, int height) {
        prepareBackgroundSize();
    }


//////////////////////////// SCREEN COMPONENTS

    private void prepareBackground() {
        background = new TablexiaNoBlendingImage(getApplicationTextureRegion(ApplicationAtlasManager.BACKGROUND_WOODEN));
        prepareBackgroundSize();
    }

    private void prepareBackgroundSize() {
        background.setBounds(getViewportLeftX(), getViewportBottomY(), getViewportWidth(), getViewportHeight());
    }

    private Image preparePaper() {
        Image paper = new Image(paperTextureRegion);
        float paperRatio = (getSceneInnerHeight() * PAPER_HEIGHT_RATIO) / paper.getHeight();
        float paperWidth = paper.getWidth() * paperRatio;
        float paperHeight = paper.getHeight() * paperRatio;
        paper.setBounds((getSceneWidth() / 2) - (paperWidth / 2), (getSceneInnerHeight() / 2) - (paperHeight / 2), paperWidth, paperHeight);
        return paper;
    }

    private Image prepareTitle(float paperX, float paperY, float paperWidth, float paperHeight) {
        float titleWidth = paperWidth * TITLE_WIDTH_RATIO;
        float titleHeight = paperHeight * TITLE_HEIGHT_RATIO;

        Image title = new Image(getScreenTextureRegion(RobberyAssets.SCREEN_RULEMESSAGE_PAPER_TITLE));
        title.setBounds(paperX + (paperWidth / 2) - (titleWidth / 2) - (titleWidth * TITLE_PAPER_POSITION_X_OFFSET),
                        paperY + (paperHeight * TITLE_PAPER_Y_POSITION_RATIO),
                        titleWidth,
                        titleHeight);

        return title;
    }

    private TablexiaLabel prepareLabel(float paperX, float paperY, float paperWidth, float paperHeight) {
        float labelWidth = paperWidth * TITLE_WIDTH_RATIO;
        float labelHeight = paperHeight * TITLE_HEIGHT_RATIO;

        ApplicationFontManager.FontType fontType = ApplicationFontManager.FontType.REGULAR_18;
        TablexiaLabel label = new TablexiaLabel(getData().getRuleMessageText(this), new TablexiaLabel.TablexiaLabelStyle(fontType, LABEL_TEXT_COLOR), true);
        label.setWrap(true);
        label.setBounds(paperX + (paperWidth / 2) - (labelWidth / 2),
                        paperY + (paperHeight * LABEL_Y_POSITION_RATIO) - labelHeight,
                        labelWidth,
                        labelHeight);

        return label;
    }

    private TablexiaButton prepareButton(float x, float y, float width, float height) {
        float buttonX = x + (width * BUTTON_X_POSITION_RATIO) - (BUTTON_WIDTH / 2);
        float buttonY = y + (height * BUTTON_Y_POSITION_RATIO) - (BUTTON_HEIGHT / 2);

        return new StandardTablexiaButton(getText(BUTTON_TEXT_KEY))
                .useOnce(true)
                .setButtonBounds(buttonX, buttonY, BUTTON_WIDTH, BUTTON_HEIGHT)
                .setInputListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        showGameScreen();
                    }
                });
    }
}
