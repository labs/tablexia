/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.creature.attribute.clothing;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.CreatureSuperGenericType;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.accessories.AccessoriesAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.bottom.BottomAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.headgear.HeadgearAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.top.TopAttribute;

@CreatureSuperGenericType(isGeneric = true)
public abstract class ClothingAttribute extends Attribute {

    public static List<AttributeDescription> getTextures() {
        return new ArrayList<AttributeDescription>() {

            private static final long serialVersionUID = 8692724100864869650L;

            {
                addAll(HeadgearAttribute.getTextures());
                addAll(TopAttribute.getTextures());
                addAll(BottomAttribute.getTextures());
                addAll(AccessoriesAttribute.getTextures());
            }
        };
    }
    
    public static List<Class<? extends Attribute>> getAttributeClasses() {
        return new ArrayList<Class<? extends Attribute>>() {

            private static final long serialVersionUID = -1377013489500837025L;

            {
            	add(ClothingAttribute.class);
                addAll(HeadgearAttribute.getAttributeClasses());
                addAll(TopAttribute.getAttributeClasses());
                addAll(BottomAttribute.getAttributeClasses());
                addAll(AccessoriesAttribute.getAttributeClasses());
            }
        };
    }

    public ClothingAttribute(AttributeConstituent attributeConstituent, AttributeColor color, float x, float y, int z, String textureName) {
        super(attributeConstituent, color, x, y, z, textureName);
    }

}
