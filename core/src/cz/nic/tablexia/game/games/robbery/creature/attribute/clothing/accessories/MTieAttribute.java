/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.accessories;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.robbery.assets.RobberyAssets;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot.AttributeGender;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;

public class MTieAttribute extends AccessoriesAttribute {

    private static final AttributeConstituent       ATTRIBUTE_CONSTITUENT   = AttributeConstituent.F;
    private static final List<AttributeDescription> TEXTURES                = new ArrayList<AttributeDescription>() {

		private static final long serialVersionUID = 6731353377162453034L;

		{
            add(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, RobberyAssets.ATTRIBUTE_M_TIE_GREEN, MTieAttribute.class));
            add(new AttributeDescription(AttributeColor.GREY,  AttributeGender.MALE, RobberyAssets.ATTRIBUTE_M_TIE_GREY,  MTieAttribute.class));
            add(new AttributeDescription(AttributeColor.RED,   AttributeGender.MALE, RobberyAssets.ATTRIBUTE_M_TIE_RED,   MTieAttribute.class));
        }
    };

    public static List<AttributeDescription> getTextures() {
        return TEXTURES;
    }
    
    public static AttributeGender getAttributeGender() {
		return AttributeGender.MALE;
	}

    public MTieAttribute(AttributeColor color, String textureName) {
        super(ATTRIBUTE_CONSTITUENT, color, textureName);
    }

}
