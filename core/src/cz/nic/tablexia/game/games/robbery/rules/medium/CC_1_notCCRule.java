/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules.medium;

import java.util.Random;

import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.game.games.robbery.rules.easy.CC_0_notCCRule;

/**
 * 
 * @author Matyáš Latner
 */
public class CC_1_notCCRule extends CC_0_notCCRule {
    
	private   static final int 	 	GROUP_SIZE	= 3;

    public CC_1_notCCRule(Random random) {
        super(random, GROUP_SIZE);
        T1_OFFSET = Integer.valueOf(2);
    }
    
    @Override
    public GameRulesDefinition getGameRuleDefinition() {
    	return GameRulesDefinition.CC_1_notCC;
    }

}
