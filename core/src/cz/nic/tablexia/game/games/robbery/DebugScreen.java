/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.robbery;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import java.util.List;
import java.util.Map;

import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.TablexiaLabel;

public class DebugScreen extends AbstractRobberyScreen {

    private static final float SELECTBOX_WIDTH_RATIO    = 1f/5;
    private static final float SELECTBOX_HEIGHT_RATIO   = 1f/20;
    private static final float SELECTBOX_X_HEIGHT_RATIO = 17f/20;
    private static final float RULE_WIDTH_RATIO         = 2f/3;
    private static final float RULE_HEIGHT_RATIO        = 2f/10;
    private static final float RULE_X_RATIO             = 1f/2;
    private static final float RULE_Y_RATIO             = 1f/50;

    private enum DebugGroupColors {
        RED(Color.RED),
        GREEN(Color.GREEN),
        BLUE(Color.BLUE),
        YELLOW(Color.YELLOW),
        WHITE(Color.WHITE),
        PINK(Color.PINK),
        CYAN(Color.CYAN),
        LIGHTBLUE(new Color(0.569f, 0.612f, 0.976f, 1f)),
        LIGHTRED(new Color(0.976f, 0.569f, 0.569f, 1f)),
        LIGHTGREEN(new Color(0.569f, 0.976f, 0.631f, 1f)),
        LIGHTPURPLE(new Color(0.969f, 0.569f, 0.976f, 1f));

        Color color;

        DebugGroupColors(Color color) {
            this.color = color;
        }

        public Color getColor() {
            return color;
        }
    }

    public DebugScreen(RobberyGame robberyGame) {
        super(robberyGame);
    }


//////////////////////////// SCREEN LIFECYCLE
	
	@Override
	protected void screenLoaded(Map<String, String> screenState) {
		displayAllCreatures(getData().getCreatures(), getStage());
        displayRule();
        displayRuleSelectBox();
	}


//////////////////////////// DEBUG SCREEN

    private void displayRuleSelectBox() {
        BitmapFont font = ApplicationFontManager.getInstance().getDistanceFieldFont(ApplicationFontManager.FontType.REGULAR_16);

        ScrollPane.ScrollPaneStyle scrollPaneStyle = new ScrollPane.ScrollPaneStyle();
        scrollPaneStyle.background = new TextureRegionDrawable(getColorTextureRegion(Color.GRAY));
        scrollPaneStyle.background.setMinWidth(50); // temporary size
        scrollPaneStyle.background.setMinHeight(50);
        com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle listStyle = new com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle();
        listStyle.selection = new TextureRegionDrawable(getColorTextureRegion(Color.GRAY));
        listStyle.font = font;
        SelectBox.SelectBoxStyle selectBoxStyle = new SelectBox.SelectBoxStyle();
        selectBoxStyle.font = font;
        selectBoxStyle.scrollStyle = scrollPaneStyle;
        selectBoxStyle.listStyle = listStyle;
        selectBoxStyle.background = new TextureRegionDrawable(getColorTextureRegion(Color.GRAY));
        selectBoxStyle.background.setMinWidth(50); // temporary size
        selectBoxStyle.background.setMinHeight(50);
        final SelectBox<GameRulesDefinition> ruleSelectBox = new SelectBox<GameRulesDefinition>(selectBoxStyle);
        ruleSelectBox.setItems(GameRulesDefinition.values());
        ruleSelectBox.setSelected(getRulesDefinition());
        ruleSelectBox.addCaptureListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                startNewGame(ruleSelectBox.getSelected());
            }
        });
        getStage().addActor(ruleSelectBox);

        float selectBoxWidth = getStage().getWidth() * SELECTBOX_WIDTH_RATIO;
        float selectBoxHeight = getStage().getHeight() * SELECTBOX_HEIGHT_RATIO;
        ruleSelectBox.setBounds((getStage().getWidth() / 2) - (selectBoxWidth / 2), getStage().getHeight() * SELECTBOX_X_HEIGHT_RATIO, selectBoxWidth, selectBoxHeight);
    }

    private void displayRule() {
        TablexiaLabel label = new TablexiaLabel(getData().getRuleMessageText(this), new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_16, Color.WHITE));
        label.setWrap(true);

        float labelWidth = getStage().getWidth() * RULE_WIDTH_RATIO;
        float labelHeight = getStage().getHeight() * RULE_HEIGHT_RATIO;

        label.setBounds((getStage().getWidth() * RULE_X_RATIO) - (labelWidth / 2), getStage().getHeight() * RULE_Y_RATIO, labelWidth, labelHeight);
        label.setAlignment(Align.center);
        getStage().addActor(label);
    }

    private void displayAllCreatures(List<CreatureRoot> creatures, Stage stage) {
		if (creatures != null && creatures.size() > 0) {				
			float creatureScale 			= 0.4f;

			float creatureWidth  			= RobberyGame.CREATURES_WIDTH * creatureScale;
			float creatureHeight 			= RobberyGame.CREATURES_HEIGHT * creatureScale;
			
			float borderX 					= getStage().getWidth() / 40;
			float borderY 					= getStage().getHeight() / 5;
			
			int creaturesInLine 			= (int)Math.ceil(Double.valueOf(creatures.size()) / 2);
			float xStep						= (getStage().getWidth() - (2 * borderX) - (creatureWidth / 2)) / creaturesInLine;
			
			float positionX 				= borderX;
			float positionY 				= getStage().getHeight() - (creatureHeight) - borderY;
			
			for (int i = 0; i < creatures.size(); i++) {
				CreatureRoot creatureRoot = creatures.get(i);
				creatureRoot.setScale(creatureScale);
				creatureRoot.setPosition(positionX, positionY);
				int groupNumber = creatureRoot.getGroupNumber();
				if (groupNumber >= 0) {
					creatureRoot.highliteWithColor(this, DebugGroupColors.values()[groupNumber].getColor());
				}

				positionX = positionX + xStep;
				if ((i + 1) % creaturesInLine == 0) {
					positionX = borderX;
					positionY = borderY;
				}

				stage.addActor(creatureRoot);
			}
		} else {
			Log.info(getClass(), "No creatures to display!");
		}
    }

}
