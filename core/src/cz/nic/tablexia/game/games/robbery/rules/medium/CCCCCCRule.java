/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules.medium;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.headgear.HeadgearAttribute;
import cz.nic.tablexia.game.games.robbery.rules.GameRule;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * 
 * @author Matyáš Latner
 */
public class CCCCCCRule extends GameRule {
	
	private static final int 	 	GROUP_SIZE	= 1;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;

    public CCCCCCRule(Random random) {
        super(random, GROUP_SIZE);
    }
    
    @Override
    public GameRulesDefinition getGameRuleDefinition() {
    	return GameRulesDefinition.CCCCCC;
    }

    @Override
    public String[] prepareRuleMessageConstituentParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        AttributeDescription t0Description0 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0);
        AttributeDescription t0Description1 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(1);
        AttributeDescription t0Description2 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(2);
        return new String[] {
                t0Description0.getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
                t0Description0.getAttributeConstituent().getConstituent2(abstractTablexiaScreen),
                t0Description1.getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
                t0Description1.getAttributeConstituent().getConstituent2(abstractTablexiaScreen),
                t0Description2.getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
                t0Description2.getAttributeConstituent().getConstituent2(abstractTablexiaScreen)
        };
    }

    @Override
    public String[] prepareRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        AttributeDescription t0Description0 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0);
        AttributeDescription t0Description1 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(1);
        AttributeDescription t0Description2 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(2);
		return new String[] {
        		getAttributeColorName(abstractTablexiaScreen, t0Description0),
        		getAttributeName(abstractTablexiaScreen, t0Description0, false),
                getAttributeColorName(abstractTablexiaScreen, t0Description1),
                getAttributeName(abstractTablexiaScreen, t0Description1, false),
                getAttributeColorName(abstractTablexiaScreen, t0Description2),
                getAttributeName(abstractTablexiaScreen, t0Description2, false)
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	CreatureDescriptor creatureDescriptorToForce = new CreatureDescriptor();
    	creatureDescriptorToForce.addDescription(new AttributeDescription(null, null, HeadgearAttribute.class));
    	addGlobalCreatureDescriptor(T0_OFFSET, getRandomCreatureDescriptionWithNAttributes(CreatureFactory.getInstance().generateCreature(creatureDescriptorToForce, BAN_ATTRIBUTES_SET_FOR_GENERATING, getRandom()).getCreatureDescrition(), 3));
    }

    @Override
    public List<CreatureRoot> prepareCreatureDescriptionsA(int numberOfCreatures) {

        List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        
        CreatureDescriptor t0CreatureDescription = getGlobalCreatureDescriptor(T0_OFFSET);
        CreatureDescriptor creatureDescriptorToBan0 = new CreatureDescriptor();
		AttributeDescription t0AttributeDescription0 = t0CreatureDescription.getDescriptions().get(0);
		creatureDescriptorToBan0.addDescription(t0AttributeDescription0);
        CreatureDescriptor creatureDescriptorToBan1 = new CreatureDescriptor();
        AttributeDescription t0AttributeDescription1 = t0CreatureDescription.getDescriptions().get(1);
		creatureDescriptorToBan1.addDescription(t0AttributeDescription1);
        CreatureDescriptor creatureDescriptorToBan2 = new CreatureDescriptor();
        AttributeDescription t0AttributeDescription2 = t0CreatureDescription.getDescriptions().get(2);
		creatureDescriptorToBan2.addDescription(t0AttributeDescription2);
        
        for (int i = 0; i < numberOfCreatures; i++) {
            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
            if (creatureDescriptor != null) { // add special creature
            	creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, null, getRandom()));
            } else {
            	CreatureDescriptor creatureDescriptorToBan = null;
            	CreatureDescriptor baitCreatureDescriptor = new CreatureDescriptor();
            	switch(getRandom().nextInt(3)) {
            		case 0:
            			creatureDescriptorToBan = creatureDescriptorToBan0;
            			baitCreatureDescriptor.addDescription(t0AttributeDescription1);
            			baitCreatureDescriptor.addDescription(t0AttributeDescription2);
            			break;
            		case 1:
            			creatureDescriptorToBan = creatureDescriptorToBan1;
            			baitCreatureDescriptor.addDescription(t0AttributeDescription0);
            			baitCreatureDescriptor.addDescription(t0AttributeDescription2);
            			break;
            		case 2:
            			creatureDescriptorToBan = creatureDescriptorToBan2;
            			baitCreatureDescriptor.addDescription(t0AttributeDescription0);
            			baitCreatureDescriptor.addDescription(t0AttributeDescription1);
            			break;
            	}
            	newBaitCreatureDescription(baitCreatureDescriptor);
            	creatures.add(CreatureFactory.getInstance().generateCreature(getBaitCreatureDescriptionRandomly(), creatureDescriptorToBan, getRandom()));
            }
        }

        return creatures;
    }

}
