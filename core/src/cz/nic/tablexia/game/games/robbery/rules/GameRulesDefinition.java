/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules;

import com.badlogic.gdx.utils.reflect.ClassReflection;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.robbery.rules.easy.BC_1_BCRule;
import cz.nic.tablexia.game.games.robbery.rules.easy.Bb_0_BbRule;
import cz.nic.tablexia.game.games.robbery.rules.easy.Bt_0_BtRule;
import cz.nic.tablexia.game.games.robbery.rules.easy.CACARule;
import cz.nic.tablexia.game.games.robbery.rules.easy.CACCRule;
import cz.nic.tablexia.game.games.robbery.rules.easy.CA_0_CC_0_TRule;
import cz.nic.tablexia.game.games.robbery.rules.easy.CCCCRule;
import cz.nic.tablexia.game.games.robbery.rules.easy.CC_0_CC_0_TRule;
import cz.nic.tablexia.game.games.robbery.rules.easy.CC_0_notCCRule;
import cz.nic.tablexia.game.games.robbery.rules.hard.Bb_1_BbRule;
import cz.nic.tablexia.game.games.robbery.rules.hard.Bt_1_BtRule;
import cz.nic.tablexia.game.games.robbery.rules.hard.CCCCnotCCRule;
import cz.nic.tablexia.game.games.robbery.rules.hard.CC_0_CC_0_CCRule;
import cz.nic.tablexia.game.games.robbery.rules.hard.CC_0_notCA_0_TRule;
import cz.nic.tablexia.game.games.robbery.rules.hard.CC_0_notCC_0_TRule;
import cz.nic.tablexia.game.games.robbery.rules.hard.CC_2_CCRule;
import cz.nic.tablexia.game.games.robbery.rules.medium.BC_2_BCRule;
import cz.nic.tablexia.game.games.robbery.rules.medium.CA_1_CCRule;
import cz.nic.tablexia.game.games.robbery.rules.medium.CCCCCCRule;
import cz.nic.tablexia.game.games.robbery.rules.medium.CC_1_CCRule;
import cz.nic.tablexia.game.games.robbery.rules.medium.CC_1_notCARule;
import cz.nic.tablexia.game.games.robbery.rules.medium.CC_1_notCCRule;
import cz.nic.tablexia.util.Log;


/**
 * Game rules definition enums
 * 
 * @author Matyáš Latner
 */
public enum GameRulesDefinition {

	/*
	 *	Generované atributy:
	 *		A - náhodný atribut (generovaný pro každou postavu zlášť)
	 *		B - náhodný atribut stejný u skupiny (generuje se zvlášť pro každou skupinu postav, která obsahuje zloděje a postavy, které identifikují zloděje --> v rámci této skupiny je atribut stejný)
	 *		C - stejný atribut (generovaný jen na začátku hry a pro všechny postavy v rámci jedné hry je stejný --> vygenerovaný atribut je součástí textu pravidla)
     *
	 *	Konstantní atributy pro komponenty:
	 *		t  - skupina komponent TOP
	 *		b  - skupina komponent BOTTOM
     *
     *
     *
     *
	 *	Gramatika pro sestavování pravidel:
	 *	
	 *	    s  --> w
	 *	    w  --> gx
	 *	    x  --> e | w | y
	 *		y  --> _n_w | _n_T
	 *	    n  --> 0, 1, 2, ...
	 *	    g  --> CA | notCA
	 *	    C  --> A | B | C          (barva komponenty)
	 *	    A  --> A | B | C | t | b  (komponenta)
     *
	 *
	 *
	 *
	 *	Ukázka pravidla: "1.1 Pokud po člověku, který má na sobě něco zeleného, přichází člověk s hnědými vlasy, tak za ním v pořadí je zloděj."
	 *
	 *
	 *		 barva atributu
	 *	    	 \
	 *	      	  \   typ komponenty
	 *	       	   \  / 
	 *		        CA_0_CC_0_T <--- poslední postava (zloděj je vždy poslední postava)
	 *	            /   \ \
	 *	           /     \ druhá postava
	 *	          /       \
	 *	         /      počet náhodných postav mezi první a druhou postavou
	 *	        /
	 *	     první postava
	 *	
	 *	
	 *	     C -> stejná barva celou hru ("má na sobě něco zeleného") --> barva se generuje jednou na začátku hry
	 *	      \
     *         \    A -> náhodně vygenerovaná komponenta zlášť pro každou postavu ("má na sobě něco zeleného") --> každá postava může mít jinou komponentu
     *          \  / 
     *           CA_0_CC_0_T   <--- T -> zloděj namá určeno jak bude vypadat, jen pořadí ("tak za ním v pořadí je zloděj")
     *               /  \
     *              /    C -> stejná komponenta po celou hru ("přichází člověk s hnědými vlasy") --> komponenta se generuje jednou na začátku hry
     *             /
     *            C -> stejná barva celou hru ("přichází člověk s hnědými vlasy") --> barva se generuje jednou na začátku hry
	 */
    CCCC(0, GameDifficulty.EASY, CCCCRule.class, "game_robbery_rule_cccc"),
    CACC(1, GameDifficulty.EASY, CACCRule.class, "game_robbery_rule_cacc"),
    CACA(2, GameDifficulty.EASY, CACARule.class, "game_robbery_rule_caca"),
    Bt_0_Bt(3, GameDifficulty.EASY, Bt_0_BtRule.class, "game_robbery_rule_bx0bx"),
    Bb_0_Bb(4, GameDifficulty.EASY, Bb_0_BbRule.class, "game_robbery_rule_bx0bx"),
    CCCCCC(5, GameDifficulty.EASY, CCCCCCRule.class, "game_robbery_rule_cccccc"),
    CCCCnotCC(6, GameDifficulty.EASY, CCCCnotCCRule.class, "game_robbery_rule_ccccnotcc"),
    
    CC_0_CC_0_T(7, GameDifficulty.MEDIUM, CC_0_CC_0_TRule.class, "game_robbery_rule_cc0cc0t"),
    CA_0_CC_0_T(8, GameDifficulty.MEDIUM, CA_0_CC_0_TRule.class, "game_robbery_rule_ca0cc0t"),
    BC_1_BC(9, GameDifficulty.MEDIUM, BC_1_BCRule.class, "game_robbery_rule_bc1bc"),
    CC_0_notCC(10, GameDifficulty.MEDIUM, CC_0_notCCRule.class, "game_robbery_rule_cc0notcc"),
    CA_1_CC(11, GameDifficulty.MEDIUM, CA_1_CCRule.class, "game_robbery_rule_ca1cc"),
    CC_1_CC(12, GameDifficulty.MEDIUM, CC_1_CCRule.class, "game_robbery_rule_cc1cc"),
    CC_1_notCC(13, GameDifficulty.MEDIUM, CC_1_notCCRule.class, "game_robbery_rule_cc1notcc"),

    Bt_1_Bt(14, GameDifficulty.HARD, Bt_1_BtRule.class, "game_robbery_rule_bx1bx"),
    Bb_1_Bb(15, GameDifficulty.HARD, Bb_1_BbRule.class, "game_robbery_rule_bx1bx"),
    BC_2_BC(16, GameDifficulty.HARD, BC_2_BCRule.class, "game_robbery_rule_bc2bc"),
    CC_1_notCA(17, GameDifficulty.HARD, CC_1_notCARule.class, "game_robbery_rule_cc1notca"),
    CC_0_notCC_0_T(18, GameDifficulty.HARD, CC_0_notCC_0_TRule.class, "game_robbery_rule_cc0notcc0t"),
    CC_0_notCA_0_T(19, GameDifficulty.HARD, CC_0_notCA_0_TRule.class, "game_robbery_rule_cc0notca0t"),
    CC_2_CC(20, GameDifficulty.HARD, CC_2_CCRule.class, "game_robbery_rule_cc2cc"),
    CC_0_CC_0_CC(21, GameDifficulty.HARD, CC_0_CC_0_CCRule.class, "game_robbery_rule_cc0cc0cc"),

    CC_1_notCA_BONUS(22, GameDifficulty.BONUS, CC_1_notCARule.class, "game_robbery_rule_cc1notca"),
    CC_0_notCC_0_T_BONUS(23, GameDifficulty.BONUS, CC_0_notCC_0_TRule.class, "game_robbery_rule_cc0notcc0t"),
    BC_2_BC_BONUS(24, GameDifficulty.BONUS, BC_2_BCRule.class, "game_robbery_rule_bc2bc"),
    CC_0_notCA_0_T_BONUS(25, GameDifficulty.BONUS, CC_0_notCA_0_TRule.class, "game_robbery_rule_cc0notca0t"),
    CC_2_CC_BONUS(26, GameDifficulty.BONUS, CC_2_CCRule.class, "game_robbery_rule_cc2cc"),
    Bb_1_Bb_BONUS(27, GameDifficulty.BONUS, Bb_1_BbRule.class, "game_robbery_rule_bx1bx"),
    CC_0_CC_0_CC_BONUS(28, GameDifficulty.BONUS, CC_0_CC_0_CCRule.class, "game_robbery_rule_cc0cc0cc");


    private Class<? extends GameRule> gameRuleClass;
    private String                    gameRuleStringName;
    private GameDifficulty            difficulty;
    private int                       id;

    GameRulesDefinition(int id, GameDifficulty difficulty, Class<? extends GameRule> gameRuleClass, String gameRuleStringName) {
        this.id = id;
        this.difficulty = difficulty;
        this.gameRuleClass = gameRuleClass;
        this.gameRuleStringName = gameRuleStringName;
    }

    public int getId() {
        return id;
    }
    /**
     * Returns difficulty for rule
     * 
     * @return rule difficulty
     */
    public GameDifficulty getDifficulty() {
        return difficulty;
    }

    /**
     * Returns game rule class
     * 
     * @return rule class
     */
    public Class<? extends GameRule> getGameRuleClass() {
        return gameRuleClass;
    }
    
    public String getGameRuleStringName() {
		return gameRuleStringName;
	}

    /**
     * Returns new instance of game rule class
     * 
     * @return new instance of game rule class
     */
    public GameRule getGameRuleInstance(Random random) {
        try {
            // TODO: 9.6.17 get rid of initial count of creatures and thieves 
            return (GameRule)ClassReflection.getConstructor(getGameRuleClass(), Random.class).newInstance(random);
        } catch (Exception e) {
        	Log.err(getClass(), "Cannot create instance of game rule: " + getGameRuleClass(), e);
        }
        return null;
    }

    /**
     * Returns random game rule for difficulty in parameter
     * 
     * @return random game rule for difficulty
     */
    public static GameRulesDefinition getRandomGameRuleForDifficulty(GameDifficulty difficulty, Random random) {
        List<GameRulesDefinition> gameRules = new ArrayList<GameRulesDefinition>();
        for (GameRulesDefinition gameRuleDefinition : GameRulesDefinition.values()) {
            if (gameRuleDefinition.getDifficulty() == difficulty) {
                gameRules.add(gameRuleDefinition);
            }
        }
        if (gameRules.size() > 0) {
            return gameRules.get(random.nextInt(gameRules.size()));
        }
        return null;
    }

    public static GameRulesDefinition getFirstGameRuleForDifficulty(GameDifficulty difficulty) {
        for (GameRulesDefinition gameRulesDefinition : GameRulesDefinition.values()) {
            if (gameRulesDefinition.getDifficulty().equals(difficulty)) return gameRulesDefinition;
        }

        return null;
    }

    /**
     * Cycle through rules for certain difficulty
     * @param difficulty game difficulty for current game
     * @param lastPlayedRule id of last played rule
     * @return next rule for that difficulty
     */
    public static GameRulesDefinition getNextGameRuleForDifficulty(GameDifficulty difficulty, String lastPlayedRule) {
        if (lastPlayedRule == null) return GameRulesDefinition.getFirstGameRuleForDifficulty(difficulty);
        
        int lastPlayedRuleId = Integer.parseInt(lastPlayedRule);
        if (lastPlayedRuleId + 1 >= GameRulesDefinition.values().length ||
                !GameRulesDefinition.values()[lastPlayedRuleId + 1].getDifficulty().equals(difficulty)) {
            return GameRulesDefinition.getFirstGameRuleForDifficulty(difficulty);
        }

        return GameRulesDefinition.values()[lastPlayedRuleId + 1];
    }


}
