/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.robbery;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.common.ui.health_bar.HealthBar;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.robbery.assets.RobberyAssets;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot;
import cz.nic.tablexia.game.games.robbery.rules.RobberyDifficultyDefinition;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class GameScreen extends AbstractRobberyScreen {

	private enum InfoItem {
		
		ARRESTED	(RobberyAssets.INFOITEM_ARRESTED,	RobberyAssets.INFOITEM_ARRESTED_TITLE, 	COLOR_OK, 	RobberyAssets.SOUND_CHAINS),
		INNOCENCE	(RobberyAssets.INFOITEM_INNOCENCE, 	RobberyAssets.INFOITEM_INNOCENCE_TITLE,	COLOR_KO, 	RobberyAssets.SOUND_ERROR),
		ALARM		(RobberyAssets.INFOITEM_ALARM, 		RobberyAssets.INFOITEM_ALARM_TITLE,		null, 		RobberyAssets.SOUND_ALARM);
		
		private final 	String 				textureName;
		private final	String 				textureTitleName;
		private 		String 				soundName;
		private 		Color 				color;
		private 		Image               infoBanner;
        private         VerticalGroup       infoItemGroup;
		private 		Image 				infoItem;
        private 		Image 				infoItemTitle;

		InfoItem(String textureName, String textureTitleName, Color color, String soundName) {
			this.textureName = textureName;
			this.textureTitleName = textureTitleName;
			this.soundName = soundName;
			this.color = color;
		}
		
		private void init(AbstractRobberyScreen abstractRobberyScreen) {
            infoItem = new Image(abstractRobberyScreen.getScreenTextureRegion(textureName));
            infoItemTitle = new Image(abstractRobberyScreen.getScreenTextureRegion(textureTitleName));
            infoItemGroup = new VerticalGroup();
            infoItemGroup.addActor(infoItem);
            infoItemGroup.addActor(infoItemTitle);
            if (color != null) {
                infoBanner = new Image(abstractRobberyScreen.getColorTextureRegion(color));
            }
		}
		
		public void show(AbstractRobberyScreen abstractRobberyScreen) {
			for (InfoItem container : InfoItem.values()) {
				container.infoItemGroup.setVisible(false);
				if (container.infoBanner != null) {
					container.infoBanner.setVisible(false);
				}
			}
			if (infoBanner != null) {
				infoBanner.setVisible(true);
			}
			infoItemGroup.addAction(alpha(0));
			infoItemGroup.addAction(scaleTo(INFOITEM_INITIAL_SCALE, INFOITEM_INITIAL_SCALE));
			infoItemGroup.setVisible(true);
			infoItemGroup.addAction(parallel(fadeIn(INFOITEM_SHOW_DURATION, INFOITEM_SHOW_ALPHA_INTERPOLATION),
                    scaleTo(INFOITEM_FINAL_SCALE, INFOITEM_FINAL_SCALE, INFOITEM_SHOW_DURATION, INFOITEM_SHOW_SCALE_INTERPOLATION)));
            abstractRobberyScreen.getSound(soundName).play();
		}
		
		public void hide() {
			if (infoBanner != null && infoBanner.isVisible()) {
				infoBanner.setVisible(false);
			}
			if (infoItemGroup.isVisible()) {
				infoItemGroup.addAction(sequence(parallel(fadeOut(INFOITEM_HIDE_DURATION, INFOITEM_HIDE_INTERPOLATION),
													 scaleTo(INFOITEM_INITIAL_SCALE, INFOITEM_INITIAL_SCALE, INFOITEM_HIDE_DURATION, INFOITEM_HIDE_INTERPOLATION)),
											         run(new Runnable() {

												@Override
												public void run() {
													infoItemGroup.setVisible(false);
												}
											})));
			}
		}
		
		public static void init(Stage stage, Group infoItemLayer, Group colorInfoLayer, SceneBackground sceneBackground, AbstractRobberyScreen abstractRobberyScreen) {
			for (InfoItem container : InfoItem.values()) {
				container.init(abstractRobberyScreen);
				container.infoItemGroup.setVisible(false);
				infoItemLayer.addActor(container.infoItemGroup);
				if (container.infoBanner != null) {
					container.infoBanner.setVisible(false);
					colorInfoLayer.addActor(container.infoBanner);
				}
			}
			calculateDimensions(TablexiaSettings.getSceneWidth(stage), TablexiaSettings.getSceneOuterHeight(stage), sceneBackground);
		}
		
		public static void calculateDimensions(float width, float height, SceneBackground sceneBackground) {
			float infoItemPositionX = width * sceneBackground.getInfoitemPositionXRatio();
			float infoItemPositionY = height * sceneBackground.getInfoitemPositionYRatio();
			
			for (InfoItem container : InfoItem.values()) {
                container.infoItemGroup.setSize(container.infoItem.getWidth(), container.infoItem.getHeight() + container.infoItemTitle.getHeight());

				container.infoItemGroup.setPosition(infoItemPositionX - (container.infoItemGroup.getWidth() / 2), infoItemPositionY);
				container.infoItemGroup.setOrigin(Align.center);
				
				if (container.infoBanner != null) {					
					container.infoBanner.setSize(width * COLORBANNER_WIDTH_RATIO, height);
					container.infoBanner.setPosition(width * COLORBANNER_X_POSITION_RATIO, 0);
				}
			}
		}
		
	}

    private enum SceneBackground {

        NEWSSTAND   (GameDifficulty.EASY,   RobberyAssets.SCREEN_BACKGROUND_NEWSSTAND,  RobberyAssets.SCREEN_BACKGROUND_NEWSSTAND_BOTTOM, 	0.563f,  -0.04f,	308f/1000,  1f/5, 	0.83f, 0.15f),
        JEWELLERY   (GameDifficulty.MEDIUM, RobberyAssets.SCREEN_BACKGROUND_JEWELLERY,  RobberyAssets.SCREEN_BACKGROUND_JEWELLERY_BOTTOM, 	0.564f,  -0.056f,	357f/1000,  1f/6, 	0.83f, 0.15f),
        BANK        (GameDifficulty.HARD,   RobberyAssets.SCREEN_BACKGROUND_BANK,       RobberyAssets.SCREEN_BACKGROUND_BANK_BOTTOM, 		0.565f,	-0.083f,	449f/1000,  1f/5, 	0.85f, 0.16f);

        private final GameDifficulty    gameDifficulty;
        private final String            sceneBackground;
        private final String            sceneBackgroundBottom;
		private final float				bottomPieceXposition;
		private final float				bottomPieceYposition;
		private final float             infoitemPositionXRatio;
		private final float             infoitemPositionYRatio;
		private final float				counterPositionXRatio;
		private final float				counterPositionYRatio;

		SceneBackground(GameDifficulty gameDifficulty, String sceneBackground, String sceneBackgroundBottom, float bottomPieceXposition, float bottomPieceYposition, float infoitemPositionXRatio, float infoitemPositionYRatio, float counterPositionXRatio, float counterPositionYRatio) {
            this.gameDifficulty = gameDifficulty;
            this.sceneBackground = sceneBackground;
            this.sceneBackgroundBottom = sceneBackgroundBottom;
			this.bottomPieceXposition = bottomPieceXposition;
			this.bottomPieceYposition = bottomPieceYposition;
			this.infoitemPositionXRatio = infoitemPositionXRatio;
			this.infoitemPositionYRatio = infoitemPositionYRatio;
			this.counterPositionXRatio = counterPositionXRatio;
			this.counterPositionYRatio = counterPositionYRatio;
		}

        public String getSceneBackground() {
            return sceneBackground;
        }

        public String getSceneBackgroundBottom() {
            return sceneBackgroundBottom;
        }

        public float getInfoitemPositionXRatio() {
            return infoitemPositionXRatio;
        }

        public float getInfoitemPositionYRatio() {
            return infoitemPositionYRatio;
        }

		public float getCounterPositionXRatio() {
			return counterPositionXRatio;
		}

		public float getCounterPositionYRatio() {
			return counterPositionYRatio;
		}


		public static SceneBackground getSceneBackgroundForDifficulty(GameDifficulty gameDifficulty) {
            for (SceneBackground background : SceneBackground.values()) {
                if (background.gameDifficulty == gameDifficulty) {
                    return background;
                }
            }
            return null;
        }

		public float getBottomPieceXposition() {
			return bottomPieceXposition;
		}

		public float getBottomPieceYposition() {
			return bottomPieceYposition;
		}
	}

    public enum StepSound {

        FEMALE_1(CreatureRoot.AttributeGender.FEMALE,   RobberyAssets.SOUND_STEPS_FEMALE_1_IN,  RobberyAssets.SOUND_STEPS_FEMALE_1_OUT),
        FEMALE_2(CreatureRoot.AttributeGender.FEMALE,   RobberyAssets.SOUND_STEPS_FEMALE_2_IN,  RobberyAssets.SOUND_STEPS_FEMALE_2_OUT),
        MALE_1  (CreatureRoot.AttributeGender.MALE,     RobberyAssets.SOUND_STEPS_MALE_1_IN,    RobberyAssets.SOUND_STEPS_MALE_1_OUT),
        MALE_2  (CreatureRoot.AttributeGender.MALE,     RobberyAssets.SOUND_STEPS_MALE_2_IN,    RobberyAssets.SOUND_STEPS_MALE_2_OUT),
        MALE_3  (CreatureRoot.AttributeGender.MALE,     RobberyAssets.SOUND_STEPS_MALE_3_IN,    RobberyAssets.SOUND_STEPS_MALE_3_OUT);

        private final CreatureRoot.AttributeGender gender;
        private final String soundIn;
        private final String soundOut;

        StepSound(CreatureRoot.AttributeGender gender, String soundIn, String soundOut) {
            this.gender = gender;
            this.soundIn = soundIn;
            this.soundOut = soundOut;
        }

        public String getSoundIn() {
            return soundIn;
        }

        public String getSoundOut() {
            return soundOut;
        }

        public static StepSound getRandomStepSoundForGender(Random random, CreatureRoot.AttributeGender gender) {
            List<StepSound> genderStepSounds = new ArrayList<StepSound>();
            for (StepSound stepSound: StepSound.values()) {
                if (stepSound.gender == gender) {
                    genderStepSounds.add(stepSound);
                }
            }
            return genderStepSounds.size() == 0 ? null : genderStepSounds.get(random.nextInt(genderStepSounds.size()));
        }
    }

	public static final int            MAXIMUM_MISTAKES_COUNT              = 3;


	public static final String			CREATURE							= "creature";
	public static final String			DOOR_IMAGE							= "image door";
	public static final String			LABEL_PERSON_COUNTER 				= "label person counter";
	public static final String			EVENT_CREATURE_MIDDLE				= "creature middle";
	
	private static final Color 			COLOR_OK	 						= Color.valueOf("39b54aff");
	private static final Color 			COLOR_KO	 						= Color.valueOf("c1272dff");
	private static final float 			COLORBANNER_X_POSITION_RATIO 		= 1f/2;
    private static final float 			COLORBANNER_WIDTH_RATIO 			= 1f/3;

	private static final Color 								PERSON_COUNTER_TEXT_COLOR 			= Color.WHITE;
	private static final ApplicationFontManager.FontType    PERSON_COUNTER_FONT 				= ApplicationFontManager.FontType.BOLD_20;

	private static final Interpolation 	INFOITEM_HIDE_INTERPOLATION 		= Interpolation.pow4In;
	private static final Interpolation 	INFOITEM_SHOW_ALPHA_INTERPOLATION 	= Interpolation.pow4Out;
	private static final Interpolation 	INFOITEM_SHOW_SCALE_INTERPOLATION 	= Interpolation.swingOut;
	private static final float 			INFOITEM_HIDE_DURATION 				= 0.1f;
	private static final float 			INFOITEM_SHOW_DURATION 				= 0.2f;
	private static final float 			INFOITEM_INITIAL_SCALE 				= 0.5f;
    private static final float          INFOITEM_FINAL_SCALE                = 0.7f;

	private static final int 			GAME_COMPLETE_DELAY 				= 1;

    private static final float          CREATURE_STRAT_MOVE_DURATION 		= 1.5f;
    private static final float          CREATURE_MOVE_DURATION_STEP  		= 0.013f;
    private static final float          CREATURE_FROM_SCALE          		= 0.1f;
    private static final float          CREATURE_TO_SCALE            		= 0.82f;
    private static final float          CREATURE_FINAL_SCALE         		= 0.75f;

    private final static float 			CREATURE_MIDDLE_DELAY_MODIFIER 		= 0.5f;
	private final static float 			CREATURE_FINISH_DELAY_MODIFIER 		= 0.25f;

	private static final float 			CREATURE_MIDDLE_POSITION_X_RATIO 	= 23f / 40;
	private static final float 			CREATURE_MIDDLE_POSITION_Y_RATIO 	= 1f / 50;
	private static final float 			CREATURE_START_POSITION_X_RATIO 	= 4f / 5;
	private static final float 			CREATURE_START_POSITION_Y_RATIO 	= 1f / 20;
	private static final float 			CREATURE_MIDDLE2_POSITION_X_RATIO 	= 2f / 5;
	
	private final static Interpolation 	CREATURE_START_INTERPOLATION 		= Interpolation.pow4Out;
	private final static Interpolation 	CREATURE_MIDDLE_INTERPOLATION 		= Interpolation.pow4In;
	private final static Interpolation 	CREATURE_FINISH_INTERPOLATION 		= Interpolation.linear;

	private static final int            STEP_SOUND_INITIAL_SPEED            = 1;
    private static final float          STEP_SOUND_SPEED_ADD_RATIO          = 1f/2;
    private static final int            STEP_SOUND_IN_VOLUME                = 1;
    private static final float          STEP_SOUND_OUT_VOLUME               = 0.3f;
	
	private final Group colorInfoLayer 	= new Group();
	private final Group creatureLayer 	= new Group();
	private final Group infoItemLayer 	= new Group();

	private Image doorBackground;
	private Image sceneBackground;
	private Image sceneBackgroundBottom;

	private TablexiaLabel 	personCounter;
	private HealthBar 		healthBar;

	private float 	creatureStartPositionX;
	private float 	creatureStartPositionY;
	private float 	creatureMiddle1PositionX;
	private float 	creatureMiddle1PositionY;
	private float 	creatureMiddle2PositionX;
	private float 	creatureMiddle2PositionY;
	private float 	creatureFinishPositionX;
	private float 	creatureFinishPositionY;

	private boolean running;
	private boolean creatureNumberSet;
	private RobberyGame robberyGame;

    public GameScreen(RobberyGame robberyGame) {
        super(robberyGame);
        this.robberyGame = robberyGame;
    }

//////////////////////////// SCREEN LIFECYCLE

	@Override
	public void screenLoaded(Map<String, String> screenState) {
		running = false;

		doorBackground = new TablexiaNoBlendingImage(getScreenTextureRegion(RobberyAssets.SCREEN_DOOR_BACKGROUND));
		doorBackground.setName(DOOR_IMAGE);

        SceneBackground sceneBackgroundDefinition = SceneBackground.getSceneBackgroundForDifficulty(
        		RobberyDifficultyDefinition.getRobberyDifficultyDefinitionForGameDifficulty(getGameDifficulty()).getReusesDifficulty());
        sceneBackground = new Image(getScreenTextureRegion(sceneBackgroundDefinition.getSceneBackground()));
		sceneBackgroundBottom = new Image(getScreenTextureRegion(sceneBackgroundDefinition.getSceneBackgroundBottom()));

        float width = getSceneWidth();
        float height = getSceneOuterHeight();

		setActorToFullScene(sceneBackground);
		sceneBackground.setTouchable(Touchable.disabled);
		doorBackground.setPosition(getSceneLeftX(), getSceneInnerBottomY());

		sceneBackgroundBottom.setPosition(getSceneWidth()*sceneBackgroundDefinition.getBottomPieceXposition(),getSceneInnerHeight()*sceneBackgroundDefinition.getBottomPieceYposition());

		personCounter = new TablexiaLabel("", new TablexiaLabel.TablexiaLabelStyle(PERSON_COUNTER_FONT, PERSON_COUNTER_TEXT_COLOR));
		personCounter.setPosition(width * sceneBackgroundDefinition.getCounterPositionXRatio(), height * sceneBackgroundDefinition.getCounterPositionYRatio());
		personCounter.setName(LABEL_PERSON_COUNTER);

		initLifeBar();

		getStage().addActor(doorBackground);
		getStage().addActor(colorInfoLayer);
		getStage().addActor(sceneBackgroundBottom);
		getStage().addActor(creatureLayer);
		getStage().addActor(sceneBackground);
		getStage().addActor(infoItemLayer);
		getStage().addActor(personCounter);
		getStage().addActor(healthBar);
		
		InfoItem.init(getStage(), infoItemLayer, colorInfoLayer, sceneBackgroundDefinition, this);
		prepareCreaturePositions(getSceneWidth(), getSceneInnerHeight());
	}
	
	@Override
	protected void screenVisible(Map<String, String> screenState) {
		running = true;
		showNextCreature(getData().getCreatures());

		AbstractTablexiaScreen.triggerScenarioStepEvent(AbstractTablexiaGame.EVENT_GAME_READY);
	}
	
	@Override
	protected void screenDisposed() {
		running = false;
	}
	
	
//////////////////////////// ROBBERY GAME

	private void initLifeBar() {
		healthBar = new HealthBar(getGameGlobalTextureRegion(AbstractTablexiaGame.HEART_FULL),
				getGameGlobalTextureRegion(AbstractTablexiaGame.HEART_BROKEN),
				getColorTextureRegion(HealthBar.BACKGROUND_COLOR));

		healthBar.setOrigin(Align.center);
		healthBar.setHealthBarDefaultPosition(robberyGame);
	}

	private void setPersonCounter(int personCount) {
		personCounter.setText(String.valueOf(personCount));
	}
	
	private int getNumberOfMistakesInGame() {
		return getThievesEscaped() + getInnocencePersons();
	}
	
	private float getCreatureMoveDurationForCreatureNumber(int creatureNumber) {
		return CREATURE_STRAT_MOVE_DURATION - (CREATURE_MOVE_DURATION_STEP * Math.min(creatureNumber, RobberyDifficultyDefinition.DEFAULT_CREATURES_COUNT));
	}

    private float getCreatureSoundSpeedForCreatureNumber(float creatureNumber, float creaturesCount) {
		return STEP_SOUND_INITIAL_SPEED + (Math.min(1, (creatureNumber / creaturesCount)) * STEP_SOUND_SPEED_ADD_RATIO);
	}

	private void prepareCreaturePositions(float viewportWidth, float viewportHeight) {
		creatureMiddle1PositionX	= viewportWidth * CREATURE_MIDDLE_POSITION_X_RATIO;
		creatureMiddle1PositionY 	= viewportHeight * CREATURE_MIDDLE_POSITION_Y_RATIO;
        creatureStartPositionX 		= viewportWidth * CREATURE_START_POSITION_X_RATIO;
        creatureStartPositionY 		= creatureMiddle1PositionY + (viewportHeight * CREATURE_START_POSITION_Y_RATIO);
        creatureMiddle2PositionX 	= viewportWidth * CREATURE_MIDDLE2_POSITION_X_RATIO;
        creatureMiddle2PositionY 	= creatureStartPositionY;
        creatureFinishPositionX 	= 0;
        creatureFinishPositionY 	= creatureStartPositionY;
    }

    private Action playSound(final Sound sound, final float speed, final float volume) {
        RunnableAction playSoundAction = run(new Runnable() {
            @Override
            public void run() {
                sound.play(volume, speed, 0);
            }
        });
        return playSoundAction;
    }

	private void incrementActualCreatureNumber() {
		if (!creatureNumberSet) {
			creatureNumberSet = true;
			setActualCreatureNumber(getActualCreatureNumber() + 1);
		}
	}

	private void showNextCreature(final List<CreatureRoot> creatures) {
		creatureNumberSet = false;
		if (isUnderLimit()) {
			int creatureNumberToShow = getActualCreatureNumber() + 1;
			setPersonCounter(creatureNumberToShow);
			printScreenInfo("Character", creatureNumberToShow + "/" + RobberyDifficultyDefinition.getMaxCreatures(getGameDifficulty().getDifficultyDefinition()));
        	final float duration = getCreatureMoveDurationForCreatureNumber(getActualCreatureNumber());
            final CreatureRoot creature = creatures.get(getActualCreatureNumber()%creatures.size());
            creature.setOrigin(Align.center);
            creature.setPosition(creatureStartPositionX, creatureStartPositionY);
            creature.setScale(CREATURE_FROM_SCALE);
			creature.setName(CREATURE+creatureNumberToShow);
            final InputListener inputListener = new InputListener() {
            	
            	@Override
            	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
					if(button == Input.Buttons.RIGHT) return false;
					creature.removeListener(this);
					// avoid creature show twice after game resume
					incrementActualCreatureNumber();
            		if (creature.isThief()) {
            			creature.setRevealed(true);
                        InfoItem.ARRESTED.show(GameScreen.this);
						setThievesCaught(getThievesCaught() + 1);
                    } else {
                    	InfoItem.INNOCENCE.show(GameScreen.this);
						setInnocencePersons(getInnocencePersons() + 1);
						healthBar.hide();
                    }
            		
            		return true;
            	}
            	
            };
            final StepSound stepSound = StepSound.getRandomStepSoundForGender(getRandom(), creature.getAttributeGender());
            float soundSpeed = getCreatureSoundSpeedForCreatureNumber(getActualCreatureNumber(), creatures.size());
            creature.addListener(inputListener);
            creature.addAction(sequence(parallel(playSound(getSound(stepSound.getSoundIn()), soundSpeed, STEP_SOUND_IN_VOLUME),
                                                 moveTo(creatureMiddle1PositionX, creatureMiddle1PositionY, duration, CREATURE_START_INTERPOLATION),
            									 scaleTo(CREATURE_TO_SCALE, CREATURE_TO_SCALE, duration, CREATURE_START_INTERPOLATION)),
            							parallel(playSound(getSound(stepSound.getSoundOut()), soundSpeed, STEP_SOUND_OUT_VOLUME),
                                                 moveTo(creatureMiddle2PositionX, creatureMiddle2PositionY, duration * CREATURE_MIDDLE_DELAY_MODIFIER, CREATURE_MIDDLE_INTERPOLATION),
												run(new Runnable() {
														//For test
														@Override
														public void run() {
															AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_CREATURE_MIDDLE+creatureNumberToShow);
														}
													}),
            									 scaleTo(CREATURE_FINAL_SCALE, CREATURE_FINAL_SCALE, duration * CREATURE_MIDDLE_DELAY_MODIFIER, CREATURE_MIDDLE_INTERPOLATION),
            									 run(new Runnable() {

 													@Override
 													public void run() {
 														InfoItem.ALARM.hide();
 													}
 												})),
            							parallel(moveTo(creatureFinishPositionX, creatureFinishPositionY, duration * CREATURE_FINISH_DELAY_MODIFIER, CREATURE_FINISH_INTERPOLATION),
            									 run(new Runnable() {

													@Override
													public void run() {
														//do not touch behind window
														creature.removeListener(inputListener);
														InfoItem.ARRESTED.hide();
														InfoItem.INNOCENCE.hide();
														if (creature.isThief() && !creature.isRevealed()) {
															InfoItem.ALARM.show(GameScreen.this);
															setThievesEscaped(getThievesEscaped() + 1);
															healthBar.hide();
									                    }
														if (running) {
                                                            incrementActualCreatureNumber();
															if (getActualCreatureNumber() % creatures.size() == 0) {
																getData().generateNewCreaturesPack(getRobberyGame(), RobberyGame.CREATURE_PACK_DEFAULT_SIZE);
                                                                showNextCreature(getData().getCreatures());
                                                            } else {
                                                                showNextCreature(creatures);
                                                            }
                                                        }
                                                    }
												})),
            							run(new Runnable() {

                                            @Override
                                            public void run() {
                                                creature.remove();
                                            }
                                        })));
            creatureLayer.addActor(creature);
        } else {
			addAction(sequence(new DelayAction(GAME_COMPLETE_DELAY), run(new Runnable() {
				@Override
				public void run() {
					gameComplete();
				}
			})));
		}
    }

    private boolean isUnderLimit(){
		return (RobberyDifficultyDefinition.getRobberyDifficultyDefinitionForGameDifficulty(getGameDifficulty()).isEndlessGeneration() ||
				(getActualCreatureNumber() < RobberyDifficultyDefinition.DEFAULT_CREATURES_COUNT)) &&
				(getNumberOfMistakesInGame() < MAXIMUM_MISTAKES_COUNT);
	}


	//Get for testing

	public float getCreatureMiddle2PositionX() {
		return creatureMiddle2PositionX;
	}

	public float getCreatureMiddle2PositionY() {
		return creatureMiddle2PositionY;
	}
}
