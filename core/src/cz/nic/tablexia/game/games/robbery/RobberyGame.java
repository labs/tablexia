/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.robbery;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.robbery.assets.RobberyAssets;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.rules.GameRule;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.game.games.robbery.rules.RobberyDifficultyDefinition;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;
import cz.nic.tablexia.shared.model.resolvers.RobberyScoreResolver;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

public class RobberyGame extends AbstractTablexiaGame<GameRule> {

	private enum ResultMapping {

		NO_STAR_TEXT	(GameResult.NO_STAR, 	RobberyAssets.VICTORYTEXT_NOSTAR, 		RobberyAssets.VICTORYSPEECH_NOSTAR),
		ONE_STAR_TEXT	(GameResult.ONE_STAR, 	RobberyAssets.VICTORYTEXT_ONESTAR, 		RobberyAssets.VICTORYSPEECH_ONESTAR),
		TWO_STAR_TEXT	(GameResult.TWO_STAR, 	RobberyAssets.VICTORYTEXT_TWOSTAR, 		RobberyAssets.VICTORYSPEECH_TWOSTAR),
		THREE_STAR_TEXT	(GameResult.THREE_STAR, RobberyAssets.VICTORYTEXT_THREESTARS,	RobberyAssets.VICTORYSPEECH_THREESTAR);

		private final GameResult gameResult;
		private final String textKey;
		private final String soundName;

		ResultMapping(GameResult gameResult, String textKey, String soundName) {
			this.gameResult = gameResult;
			this.textKey = textKey;
			this.soundName = soundName;
		}

		public String getTextKey() {
			return textKey;
		}

		public String getSoundName() {
			return soundName;
		}

		public static ResultMapping getResultTextMappingForGameResult(GameResult gameResult) {
			for (ResultMapping resultMapping : ResultMapping.values()) {
				if (resultMapping.gameResult.equals(gameResult)) {
					return resultMapping;
				}
			}
			return null;
		}
	}

	private static final String 		SUMMARY_TEXT_KEY 						= "game_robbery_victorytext_summary";
	private static final String 		SUMMARY_TEXT_KEY_BONUS 					= "game_robbery_victorytext_summary_bonus";
	private static final String 		SUMMARY_IDENTIFICATION_MALE_TEXT_KEY 	= "game_robbery_victorytext_summary_identification_male";
	private static final String 		SUMMARY_IDENTIFICATION_FEMALE_TEXT_KEY 	= "game_robbery_victorytext_summary_identification_female";

	private static final String 		SCORE_KEY_PERSON_COUNT 				= RobberyScoreResolver.SCORE_KEY_PERSON_COUNT;
	private static final String 		SCORE_KEY_THIEVES_COUNT 			= RobberyScoreResolver.SCORE_KEY_THIEVES_COUNT;
	private static final String 		SCORE_KEY_CAUGHT_THIEF 				= RobberyScoreResolver.SCORE_KEY_CAUGHT_THIEF;
	public  static final String 		SCORE_KEY_INNOCENCE_PERSON 			= RobberyScoreResolver.SCORE_KEY_INNOCENCE_PERSON;
	public  static final String 		SCORE_KEY_ESCAPED_THIEVES 			= RobberyScoreResolver.SCORE_KEY_ESCAPED_THIEVES;
	public  static final String         SCORE_KEY_PERSON_NUMBER 			= RobberyScoreResolver.SCORE_KEY_PERSON_NUMBER;
	public  static final String			SCORE_KEY_GAME_RULE					= RobberyScoreResolver.SCORE_KEY_GAME_RULE;

	public static final int				CREATURE_PACK_DEFAULT_SIZE			= 25;
	public static final int				THIEVES_PACK_DEFAULT_SIZE			= 4;

	private static final GameRulesDefinition TUTORIAL_RULE                      		= GameRulesDefinition.CCCC;
	private static final int                 TUTORIAL_GAMES_COMPLETED_THRESHOLD 	= 3;

	public static int			CREATURES_WIDTH 				= 193;
	public static int			CREATURES_HEIGHT 				= 457;

	private InputMultiplexer						inputMultiplexer;
	private AbstractTablexiaScreen<Void> 			actualScreen;
	private GameRulesDefinition 					rulesDefinition;
	private GameRulesDefinition 					forcedRulesDefinition;

	private boolean isRootScreenVisible;

	private boolean isComponentScreenVisible;
	private int     actualCreatureNumber;
	private int		thievesEscaped;
	private int		innocencePersons;
	private int		thievesCaught;
    private int     thievesLimit;
    private int     creaturesLimit;


//////////////////////////// SCREEN LOADERS

	@Override
	protected void prepareGameSoundAssetNames(List<String> soundsFileNames) {
		soundsFileNames.add(RobberyAssets.SOUND_ALARM);
		soundsFileNames.add(RobberyAssets.SOUND_ERROR);
		soundsFileNames.add(RobberyAssets.SOUND_CHAINS);
		soundsFileNames.add(RobberyAssets.SOUND_STEPS_FEMALE_1_IN);
		soundsFileNames.add(RobberyAssets.SOUND_STEPS_FEMALE_1_OUT);
		soundsFileNames.add(RobberyAssets.SOUND_STEPS_FEMALE_2_IN);
		soundsFileNames.add(RobberyAssets.SOUND_STEPS_FEMALE_2_OUT);
		soundsFileNames.add(RobberyAssets.SOUND_STEPS_MALE_1_IN);
		soundsFileNames.add(RobberyAssets.SOUND_STEPS_MALE_1_OUT);
		soundsFileNames.add(RobberyAssets.SOUND_STEPS_MALE_2_IN);
		soundsFileNames.add(RobberyAssets.SOUND_STEPS_MALE_2_OUT);
		soundsFileNames.add(RobberyAssets.SOUND_STEPS_MALE_3_IN);
		soundsFileNames.add(RobberyAssets.SOUND_STEPS_MALE_3_OUT);
	}

	@Override
	protected GameRule prepareGameData(Map<String, String> gameState) {
        RobberyDifficultyDefinition robberyDifficultyDefinition = RobberyDifficultyDefinition.getRobberyDifficultyDefinitionForGameDifficulty(getGameDifficulty());
        thievesLimit = RobberyDifficultyDefinition.DEFAULT_THIEVES_COUNT;
        creaturesLimit = RobberyDifficultyDefinition.DEFAULT_CREATURES_COUNT;
        if (getGameDifficulty().equals(GameDifficulty.EASY) &&
				GameDAO.getNumberOfCompleteGamesForGameDefinitionAndDifficulty(GameDefinition.ROBBERY, GameDifficulty.EASY, getSelectedUser()) < TUTORIAL_GAMES_COMPLETED_THRESHOLD){
			rulesDefinition = TUTORIAL_RULE;
		}else {
			String lastRule = GameDAO.getLastGameScoreValueForGameDifficultyAndKey(getGameDifficulty().getDifficultyNumber(), getGame().getGameNumber(), SCORE_KEY_GAME_RULE);
			rulesDefinition = forcedRulesDefinition != null ? forcedRulesDefinition : GameRulesDefinition.getNextGameRuleForDifficulty(robberyDifficultyDefinition.getGameDifficulty(), lastRule);
		}
		setGameScore(SCORE_KEY_GAME_RULE, rulesDefinition.getId());

		GameRule gameRule = rulesDefinition.getGameRuleInstance(getRandom());
		gameRule.generateCreatures(this, CREATURE_PACK_DEFAULT_SIZE, THIEVES_PACK_DEFAULT_SIZE);

		actualCreatureNumber 	= getGameScoreInteger(SCORE_KEY_PERSON_NUMBER, 0);
		thievesEscaped 			= getGameScoreInteger(SCORE_KEY_ESCAPED_THIEVES, 0);
		innocencePersons		= getGameScoreInteger(SCORE_KEY_INNOCENCE_PERSON, 0);
		thievesCaught			= getGameScoreInteger(SCORE_KEY_CAUGHT_THIEF, 0);

		printScreenInfo("Game Rule", gameRule.getGameRuleDefinition().name());

		return gameRule;
	}


//////////////////////////// INPUT PROCESSOR


	@Override
	public InputProcessor getInputProcessor() {
		return inputMultiplexer;
	}


//////////////////////////// SCREEN LIFECYCLE

	public RobberyGame() {
		isRootScreenVisible = false;
		isComponentScreenVisible = false;
		inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(getStage());
	}

	@Override
	protected void gameLoaded(Map<String, String> gameState) {
		setGameScore(SCORE_KEY_PERSON_COUNT, creaturesLimit);
		setGameScore(SCORE_KEY_THIEVES_COUNT, thievesLimit);
		if (actualCreatureNumber > 0) {
			showGameScreen(false);
		} else {
			showRuleScreen(false);
		}
	}

	@Override
	protected void gameVisible() {
		isRootScreenVisible = true;
		performComponentScreenVisible();
	}

	@Override
	public void screenResized(int width, int height) {
		if (actualScreen != null) {
			actualScreen.resize(width, height);
		}
	}

	@Override
	public void gameRender(float delta) {
		if (actualScreen != null) {
			actualScreen.render(delta);
		}
	}

	@Override
	protected void gameDisposed() {
		processLastScreen(actualScreen);
		CreatureFactory.getInstance().dispose();
	}


//////////////////////////// ABSTRACT TABLEXIA GAME

	@Override
	protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
		String identification = game.getUser().getGender() == GenderDefinition.MALE ? getText(SUMMARY_IDENTIFICATION_MALE_TEXT_KEY) : getText(SUMMARY_IDENTIFICATION_FEMALE_TEXT_KEY);
		return Arrays.asList(new SummaryMessage(
				SummaryImage.STATS,
				getVictoryDialogSummaryText(game, identification)
		));
	}

	@Override
	protected String getTextKeyForGameResult(GameResult gameResult) {
		return ResultMapping.getResultTextMappingForGameResult(gameResult).getTextKey();
	}

	@Override
	protected String getSoundNameForGameResult(GameResult gameResult) {
		return ResultMapping.getResultTextMappingForGameResult(gameResult).getSoundName();
	}

	private String getVictoryDialogSummaryText(Game game, String identification) {
		String summaryText;

		if (game.getGameDifficulty() == GameDifficulty.BONUS.getDifficultyNumber()) {
			summaryText = getFormattedText(
					SUMMARY_TEXT_KEY_BONUS,
					identification,
					((RobberyScoreResolver) GameDefinition.ROBBERY.getCurrentGameScoreResolver()).getFinalScore(game)
			);
		} else {
			summaryText = getFormattedText(
					SUMMARY_TEXT_KEY,
					identification,
					((RobberyScoreResolver) GameDefinition.ROBBERY.getCurrentGameScoreResolver()).getFinalScore(game),
					game.getGameScoreValue(SCORE_KEY_PERSON_COUNT)
			);
		}
		return summaryText;
	}


//////////////////////////// COMPONENT SCREENS

	private void showScreen(final AbstractTablexiaScreen newScreen, boolean transaction) {
		isComponentScreenVisible = false;
		if (transaction) {
			performHideTransaction(new Runnable() {

				@Override
				public void run() {
					processNewScreen(newScreen);
					performShowTransaction(new Runnable() {
						@Override
						public void run() {
							isComponentScreenVisible = true;
							performComponentScreenVisible();
						}
					});
				}
			});
		} else {
			processNewScreen(newScreen);
			isComponentScreenVisible = true;
			performComponentScreenVisible();
		}
	}

	private void performComponentScreenVisible() {
		if (isRootScreenVisible && isComponentScreenVisible) {
			isComponentScreenVisible = false;
			actualScreen.performScreenVisible();
		}
	}

	private void processNewScreen(AbstractTablexiaScreen<Void> newScreen) {
		AbstractTablexiaScreen<Void> lastScreen = actualScreen;
		actualScreen = newScreen;


		inputMultiplexer.addProcessor(newScreen.getInputProcessor());
		newScreen.show();
		if (lastScreen != null) {
			processLastScreen(lastScreen);
		}
	}

	private void processLastScreen(AbstractTablexiaScreen<Void> lastScreen) {
		inputMultiplexer.removeProcessor(lastScreen.getInputProcessor());
		lastScreen.hide();
		lastScreen.dispose();
	}

	public void showGameScreen(final boolean transaction) {
		Gdx.app.postRunnable(new Runnable() {

			@Override
			public void run() {
				showScreen(new GameScreen(RobberyGame.this), transaction);
			}
		});
	}

	private void showRuleScreen(final boolean transaction) {
		Gdx.app.postRunnable(new Runnable() {

			@Override
			public void run() {
				showScreen(new RuleScreen(RobberyGame.this), transaction);
			}
		});
	}


//////////////////////////// GAME SCORE

	public int getActualCreatureNumber() {
		return actualCreatureNumber;
	}

	public void setActualCreatureNumber(int actualCreatureNumber) {
		if(isGameFinished()) return;

		this.actualCreatureNumber = actualCreatureNumber;
		setGameScore(SCORE_KEY_PERSON_NUMBER, actualCreatureNumber);
	}

	public void setInnocencePersons(int innocencePersons) {
		if(isGameFinished()) return;

		this.innocencePersons = innocencePersons;
		setGameScore(SCORE_KEY_INNOCENCE_PERSON, innocencePersons);
	}

	public int getInnocencePersons() {
		return innocencePersons;
	}

	public void setThievesEscaped(int thievesEscaped) {
		if(isGameFinished()) return;

		this.thievesEscaped = thievesEscaped;
		setGameScore(SCORE_KEY_ESCAPED_THIEVES, thievesEscaped);
	}

	public int getThievesEscaped() {
		return thievesEscaped;
	}

	public void setThievesCaught(int thievesCaught) {
		this.thievesCaught = thievesCaught;
		setGameScore(SCORE_KEY_CAUGHT_THIEF, thievesCaught);
	}

	public int getThievesCaught() {
		return thievesCaught;
	}

	public GameRulesDefinition getRulesDefinition() {
		return rulesDefinition;
	}

	public void setForcedRulesDefinition(GameRulesDefinition forcedRulesDefinition) {
		this.forcedRulesDefinition = forcedRulesDefinition;
	}


	///////// REUSING ASSETS FROM OTHER DIFFICULTY

	@Override
	protected void prepareScreenAtlases(List<String> atlasesNames) {
		//Bonus difficulty uses hard difficulty assets
		if (getGameDifficulty().equals(GameDifficulty.BONUS)) atlasesNames.add(getGameDifficultyAtlasPath(GameDifficulty.HARD));
		super.prepareScreenAtlases(atlasesNames);
	}

	@Override
	public TextureRegion getScreenTextureRegion(String regionName) {
		GameDifficulty usedDifficulty = RobberyDifficultyDefinition.getRobberyDifficultyDefinitionForGameDifficulty(getGameDifficulty()).getReusesDifficulty();
		TextureRegion textureRegion = getTextureRegionForAtlas(getGameDifficultyAtlasPath(usedDifficulty), regionName, null, false);
		if (textureRegion == null) {
			textureRegion = super.getScreenTextureRegion(regionName, null);
		}
		return textureRegion;
	}


//////////////////////////////////////////// PRELOADER

	private static final String                             PRELOADER_ANIM_IMAGE            = "preloader_anim";
	private static final int                                PRELOADER_ANIM_FRAMES           = 11;
	private static final float                              PRELOADER_ANIM_FRAME_DURATION   = 0.5f;
	private static final String                             PRELOADER_TEXT_KEY             	= ApplicationTextManager.ApplicationTextsAssets.GAME_ROBBERY_PRELOADER_TEXT;

	private static final Scaling 		PRELOADER_IMAGE_SCALING 			= Scaling.fit;
	private static final int 			PRELOADER_TEXT_ALIGN 				= Align.left;
	private static final float 			PRELOADER_TEXT_PADDING 				= 10f;
	private static final float 			PRELOADER_LEFT_COLUMN_WIDTH_RATIO 	= 1f / 4;
	private static final float 			PRELOADER_RIGHT_COLUMN_RATIO 		= 5f / 8;
	private static final float 			PRELOADER_ROW_HEIGHT 				= 1f / 3;

	@Override
	public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
		AnimatedImage preloaderImage = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_IMAGE, PRELOADER_ANIM_FRAMES, PRELOADER_ANIM_FRAME_DURATION), false);
		preloaderImage.startAnimationLoop();
		String preloaderText = preloaderAssetsManager.getText(PRELOADER_TEXT_KEY);
		components.add(new TwoColumnContentDialogComponent(
				new AnimatedImageContentDialogComponent(preloaderImage, PRELOADER_IMAGE_SCALING),
				new TextContentDialogComponent(preloaderText, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
				PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
				PRELOADER_RIGHT_COLUMN_RATIO,
				PRELOADER_ROW_HEIGHT));
	}

	//////////////////////////////////////////// DEBUG - FORCE GAME END


	@Override
	protected void onForceGameEnd(GameResultDefinition gameResult) {
		this.actualCreatureNumber = RobberyDifficultyDefinition.getRobberyDifficultyDefinitionForGameDifficulty(getGameDifficulty()).getForcedEndCreaturesValue();
		this.thievesEscaped = RobberyDifficultyDefinition.getRobberyDifficultyDefinitionForGameDifficulty(getGameDifficulty()).getForcedEndThievsValue();
	}


	//Gets for testing

	public AbstractTablexiaScreen<Void> getActualScreen() {
		return actualScreen;
	}
}
