/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.creature.attribute.glasses;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.CreatureGenericType;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;

@CreatureGenericType(isGeneric = true)
public abstract class GlassesAttribute extends Attribute {

    private static final AttributeConstituent ATTRIBUTE_CONSTITUENT = AttributeConstituent.F_FORCED;

    private static final int POSITION_X = 0;
    private static final float POSITION_Y = 0.787746f;
    private static final int Z_INDEX    = 6;

    public static List<AttributeDescription> getTextures() {
        return new ArrayList<AttributeDescription>() {

            private static final long serialVersionUID = -1377013489500837025L;

            {
                addAll(MGlassesAttribute.getTextures());
                addAll(FGlassesAttribute.getTextures());
                add(null);
                add(null);
            }
        };
    }
    
    public static List<Class<? extends Attribute>> getAttributeClasses() {
        return new ArrayList<Class<? extends Attribute>>() {

            private static final long serialVersionUID = -1377013489500837025L;

            {
            	add(GlassesAttribute.class);
                add(MGlassesAttribute.class);
                add(FGlassesAttribute.class);
            }
        };
    }

    public GlassesAttribute(AttributeColor color, String textureName) {
        super(ATTRIBUTE_CONSTITUENT, color, POSITION_X, POSITION_Y, Z_INDEX, textureName);
    }

    @Override
    public Class<? extends Attribute> getGenericType() {
        return GlassesAttribute.class;
    }

}
