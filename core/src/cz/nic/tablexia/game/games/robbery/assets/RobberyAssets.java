/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.robbery.assets;


public final class RobberyAssets {
	
	private RobberyAssets() {}

	public static final String 	COMMON_PATH 					= "common/";
	public static final String 	SOUND_ALARM 					= COMMON_PATH + "alarm.mp3";
	public static final String 	SOUND_ERROR 					= COMMON_PATH + "error.mp3";
	public static final String 	SOUND_CHAINS 					= COMMON_PATH + "chains.mp3";

	public static final String 	VICTORYTEXT_NOSTAR 				= "game_robbery_victorytext_nostar";
	public static final String 	VICTORYTEXT_ONESTAR 			= "game_robbery_victorytext_onestar";
	public static final String 	VICTORYTEXT_TWOSTAR 			= "game_robbery_victorytext_twostar";
	public static final String 	VICTORYTEXT_THREESTARS 			= "game_robbery_victorytext_threestars";

	public static final String 	VICTORYSPEECH_NOSTAR 			= COMMON_PATH + "result_0.mp3";
	public static final String 	VICTORYSPEECH_ONESTAR 			= COMMON_PATH + "result_1.mp3";
	public static final String 	VICTORYSPEECH_TWOSTAR 			= COMMON_PATH + "result_2.mp3";
	public static final String 	VICTORYSPEECH_THREESTAR			= COMMON_PATH + "result_3.mp3";

	public static final String 	SOUND_STEPS_FEMALE_1_IN			= COMMON_PATH + "steps_female_in_1.mp3";
	public static final String 	SOUND_STEPS_FEMALE_1_OUT		= COMMON_PATH + "steps_female_out_1.mp3";
	public static final String 	SOUND_STEPS_FEMALE_2_IN			= COMMON_PATH + "steps_female_in_2.mp3";
	public static final String 	SOUND_STEPS_FEMALE_2_OUT		= COMMON_PATH + "steps_female_out_2.mp3";
	public static final String 	SOUND_STEPS_MALE_1_IN			= COMMON_PATH + "steps_male_in_1.mp3";
	public static final String 	SOUND_STEPS_MALE_1_OUT			= COMMON_PATH + "steps_male_out_1.mp3";
	public static final String 	SOUND_STEPS_MALE_2_IN			= COMMON_PATH + "steps_male_in_2.mp3";
	public static final String 	SOUND_STEPS_MALE_2_OUT			= COMMON_PATH + "steps_male_out_2.mp3";
	public static final String 	SOUND_STEPS_MALE_3_IN			= COMMON_PATH + "steps_male_in_3.mp3";
	public static final String 	SOUND_STEPS_MALE_3_OUT			= COMMON_PATH + "steps_male_out_3.mp3";
	
	public static final String 	SCREEN_PATH 					= "gamescreen/";
	
	public static final String 	SCREEN_RULEMESSAGE_PAPER_S1		= SCREEN_PATH + "game_robbery_rulemessage_paper";
	public static final String 	SCREEN_RULEMESSAGE_PAPER_TITLE	= SCREEN_PATH + "rulemessage_paper_title";
	
	public static final String 	SCREEN_BACKGROUND_BANK 				= SCREEN_PATH + "background_bank";
	public static final String 	SCREEN_BACKGROUND_BANK_BOTTOM		= SCREEN_PATH + "background_bank_bottom";
	public static final String 	SCREEN_BACKGROUND_NEWSSTAND 		= SCREEN_PATH + "background_newsstand";
	public static final String 	SCREEN_BACKGROUND_NEWSSTAND_BOTTOM	= SCREEN_PATH + "background_newsstand_bottom";
	public static final String 	SCREEN_BACKGROUND_JEWELLERY 		= SCREEN_PATH + "background_jewellery";
	public static final String 	SCREEN_BACKGROUND_JEWELLERY_BOTTOM	= SCREEN_PATH + "background_jewellery_bottom";
	public static final String 	SCREEN_DOOR_BACKGROUND 				= SCREEN_PATH + "door_background";
	
	public static final String 	INFOITEM_ARRESTED				= SCREEN_PATH + "arrested";
	public static final String 	INFOITEM_ARRESTED_TITLE			= SCREEN_PATH + "arrested_title";
	public static final String 	INFOITEM_INNOCENCE				= SCREEN_PATH + "innocence";
	public static final String 	INFOITEM_INNOCENCE_TITLE		= SCREEN_PATH + "innocence_title";
	public static final String 	INFOITEM_ALARM					= SCREEN_PATH + "thief";
	public static final String 	INFOITEM_ALARM_TITLE			= SCREEN_PATH + "thief_title";
	
	public static final String 	CREATURE_PATH 					= "character/";
	
	public static final String 	CREATURE_BACKGROUND_TEXTURE 	= CREATURE_PATH + "a_background_white";
	
	public static final String 	ATTRIBUTE_F_BEADS_BLUE 			= CREATURE_PATH + "f_beads_blue";
	public static final String 	ATTRIBUTE_F_BEADS_GREY 			= CREATURE_PATH + "f_beads_grey";
	public static final String 	ATTRIBUTE_F_BEADS_RED 			= CREATURE_PATH + "f_beads_red";
	
	public static final String 	ATTRIBUTE_F_SCARF_GREEN			= CREATURE_PATH + "f_scarf_green";
	public static final String 	ATTRIBUTE_F_SCARF_PURPLE		= CREATURE_PATH + "f_scarf_purple";
	public static final String 	ATTRIBUTE_F_SCARF_YELLOW		= CREATURE_PATH + "f_scarf_yellow";
	
	public static final String 	ATTRIBUTE_M_SCARF_BLUE			= CREATURE_PATH + "m_scarf_blue";
	public static final String 	ATTRIBUTE_M_SCARF_BROWN			= CREATURE_PATH + "m_scarf_brown";
	public static final String 	ATTRIBUTE_M_SCARF_YELLOW		= CREATURE_PATH + "m_scarf_yellow";
	
	public static final String 	ATTRIBUTE_M_TIE_GREEN			= CREATURE_PATH + "m_tie_green";
	public static final String 	ATTRIBUTE_M_TIE_GREY			= CREATURE_PATH + "m_tie_grey";
	public static final String 	ATTRIBUTE_M_TIE_RED				= CREATURE_PATH + "m_tie_red";
	
	public static final String 	ATTRIBUTE_F_PANTS_BLUE			= CREATURE_PATH + "f_pants_blue";
	public static final String 	ATTRIBUTE_F_PANTS_BROWN			= CREATURE_PATH + "f_pants_brown";
	public static final String 	ATTRIBUTE_F_PANTS_GREEN			= CREATURE_PATH + "f_pants_green";
	public static final String 	ATTRIBUTE_F_PANTS_GREY			= CREATURE_PATH + "f_pants_grey";
	public static final String 	ATTRIBUTE_F_PANTS_PURPLE		= CREATURE_PATH + "f_pants_purple";
	public static final String 	ATTRIBUTE_F_PANTS_RED			= CREATURE_PATH + "f_pants_red";
	
	public static final String 	ATTRIBUTE_F_SKIRT_BLUE			= CREATURE_PATH + "f_skirt_blue";
	public static final String 	ATTRIBUTE_F_SKIRT_GREEN			= CREATURE_PATH + "f_skirt_green";
	public static final String 	ATTRIBUTE_F_SKIRT_GREY			= CREATURE_PATH + "f_skirt_grey";
	public static final String 	ATTRIBUTE_F_SKIRT_PURPLE		= CREATURE_PATH + "f_skirt_purple";
	public static final String 	ATTRIBUTE_F_SKIRT_RED			= CREATURE_PATH + "f_skirt_red";
	public static final String 	ATTRIBUTE_F_SKIRT_YELLOW		= CREATURE_PATH + "f_skirt_yellow";

	public static final String 	ATTRIBUTE_M_PANTS_BLUE			= CREATURE_PATH + "m_pants_blue";
	public static final String 	ATTRIBUTE_M_PANTS_BROWN			= CREATURE_PATH + "m_pants_brown";
	public static final String 	ATTRIBUTE_M_PANTS_GREEN			= CREATURE_PATH + "m_pants_green";
	public static final String 	ATTRIBUTE_M_PANTS_GREY			= CREATURE_PATH + "m_pants_grey";
	public static final String 	ATTRIBUTE_M_PANTS_RED			= CREATURE_PATH + "m_pants_red";
	public static final String 	ATTRIBUTE_M_PANTS_YELLOW		= CREATURE_PATH + "m_pants_yellow";
	
	public static final String 	ATTRIBUTE_M_SHORT_BLUE			= CREATURE_PATH + "m_shorts_blue";
	public static final String 	ATTRIBUTE_M_SHORT_GREEN			= CREATURE_PATH + "m_shorts_green";
	public static final String 	ATTRIBUTE_M_SHORT_GREY			= CREATURE_PATH + "m_shorts_grey";
	public static final String 	ATTRIBUTE_M_SHORT_PURPLE		= CREATURE_PATH + "m_shorts_purple";
	public static final String 	ATTRIBUTE_M_SHORT_RED			= CREATURE_PATH + "m_shorts_red";
	public static final String 	ATTRIBUTE_M_SHORT_YELLOW		= CREATURE_PATH + "m_shorts_yellow";
	
	public static final String 	ATTRIBUTE_F_HAT_BLUE			= CREATURE_PATH + "f_hat_blue";
	public static final String 	ATTRIBUTE_F_HAT_GREEN			= CREATURE_PATH + "f_hat_green";
	public static final String 	ATTRIBUTE_F_HAT_GREY			= CREATURE_PATH + "f_hat_grey";
	public static final String 	ATTRIBUTE_F_HAT_PURPLE			= CREATURE_PATH + "f_hat_purple";
	public static final String 	ATTRIBUTE_F_HAT_RED				= CREATURE_PATH + "f_hat_red";
	public static final String 	ATTRIBUTE_F_HAT_YELLOW			= CREATURE_PATH + "f_hat_yellow";
	
	public static final String 	ATTRIBUTE_M_HAT_BROWN			= CREATURE_PATH + "m_hat_brown";
	public static final String 	ATTRIBUTE_M_HAT_GREEN			= CREATURE_PATH + "m_hat_green";
	public static final String 	ATTRIBUTE_M_HAT_GREY			= CREATURE_PATH + "m_hat_grey";
	public static final String 	ATTRIBUTE_M_HAT_PURPLE			= CREATURE_PATH + "m_hat_purple";
	public static final String 	ATTRIBUTE_M_HAT_RED				= CREATURE_PATH + "m_hat_red";
	public static final String 	ATTRIBUTE_M_HAT_YELLOW			= CREATURE_PATH + "m_hat_yellow";
	
	public static final String 	ATTRIBUTE_F_COAT_BROWN			= CREATURE_PATH + "f_coat_brown";
	public static final String 	ATTRIBUTE_F_COAT_GREEN			= CREATURE_PATH + "f_coat_green";
	public static final String 	ATTRIBUTE_F_COAT_GREY			= CREATURE_PATH + "f_coat_grey";
	
	public static final String 	ATTRIBUTE_F_SHIRT_BLUE			= CREATURE_PATH + "f_shirt_blue";
	public static final String 	ATTRIBUTE_F_SHIRT_GREEN			= CREATURE_PATH + "f_shirt_green";
	public static final String 	ATTRIBUTE_F_SHIRT_GREY			= CREATURE_PATH + "f_shirt_grey";
	public static final String 	ATTRIBUTE_F_SHIRT_PURPLE		= CREATURE_PATH + "f_shirt_purple";
	public static final String 	ATTRIBUTE_F_SHIRT_RED			= CREATURE_PATH + "f_shirt_red";
	public static final String 	ATTRIBUTE_F_SHIRT_YELLOW		= CREATURE_PATH + "f_shirt_yellow";
	
	public static final String 	ATTRIBUTE_F_SWEATER_BROWN		= CREATURE_PATH + "f_sweater_brown";
	public static final String 	ATTRIBUTE_F_SWEATER_GREEN		= CREATURE_PATH + "f_sweater_green";
	public static final String 	ATTRIBUTE_F_SWEATER_GREY		= CREATURE_PATH + "f_sweater_grey";
	public static final String 	ATTRIBUTE_F_SWEATER_PURPLE		= CREATURE_PATH + "f_sweater_purple";
	public static final String 	ATTRIBUTE_F_SWEATER_RED			= CREATURE_PATH + "f_sweater_red";
	public static final String 	ATTRIBUTE_F_SWEATER_YELLOW		= CREATURE_PATH + "f_sweater_yellow";
	
	public static final String 	ATTRIBUTE_F_VEST_BLUE			= CREATURE_PATH + "f_vest_blue";
	public static final String 	ATTRIBUTE_F_VEST_BROWN			= CREATURE_PATH + "f_vest_brown";
	public static final String 	ATTRIBUTE_F_VEST_GREEN			= CREATURE_PATH + "f_vest_green";
	public static final String 	ATTRIBUTE_F_VEST_GREY			= CREATURE_PATH + "f_vest_grey";
	public static final String 	ATTRIBUTE_F_VEST_PURPLE			= CREATURE_PATH + "f_vest_purple";
	public static final String 	ATTRIBUTE_F_VEST_YELLOW			= CREATURE_PATH + "f_vest_yellow";
	
	public static final String 	ATTRIBUTE_M_COAT_BLUE			= CREATURE_PATH + "m_coat_blue";
	public static final String 	ATTRIBUTE_M_COAT_BROWN			= CREATURE_PATH + "m_coat_brown";
	public static final String 	ATTRIBUTE_M_COAT_GREY			= CREATURE_PATH + "m_coat_grey";
	
	public static final String 	ATTRIBUTE_M_SHIRT_BLUE			= CREATURE_PATH + "m_shirt_blue";
	public static final String 	ATTRIBUTE_M_SHIRT_BROWN			= CREATURE_PATH + "m_shirt_brown";
	public static final String 	ATTRIBUTE_M_SHIRT_GREEN			= CREATURE_PATH + "m_shirt_green";
	public static final String 	ATTRIBUTE_M_SHIRT_GREY			= CREATURE_PATH + "m_shirt_grey";
	public static final String 	ATTRIBUTE_M_SHIRT_RED			= CREATURE_PATH + "m_shirt_red";
	public static final String 	ATTRIBUTE_M_SHIRT_YELLOW		= CREATURE_PATH + "m_shirt_yellow";
	
	public static final String 	ATTRIBUTE_M_SWEATER_BLUE		= CREATURE_PATH + "m_sweater_blue";
	public static final String 	ATTRIBUTE_M_SWEATER_BROWN		= CREATURE_PATH + "m_sweater_brown";
	public static final String 	ATTRIBUTE_M_SWEATER_GREEN		= CREATURE_PATH + "m_sweater_green";
	public static final String 	ATTRIBUTE_M_SWEATER_GREY		= CREATURE_PATH + "m_sweater_grey";
	public static final String 	ATTRIBUTE_M_SWEATER_RED			= CREATURE_PATH + "m_sweater_red";
	public static final String 	ATTRIBUTE_M_SWEATER_YELLOW		= CREATURE_PATH + "m_sweater_yellow";
	
	public static final String 	ATTRIBUTE_M_VEST_BLUE			= CREATURE_PATH + "m_vest_blue";
	public static final String 	ATTRIBUTE_M_VEST_GREEN			= CREATURE_PATH + "m_vest_green";
	public static final String 	ATTRIBUTE_M_VEST_GREY			= CREATURE_PATH + "m_vest_grey";
	public static final String 	ATTRIBUTE_M_VEST_PURPLE			= CREATURE_PATH + "m_vest_purple";
	public static final String 	ATTRIBUTE_M_VEST_RED			= CREATURE_PATH + "m_vest_red";
	public static final String 	ATTRIBUTE_M_VEST_YELLOW			= CREATURE_PATH + "m_vest_yellow";
	
	public static final String 	ATTRIBUTE_F_GLASSES				= CREATURE_PATH + "f_glasses";
	
	public static final String 	ATTRIBUTE_M_GLASSES				= CREATURE_PATH + "m_glasses";
	
	public static final String 	ATTRIBUTE_F_BRAIDHAIR_BLACK		= CREATURE_PATH + "f_braidhair_black";
	public static final String 	ATTRIBUTE_F_BRAIDHAIR_BROWN		= CREATURE_PATH + "f_braidhair_brown";
	public static final String 	ATTRIBUTE_F_BRAIDHAIR_YELLOW	= CREATURE_PATH + "f_braidhair_yellow";
	
	public static final String 	ATTRIBUTE_F_LONGHAIR_BLACK		= CREATURE_PATH + "f_longhair_black";
	public static final String 	ATTRIBUTE_F_LONGHAIR_BROWN		= CREATURE_PATH + "f_longhair_brown";
	public static final String 	ATTRIBUTE_F_LONGHAIR_YELLOW		= CREATURE_PATH + "f_longhair_yellow";
	
	public static final String 	ATTRIBUTE_M_HAIR_BLACK			= CREATURE_PATH + "m_hair_black";
	public static final String 	ATTRIBUTE_M_HAIR_BROWN			= CREATURE_PATH + "m_hair_brown";
	public static final String 	ATTRIBUTE_M_HAIR_YELLOW			= CREATURE_PATH + "m_hair_yellow";
	
	public static final String 	ATTRIBUTE_F_HEAD_1				= CREATURE_PATH + "f_head1";
	public static final String 	ATTRIBUTE_F_HEAD_2				= CREATURE_PATH + "f_head2";
	
	public static final String 	ATTRIBUTE_M_HEAD_1				= CREATURE_PATH + "m_head1";
	public static final String 	ATTRIBUTE_M_HEAD_2				= CREATURE_PATH + "m_head2";

	public static final String 	CONSTITUENT1_M 					= "constituent1_m";
	public static final String 	CONSTITUENT1_F 					= "constituent1_f";
	public static final String 	CONSTITUENT1_N 					= "constituent1_n";
	public static final String 	CONSTITUENT2_M 					= "constituent2_m";
	public static final String 	CONSTITUENT2_F 					= "constituent2_f";
	public static final String 	CONSTITUENT2_N 					= "constituent2_n";
	public static final String 	CONSTITUENT3_M 					= "constituent3_m";
	public static final String 	CONSTITUENT3_F 					= "constituent3_f";
	public static final String 	CONSTITUENT3_N 					= "constituent3_n";
}