/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.creature.attribute.head;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.CreatureGenericType;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;

@CreatureGenericType(isGeneric = true)
public abstract class HeadAttribute extends Attribute {

    private static final AttributeConstituent   ATTRIBUTE_CONSTITUENT = AttributeConstituent.M;

    private static final int                    POSITION_X = 0;
    private static final float                    POSITION_Y = 0.737418f;
    private static final int                    Z_INDEX    = 3;

    public static List<AttributeDescription> getTextures() {
        return new ArrayList<AttributeDescription>() {

            private static final long serialVersionUID = 618159614388924773L;

            {
                addAll(FHead1Attribute.getTextures());
                addAll(FHead2Attribute.getTextures());
                addAll(MHead1Attribute.getTextures());
                addAll(MHead2Attribute.getTextures());
            }
        };
    }
    
    public static List<Class<? extends Attribute>> getAttributeClasses() {
        return new ArrayList<Class<? extends Attribute>>() {

            private static final long serialVersionUID = -1377013489500837025L;

            {
            	add(HeadAttribute.class);
                add(FHead1Attribute.class);
                add(FHead2Attribute.class);
                add(MHead1Attribute.class);
                add(MHead2Attribute.class);
            }
        };
    }

    public HeadAttribute(AttributeColor color, String textureName) {
        super(ATTRIBUTE_CONSTITUENT, color, POSITION_X, POSITION_Y, Z_INDEX, textureName);
    }

    @Override
    public Class<? extends Attribute> getGenericType() {
        return HeadAttribute.class;
    }

}
