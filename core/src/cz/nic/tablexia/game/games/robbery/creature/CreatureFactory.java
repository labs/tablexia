/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.creature;

import com.badlogic.gdx.utils.Disposable;

import java.util.Random;

import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot.AttributeGender;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;

/**
 * Factory for creating creatures
 * 
 * @author Matyáš Latner
 */
public class CreatureFactory implements Disposable {
	
	private static final int GENERATOR_TRY_COUNT = 1000;
    private static CreatureFactory instance;

    public static CreatureFactory getInstance() {
        if (instance == null) {
            instance = new CreatureFactory();
        }
        return instance;
    }
    
    @Override
    public void dispose() {
    	instance = null;
    }

    /**
     * Returns random creature with specific forced description and banned description.
     * If description is <code>null</code> or field gender on description is <code>null</code>, then is generated creature with random gender.
     * All creature attributes which are not defined in description are generated.
     * 
     * @return randomly generated creature
     */
    public CreatureRoot generateCreature(Random random) {
        return generateCreature(null, null, random);
    }

    /**
     * Returns random creature with specific forced description and banned description.
     * If description is <code>null</code> or field gender on description is <code>null</code>, then is generated creature with random gender.
     * All creature attributes which are not defined in description are generated.
     * 
     * @param descriptionsToForce list of forced attributes, for <code>null</code> value is no attribute forced
     * @param descriptionToBan list of banned attributes, for <code>null</code> value is no attribute banned
     * @return randomly generated creature
     */
    public CreatureRoot generateCreature(CreatureDescriptor descriptionsToForce, CreatureDescriptor descriptionToBan, Random random) {
        return generateCreature(0, 0, descriptionsToForce, descriptionToBan, random);
    }

    /**
     * Returns random creature with specific forced description and banned description.
     * If description is <code>null</code> or field gender on description is <code>null</code>, then is generated creature with random gender.
     * All creature attributes which are not defined in description are generated.
     * 
     * @param pX creature position x
     * @param pY creature position y
     * @param descriptionsToForce list of forced attributes, for <code>null</code> value is no attribute forced
     * @param descriptionToBan list of banned attributes, for <code>null</code> value is no attribute banned
     * @return randomly generated creature
     */
    public CreatureRoot generateCreature(float pX, float pY, CreatureDescriptor descriptionsToForce, CreatureDescriptor descriptionToBan, Random random) {

        CreatureRoot creatureRoot = createCreatureAndAttributes(pX, pY, descriptionsToForce, descriptionToBan, random);
        creatureRoot.generateCreature(descriptionToBan, random);
        creatureRoot.setAsThief(descriptionsToForce != null ? descriptionsToForce.isThief() : false);
        creatureRoot.setGroupNumber(descriptionsToForce != null ? descriptionsToForce.getGroupNumber() : CreatureRoot.CREATURE_GROUP_NUMBER_NOGROUP);
        return creatureRoot;
    }

    /**
     * Returns creature with specific description from parameter. If description is not valid, method returns <code>null</code>.
     * 
     * @param descriptionsToForce list of forced attributes, for <code>null</code> value is no attribute forced
     * @param descriptionToBan list of banned attributes, for <code>null</code> value is no attribute banned
     * @param copyThiefAttribute <code>true</code> copy thief flag from descriptionToForce
     * @return randomly generated creature
     */
    public CreatureRoot getCreatureWithAttributes(CreatureDescriptor descriptionsToForce, CreatureDescriptor descriptionToBan, boolean copyThiefAttribute, Random random) {

        return getCreatureWithAttributes(0, 0, descriptionsToForce, descriptionToBan, copyThiefAttribute, random);
    }

    /**
     * Returns creature with specific description from parameter. If description is not valid, method returns <code>null</code>.
     * 
     * @param pX creature position x
     * @param pY creature position y
     * @param descriptionsToForce list of forced attributes, for <code>null</code> value is no attribute forced
     * @param descriptionToBan list of banned attributes, for <code>null</code> value is no attribute banned
     * @param copyThiefAttribute <code>true</code> copy thief flag from descriptionToForce
     * @return randomly generated creature
     */
    public CreatureRoot getCreatureWithAttributes(float pX, float pY, CreatureDescriptor descriptionsToForce, CreatureDescriptor descriptionToBan, boolean copyThiefAttribute, Random random) {

        if ((descriptionsToForce != null) && descriptionsToForce.hasValidDescription()) {
            CreatureRoot creatureRoot = createCreatureAndAttributes(pX, pY, descriptionsToForce, descriptionToBan, random);
            if (copyThiefAttribute && descriptionsToForce.isThief()) {
                creatureRoot.setAsThief(true);
            }
            return creatureRoot;
        }
        return null;
    }

    /**
     * Creates creature with attributes in description parameter
     */
    private CreatureRoot createCreatureAndAttributes(float pX, float pY, CreatureDescriptor descriptionsToForce, CreatureDescriptor descriptionToBan, Random random) {
        AttributeGender genre = (descriptionsToForce != null) && (descriptionsToForce.getGender() != null) ? descriptionsToForce.getGender() : AttributeGender.getRandomGender(random);
        CreatureRoot creatureRoot = new CreatureRoot(genre, pX, pY);
        if (descriptionsToForce != null) {
            for (AttributeDescription attributeDescriptionForce : descriptionsToForce.getDescriptions()) {
            	for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
            		if (creatureRoot.generateAttribute(attributeDescriptionForce, descriptionToBan, false, random)) {
            			break;
            		}
				}
            }
        }
        return creatureRoot;
    }
}
