/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules.easy;

import java.util.Random;

import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.ClothingAttribute;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * 
 * @author Matyáš Latner
 */
public class CACCRule extends CCCCRule {

    public CACCRule(Random random) {
        super(random);
    }
    
    @Override
    public GameRulesDefinition getGameRuleDefinition() {
    	return GameRulesDefinition.CACC;
    }

    @Override
    public String[] prepareRuleMessageConstituentParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        AttributeDescription t0Description0 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0);
        return new String[] {
                t0Description0.getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
                t0Description0.getAttributeConstituent().getConstituent2(abstractTablexiaScreen)
        };
    }

    @Override
    public String[] prepareRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        AttributeDescription t0Description0 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0);
        AttributeDescription t0Description1 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(1);
		return new String[] {
        		getAttributeColorName(abstractTablexiaScreen, t0Description1),
                getAttributeColorName(abstractTablexiaScreen, t0Description0),
                getAttributeName(abstractTablexiaScreen, t0Description0, false),
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	CreatureDescriptor creatureDescriptor = new CreatureDescriptor();
    	CreatureDescriptor randomCreatureDescrition = CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, getRandom()).getCreatureDescrition();
        
        int randomDescriptionPosition1 = getRandom().nextInt(randomCreatureDescrition.getDescriptions().size());
        AttributeColor attributeColor = randomCreatureDescrition.getDescriptions().get(randomDescriptionPosition1).getAttributeColor();
        
        for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
        	AttributeColor randomColor = getRandomColorFromGeneratedCreature(BAN_ATTRIBUTES_SET_FOR_GENERATING);
        	if (attributeColor != randomColor) {
        		creatureDescriptor.addDescription(randomCreatureDescrition.getDescriptions().get(randomDescriptionPosition1));
        		creatureDescriptor.addDescription(new AttributeDescription(randomColor, null, ClothingAttribute.class));
        		addGlobalCreatureDescriptor(T0_OFFSET, creatureDescriptor);
        		return;
        	}
		}
        
        throw new IllegalStateException("Cannot select different color for creature description!");
    }

}
