/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules.medium;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.rules.GameRule;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * 
 * @author Matyáš Latner
 */
public class CC_1_CCRule extends GameRule {
    
	private   static final int 	 	GROUP_SIZE	= 3;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;
	protected 			   Integer 	T1_OFFSET 	= Integer.valueOf(2);

    public CC_1_CCRule(Random random) {
        super(random, GROUP_SIZE);
    }
    
    public CC_1_CCRule(Random random, int creatureNumber) {
        super(random, creatureNumber);
    }
    
    @Override
    public GameRulesDefinition getGameRuleDefinition() {
    	return GameRulesDefinition.CC_1_CC;
    }

	@Override
	public String[] prepareRuleMessageConstituentParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
		return new String[] {
				getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
				getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent2(abstractTablexiaScreen),
				getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
				getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent2(abstractTablexiaScreen)
		};
	}

	@Override
    public String[] prepareRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[] {
        		getAttributeColorName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0)),
        		getAttributeName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0), false),
        		getAttributeColorName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0)),
        		getAttributeName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0), false)
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	CreatureDescriptor t1CreatureDescriptor = getRandomCreatureDescriptionWithOneAttribute(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, getRandom()).getCreatureDescrition());
        addGlobalCreatureDescriptor(T1_OFFSET, t1CreatureDescriptor);
        
        CreatureDescriptor creatureDescriptorToBan = new CreatureDescriptor();
    	creatureDescriptorToBan.disableGenderCompatibilityCheck();
    	creatureDescriptorToBan.addDescriptions(BAN_ATTRIBUTES_SET_FOR_GENERATING.getDescriptions());
    	creatureDescriptorToBan.addDescriptions(t1CreatureDescriptor.getDescriptions());
    	
        CreatureDescriptor t0CreatureDescriptor = getRandomCreatureDescriptionWithOneAttribute(CreatureFactory.getInstance().generateCreature(null, creatureDescriptorToBan, getRandom()).getCreatureDescrition());
        addGlobalCreatureDescriptor(T0_OFFSET, t0CreatureDescriptor);
    }
    
    @Override
    protected List<CreatureRoot> prepareCreatureDescriptionsA(int numberOfCreatures) {
    	
    	AttributeDescription t1AttributeDescription = getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0);
    	AttributeDescription t0AttributeDescription = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0);
    	
    	List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        for (int i = 0; i < numberOfCreatures; i++) {
        	
        	CreatureDescriptor creatureDescriptorToBan = new CreatureDescriptor();
        	creatureDescriptorToBan.disableGenderCompatibilityCheck();
        	
        	CreatureRoot lastCreature = null;
			int lastPosition = i - T1_OFFSET;
			if (lastPosition >= 0) {					
				lastCreature = creatures.get(lastPosition);
			}
        	
            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
			if (creatureDescriptor != null) {
            	
            	// SPECIAL CREATURE
            	
            	if (!creatureDescriptor.isThief() && lastCreature != null && lastCreature.hasAttribute(t1AttributeDescription)) {
            		// T1 SPECIAL CREATURE
					creatureDescriptorToBan.addDescription(t0AttributeDescription);
				}
            	
            	creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, creatureDescriptorToBan, getRandom()));
			} else {
            	
            	// RANDOM CREATURE
				
				boolean lastCreatureHasT1Attribute = lastCreature != null && lastCreature.hasAttribute(t1AttributeDescription);
				if (lastCreatureHasT1Attribute) {
					creatureDescriptorToBan.addDescription(t0AttributeDescription);
				}
				
				// BAIT ATTRIBUTE
				if (getRandom().nextBoolean() || lastCreatureHasT1Attribute) {
					newBaitCreatureDescriptionFromAttribute(t1AttributeDescription);
				} else {
					newBaitCreatureDescriptionFromAttribute(t0AttributeDescription);
				}
				
				creatures.add(CreatureFactory.getInstance().generateCreature(getBaitCreatureDescriptionRandomly(), creatureDescriptorToBan, getRandom()));
			}
        }

        return creatures;
    }

}
