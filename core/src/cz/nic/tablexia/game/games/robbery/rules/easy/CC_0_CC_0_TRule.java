/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules.easy;

import java.util.Random;

import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * 
 * @author Matyáš Latner
 */
public class CC_0_CC_0_TRule extends CA_0_CC_0_TRule {

    public CC_0_CC_0_TRule(Random random) {
        super(random);
    }
    
    @Override
    public GameRulesDefinition getGameRuleDefinition() {
    	return GameRulesDefinition.CC_0_CC_0_T;
    }

    @Override
    public String[] prepareRuleMessageConstituentParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[] {
                getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
                getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent2(abstractTablexiaScreen),
                getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
                getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent2(abstractTablexiaScreen)
        };
    }
    
    @Override
    public String[] prepareRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[] {
        		getAttributeColorName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0)),
        		getAttributeName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0), false),
                getAttributeColorName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0)),
                getAttributeName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0), false),
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	
        CreatureDescriptor t2CreatureDescriptor = getRandomCreatureDescriptionWithOneAttribute(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, getRandom()).getCreatureDescrition());
        AttributeDescription t2AttributeDescription = t2CreatureDescriptor.getDescriptions().get(0);
        addGlobalCreatureDescriptor(T2_OFFSET, t2CreatureDescriptor);
        
        BAN_ATTRIBUTES_SET_FOR_GENERATING.addDescription(t2AttributeDescription);
        addGlobalCreatureDescriptor(T1_OFFSET, getRandomCreatureDescriptionWithOneAttribute(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, getRandom()).getCreatureDescrition()));
        
        addGlobalCreatureDescriptor(T0_OFFSET, new CreatureDescriptor());
    }

}
