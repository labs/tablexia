/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.hair.HairAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.head.HeadAttribute;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;

/**
 * Abstract game rule class. Contains common functions for all game rules.
 *
 * @author Matyáš Latner
 */
public abstract class GameRule {

    protected static final int                  GENERATOR_TRY_COUNT                 = 10000;
    private static final String                 SUFFIX_STRING                       = "<SFIX>";
    private static final String                 NEXT_BIG_CHAR_PATTERN               = "<BIG>";
    protected static final String               RULE_LOG_PREFIX                     = "[RULE] ";
    private static final double                 BAIT_PROBABILITY                    = 0.3;

    protected static final CreatureDescriptor   BAN_ATTRIBUTES_SET_FOR_GENERATING   = new CreatureDescriptor() {
        {

            addDescription(new AttributeDescription(null, null, HeadAttribute.class));
            addDescription(new AttributeDescription(null, null, HairAttribute.class));
            disableGenderCompatibilityCheck();

        }
    };


    private   Random random;
//    protected int    numberOfCreatures;
//    protected int    numberOfThieves;
    private int generatedCreaturesCount = 0;
    
    private int thiefGroupSize;
    private Map<Integer, CreatureDescriptor> globalCreatureDescriptors;

    private int groupNumber = 1;

    private List<CreatureRoot> creatures;
    private CreatureDescriptor baitCreatureDescriptor = new CreatureDescriptor();



    /*
     * Map for special creatures contains thieves and creatures which is important for thieves to be founded.
     */
    protected Map<Integer, CreatureDescriptor> specialCreatures;

    public GameRule(Random random, int thiefGroupSize) {
        this.random = random;
//        this.numberOfCreatures = numberOfCreatures;
//        this.numberOfThieves = numberOfThieves;
        this.thiefGroupSize = thiefGroupSize;
        specialCreatures = new HashMap<Integer, CreatureDescriptor>();
        globalCreatureDescriptors = new HashMap<Integer, CreatureDescriptor>();

        Log.debug(getClass(), " ------- " + RULE_LOG_PREFIX + " " + getClass().getSimpleName() + " starts generating creatures with: [" + random.toString() + "] -------");
    }

    public abstract GameRulesDefinition getGameRuleDefinition();

    public Random getRandom() {
        return random;
    }

    protected void incrementGroupNumber() {
        groupNumber++;
        Log.debug(getClass(), RULE_LOG_PREFIX + "GROUP COMPLETE");
    }


    /**
     * Add creature to the map of specials creatures at specific position
     *
     * @param position key to special creatures map
     * @param specialCreature creature to add in
     */
    protected void addSpecialCreature(Integer position, CreatureDescriptor specialCreature) {
        if (specialCreature != null) {
            CreatureDescriptor existingCreature = specialCreatures.get(position);
            if (existingCreature != null) {
                for (AttributeDescription attributeDescription : specialCreature.getDescriptions()) {
                    existingCreature.addDescription(attributeDescription);
                }
                Log.debug(getClass(), RULE_LOG_PREFIX + " P[" + position + "] SPECIAL added: " + specialCreature);
            } else {
                existingCreature = new CreatureDescriptor(specialCreature.getDescriptions());
                specialCreatures.put(position, existingCreature);
                Log.debug(getClass(), RULE_LOG_PREFIX + " P[" + position + "] SPECIAL generated: " + specialCreature);
            }
            existingCreature.setGroupNumber(groupNumber);
            existingCreature.addCreatureOffsets(specialCreature.getCreatureOffsets());
            existingCreature.setThief(existingCreature.isThief() || specialCreature.isThief());
        }
    }

    /**
     * Return random number from interval for thief position.
     *
     * @param intervalStart start of interval
     * @param intervalSize size of interval
     * @return
     */
    protected int getThiefRandomPositionInInterval(int intervalStart, int intervalSize, int minValue) {
        int position = getRandom().nextInt(intervalSize);
        if (position < minValue) {
            position = minValue;
        }
        return intervalStart + position;
    }

    /**
     * Return random number from interval for thief position.
     * TODO fix comments
     * @return
     */
    protected int getThiefRandomPositionInInterval(int minValue, int numberOfCreatures, List<Integer> bannedPositions) {
        int result = -1;
        for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
            int position = getRandom().nextInt(numberOfCreatures);
            if (position < minValue) {
                position = minValue;
            }
            if (bannedPositions == null || !bannedPositions.contains(position)) {
                result = position;
                break;
            }
        }
        return result;
    }

    /**
     * Returns creature description with two random attributes.
     *
     * @param descriptor random creature description from is choose random attributes
     * @return creature description with one random attribute
     */
    protected CreatureDescriptor getRandomCreatureDescriptionWithTwoAttributes(CreatureDescriptor descriptor) {
        CreatureDescriptor creatureDescriptor = new CreatureDescriptor();

        int randomDescriptionPosition1 = getRandom().nextInt(descriptor.getDescriptions().size());
        int randomDescriptionPosition2;
        for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
            randomDescriptionPosition2 = getRandom().nextInt(descriptor.getDescriptions().size());
            if (randomDescriptionPosition1 != randomDescriptionPosition2) {
                creatureDescriptor.addDescription(descriptor.getDescriptions().get(randomDescriptionPosition1));
                creatureDescriptor.addDescription(descriptor.getDescriptions().get(randomDescriptionPosition2));
                return creatureDescriptor;
            }
        }
        throw new IllegalStateException("Cannot select two different descriptions from creature description!");
    }

    /**
     * Returns creature description with N random attributes.
     *
     * @param sourceCreatureDescriptor creature description for generating from
     * @param descriptionsNumber number of used attribute descriptions from creature description
     * @return creature description with N random attributes.
     */
    protected CreatureDescriptor getRandomCreatureDescriptionWithNAttributes(CreatureDescriptor sourceCreatureDescriptor, int descriptionsNumber) {
        CreatureDescriptor creatureDescriptor = new CreatureDescriptor();

        for (int i = 0; i < descriptionsNumber; i++) {
            creatureDescriptor.addDescription(getRandomAttributeDescriptionFromCreatureDescription(sourceCreatureDescriptor, creatureDescriptor.getDescriptions()));
        }

        return creatureDescriptor;
    }

    /**
     * Returns attribute description from source creature description which is not in bannedAttributeDescriptions list parameter
     *
     * @param bannedAttributeDescriptions list of banned attributes descriptions which is not selected from creature description
     * @return randomly selected attribute description from creature description in parameter
     */
    public AttributeDescription getRandomAttributeDescriptionFromCreatureDescription(CreatureDescriptor creatureDescriptor, List<AttributeDescription> bannedAttributeDescriptions) {
        for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
            AttributeDescription attributeDescription = creatureDescriptor.getDescriptions().get(getRandom().nextInt(creatureDescriptor.getDescriptions().size()));
            if (!bannedAttributeDescriptions.contains(attributeDescription)) {
                return attributeDescription;
            }
        }
        throw new IllegalStateException("Cannot select attribute description from creature description!");
    }

    /**
     * Returns creature description with one random attribute.
     *
     * @param descriptor random creature description from is choose random attribute
     * @return creature description with one random attribute
     */
    protected CreatureDescriptor getRandomCreatureDescriptionWithOneAttribute(CreatureDescriptor descriptor) {
        CreatureDescriptor creatureDescriptor = new CreatureDescriptor();
        creatureDescriptor.addDescription(getRandomAttributeDescription(descriptor));

        return creatureDescriptor;
    }

    /**
     * Return creature descriptor for specific offset
     * @param key offset for selecting creature description
     * @return creature descriptor for specific offset
     */
    public CreatureDescriptor getGlobalCreatureDescriptor(Integer key) {
        return globalCreatureDescriptors.get(key);
    }


    /**
     * Returns random attribute description from creature description
     *
     * @param descriptor random creature description from is choose random attribute
     * @return random attribute description
     */
    protected AttributeDescription getRandomAttributeDescription(CreatureDescriptor descriptor) {
        List<AttributeDescription> descriptions = descriptor.getDescriptions();
        return descriptions.get(getRandom().nextInt(descriptions.size()));
    }

    /**
     * Returns bait creature description with probability from BAIT_PROBABILITY constant or <code>null</code>
     * @return bait creature description or <code>null</code>
     */
    protected CreatureDescriptor getBaitCreatureDescriptionRandomly() {
        if (getRandom().nextDouble() > BAIT_PROBABILITY) {
            return null;
        }
        return baitCreatureDescriptor;
    }

    /**
     * Set creature description from parameter as new bait creature description
     * @param creatureDescriptor new bait creature description
     */
    protected void newBaitCreatureDescription(CreatureDescriptor creatureDescriptor) {
        baitCreatureDescriptor = creatureDescriptor;
    }

    /**
     * Creates new bait creature description and add attribute description from parameter to it
     * @param attributeDescription attribute description for new bait creature description
     */
    protected void newBaitCreatureDescriptionFromAttribute(AttributeDescription attributeDescription) {
        baitCreatureDescriptor = new CreatureDescriptor();
        baitCreatureDescriptor.addDescription(attributeDescription);
    }


    /**
     * Returns attribute description from bait creature description at position 
     * specified in parameter with probability from BAIT_PROBABILITY constant or <code>null</code>
     * @param position position of attribute
     * @return attribute description from bait creature description or <code>null</code>
     */
    protected AttributeDescription getBaitCreatureDescriptionAttributeAtPosition(int position) {
        if (getRandom().nextDouble() <= BAIT_PROBABILITY) {
            if (baitCreatureDescriptor != null) {
                return baitCreatureDescriptor.getDescriptions().get(position);
            }
        }
        return null;
    }

    /**
     * Returns attribute description.
     *
     * @param attributeDescription attribute description class
     * @return attribute description
     */
    protected String getAttributeName(AbstractTablexiaScreen abstractTablexiaScreen, AttributeDescription attributeDescription, boolean useForcedVersion) {
        return getAttributeName(abstractTablexiaScreen, attributeDescription.getAttributeClass(), useForcedVersion);
    }

    /**
     * Returns attribute description.
     *
     * @param attributeClass attribute class
     * @return attribute description
     */
    protected String getAttributeName(AbstractTablexiaScreen abstractTablexiaScreen, Class<? extends Attribute> attributeClass, boolean useForcedVersion) {
        return Attribute.getDescriptionForAttributeClass(abstractTablexiaScreen, attributeClass, useForcedVersion);
    }

    /**
     * Returns color description. If there is no color returns empty string.
     *
     * @param attributeDescription attribute description for obtain color from
     * @return color description
     */
    protected String getAttributeColorName(AbstractTablexiaScreen abstractTablexiaScreen, AttributeDescription attributeDescription) {
        AttributeColor attributeColor = attributeDescription.getAttributeColor();
        return attributeColor != null ? abstractTablexiaScreen.getText(attributeColor.getDescriptionResourceName()) : "";
    }

    public String getRuleMessageText(AbstractTablexiaScreen abstractTablexiaScreen) {
        String formattedRule = abstractTablexiaScreen.getFormattedText(getGameRuleDefinition().getGameRuleStringName(), (Object[]) getRuleMessageParameters(abstractTablexiaScreen));

        // suffixes processing
        for (String ruleMessageConstituentParameter: prepareRuleMessageConstituentParameters(abstractTablexiaScreen)) {
            formattedRule = formattedRule.replaceFirst(SUFFIX_STRING, ruleMessageConstituentParameter);
        }

        // big chars processing
        int index = -1;
        while ((index = formattedRule.indexOf(NEXT_BIG_CHAR_PATTERN, index + 1)) != -1) {
            if (index + NEXT_BIG_CHAR_PATTERN.length() < formattedRule.length()) {
                String charToReplace = String.valueOf(formattedRule.charAt(index + NEXT_BIG_CHAR_PATTERN.length()));
                formattedRule = formattedRule.replaceFirst(NEXT_BIG_CHAR_PATTERN + charToReplace, charToReplace.toUpperCase());
            }
        }

        return formattedRule;
    }

    /**
     * Returns random color from randomly generated creature without any banned attributes from parameter
     *
     * @param banAttributes banned attributes
     * @return randomly generated color
     */
    protected AttributeColor getRandomColorFromGeneratedCreature(CreatureDescriptor banAttributes) {
        AttributeColor randomColor = null;
        int tryCount = 0;
        while (randomColor == null) {
            CreatureDescriptor randomCreature = CreatureFactory.getInstance().generateCreature(null, banAttributes, getRandom()).getCreatureDescrition();
            List<AttributeDescription> randomDescriptions = randomCreature.getDescriptions();
            AttributeDescription randomDescription = randomDescriptions.get(getRandom().nextInt(randomDescriptions.size()));
            randomColor = randomDescription.getAttributeColor();
            if (tryCount >= GENERATOR_TRY_COUNT) {
                throw new IllegalStateException("Cannot generate random color!");
            }
            tryCount++;
        }

        return randomColor;
    }

    /**
     * Adds random creature which has not thieve attribute when there is creature before with "before thief attribute"
     *
     * @param creatures list of creatures
     * @param lastCreature last generated creature
     * @param t1CreatureDescription description of creature before thief
     * @param t0CreatureDescription description of thief
     */
    protected void addRandomCreatureAndCheckCreatureOneBefore(List<CreatureRoot> creatures, CreatureRoot lastCreature, CreatureDescriptor t1CreatureDescription, CreatureDescriptor t0CreatureDescription) {
        if ((lastCreature != null) && lastCreature.hasAttributes(t1CreatureDescription)) {
            creatures.add(CreatureFactory.getInstance().generateCreature(null, t0CreatureDescription, getRandom()));
        } else {
            creatures.add(CreatureFactory.getInstance().generateCreature(getRandom()));
        }
    }

    /**
     * Returns array of strings with parameters to the rule message
     *
     * @return array of strings with parameters to the rule message
     */
    public String[] getRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        String[] messageParameters = prepareRuleMessageParameters(abstractTablexiaScreen);
        if (messageParameters != null) {
            for (String parameter : messageParameters) {
                Log.debug(getClass(), RULE_LOG_PREFIX + "parameter: " + parameter);
            }
        }
        return messageParameters;
    }

    /**
     * Generate new creature on n - 1 position without before thief creature attribute for special creature on position n
     * which is not thief and has thief attribute. When creature on n - 1 position has beforeThiefAttribute.
     * n = lastPosition parameter
     *
     * @param creatures list of creatures to add in
     * @param lastPosition position n
     * @param lastCreature last generated creature
     * @param specialCreature special creature to check
     * @param thiefCreatureDescription description of thief
     * @param beforeThiefCreatureDescription complete description of creature before thief
     */
    protected void checkBeforeSpecialCreature(List<CreatureRoot> creatures, int lastPosition, CreatureRoot lastCreature, CreatureRoot specialCreature, CreatureDescriptor thiefCreatureDescription, CreatureDescriptor beforeThiefCreatureDescription) {
        if (!specialCreature.isThief() && specialCreature.hasAttributes(thiefCreatureDescription)) {
            if ((lastCreature != null) && lastCreature.hasAttributes(beforeThiefCreatureDescription)) {
                creatures.remove(lastPosition);
                creatures.add(CreatureFactory.getInstance().generateCreature(null, beforeThiefCreatureDescription, getRandom()));
            }
        }
    }

    /**
     * Check compatibility for creature at specified position and creature from parameter 
     * @param creatureDescriptor creature to check
     * @param creaturePosition position of creature to check
     * @param isThief <code>true</code> if creature description from parameter is thief
     * @return <code>true</code> if creature description from parameter is compatible with creature in position
     */
    protected boolean checkCreatureCompactibilityForPosition(CreatureDescriptor creatureDescriptor, int creaturePosition, boolean isThief) {
        CreatureDescriptor specialCreature = specialCreatures.get(creaturePosition);
        return specialCreature != null ? !(specialCreature.isThief() && isThief) && (specialCreature.checkDescriptionCompactibility(creatureDescriptor)) : true;
    }

    public String[] prepareRuleMessageConstituentParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[]{};
    }

    /**
     * Returns array of strings with parameters to the rule message. Specific rule class implementation
     *
     * @return array of strings with parameters to the rule message
     */
    public abstract String[] prepareRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen);

    /**
     * Returns list of creatures for displaying in game. List contains random creatures and thieves.
     *
     * @return list of random creatures and thieves
     */
    public void generateCreatures(AbstractTablexiaGame abstractTablexiaGame, int numberOfCreatures, int numberOfThieves) {
        prepareCreatureDescriptionsC();
        creatures = prepareCreatures(numberOfCreatures, numberOfThieves);
        for (int i = 0; i < creatures.size(); i++) {
            CreatureRoot creature = creatures.get(i);
            creature.loadAssets(abstractTablexiaGame);
            Log.debug(getClass(), RULE_LOG_PREFIX + " ---> [" + i + "] generated creature: " + creature);
        }
        generatedCreaturesCount += creatures.size();
    }
    
    public void generateNewCreaturesPack(AbstractTablexiaGame abstractTablexiaGame, int numberOfCreatures){
        creatures = prepareCreatureDescriptionsA(numberOfCreatures);
        for (int i = 0; i < creatures.size(); i++) {
            CreatureRoot creature = creatures.get(i);
            creature.loadAssets(abstractTablexiaGame);
            Log.debug(getClass(), RULE_LOG_PREFIX + " ---> [" + i + "] generated creature: " + creature);
        }
        generatedCreaturesCount += creatures.size();
    }
    
    public int getGeneratedCreaturesCount(){
        return generatedCreaturesCount;
    }

    public List<CreatureRoot> getCreatures() {
        return creatures;
    }
    
    /**
     * Returns list of creatures for displaying in game. List contains random creatures and thieves. Specific rule class implementation.
     *
     * @return list of random creatures and thieves
     */

    public List<CreatureRoot> prepareCreatures(int numberOfCreatures, int numberOfThieves) {
        // generate thieves and pair creatures
        for (int i = 0; i < numberOfThieves; i++) {

            int initialPosition = 0;
            boolean condition = true;
            List<Integer> bannedPositions = new ArrayList<Integer>();

            prepareCreatureDescriptionsB();

            while (condition) {
                initialPosition = getThiefRandomPositionInInterval(thiefGroupSize - 1,numberOfCreatures, bannedPositions);

                if (initialPosition < 0) {
                    throw new IllegalStateException("Cannot generate creature serie! Too many conflicts!");
                }

                bannedPositions.add(initialPosition);
                condition = !checkCreaturePositions(initialPosition);
            }

            addSpecialCreatures(initialPosition);
            incrementGroupNumber();
        }

        return prepareCreatureDescriptionsA(numberOfCreatures);
    }

    private void addSpecialCreatures(int initialPosition) {
        for (Integer creatureOffset : globalCreatureDescriptors.keySet()) {
            addSpecialCreature(initialPosition - creatureOffset, globalCreatureDescriptors.get(creatureOffset));
        }
    }

    

    private boolean checkCreaturePositions(int initialPosition) {
        boolean result = true;
        for (Integer creatureOffset : globalCreatureDescriptors.keySet()) {
            int positionToTry = initialPosition - creatureOffset;
            result = result && checkSpecialCreaturePosition(positionToTry, creatureOffset) && checkCreatureCompactibilityForPosition(globalCreatureDescriptors.get(creatureOffset), positionToTry, creatureOffset == CreatureDescriptor.THIEF_OFFSET);
            if (!result) {
                break;
            }
        }
        return result;
    }

    protected boolean checkSpecialCreaturePosition(int position, Integer creatureOffset) {
        return true;
    }

    /**
     * Add global creature descriptor with offset from thief.
     * @param offset offset from thief - 0 = thief
     * @param creatureDescriptor creature descriptor to add
     */
    protected void addGlobalCreatureDescriptor(int offset, CreatureDescriptor creatureDescriptor) {
        globalCreatureDescriptors.put(offset, creatureDescriptor);
        creatureDescriptor.addCreatureOffset(Integer.valueOf(offset));
    }

    /**
     * Prepares all creatures root
     * @return list of all creatures root
     * @param numberOfCreatures
     */
    protected abstract List<CreatureRoot> prepareCreatureDescriptionsA(int numberOfCreatures);
    
    protected void prepareCreatureDescriptionsB() {}

    protected abstract void prepareCreatureDescriptionsC();


}
