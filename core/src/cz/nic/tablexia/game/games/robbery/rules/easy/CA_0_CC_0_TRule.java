/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules.easy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.ClothingAttribute;
import cz.nic.tablexia.game.games.robbery.rules.GameRule;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * 
 * @author Matyáš Latner
 */
public class CA_0_CC_0_TRule extends GameRule {
    
	private static final double 	BAIT_RANDOM_DISTRIBUTION = 0.3;
	private   static final int 	 	GROUP_SIZE	= 3;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;
	protected static final Integer 	T1_OFFSET 	= Integer.valueOf(1);
	protected static final Integer 	T2_OFFSET 	= Integer.valueOf(2);

    public CA_0_CC_0_TRule(Random random) {
        super(random, GROUP_SIZE);
    }
    
    @Override
    public GameRulesDefinition getGameRuleDefinition() {
    	return GameRulesDefinition.CA_0_CC_0_T;
    }

    @Override
    public String[] prepareRuleMessageConstituentParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[] {
                getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
                getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent2(abstractTablexiaScreen)
        };
    }

    @Override
    public String[] prepareRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[] {
        		getAttributeColorName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0)),
                getAttributeColorName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0)),
                getAttributeName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0), false),
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
        AttributeColor t2CommonAttributeColor = getRandomColorFromGeneratedCreature(BAN_ATTRIBUTES_SET_FOR_GENERATING);
        CreatureDescriptor t2CreatureDescriptor = new CreatureDescriptor();
        t2CreatureDescriptor.addDescription(new AttributeDescription(t2CommonAttributeColor, null, ClothingAttribute.class));
        addGlobalCreatureDescriptor(T2_OFFSET, t2CreatureDescriptor);
        
        addGlobalCreatureDescriptor(T1_OFFSET, getRandomCreatureDescriptionWithNAttributes(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, getRandom()).getCreatureDescrition(), 1));
        
        addGlobalCreatureDescriptor(T0_OFFSET, new CreatureDescriptor());
    }
    
    @Override
    protected List<CreatureRoot> prepareCreatureDescriptionsA(int numberOfCreatures) {
    	List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        CreatureRoot lastCreature = null;
        int lastPosition;
        // generate random creatures and add thieves and pair creatures to the specific positions
        for (int i = 0; i < numberOfCreatures; i++) {
            lastPosition = creatures.size() - 1;
            if (lastPosition >= 0) {
                lastCreature = creatures.get(lastPosition);
            }

            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
            if (creatureDescriptor != null) { // add special creature
            	if (lastCreature != null && lastCreature.hasAttribute(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0)) && !creatureDescriptor.containsAttributeDescription(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0))) {
            		creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, getGlobalCreatureDescriptor(T1_OFFSET), getRandom()));
                } else {
            		creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, null, getRandom()));
                }
            } else {
            	if ((lastCreature != null) && lastCreature.hasAttributes(getGlobalCreatureDescriptor(T2_OFFSET))) {
                    creatures.add(CreatureFactory.getInstance().generateCreature(null, getGlobalCreatureDescriptor(T1_OFFSET), getRandom()));
                } else {
                	
                	if(getRandom().nextDouble() < BAIT_RANDOM_DISTRIBUTION) {
                		newBaitCreatureDescription(getGlobalCreatureDescriptor(T2_OFFSET));
                	} else {
                		newBaitCreatureDescription(getGlobalCreatureDescriptor(T1_OFFSET));
                	}
                	
                    creatures.add(CreatureFactory.getInstance().generateCreature(getBaitCreatureDescriptionRandomly(), null, getRandom()));
                }
            }
        }

        return creatures;
    }

}
