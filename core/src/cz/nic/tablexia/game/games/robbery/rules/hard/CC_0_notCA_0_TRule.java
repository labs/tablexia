/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules.hard;

import java.util.Random;

import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.ClothingAttribute;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * 
 * @author Matyáš Latner
 */
public class CC_0_notCA_0_TRule extends CC_0_notCC_0_TRule {

    public CC_0_notCA_0_TRule(Random random) {
        super(random);
    }
    
    @Override
    public GameRulesDefinition getGameRuleDefinition() {
    	return GameRulesDefinition.CC_0_notCA_0_T;
    }

    @Override
    public String[] prepareRuleMessageConstituentParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[] {
                getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
                getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent2(abstractTablexiaScreen)
        };
    }

    @Override
    public String[] prepareRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[] {
        		getAttributeColorName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0)),
        		getAttributeName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0), false),
        		getAttributeColorName(abstractTablexiaScreen, t1CreatureDescriptorToBan.getDescriptions().get(0)),
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
        AttributeDescription t2AttributeDescription = getRandomAttributeDescription(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, getRandom()).getCreatureDescrition());
    	CreatureDescriptor t2CreatureDescriptor = new CreatureDescriptor();
    	t2CreatureDescriptor.addDescription(t2AttributeDescription);
        addGlobalCreatureDescriptor(T2_OFFSET, t2CreatureDescriptor);
        
        
        CreatureDescriptor t1BannedAttributesForGenerator = new CreatureDescriptor();
        t1BannedAttributesForGenerator.disableGenderCompatibilityCheck();
        t1BannedAttributesForGenerator.addDescriptions(BAN_ATTRIBUTES_SET_FOR_GENERATING.getDescriptions());
        if (t2AttributeDescription.hasSpecificColor()) {        	
        	t1BannedAttributesForGenerator.addDescription(new AttributeDescription(t2AttributeDescription.getAttributeColor(), null, ClothingAttribute.class));
        }
        
        addGlobalCreatureDescriptor(T1_OFFSET, new CreatureDescriptor());
    	AttributeColor t0CommonAttributeColor = getRandomColorFromGeneratedCreature(t1BannedAttributesForGenerator);
    	t1CreatureDescriptorToBan = new CreatureDescriptor();
        AttributeDescription t1AttributeDescription = new AttributeDescription(t0CommonAttributeColor, null, ClothingAttribute.class);
        t1CreatureDescriptorToBan.addDescription(t1AttributeDescription);
        
        addGlobalCreatureDescriptor(T0_OFFSET, new CreatureDescriptor());
    }

}
