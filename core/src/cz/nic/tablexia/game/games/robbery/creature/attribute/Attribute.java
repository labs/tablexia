/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.creature.attribute;

import com.badlogic.gdx.utils.reflect.ClassReflection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Random;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.games.robbery.assets.RobberyAssets;
import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot.AttributeGender;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.ClothingAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.glasses.GlassesAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.hair.HairAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.head.HeadAttribute;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;

/**
 * Creature attribute class
 * 
 * @author Matyáš Latner
 */
public abstract class Attribute {

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface CreatureGenericType {
	 
	    boolean isGeneric() default false;
	 
	}
	
	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface CreatureSuperGenericType {
	 
	    boolean isGeneric() default false;
	 
	}

    private static final String STRING_RESOURCE_PREFIX = "game_robbery_attribute_";

    /**
     * Attributes constituents
     *
     * @author Matyáš Latner
     */
    public enum AttributeConstituent {

        M           (RobberyAssets.CONSTITUENT1_M, RobberyAssets.CONSTITUENT2_M, RobberyAssets.CONSTITUENT3_M),
        F           (RobberyAssets.CONSTITUENT1_F, RobberyAssets.CONSTITUENT2_F, RobberyAssets.CONSTITUENT3_F),
        N           (RobberyAssets.CONSTITUENT1_N, RobberyAssets.CONSTITUENT2_N, RobberyAssets.CONSTITUENT3_N),
        M_FORCED    (M.constituentKey1, null, M.constituentKey3),
        F_FORCED    (F.constituentKey1, null, F.constituentKey3),
        N_FORCED    (N.constituentKey1, null, N.constituentKey3);

        private String constituentKey1;
        private String constituentKey2;
        private String constituentKey3;

        AttributeConstituent(String constituentKey1, String constituentKey2, String constituentKey3) {
            this.constituentKey1 = constituentKey1;
            this.constituentKey2 = constituentKey2;
            this.constituentKey3 = constituentKey3;
        }

        public String getConstituent1(AbstractTablexiaScreen abstractTablexiaScreen) {
            return constituentKey1 != null ? abstractTablexiaScreen.getText(constituentKey1) : "";
        }

        public String getConstituent2(AbstractTablexiaScreen abstractTablexiaScreen) {
            return constituentKey2 != null ? abstractTablexiaScreen.getText(constituentKey2) : "";
        }

        public String getConstituent3(AbstractTablexiaScreen abstractTablexiaScreen) {
            return constituentKey3 != null ? abstractTablexiaScreen.getText(constituentKey3) : "";
        }
    }

    /**
     * Attributes color types
     * 
     * @author Matyáš Latner
     */
    public enum AttributeColor {

        RED("game_robbery_color_red"),
        GREEN("game_robbery_color_green"),
        BLUE("game_robbery_color_blue"),
        BROWN("game_robbery_color_brown"),
        GREY("game_robbery_color_grey"),
        ORANGE("game_robbery_color_orange"),
        PURPLE("game_robbery_color_purple"),
        PINK("game_robbery_color_pink"),
        YELLOW("game_robbery_color_yellow"),
        BLACK("game_robbery_color_black"),
        WHITE("game_robbery_color_white");

        private String descriptionResourceName;

        AttributeColor(String descriptionResourceId) {
            this.descriptionResourceName = descriptionResourceId;
        }

        public String getDescriptionResourceName() {
            return descriptionResourceName;
        }

        public static AttributeColor getRandomColor(Random random) {
            AttributeColor[] values = AttributeColor.values();
            return values[random.nextInt(values.length)];
        }
    }

    private AttributeConstituent    attributeConstituent    = null;
    private AttributeColor 	        attributeColor          = null;
	private float 			        x;
	private float 			        y;
    private int                     z;
    private String 			        textureName;

    /**
     * Attributes constructor
     * 
     * @param attributeColor current attributes property
     */
    public Attribute(AttributeConstituent attributeConstituent, AttributeColor attributeColor, float x, float y, int z, String textureName) {
        this.attributeConstituent = attributeConstituent;
        this.attributeColor = attributeColor;
		this.x = x;
		this.y = y;
        this.z = z;
        this.textureName = textureName;
    }
    
    public float getX() {
		return x;
	}
    
    public float getY() {
		return y;
	}

    public int getZ() {
        return z;
    }

    public String getTextureName() {
		return textureName;
	}

    /**
     * Returns attributes generic type
     */
    public Class<? extends Attribute> getGenericType() {
        return Attribute.class;
    }
    
    /**
     * Returns current attribute gender
     * 
     * @return current attribute gender
     */
    public static AttributeGender getAttributeGender() {
		return AttributeGender.ANY;
	}

    /**
     * Returns current attribute constituent
     *
     * @return current attribute constituent
     */
    public AttributeConstituent getAttributeConstituent() {
        return attributeConstituent;
    }

    /**
     * Returns current attribute color
     * 
     * @return current attribute color
     */
    public AttributeColor getAttributeColor() {
        return attributeColor;
    }

    /**
     * Returns attribute description for this attribute
     * 
     * @return attribute description for this attribute
     */
    public AttributeDescription getAttributeDescription() {
        return new AttributeDescription(this);
    }
    /**
     * Returns all available textures for current attribute.
     * Generic types returns all textures from child types.
     * 
     * @return list of attributes textures
     */
    public static List<AttributeDescription> getTextures() {
        return new ArrayList<AttributeDescription>() {

            private static final long serialVersionUID = 3710521203238454467L;

            {
                addAll(GlassesAttribute.getTextures());
                addAll(HairAttribute.getTextures());
                addAll(HeadAttribute.getTextures());
                addAll(ClothingAttribute.getTextures());
            }
        };
    }
    
    public static List<Class<? extends Attribute>> getAttributeClasses() {
        return new ArrayList<Class<? extends Attribute>>() {

            private static final long serialVersionUID = 3710521203238454467L;

            {
                addAll(GlassesAttribute.getAttributeClasses());
                addAll(HairAttribute.getAttributeClasses());
                addAll(HeadAttribute.getAttributeClasses());
                addAll(ClothingAttribute.getAttributeClasses());
            }
        };
    }

    /**
     * Returns text description from <code>strings.xml</code> for attribute defined by a class in parameter
     * 
     * @param attributeClass class defining attribute
     * @return description of attribute
     */
    public static String getDescriptionForAttributeClass(AbstractTablexiaGame abstractTablexiaGame, Class<? extends Attribute> attributeClass) {
        return getDescriptionForAttributeClass(abstractTablexiaGame, attributeClass, false);
    }

    /**
     * Returns text description from <code>strings.xml</code> for attribute defined by a class in parameter
     * 
     * @param attributeClass class defining attribute
     * @param useForcedVersion for <code>true</code> value use simple version of string
     * @return description of attribute
     */
    public static String getDescriptionForAttributeClass(AbstractTablexiaScreen abstractTablexiaScreen, Class<? extends Attribute> attributeClass, boolean useForcedVersion) {
        String key = STRING_RESOURCE_PREFIX + attributeClass.getSimpleName().toLowerCase();
        if (useForcedVersion) {
        	String forcedKey = key + "_forced";
        	try {
        		abstractTablexiaScreen.getText(forcedKey);
        		key = forcedKey;
			} catch (MissingResourceException e) {
				Log.info(Attribute.class, "Missing forced key: " + forcedKey + " -> Using standart key: " + key);
        	}
        }
		return abstractTablexiaScreen.getText(key);
    }

    /**
     * Returns random attribute from first parameter for specified color and gender. For <code>null</code> color and gender is use random value.
     * 
     * @param globalGender global creature gender, is used if there is no attributeDescriptionForce gender
     * @param attributeDescriptionForce forced attribute
     * @param descriptionsToBan list of banned attributes
     * @param generateNull for <code>true</code> value can be chosen <code>null</code> value as result
     * @param random instance of {@link Random}
     * @return random attribute
     */
    public static Attribute getRandomAttribute(AttributeGender globalGender,
            AttributeDescription attributeDescriptionForce,
            CreatureDescriptor descriptionsToBan,
            boolean generateNull,
            Random random) {

        List<AttributeDescription> textures = getTexturesFromAttributeClass(attributeDescriptionForce.getAttributeClass());
        List<AttributeDescription> resultList = textures;
        AttributeColor  color  = attributeDescriptionForce.getAttributeColor();
        AttributeGender gender = attributeDescriptionForce.hasSpecificGender() ? attributeDescriptionForce.getAttributeGender() : globalGender;

        // is set color or gender filter or throw out null values or there is some banned attribute else skip this block and use all textures list
        if ((color != null) || (gender != null) || !generateNull || ((descriptionsToBan != null) && (descriptionsToBan.getDescriptions().size() > 0))) {
            resultList = new ArrayList<AttributeDescription>();
            for (AttributeDescription textureContainer : textures) {

                if ((textureContainer == null) && generateNull) {
                    resultList.add(textureContainer);
                } else if (textureContainer != null) {

                    // check if this color is wanted
                    boolean sameColor = (color != null) && (textureContainer.getAttributeColor() == color); // color is banned and color property of attribute is not same as parameter

                    // check if this gender is wanted
                    boolean sameGender = (gender != null) && ((gender == AttributeGender.ANY) || // any gender parameter (same as null gender parameter)
                            (textureContainer.getAttributeGender() == AttributeGender.ANY) ||    // any gender property of attribute
                            (textureContainer.getAttributeGender() == gender));                  // gender parameter is same as gender property of attribute

                    // check if this attribute is banned
                    boolean isAttributeBan = isAttributeBan(descriptionsToBan, textureContainer);

                    if ((((color == null) && sameGender) || ((gender == null) && sameColor) || (sameColor && sameGender) || ((color == null) && (gender == null) && !generateNull)) && !isAttributeBan) {

                        resultList.add(textureContainer);
                    }

                }
            }
        }

        // randomly select one item from list of attributes
        if (resultList.size() > 0) {
            AttributeDescription textureContainer = resultList.get(random.nextInt(resultList.size()));
            if (textureContainer != null) {
                try {
               
                	return (Attribute) ClassReflection.getConstructor(textureContainer.getAttributeClass(), AttributeColor.class,
                																		 					String.class).newInstance(textureContainer.getAttributeColor(),
                                                                    		   					 				   					  textureContainer.getTexturePath());
                } catch (Exception e) {
                	Log.err(Attribute.class, "Cannot create character attribute: " + String.valueOf(textureContainer.getAttributeClass()), e);
                }
            }
        }


        return null;
    }

    /**
     * Check if current attribute is banned in parameter list
     * 
     * @param descriptionsToBan list of banned attributes
     * @param attributeDescription current attribute description
     * @return <code>true</code> if current attribute is not banned
     */
    private static boolean isAttributeBan(CreatureDescriptor descriptionsToBan, AttributeDescription attributeDescription) {
        boolean isAttributeBan = false;
        if (descriptionsToBan != null) {
            for (AttributeDescription attributeDescriptionBan : descriptionsToBan.getDescriptions()) {
                Class<? extends Attribute> attributeDescriptionBanClass = attributeDescriptionBan.getAttributeClass();
                AttributeColor attributeDescriptionBanColor = attributeDescriptionBan.getAttributeColor();

                isAttributeBan = isAttributeBan || (isClassGenericType(attributeDescription.getAttributeClass(), attributeDescriptionBanClass) && ((attributeDescriptionBanColor == null) || (attributeDescriptionBanColor == attributeDescription.getAttributeColor())));
                if (isAttributeBan) {
                    break;
                }
            }
        }
        return isAttributeBan;
    }
    
    /**
     * Call getAttributeGender static method from attribute class in parameter
     * 
     * @param attributeClass attribute class for calling method getGender from
     * @return parameter attribute gender
     */
    public static AttributeGender getGenderFromAttributeClass(Class<? extends Attribute> attributeClass) {
        try {
        	return (AttributeGender) ClassReflection.getMethod(attributeClass, "getAttributeGender").invoke(attributeClass);
        } catch (Exception e) {
        	Log.err(Attribute.class, "Cannot get gender from class: " + attributeClass, e);
            return null;
        }
    }

    /**
     * Call getTextures static method from attribute class in parameter
     * 
     * @param attributeClass attribute class for calling method getTextures from
     * @return list of attribute descriptions containing textures
     */
    @SuppressWarnings("unchecked")
    public static List<AttributeDescription> getTexturesFromAttributeClass(Class<? extends Attribute> attributeClass) {
        try {
        	return (List<AttributeDescription>) ClassReflection.getMethod(attributeClass, "getTextures").invoke(attributeClass);
        } catch (Exception e) {
        	Log.err(Attribute.class, "Cannot get textures from class: " + attributeClass, e);
            return new ArrayList<AttributeDescription>();
        }
    }

    /**
     * Returns all available colors from {@link Attribute} class
     * 
     * @param attributeClass attribute class for obtain colors
     * @return list of available colors
     */
    public static List<AttributeColor> getAvalibleColorsFromAttributeClass(Class<? extends Attribute> attributeClass) {
        List<AttributeDescription> texturesFromAttributeClass = getTexturesFromAttributeClass(attributeClass);
        List<AttributeColor> attributeColors = new ArrayList<AttributeColor>();
        for (AttributeDescription attributeDescription : texturesFromAttributeClass) {
            attributeColors.add(attributeDescription.getAttributeColor());
        }
        return attributeColors;
    }
    
    /**
     * Returns available gender for attribute color and attribute class parameters
     * 
     * @param attributeColor attribute color for obtain gender
     * @param attributeClass attribute class for obtain gender
     * @return available genders
     */
    public static AttributeGender getAvalibleGenderForColorAndClass(AttributeColor attributeColor, Class<? extends Attribute> attributeClass) {
        List<AttributeDescription> texturesFromAttributeClass = getTexturesFromAttributeClass(attributeClass);
        List<AttributeGender> attributeGenders = new ArrayList<AttributeGender>();
        for (AttributeDescription attributeDescription : texturesFromAttributeClass) {
        	if (attributeDescription != null && attributeDescription.getAttributeColor() == attributeColor) {        		
        		if (attributeDescription.hasSpecificGender() && !attributeGenders.contains(attributeDescription.getAttributeGender())) {        		
        			attributeGenders.add(attributeDescription.getAttributeGender());
        		}
        	}
        }
        if (attributeGenders.size() == AttributeGender.values().length) {        	
        	return AttributeGender.ANY;
        } else if (attributeGenders.size() == 1) {
        	return attributeGenders.get(0);
        }
        return null;
    }
    
    /**
     * Returns all generic attribute classes
     * @return all generic attribute classes
     */
    public static List<Class<? extends Attribute>> getAllGenericAttributesClasses() {
    	List<Class<? extends Attribute>> result = new ArrayList<Class<? extends Attribute>>();
    	for (Class<? extends Attribute> attributeClass : getAttributeClasses()) {
    		if (ClassReflection.isAnnotationPresent(attributeClass, CreatureGenericType.class) && ClassReflection.getDeclaredAnnotation(attributeClass, CreatureGenericType.class).getAnnotation(CreatureGenericType.class).isGeneric()) {
    			result.add(attributeClass);
    		}
		}
    	
    	return result;
	}

    /**
     * Returns random color from available colors in {@link Attribute} class
     * 
     * @param attributeClass attribute class for obtain color
     * @return random available color
     */
    public static AttributeColor getRandomAvalibleColorFromAttributeClass(Class<? extends Attribute> attributeClass, Random random) {
        List<AttributeColor> colorsFromAttributeClass = getAvalibleColorsFromAttributeClass(attributeClass);
        return colorsFromAttributeClass.get(random.nextInt(colorsFromAttributeClass.size()));
    }

    /**
     * Returns <code>true</code> if class in attribute1 parameter has class in attribute2 parameter as generic type
     */
    public static boolean isClassGenericType(Class<? extends Attribute> attribute1, Class<? extends Attribute> attribute2) {
        Class<?> attributeToCheck = attribute1;
        while (attributeToCheck != null) {
            if (attributeToCheck.equals(attribute2)) {
                return true;
            }
            if (attributeToCheck == Attribute.class) {
                break;
            }
            attributeToCheck = attributeToCheck.getSuperclass();
        }
        return false;
    }
    
}

