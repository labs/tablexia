/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules.hard;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.headgear.HeadgearAttribute;
import cz.nic.tablexia.game.games.robbery.rules.GameRule;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * 
 * @author Matyáš Latner
 */
public class CCCCnotCCRule extends GameRule {
	
	private static final int 	 	GROUP_SIZE	= 1;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;
	private AttributeDescription 	t0AttributeDescriptionToBan;

    public CCCCnotCCRule(Random random) {
        super(random, GROUP_SIZE);
    }
    
    @Override
    public GameRulesDefinition getGameRuleDefinition() {
    	return GameRulesDefinition.CCCCnotCC;
    }

	@Override
	public String[] prepareRuleMessageConstituentParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
		AttributeDescription t0Description0 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0);
		AttributeDescription t0Description1 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(1);
		return new String[] {
				t0Description0.getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
				t0Description0.getAttributeConstituent().getConstituent2(abstractTablexiaScreen),
				t0Description1.getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
				t0Description1.getAttributeConstituent().getConstituent2(abstractTablexiaScreen),
				t0AttributeDescriptionToBan.getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
				t0AttributeDescriptionToBan.getAttributeConstituent().getConstituent2(abstractTablexiaScreen)
		};
	}

	@Override
    public String[] prepareRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        AttributeDescription t0Description0 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0);
        AttributeDescription t0Description1 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(1);
		return new String[] {
        		getAttributeColorName(abstractTablexiaScreen, t0Description0),
        		getAttributeName(abstractTablexiaScreen, t0Description0, false),
                getAttributeColorName(abstractTablexiaScreen, t0Description1),
                getAttributeName(abstractTablexiaScreen, t0Description1, false),
                getAttributeColorName(abstractTablexiaScreen, t0AttributeDescriptionToBan),
                getAttributeName(abstractTablexiaScreen, t0AttributeDescriptionToBan, false),
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	CreatureDescriptor creatureDescriptorToForce = new CreatureDescriptor();
    	creatureDescriptorToForce.addDescription(new AttributeDescription(null, null, HeadgearAttribute.class));
    	CreatureDescriptor creatureDescriptor = getRandomCreatureDescriptionWithNAttributes(CreatureFactory.getInstance().generateCreature(creatureDescriptorToForce, BAN_ATTRIBUTES_SET_FOR_GENERATING, getRandom()).getCreatureDescrition(), 3);
    	t0AttributeDescriptionToBan = getRandomAttributeDescription(creatureDescriptor);
    	creatureDescriptor.getDescriptions().remove(t0AttributeDescriptionToBan);
		addGlobalCreatureDescriptor(T0_OFFSET, creatureDescriptor);
    }

    @Override
    public List<CreatureRoot> prepareCreatureDescriptionsA(int numberOfCreatures) {

        List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        
        CreatureDescriptor t0CreatureDescriptionToBan = new CreatureDescriptor();
        t0CreatureDescriptionToBan.addDescription(t0AttributeDescriptionToBan);
        
        CreatureDescriptor t0CreatureDescription = getGlobalCreatureDescriptor(T0_OFFSET);
        CreatureDescriptor creatureDescriptorToBan0 = new CreatureDescriptor();
		creatureDescriptorToBan0.addDescription(t0CreatureDescription.getDescriptions().get(0));
        CreatureDescriptor creatureDescriptorToBan1 = new CreatureDescriptor();
        creatureDescriptorToBan1.addDescription(t0CreatureDescription.getDescriptions().get(1));
        
        for (int i = 0; i < numberOfCreatures; i++) {
            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
            if (creatureDescriptor != null) {
            	creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, t0CreatureDescriptionToBan, getRandom()));
			} else {
            	CreatureDescriptor baitCreatureDescriptor = new CreatureDescriptor();
            	CreatureDescriptor creatureDescriptorToBan = null;
            	creatureDescriptor = new CreatureDescriptor();
            	
            	switch(getRandom().nextInt(3)) {
            		case 0:
            			creatureDescriptorToBan = creatureDescriptorToBan0;
            			
            			baitCreatureDescriptor.addDescriptions(creatureDescriptorToBan1.getDescriptions());
            			break;
            		case 1:
            			creatureDescriptorToBan = creatureDescriptorToBan1;
            			
            			baitCreatureDescriptor.addDescriptions(creatureDescriptorToBan0.getDescriptions());
            			break;
            		case 2:
            			creatureDescriptor = t0CreatureDescriptionToBan;
            			
            			baitCreatureDescriptor.addDescriptions(t0CreatureDescriptionToBan.getDescriptions());
            			baitCreatureDescriptor.addDescriptions(creatureDescriptorToBan0.getDescriptions());
            			baitCreatureDescriptor.addDescriptions(creatureDescriptorToBan1.getDescriptions());
            			break;
            	}
            	
            	newBaitCreatureDescription(baitCreatureDescriptor);
            	CreatureDescriptor baitCreatureDescriptionRandomly = getBaitCreatureDescriptionRandomly();
            	if (baitCreatureDescriptionRandomly != null) {
            		creatures.add(CreatureFactory.getInstance().generateCreature(baitCreatureDescriptionRandomly, creatureDescriptorToBan, getRandom()));
				} else {
            		creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, creatureDescriptorToBan, getRandom()));
				}
            }
        }

        return creatures;
    }

}
