/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.creature.attribute.hair;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.robbery.RobberyGame;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.CreatureGenericType;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;

@CreatureGenericType(isGeneric = true)
public abstract class HairAttribute extends Attribute {

    private static final AttributeConstituent ATTRIBUTE_CONSTITUENT = AttributeConstituent.F;

    private static final int   POSITION_X = 0;
    private static final float POSITION_Y = 253f / RobberyGame.CREATURES_HEIGHT;
    private static final int   Z_INDEX    = 4;

    public static List<AttributeDescription> getTextures() {
        return new ArrayList<AttributeDescription>() {

            private static final long serialVersionUID = 618159614388924773L;

            {
                addAll(FBraidhairAttribute.getTextures());
                addAll(FLonghairAttribute.getTextures());
                addAll(MHairAttribute.getTextures());
            }
        };
    }
    
    public static List<Class<? extends Attribute>> getAttributeClasses() {
        return new ArrayList<Class<? extends Attribute>>() {

            private static final long serialVersionUID = -1377013489500837025L;

            {
            	add(HairAttribute.class);
                add(FBraidhairAttribute.class);
                add(FLonghairAttribute.class);
                add(MHairAttribute.class);
            }
        };
    }

    public HairAttribute(AttributeColor color, String textureName) {
        super(ATTRIBUTE_CONSTITUENT, color, POSITION_X, POSITION_Y, Z_INDEX, textureName);
    }

    @Override
    public Class<? extends Attribute> getGenericType() {
        return HairAttribute.class;
    }

}
