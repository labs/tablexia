/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.robbery.creature.attribute;

import com.badlogic.gdx.utils.reflect.ClassReflection;

import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot.AttributeGender;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.CreatureGenericType;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.CreatureSuperGenericType;

/**
 * Attribute description object for description of specific attribute to force or ban.
 * 
 * @author Matyáš Latner
 */
public class AttributeDescription {

    private Attribute.AttributeConstituent  attributeConstituent;
    private AttributeColor                  attributeColor;
    private AttributeGender                 attributeGender;
    private String                          texturePath;
    private Class<? extends Attribute>      attributeClass;

    public AttributeDescription(AttributeColor attributeColor, AttributeGender attributeGender, String texturePath, Class<? extends Attribute> attributeClass) {
        if ((attributeGender != null) && (attributeClass != null)) {
            AttributeGender attributeClassGender = Attribute.getGenderFromAttributeClass(attributeClass);
            if ((attributeClassGender != null) && (attributeClassGender != AttributeGender.ANY)) {
                if (attributeGender != attributeClassGender) {
                    throw new IllegalStateException("Incompactible genders are set for attribute description. GENDER: " + attributeGender + " - ATTRIBUTE CLASS: (" + attributeClassGender + ") " + attributeClass.getSimpleName());
                }
            }
        }

        this.texturePath 		    = texturePath;
        this.attributeClass 	    = attributeClass;
        this.attributeColor 	    = attributeColor;
        this.attributeGender 	    = attributeGender;
    }

    public AttributeDescription(AttributeColor attributeColor, AttributeGender attributeGender, Class<? extends Attribute> attributeClass) {
        this(attributeColor, attributeGender, null, attributeClass);
    }

    public AttributeDescription(Attribute attribute) {
        this(attribute.getAttributeColor(), null, attribute.getClass());
        attributeConstituent = attribute.getAttributeConstituent();
    }

    public String getTexturePath() {
        return texturePath;
    }

    public Class<? extends Attribute> getAttributeClass() {
        return attributeClass;
    }

    public Attribute.AttributeConstituent getAttributeConstituent() {
        return attributeConstituent;
    }

    public AttributeColor getAttributeColor() {
        return attributeColor;
    }

    /**
     * Return attribute gender. If gender is not explicitly set returns gender from attribute class or available gender for color.
     * 
     * @return
     */
    public AttributeGender getAttributeGender() {

        AttributeGender result = attributeGender;
        if (!AttributeGender.isGenderSpecified(result)) {
            result = Attribute.getGenderFromAttributeClass(getAttributeClass());
        }
        if (!AttributeGender.isGenderSpecified(result)) {
            result = Attribute.getAvalibleGenderForColorAndClass(getAttributeColor(), getAttributeClass());
        }
        return result;
    }

    /**
     * Return <code>true</code> if is gender specified
     * 
     * @return <code>true</code> if is gender specified
     */
    public boolean hasSpecificGender() {
        return AttributeGender.isGenderSpecified(getAttributeGender());
    }

    public boolean hasSpecificColor() {
        return getAttributeColor() != null;
    }

    /**
     * Check class from parameter with current attribute class. Returns <code>true</code> if compared classes have same generic type.
     * 
     * @param attributeClassToCheck class to check with current class
     * @return <code>true</code> if class from parameter has same genric type as current class
     */
    public boolean isSameGenericType(Class<? extends Attribute> attributeClassToCheck) {
        Class<?> currentClass = getGenericType();
        Class<?> classToCheck = getGenericType(attributeClassToCheck);

        if ((currentClass == null) || (classToCheck == null)) {
            return false;
        }

        return currentClass.equals(classToCheck);
    }

    /**
     * Returns generic type of current attribute class
     * 
     * @return generic type of current attribute class
     */
    public Class<? extends Attribute> getGenericType() {
        return getGenericType(getAttributeClass());
    }

    /**
     * Returns <code>true</code> if class from parameter is generic
     * 
     * @param attributeClassToCheck checked attribute class
     * @return <code>true</code> if class from parameter is generic
     */
    public static boolean isClassGeneric(Class<? extends Attribute> attributeClassToCheck) {
        if (ClassReflection.isAnnotationPresent(attributeClassToCheck, CreatureGenericType.class) && ClassReflection.getDeclaredAnnotation(attributeClassToCheck, CreatureGenericType.class).getAnnotation(CreatureGenericType.class).isGeneric()) {
            return true;
        }
        return false;
    }

    /**
     * Returns <code>true</code> if class from parameter is super generic
     * 
     * @param attributeClassToCheck checked attribute class
     * @return <code>true</code> if class from parameter is super generic
     */
    public static boolean isClassSuperGeneric(Class<? extends Attribute> attributeClassToCheck) {
        if (ClassReflection.isAnnotationPresent(attributeClassToCheck, CreatureSuperGenericType.class) && ClassReflection.getDeclaredAnnotation(attributeClassToCheck, CreatureSuperGenericType.class).getAnnotation(CreatureSuperGenericType.class).isGeneric()) {
            return true;
        }
        return false;
    }

    /**
     * Returns generic type for attribute class in parameter
     * 
     * @param classToCheck attribute class to obtain generic type from
     * @return generic type for attribute class in parameter
     */
    @SuppressWarnings("unchecked")
    public static Class<? extends Attribute> getGenericType(Class<? extends Attribute> classToCheck) {
        Class<? extends Attribute> currentClass = classToCheck;
        while ((currentClass != null) && !isClassGeneric(currentClass)) {
            currentClass = (Class<? extends Attribute>) currentClass.getSuperclass();
        }

        return currentClass == null ? null : (Class<? extends Attribute>) currentClass;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            if (o instanceof AttributeDescription) {
                AttributeDescription attributeDescriptionToCheck = (AttributeDescription) o;
                return (attributeDescriptionToCheck.getAttributeClass() == attributeClass) && (attributeDescriptionToCheck.getAttributeColor() == attributeColor) && (attributeDescriptionToCheck.getAttributeGender() == getAttributeGender());
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return getAttributeClass().getSimpleName() + "[G: " + String.valueOf(getAttributeGender()) + " C: " + String.valueOf(attributeColor) + "]";
    }
}
