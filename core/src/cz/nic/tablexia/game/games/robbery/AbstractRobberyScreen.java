/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.robbery;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.Random;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.robbery.rules.GameRule;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;


public abstract class AbstractRobberyScreen extends AbstractTablexiaScreen<GameRule> {

    public RobberyGame getRobberyGame() {
        return robberyGame;
    }

    private RobberyGame robberyGame;

	public AbstractRobberyScreen(RobberyGame robberyGame) {
		this.robberyGame = robberyGame;
	}


//////////////////////////// SCREEN LIFECYCLE

    @Override
	protected String prepareScreenAtlasPath(String screenAssetsPath, String screenName) {
		// no atlas loading
		return null;
	}

	@Override
	protected String prepareScreenTextResourcesAssetName() {
		// no text loading
		return null;
	}

    @Override
    public void backButtonPressed() {
        // disabled method
    }


//////////////////////////// SCREEN PAUSE

    @Override
    public boolean isScreenPaused() {
        return robberyGame.isScreenPaused();
    }

    @Override
    protected boolean canSyncOnResume() {
        return false;
    }

//////////////////////////// ASSETS ACCESS

    @Override
    public TextureRegion getScreenTextureRegion(String regionName) {
        return robberyGame.getScreenTextureRegion(regionName);
    }

	@Override
	public Sound getSound(String soundName) {
		return robberyGame.getSound(soundName);
	}

	@Override
	public String getText(String key) {
		return robberyGame.getText(key);
	}

	@Override
	public String getFormattedText(String key, Object... args) {
		return robberyGame.getFormattedText(key, args);
	}

	protected TextureRegion getGameGlobalTextureRegion(String regionName){
	    return robberyGame.getGameGlobalTextureRegion(regionName);
    }


//////////////////////////// GAME DATA ACCESS

    protected Random getRandom() {
        return robberyGame.getRandom();
    }

    @Override
    public GameRule getData() {
        return robberyGame.getData();
    }

    protected GameDifficulty getGameDifficulty() {
        return robberyGame.getGameDifficulty();
    }

    protected int getActualCreatureNumber() {
        return robberyGame.getActualCreatureNumber();
    }

    protected void setActualCreatureNumber(int actualCreatureNumber) {
        robberyGame.setActualCreatureNumber(actualCreatureNumber);
    }

    protected GameRulesDefinition getRulesDefinition() {
        return robberyGame.getRulesDefinition();
    }


//////////////////////////// GAME CONTROL

    protected void showGameScreen() {
        robberyGame.showGameScreen(true);
	}

    protected void gameComplete() {
        robberyGame.gameComplete();

    }

    protected void startNewGame(GameRulesDefinition rulesDefinition) {
        robberyGame.setForcedRulesDefinition(rulesDefinition);
        robberyGame.startNewGame();
    }


//////////////////////////// GAME SCORE

    public void setInnocencePersons(int innocencePersons) {
        robberyGame.setInnocencePersons(innocencePersons);
    }

    public int getInnocencePersons() {
        return robberyGame.getInnocencePersons();
    }

    public void setThievesEscaped(int thievesEscaped) {
        robberyGame.setThievesEscaped(thievesEscaped);
    }

    public int getThievesEscaped() {
        return robberyGame.getThievesEscaped();
    }

    public void setThievesCaught(int thievesCaught) {
        robberyGame.setThievesCaught(thievesCaught);
    }

    public int getThievesCaught() {
        return robberyGame.getThievesCaught();
    }
}
