/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules.medium;

import java.util.Random;

import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.ClothingAttribute;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * 
 * @author Matyáš Latner
 */
public class CA_1_CCRule extends CC_1_CCRule {

    public CA_1_CCRule(Random random) {
        super(random);
    }
    
    @Override
    public GameRulesDefinition getGameRuleDefinition() {
    	return GameRulesDefinition.CA_1_CC;
    }

    @Override
    public String[] prepareRuleMessageConstituentParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[] {
                getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent1(abstractTablexiaScreen),
                getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0).getAttributeConstituent().getConstituent2(abstractTablexiaScreen)
        };
    }
    
    @Override
    public String[] prepareRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[] {
        		getAttributeColorName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0)),
        		getAttributeName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0), false),
        		getAttributeColorName(abstractTablexiaScreen, getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0))
        };
    }

    @Override
    protected void prepareCreatureDescriptionsC() {
    	AttributeColor t1CommonAttributeColor = getRandomColorFromGeneratedCreature(BAN_ATTRIBUTES_SET_FOR_GENERATING);
        CreatureDescriptor t1CreatureDescriptor = new CreatureDescriptor();
        t1CreatureDescriptor.addDescription(new AttributeDescription(t1CommonAttributeColor, null, ClothingAttribute.class));
        addGlobalCreatureDescriptor(T1_OFFSET, t1CreatureDescriptor);
    	
        CreatureDescriptor t0CreatureDescriptor = getRandomCreatureDescriptionWithOneAttribute(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, getRandom()).getCreatureDescrition());
        addGlobalCreatureDescriptor(T0_OFFSET, t0CreatureDescriptor);
    }

}
