/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.robbery.rules;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;

public enum RobberyDifficultyDefinition {

    EASY(GameDifficulty.EASY, false),
    MEDIUM(GameDifficulty.MEDIUM, false),
    HARD(GameDifficulty.HARD, false),
    BONUS(GameDifficulty.BONUS, GameDifficulty.HARD, true, 105, 4);


    public static final int DEFAULT_CREATURES_COUNT = 50;
    public static final int DEFAULT_THIEVES_COUNT   = 8;

    private GameDifficulty gameDifficulty;
    private GameDifficulty reusesDifficulty;
    private boolean endlessGeneration;
    private int forcedEndCreaturesValue;
    private int forcedEndThievsValue;

    RobberyDifficultyDefinition(GameDifficulty gameDifficulty, boolean endlessGeneration) {
        this(gameDifficulty, gameDifficulty, endlessGeneration, DEFAULT_CREATURES_COUNT - 1, 1);
    }

    RobberyDifficultyDefinition(GameDifficulty gameDifficulty, GameDifficulty reusesDifficulty, boolean endlessGeneration, int forcedEndCreaturesValue, int forcedEndThievsValue) {
        this.gameDifficulty = gameDifficulty;
        this.reusesDifficulty = reusesDifficulty;
        this.endlessGeneration = endlessGeneration;
        this.forcedEndCreaturesValue = forcedEndCreaturesValue;
        this.forcedEndThievsValue = forcedEndThievsValue;
    }

    public GameDifficulty getGameDifficulty() {
        return gameDifficulty;
    }

    public boolean isEndlessGeneration() {
        return endlessGeneration;
    }

    public GameDifficulty getReusesDifficulty() {
        return reusesDifficulty;
    }

    public int getForcedEndCreaturesValue() {
        return forcedEndCreaturesValue;
    }

    public int getForcedEndThievsValue() {
        return forcedEndThievsValue;
    }

    public static RobberyDifficultyDefinition getRobberyDifficultyDefinitionForGameDifficulty(GameDifficulty gameDifficulty) {
        for (RobberyDifficultyDefinition robberyDifficultyDefinition : RobberyDifficultyDefinition.values()) {
            if (robberyDifficultyDefinition.getGameDifficulty().equals(gameDifficulty))
                return robberyDifficultyDefinition;
        }
        return null;
    }
    
    public static String getMaxCreatures(DifficultyDefinition difficultyDefinition){
        if (difficultyDefinition.equals(DifficultyDefinition.BONUS)){
            return "inf";
        } else {
            return String.valueOf(DEFAULT_CREATURES_COUNT);
        }
    }
}
