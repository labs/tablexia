/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.top;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.robbery.assets.RobberyAssets;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot.AttributeGender;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;

public class MVestAttribute extends TopAttribute {

    private static final AttributeConstituent       ATTRIBUTE_CONSTITUENT   = AttributeConstituent.N;
    private static final List<AttributeDescription> TEXTURES                = new ArrayList<AttributeDescription>() {

        private static final long serialVersionUID = 1614881281381641153L;

        {
            add(new AttributeDescription(AttributeColor.BLUE, 	AttributeGender.MALE, RobberyAssets.ATTRIBUTE_M_VEST_BLUE, 	MVestAttribute.class));
            add(new AttributeDescription(AttributeColor.GREEN, 	AttributeGender.MALE, RobberyAssets.ATTRIBUTE_M_VEST_GREEN, 	MVestAttribute.class));
            add(new AttributeDescription(AttributeColor.GREY, 	AttributeGender.MALE, RobberyAssets.ATTRIBUTE_M_VEST_GREY, 	MVestAttribute.class));
            add(new AttributeDescription(AttributeColor.PURPLE, AttributeGender.MALE, RobberyAssets.ATTRIBUTE_M_VEST_PURPLE, 	MVestAttribute.class));
            add(new AttributeDescription(AttributeColor.RED, 	AttributeGender.MALE, RobberyAssets.ATTRIBUTE_M_VEST_RED, 	MVestAttribute.class));
            add(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, RobberyAssets.ATTRIBUTE_M_VEST_YELLOW, 	MVestAttribute.class));
        }
    };

    public static List<AttributeDescription> getTextures() {
        return TEXTURES;
    }
    
    public static AttributeGender getAttributeGender() {
		return AttributeGender.MALE;
	}
    

    public MVestAttribute(AttributeColor color, String textureName) {
        super(ATTRIBUTE_CONSTITUENT, color, textureName);
    }

}
