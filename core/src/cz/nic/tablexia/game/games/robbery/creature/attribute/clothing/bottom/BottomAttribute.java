/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.bottom;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.CreatureGenericType;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.ClothingAttribute;

@CreatureGenericType(isGeneric = true)
public class BottomAttribute extends ClothingAttribute {

    private static final AttributeConstituent   ATTRIBUTE_CONSTITUENT = AttributeConstituent.N;

    private static final int POSITION_X = 0;
    private static final float POSITION_Y = 0;
    private static final int Z_INDEX    = 0;

    public static List<AttributeDescription> getTextures() {
        return new ArrayList<AttributeDescription>() {

            private static final long serialVersionUID = 3868163242135224471L;

            {
                addAll(FPantsAttribute.getTextures());
                addAll(FSkirtAttribute.getTextures());
                addAll(MPantsAttribute.getTextures());
                addAll(MShortsAttribute.getTextures());
            }
        };
    }
    
    public static List<Class<? extends Attribute>> getAttributeClasses() {
        return new ArrayList<Class<? extends Attribute>>() {

            private static final long serialVersionUID = -1377013489500837025L;

            {
            	add(BottomAttribute.class);
                add(FPantsAttribute.class);
                add(FSkirtAttribute.class);
                add(MPantsAttribute.class);
                add(MShortsAttribute.class);
            }
        };
    }


    public BottomAttribute() {
        this(ATTRIBUTE_CONSTITUENT, null, null);
    }

    public BottomAttribute(AttributeConstituent attributeConstituent, AttributeColor color, String textureName) {
        super(attributeConstituent, color, POSITION_X, POSITION_Y, Z_INDEX, textureName);
    }

    @Override
    public Class<? extends Attribute> getGenericType() {
        return BottomAttribute.class;
    }

}
