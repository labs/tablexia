/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.robbery.creature;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.Comparator;
import java.util.Random;
import java.util.TreeSet;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.games.robbery.AbstractRobberyScreen;
import cz.nic.tablexia.game.games.robbery.RobberyGame;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.accessories.AccessoriesAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.bottom.BottomAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.headgear.HeadgearAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.top.TopAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.glasses.GlassesAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.hair.HairAttribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.head.HeadAttribute;

/**
 * Root of creature, defines creature gender
 * 
 * @author Matyáš Latner
 */
public class CreatureRoot extends Group {

    public static final int 	CREATURE_GROUP_NUMBER_NOGROUP 	= -1;

    /**
     * Creatures gender types
     */
    public enum AttributeGender {
        MALE, FEMALE, ANY;

        /**
         * Returns inversive gender for gender in parameter
         */
        public static AttributeGender getInverseveGender(AttributeGender gender) {
            switch (gender) {
                case MALE:
                    return AttributeGender.FEMALE;

                case FEMALE:
                    return AttributeGender.MALE;

                default:
                    return AttributeGender.ANY;
            }

        }

        /**
         * Returns <code>true</code> if gender attribute from parameter is specified
         * 
         * @param attributeGender gender attribute to check
         * @return <code>true</code> if gender attribute from parameter is specified
         */
        public static boolean isGenderSpecified(AttributeGender attributeGender) {
            return (attributeGender != null) && (attributeGender != ANY);
        }

        /**
         * Returns random gender
         */
        public static AttributeGender getRandomGender(Random random) {
            return random.nextBoolean() ? AttributeGender.MALE : AttributeGender.FEMALE;
        }
    }

    private Comparator<Attribute> attributeZIndexComparator = new Comparator<Attribute>() {

        @Override
        public int compare(Attribute attribute1, Attribute attribute2) {
            if (attribute1.getZ() < attribute2.getZ()) {
                return -1;
            }
            if (attribute1.getZ() > attribute2.getZ()) {
                return 1;
            }
            return 0;
        }

    };

    private boolean                	isRevealed  = false;
    private boolean                	isThief     = false;
    private int                    	groupNumber = CREATURE_GROUP_NUMBER_NOGROUP;

    protected TreeSet<Attribute>    attributes;
    private AttributeGender        	attributeGender;

    public CreatureRoot(AttributeGender attributeGender, float x, float y) {
		this.attributeGender = attributeGender;
        attributes = new TreeSet<Attribute>(attributeZIndexComparator);
        setBounds(x, y, RobberyGame.CREATURES_WIDTH, RobberyGame.CREATURES_HEIGHT);
    }

    /**
     * Generates random creature attributes for gender specified in constructor.
     * Already sets attributes are not generated again.
     * 
     * @param descriptionToBan list of banned attributes
     * @param random instance of {@link Random}
     * @return instance of creature root
     */
    public CreatureRoot generateCreature(CreatureDescriptor descriptionToBan, Random random) {
        generateAttribute(new AttributeDescription(null, attributeGender, null, BottomAttribute.class), descriptionToBan, true, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, TopAttribute.class), descriptionToBan, true, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, HeadAttribute.class), descriptionToBan, true,  random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, HairAttribute.class), descriptionToBan, true, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, HeadgearAttribute.class), descriptionToBan, true, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, GlassesAttribute.class), descriptionToBan, true, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, AccessoriesAttribute.class), descriptionToBan, true, random);

        return this;
    }

    /**
     * Returns <code>true</code> if creature is thief.
     * 
     * @return <code>true</code> if creature is thief.
     */
    public boolean isThief() {
        return isThief;
    }

    /**
     * Returns number of creature group. <code>-1</code> when creature is not in group <code>0</code> when creature is special and is not in group
     * 
     * @return <code>true</code> number of thief group.
     */
    public int getGroupNumber() {
        return groupNumber;
    }

    /**
     * Returns <code>true</code> when current creature is thief and is revealed
     * 
     * @return <code>true</code> when current creature is thief and is revealed
     */
    public boolean isRevealed() {
        return isRevealed;
    }
    
    public void setRevealed(boolean isRevealed) {
		this.isRevealed = isRevealed;
	}

    /**
     * Set creature as thief.
     */
    public void setAsThief(boolean isThief) {
        this.isThief = isThief;
    }

    /**
     * Set number of creature group. <code>-1</code> when creature is not in group <code>0</code> when creature is special and is not in group
     * 
     * @param groupNumber number of creature group or <code>0</code> or <code>-1</code>
     */
    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    /**
     * Generate new creature attribute. If creature attribute already exists do nothing.
     * 
     * @param attributeDescriptionForce forced attribute
     * @param descriptionToBan list of banned attributes
     * @param generateNull <code>true</code> for generating empty attributes
     * @param random instance of {@link Random}
     */
    public boolean generateAttribute(AttributeDescription attributeDescriptionForce, CreatureDescriptor descriptionToBan, boolean generateNull, Random random) {
        Attribute attribute = Attribute.getRandomAttribute(getAttributeGender(), attributeDescriptionForce, descriptionToBan, generateNull, random);
        if ((attribute != null) && !hasAttributeGenericType(attribute.getGenericType())) {
            attachAttribute(attribute);
            return true;
        }
        return false;
    }

    /**
     * Checks if attribute with generic type is already attached
     */
    private boolean hasAttributeGenericType(Class<? extends Attribute> genericAttributeType) {
        for (Attribute attribute : attributes) {
            if (attribute.getGenericType().equals(genericAttributeType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Attaches attribute and sprite
     */
    public void attachAttribute(Attribute attributCreature) throws IllegalStateException {
        if (attributCreature != null) {
            attributes.add(attributCreature);
        }
    }

    /**
     * Detaches attribute and sprite
     */
    public boolean detachAttribute(Attribute attributCreature) {
        if (attributCreature != null) {
            attributes.remove(attributCreature);
        }
        return false;
    }

    /**
     * Check if current creature has all attributes from parameter
     */
    public boolean hasAttributes(CreatureDescriptor creatureDescriptor) {
        for (AttributeDescription attributeDescription : creatureDescriptor.getDescriptions()) {
            if (!hasAttribute(attributeDescription)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if current creature has attribute from parameter
     */
    public boolean hasAttribute(AttributeDescription attributeToCheck) {
        for (Attribute attribute : attributes) {
            if (attributeToCheck.getAttributeClass().equals(attribute.getClass()) || Attribute.isClassGenericType(attribute.getClass(), attributeToCheck.getAttributeClass())) {

                if ((attributeToCheck.getAttributeColor() == null) || attributeToCheck.getAttributeColor().equals(attribute.getAttributeColor())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns <code>true</code> if creature has any attribute with specific color
     */
    public boolean hasColor(AttributeColor color) {
        if (color != null) {
            for (Attribute attribute : attributes) {
                if (color.equals(attribute.getAttributeColor())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns current attributes property
     */
    public AttributeGender getAttributeGender() {
        return attributeGender;
    }

    /**
     * Returns attribute for specific attribute type in parameter if exists
     * 
     * @return attribute for specific type else <code>null</code>
     */
    public Attribute getAttributeForType(Class<? extends Attribute> attributeTypeClass) {
        for (Attribute attribute : attributes) {
            if (attribute.getAttributeDescription().getAttributeClass().equals(attributeTypeClass) || (AttributeDescription.isClassGeneric(attributeTypeClass) && AttributeDescription.getGenericType(attribute.getAttributeDescription().getAttributeClass()).equals(attributeTypeClass))) {
                return attribute;
            }
        }
        return null;
    }

    /**
     * Returns current creatures description
     * 
     * @return description object for current creature
     */
    public CreatureDescriptor getCreatureDescrition() {
        CreatureDescriptor creatureDescriptor = new CreatureDescriptor();
        for (Attribute attribute : attributes) {
            creatureDescriptor.addDescription(new AttributeDescription(attribute));
        }
        creatureDescriptor.setThief(isThief);
        return creatureDescriptor;
    }
    

    @Override
    public String toString() {
        return getCreatureDescrition().toString();
    }
    
    public void highliteWithColor(AbstractRobberyScreen robberyScreen, Color highliteColor) {
        addActor(new Image(robberyScreen.getColorTextureRegion(highliteColor)));
	}

    public void loadAssets(AbstractTablexiaGame abstractTablexiaGame) {
        for (Attribute attribute: attributes) {
            Actor newAttribute = new Image(abstractTablexiaGame.getScreenTextureRegion(attribute.getTextureName())); 
            newAttribute.setPosition(RobberyGame.CREATURES_WIDTH/2 - newAttribute.getWidth()/2,attribute.getY()*RobberyGame.CREATURES_HEIGHT);
            addActor(newAttribute);
        }
    }

}
