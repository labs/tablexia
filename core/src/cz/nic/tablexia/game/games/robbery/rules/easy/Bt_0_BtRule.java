/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.robbery.rules.easy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.games.robbery.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.robbery.creature.CreatureFactory;
import cz.nic.tablexia.game.games.robbery.creature.CreatureRoot;
import cz.nic.tablexia.game.games.robbery.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.robbery.creature.attribute.AttributeDescription;
import cz.nic.tablexia.game.games.robbery.creature.attribute.clothing.top.TopAttribute;
import cz.nic.tablexia.game.games.robbery.rules.GameRule;
import cz.nic.tablexia.game.games.robbery.rules.GameRulesDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * 
 * @author Matyáš Latner
 */
public class Bt_0_BtRule extends GameRule {
    
	private   static final int 	 	GROUP_SIZE	= 2;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;
	protected 			   Integer 	T1_OFFSET 	= Integer.valueOf(1);
	
	protected Attribute attribute;

    public Bt_0_BtRule(Random random) {
        super(random, GROUP_SIZE);
    }
    
    public Bt_0_BtRule(Random random, int groupSize) {
        super(random, groupSize);
    }
    
    @Override
    public GameRulesDefinition getGameRuleDefinition() {
    	return GameRulesDefinition.Bt_0_Bt;
    }

	@Override
	public String[] prepareRuleMessageConstituentParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
		return new String[] {
				attribute.getAttributeConstituent().getConstituent3(abstractTablexiaScreen)
		};
	}

	@Override
    public String[] prepareRuleMessageParameters(AbstractTablexiaScreen abstractTablexiaScreen) {
        return new String[] {
        		getAttributeName(abstractTablexiaScreen, attribute.getClass(), false)
        };
    }
    
    @Override
    protected boolean checkSpecialCreaturePosition(int positionToTry, Integer creatureOffset) {
    	if (creatureOffset == T1_OFFSET) {
    		CreatureDescriptor creatureToCheck = specialCreatures.get(positionToTry - T1_OFFSET);
    		if (creatureToCheck != null && creatureToCheck.isThief() && creatureToCheck.getDescriptions().get(0).equals(getGlobalCreatureDescriptor(creatureOffset).getDescriptions().get(0))) {
    			return false;
    		}
    	}
    	
    	if (creatureOffset == T0_OFFSET) {
    		CreatureDescriptor creatureToCheck = specialCreatures.get(positionToTry + T1_OFFSET);
    		if (creatureToCheck != null && !creatureToCheck.isThief() && creatureToCheck.getDescriptions().get(0).equals(getGlobalCreatureDescriptor(creatureOffset).getDescriptions().get(0))) {
    			return false;
    		}
    	}
    	
    	return true;
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
        addGlobalCreatureDescriptor(T1_OFFSET, new CreatureDescriptor());
        addGlobalCreatureDescriptor(T0_OFFSET, new CreatureDescriptor());
        attribute = new TopAttribute();
    }
    
    @Override
    protected void prepareCreatureDescriptionsB() {
    	AttributeDescription commonAttributeDescription = CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, getRandom()).getAttributeForType(attribute.getClass()).getAttributeDescription();
    	
    	getGlobalCreatureDescriptor(T1_OFFSET).clearDescriptions().addDescription(commonAttributeDescription);
    	getGlobalCreatureDescriptor(T0_OFFSET).clearDescriptions().addDescription(commonAttributeDescription);
    }
    
    @Override
    protected List<CreatureRoot> prepareCreatureDescriptionsA(int numberOfCreatures) {
    	List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        for (int i = 0; i < numberOfCreatures; i++) {
            
            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
            if (creatureDescriptor != null) { // add special creature
            	creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, null, getRandom()));
			} else {
            	CreatureDescriptor creatureDescriptorToBan = new CreatureDescriptor();
            	creatureDescriptorToBan.disableGenderCompatibilityCheck();
            	
            	CreatureDescriptor nextSpecialCreature = specialCreatures.get(i + T1_OFFSET);
				if (nextSpecialCreature != null) {
            		creatureDescriptorToBan.addDescription(nextSpecialCreature.getDescriptions().get(0));            		
            	}
				
				int lastPosition = i - T1_OFFSET;
				if (lastPosition >= 0) {					
					CreatureRoot lastCreature = creatures.get(lastPosition);
					if (lastCreature != null) {
						Attribute attributeToBan = lastCreature.getAttributeForType(attribute.getClass());
						if (attributeToBan != null) {							
							creatureDescriptorToBan.addDescription(attributeToBan.getAttributeDescription());
						}
					}
				}
				
				creatures.add(CreatureFactory.getInstance().generateCreature(null, creatureDescriptorToBan, getRandom()));
			}
        }

        return creatures;
    }

}
