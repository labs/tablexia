/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.night_watch;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import net.dermetfan.utils.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.night_watch.assets.NightWatchAssets;
import cz.nic.tablexia.game.games.night_watch.helper.GameRulesHelper;
import cz.nic.tablexia.game.games.night_watch.helper.TextureHelper;
import cz.nic.tablexia.game.games.night_watch.model.NightWatchDifficulty;
import cz.nic.tablexia.game.games.night_watch.model.WindowTypeDefinition;
import cz.nic.tablexia.game.games.night_watch.solution.GameSolutionGenerator;
import cz.nic.tablexia.game.games.night_watch.solution.Solution;
import cz.nic.tablexia.game.games.night_watch.subscene.Watch;
import cz.nic.tablexia.game.common.ui.health_bar.HealthBar;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaTextureManager;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.resolvers.NightWatchScoreResolver;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Utility;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.button.GameImageTablexiaButton;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

/**
 * Created by Václav Tarantík on 6.3.15.
 */
public class NightWatchGame extends AbstractTablexiaGame<int[][]> {
    public static final String COMPLETE_BUTTON          = "complete button";
    public static final String WATCH                    = "watch";
    public static final String WINDOWS_GROUP            = "windows group";
    public static final String STACK_WINDOWS            = "stack windows";
    public static final String WINDOW                   = "window";

    public static final String EVENT_NEW_DAY            = "new day";
    public static final String EVENT_NEW_NIGHT          = "new night";
    public static final String EVENT_WINDOW_SELECTED    = "window selected: ";
    public static final String EVENT_EVALUATE_ROUND      = "evaluate round: ";

    private enum ResultMapping {

        NO_STAR_TEXT	(GameResult.NO_STAR, 	NightWatchAssets.VICTORYTEXT_NOSTAR, 		NightWatchAssets.VICTORYSPEECH_NOSTAR),
        ONE_STAR_TEXT	(GameResult.ONE_STAR, 	NightWatchAssets.VICTORYTEXT_ONESTAR, 		NightWatchAssets.VICTORYSPEECH_ONESTAR),
        TWO_STAR_TEXT	(GameResult.TWO_STAR, 	NightWatchAssets.VICTORYTEXT_TWOSTARS, 	    NightWatchAssets.VICTORYSPEECH_TWOSTAR),
        THREE_STAR_TEXT	(GameResult.THREE_STAR, NightWatchAssets.VICTORYTEXT_THREESTARS,	NightWatchAssets.VICTORYSPEECH_THREESTAR);

        private final GameResult gameResult;
        private final String textKey;
        private final String soundName;

        ResultMapping(GameResult gameResult, String textKey, String soundName) {
            this.gameResult = gameResult;
            this.textKey = textKey;
            this.soundName = soundName;
        }

        public String getTextKey() {
            return textKey;
        }

        public String getSoundName() {
            return soundName;
        }

        public static ResultMapping getResultTextMappingForGameResult(GameResult gameResult) {
            for (ResultMapping resultMapping : ResultMapping.values()) {
                if (resultMapping.gameResult.equals(gameResult)) {
                    return resultMapping;
                }
            }
            return null;
        }
    }
    private static final int   SCREEN_WIDTH                 = TablexiaSettings.getWorldSize();
    private static final int   SCREEN_MIN_HEIGHT            = TablexiaSettings.getMinWorldHeight();
    private static final int   INITIAL_BG_TEXTURE_INDEX     = 0;
    private static final float SOLUTION_VISIBLE_SECONDS     = 2.0f;
    private static final float SOLUTION_DELAY               = 0.5f;
    private static final float TRANSITION_DELAY             = 2;
    private static final float TRANSITION_ANIMATION_DELAY   = 0.2f;
    private static final float TRANSITION_ANIMATION_FADE_IN = 0.2f;

    public static final int   MAX_GAME_ERRORS               = 2;

    private static final int WINDOW_POSITION_Y_OFFSET       = 38;


    private static final String SUMMARY_TEXT_ROUNDS_KEY     = "game_nightwatch_victory_text_rounds";
    private static final String SCORE_ROUND_COUNT           = NightWatchScoreResolver.SCORE_ROUND_COUNT;

    private static final int WATCH_WIDTH    = 300;
    private static final int BUTTON_X       = 800;
    private static final int BUTTON_Y       = 20;
    private static final int BUTTON_WIDTH   = 150;
    private static final int BUTTON_HEIGHT  = 80;

    private int roundErrors = 0;
    private boolean userPlaying = false;
    private int numberOfSelectedWindows;
    private HashMap<Integer, Actor> selectedWindows;

    private static int currentRound;
    private Group windowsGroup;
    private Group contentGroup;
    private Stack backgroundStack;
    private List<Solution> gameSolution;
    private Watch watch;
    private GameImageTablexiaButton button;
    private Music backgroundSound;
    private String actualBackgroundSound;
    private Image backgroundImage;

    private HealthBar healthBar;

    private Texture clickMap;
    private String clickMapName;
    private TablexiaTextureManager textureManager;
    private int clickMapWidth;
    private int clickMapHeight;

    //For test
    public  static Solution lastSolution = null;
    private int testCountEvent = -1;

    @Override
    protected void gameLoaded(Map<String, String> gameState) {
        setGameScore(SCORE_ROUND_COUNT, 0);
        currentRound = 0;
        roundErrors = 0;
        contentGroup = new Group();
        contentGroup.setSize(getSceneWidth(), getSceneInnerHeight());
        contentGroup.setPosition(getSceneLeftX(), getSceneInnerBottomY());
        getStage().addActor(contentGroup);

        gameSolution = GameSolutionGenerator.generateSolutions(getGameDifficulty(), getRandom());
        selectedWindows = new HashMap<>();

        prepareBackground();
        prepareWindows();
        prepareWatch();
        prepareHealthBar();
        prepareButton();
    }

    @Override
    protected void gameVisible() {
        triggerScenarioStepEvent(EVENT_GAME_READY);
        startRound();
    }

    @Override
    protected int[][] prepareGameData(Map<String, String> gameState) {
        if(getGameDifficulty() == GameDifficulty.BONUS) clickMapName = prepareScreenAssetsPath(prepareScreenName())+ "excluded/clickmap_hard" + ".png";
        else clickMapName = prepareScreenAssetsPath(prepareScreenName())+ "excluded/clickmap_" + getGameDifficulty().name().toLowerCase() + ".png";
        final Object loaderLock = new Object();
        synchronized (loaderLock) {

            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    synchronized (loaderLock) {
                        tryToDisposeTextureManager();
                        textureManager = new TablexiaTextureManager(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL);
                        textureManager.loadTexture(clickMapName);
                        textureManager.finishLoading();
                        loaderLock.notify();
                    }
                }
            });

            try {
                loaderLock.wait();
            } catch (InterruptedException e) {
                Log.err(getClass(), "Error while waiting to texture loader!", e);
            }
        }

        clickMap = textureManager.getTexture(clickMapName);
        if (!clickMap.getTextureData().isPrepared()) {
            clickMap.getTextureData().prepare();
        }
        clickMapWidth = clickMap.getWidth();
        clickMapHeight = clickMap.getHeight();
        return Utility.createColorMap(clickMap);

    }

    private void tryToDisposeTextureManager() {
        if (textureManager != null) {
            textureManager.dispose();
            textureManager = null;
        }
    }

    private void prepareBackground() {
        backgroundStack = new Stack();
        TextureRegion backgroundTexture = getScreenTextureRegion(TextureHelper.getBackgroundTexturesNames().get(INITIAL_BG_TEXTURE_INDEX));
        backgroundImage = new Image(backgroundTexture);

        backgroundStack.setName(STACK_WINDOWS);
        backgroundStack.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                final Integer touchedWindowIndex = TextureHelper.colorMap.get(getTouchedColor(x, y));
                if (touchedWindowIndex != null) {
                    int maxNumberOfWindows = NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(getGameDifficulty()).getNumberOfWindowsLightenedInCurrentRound(currentRound);
                    if(selectedWindows.containsKey(touchedWindowIndex)) {
                        Actor actor = selectedWindows.get(touchedWindowIndex);
                        actor.remove();
                        selectedWindows.remove(touchedWindowIndex);
                        numberOfSelectedWindows--;
                        NightWatchGame.this.button.setVisible(false);
                    }
                    else {
                        if (numberOfSelectedWindows < maxNumberOfWindows) {
                            lightUpWindow(touchedWindowIndex);
                            triggerScenarioStepEvent(EVENT_WINDOW_SELECTED+touchedWindowIndex);
                        }
                    }

                    if (numberOfSelectedWindows == maxNumberOfWindows) {
                        NightWatchGame.this.button.setVisible(true);
                        NightWatchGame.this.button.setChecked(false);
                    }
                }
                return true;
            }
        });
        backgroundStack.addActor(backgroundImage);
        contentGroup.addActor(backgroundStack);

        setActorToFullScene(backgroundStack);
    }

    private void prepareWatch() {
        watch = new Watch(WATCH_WIDTH, getScreenTextureRegion(NightWatchAssets.WATCH), getScreenTextureRegion(NightWatchAssets.HOUR_HAND), getScreenTextureRegion(NightWatchAssets.MINUTE_HAND), getScreenTextureRegion(NightWatchAssets.WATCH_ONLY), getSound(NightWatchAssets.SOUND_WATCH_1));
        watch.setPosition(getSceneWidth() / 2 - watch.getWidth() / 2, getViewportBottomY());
        watch.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
        watch.setName(WATCH);
        contentGroup.addActor(watch);
    }

    private void prepareHealthBar() {
        healthBar = new HealthBar(getGameGlobalTextureRegion(HEART_FULL),
                getGameGlobalTextureRegion(HEART_BROKEN),
                getColorTextureRegion(HealthBar.BACKGROUND_COLOR),
                MAX_GAME_ERRORS);

        healthBar.setHealthBarDefaultPosition(this);
        contentGroup.addActor(healthBar);
    }

    private void prepareWindows() {
       windowsGroup = new Group();
        windowsGroup.setName(WINDOWS_GROUP);
       contentGroup.addActor(windowsGroup);
    }

    private void prepareButton() {
        String buttonText = getText(NightWatchAssets.BUTTON_TEXT);

        button = new GameImageTablexiaButton(buttonText, new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_YES_ICON)));
        button.setButtonBounds(BUTTON_X, getViewportBottomY() + BUTTON_Y, BUTTON_WIDTH, BUTTON_HEIGHT);
        button.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                userPlaying = false;
                button.setVisible(false);
                //disabling background so windows cannot be clicked until
                enableClickables(false);
                addAction(Actions.sequence(evaluate(), Actions.delay(SOLUTION_VISIBLE_SECONDS), resetScene(), Actions.delay(SOLUTION_DELAY), saveScore(), decideWhatFollows(),Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_EVALUATE_ROUND + testCountEvent);
                    }
                })));
            }
        });
        button.setVisible(false);
        button.setName(COMPLETE_BUTTON);
        contentGroup.addActor(button);
    }

    //initial animation
    private void startRound() {
        backgroundStack.clearChildren();
        backgroundStack.addActor(backgroundImage);
        enableClickables(false);
        playBackgroundSound(NightWatchAssets.SOUND_DAY);
        float currentLidTime = GameRulesHelper.getCurrentLidTime(NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(getGameDifficulty()), currentRound);
        addAction(Actions.sequence(Actions.delay(TRANSITION_DELAY), animateDaytimeTransition(true), Actions.delay(TRANSITION_DELAY), showSolution(),
                Actions.delay(currentLidTime), resetScene(), animateDaytimeTransition(false), play()));
    }

    //shows generated solution for this round
    private RunnableAction showSolution() {
        return new RunnableAction() {
            @Override
            public void run() {
                //Log.info(getClass(), "SHOW SOLUTION");

                Solution currentSolution = gameSolution.get(currentRound);
               // Log.info(getClass(), "Solution: ");
                for (int i = 0; i < currentSolution.getWindowSequence().size(); i++) {
                   // Log.info(getClass(),currentSolution.getWindowSequence().get(i).toString());
                    lightUpWindow(currentSolution.getWindowSequence().get(i));
                    lightUpWindow(currentSolution.getWindowSequence().get(i), NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(getGameDifficulty()).getTargetTypeDefinition());
                }

                for (int i = 0; i < currentSolution.getDistractionWindowSequence().size(); i++) {
                    lightUpWindow(currentSolution.getDistractionWindowSequence().get(i), WindowTypeDefinition.EMPTY);
                }
                //Log.info(getClass(), "TIME: " + currentSolution.getTime());
                watch.setTime(currentSolution.getTime());

                lastSolution = gameSolution.get(currentRound);
            }
        };
    }

    private RunnableAction resetScene() {
        return new RunnableAction() {
            @Override
            public void run() {
               // Log.info(getClass(), "RESET SCENE");
                selectedWindows.clear();
                resetWindows();
                watch.resetWatch();
            }
        };
    }

    private RunnableAction play() {
        return new RunnableAction() {
            @Override
            public void run() {
               // Log.info(getClass(), "PLAY");
                userPlaying = true;
                enableClickables(true);
            }
        };
    }

    private RunnableAction evaluate() {
        return new RunnableAction() {
            @Override
            public void run() {
                //Log.info(getClass(), "EVALUATE");
                int userSetTime = watch.getCurrentTime();
                List<Integer> windows = new ArrayList<>(selectedWindows.keySet());

                Solution correctSolution = gameSolution.get(currentRound);
                boolean correct = correctSolution.check(windows, userSetTime);

                if(!correctSolution.checkWindows(windows) || !correctSolution.checkTime(userSetTime)) {
                    roundErrors++;
                    healthBar.hide();
                }
                //Log.info(getClass(), "CORRECT: " + correct);

                for (int i = 0; i < correctSolution.getWindowSequence().size(); i++) {
                    lightUpWindowWithColor(correctSolution.getWindowSequence().get(i), NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(getGameDifficulty()).getTargetTypeDefinition(), Color.GREEN);
                }

                watch.showCorrectTime(correctSolution.getTime());

                if (!correct) {
                    for (int i = 0; i < windows.size(); i++) {
                        if (!correctSolution.getWindowSequence().contains(windows.get(i))) {
                            lightUpWindowWithColor(windows.get(i), WindowTypeDefinition.EMPTY, Color.RED);
                        }
                    }
                    if (correctSolution.getTime() != userSetTime) {
                        watch.showWrongTime(userSetTime);
                    }
                }
            }
        };
    }

    private RunnableAction animateDaytimeTransition(final boolean dayToNight) {
        return new RunnableAction() {
            @Override
            public void run() {
                List<String> dayTimeAnimationTextures = TextureHelper.getDaytimeTransitionTextureNames(dayToNight);
                for (int i = 1; i < dayTimeAnimationTextures.size(); i++) {
                    Image replacingImage = new Image(getScreenTextureRegion(dayTimeAnimationTextures.get(i)));
                    replacingImage.setSize(SCREEN_WIDTH, SCREEN_MIN_HEIGHT);
                    replacingImage.getColor().a = 0f;
                    backgroundStack.addActor(replacingImage);
                    final int animatedCnt = i; //for test
                    replacingImage.addAction(Actions.sequence(Actions.delay(i * TRANSITION_ANIMATION_DELAY), Actions.fadeIn(TRANSITION_ANIMATION_FADE_IN),Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            if(animatedCnt == dayTimeAnimationTextures.size()-1) {
                                if (!dayToNight) {
                                    AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_NEW_DAY + testCountEvent);
                                } else {
                                    AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_NEW_NIGHT + testCountEvent);
                                    testCountEvent++;
                                }
                            }
                        }
                    })));
                }
                playBackgroundSound(dayToNight ? NightWatchAssets.SOUND_NIGHT : NightWatchAssets.SOUND_DAY);
                playRandomTransitionSound(dayToNight);
            }
        };
    }

    private RunnableAction saveScore() {
        return new RunnableAction() {
            @Override
            public void run() {
                setGameScore(SCORE_ROUND_COUNT, ++currentRound);
            }
        };
    }

    private RunnableAction decideWhatFollows() {
        return new RunnableAction() {
            @Override
            public void run() {
                if (currentRound < NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(getGameDifficulty()).getNumberOfRounds() && roundErrors < MAX_GAME_ERRORS) {
                    startRound();
                } else {
                    gameComplete();
                }
            }
        };
    }

    private Image lightUpWindow(final int index) {
        return lightUpWindowWithColor(index, WindowTypeDefinition.EMPTY, null);
    }

    private Image lightUpWindow(final int index, WindowTypeDefinition windowTypeDefinition) {
        return lightUpWindowWithColor(index, windowTypeDefinition, null);
    }

    private Image lightUpWindowWithColor(final int index, WindowTypeDefinition windowTypeDefinition, Color color) {
        List<Pair<Integer, Integer>> windowPositions = NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(getGameDifficulty()).getWindowsPositions();
        final Image windowImage = new Image(getScreenTextureRegion(windowTypeDefinition.getAssetsFolderName() + index));
        if (color != null) {
            windowImage.setColor(color);
        }
        windowImage.setName(WINDOW+index);
        windowImage.setPosition(windowPositions.get(index).getKey(), windowPositions.get(index).getValue() - WINDOW_POSITION_Y_OFFSET);


        windowImage.setTouchable(Touchable.disabled);
        selectedWindows.put(index, windowImage);
        windowsGroup.addActor(windowImage);
        windowsGroup.setTouchable(Touchable.disabled);
        //TODO add reset scene action here

        //user is playing, increment number of lid windows
        int maxNumberOfWindows = NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(getGameDifficulty()).getNumberOfWindowsLightenedInCurrentRound(currentRound);
        numberOfSelectedWindows++;
        if (numberOfSelectedWindows == maxNumberOfWindows && userPlaying) {
            button.setVisible(true);
        }

        return windowImage;
    }

    //method enables or disables all clickables so it cannot be clicked while animating or during screen transitions
    private void enableClickables(boolean enable) {
        Touchable touchable;
        if (enable) {
            touchable = Touchable.enabled;
        } else {
            touchable = Touchable.disabled;
        }
        backgroundStack.setTouchable(touchable);
        watch.enable(enable);
    }

    private void resetWindows() {
        windowsGroup.clearChildren();
        numberOfSelectedWindows = 0;
    }

    private Color getTouchedColor(float x, float y) {
        int clickX = (int) (x / getSceneWidth() * clickMapWidth);
        int clickY = clickMapHeight - (int) (y / getSceneOuterHeight() * clickMapHeight);

        if (clickY < 0 || clickY > clickMapHeight) {
            return Color.BLACK;
        }

        return new Color(getData()[clickX][clickY]);
    }

    private void playBackgroundSound(String name) {
        // don't play again background sound, which is actually playing
        if (actualBackgroundSound != null && actualBackgroundSound.equals(name) && backgroundSound.isPlaying()) {
            return;
        }

        if (backgroundSound != null && backgroundSound.isPlaying()) {
            backgroundSound.stop();
        }

        backgroundSound = getMusic(name);
        backgroundSound.setLooping(true);
        backgroundSound.play();
        actualBackgroundSound = name;
    }

    private void playRandomTransitionSound(boolean dayToNight) {
        List<String> sounds = dayToNight ? NightWatchAssets.getEveningTransitionSounds() : NightWatchAssets.getMorningTransitionSounds();
        getSound(sounds.get(getRandom().nextInt(sounds.size()))).play();
    }

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(new SummaryMessage(SummaryImage.STATS, getFormattedText(SUMMARY_TEXT_ROUNDS_KEY, game.getGameScore(SCORE_ROUND_COUNT, "0") + " / " +
                NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(getGameDifficulty()).getNumberOfRounds())));
    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        ResultMapping mapping = ResultMapping.getResultTextMappingForGameResult(gameResult);
        if (mapping != null) {
            return mapping.getTextKey();
        }

        return null;
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        ResultMapping mapping = ResultMapping.getResultTextMappingForGameResult(gameResult);
        if (mapping != null) {
            return mapping.getSoundName();
        }
        return null;
    }

    @Override
    protected void screenResized(int width, int height) {
        if (watch != null) {
            watch.setY(getViewportBottomY());
        }

        if (button != null) {
            button.setY(getViewportBottomY() + BUTTON_Y);
        }
    }

    @Override
    protected void gameDisposed() {
        if (backgroundSound != null) {
            backgroundSound.stop();
            backgroundSound.dispose();
        }
        tryToDisposeTextureManager();
        super.gameDisposed();
    }

    @Override
    protected void prepareGameSoundAssetNames(List<String> soundsFileNames) {
        soundsFileNames.addAll(NightWatchAssets.getSoundsToLoad());
    }

    @Override
    protected void prepareScreenAtlases(List<String> atlasesNames) {
        //Bonus difficulty uses hard difficulty assets
        if (getGameDifficulty().equals(GameDifficulty.BONUS)) atlasesNames.add(getGameDifficultyAtlasPath(GameDifficulty.HARD));
        super.prepareScreenAtlases(atlasesNames);
    }

    @Override
    public TextureRegion getScreenTextureRegion(String regionName) {
        TextureRegion textureRegion = getTextureRegionForAtlas(getGameDifficultyAtlasPath(NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(getGameDifficulty()).getReusesGameDifficulty()), regionName, null, false);
        if (textureRegion == null) {
            textureRegion = super.getScreenTextureRegion(regionName, null);
        }
        return textureRegion;
    }

    @Override
    public void gameComplete() {
        if (backgroundSound != null) {
            backgroundSound.stop();
        }
        super.gameComplete();
    }

    @Override
    protected void gamePaused(Map<String, String> gameState) {
        if (backgroundSound != null) {
            backgroundSound.pause();
        }
    }

    @Override
    protected void gameResumed() {
        if (backgroundSound != null) {
            backgroundSound.play();
        }
    }


//////////////////////////////////////////// PRELOADER

    private static final String PRELOADER_ANIM_IMAGE            = "preloader_anim";
    private static final int    PRELOADER_ANIM_FRAMES           = 19;
    private static final float  PRELOADER_ANIM_FRAME_DURATION   = 0.5f;
    private static final String PRELOADER_TEXT_KEY             	= ApplicationTextManager.ApplicationTextsAssets.GAME_NIGHT_WATCH_PRELOADER_TEXT;
    private static final String PRELOADER_BONUS_TEXT_KEY        = ApplicationTextManager.ApplicationTextsAssets.GAME_NIGHT_WATCH_PRELOADER_BONUS_TEXT;
    private static final Color  PRELOADER_TEXT_COLOR            = Color.DARK_GRAY;

    private static final Scaling        PRELOADER_IMAGE_SCALING 		    = Scaling.fit;
    private static final int 			PRELOADER_TEXT_ALIGN 				= Align.left;
    private static final float 			PRELOADER_TEXT_PADDING 				= 10f;
    private static final float 			PRELOADER_LEFT_COLUMN_WIDTH_RATIO 	= 1f / 4;
    private static final float 			PRELOADER_RIGHT_COLUMN_RATIO 		= 5f / 8;
    private static final float 			PRELOADER_ROW_HEIGHT 				= 1f / 3;

    @Override
    protected String preparePreloaderSpeechFileBaseName() {
        return getGameDifficulty().equals(GameDifficulty.BONUS) ? super.preparePreloaderSpeechFileBaseName() + prepareDifficultySuffix() : super.preparePreloaderSpeechFileBaseName();
    }

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        AnimatedImage preloaderImage = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_IMAGE, PRELOADER_ANIM_FRAMES, PRELOADER_ANIM_FRAME_DURATION), false);
        preloaderImage.startAnimationLoop();
        String preloaderText = getGameDifficulty().equals(GameDifficulty.BONUS) ? preloaderAssetsManager.getText(PRELOADER_BONUS_TEXT_KEY) : preloaderAssetsManager.getText(PRELOADER_TEXT_KEY);
        components.add(new TwoColumnContentDialogComponent(
                new AnimatedImageContentDialogComponent(preloaderImage, PRELOADER_IMAGE_SCALING),
                new TextContentDialogComponent(preloaderText, PRELOADER_TEXT_COLOR, PRELOADER_TEXT_ALIGN, PRELOADER_TEXT_PADDING),
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_ROW_HEIGHT));
    }

    @Override
    public boolean preloaderHasSpeech() {
        return true;
    }
}
