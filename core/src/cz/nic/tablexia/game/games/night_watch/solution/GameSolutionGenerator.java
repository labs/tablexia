/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.night_watch.solution;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.night_watch.helper.GameRulesHelper;
import cz.nic.tablexia.game.games.night_watch.model.NightWatchDifficulty;

/**
 * Created by Václav Tarantík on 13.5.15.
 * Edited by danilov on 3.8.16.
 */
public class GameSolutionGenerator {

   public static List<Solution> generateSolutions(GameDifficulty gameDifficulty, Random random) {
       NightWatchDifficulty nightWatchDifficulty = NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(gameDifficulty);
       int numberOfRounds = nightWatchDifficulty.getNumberOfRounds();
       List<Solution> currentLevelSolutions = new ArrayList<Solution>();
       for(int i = 0; i < numberOfRounds; i++) {
           int totalWindows = nightWatchDifficulty.getTotalNumberOfWindows();
           int lightenedWindows = nightWatchDifficulty.getNumberOfWindowsLightenedInCurrentRound(i);
           int windowsWithShadow = nightWatchDifficulty.getNumberOfWindowsShadowedInCurrentRound(i);
           List<Integer> generatedSequence = PropertiesGenerator.getRandomWindowSequence(totalWindows, lightenedWindows, random); 
           Solution currentRoundSolution = new Solution(generatedSequence,  PropertiesGenerator.getRandomDistractionSequence(windowsWithShadow, totalWindows, generatedSequence, random), PropertiesGenerator.getRandomTime(random));
           currentLevelSolutions.add(currentRoundSolution);
       }
        return currentLevelSolutions;
    }
}
