/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.night_watch.subscene;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.util.Log;

/**
 * Created by Václav Tarantík on 4.5.15.
 */
public class Watch extends Group {
    private static final int    HOUR_HAND_Y_OFFSET      = 25;
    private static final int    MINUTE_HAND_Y_OFFSET    = 29;
    private static final float  TIME_DEGREE_TOLERANCE   = 10f;
    private static final int    ANGLE_DEGREE_OFFSET     = 90;

    private float width;
    private float height;

    private Image hourHandImage;
    private Image correctTimeHand;
    private Image wrongTimeHand;
    private Image watchOnlyImage;

    private TextureRegion backgroundTexture;
    private TextureRegion hourHandTexture;
    private TextureRegion minuteHandTexture;
    private TextureRegion watchOnlyTexture;

    private Sound watchHandSound;

    //setting only width, height is calculated according to original texture size ratio
    public Watch(float width, TextureRegion backgroundTexture, TextureRegion hourHandTexture, TextureRegion minuteHandTexture, TextureRegion watchOnlyTexture, Sound watchHandSound) {
        this.backgroundTexture = backgroundTexture;
        this.hourHandTexture = hourHandTexture;
        this.minuteHandTexture = minuteHandTexture;
        this.watchHandSound = watchHandSound;
        this.watchOnlyTexture = watchOnlyTexture;

        this.width = width;
        float backgroundTextureRatio = (float)backgroundTexture.getRegionHeight() / (float)backgroundTexture.getRegionWidth();
        this.height = width*backgroundTextureRatio;

        setSize(width, height);

        prepareBackground();
        prepareHands();

    }

    private void prepareBackground() {
        Image bgImage = new Image(backgroundTexture);
        watchOnlyImage = new Image(watchOnlyTexture);
        final float ratio = getWidth() / bgImage.getWidth();

        watchOnlyImage.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                Vector2 touchPoint = new Vector2(x, y);
                Vector2 rotationCenter = new Vector2(watchOnlyImage.getWidth() / 2, watchOnlyImage.getHeight() / 2);
                float newAngle = rotationCenter.sub(touchPoint).angle() + ANGLE_DEGREE_OFFSET;
                hourHandImage.setRotation(newAngle);
                return true;
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                Vector2 touchPoint = new Vector2(x, y);
                Vector2 rotationCenter = new Vector2(watchOnlyImage.getWidth() / 2, watchOnlyImage.getHeight() / 2);
                float newAngle = rotationCenter.sub(touchPoint).angle() + ANGLE_DEGREE_OFFSET;

                if (!hourHandImage.hasActions()) {
                    hourHandImage.addAction(Actions.sequence(Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            watchHandSound.play();
                        }
                    }), Actions.delay(0.5f)));
                }

                hourHandImage.setRotation(newAngle);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                setTime(getClosestHourValue(hourHandImage.getRotation()));
                watchHandSound.play();
            }
        });

        bgImage.setTouchable(Touchable.disabled);
        this.setTouchable(Touchable.disabled);

        bgImage.setSize(getWidth(), getHeight());
        watchOnlyImage.setSize(watchOnlyImage.getWidth() * ratio, watchOnlyImage.getHeight() * ratio);
        watchOnlyImage.setX(getWidth() / 2 - watchOnlyImage.getWidth() / 2 - 1);

        addActor(bgImage);
        addActor(watchOnlyImage);
    }

    private void prepareHands() {
        hourHandImage = new Image(hourHandTexture);
        Image minuteHandImage = new Image(minuteHandTexture);

        minuteHandImage.setTouchable(Touchable.disabled);
        hourHandImage.setTouchable(Touchable.disabled);

        float minuteHandHeight = height / 2.5f;
        float minuteHandTextureRatio = (float)minuteHandTexture.getRegionWidth() / (float)minuteHandTexture.getRegionHeight();
        float minuteHandWidth = minuteHandHeight*minuteHandTextureRatio;

        float hourHandHeight = height / 4;
        float hourHandTextureRatio = (float)hourHandTexture.getRegionWidth() / (float)hourHandTexture.getRegionHeight();
        float hourHandWidth = hourHandHeight*hourHandTextureRatio;

        minuteHandImage.setSize(minuteHandWidth,minuteHandHeight);
        minuteHandImage.setPosition(width / 2 - minuteHandImage.getWidth() / 2, height / 2 - MINUTE_HAND_Y_OFFSET);

        hourHandImage.setSize(hourHandWidth, hourHandHeight);
        hourHandImage.setPosition(width / 2 - hourHandImage.getWidth() / 2, height / 2 - HOUR_HAND_Y_OFFSET);

        hourHandImage.setOrigin(hourHandImage.getWidth() / 2, 4);

        addActor(minuteHandImage);
        addActor(hourHandImage);
    }

    public void setTime(int hours) {
        Log.info(getClass(), "TIME SET: " + hours);
        hourHandImage.setRotation(360 - hours * 30);
    }

    private int getClosestHourValue(float angle) {
        float angleInsideThreeSixty = angle % 360;
        float recalcAngle = 360 - angleInsideThreeSixty;

        int hours = (int)(360 - angleInsideThreeSixty) / 30;
        if (recalcAngle % 30 > TIME_DEGREE_TOLERANCE) {
            hours += 1;
        }
        return hours;
    }

    public void showCorrectTime(int hours) {
        hideHourHand();
        correctTimeHand = createHourHand(hourHandImage.getX(), hourHandImage.getY(), hourHandImage.getWidth(), hourHandImage.getHeight(), hourHandImage.getScaleX(), hourHandImage.getScaleY(), hourHandImage.getOriginX(), hourHandImage.getOriginY());
        correctTimeHand.setColor(Color.GREEN);
        correctTimeHand.setRotation(360 - hours * 30);
        addActor(correctTimeHand);
    }

    public void showWrongTime(int hours) {
        hideHourHand();
        wrongTimeHand = createHourHand(hourHandImage.getX(), hourHandImage.getY(), hourHandImage.getWidth(), hourHandImage.getHeight(), hourHandImage.getScaleX(), hourHandImage.getScaleY(), hourHandImage.getOriginX(), hourHandImage.getOriginY());
        wrongTimeHand.setColor(Color.RED);
        wrongTimeHand.setRotation(360 - hours * 30);
        addActor(wrongTimeHand);
    }

    public int getCurrentTime() {
        return (int)((360 - hourHandImage.getRotation()) / 30);
    }

    private void hideHourHand() {
        if (hourHandImage.isVisible()) {
            hourHandImage.setVisible(false);
        }
    }

    public void resetWatch() {
        if (correctTimeHand != null) {
            correctTimeHand.remove();
        }
        if (wrongTimeHand != null) {
            wrongTimeHand.remove();
        }
        hourHandImage.setVisible(true);
        setTime(0);
    }

    private Image createHourHand(float x, float y, float width, float height,float scaleX, float scaleY, float originX, float originY) {
        Image hourHand = new Image(hourHandTexture);
        hourHand.setPosition(x, y);
        hourHand.setSize(width, height);
        hourHand.setScale(scaleX, scaleY);
        hourHand.setOrigin(originX, originY);

        return hourHand;
    }

    public void enable(boolean enable) {
        if (enable) {
            setTouchable(Touchable.childrenOnly);
        } else {
            setTouchable(Touchable.disabled);
        }
    }

    @Override
    public void setDebug(boolean enabled) {
        watchOnlyImage.setDebug(enabled);
        hourHandImage.setDebug(enabled);
    }

    //Get for testing

    public Image getHourHandImage() {
        return hourHandImage;
    }

    public Image getWatchOnlyImage() {
        return watchOnlyImage;
    }
}