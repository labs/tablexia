/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.night_watch.solution;

/**
 * Created by Vaclav Tarantik on 30.4.2015.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PropertiesGenerator {
    private static final int            MIN_TIME = 1;
    private static final int            MAX_TIME = 11;

    public static List<Integer> getRandomWindowSequence(int windowsInRound, int sequenceSize, Random random) {
        List<Integer> possibleWindowNumbers = new ArrayList<Integer>();
        for (int i = 0; i < windowsInRound; i++) {
            possibleWindowNumbers.add(i);
        }
        //using random seed to shuffle sequence collection
        Collections.shuffle(possibleWindowNumbers,random);

        return possibleWindowNumbers.subList(0, sequenceSize);

    }
    
    public static List<Integer> getRandomDistractionSequence(int distractionCount, int sequenceSize, List<Integer> usedWindows, Random random) {
        List<Integer> possibleWindowNumbers = new ArrayList<Integer>();
        for (int i = 0; i < sequenceSize; i++) {
            if (!usedWindows.contains(i)) possibleWindowNumbers.add(i);
        }
        //using random seed to shuffle sequence collection
        Collections.shuffle(possibleWindowNumbers,random);

        return possibleWindowNumbers.subList(0, distractionCount);
    }

    public static int getRandomTime(Random r) {
        return r.nextInt((MAX_TIME - MIN_TIME) + 1) + MIN_TIME;
    }
}
