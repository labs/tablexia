/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.night_watch.helper;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.games.night_watch.model.NightWatchDifficulty;

/**
 * Created by Vendik on 30.4.2015.
 * Edited by danilov on 3.8.2016.
 */
public class GameRulesHelper {

    private enum ResultStars {
        THREE_STAR(AbstractTablexiaGame.GameResult.THREE_STAR, 14, 12),
        TWO_STAR(AbstractTablexiaGame.GameResult.TWO_STAR, 9, 8),
        ONE_STAR(AbstractTablexiaGame.GameResult.ONE_STAR, 5, 4);

        AbstractTablexiaGame.GameResult gameResult;
        int oldScore;
        int newScore;

        ResultStars(AbstractTablexiaGame.GameResult gameResult, int oldScore, int newScore) {
            this.gameResult = gameResult;
            this.oldScore = oldScore;
            this.newScore = newScore;
        }
    }

    //time difference is divided by number of rounds in each level showing the same number of windows
    public static final float DEFAULT_MAX_LID_TIME     = 1.5f;
    public static final float DEFAULT_MIN_LID_TIME     = 0.7f;
    public static final float BONUS_MAX_LID_TIME       = 2.5f;
    public static final float BONUS_MIN_LID_TIME       = 1.2f;
    public static final int   NUMBER_OF_ROUNDS         = 12;
    public static final int   BONUS_NUMBER_OF_ROUNDS   = 20;
    public static final int[] ZERO_NUMBER_OF_SHADOWED  = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    public static float getCurrentLidTime(NightWatchDifficulty nightWatchDifficulty, int currentRound){
        float timeUnit = (nightWatchDifficulty.getMaxLitTime() - nightWatchDifficulty.getMinLitTime()) / nightWatchDifficulty.getNumberOfRounds();
        return nightWatchDifficulty.getMaxLitTime() - (timeUnit * currentRound);
    }

    public static AbstractTablexiaGame.GameResult getNumberOfStarsForResult(int points, boolean oldVersion){
        for(ResultStars resultStars : ResultStars.values()) {
            if(oldVersion) {
                if(points >= resultStars.oldScore) return resultStars.gameResult;
            } else {
                if(points >= resultStars.newScore) return resultStars.gameResult;
            }
        }
        return AbstractTablexiaGame.GameResult.NO_STAR;
    }

    public static int getCheatingButtonScoreForResult(AbstractTablexiaGame.GameResult gameResult) {
        for(ResultStars resultStars : ResultStars.values()) {
            if(resultStars.gameResult == gameResult) return resultStars.newScore;
        }
        return 0;
    }
}
