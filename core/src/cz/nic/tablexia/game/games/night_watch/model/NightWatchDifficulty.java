/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.night_watch.model;

import net.dermetfan.utils.Pair;

import java.util.List;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.night_watch.helper.GameRulesHelper;
import cz.nic.tablexia.game.games.night_watch.helper.TextureHelper;

public enum NightWatchDifficulty {

    EASY(GameDifficulty.EASY, 16, TextureHelper.sixteenWindowsProperties, new int[] {1,1,1,2,2,3,3,4,4,4,5,5}),
    MEDIUM(GameDifficulty.MEDIUM, 24, TextureHelper.twentyFourWindowsPositions, new int[] {1,2,2,3,3,4,4,5,5,5,6,6}),
    HARD(GameDifficulty.HARD, 32, TextureHelper.thirtyTwoWindowsPositions, new int[] {1,2,3,3,4,4,5,5,6,6,6,7}),
    BONUS(GameDifficulty.BONUS, GameDifficulty.HARD, 20, 32, TextureHelper.thirtyTwoWindowsPositions, new int[] {2,2,2,3,3,3,3,3,3,4,4,4,4,5,5,5,6,6,6,7},
            new int[]{2,3,3,2,2,3,3,3,4,3,3,4,4,4,4,5,4,5,6,6}, WindowTypeDefinition.SHADOWED, GameRulesHelper.BONUS_MAX_LID_TIME, GameRulesHelper.BONUS_MIN_LID_TIME);

    private GameDifficulty               gameDifficulty;
    private GameDifficulty               reusesGameDifficulty;
    private int                          numberOfRounds;
    private int                          totalNumberOfWindows;
    private List<Pair<Integer, Integer>> windowsPositions;
    private int[]                        numberOfWindowsInRound;
    private int[]                        numberOfDistractions;
    private WindowTypeDefinition         targetTypeDefinition;
    private float                        maxLitTime;
    private float                        minLitTime;
    
    NightWatchDifficulty (GameDifficulty gameDifficulty, int totalNumberOfWindows, List<Pair<Integer,Integer>> windowsPositions, int[] numberOfWindowsInRound){
        this(gameDifficulty, gameDifficulty, GameRulesHelper.NUMBER_OF_ROUNDS, totalNumberOfWindows, windowsPositions, numberOfWindowsInRound,
                GameRulesHelper.ZERO_NUMBER_OF_SHADOWED, WindowTypeDefinition.EMPTY, GameRulesHelper.DEFAULT_MAX_LID_TIME, GameRulesHelper.DEFAULT_MIN_LID_TIME);
    }
    
    NightWatchDifficulty (GameDifficulty gameDifficulty, GameDifficulty reusesGameDifficulty, int numberOfRounds, int totalNumberOfWindows, List<Pair<Integer,Integer>> windowsPositions,
                          int[] numberOfWindowsInRound, int[] numberOfDistractions, WindowTypeDefinition targetTypeDefinition, float maxLitTime, float minLitTime) {
        this.gameDifficulty = gameDifficulty;
        this.reusesGameDifficulty = reusesGameDifficulty;
        this.numberOfRounds = numberOfRounds;
        this.totalNumberOfWindows = totalNumberOfWindows;
        this.windowsPositions = windowsPositions;
        this.numberOfWindowsInRound = numberOfWindowsInRound;
        this.numberOfDistractions = numberOfDistractions;
        this.targetTypeDefinition = targetTypeDefinition;
        this.maxLitTime = maxLitTime;
        this.minLitTime = minLitTime;
    }

    public static NightWatchDifficulty getNightWatchDifficultyForGameDifficulty(GameDifficulty gameDifficulty) throws IllegalArgumentException {
        for (NightWatchDifficulty nightWatchDifficulty : NightWatchDifficulty.values()) {
            if (nightWatchDifficulty .gameDifficulty == gameDifficulty) {
                return nightWatchDifficulty ;
            }
        }
        throw new IllegalArgumentException();
    }

    public List<Pair<Integer, Integer>> getWindowsPositions() {
        return windowsPositions;
    }

    public int getNumberOfRounds() {
        return numberOfRounds;
    }

    public int getTotalNumberOfWindows() {
        return totalNumberOfWindows;
    }

    public int getNumberOfWindowsLightenedInCurrentRound(int round) {
        return numberOfWindowsInRound[round];
    }
    
    public int getNumberOfWindowsShadowedInCurrentRound(int round){
        return numberOfDistractions[round];
    }

    public WindowTypeDefinition getTargetTypeDefinition() {
        return targetTypeDefinition;
    }

    public GameDifficulty getReusesGameDifficulty() {
        return reusesGameDifficulty;
    }

    public float getMaxLitTime() {
        return maxLitTime;
    }

    public float getMinLitTime() {
        return minLitTime;
    }
}
