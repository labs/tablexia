/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.night_watch.assets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Vendik on 30.4.2015.
 */
public final class NightWatchAssets {
    public static final String SOUNDS_PATH          = "common/sfx/";
    public static final String MP3_EXTENSION        = ".mp3";
    public static final String BACKGROUND_FOLDER    = "background/";
    public static final String WINDOWS_FOLDER       = "windows/";
    public static final String SHADOWS_FOLDER       = "shadows/";

    public static final String WATCH          = "watch";
    public static final String WATCH_ONLY     = "watch_only";
    public static final String HOUR_HAND      = "hour_hand_moving";
    public static final String MINUTE_HAND    = "minute_hand_static";


    //SOUNDS
    public static final String SOUND_DAY = SOUNDS_PATH + "day" + MP3_EXTENSION;
    public static final String SOUND_NIGHT = SOUNDS_PATH + "night" + MP3_EXTENSION;
    public static final String SOUND_TRANSITION_EVENING_1 = SOUNDS_PATH + "transition_evening_1" + MP3_EXTENSION;
    public static final String SOUND_TRANSITION_EVENING_2 = SOUNDS_PATH + "transition_evening_2" + MP3_EXTENSION;
    public static final String SOUND_TRANSITION_EVENING_3 = SOUNDS_PATH + "transition_evening_3" + MP3_EXTENSION;
    public static final String SOUND_TRANSITION_MORNING_1 = SOUNDS_PATH + "transition_morning_1" + MP3_EXTENSION;
    public static final String SOUND_TRANSITION_MORNING_2 = SOUNDS_PATH + "transition_morning_2" + MP3_EXTENSION;
    public static final String SOUND_TRANSITION_MORNING_3 = SOUNDS_PATH + "transition_morning_3" + MP3_EXTENSION;
    public static final String SOUND_WATCH_1 = SOUNDS_PATH + "watch_1" + MP3_EXTENSION;
    public static final String SOUND_WATCH_2 = SOUNDS_PATH + "watch_2" + MP3_EXTENSION;

    public static final String VICTORYSPEECH_NOSTAR =  SOUNDS_PATH + "result_0" + MP3_EXTENSION;
    public static final String VICTORYSPEECH_ONESTAR =  SOUNDS_PATH + "result_1" + MP3_EXTENSION;
    public static final String VICTORYSPEECH_TWOSTAR =  SOUNDS_PATH + "result_2" + MP3_EXTENSION;
    public static final String VICTORYSPEECH_THREESTAR =  SOUNDS_PATH + "result_3" + MP3_EXTENSION;

    //TEXT+
    public static final String VICTORYTEXT_NOSTAR = "game_nightwatch_victory_text_0";
    public static final String VICTORYTEXT_ONESTAR = "game_nightwatch_victory_text_1";
    public static final String VICTORYTEXT_TWOSTARS = "game_nightwatch_victory_text_2";
    public static final String VICTORYTEXT_THREESTARS = "game_nightwatch_victory_text_3";

    public static final String BUTTON_TEXT = "game_nightwatch_btncontinue_text";

    public static List<String> getSoundsToLoad() {
        List<String> list = new ArrayList<String>(Arrays.asList(SOUND_WATCH_1, SOUND_WATCH_2));
        list.addAll(getMorningTransitionSounds());
        list.addAll(getEveningTransitionSounds());
        return list;
    }

    public static List<String> getMorningTransitionSounds() {
        return Arrays.asList(
                SOUND_TRANSITION_MORNING_1,
                SOUND_TRANSITION_MORNING_2,
                SOUND_TRANSITION_MORNING_3
        );
    }

    public static List<String> getEveningTransitionSounds() {
        return Arrays.asList(
                SOUND_TRANSITION_EVENING_1,
                SOUND_TRANSITION_EVENING_2,
                SOUND_TRANSITION_EVENING_3
        );
    }
}
