/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.night_watch.helper;

import com.badlogic.gdx.graphics.Color;

import net.dermetfan.utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.night_watch.assets.NightWatchAssets;
import cz.nic.tablexia.game.games.night_watch.model.NightWatchDifficulty;

/**
 * Created by Vaclav Tarantik on 30.4.2015.
 */
public class TextureHelper {
    private static final int NUMBER_OF_BACKGROUNDS_FOR_ANIMATION = 7;
    private static final int NIGHT_TO_DAY_FIRST_TEXTURE_INDEX = 4;

    public static List<Pair<Integer,Integer>> sixteenWindowsProperties = new ArrayList<Pair<Integer,Integer>>();
    public static List<Pair<Integer,Integer>> twentyFourWindowsPositions = new ArrayList<Pair<Integer,Integer>>();
    public static List<Pair<Integer,Integer>> thirtyTwoWindowsPositions = new ArrayList<Pair<Integer,Integer>>();

    public static List<Pair<Integer, Integer>> testSixteenWindowsProperties = new ArrayList<>();
    public static List<Pair<Integer, Integer>> testTwentyFourWindowsPositions = new ArrayList<>();
    public static List<Pair<Integer, Integer>> testThirtyTwoWindowsPositions = new ArrayList<>();

    public static Map<Color,Integer> colorMap = new HashMap<Color, Integer>();

    static {
        colorMap.put(new Color (110 / 255f, 120 / 255f, 130 / 255f, 1), 0);
        colorMap.put(new Color(1, 1, 0, 1), 1);
        colorMap.put(new Color(1, 0, 0, 1), 2);
        colorMap.put(new Color(1, 0, 1, 1), 3);
        colorMap.put(new Color(0, 0, 1, 1), 4);
        colorMap.put(new Color(0, 1, 1, 1), 5);
        colorMap.put(new Color(0, 1, 0, 1), 6);
        colorMap.put(new Color(1, 1, 1, 1), 7);
        colorMap.put(new Color(1, 100 / 255f, 0, 1), 8);
        colorMap.put(new Color(1, 0, 100 / 255f, 1), 9);
        colorMap.put(new Color(1, 100 / 255f, 100 / 255f, 1), 10);
        colorMap.put(new Color(100 / 255f, 100 / 255f, 1, 1), 11);
        colorMap.put(new Color(0, 1, 100 / 255f, 1), 12);
        colorMap.put(new Color(0, 100 / 255f, 1, 1), 13);
        colorMap.put(new Color(100 / 255f, 1, 1, 1), 14);
        colorMap.put(new Color(100 / 255f, 100 / 255f, 100 / 255f, 1), 15);
        colorMap.put(new Color(200 / 255f, 100 / 255f, 1, 1), 16);
        colorMap.put(new Color(200 / 255f, 200 / 255f, 1, 1), 17);
        colorMap.put(new Color(100 / 255f, 200 / 255f, 100 / 255f, 1), 18);
        colorMap.put(new Color(200 / 255f, 1, 1, 1), 19);
        colorMap.put(new Color(200 / 255f, 200 / 255f, 200 / 255f, 1), 20);
        colorMap.put(new Color(100 / 255f, 200 / 255f, 1, 1), 21);
        colorMap.put(new Color(1, 100 / 255f, 200 / 255f, 1), 22);
        colorMap.put(new Color(1, 200 / 255f, 0, 1), 23);
        colorMap.put(new Color(150 / 255f, 1, 200 / 255f, 1), 24);
        colorMap.put(new Color(150 / 255f, 1, 100 / 255f, 1), 25);
        colorMap.put(new Color(1, 1, 100 / 255f, 1), 26);
        colorMap.put(new Color(1, 150 / 255f, 100 / 255f, 1), 27);
        colorMap.put(new Color(150 / 255f, 150 / 255f, 100 / 255f, 1), 28);
        colorMap.put(new Color(150 / 255f, 1, 150 / 255f, 1), 29);
        colorMap.put(new Color(1, 150/255f, 150 / 255f, 1), 30);
        colorMap.put(new Color(150 / 255f, 150 / 255f, 1, 1), 31);

        sixteenWindowsProperties.add(new Pair<Integer, Integer>(27, 81));//0
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(64, 276));//1
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(165, 231));//2
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(246, 198));//3
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(184, 384));//4
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(250, 328));//5
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(312, 294));//6
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(333, 411));//7
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(511, 430));//8
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(572, 448));//9
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(512, 306));//10
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(632, 402));//11
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(643, 293));//12
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(707, 385));//13
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(747, 289));//14
        sixteenWindowsProperties.add(new Pair<Integer, Integer>(864, 295));//15

        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(9, 135));//0
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(126, 148));//1
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(87, 378));//2
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(500, 392));//3
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(269, 146));//4
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(266, 229));//5
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(374, 207));//6
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(374, 207));//7
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(374, 180));//8
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(482, 326));//9
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(503, 326));//10
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(482, 249));//11
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(500, 251));//12
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(683, 379));//13
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(683, 296));//14
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(682, 213));//15
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(742, 404));//16
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(742, 304));//17
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(742, 204));//18
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(810, 429));//19
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(812, 302));//20
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(884, 309));//21
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(815, 176));//22
        twentyFourWindowsPositions.add(new Pair<Integer,Integer>(887, 157));//23

        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(7, 281));//0
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(90, 279));//1
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(166, 279));//2
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(300, 391));//3
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(140, 401));//4
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(8, 137));//5
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(89, 146));//6
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(166, 156));//7
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(246, 233));//8
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(245, 311));//9
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(300, 308));//10
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(301, 234));//11
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(355, 304));//12
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(355, 238));//13
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(487, 316));//14
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(538, 325));//15
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(596, 335));//16
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(488, 237));//17
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(536, 235));//18
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(597, 231));//19
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(730, 382));//20
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(876, 413));//21
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(686, 282));//22
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(741, 283));//23
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(800, 285));//24
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(864, 286));//25
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(936, 289));//26
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(687, 210));//27
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(741, 203));//28
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(800, 197));//29
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(864, 190));//30
        thirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(938, 184));//31

        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(64, 157));//0
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(96, 370));//1
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(191, 300));//2
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(267, 259));//3
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(215, 482));//4
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(272, 408));//5
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(333, 378));//6
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(352, 496));//7
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(536, 473));//8
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(598, 486));//9
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(538, 347));//10
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(650, 488));//11
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(662, 361));//12
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(731, 474));//13
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(773, 366));//14
        testSixteenWindowsProperties.add(new Pair<Integer, Integer>(902, 374));//15

        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(45, 205));//0
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(157, 209));//1
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(123, 463));//2
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(522, 418));//3
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(300, 183));//4
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(294, 287));//5
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(393, 291));//6
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(429, 285));//7
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(391, 217));//8
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(501, 351));//9
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(555, 351));//10
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(502, 282));//11
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(555, 282));//12
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(704, 420));//13
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(704, 334));//14
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(704, 251));//15
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(765, 458));//16
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(765, 352));//17
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(765, 247));//18
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(842, 496));//19
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(842, 355));//20
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(921, 371));//21
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(842, 225));//22
        testTwentyFourWindowsPositions.add(new Pair<Integer,Integer>(921, 220));//23

        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(33, 336));//0
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(113, 329));//1
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(185, 323));//2
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(316, 416));//3
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(159, 444));//4
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(30, 184));//5
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(110, 196));//6
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(184, 204));//7
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(259, 270));//8
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(259, 347));//9
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(316, 334));//10
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(314, 266));//11
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(370, 334));//12
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(368, 268));//13
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(507, 349));//14
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(559, 361));//15
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(618, 376));//16
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(507, 269));//17
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(558, 271));//18
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(622, 270));//19
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(748, 413));//20
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(903, 441));//21
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(704, 310));//22
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(763, 316));//23
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(824, 320));//24
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(891, 330));//25
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(967, 333));//26
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(705, 244));//27
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(763, 242));//28
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(822, 243));//29
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(891, 242));//30
        testThirtyTwoWindowsPositions.add(new Pair<Integer,Integer>(968, 234));//31


        if (sixteenWindowsProperties.size() != 16 || twentyFourWindowsPositions.size() != 24 || thirtyTwoWindowsPositions.size() != 32) {
            throw new IllegalStateException("Incomplete windows position info");
        }
    }

    public static List<String> getBackgroundTexturesNames() {
        List<String> namesToReturn = new ArrayList<String>();
        for (int i = 0; i < NUMBER_OF_BACKGROUNDS_FOR_ANIMATION; i++) {
            namesToReturn.add(NightWatchAssets.BACKGROUND_FOLDER + i);
        }
        return namesToReturn;
    }

    public static List<String> getWindowsNames(GameDifficulty gameDifficulty) {
        List<String> listToReturn = new ArrayList<String>();
        for (int i = 0; i < NightWatchDifficulty.getNightWatchDifficultyForGameDifficulty(gameDifficulty).getTotalNumberOfWindows(); i++) {
            listToReturn.add(NightWatchAssets.WINDOWS_FOLDER + Integer.toString(i));
        }
        return listToReturn;
    }

    public static List<String> getDaytimeTransitionTextureNames(boolean dayToNight) {
        int startingIndex = 0;
        int endingIndex = 0;
        if (dayToNight) {
            startingIndex = 1;
            endingIndex = NIGHT_TO_DAY_FIRST_TEXTURE_INDEX;
        } else {
            startingIndex = NIGHT_TO_DAY_FIRST_TEXTURE_INDEX;
            endingIndex = NUMBER_OF_BACKGROUNDS_FOR_ANIMATION;
        }
        List<String> namesToReturn = new ArrayList<String>();
        for (int i = startingIndex; i < endingIndex; i++) {
            namesToReturn.add(NightWatchAssets.BACKGROUND_FOLDER + i);
        }
        //adding initial bg if night to day
        if (!dayToNight) {
            namesToReturn.add(NightWatchAssets.BACKGROUND_FOLDER + 0);
        }
        return namesToReturn;
    }
}
