/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.night_watch.solution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Václav Tarantík on 13.5.15.
 */
public class Solution {
    private List<Integer> windowSequence;
    private List<Integer> distractionWindowSequence;
    private int           time;

    public Solution(List<Integer> windowSequence, int time) {
        this(windowSequence, new ArrayList<>(), time);
    }

    public Solution(List<Integer> windowSequence, List<Integer> distractionWindowSequence, int time) {
        this.windowSequence = windowSequence;
        this.distractionWindowSequence = distractionWindowSequence;
        this.time = time;
    }

    public List<Integer> getWindowSequence() {
        return windowSequence;
    }

    public int getTime() {
        return time;
    }

    //returns whether passed parameters fulfill this solution
    public boolean check(List<Integer>chosenWindows,int chosenTime) {
        return checkTime(chosenTime) && checkWindows(chosenWindows);
    }


    public boolean checkWindows(List<Integer>chosenWindows) {
        for (int correctWindow: windowSequence) {
            if (!chosenWindows.contains(correctWindow)) {
                return false;
            }
        }
        return true;
    }

    public boolean checkTime(int chosenTime) {
        if (chosenTime != time) {
            return false;
        }
        return true;
    }

    public List<Integer> getDistractionWindowSequence() {
        return distractionWindowSequence;
    }

    @Override
    public String toString() {
        return "WINDOW SEQUENCE: " + windowSequence.toArray().toString() + ", TIME: " + time;
    }
}
