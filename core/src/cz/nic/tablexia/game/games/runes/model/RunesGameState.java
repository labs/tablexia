/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.model;


/**
 * Created by Vitaliy Vashchenko on 18.4.16.
 */
public class RunesGameState {
    
    private float time         = 0f;
    private int   score        = 0;
    private int   wrongRunes   = 0;
    private int   round        = 0;
    
    private RunesGamePhase phase = RunesGamePhase.PAUSE;

    public enum RunesGamePhase{
        GAME,
        ANIMATION,
        PAUSE,
        END;
    }
    

    public boolean hasNextRound() {
        return  round+1<=RunesGameProperties.GAME_ROUNDS;
    }
    
    public void startGame(){
        phase = RunesGamePhase.GAME;
    }
    
    public void stopGame(){
        phase = RunesGamePhase.END;
        time = 0f;
    }

    private void addScore(int val){
        score += val;
    }

    public void addHit(){
        addScore(1);
    }

    private void addWrongRunes(int val) {
        wrongRunes += val;
    }

    public void addWrongRune() {
        addWrongRunes(1);
    }

    public int getWrongRunes() {
        return wrongRunes;
    }

    public float getTime() {
        return time;
    }

    
    public void nextRound(){
            round++;
            time = 0f;
    }

    public RunesGamePhase getPhase() {
        return phase;
    }

    public void setPhase(RunesGamePhase phase) {
        this.phase = phase;
    }

    public int getRound() {
        return round;
    }

    public int getScore() {
        return score;
    }

    
    public void addTime(float delta){
        time = time+delta;
    }
}
