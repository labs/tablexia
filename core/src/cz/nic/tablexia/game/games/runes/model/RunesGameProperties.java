/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.model;

import cz.nic.tablexia.shared.model.resolvers.RunesScoreResolver;

/**
 * Created by Vitaliy Vashchenko on 25.4.16.
 */
public class RunesGameProperties {
    public static final String SCORE_TOTAL = RunesScoreResolver.SCORE_TOTAL;
    public static final String WRONG_RUNES = RunesScoreResolver.WRONG_RUNES;

    public static final int    GAME_TIME            = 15;
    public static final int    GAME_ROUNDS          = 12;
    public static final int    RUNES_TO_FIND_TOTAL  = 44;

    public static final int[]  GROUPS_EASY          = {2, 2, 3, 3, 4, 4, 4, 4, 5, 5, 6, 6};
    public static final int[]  GROUPS_MEDIUM        = {3, 3, 4, 4, 4, 4, 5, 5, 6, 6, 6, 6};
    public static final int[]  GROUPS_HARD          = {2, 2, 3, 3, 4, 4, 4, 4, 5, 5, 6, 6};
    public static final int[]  BIG_GROUPS_ON_ROUND  = {1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2};


    public static final int[]  RUNES_COUNT          = {6, 7, 8, 9, 10, 11, 11, 12, 13, 13, 14, 15};
    public static final int[]  RUNES_TO_FIND        = {2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6}; //44

    public static final String MFX_PATH = "common/sfx/";

    public static final String[] RESULT_SOUNDS = new String[]{MFX_PATH + "result_0.mp3",
            MFX_PATH + "result_1.mp3",
            MFX_PATH + "result_2.mp3",
            MFX_PATH + "result_3.mp3"};

    public static final String[] RESULT_TEXT = new String[]{"game_runes_result_0",
            "game_runes_result_1",
            "game_runes_result_2",
            "game_runes_result_3"};

    public static final String RESULT_TEXT_SUMMARY = "game_runes_stats";

}
