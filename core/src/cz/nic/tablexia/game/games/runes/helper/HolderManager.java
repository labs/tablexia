/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.helper;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.games.runes.actors.HorizontalRunesHolder;
import cz.nic.tablexia.game.games.runes.actors.Rune;
import cz.nic.tablexia.game.games.runes.actors.RuneInstance;
import cz.nic.tablexia.game.games.runes.actors.RunesCluster;
import cz.nic.tablexia.game.games.runes.actors.RunesHolder;
import cz.nic.tablexia.game.games.runes.actors.RunesHolderTypeDefinition;
import cz.nic.tablexia.game.games.runes.actors.VerticalRunesHolder;
import cz.nic.tablexia.game.games.runes.model.RunesDifficultyDefinition;
import cz.nic.tablexia.game.games.runes.model.RunesGameProperties;

public class HolderManager {
    public static final int PAD = 3;

    protected List<RunesHolder> holders;
    protected TablexiaRandom tablexiaRandom;
    protected RunesDifficultyDefinition runesDifficultyDefinition;

    public HolderManager(RunesDifficultyDefinition runesDifficultyDefinition, TablexiaRandom tablexiaRandom) {
        this.holders = new ArrayList<>();
        this.tablexiaRandom = tablexiaRandom;
        this.runesDifficultyDefinition = runesDifficultyDefinition;
    }

    public void clearHolders() {
        for (RunesHolder rh : holders) {
            rh.clearListeners();
        }
        holders.clear();
    }

    public List<RunesHolder> getHolders() {
        return holders;
    }

    public int getTotalRunesCount() {
        int i = 0;
        for (RunesHolder holder : holders) {
            i += holder.getAllHolderRunes().size();
        }
        return i;
    }

    public void resetHoldersState() {

    }

    public void updateHolders(List<RuneDescription> roundTargets, int round) {
        for (RunesHolder rh : holders) {
            rh.setRoundTargets(roundTargets);  // set targets that are present in round
        }

        int j = 0;
        for (RuneDescription runeDescription : roundTargets) {
            holders.get(j % holders.size()).addTarget(runeDescription); // set targets that will be in selected holder
            j++;
        }

//        //distribute runes to holder and set how many will every holder have
        int runesToDistribute = RunesGameProperties.RUNES_COUNT[round] - RunesGameProperties.RUNES_TO_FIND[round];

        j = tablexiaRandom.nextInt(holders.size());
        RunesHolder currHolder = holders.get(j % holders.size());
        while (runesToDistribute > 0) {
            if (currHolder.getHolderTargets().size() == 0 && currHolder.getWrongRunesCount() == 0) {
                currHolder.increaseWrongRunesCount();
                runesToDistribute--;
            } else if (currHolder.canFitOneMoreRune() && runesToDistribute > 0) {
                currHolder.increaseWrongRunesCount();
                runesToDistribute--;
            }
            j++;
            currHolder = holders.get(j % holders.size());
        }
    }

    public void addHoldersActors(GfxLibrary gfxLibrary, ClickListener correctRuneListener, float sceneWidth, float sceneHeight) {
        for (RunesHolder rh : holders) {
            prepareHolder(rh);
            Actor holderActor;
            rh.clearGroup();
            if (runesDifficultyDefinition.getTargetType().equals(RunesDifficultyDefinition.TargetType.CLUSTER)) {
                for (RuneDescription runeDescription : rh.getAllHolderRunes()) {
                    holderActor = RunesCluster.createRuneCluster(gfxLibrary, runeDescription, rh.getRoundTargets(), tablexiaRandom, runesDifficultyDefinition.getRunesDefaultColor());
                    rh.addRuneToHolder(holderActor, correctRuneListener, runesDifficultyDefinition.getTargetType().getSpace());
                }
            } else {
                for (RuneDescription runeDescription : rh.getAllHolderRunes()) {
                    holderActor = Rune.RunesFactory.createInstance(gfxLibrary, runeDescription.getRuneDefinition(), runesDifficultyDefinition.getRunesDefaultColor(), runeDescription.isFlipped());
                    rh.addRuneToHolder(holderActor, correctRuneListener, runesDifficultyDefinition.getTargetType().getSpace());
                }
            }

            correctPosition(rh, sceneWidth, sceneHeight);
        }
    }

    public void correctPosition(RunesHolder runesHolder, float sceneWidth, float sceneHeight) {
        if (runesHolder.getTop() > sceneHeight) {
            runesHolder.setY(sceneHeight - PAD - runesHolder.getHeight() * runesHolder.getScaleY());
        }
    }

    public void prepareHolder(RunesHolder rh) {
        rh.setWrongRunes(RuneInstance.getSimilarRuneInstances(rh.getHolderTargets(), rh.getRoundTargets(), tablexiaRandom, rh.getWrongRunesCount(), runesDifficultyDefinition.isFlipRunes()));
        rh.randomizeRunesOrder(tablexiaRandom);
    }

    public RunesHolder createHolder(Collection<RuneDescription> activeTargets, RunesDifficultyDefinition runesDifficultyDefinition, RunesHolderTypeDefinition holderTypeDefinition) {
        return holderTypeDefinition.equals(RunesHolderTypeDefinition.HORIZONTAL) ?
                new HorizontalRunesHolder((List<RuneDescription>) activeTargets, runesDifficultyDefinition) :
                new VerticalRunesHolder((List<RuneDescription>) activeTargets, runesDifficultyDefinition);
    }

}