/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.helper;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.Collection;
import java.util.List;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.games.runes.actors.HorizontalRunesHolder;
import cz.nic.tablexia.game.games.runes.actors.RunesHolder;
import cz.nic.tablexia.game.games.runes.actors.RunesHolderTypeDefinition;
import cz.nic.tablexia.game.games.runes.actors.VerticalRunesHolder;
import cz.nic.tablexia.game.games.runes.model.RunesDifficultyDefinition;

/**
 * Created by Vitaliy Vashchenko on 20.4.16.
 */
public class MobileHolderManager extends HolderManager {

    public MobileHolderManager(RunesDifficultyDefinition runesDifficultyDefinition, TablexiaRandom tablexiaRandom) {
        super(runesDifficultyDefinition, tablexiaRandom);
    }

    @Override
    public void resetHoldersState() {
        for (RunesHolder runesHolder : holders) {
            runesHolder.fold();
        }
    }

    @Override
    public RunesHolder createHolder(Collection<RuneDescription> activeTargets, RunesDifficultyDefinition runesDifficultyDefinition, RunesHolderTypeDefinition holderTypeDefinition) {
        RunesHolder runesHolder = holderTypeDefinition.equals(RunesHolderTypeDefinition.HORIZONTAL) ? new HorizontalRunesHolder((List<RuneDescription>) activeTargets, runesDifficultyDefinition) : new VerticalRunesHolder((List<RuneDescription>) activeTargets, runesDifficultyDefinition);
        runesHolder.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                switchFolded(runesHolder);
                super.clicked(event, x, y);
            }
        });

        return runesHolder;
    }

    public void switchFolded(RunesHolder runesHolder) {
        if (runesHolder.isFolded()) {
            resetHoldersState();
            runesHolder.unfold();
        } else {
            resetHoldersState();
        }
    }

}