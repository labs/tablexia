/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.assets;


import java.util.Collection;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.common.media.AssetDescription;
import cz.nic.tablexia.game.games.runes.actors.RuneInstance;
import cz.nic.tablexia.game.games.runes.helper.RuneDescription;

/**
 * Created by Vitaliy Vashchenko on 8.4.16.
 */
public enum RuneDefinition implements AssetDescription, RuneDescription {

    RUNE1("runes/1",1),
    RUNE2("runes/2"),
    RUNE3("runes/3"),
    RUNE4("runes/4"),
    RUNE5("runes/5"),
    RUNE6("runes/6"),
    RUNE7("runes/7"),
    RUNE8("runes/8"),
    RUNE9("runes/9"),
    RUNE10("runes/10"),
    RUNE11("runes/11",2),
    RUNE12("runes/12",2),
    RUNE13("runes/13",2),
    RUNE14("runes/14"),
    RUNE15("runes/15"),
    RUNE16("runes/16",1),
    RUNE17("runes/17"),
    RUNE18("runes/18"),
    RUNE19("runes/19"),
    RUNE20("runes/20"),
    RUNE21("runes/21"),
    RUNE22("runes/22",2),
    RUNE23("runes/23",2),
    RUNE24("runes/24",3),
    RUNE25("runes/25"),
    RUNE26("runes/26"),
    RUNE27("runes/27"),
    RUNE28("runes/28",3),
    RUNE29("runes/29"),
    RUNE30("runes/30"),
    RUNE31("runes/31"),
    RUNE32("runes/32"),
    RUNE33("runes/33"),
    RUNE34("runes/34"),
    RUNE35("runes/35"),
    RUNE36("runes/36",1),
    RUNE37("runes/37",1),
    RUNE38("runes/38"),
    RUNE39("runes/39"),
    RUNE40("runes/40"),
    RUNE41("runes/41",3),
    RUNE42("runes/42"),
    RUNE43("runes/43",3),
    RUNE44("runes/44"),
    RUNE45("runes/45"),
    RUNE46("runes/46"),
    RUNE47("runes/47",3),
    RUNE48("runes/48",3),
    RUNE49("runes/49",3);

    public static final RuneDefinition[] ALL_RUNES = {RUNE1, RUNE2, RUNE3, RUNE4, RUNE5, RUNE6, RUNE7, RUNE8, RUNE9, RUNE10, RUNE11, 
            RUNE12, RUNE13, RUNE14, RUNE15, RUNE16, RUNE17, RUNE18, RUNE19, RUNE20, RUNE21, RUNE22, RUNE23, RUNE24, RUNE25, RUNE26, 
            RUNE27, RUNE28,RUNE29,RUNE30,RUNE31,RUNE32,RUNE33,RUNE34,RUNE35,RUNE36,RUNE37,RUNE38,RUNE39, RUNE40, RUNE41, RUNE42,
            RUNE43,RUNE44,RUNE45,RUNE46,RUNE47,RUNE48,RUNE49};

    public static final RuneDefinition[] MIRROR_RUNES = {RUNE2, RUNE3, RUNE4, RUNE5, RUNE6, RUNE7, RUNE8, RUNE9, RUNE10, RUNE11,
            RUNE12, RUNE13, RUNE16, RUNE17, RUNE18, RUNE20, RUNE22, RUNE23, RUNE25, RUNE26,
            RUNE27, RUNE28,RUNE29,RUNE30,RUNE31,RUNE32,RUNE33,RUNE34,RUNE35,RUNE37, RUNE40, RUNE41, RUNE42,
            RUNE43,RUNE44,RUNE45,RUNE46,RUNE47,RUNE48,RUNE49};

    public static final RuneDefinition[] GROUP1 = {RUNE1,RUNE16,RUNE36,RUNE37,RUNE44, RUNE45, RUNE46, };
    public static final RuneDefinition[] GROUP2 = {RUNE23,RUNE4,RUNE11,RUNE12,RUNE13,RUNE22};
    public static final RuneDefinition[] GROUP3 = {RUNE24,RUNE28,RUNE41,RUNE43,RUNE47,RUNE48,RUNE49};



    public static final RuneDefinition[] TEST_RUNES = {RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38,RUNE38}; // runes with max width for test purposes
    

    private String path;
    private int type;

    RuneDefinition(String path, int type) {
        this.path = path;
        this.type = type;
    }

    RuneDefinition(String path) {
        this.path = path;
        this.type = 0;
    }
    
    

    @Override
    public String getResource() {
        return this.path;
    }

    public int getType() {
        return type;
    }

    public static RuneDefinition getRandomRuneDefinitionFromSelectedPool(RuneDefinition[] pool, TablexiaRandom tablexiaRandom) {
        return pool[tablexiaRandom.nextInt(pool.length)];
    }

    public static RuneDefinition getRandomRuneDefinition(RuneDefinition[] runeDefinitions, Collection runeExceptions, TablexiaRandom tablexiaRandom) {
        runeDefinitions = runeDefinitions==null ? RuneDefinition.ALL_RUNES : runeDefinitions;
        RuneDefinition newRandomRuneDefinition;
        if (runeExceptions != null) {
            do {
                newRandomRuneDefinition = getRandomRuneDefinitionFromSelectedPool(runeDefinitions, tablexiaRandom);
            } while (RuneInstance.hasInstanceWithSameDefinition(runeExceptions, newRandomRuneDefinition));
        } else {
            newRandomRuneDefinition = getRandomRuneDefinitionFromSelectedPool(runeDefinitions, tablexiaRandom);
        }
        return newRandomRuneDefinition;
    }


    @Override
    public RuneDefinition getRuneDefinition() {
        return this;
    }

    @Override
    public boolean isFlipped() {
        return false;
    }


}

