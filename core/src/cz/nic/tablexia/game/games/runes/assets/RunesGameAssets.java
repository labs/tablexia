/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.assets;

/**
 * Created by Vitaliy Vashchenko on 7.4.16.
 */
public class RunesGameAssets {
    
    private static final String GFX_PATH = "gfx/";
    
    public static final String SIDEBAR_BACKGROUND = GFX_PATH + "sidebar-background";
    public static final String BACKGROUND_EASY  = GFX_PATH + "background-easy";
    public static final String BACKGROUND_MEDIUM = GFX_PATH + "background-medium";
    public static final String BACKGROUND_HARD = GFX_PATH + "background-hard";
    public static final String BACKGROUND_BONUS_DARK = GFX_PATH + "background-bonus-1";
    public static final String BACKGROUND_BONUS_LIGHT = GFX_PATH + "background-bonus-light";
    public static final String BACKGROUND = "background";
    public static final String GLASS = GFX_PATH + "hand-glass";
    public static final String BAR_FG = GFX_PATH + "time-bar-fg";
    public static final String BAR_BG_WHITE = GFX_PATH + "time-bar-bg-white";
    public static final String SCORE_TEXT = "game_runes_score_text";

    private static final String SOUND_PATH = "common/sfx/";
    private static final String SOUND_EXTENSION = ".mp3";
    
    public static final String CORRECT_SOUND = SOUND_PATH  + "correct" + SOUND_EXTENSION;
    public static final String WRONG_SOUND = SOUND_PATH + "wrong" + SOUND_EXTENSION;
    public static final String CLOCK_SOUND = SOUND_PATH + "clock" + SOUND_EXTENSION;
    
}
