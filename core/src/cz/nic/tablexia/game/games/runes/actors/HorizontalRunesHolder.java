/*
 * Copyright (C) 2021 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.actors;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.utils.SnapshotArray;

import java.util.List;

import cz.nic.tablexia.game.games.runes.RunesGame;
import cz.nic.tablexia.game.games.runes.helper.RuneDescription;
import cz.nic.tablexia.game.games.runes.model.RunesDifficultyDefinition;


public class HorizontalRunesHolder extends RunesHolder {

    private HorizontalGroup runesGroup;


    public HorizontalRunesHolder(List<RuneDescription> roundTargets, RunesDifficultyDefinition runesDifficultyDefinition) {
        super(roundTargets, runesDifficultyDefinition);
    }

    @Override
    public void init() {
        runesGroup = new HorizontalGroup();
        runesGroup.setFillParent(true);
        runesGroup.space(runesDifficultyDefinition.getTargetType().getSpace());
        runesGroup.padLeft(PAD);
        runesGroup.padRight(PAD);
        setHeight(HEIGHT);
        addActor(runesGroup);
    }

    @Override
    public void fold() {
        if (!folded) {
            for (Actor a : getChildren()) {
                a.setTouchable(Touchable.disabled);
            }
            setTouchable(Touchable.enabled);
            setScale(alternativeScale);
            if (prevX != 0) {
                setX(prevX);
            }
            folded = true;
        }
    }

    @Override
    public void unfold() {
        if (folded && getStage() != null) {
            for (Actor a : getChildren()) {
                a.setTouchable(Touchable.enabled);
            }
            setTouchable(Touchable.childrenOnly);
            toFront();
            prevX = getX();
            float stageWidth = getStage().getWidth() * (1 - RunesGame.SIDEBAR_RELATIVE_WIDTH);
            float prevMid = prevX + getWidth() * 0.5f * getScaleX();
            setScale(UNFOLDED_SCALE);
            float newX = prevMid - getWidth() * getScaleX() * 0.5f;
            if (newX + getWidth() * getScaleX() >= stageWidth - PAD) {
                setX(stageWidth - PAD - getWidth() * getScaleX());
            } else if (newX <= PAD) {
                setX(PAD); //if centered unfolded holder is out of the stage = set its X to pad
            } else {
                setX(newX); // center unfolded holder position
            }
            folded = false;
        }
    }

    @Override
    protected void addRuneActor(Actor actor) {
        runesGroup.addActor(actor);
    }

    @Override
    protected void updateSize(Actor newRune, float space) {
        setWidth(getWidth() + newRune.getWidth() + space);
    }

    @Override
    public SnapshotArray<Actor> getChildren() {
        return runesGroup.getChildren();
    }



    @Override
    public void clearGroup() {
        runesGroup.clearChildren();
    }
}
