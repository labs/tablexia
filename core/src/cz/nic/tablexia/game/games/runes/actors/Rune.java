/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.games.runes.assets.RuneDefinition;
import cz.nic.tablexia.game.games.runes.helper.RuneDescription;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by Vitaliy Vashchenko on 8.4.16.
 */
public class Rune extends Image implements IRune {

    private TextureRegion textureRegion;
    private RuneDefinition runeDefinition;
    private Color defaultColor;

    public Rune(TextureRegion textureRegion, RuneDefinition runeDefinition, Color defaultColor) {
        super(textureRegion);
        this.textureRegion = textureRegion;
        this.runeDefinition = runeDefinition;
        this.defaultColor = defaultColor;
        setColor(defaultColor);
    }

    public Rune(TextureRegion textureRegion, RuneDefinition runeDefinition) {
        this(textureRegion, runeDefinition, DEFAULT_COLOR);
    }

    public RuneDefinition getRuneType() {
        return runeDefinition;
    }

    public TextureRegion getTextureRegion() {
        return textureRegion;
    }

    @Override
    public RuneDefinition getType() {
        return runeDefinition;
    }

    @Override
    public void showAnimation(AnimationType animationType) {
        if (!hasActions()) {
            addAction(Actions.sequence(Actions.color(animationType.getColor(), animationType.getDuration()), Actions.color(DEFAULT_COLOR, animationType.getDuration())));
            addAction(Actions.after(Actions.sequence(Actions.color(DEFAULT_COLOR), Actions.run(new Runnable() {
                @Override
                public void run() {
                    AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_ANIMATE_RUNE_DONE);
                }
            }))));
        }
    }

    @Override
    public boolean isFlipped() {
        return getTextureRegion().isFlipX();
    }

    public static class RunesFactory {

        public static Rune createInstance(GfxLibrary gfxLibrary, RuneDefinition runeDefinition, Color defaultRunesColor) {

            Rune r = new Rune(gfxLibrary.getTextureRegion(runeDefinition), runeDefinition, defaultRunesColor);
            return r;
        }

        public static Rune createInstance(GfxLibrary gfxLibrary, RuneDefinition runeDefinition) {
            return createInstance(gfxLibrary, runeDefinition, DEFAULT_COLOR);
        }

        public static Rune createFlippedInstance(GfxLibrary gfxLibrary, RuneDefinition runeDefinition, Color defaultRunesColor) {
            TextureRegion tr = new TextureRegion(gfxLibrary.getTextureRegion(runeDefinition));
            tr.flip(true, false);
            Rune r = new Rune(tr, runeDefinition, defaultRunesColor);
            return r;

        }

        public static Rune createFlippedInstance(GfxLibrary gfxLibrary, RuneDefinition runeDefinition) {
            return createFlippedInstance(gfxLibrary, runeDefinition, DEFAULT_COLOR);
        }

        public static Rune createInstance(GfxLibrary gfxLibrary, RuneDescription runeDescription, Color defaultRunesColor) {
            if (runeDescription.isFlipped()) {
                return createFlippedInstance(gfxLibrary, runeDescription.getRuneDefinition(), defaultRunesColor);
            } else {
                return createInstance(gfxLibrary, runeDescription.getRuneDefinition(), defaultRunesColor);
            }
        }

        public static Rune createInstance(GfxLibrary gfxLibrary, RuneDescription runeDescription) {
            return createInstance(gfxLibrary, runeDescription, DEFAULT_COLOR);
        }


        public static Rune createInstance(GfxLibrary gfxLibrary, RuneDefinition runeDefinition, Color defaultRunesColor, boolean flip) {
            if (flip) {
                return createFlippedInstance(gfxLibrary, runeDefinition, defaultRunesColor);
            } else {
                return createInstance(gfxLibrary, runeDefinition, defaultRunesColor);
            }
        }

        public static Rune createInstance(GfxLibrary gfxLibrary, RuneDefinition runeDefinition, boolean flip) {
            return createInstance(gfxLibrary, runeDefinition, DEFAULT_COLOR, flip);
        }
    }

}