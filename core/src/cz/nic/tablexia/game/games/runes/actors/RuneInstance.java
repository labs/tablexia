/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.actors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.games.runes.assets.RuneDefinition;
import cz.nic.tablexia.game.games.runes.helper.RuneDescription;

/**
 * Created by Vitaliy Vashchenko on 1.9.16.
 */
public class RuneInstance implements RuneDescription {

    private final RuneDefinition runeDefinition;
    private final boolean flipped;

    public RuneInstance(RuneDefinition runeDefinition, boolean flipped) {
        this.runeDefinition = runeDefinition;
        this.flipped = flipped;
    }

    public RuneInstance(RuneDefinition runeDefinition) {
        this(runeDefinition, false);
    }

    public static boolean hasInstanceWithSameDefinition(Collection<RuneDescription> runeInstances, RuneDefinition runeDefinition) {
        for (RuneDescription rd : runeInstances) {
            if (rd.getRuneDefinition().equals(runeDefinition)) return true;
        }
        return false;
    }

    /**
     * Get similar rune instance depending on group(round rune, square rune, etc.).
     *
     * @param runeDefinition target instance definition.
     * @param runeExceptions excluded runes.
     * @param tablexiaRandom random from current seed.
     * @param flipped        whether new instance should be flipped.
     * @return instance that is from same similarity group as runeDefinition.
     */
    public static RuneInstance getSimilarRuneInstance(RuneDefinition runeDefinition, List<RuneDescription> runeExceptions, TablexiaRandom tablexiaRandom, boolean flipped) {
        RuneDefinition[] runeGroup = RuneDefinition.ALL_RUNES;
        switch (runeDefinition.getType()) {
            case 1:
                runeGroup = RuneDefinition.GROUP1;
                break;
            case 2:
                runeGroup = RuneDefinition.GROUP2;
                break;
            case 3:
                runeGroup = RuneDefinition.GROUP3;
                break;
            default:
        }
        return new RuneInstance(RuneDefinition.getRandomRuneDefinition(runeGroup, runeExceptions, tablexiaRandom), flipped);
    }

    /**
     * Create list with instances that are similar to targetRunes. Output will not contain runes that are defined in runeExceptions.
     *
     * @param targetRunes    runes in holder, from which wrong runes will be generated. Target runes will be excluded.
     * @param runeExceptions all target runes, which will be excluded.
     * @param tablexiaRandom random from current seed.
     * @param count          how many wrong runes to generate.
     * @param flipped        depending on difficulty, generated runes will or will not be flipped.
     * @return list of runes that are similar to targetRunes.
     */
    public static List<RuneDescription> getSimilarRuneInstances(List<RuneDescription> targetRunes, List<RuneDescription> runeExceptions, TablexiaRandom tablexiaRandom, int count, boolean flipped) {
        List<RuneDescription> newDefinitionsList = new ArrayList();
        if (targetRunes.size() > 0) {
            for (int i = 0; i < count; i++) {
                newDefinitionsList.add(getSimilarRuneInstance(targetRunes.get(i % targetRunes.size()).getRuneDefinition(), runeExceptions, tablexiaRandom, flipped));
            }
            if (count > 0 && flipped && tablexiaRandom.nextBoolean()) {
                newDefinitionsList.set(newDefinitionsList.size() - 1, new RuneInstance(runeExceptions.get(tablexiaRandom.nextInt(runeExceptions.size())).getRuneDefinition(), false)); //replacing non flipped instead of last wrong rune
            }
        } else {
            for (int i = 0; i < count; i++) {
                newDefinitionsList.add(new RuneInstance(RuneDefinition.getRandomRuneDefinition(RuneDefinition.ALL_RUNES, runeExceptions, tablexiaRandom), flipped));
            }

        }
        return newDefinitionsList;
    }

    @Override
    public RuneDefinition getRuneDefinition() {
        return runeDefinition;
    }

    @Override
    public boolean isFlipped() {
        return flipped;
    }

}
