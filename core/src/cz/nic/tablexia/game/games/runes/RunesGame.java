/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.common.ui.health_bar.HealthBar;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.runes.actors.IRune;
import cz.nic.tablexia.game.games.runes.actors.RunesHolder;
import cz.nic.tablexia.game.games.runes.actors.RunesHolderTypeDefinition;
import cz.nic.tablexia.game.games.runes.actors.TargetPlate;
import cz.nic.tablexia.game.games.runes.actors.TimeBar;
import cz.nic.tablexia.game.games.runes.assets.RuneDefinition;
import cz.nic.tablexia.game.games.runes.assets.RunesGameAssets;
import cz.nic.tablexia.game.games.runes.helper.HolderManager;
import cz.nic.tablexia.game.games.runes.helper.MobileHolderManager;
import cz.nic.tablexia.game.games.runes.helper.PositionDefinition;
import cz.nic.tablexia.game.games.runes.helper.PositionManager;
import cz.nic.tablexia.game.games.runes.model.RunesDifficultyDefinition;
import cz.nic.tablexia.game.games.runes.model.RunesGameProperties;
import cz.nic.tablexia.game.games.runes.model.RunesGameState;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.util.MusicUtil;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

/**
 * Created by Vitaliy Vashchenko on 7.4.16.
 */
public class RunesGame extends AbstractTablexiaGame<RunesGameState> {
    public static final String TARGET_PLATE = "target plate";
    public static final String RUNES_GROUP = "runes group";
    public static final String HEALTH_BAR = "health bar";

    public static final String EVENT_ROUND_READY = "round ready";
    public static final float SIDEBAR_RELATIVE_WIDTH = 0.17f;
    private static final int ONE_SECOND_DELAY = 1;
    private static final int START_SOUND_BOUND = 8;
    private static final float MUSIC_FADE_OUT_DURATION = 0.5f;
    private static final int WRONG_CLICKS_IN_ROUND_LIMIT = 5;
    private static final float LIGHT_FADE_DURATION = 0.1f;
    private static final float LIGHT_ON_MIN_DURATION = 0.5f;
    private static final int LIGHT_SEQUENCE_MIN_ACTIONS = 4;
    private static final int SIDEBAR_ITEMS_PAD = 10;
    private static final int HEALTH_BAR_PADDING = 20;
    private static final float TARGET_PLATE_RELATIVE_HEIGHT = 0.45f;
    private static final float TIME_BAR_RELATIVE_HEIGHT = 0.25f;
    private static final float FADE_ANIMATION_DURATION = 0.5f;
    private static final String PRELOADER_ANIM_IMAGE = "preloader_anim";
    private static final int PRELOADER_ANIM_FRAMES = 6;
    private static final float PRELOADER_ANIM_FRAME_DURATION = 0.4f;
    private static final Color PRELOADER_TEXT_COLOR = Color.DARK_GRAY;
    private static final String PRELOADER_TEXT_KEY_1 = ApplicationTextManager.ApplicationTextsAssets.GAME_RUNES_PRELOADER_TEXT1;
    private static final String PRELOADER_TEXT_KEY_2 = ApplicationTextManager.ApplicationTextsAssets.GAME_RUNES_PRELOADER_TEXT2;
    private static final String PRELOADER_TEXT_KEY_3 = ApplicationTextManager.ApplicationTextsAssets.GAME_RUNES_PRELOADER_TEXT3;
    private static final Scaling PRELOADER_IMAGE_SCALING = Scaling.fit;
    private static final int PRELOADER_TEXT_ALIGN = Align.left;
    private static final float PRELOADER_TEXT_PADDING = 10f;
    private static final float PRELOADER_IMAGE_COLUMN_WIDTH_RATIO = 1f / 4;
    private static final float PRELOADER_TEXT_COLUMN_WIDTH_RATIO = 5f / 8;
    private static final float PRELOADER_ROW_HEIGHT = 1f / 3;
    private static final String PRELOADER_DIFFICULTY_SUFFIX_SEPARATOR = "_";
    private static final String PRELOADER_DESKTOP_PREFIX = "desktop_";
    private TargetPlate targetPlate;
    private TimeBar timeBar;
    private TablexiaLabel score;
    private HealthBar healthBar;
    private HolderManager holderManager;
    private PositionManager positionManager;
    private Group content;
    private Group runesGroup;
    private Group sidebarGroup;
    private Music endingSound;
    private RunesDifficultyDefinition runesDifficultyDefinition;
    private int testCountRound = 0;
    private int wrongClicksInRound = 0;
    private GfxLibrary runesLibrary = new GfxLibrary(this, RuneDefinition.values());

    private ClickListener correctRuneListener = new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            if (getData().getPhase() == RunesGameState.RunesGamePhase.GAME) {
                IRune clicked = (IRune) event.getListenerActor();
                if (correctHit(clicked)) {
                    getMusic(RunesGameAssets.CORRECT_SOUND).play();
                    clicked.showAnimation(IRune.AnimationType.CORRECT);
                    targetPlate.removeTarget(clicked.getType());
                    getData().addHit();
                    setGameScore(RunesGameProperties.SCORE_TOTAL, getData().getScore());
                    score.setText(getFormattedText(RunesGameAssets.SCORE_TEXT, getData().getScore(), RunesGameProperties.RUNES_TO_FIND_TOTAL));

                    if (targetPlate.foundAllTargets()) {
                        timeBar.setState(TimeBar.TimeBarState.FREEZE);
                        ((Actor) clicked).addAction(Actions.delay(IRune.CORRECT_ANIMATION_DURATION * 2, Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                if (getData().hasNextRound()) endRound();
                                else finishGame();
                            }
                        })));
                    }
                } else {
                    getData().addWrongRune();
                    setGameScore(RunesGameProperties.WRONG_RUNES, getData().getWrongRunes());
                    addWrongHit(clicked);
                }
            }
            super.clicked(event, x, y);
        }
    };

    @Override
    protected void gameLoaded(Map<String, String> gameState) {
        runesDifficultyDefinition = RunesDifficultyDefinition.getRunesDifficultyForGameDifficulty(getGameDifficulty());
        super.gameLoaded(gameState);
        content = new Group();
        initScene();
        initSidebar();
        getStage().addActor(content);
        targetPlate.changeTargets(getData().getRound());
        initHolders();
    }

    @Override
    protected void gameVisible() {
        runesGroup.addAction(Actions.sequence(Actions.alpha(0), Actions.show(), Actions.fadeIn(FADE_ANIMATION_DURATION)));
        sidebarGroup.addAction(Actions.sequence(Actions.alpha(0), Actions.show(), Actions.fadeIn(FADE_ANIMATION_DURATION)));
        content.addAction(Actions.delay(ONE_SECOND_DELAY, Actions.run(new Runnable() {
            @Override
            public void run() {
                getData().startGame();
                triggerScenarioStepEvent(EVENT_GAME_READY);
            }
        })));
    }

    /**
     * Init scene elements with certain positions
     */
    private void initScene() {
        holderManager = TablexiaSettings.getInstance().isRunningOnMobileDevice() ? new MobileHolderManager(runesDifficultyDefinition, getRandom()) : new HolderManager(runesDifficultyDefinition, getRandom());
        positionManager = new PositionManager(runesDifficultyDefinition);
        runesGroup = new Group();
        runesGroup.setVisible(false);
        runesGroup.setName(RUNES_GROUP);
        final Image background = new Image(getTextureRegionForAtlas(getGameDifficultyAtlasPath(), runesDifficultyDefinition.getDifficultyBackground()));
        final Object foldTrigger = new Object();
        background.setUserObject(foldTrigger);
        content.addActor(background);

        if (getGameDifficulty().equals(GameDifficulty.BONUS)) {
            Image light = new Image(getScreenTextureRegion(RunesGameAssets.BACKGROUND_BONUS_LIGHT));
            light.setUserObject(foldTrigger);
            light.addAction(createRandomBonusDistractionSequence());
            content.addActor(light);
            setActorToFullScene(light);
        }

        content.addActor(runesGroup);

        setActorToFullScene(background);

        // click listener to fold all holders on click on background
        if (TablexiaSettings.getInstance().getPlatform() != TablexiaSettings.Platform.DESKTOP) {
            getStage().addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (event.getTarget().getUserObject() != null && event.getTarget().getUserObject().equals(foldTrigger)) {
                        holderManager.resetHoldersState();
                    }
                    super.clicked(event, x, y);
                }
            });
        }

    }

    private Action createRandomBonusDistractionSequence() {
        TablexiaRandom r = getRandom();
        int sequenceParts = LIGHT_SEQUENCE_MIN_ACTIONS + r.nextInt(LIGHT_SEQUENCE_MIN_ACTIONS);
        Action[] actionsArray = new Action[sequenceParts];
        for (int i = 0; i < sequenceParts; i++) {
            actionsArray[i] = Actions.sequence(
                    Actions.parallel(Actions.fadeIn(LIGHT_FADE_DURATION), Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            runesGroup.addAction(Actions.alpha(1f));
                        }
                    })),
                    Actions.delay(LIGHT_ON_MIN_DURATION + r.nextFloat()),
                    Actions.fadeOut(LIGHT_FADE_DURATION),
                    Actions.parallel(Actions.delay(r.nextFloat()), Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            runesGroup.addAction(Actions.alpha(0.5f));
                        }
                    }))
            );
        }
        return Actions.forever(Actions.sequence(actionsArray));
    }

    private void initSidebar() {
        sidebarGroup = new Group();
        Image sidebarBackground = new Image(getScreenTextureRegion(RunesGameAssets.SIDEBAR_BACKGROUND));
        sidebarBackground.setFillParent(true);

        healthBar = new HealthBar(getGameGlobalTextureRegion(HEART_FULL),
                getGameGlobalTextureRegion(HEART_BROKEN));

        healthBar.setName(HEALTH_BAR);

        targetPlate = new TargetPlate(runesLibrary, getRandom(), RunesDifficultyDefinition.getRunesDifficultyForGameDifficulty(getGameDifficulty()));
        targetPlate.setName(TARGET_PLATE);

        score = new TablexiaLabel(getFormattedText(RunesGameAssets.SCORE_TEXT, getData().getScore(), RunesGameProperties.RUNES_TO_FIND_TOTAL), new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.BOLD_18, Color.BLACK));
        score.setAlignment(Align.center);

        timeBar = new TimeBar(getScreenTextureRegion(RunesGameAssets.BAR_BG_WHITE), getScreenTextureRegion(RunesGameAssets.BAR_FG), new TimeBar.TimeBarCallback() {
            @Override
            public void onRestored() {
                if (getData().hasNextRound()) {
                    nextRound();
                } else {
                    finishGame();
                }

            }
        });

        sidebarGroup.addActor(sidebarBackground);
        sidebarGroup.addActor(targetPlate);
        sidebarGroup.addActor(timeBar);
        sidebarGroup.addActor(healthBar);
        sidebarGroup.addActor(score);

        setSidebarSize();

        setSidebarPosition();


        sidebarGroup.setVisible(false);
        content.addActor(sidebarGroup);

    }

    private void disposeSounds() {
        if (endingSound != null) {
            endingSound.dispose();
            endingSound = null;
        }
    }

    private void setSidebarPosition() {
        score.setPosition(sidebarGroup.getWidth() / 2 - score.getWidth() / 2, SIDEBAR_ITEMS_PAD);
        timeBar.setPosition(sidebarGroup.getWidth() / 2 - timeBar.getWidth() / 2, score.getTop() + SIDEBAR_ITEMS_PAD);
        targetPlate.setPosition(sidebarGroup.getWidth() / 2 - targetPlate.getWidth() / 2, timeBar.getTop() + SIDEBAR_ITEMS_PAD + HEALTH_BAR_PADDING);

        healthBar.setPosition(sidebarGroup.getWidth() - healthBar.getWidth() - SIDEBAR_ITEMS_PAD, sidebarGroup.getHeight() - healthBar.getHeight() - SIDEBAR_ITEMS_PAD);
    }

    private void setSidebarSize() {
        sidebarGroup.setSize(getViewportWidth() * SIDEBAR_RELATIVE_WIDTH, getViewportHeight());
        sidebarGroup.setPosition(getSceneRightX() - sidebarGroup.getWidth(), getViewportBottomY());
        targetPlate.setSize(sidebarGroup.getWidth(), sidebarGroup.getHeight() * TARGET_PLATE_RELATIVE_HEIGHT);
        timeBar.setSize(sidebarGroup.getWidth(), sidebarGroup.getHeight() * TIME_BAR_RELATIVE_HEIGHT);
    }

    @Override
    protected RunesGameState prepareGameData(Map<String, String> gameState) {
        return new RunesGameState();
    }

    @Override
    protected void gameRender(float delta) {
        if (!isScreenPaused()) {
            if (getData().getPhase() == RunesGameState.RunesGamePhase.GAME) {
                if ((int) getData().getTime() < RunesGameProperties.GAME_TIME && timeBar.getState() != TimeBar.TimeBarState.FREEZE) {
                    getData().addTime(delta);
                    timeBar.setCurrentPercentage(getData().getTime() / (float) RunesGameProperties.GAME_TIME);
                    if ((int) getData().getTime() > START_SOUND_BOUND) {
                        if (endingSound == null)
                            endingSound = getMusic(RunesGameAssets.CLOCK_SOUND);

                        endingSound.play();
                    }
                } else {
                    if (!targetPlate.foundAllTargets()) {
                        healthBar.hide();
                        if (getData().hasNextRound()) {
                            if (healthBar.hasHealthsLeft()) {
                                endRound();
                            } else {
                                finishGame();
                            }
                        } else {
                            finishGame();
                        }
                    }

                }
            } else if (getData().getPhase() == RunesGameState.RunesGamePhase.ANIMATION) {
                timeBar.performStep();
            }
        } else {
            if (endingSound != null) endingSound.pause();
        }
    }


////////////////////// PRELOAD MESSAGE

    @Override
    protected void screenResized(int width, int height) {
        setSidebarSize();
        setSidebarPosition();
    }

    private void initHolders() {
        List<PositionDefinition> newPositions = positionManager.getRoundPositions(getData().getRound(), getRandom());
        for (PositionDefinition positionDefinition : newPositions) {
            RunesHolder holder = holderManager.createHolder(targetPlate.getActiveTargetsDescriptions(), runesDifficultyDefinition, positionDefinition.isVertical() ? RunesHolderTypeDefinition.VERTICAL : RunesHolderTypeDefinition.HORIZONTAL);
            holder.init();
            holder.updateHolderPosition(positionDefinition, getSceneWidth(), getSceneInnerHeight());
            holder.setMaxEntities(PositionDefinition.getMaxChildrenCount(runesDifficultyDefinition, positionDefinition));

            holderManager.getHolders().add(holder);
            runesGroup.addActor(holder);
        }

        holderManager.updateHolders(targetPlate.getActiveTargetsDescriptions(), getData().getRound());
        holderManager.addHoldersActors(runesLibrary, correctRuneListener, getSceneWidth(), getSceneInnerHeight());

        if (TablexiaSettings.getInstance().getPlatform() != TablexiaSettings.Platform.DESKTOP) {
            holderManager.resetHoldersState();
        }
    }

    private void endRound() {
        stopEndingSound();
        runesGroup.addAction(Actions.fadeOut(FADE_ANIMATION_DURATION));
        targetPlate.addAction(Actions.fadeOut(FADE_ANIMATION_DURATION));
        getData().setPhase(RunesGameState.RunesGamePhase.ANIMATION);
        getData().nextRound();
        timeBar.restoreClock();
        resetWrongHitCounter();
        disposeSounds();
    }

    private void nextRound() {
        runesGroup.addAction(Actions.sequence(Actions.fadeIn(FADE_ANIMATION_DURATION), Actions.run(new Runnable() {
            @Override
            public void run() {
                triggerScenarioStepEvent(EVENT_ROUND_READY);
            }
        })));
        getData().setPhase(RunesGameState.RunesGamePhase.GAME);
        timeBar.startGameState();
        targetPlate.changeTargets(getData().getRound());
        targetPlate.addAction(Actions.fadeIn(FADE_ANIMATION_DURATION));

        holderManager.clearHolders();
        runesGroup.clearChildren();
        initHolders();
    }


    private boolean correctHit(IRune iRune) {
        return targetPlate.isTarget(iRune);
    }

    private void addWrongHit(IRune selectedRune) {
        wrongClicksInRound++;
        getMusic(RunesGameAssets.WRONG_SOUND).play();
        selectedRune.showAnimation(IRune.AnimationType.WRONG);
        if (wrongClicksInRound == WRONG_CLICKS_IN_ROUND_LIMIT) {
            healthBar.hide();
            if (healthBar.hasHealthsLeft()) {
                if (getData().hasNextRound()) endRound();
                else finishGame();
            } else {
                finishGame();
            }
        }
    }

    private void resetWrongHitCounter() {
        wrongClicksInRound = 0;
    }

    private void finishGame() {
        stopEndingSound();
        runesGroup.setVisible(false);
        sidebarGroup.setVisible(false);
        holderManager.resetHoldersState();
        resetWrongHitCounter();
        getData().stopGame();
        gameComplete();
    }

    private void stopEndingSound() {
        MusicUtil.fadeOut(endingSound, MUSIC_FADE_OUT_DURATION);
    }

    @Override
    protected void gameDisposed() {
        super.gameDisposed();
    }

    @Override
    protected void gameEnd() {
        getStage().clear();
        super.gameEnd();
    }

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(new SummaryMessage(SummaryImage.STATS, getFormattedText(RunesGameProperties.RESULT_TEXT_SUMMARY, game.getGameScore(RunesGameProperties.SCORE_TOTAL, "0"), RunesGameProperties.RUNES_TO_FIND_TOTAL)));

    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        return RunesGameProperties.RESULT_TEXT[gameResult.getStarCount()];
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        return RunesGameProperties.RESULT_SOUNDS[gameResult.getStarCount()];
    }

    @Override
    protected String preparePreloaderSpeechFileBaseName() {
        String speechFilePrefix = TablexiaSettings.getInstance().isRunningOnMobileDevice() ? "" : PRELOADER_DESKTOP_PREFIX;
        return speechFilePrefix + super.preparePreloaderSpeechFileBaseName() + prepareDifficultySuffix();
    }

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        AnimatedImage preloaderImage = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_IMAGE, PRELOADER_ANIM_FRAMES, PRELOADER_ANIM_FRAME_DURATION), false);
        preloaderImage.startAnimationLoop();
        String preloaderText1 = preloaderAssetsManager.getText(PRELOADER_TEXT_KEY_1);
        String preloaderText2 = preloaderAssetsManager.getText(PRELOADER_TEXT_KEY_2 + prepareDifficultySuffix());
        String preloaderText3 = TablexiaSettings.getInstance().isRunningOnMobileDevice() ? preloaderAssetsManager.getText(PRELOADER_TEXT_KEY_3) : "";
        components.add(
                new TwoColumnContentDialogComponent(
                        new AnimatedImageContentDialogComponent(preloaderImage, PRELOADER_IMAGE_SCALING),
                        new TextContentDialogComponent(preloaderText1 + " " + preloaderText2 + '\n' + preloaderText3, PRELOADER_TEXT_COLOR, PRELOADER_TEXT_ALIGN, PRELOADER_TEXT_PADDING),
                        PRELOADER_IMAGE_COLUMN_WIDTH_RATIO,
                        PRELOADER_TEXT_COLUMN_WIDTH_RATIO,
                        PRELOADER_ROW_HEIGHT
                )
        );
    }

    @Override
    public String prepareDifficultySuffix() {
        String suffix = getGameDifficulty().equals(GameDifficulty.BONUS) ? GameDifficulty.EASY.name().toLowerCase() : getGameDifficulty().name().toLowerCase();
        return PRELOADER_DIFFICULTY_SUFFIX_SEPARATOR + suffix;
    }
}