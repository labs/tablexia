/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.actors;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.runes.assets.RuneDefinition;
import cz.nic.tablexia.game.games.runes.helper.RuneDescription;
import cz.nic.tablexia.game.games.runes.model.RunesDifficultyDefinition;
import cz.nic.tablexia.game.games.runes.model.RunesGameProperties;

/**
 * Created by Vitaliy Vashchenko on 14.4.16.
 */
public class TargetPlate extends Group{
    private static final float PAD              = 30;
    private static final float INNER_PAD        = 2;
    private static final float FADE_DURATION     = 0.35f;
    
    private Group           plate;
    private Table           root;
    private GfxLibrary      runesLibrary;
    private TablexiaRandom  tablexiaRandom;
    private List<RuneDescription> activeTargetsDefinitions = new ArrayList<RuneDescription>();
    private RunesDifficultyDefinition runesDifficultyDefinition;

    public TargetPlate(GfxLibrary runesLibrary, TablexiaRandom tablexiaRandom, RunesDifficultyDefinition runesDifficultyDefinition) {
        this.runesLibrary = runesLibrary;
        this.tablexiaRandom = tablexiaRandom;
        this.runesDifficultyDefinition = runesDifficultyDefinition;
        plate = new Group();
        addActor(plate);
        root = new Table();
        root.defaults().pad(INNER_PAD);
        root.defaults().expand();
        plate.addActor(root);
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        plate.setSize(getWidth() - PAD, getHeight() - PAD);
        root.setSize(plate.getWidth(), plate.getHeight());
        root.setX(getWidth()/2 - root.getWidth()/2);
    }

    public void changeTargets(int round) {
        activeTargetsDefinitions.clear();
        activeTargetsDefinitions.addAll(generateNewTargets(round, runesDifficultyDefinition, tablexiaRandom));
        updatePlate();
    }

    // TODO: 8.11.16 move logic away from actor class 
    public static List<RuneDescription> generateNewTargets(int round, RunesDifficultyDefinition runesDifficultyDefinition, TablexiaRandom tablexiaRandom){
        List<RuneDescription> runeDescriptions = new ArrayList<>();
        RuneDefinition[] runesPool =runesDifficultyDefinition.getRuneDefinitions();

        int targetsCount = RunesGameProperties.RUNES_TO_FIND[round];
        do {
            RuneDefinition newRandomRuneDefinition = RuneDefinition.getRandomRuneDefinition(runesPool,runeDescriptions,tablexiaRandom);
            runeDescriptions.add(newRandomRuneDefinition);
        }while (runeDescriptions.size()< targetsCount);
        return runeDescriptions;
    }
    
    private void updatePlate(){
        if (root.hasChildren()) root.clearChildren();
        if (activeTargetsDefinitions.size() <= 2) {
            root.defaults().maxHeight(plate.getHeight()/2);
            for (RuneDescription runeDescription: activeTargetsDefinitions) {
                addRune(runeDescription);
                root.row();
            }
        }else {
            root.defaults().maxHeight(plate.getHeight()/(activeTargetsDefinitions.size()/2));
            int i = 0;
            for (RuneDescription runeDescription : activeTargetsDefinitions) {
                addRune(runeDescription);
                i++;
                if (i == 2) {
                    root.row();
                    i = 0;
                }
            }
        }
    }

    public boolean foundAllTargets() {
        return activeTargetsDefinitions.size() == 0;
    }
    
    public boolean isTarget(IRune iRune){
        Rune rune = findRuneByRuneDefinition(iRune.getType());
        if (runesDifficultyDefinition.isFlipRunes()) return isInTargets(iRune.getType()) && rune!=null && rune.isFlipped()!= iRune.isFlipped(); //control if runes are with different mirroring
        return isInTargets(iRune.getType());
    }
    
    private boolean isInTargets(RuneDescription runeDescription){
        for (RuneDescription targetRuneDescription: activeTargetsDefinitions ){
            if (targetRuneDescription.equals(runeDescription)) return true;
        }
        return false;
    }
    
    public void removeTarget(RuneDefinition runeDefinition){
        hideTarget(runeDefinition);
        activeTargetsDefinitions.remove(runeDefinition);
    }

    public void hideTarget(RuneDefinition runeDefinition) {
        Rune r = findRuneByRuneDefinition(runeDefinition);
        if(r!=null) r.addAction(Actions.fadeOut(FADE_DURATION));
    }
    
    private Rune findRuneByRuneDefinition(RuneDefinition runeDefinition){
        for (Actor actor : root.getChildren()) {
            Rune rune = (Rune) actor;
            if (rune.getType().equals(runeDefinition)) return rune;
        }
        return null;
    }

    public List<RuneDescription> getActiveTargetsDescriptions() {
        return activeTargetsDefinitions;
    }

    private void addRune(RuneDescription runeDescription) {
        Rune rune = Rune.RunesFactory.createInstance(runesLibrary, runeDescription.getRuneDefinition());
        root.add(rune);
    }

}
