/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;

/**
 * Created by Vitaliy Vashchenko on 13.5.16.
 */
public class TimeBar extends Group {

    private final Color TOP = new Color(0x71B714ff);
    private final Color BOTTOM = Color.RED;
    private float current;
    private TimeBarState state = TimeBarState.GAME;
    private TimeBarCallback callback;
    private TextureRegion backgroundRegion;
    private TextureRegion foregroundRegion;
    private int origRegionHeight;
    public TimeBar(TextureRegion backgroundRegion, TextureRegion foregroundRegion, TimeBarCallback callback) {
        this.callback = callback;
        this.backgroundRegion = backgroundRegion;
        this.foregroundRegion = foregroundRegion;
        origRegionHeight = backgroundRegion.getRegionHeight();
        setSize(foregroundRegion.getRegionWidth(), foregroundRegion.getRegionHeight());

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        current = MathUtils.clamp(current, 0, 1);
    }

    @Override
    public void setSize(float width, float height) {
        float newWidth = getWidth() > 0 || getHeight() > 0 ? height * (getWidth() / getHeight()) : width;
        super.setSize(newWidth, height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        /*
         * Draw Background/Progress Layer
         */
        if (state != TimeBarState.IDLE) {

            backgroundRegion.flip(false, true); //flip region to reduce height from top to match clock animation
            int newBackgroundRegionHeight = (int) (backgroundRegion.getRegionHeight() * (1 - current));
            backgroundRegion.setRegionHeight(newBackgroundRegionHeight);

            batch.setColor(new Color(this.getColor().r, this.getColor().g, this.getColor().b, this.getColor().a * parentAlpha));

            batch.enableBlending();

            /*
             * Variables for Background/Progress Layer
             */

            float borderX = (foregroundRegion.getRegionWidth() - backgroundRegion.getRegionWidth()) / 2;
            float borderY = (foregroundRegion.getRegionHeight() - origRegionHeight) / 2;

            float relativeBorderX = borderX / foregroundRegion.getRegionWidth();
            float relativeBorderY = borderY / foregroundRegion.getRegionHeight();

            float offsetX = relativeBorderX * getWidth();
            float offsetY = relativeBorderY * getHeight();

            float visibleValue = 1f - current;

            /**
             * Draw white textureRegion and two colors with different alpha to make gradient effect
             */

            batch.draw(
                    backgroundRegion,
                    getX() + offsetX,
                    getY() + offsetY,
                    getWidth() - 2 * offsetX,
                    (getHeight() - 2 * offsetY) * visibleValue
            );

            batch.setColor(TOP.r, TOP.g, TOP.b, visibleValue);

            batch.draw(
                    backgroundRegion,
                    getX() + offsetX,
                    getY() + offsetY,
                    getWidth() - 2 * offsetX,
                    (getHeight() - 2 * offsetY) * visibleValue
            );

            batch.setColor(BOTTOM.r, BOTTOM.g, BOTTOM.b, current);

            batch.draw(
                    backgroundRegion,
                    getX() + offsetX,
                    getY() + offsetY,
                    getWidth() - 2 * offsetX,
                    (getHeight() - 2 * offsetY) * visibleValue
            );

            batch.setColor(Color.WHITE);

            backgroundRegion.flip(false, true); //flip back to original state
            backgroundRegion.setRegionHeight(origRegionHeight);

            if (state == TimeBarState.RESTORING && current == 0) {
                state = TimeBarState.IDLE;
                callback.onRestored();
            } else if (state == TimeBarState.DECREASING && current == 1f) {
                state = TimeBarState.RESTORING;
            }
        }
        /*
         * Draw MovingImage/Progress Layer
         */
        batch.draw(foregroundRegion, getX(), getY(), getWidth(), getHeight());

    }

    public TimeBarState getState() {
        return state;
    }

    public void setState(TimeBarState state) {
        this.state = state;
    }

    public void restoreClock() {
        if (current < 1f) {
            state = TimeBarState.DECREASING;
        } else {
            state = TimeBarState.RESTORING;
        }
    }

    public void startGameState() {
        state = TimeBarState.GAME;
    }

    public void performStep() {
        this.current = MathUtils.clamp(this.current + state.getStep(), 0, 1);
    }

    public void setCurrentPercentage(float value) {
        value = MathUtils.clamp(value, 0, 1);
        this.current = value;
    }

    public enum TimeBarState {
        GAME(),
        FREEZE(),
        IDLE(),
        RESTORING(-0.015f),
        DECREASING(0.04f);

        float step;

        TimeBarState(float step) {
            this.step = step;
        }

        TimeBarState() {
            this.step = 0f;
        }

        public float getStep() {
            return step;
        }
    }

    public interface TimeBarCallback {
        void onRestored();
    }
}

