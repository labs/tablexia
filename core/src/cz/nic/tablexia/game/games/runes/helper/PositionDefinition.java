/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.helper;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.runes.model.RunesDifficultyDefinition;
import cz.nic.tablexia.game.games.runes.model.RunesGameProperties;

/**
 * Created by Vitaliy Vashchenko on 20.4.16.
 */
public enum PositionDefinition {

    POS1(0.1f, 0.1f,
            0.1f, 0.1f,
            0.1f, 0.1f,
            0.1f, 0.1f,
            0.6f, 4, 4, false, true),
    POS2(0.1f, 0.69f,
            0.1f, 0.65f,
            0.1f, 0.65f,
            0.1f, 0.65f,
            0.6f, 4, 4, false, true),
    POS3(0.23f, 0.35f,
            0.25f, 0.35f,
            0.19f, 0.35f,
            0.19f, 0.35f,
            0.6f, 5, 4, true, false),
    POS4(0.45f, 0.42f,
            0.45f, 0.5f,
            0.45f, 0.35f,
            0.45f, 0.35f,
            0.6f, 5, 4, true, false),
    POS5(0.72f, 0.1f,
            0.72f, 0.1f,
            0.72f, 0.1f,
            0.725f, 0.1f,
            0.6f, 4, 4, false, true),
    POS6(0.72f, 0.69f,
            0.72f, 0.65f,
            0.72f, 0.65f,
            0.725f, 0.65f,
            0.6f, 4, 4, false, true);


    private float easyX, mediumX, hardX, bonusX;
    private float easyY, mediumY, hardY, bonusY;
    private float scale;
    private int maxRunes, maxRunesClusters;
    private boolean big;
    private boolean vertical;

    PositionDefinition(float easyX, float easyY, float mediumX, float mediumY, float hardX, float hardY, float bonusX, float bonusY, float scale, int maxRunes, int maxRunesClusters, boolean big, boolean vertical) {
        this.easyX = easyX;
        this.easyY = easyY;
        this.mediumX = mediumX;
        this.mediumY = mediumY;
        this.hardX = hardX;
        this.hardY = hardY;
        this.bonusX = bonusX;
        this.bonusY = bonusY;
        this.maxRunes = maxRunes;
        this.maxRunesClusters = maxRunesClusters;
        this.scale = scale;
        this.big = big;
        this.vertical = vertical;
    }

    public static int getGroupsCount(int round, GameDifficulty gameDifficulty) {
        return RunesDifficultyDefinition.getRunesDifficultyForGameDifficulty(gameDifficulty).getGroupsCountForRound(round);
    }

    public static int getMaxChildrenCount(RunesDifficultyDefinition runesDifficultyDefinition, PositionDefinition positionDefinition) {
        if (runesDifficultyDefinition.getTargetType().equals(RunesDifficultyDefinition.TargetType.CLUSTER))
            return positionDefinition.getMaxRunesClusters();
        else return positionDefinition.getMaxRunes();
    }

    public static List<PositionDefinition> getPositionsByLimit(RunesDifficultyDefinition runesDifficultyDefinition, boolean big) {
        List<PositionDefinition> customPositions = new ArrayList<>();
        for (int i = 0; i < PositionDefinition.values().length; i++) {
            if (PositionDefinition.values()[i].isBig() == big) {
                customPositions.add(PositionDefinition.values()[i]);
            }
        }
        return customPositions;
    }

    public static List<PositionDefinition> getVerticalPositions(RunesDifficultyDefinition runesDifficultyDefinition, boolean vertical) {
        List<PositionDefinition> customPositions = new ArrayList<>();
        for (int i = 0; i < PositionDefinition.values().length; i++) {
            if (PositionDefinition.values()[i].isVertical() == vertical) {
                customPositions.add(PositionDefinition.values()[i]);
            }
        }
        return customPositions;
    }

    public static List<PositionDefinition> getBigPositions(RunesDifficultyDefinition runesDifficultyDefinition) {
        return getPositionsByLimit(runesDifficultyDefinition, true);
    }

    public static List<PositionDefinition> getSmallPositions(RunesDifficultyDefinition runesDifficultyDefinition) {
        return getPositionsByLimit(runesDifficultyDefinition, false);
    }

    public static int getBigPositionsForRound(int round) {
        return RunesGameProperties.BIG_GROUPS_ON_ROUND[round];
    }

    public int getMaxRunes() {
        return maxRunes;
    }

    public float getScale() {
        return scale;
    }

    public float getEasyX() {
        return easyX;
    }

    public float getMediumX() {
        return mediumX;
    }

    public float getHardX() {
        return hardX;
    }

    public float getBonusX() {
        return bonusX;
    }

    public float getEasyY() {
        return easyY;
    }

    public float getMediumY() {
        return mediumY;
    }

    public float getHardY() {
        return hardY;
    }

    public float getBonusY() {
        return bonusY;
    }

    public float getX(GameDifficulty gameDifficulty) {
        switch (gameDifficulty) {
            case MEDIUM:
                return getMediumX();
            case HARD:
                return getHardX();
            case BONUS:
                return getBonusX();
            default:
                return getEasyX();
        }
    }

    public float getY(GameDifficulty gameDifficulty) {
        switch (gameDifficulty) {
            case MEDIUM:
                return getMediumY();
            case HARD:
                return getHardY();
            case BONUS:
                return getBonusY();
            default:
                return getEasyY();
        }
    }

    public boolean isBig() {
        return big;
    }

    public int getMaxRunesClusters() {
        return maxRunesClusters;
    }

    public boolean isVertical() {
        return vertical;
    }

}
