/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.actors;

import com.badlogic.gdx.graphics.Color;

import cz.nic.tablexia.game.games.runes.assets.RuneDefinition;

/**
 * Created by Vitaliy Vashchenko on 27.4.16.
 */
public interface IRune {
    Color DEFAULT_COLOR              = Color.BLACK;
    Color DEFAULT_COLOR_BONUS        = new Color(0, 0, 0, 0.2f);
    float CORRECT_ANIMATION_DURATION = 0.5f;
    float WRONG_ANIMATION_DURATION   = 0.3f;
    String EVENT_ANIMATE_RUNE_DONE   = "animation of rune done";
    
    enum AnimationType{
        CORRECT(Color.GREEN, CORRECT_ANIMATION_DURATION),
        WRONG(Color.RED, WRONG_ANIMATION_DURATION);

        private Color color;
        private float duration;

        AnimationType(Color color, float duration) {
            this.color = color;
            this.duration = duration;
        }

        public Color getColor() {
            return color;
        }

        public float getDuration() {
            return duration;
        }
    }
    
    RuneDefinition getType();
    
    boolean isFlipped();
    
    void showAnimation(AnimationType animationType);
    
}
