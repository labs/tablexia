/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.actors;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.SnapshotArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.games.runes.RunesGame;
import cz.nic.tablexia.game.games.runes.helper.PositionDefinition;
import cz.nic.tablexia.game.games.runes.helper.RuneDescription;
import cz.nic.tablexia.game.games.runes.model.RunesDifficultyDefinition;

/**
 * Created by Vitaliy Vashchenko on 15.4.16.
 */


public abstract class RunesHolder extends Group {


    protected static final int PAD = 3;
    protected static final float UNFOLDED_SCALE = 0.8f;
    protected static final int HEIGHT = 70;
    protected static final int WIDTH = 70;

    protected int maxEntities;

    // TODO: 4/22/21 to own class
    protected List<RuneDescription> allHolderRunes;
    protected RunesDifficultyDefinition runesDifficultyDefinition;
    protected int wrongRunesCount;
    protected boolean folded = false;
    protected float alternativeScale; //scale value that can be changed for every defined map position

    protected List<RuneDescription> roundTargets;
    protected List<RuneDescription> holderTargets;
    protected RunesHolderTypeDefinition holderTypeDefinition;

    protected float prevX = 0; //save previous position because of shifting to center when unfolding


    public RunesHolder(List<RuneDescription> roundTargets, RunesDifficultyDefinition runesDifficultyDefinition) {
        this.holderTargets = new ArrayList<>();
        this.roundTargets = roundTargets;
        this.runesDifficultyDefinition = runesDifficultyDefinition;
        this.allHolderRunes = new ArrayList<>();
        wrongRunesCount = 0;
    }

    // TODO: 4/22/21 add to own class
    public void addRuneToHolder(Actor newRune, ClickListener clickListener, float space) {
        newRune.addListener(clickListener);
        addRuneActor(newRune);
        updateSize(newRune, space);
    }

    public abstract void init();

    protected abstract void addRuneActor(Actor actor);

    protected abstract void updateSize(Actor newRune, float space);

    public abstract SnapshotArray<Actor> getChildren();

    public void increaseWrongRunesCount() {
        wrongRunesCount++;
    }

    public int getWrongRunesCount() {
        return wrongRunesCount;
    }

    public void setWrongRunes(List<RuneDescription> wrongRunesDescription) {
        allHolderRunes.clear();
        allHolderRunes.addAll(wrongRunesDescription);
        if (!runesDifficultyDefinition.isFlipRunes()) {
            allHolderRunes.addAll(holderTargets);
        } else {
            for (RuneDescription runeDescription : holderTargets) {
                allHolderRunes.add(new RuneInstance(runeDescription.getRuneDefinition(), runesDifficultyDefinition.isFlipRunes())); //add flipped targets (correct ones)
            }
        }
    }

    public void randomizeRunesOrder(TablexiaRandom tablexiaRandom) {
        Collections.shuffle(allHolderRunes, tablexiaRandom);
    }

    public List<RuneDescription> getRoundTargets() {
        return roundTargets;
    }

    public void setRoundTargets(List<RuneDescription> roundTargets) {
        this.roundTargets = roundTargets;
        this.holderTargets = new ArrayList<>();
    }


    public List<RuneDescription> getAllHolderRunes() {
        return allHolderRunes;
    }

    /**
     * Set scale value depending on position (folded holder can be smaller/bigger depending on PositionDefinition values)
     *
     * @param alternativeScale folded scale
     */
    public void setAlternativeScale(float alternativeScale) {
        this.alternativeScale = alternativeScale;
        setScale(alternativeScale);
    }

    public void addTarget(RuneDescription runeDescription) {
        holderTargets.add(new RuneInstance(runeDescription.getRuneDefinition()));
    }


    public abstract void clearGroup();
    public abstract void fold();
    public abstract void unfold();


    public List<RuneDescription> getHolderTargets() {
        return holderTargets;
    }

    public void updateHolderPosition(PositionDefinition positionDefinition, float width, float height) {
        setAlternativeScale(positionDefinition.getScale());
        setPosition(positionDefinition.getX(runesDifficultyDefinition.getGameDifficulty()) * width, positionDefinition.getY(runesDifficultyDefinition.getGameDifficulty()) * height);
    }

    public void setMaxEntities(int maxEntities) {
        this.maxEntities = maxEntities;
    }

    public boolean canFitOneMoreRune() {
        return (holderTargets.size() + wrongRunesCount) < maxEntities;
    }

    public boolean isFolded() {
        return folded;
    }

    public RunesHolderTypeDefinition getHolderTypeDefinition() {
        return holderTypeDefinition;
    }
}
