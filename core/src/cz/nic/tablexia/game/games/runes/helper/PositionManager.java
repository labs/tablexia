/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.games.runes.model.RunesDifficultyDefinition;


public class PositionManager {
    
    private RunesDifficultyDefinition runesDifficultyDefinition;
    private List<PositionDefinition> widePositions;
    private List<PositionDefinition> narrowPositions;

    public PositionManager(RunesDifficultyDefinition runesDifficultyDefinition) {
        this.runesDifficultyDefinition = runesDifficultyDefinition;
        widePositions = PositionDefinition.getBigPositions(runesDifficultyDefinition);
        narrowPositions = PositionDefinition.getSmallPositions(runesDifficultyDefinition);
    }
    
    public List<PositionDefinition> getRoundPositions(int round, TablexiaRandom tablexiaRandom){
        List<PositionDefinition> roundPositions = new ArrayList<>();
        int roundPositionsCount = runesDifficultyDefinition.getGroupsCountForRound(round);
        Collections.shuffle(widePositions, tablexiaRandom);
        Collections.shuffle(narrowPositions, tablexiaRandom);
        int wideCount = PositionDefinition.getBigPositionsForRound(round);
        roundPositions.addAll(widePositions.subList(0, wideCount));
        roundPositions.addAll(narrowPositions.subList(0, roundPositionsCount - wideCount));
        return roundPositions;
    }
}
