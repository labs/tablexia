/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.model;

import com.badlogic.gdx.graphics.Color;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.runes.actors.IRune;
import cz.nic.tablexia.game.games.runes.assets.RuneDefinition;
import cz.nic.tablexia.game.games.runes.assets.RunesGameAssets;
import cz.nic.tablexia.game.games.runes.helper.PositionDefinition;

/**
 * Created by Vitaliy Vashchenko on 3.6.16.
 */
public enum RunesDifficultyDefinition {

    DIFFICULTY_EASY(GameDifficulty.EASY, RunesGameAssets.BACKGROUND, RunesGameProperties.GROUPS_EASY, RuneDefinition.ALL_RUNES, TargetType.DEFAULT, false),
    DIFFICULTY_MEDIUM(GameDifficulty.MEDIUM, RunesGameAssets.BACKGROUND, RunesGameProperties.GROUPS_MEDIUM, RuneDefinition.ALL_RUNES, TargetType.CLUSTER, false),
    DIFFICULTY_HARD(GameDifficulty.HARD, RunesGameAssets.BACKGROUND, RunesGameProperties.GROUPS_HARD, RuneDefinition.MIRROR_RUNES, TargetType.DEFAULT, true),
    DIFFICULTY_BONUS(GameDifficulty.BONUS, RunesGameAssets.BACKGROUND, RunesGameProperties.GROUPS_EASY, RuneDefinition.ALL_RUNES, IRune.DEFAULT_COLOR_BONUS, TargetType.DEFAULT, false);

    private GameDifficulty gameDifficulty;
    private int[] groupsCount;
    private RuneDefinition[] runeDefinitions;
    private Color runesDefaultColor;
    private TargetType targetType;
    private boolean flipRunes;
    private String difficultyBackground;

    RunesDifficultyDefinition(GameDifficulty gameDifficulty, String difficultyBackground, int[] groupsCount, RuneDefinition[] runeDefinitions, Color runesDefaultColor, TargetType targetType, boolean flipRunes) {
        this.gameDifficulty = gameDifficulty;
        this.difficultyBackground = difficultyBackground;
        this.groupsCount = groupsCount;
        this.runeDefinitions = runeDefinitions;
        this.runesDefaultColor = runesDefaultColor;
        this.targetType = targetType;
        this.flipRunes = flipRunes;
    }

    RunesDifficultyDefinition(GameDifficulty gameDifficulty, String difficultyBackground, int[] groupsCount, RuneDefinition[] runeDefinitions, TargetType targetType, boolean flipRunes) {
        this(gameDifficulty, difficultyBackground, groupsCount, runeDefinitions, IRune.DEFAULT_COLOR, targetType, flipRunes);
    }

        public static RunesDifficultyDefinition getRunesDifficultyForGameDifficulty(GameDifficulty gameDifficulty) {
        for (RunesDifficultyDefinition runesDifficultyDefinition : RunesDifficultyDefinition.values()) {
            if (runesDifficultyDefinition.gameDifficulty == gameDifficulty) {
                return runesDifficultyDefinition;
            }
        }
        return RunesDifficultyDefinition.DIFFICULTY_EASY;
    }

    public String getDifficultyBackground() {
        return difficultyBackground;
    }

    public TargetType getTargetType() {
        return targetType;
    }

    public GameDifficulty getGameDifficulty() {
        return gameDifficulty;
    }

    public boolean isFlipRunes() {
        return flipRunes;
    }

    public int[] getGroupsCount() {
        return groupsCount;
    }

    public RuneDefinition[] getRuneDefinitions() {
        return runeDefinitions;
    }

    public Color getRunesDefaultColor() {
        return runesDefaultColor;
    }

    public int getGroupsCountForRound(int round) {
        return groupsCount[round];
    }

    public enum TargetType {
        DEFAULT(8),
        CLUSTER(12);

        int space;

        TargetType(int space) {
            this.space = space;
        }

        public int getSpace() {
            return space;
        }
    }
}
