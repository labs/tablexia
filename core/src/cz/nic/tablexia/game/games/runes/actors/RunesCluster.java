/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.runes.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import java.util.Collection;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.common.media.GfxLibrary;
import cz.nic.tablexia.game.games.runes.assets.RuneDefinition;
import cz.nic.tablexia.game.games.runes.helper.RuneDescription;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by Vitaliy Vashchenko on 27.4.16.
 */
public class RunesCluster extends Group implements IRune {

    private static final float OVERLAPPING = 0.5f;

    private Rune rune1;
    private Rune rune2;
    private boolean targetOnLeft;
    private Color defaultColor;

    public RunesCluster(Rune rune1, Rune rune2, TablexiaRandom random, Color defaultColor) {
        this.rune1 = rune1;
        this.rune2 = rune2;
        this.defaultColor = defaultColor;
        this.targetOnLeft = random.nextBoolean(); //target rune is on the left side of the cluster
        if (targetOnLeft) {
            addActor(rune1);
            rune2.setX(rune1.getWidth() * OVERLAPPING);
            addActor(rune2);
        } else {
            addActor(rune2);
            rune1.setX(rune2.getWidth() * OVERLAPPING);
            addActor(rune1);
        }
        setSize(getWidth(), getHeight());
    }

    public RunesCluster(Rune rune1, Rune rune2, TablexiaRandom random) {
        this(rune1, rune2, random, DEFAULT_COLOR);
    }


    public static RunesCluster createRuneCluster(GfxLibrary gfxLibrary, RuneDescription runeDescription, Collection<RuneDescription> exceptions, TablexiaRandom tablexiaRandom, Color runesColor) {
        return new RunesCluster(
                Rune.RunesFactory.createInstance(gfxLibrary, runeDescription),
                Rune.RunesFactory.createInstance(gfxLibrary, RuneDefinition.getRandomRuneDefinition(RuneDefinition.ALL_RUNES, exceptions, tablexiaRandom)),
                tablexiaRandom,
                runesColor);
    }

    @Override
    public boolean isFlipped() {
        return false;
    }

    @Override
    public float getWidth() {
        return rune1.getWidth() * OVERLAPPING + rune2.getWidth();
    }

    @Override
    public float getHeight() {
        if (rune1.getHeight() > rune2.getHeight()) return rune1.getHeight();
        else return rune2.getHeight();
    }

    @Override
    public RuneDefinition getType() {
        return rune1.getType();
    }

    @Override
    public void showAnimation(final AnimationType animationType) {
        if (!hasActions()) {
            addAction(Actions.parallel(Actions.run(new Runnable() {
                @Override
                public void run() {
                    animateRune(rune1, animationType);
                }
            }), Actions.run(new Runnable() {
                @Override
                public void run() {
                    animateRune(rune2, animationType);
                }
            })));

        }
    }

    private void animateRune(Rune rune, AnimationType animationType) {
        if (!rune.hasActions()) {
            rune.addAction(Actions.sequence(Actions.color(animationType.getColor()), Actions.color(DEFAULT_COLOR, animationType.getDuration())));
            rune.addAction(Actions.after(Actions.sequence(Actions.color(DEFAULT_COLOR), Actions.run(new Runnable() {
                @Override
                public void run() {
                    AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_ANIMATE_RUNE_DONE);
                }
            }))));
        }
    }

    //Get for testing

    public Rune getRune1() {
        return rune1;
    }

    public Rune getRune2() {
        return rune2;
    }
}
