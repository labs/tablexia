/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness.action.rule;

import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.action.InTheDarknessActionType.IActionFinishedListener;
import cz.nic.tablexia.game.games.in_the_darkness.action.ActionUtility;
import cz.nic.tablexia.game.games.in_the_darkness.creature.Player;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobstacle.MapObstacle;

/**
 * Rule for GO action
 * 
 * @author Matyáš Latner
 *
 */
public class GoActionRule extends AbstractActionTypeRule {

	@Override
    public void performActionRule(final InTheDarknessGame inTheDarknessGame, TileMap tileMap, Player player, int mapStartPositionX, int mapStartPositionY, final IActionFinishedListener finishedListener) {
        Player.PlayerOrientation actualPlayerOrientation = player.getActualPlayerOrientation();
        TileMapPosition actualTileMapPosition = player.getActualTileMapPosition();

        MapObstacle obstacleInDirection = ActionUtility.getObstacleInDirection(tileMap, actualTileMapPosition, actualPlayerOrientation);

        if (!ActionUtility.checkMoveDirection(tileMap, actualTileMapPosition, player.getNextTileMapPosition())) {
        	crashToTileMapPosition(inTheDarknessGame, true, player);
        	performOnActionFinishWithDelay(player, finishedListener, false, InTheDarknessGame.ERROR_ACTION_DELAY);
        } else if (obstacleInDirection != null && obstacleInDirection.isEnabled()){
        	crashToTileMapPosition(inTheDarknessGame, true, player);
			inTheDarknessGame.playRandomSoundFromGroup(obstacleInDirection.getMapObstacleType().getSoundGroup());
        	obstacleInDirection.reactWithPlayer();
        	performOnActionFinishWithDelay(player, finishedListener, false, InTheDarknessGame.ERROR_ACTION_DELAY);
        } else {
        	player.walkToTileMapPosition(inTheDarknessGame, mapStartPositionX, mapStartPositionY, null, new Runnable() {
				@Override
				public void run() {
					performOnActionFinished(finishedListener, true);
				}
			});
        }
    }

	private void crashToTileMapPosition(final InTheDarknessGame inTheDarknessGame, boolean withSound, Player player) {
		player.crashToTileMapPositionHalfWay(inTheDarknessGame, withSound, null, new Runnable() {
    		@Override
    		public void run() {
    			inTheDarknessGame.showCrashInfo();
    		}
    	});
	}
	
}
