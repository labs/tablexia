/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.in_the_darkness.map.mapobject;

import cz.nic.tablexia.game.games.in_the_darkness.assets.InTheDarknessAssets;

/**
 * Map object type definition
 * 
 * @author Matyáš Latner 
 *
 */
public enum MapObjectType {
	
	SAFE	(InTheDarknessAssets.OBJECT_SAFE, 	false, 	0.5f, 1f/4, 1f/4),
	KEY		(InTheDarknessAssets.OBJECT_KEY,	false, 	0.5f, 1f/4, 1f/4),
	STAIRS	(InTheDarknessAssets.OBJECT_STAIRS, true, 	1f,   0f, 	0f);
	
	private String 		texture;
	private boolean 	rotable;
	private float 		objectSizeRation;
	private final float xOffsetRatio;
	private final float yOffsetRatio;

	MapObjectType(String texture, boolean rotable, float objectSizeRation, float xOffsetRatio, float yOffsetRatio) {
		this.texture 			= texture;
		this.rotable 			= rotable;
		this.objectSizeRation 	= objectSizeRation;
		this.xOffsetRatio 		= xOffsetRatio;
		this.yOffsetRatio 		= yOffsetRatio;
	}

	public String getTexture() {
		return texture;
	}
	
	public boolean isRotable() {
		return rotable;
	}

	public float getXOffsetRatio() {
		return xOffsetRatio;
	}

	public float getYOffsetRatio() {
		return yOffsetRatio;
	}

	public float getObjectSizeRation() {
		return objectSizeRation;
	}

}
