/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness.assets;


public final class InTheDarknessAssets {
	
	private InTheDarknessAssets() {}

	public static final String GFX_PATH 			= "gfx/";

	public static final String BACKGROUND_PATH 		= GFX_PATH + "background/";
	public static final String BACKGROUND 			= BACKGROUND_PATH + "background";
	public static final String BACKGROUND_SMALL_MAP = BACKGROUND_PATH + "background_small_map";
	public static final String BACKGROUND_LARGE_MAP	= BACKGROUND_PATH + "background_large_map";
	public static final String BACKGROUND_ACTIONS	= BACKGROUND_PATH + "background_actions";

	public static final String OBJECTS_PATH 		= GFX_PATH + "objects/";
	public static final String OBJECT_KEY 			= OBJECTS_PATH + "key";
	public static final String OBJECT_SAFE 			= OBJECTS_PATH + "safe";
	public static final String OBJECT_STAIRS		= OBJECTS_PATH + "stairs";

	public static final String TILES_PATH 			= GFX_PATH + "tiles/";
	public static final String TILE_TILE0 			= TILES_PATH + "tile0";
	public static final String TILE_TILE1 			= TILES_PATH + "tile1";
	public static final String TILE_TILE2I 			= TILES_PATH + "tile2I";
	public static final String TILE_TILE2L 			= TILES_PATH + "tile2L";
	public static final String TILE_TILE3 			= TILES_PATH + "tile3";
	public static final String TILE_TILE4 			= TILES_PATH + "tile4";

	public static final String OBSTACLES_PATH 		= GFX_PATH + "obstacles/";
	public static final String OBSTACLE_DOG_ATTACK 	= OBSTACLES_PATH + "dog_attack";
	public static final String OBSTACLE_DOG_SLEEP 	= OBSTACLES_PATH + "dog_sleep";
	public static final String OBSTACLE_DOOR_CLOSED	= OBSTACLES_PATH + "door_closed";
	public static final String OBSTACLE_DOOR_OPENED	= OBSTACLES_PATH + "door_opened";

	public static final String ACTIONS_PATH 		= GFX_PATH + "actions/";
	public static final String ACTION_DOG 			= ACTIONS_PATH + "dog";
	public static final String ACTION_DOOR 			= ACTIONS_PATH + "door";
	public static final String ACTION_GO 			= ACTIONS_PATH + "go";
	public static final String ACTION_KEY 			= ACTIONS_PATH + "key";
	public static final String ACTION_LEFT 			= ACTIONS_PATH + "left";
	public static final String ACTION_RIGHT			= ACTIONS_PATH + "right";
	public static final String ACTION_STAIRS		= ACTIONS_PATH + "stairs";

	public static final String CONTROLS_PATH 		= GFX_PATH + "control/";
	public static final String CONTROL_ACTUAL 		= CONTROLS_PATH + "actual";
	public static final String CONTROL_KEY	 		= CONTROLS_PATH + "key";
	public static final String CONTROL_NEXT	 		= CONTROLS_PATH + "next";

	public static final String INFOS_PATH 			= GFX_PATH + "info/";
	public static final String INFO_CRASH 			= INFOS_PATH + "crash";
	public static final String INFO_SAFE1 			= INFOS_PATH + "safe1";
	public static final String INFO_SAFE2 			= INFOS_PATH + "safe2";
	public static final String INFO_START_ARROW		= INFOS_PATH + "start_arrow";

	public static final String 	CREATURES_PATH 					= GFX_PATH + "creature/";
	public static final String 	CREATURE_PLAYER_CRASH			= CREATURES_PATH + "player_crash";
	public static final String 	CREATURE_PLAYER_JUMP			= CREATURES_PATH + "player_jump";
	public static final String 	CREATURE_PLAYER_WALK			= CREATURES_PATH + "player_walk";
	public static final int 	CREATURE_PLAYER_CRASH_FRAMES	= 7;
	public static final int 	CREATURE_PLAYER_JUMP_FRAMES		= 6;
	public static final int 	CREATURE_PLAYER_WALK_FRAMES		= 8;

	public static final String SFX_PATH 			= "common/sfx/";
	public static final String SOUND_BARK1 			= SFX_PATH + "bark1.mp3";
	public static final String SOUND_BARK2 			= SFX_PATH + "bark2.mp3";
	public static final String SOUND_BUMP1 			= SFX_PATH + "bump1.mp3";
	public static final String SOUND_BUMP2 			= SFX_PATH + "bump2.mp3";
	public static final String SOUND_DOOR 			= SFX_PATH + "door.mp3";
	public static final String SOUND_ERROR 			= SFX_PATH + "error.mp3";
	public static final String SOUND_JUMP1 			= SFX_PATH + "jump1.mp3";
	public static final String SOUND_JUMP2 			= SFX_PATH + "jump2.mp3";
	public static final String SOUND_KEY 			= SFX_PATH + "key.mp3";
	public static final String SOUND_SAVE 			= SFX_PATH + "safe.mp3";
	public static final String SOUND_STEPS1			= SFX_PATH + "steps1.mp3";
	public static final String SOUND_STEPS2			= SFX_PATH + "steps2.mp3";
	public static final String SOUND_STEPS3			= SFX_PATH + "steps3.mp3";

	public static final String SOUND_SCORE_0		= SFX_PATH + "score_0.mp3";
	public static final String SOUND_SCORE_1		= SFX_PATH + "score_1.mp3";
	public static final String SOUND_SCORE_2		= SFX_PATH + "score_2.mp3";
	public static final String SOUND_SCORE_3		= SFX_PATH + "score_3.mp3";
	public static final String SOUND_SCORE_TUTORIAL = SFX_PATH + "score_tutorial.mp3";

	public static final String[] SOUND_BUMPS 		= new String[]{SOUND_BUMP1, SOUND_BUMP2};
	public static final String[] SOUND_BARKS 		= new String[]{SOUND_BARK1, SOUND_BARK2};
	public static final String[] SOUND_JUMPS 		= new String[]{SOUND_JUMP1, SOUND_JUMP2};
	public static final String[] SOUND_STEPS 		= new String[]{SOUND_STEPS1, SOUND_STEPS2, SOUND_STEPS3};
}