/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness.creature;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.assets.InTheDarknessAssets;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.in_the_darkness.map.tile.TileType;
import cz.nic.tablexia.util.ui.AnimatedImage;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.rotateTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Player sprite
 * 
 * @author Matyáš Latner
 */
public class Player extends Group {

	private static final float 	            PLAYER_WALK_SOUND_PITCH			= 2f;
	private static final float 	            PLAYER_JUMP_SCALE 				= 1.5f;
    private static final Interpolation.Pow  PLAYER_JUMP_INTERPOLATION       = Interpolation.pow2;
    private static final float              PLAYER_ANIMATION_FRAME_DURATION = 0.1f;

    public enum PlayerOrientation {

        TOP		(0, 1),
		RIGHT	(1, 1),
        BOTTOM	(2, -1),
        LEFT	(3, -1);

        private int rotation;
		private int directionMultiplier;

        PlayerOrientation(int rotation, int directionMultiplier) {
            this.rotation = rotation;
			this.directionMultiplier = directionMultiplier;
        }

        public int getRotation() {
            return rotation;
        }
        
        public int getDirectionMultiplier() {
			return directionMultiplier;
		}
        
        private boolean isHorizontal() {
        	return this == LEFT || this == RIGHT;
		}
    }

    private static final int               	PLAYER_WIDTH        = 55;
    private static final int               	PLAYER_HEIGHT       = 55;
    
    private static final int               	QUARTAL_ROTATION    = 90;

    private AnimatedImage 					actualPlayerAnimatedImage;
    private AnimatedImage                   playerWalkAnimatedImage;
    private AnimatedImage 					playerJumpAnimatedImage;
    private AnimatedImage 					playerCrashAnimatedImage;
    
    
    private PlayerOrientation              	actualPlayerOrientation;
    private TileMapPosition                	actualTileMapPosition;
    private int                            	actualFloor;

    private boolean                        	hasKey;

    public Player(int actualFloor, InTheDarknessGame inTheDarknessGame) {
        this.actualFloor = actualFloor;
        setOrigin(Align.center);
        
        actualPlayerOrientation = PlayerOrientation.TOP;
        
        playerWalkAnimatedImage = createPlayerAnimatedImage(PLAYER_WIDTH, PLAYER_HEIGHT, InTheDarknessAssets.CREATURE_PLAYER_WALK, InTheDarknessAssets.CREATURE_PLAYER_WALK_FRAMES, inTheDarknessGame);
        playerJumpAnimatedImage = createPlayerAnimatedImage(PLAYER_WIDTH, PLAYER_HEIGHT, InTheDarknessAssets.CREATURE_PLAYER_JUMP, InTheDarknessAssets.CREATURE_PLAYER_JUMP_FRAMES, inTheDarknessGame);
        playerCrashAnimatedImage = createPlayerAnimatedImage(PLAYER_WIDTH, PLAYER_HEIGHT, InTheDarknessAssets.CREATURE_PLAYER_CRASH, InTheDarknessAssets.CREATURE_PLAYER_CRASH_FRAMES, inTheDarknessGame);
        
        replacePlayerSprite(playerWalkAnimatedImage);

        hasKey = false;
    }

    public void setActualFloor(int actualFloor) {
        this.actualFloor = actualFloor;
    }

    public int getActualFloor() {
        return actualFloor;
    }

    public void setKey(boolean hasKey) {
        this.hasKey = hasKey;
    }

    public boolean hasKey() {
        return hasKey;
    }

    /**
     * Set player direction to one of possible directions in current player tile.
     * In priority RIGHT, TOP, BOTTOM, LEFT
     */
    public void setPossibleDirection(TileMap tileMap) {
    	if (actualTileMapPosition != null) {
    		TileType tileType = tileMap.getTileAtPosition(actualTileMapPosition).getTileType();
    		if (tileType.isLeftDoor()) {
    			setPlayerOrientation(PlayerOrientation.LEFT, null, false);
    		} else if (tileType.isTopDoor()) {
    			setPlayerOrientation(PlayerOrientation.TOP, null, false);
    		} else if (tileType.isBottomDoor()) {
    			setPlayerOrientation(PlayerOrientation.BOTTOM, null, false);
    		} else if (tileType.isRightDoor()) {
    			setPlayerOrientation(PlayerOrientation.RIGHT, null, false);
    		}
    	}
    }
    
    public void crashToTileMapPositionHalfWay(final InTheDarknessGame inTheDarknessGame, final boolean withSound, final Runnable startHandler, final Runnable finishHandler) {
    	final AnimatedImage lastAnimatedImage = actualPlayerAnimatedImage;
    	replacePlayerSprite(playerCrashAnimatedImage);
        moveHalfWay(new Runnable() {
                        @Override
                        public void run() {
                            if (withSound) {
                                inTheDarknessGame.playRandomSoundFromGroup(InTheDarknessAssets.SOUND_BUMPS);
    				        }
                            if (startHandler != null) {
                                startHandler.run();
                            }
                        }
                    },
                    new Runnable() {
                        @Override
                        public void run() {
                            replacePlayerSprite(lastAnimatedImage);
                            if (finishHandler != null) {
                                finishHandler.run();
                            }
                        }
                    });
    }
    
    public void jumpToTileMapPosition(final InTheDarknessGame inTheDarknessGame, int mapStartPositionX, int mapStartPositionY, final Runnable startHandler, final Runnable finishHandler) {
    	final AnimatedImage lastAnimatedImage = actualPlayerAnimatedImage;
    	replacePlayerSprite(playerJumpAnimatedImage);

        moveToTileMapPosition(mapStartPositionX, mapStartPositionY, new Runnable() {
                    @Override
                    public void run() {
                        actualPlayerAnimatedImage.addAction(sequence(scaleTo(PLAYER_JUMP_SCALE, PLAYER_JUMP_SCALE, InTheDarknessGame.ANIMATION_DURATION / 2, PLAYER_JUMP_INTERPOLATION),
                                scaleTo(1, 1, InTheDarknessGame.ANIMATION_DURATION / 2, Interpolation.pow2)));
                        inTheDarknessGame.playRandomSoundFromGroup(InTheDarknessAssets.SOUND_JUMPS, PLAYER_WALK_SOUND_PITCH);
                        if (startHandler != null) {
                            startHandler.run();
                        }
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        replacePlayerSprite(lastAnimatedImage);
                        if (finishHandler != null) {
                            finishHandler.run();
                        }
                    }
                });
	}
    
    public void walkToTileMapPosition(final InTheDarknessGame inTheDarknessGame, int mapStartPositionX, int mapStartPositionY, final Runnable startHandler, final Runnable finishHandler) {
    	replacePlayerSprite(playerWalkAnimatedImage);
        moveToTileMapPosition(mapStartPositionX, mapStartPositionY, new Runnable() {
                    @Override
                    public void run() {
                        if (startHandler != null) {
                            startHandler.run();
                        }
                        inTheDarknessGame.playRandomSoundFromGroup(InTheDarknessAssets.SOUND_STEPS, PLAYER_WALK_SOUND_PITCH);
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        if (finishHandler != null) {
                            finishHandler.run();
                        }
                    }
                });
	}
    
    private void moveToTileMapPosition(int mapStartPositionX, int mapStartPositionY, final Runnable startHandler, final Runnable finishHandler) {
        TileMapPosition nextTileMapPosition = getNextTileMapPosition();
        float finishDestinationX = getPlayerPositionXForTileMapPosition(mapStartPositionX, nextTileMapPosition);
        float finishDestinationY = getPlayerPositionYForTileMapPosition(mapStartPositionY, nextTileMapPosition);
        actualTileMapPosition = new TileMapPosition(nextTileMapPosition.getPositionX(), nextTileMapPosition.getPositionY());

        move(InTheDarknessGame.ANIMATION_DURATION, finishDestinationX, finishDestinationY, startHandler, finishHandler, false);
    }

    private void moveHalfWay(final Runnable startHandler, final Runnable finishHandler) {
        int halfWayOffset = actualPlayerOrientation.getDirectionMultiplier() * (InTheDarknessGame.TILE_SIZE / 2 - InTheDarknessGame.TILE_SIZE / 10);
        float finishDestinationX = getX() + (actualPlayerOrientation.isHorizontal() ? halfWayOffset : 0);
        float finishDestinationY = getY() + (!actualPlayerOrientation.isHorizontal() ? halfWayOffset : 0);

        move(InTheDarknessGame.ANIMATION_DURATION / 2, finishDestinationX, finishDestinationY, startHandler, finishHandler, true);
    }

    private void move(float moveDuration, float finishDestinationX, float finishDestinationY, final Runnable startHandler, final Runnable finishHandler, final boolean waitToAnimation) {
        addAction(sequence(parallel(run(new Runnable() {
                            @Override
                            public void run() {
                                if (startHandler != null) {
                                    startHandler.run();
                                }
                                if (waitToAnimation) {
                                    actualPlayerAnimatedImage.startAnimation(new Runnable() {
                                        @Override
                                        public void run() {
                                            finishMove(finishHandler);
                                        }
                                    });
                                } else {
                                    actualPlayerAnimatedImage.startAnimationLoop();
                                }
                            }
                        }),
                        moveTo(finishDestinationX, finishDestinationY, moveDuration)),
                run(new Runnable() {
                    @Override
                    public void run() {
                        if (!waitToAnimation) {
                            finishMove(finishHandler);
                        }
                    }
                })));
    }

    private void finishMove(Runnable finishHandler) {
        actualPlayerAnimatedImage.stopAnimation();
        if (finishHandler != null) {
            finishHandler.run();
        }
    }

    public boolean resetPlayerMapPosition(TileMap tileMap, int mapStartPositionX, int mapStartPositionY) {
        if (tileMap.isTileAtPosition(tileMap.getMapStartPosition().getPositionX(), tileMap.getMapStartPosition().getPositionY())) {
            setPosition(getPlayerPositionXForTileMapPosition(mapStartPositionX, tileMap.getMapStartPosition()),
                        getPlayerPositionYForTileMapPosition(mapStartPositionY, tileMap.getMapStartPosition()));
            actualTileMapPosition = new TileMapPosition(tileMap.getMapStartPosition().getPositionX(), tileMap.getMapStartPosition().getPositionY());
            return true;
        } else {
            return false;
        }
    }

    private float getPlayerPositionXForTileMapPosition(int mapStartPositionX, TileMapPosition tileMapPosition) {
        return mapStartPositionX + (tileMapPosition.getPositionX() * InTheDarknessGame.TILE_SIZE) + (InTheDarknessGame.TILE_SIZE / 2) - (actualPlayerAnimatedImage.getWidth() / 2);
    }

    private float getPlayerPositionYForTileMapPosition(int mapStartPositionY, TileMapPosition tileMapPosition) {
        return mapStartPositionY + ((InTheDarknessGame.mapSizeY - tileMapPosition.getPositionY() - 1) * InTheDarknessGame.TILE_SIZE) + (InTheDarknessGame.TILE_SIZE / 2) - (actualPlayerAnimatedImage.getHeight() / 2);
    }

    private AnimatedImage createPlayerAnimatedImage(int width, int height, String animationName, int framesCount, InTheDarknessGame inTheDarknessGame) {
		AnimatedImage playerAnimatedImage = new AnimatedImage(inTheDarknessGame.getScreenAnimation(animationName, framesCount, PLAYER_ANIMATION_FRAME_DURATION), false);
		playerAnimatedImage.setSize(width, height);
    	playerAnimatedImage.setVisible(false);
        playerAnimatedImage.setOrigin(Align.center);
        playerAnimatedImage.setRotation(0);
        addActor(playerAnimatedImage);
        return playerAnimatedImage;
    }

    public void turnPlayerLeft(Runnable finishHandler) {
        int nextPlayerOrientationNumber = actualPlayerOrientation.ordinal() - 1;
        if (nextPlayerOrientationNumber < 0) {
            nextPlayerOrientationNumber = PlayerOrientation.values().length + nextPlayerOrientationNumber;
        }
        setPlayerOrientation(PlayerOrientation.values()[nextPlayerOrientationNumber], finishHandler, true);
    }

    public void turnPlayerRight(Runnable finishHandler) {
        int nextPlayerOrientationNumber = actualPlayerOrientation.ordinal() + 1;
        if (nextPlayerOrientationNumber >= PlayerOrientation.values().length) {
            nextPlayerOrientationNumber = nextPlayerOrientationNumber % PlayerOrientation.values().length;
        }
        setPlayerOrientation(PlayerOrientation.values()[nextPlayerOrientationNumber], finishHandler, true);
    }

    public TileMapPosition getActualTileMapPosition() {
        return actualTileMapPosition;
    }

    /**
     * Return next tile map position for current player direction
     */
    public TileMapPosition getNextTileMapPosition() {
        switch (actualPlayerOrientation) {
            case TOP:
                return new TileMapPosition(actualTileMapPosition.getPositionX(), actualTileMapPosition.getPositionY() - 1);
            case RIGHT:
                return new TileMapPosition(actualTileMapPosition.getPositionX() + 1, actualTileMapPosition.getPositionY());
            case LEFT:
                return new TileMapPosition(actualTileMapPosition.getPositionX() - 1, actualTileMapPosition.getPositionY());
            case BOTTOM:
                return new TileMapPosition(actualTileMapPosition.getPositionX(), actualTileMapPosition.getPositionY() + 1);
        }
        return null;
    }

    public PlayerOrientation getActualPlayerOrientation() {
        return actualPlayerOrientation;
    }

    private void setPlayerOrientation(PlayerOrientation playerOrientation, final Runnable finishHandler, boolean animated) {
    	int toRotation = playerOrientation.rotation * QUARTAL_ROTATION;
        if (animated) {
        	int fromRotation = actualPlayerOrientation.rotation * QUARTAL_ROTATION;
        	int fullCircle = PlayerOrientation.values().length * QUARTAL_ROTATION;
        	if (actualPlayerOrientation == PlayerOrientation.LEFT && playerOrientation == PlayerOrientation.TOP) {
        		toRotation = fullCircle;
        	} else if (actualPlayerOrientation == PlayerOrientation.TOP && playerOrientation == PlayerOrientation.LEFT) {
        		fromRotation = fullCircle;
            }

            actualPlayerAnimatedImage.setRotation(-fromRotation);
            actualPlayerAnimatedImage.addAction(sequence(rotateTo(-toRotation, InTheDarknessGame.ANIMATION_DURATION), run(new Runnable() {
                @Override
                public void run() {
                    finishHandler.run();
                }
            })));
    	} else {    		
    		actualPlayerAnimatedImage.setRotation(-toRotation);
    	}
        actualPlayerOrientation = playerOrientation;
    }

    private void replacePlayerSprite(AnimatedImage playerAnimatedImage) {
    	if (actualPlayerAnimatedImage != null) {
    		actualPlayerAnimatedImage.setVisible(false);
    	}
    	actualPlayerAnimatedImage = playerAnimatedImage;
        actualPlayerAnimatedImage.setRotation(-actualPlayerOrientation.rotation * QUARTAL_ROTATION);
    	actualPlayerAnimatedImage.setVisible(true);
    }
}
