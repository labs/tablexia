/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.in_the_darkness.map.mapobstacle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.assets.InTheDarknessAssets;

/**
 * Map obstacle type definition
 * 
 * @author Matyáš Latner 
 *
 */
public enum MapObstacleType {
	
	DOOR_H(InTheDarknessAssets.OBSTACLE_DOOR_CLOSED, InTheDarknessAssets.OBSTACLE_DOOR_OPENED, 0, InTheDarknessAssets.SOUND_BUMPS, new ArrayList<MapObstaclePosition>(Arrays.asList(new MapObstaclePosition[] {MapObstaclePosition.BOTTOM_POSITION})), 0, 0, 1, false),
	DOOR_V(InTheDarknessAssets.OBSTACLE_DOOR_CLOSED, InTheDarknessAssets.OBSTACLE_DOOR_OPENED, 1, InTheDarknessAssets.SOUND_BUMPS, new ArrayList<MapObstaclePosition>(Arrays.asList(new MapObstaclePosition[] {MapObstaclePosition.LEFT_POSITION})), 0, 0, 1, false),
	DOG_H (InTheDarknessAssets.OBSTACLE_DOG_SLEEP,   InTheDarknessAssets.OBSTACLE_DOG_ATTACK, 0, InTheDarknessAssets.SOUND_BARKS, new ArrayList<MapObstaclePosition>(Arrays.asList(new MapObstaclePosition[] {MapObstaclePosition.BOTTOM_POSITION})), 1f/4, 1f/4, 1.5f, true),
	DOG_V (InTheDarknessAssets.OBSTACLE_DOG_SLEEP,   InTheDarknessAssets.OBSTACLE_DOG_ATTACK, 3, InTheDarknessAssets.SOUND_BARKS, new ArrayList<MapObstaclePosition>(Arrays.asList(new MapObstaclePosition[] {MapObstaclePosition.LEFT_POSITION})), 1f/4, 1f/4, 1.5f, true);
	
	public enum MapObstaclePosition {
		
		LEFT_POSITION(-InTheDarknessGame.TILE_SIZE / 2, 0),
		BOTTOM_POSITION(0, -InTheDarknessGame.TILE_SIZE / 2);
		
		private int xOffset;
		private int yOffset;
		
		MapObstaclePosition(int xOffset, int yOffset) {
			this.xOffset = xOffset;
			this.yOffset = yOffset;
		}
		
		public int getXOffset() {
			return xOffset;
		}
		
		public int getYOffset() {
			return yOffset;
		}
	}
	
	private String 						activeTexture;
	private String 						inactiveTexture;
	private int 						rotation;
	private List<MapObstaclePosition> 	positions;
	private String[] 					soundGroup;
	private final float 				correctionOffsetX;
	private final float 				correctionOffsetY;
	private float 						obstacleSizeRation;
	private boolean 					reactOnPlayer;

	/**
	 * Map obstacle type definition
	 */
	MapObstacleType(String activeTexture,
					String inactiveTexture,
					int rotation,
					String[] soundGroup,
					List<MapObstaclePosition> obstaclePosition,
					float correctionOffsetX,
					float correctionOffsetY,
					float obstacleSizeRatio,
					boolean reactOnPlayer) {

		this.activeTexture 		= activeTexture;
		this.inactiveTexture 	= inactiveTexture;
		this.rotation 			= rotation;
		this.soundGroup 		= soundGroup;
		this.positions 			= obstaclePosition;
		this.correctionOffsetX 	= correctionOffsetX;
		this.correctionOffsetY 	= correctionOffsetY;
		this.obstacleSizeRation = obstacleSizeRatio;
		this.reactOnPlayer 		= reactOnPlayer;
	}

	public String getActiveTexture() {
		return activeTexture;
	}
	
	public String getInactiveTexture() {
		return inactiveTexture;
	}
	
	public int getRotation() {
		return rotation;
	}
	
	public String[] getSoundGroup() {
		return soundGroup;
	}

	public float getCorrectionOffsetX() {
		return correctionOffsetX;
	}

	public float getCorrectionOffsetY() {
		return correctionOffsetY;
	}

	public float getObstacleSizeRatio() {
		return obstacleSizeRation;
	}
	
	public List<MapObstaclePosition> getObstacleAvailablePositions() {
		return positions;
	}
	
	public MapObstaclePosition getObstacleRandomAvailablePosition(Random random) {
		return positions.get(random.nextInt(positions.size()));
	}

	public boolean reactOnPlayer() {
		return reactOnPlayer;
	}

	/**
	 * Return map obstacle types for position in parameter
	 * @param positions one or more positions
	 * @return obstacle types for positions in parameter
	 */
	public static List<MapObstacleType> getMapObstacleForPosition(MapObstaclePosition... positions) {
		List<MapObstacleType> result = new ArrayList<MapObstacleType>();
		for (MapObstacleType mapObstacleType : values()) {
			for (MapObstaclePosition position : positions) {				
				if (mapObstacleType.positions.contains(position)) {
					result.add(mapObstacleType);
					break;
				}
			}
		}
		return result;
	}

}
