/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness.map;

import cz.nic.tablexia.game.games.in_the_darkness.assets.InTheDarknessAssets;

public enum InTheDarknessMap {

    SMALL_MAP(InTheDarknessAssets.BACKGROUND_SMALL_MAP, 5, 6),
    LARGE_MAP(InTheDarknessAssets.BACKGROUND_LARGE_MAP, 7, 6);

    private String mapTexture;
    private int sizeX;
    private int sizeY;

    InTheDarknessMap(String mapTexture, int sizeX, int sizeY) {
        this.mapTexture = mapTexture;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }

    public String getMapTexture() {
        return mapTexture;
    }

    public int getSizeY() {
        return sizeY;
    }

    public int getSizeX() {
        return sizeX;
    }
}
