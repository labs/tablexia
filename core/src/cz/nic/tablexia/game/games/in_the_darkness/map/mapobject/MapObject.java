/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.in_the_darkness.map.mapobject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.map.tile.Tile;


/**
 * Map object
 * 
 * @author Matyáš Latner
 *
 */
public class MapObject {
	
	private static final int 	QUARTER_ROTATION 	= 90;

	
	private MapObjectType 		mapObjectType;
	private Image 				mapObjectImage;
	private Tile 				tile;
	private boolean				enabled;

	public MapObject(Tile tile, MapObjectType mapObjectType) {
		this.tile = tile;
		this.mapObjectType = mapObjectType;
		setEnable(true);
	}

	public MapObjectType getMapObjectType() {
		return mapObjectType;
	}
	
	@Override
	public String toString() {
		return "MapObject[" + mapObjectType.name() + "]";
	}
	
	public void setEnable(boolean enabled) {
		this.enabled = enabled;
		if (mapObjectImage != null) {
			mapObjectImage.setVisible(enabled);
		}
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	/**
	 * Removes current object from map
	 */
	public void removeFromMap() {
		if (mapObjectImage != null) {
			mapObjectImage.setVisible(false);
			Gdx.app.postRunnable(new Runnable() {
				@Override
				public void run() {
					mapObjectImage.remove();
					mapObjectImage = null;
				}
			});
		}
	}
	
	public Image getMapObjectSprite(InTheDarknessGame inTheDarknessGame) {
		if (mapObjectImage == null) {
			mapObjectImage = new Image(inTheDarknessGame.getScreenTextureRegion(mapObjectType.getTexture()));
			mapObjectImage.setSize(InTheDarknessGame.TILE_SIZE * mapObjectType.getObjectSizeRation(), InTheDarknessGame.TILE_SIZE * mapObjectType.getObjectSizeRation());

			MapObjectType mapObjectType = tile.getMapObject().getMapObjectType();
			Image tileImage = tile.getTileSprite(inTheDarknessGame);
			float tileImageWidth = tileImage.getWidth();
			float tileImageHeight = tileImage.getHeight();

			mapObjectImage.setPosition((tile.getMapPositionX() * tileImageWidth) + (tileImageWidth * mapObjectType.getXOffsetRatio()), (InTheDarknessGame.mapSizeY * InTheDarknessGame.TILE_SIZE) - (((tile.getMapPositionY() + 1) * tileImageHeight) - (tileImageHeight * mapObjectType.getYOffsetRatio())));
			if (getMapObjectType().isRotable()) {
				mapObjectImage.setOrigin(Align.center);
				mapObjectImage.setRotation(-tile.getTileType().getRotation() * QUARTER_ROTATION);
			}
		}
		return mapObjectImage;
	}

}
