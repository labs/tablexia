/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.games.in_the_darkness.map.mapobstacle;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.map.tile.Tile;


/**
 * Map obstacle
 * 
 * @author Matyáš Latner
 *
 */
public class MapObstacle {

	private static final int 	QUARTER_ROTATION 			= 90;
	
	private MapObstacleType 	mapObstacleType;
	private Group 				mapObstacleContainer;
	private Image 				mapObstacleActiveSprite;
	private Image				mapObstacleInactiveSprite;
	private Tile 				tile;

	private int 				offsetX;
	private int 				offsetY;

	private boolean				active;

	public MapObstacle(Tile tile, int offsetX, int offsetY, MapObstacleType mapObstacleType) {
		this.tile = tile;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.mapObstacleType = mapObstacleType;
		this.active = true;
		this.mapObstacleContainer = new Group();
	}
	
	public MapObstacleType getMapObstacleType() {
		return mapObstacleType;
	}
	
	public boolean isEnabled() {
		return active;
	}
	
	public void setActive() {
		this.active = true;
		if (mapObstacleActiveSprite != null) {
			mapObstacleContainer.clearChildren();
			mapObstacleContainer.addActor(mapObstacleActiveSprite);
		}
	}
	
	public void setInactive() {
		this.active = false;
		if (mapObstacleInactiveSprite != null) {			
			mapObstacleContainer.clearChildren();
			mapObstacleContainer.addActor(mapObstacleInactiveSprite);
		}
	}
	
	public void reactWithPlayer() {
		if (getMapObstacleType().reactOnPlayer()) {
			setInactive();
		}
	}
	
	@Override
	public String toString() {
		return "MapObstacle[" + mapObstacleType.name() + "]";
	}
	
	private Image prepareImage(String texture, InTheDarknessGame inTheDarknessGame) {
		Image image = new Image(inTheDarknessGame.getScreenTextureRegion(texture));
		image.setSize(InTheDarknessGame.TILE_SIZE / mapObstacleType.getObstacleSizeRatio(), InTheDarknessGame.TILE_SIZE / mapObstacleType.getObstacleSizeRatio());
		
		Image tileImage = tile.getTileSprite(inTheDarknessGame);
		float tileImageWidth = tileImage.getWidth();
		float tileImageHeight = tileImage.getHeight();
		
		image.setPosition((tile.getMapPositionX() * tileImageWidth) + offsetX + (image.getWidth() * getMapObstacleType().getCorrectionOffsetX()),
				 		  (InTheDarknessGame.mapSizeY * InTheDarknessGame.TILE_SIZE) - ((tile.getMapPositionY() + 1) * tileImageHeight) + offsetY  + (image.getHeight() * getMapObstacleType().getCorrectionOffsetY()));
		image.setOrigin(Align.center);
		image.setRotation(-mapObstacleType.getRotation() * QUARTER_ROTATION);
		return image;
	}
	
	private void prepareObstacleActiveImage(InTheDarknessGame inTheDarknessGame) {
		if (mapObstacleActiveSprite == null && mapObstacleType.getActiveTexture() != null) {
			mapObstacleActiveSprite = prepareImage(mapObstacleType.getActiveTexture(), inTheDarknessGame);
		}
	}
	
	private void prepareObstacleInactiveImage(InTheDarknessGame inTheDarknessGame) {
		if (mapObstacleInactiveSprite == null && mapObstacleType.getInactiveTexture() != null) {
			mapObstacleInactiveSprite = prepareImage(mapObstacleType.getInactiveTexture(), inTheDarknessGame);
		}
	}
	
	public Group getMapObstacleContainer(InTheDarknessGame inTheDarknessGame) {
		prepareObstacleActiveImage(inTheDarknessGame);
		prepareObstacleInactiveImage(inTheDarknessGame);
		return mapObstacleContainer;
	}

}
