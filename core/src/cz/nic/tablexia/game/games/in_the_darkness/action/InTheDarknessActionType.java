/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness.action;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessDifficulty;
import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.action.rule.AbstractActionTypeRule;
import cz.nic.tablexia.game.games.in_the_darkness.action.rule.DogActionRule;
import cz.nic.tablexia.game.games.in_the_darkness.action.rule.DoorActionRule;
import cz.nic.tablexia.game.games.in_the_darkness.action.rule.GoActionRule;
import cz.nic.tablexia.game.games.in_the_darkness.action.rule.KeyActionRule;
import cz.nic.tablexia.game.games.in_the_darkness.action.rule.LeftActionRule;
import cz.nic.tablexia.game.games.in_the_darkness.action.rule.RightActionRule;
import cz.nic.tablexia.game.games.in_the_darkness.action.rule.StairsActionRule;
import cz.nic.tablexia.game.games.in_the_darkness.assets.InTheDarknessAssets;
import cz.nic.tablexia.game.games.in_the_darkness.creature.Player;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap;
import cz.nic.tablexia.util.Log;

/**
 * Action type definition
 * 
 * @author Matyáš Latner
 *
 */
public enum InTheDarknessActionType {
	
	GO		(0, InTheDarknessAssets.ACTION_GO, 		GoActionRule.class, 	0, false),
	RIGHT	(1, InTheDarknessAssets.ACTION_RIGHT, 	RightActionRule.class, 	0, false),
	LEFT	(2, InTheDarknessAssets.ACTION_LEFT, 	LeftActionRule.class, 	0, false),
	DOOR	(3, InTheDarknessAssets.ACTION_DOOR, 	DoorActionRule.class, 	0, false),
	DOG		(4, InTheDarknessAssets.ACTION_DOG, 	DogActionRule.class, 	0, false),
	KEY		(5, InTheDarknessAssets.ACTION_KEY, 	KeyActionRule.class, 	0, true),
	STAIRS	(6, InTheDarknessAssets.ACTION_STAIRS, 	StairsActionRule.class, 2, false);

	public interface IActionFinishedListener {
		
		void onActionFinished(boolean result);
		
	}

	private int 					actionNumber;
	private String 					texture;
	private AbstractActionTypeRule 	actionTypeRule;
	private int 					minimumFloorCount;
	private boolean 				mustBeInDifficulty;

	InTheDarknessActionType(int actionNumber, String texture, Class<? extends AbstractActionTypeRule> clazz, int minimumFloorCount, boolean mustBeInDifficulty) {
		this.actionNumber = actionNumber;
		this.texture = texture;
		this.minimumFloorCount = minimumFloorCount;
		this.mustBeInDifficulty = mustBeInDifficulty;
		
		try {
			actionTypeRule = clazz.newInstance();
        } catch (Exception e) {
            Log.err(getClass(), "Cannot create action type: " + name(), e);
        }
	}

	public int getActionNumber() {
		return actionNumber;
	}

	public String getTexture() {
		return texture;
	}
	
	/**
     * Performs rule assigned to specific action
     * 
     * @return <code>true</code> if rule was successfully
     */
    public void performActionTypeRule(InTheDarknessGame inTheDarknessGame, TileMap tileMap, Player player, int mapStartPositionX, int mapStartPositionY, IActionFinishedListener finishedListener) {
    	actionTypeRule.performActionRule(inTheDarknessGame, tileMap, player, mapStartPositionX, mapStartPositionY, finishedListener);
    }
    
    /**
     * Returns all action types which have different difficulty than the difficulty from parameter
     * 
     * @param inTheDarknessDifficulty
     * @return
     */
    public static List<InTheDarknessActionType> getActionTypesForGameDifficulty(InTheDarknessDifficulty inTheDarknessDifficulty) {
    	List<InTheDarknessActionType> result = new ArrayList<InTheDarknessActionType>();
    	for (InTheDarknessActionType inTheDarknessActionType : InTheDarknessActionType.values()) {
			if ((inTheDarknessActionType.minimumFloorCount == 0 || inTheDarknessDifficulty.getFloorCount() >= inTheDarknessActionType.minimumFloorCount) &&
				(!inTheDarknessActionType.mustBeInDifficulty || inTheDarknessDifficulty.hasDifficultyActionType(inTheDarknessActionType))) {
				result.add(inTheDarknessActionType);
    		}
		}
    	return result;
	}

	public static InTheDarknessActionType getActionTypesForActionNumber(int actionNumber) {
		for (InTheDarknessActionType inTheDarknessActionType : InTheDarknessActionType.values()) {
			if (inTheDarknessActionType.actionNumber == actionNumber) {
				return inTheDarknessActionType;
			}
		}
		return null;
	}
	
}
