/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness.action.widget;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessDifficulty;
import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.action.InTheDarknessActionType;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.ui.actionwidget.Action;
import cz.nic.tablexia.util.ui.actionwidget.Action.ActionListener;
import cz.nic.tablexia.game.games.in_the_darkness.assets.InTheDarknessAssets;
import cz.nic.tablexia.util.ui.actionwidget.ActionsStripWidget;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Widget for showing action icons
 * 
 * @author Matyáš Latner
 *
 */
public class ActionsWidget extends Group implements ActionListener {
	public static final String EVENT_DROP_DOWN = "drop down card";

	public enum ActionLayer {
		BACKGROUND_LAYER			(0),
        ACTIONS_LAYER				(1);

        private Group layerEntity;
        private int layerZIndex;

        ActionLayer(int layerZIndex) {
            this.layerZIndex = layerZIndex;
        }

        private void setLayerEntity(Group layerEntity) {
            this.layerEntity = layerEntity;
        }

        public Group getLayerEntity() {
            return layerEntity;
        }

        public int getLayerZIndex() {
            return layerZIndex;
        }

        public static void attachLayersToEntity(Group containerGroup) {
            for (ActionLayer actionLayer : ActionLayer.values()) {
                Group group = new Group();
                actionLayer.setLayerEntity(group);
                containerGroup.addActor(group);
                group.setZIndex(actionLayer.getLayerZIndex());
            }
        }
    }
	
	private static final float 	TUTORIAL_INFO_ARROW_BLINK_DURATION 	= 0.3f;
	private static final float 	START_ANIMATION_DELAY 				= 0.15f;
	private static final int 	NUMBER_OF_COLUMNS 					= 2;
	public  static final int 	ACTION_OFFSET 						= InTheDarknessGame.ACTION_SIZE_SMALLER / 10;
	private static final float 	START_ARROW_X_OFFSET 				= InTheDarknessGame.ACTION_SIZE_SMALLER / 15;
	private static final float 	START_ARROW_Y_OFFSET 				= -InTheDarknessGame.ACTION_SIZE_SMALLER / 20;
	private static final float 	START_ARROW_WIDTH 					= InTheDarknessGame.TILE_SIZE * 1.5f;
	private static final float 	START_ARROW_HEIGHT 					= InTheDarknessGame.TILE_SIZE;

	private ActionsStripWidget 			actionsStripWidget;
	private List<Action>				actions;
	private InTheDarknessDifficulty 	inTheDarknessDifficulty;
	private int 						currentTutorialStepNumber;
	private InTheDarknessGame 			inTheDarknessGame;
	private Image 						tutorialArrowImage;
	private RepeatAction 				blinkAction;


	public ActionsWidget(InTheDarknessDifficulty inTheDarknessDifficulty,
						 ActionsStripWidget actionsStripWidget,
						 InTheDarknessGame inTheDarknessGame) {

		this.inTheDarknessGame 			= inTheDarknessGame;
		this.inTheDarknessDifficulty 	= inTheDarknessDifficulty;
		this.actionsStripWidget 		= actionsStripWidget;
		this.actions 					= new ArrayList<Action>();
		this.currentTutorialStepNumber 	= 0;

		// SET ACTOR SIZE
		List<InTheDarknessActionType> inTheDarknessActionTypes = InTheDarknessActionType.getActionTypesForGameDifficulty(inTheDarknessDifficulty);
		setSize((float) (NUMBER_OF_COLUMNS * (InTheDarknessGame.ACTION_SIZE_SMALLER + ACTION_OFFSET) - ACTION_OFFSET),
				(float) Math.ceil(Double.valueOf(inTheDarknessActionTypes.size()) / NUMBER_OF_COLUMNS) * (InTheDarknessGame.ACTION_SIZE_SMALLER + ACTION_OFFSET) - ACTION_OFFSET);

		// PREPARE LAYERS
		ActionLayer.attachLayersToEntity(this);

		prepareBackground();
		prepareActions(inTheDarknessActionTypes);
		prepareTutorialArrowImage();
	}

	private Action createAction(String texturePath, int orderNumber, boolean visible, InTheDarknessGame inTheDarknessGame, int actionNumber) {
		int actionSize = InTheDarknessGame.ACTION_SIZE_SMALLER;
		Action action = new Action(texturePath,
				   				   orderNumber,
								   actionSize,
				   				   (orderNumber % NUMBER_OF_COLUMNS) * (actionSize + ACTION_OFFSET),
								   getHeight() - actionSize - ((orderNumber / NUMBER_OF_COLUMNS) * (actionSize + ACTION_OFFSET)),
				   				   visible,
								   inTheDarknessGame,
				                   actionNumber) {

			@Override
			protected void onDoubleTap(){
				if (!TablexiaSettings.getInstance().isRunningOnMobileDevice()) {
					actionsStripWidget.addSelectedAction(getTexturePath(), actionsStripWidget.getSelectedActions().size(), inTheDarknessGame, getActionNumber());
					currentTutorialStepNumber++;
					actions.remove(this.getActionNumber());
				}
			}
		};
		
		action.addActionListener(this);
		action.setClickable();
		ActionLayer.ACTIONS_LAYER.getLayerEntity().addActor(action);
		actions.add(action.getOrderNumber(), action);
		return action;
	}


//////////////////////////////////////////// SCREEN COMPONENTS

	private void prepareBackground() {
		TextureRegion backgroundTexture = inTheDarknessGame.getScreenTextureRegion(InTheDarknessAssets.BACKGROUND_ACTIONS);
		float backgroundHeight = getHeight();
		float backgroundWidth = backgroundTexture.getRegionWidth() * (backgroundHeight / backgroundTexture.getRegionHeight());
		Image actionsBackground = new Image(backgroundTexture);
		actionsBackground.setSize(backgroundWidth, backgroundHeight);
		actionsBackground.setPosition((getWidth() / 2) - (backgroundWidth / 2), 0);
		ActionLayer.BACKGROUND_LAYER.getLayerEntity().addActor(actionsBackground);
	}

	private void prepareActions(List<InTheDarknessActionType> inTheDarknessActionTypes) {
		for (InTheDarknessActionType inTheDarknessActionType : inTheDarknessActionTypes) {
			createAction(inTheDarknessActionType.getTexture(), inTheDarknessActionType.ordinal(), false, inTheDarknessGame, inTheDarknessActionType.getActionNumber());
		}
	}

	private void prepareTutorialArrowImage() {
		if (inTheDarknessDifficulty == InTheDarknessDifficulty.TUTORIAL) {
			tutorialArrowImage = new Image(inTheDarknessGame.getScreenTextureRegion(InTheDarknessAssets.INFO_START_ARROW));
			tutorialArrowImage.setSize(START_ARROW_WIDTH, START_ARROW_HEIGHT);
			tutorialArrowImage.setVisible(false);
			blinkAction = forever(sequence(fadeIn(TUTORIAL_INFO_ARROW_BLINK_DURATION), fadeOut(TUTORIAL_INFO_ARROW_BLINK_DURATION)));
			tutorialArrowImage.addAction(blinkAction);
		}
	}

	
//////////////////////////////////////////// COLLISIONS
	
	private void prepareActionsReactionAreas(Action action) {
		action.setActionsStripWidget(actionsStripWidget);
	}
	
	
//////////////////////////////////////////// ANIMATIONS
	
	public void showActions() {
		tryDimAllActions();
		float actualDelay = 0;
		for (int i = 0; i < actions.size(); i++) {
			final Action action = actions.get(i);

			addAction(sequence(delay(actualDelay), run(new Runnable() {
				@Override
				public void run() {
					action.showWithAnimation();
					prepareActionsReactionAreas(action);
				}
			})));
			actualDelay = actualDelay + START_ANIMATION_DELAY;
		}

		addAction(sequence(delay(actualDelay + START_ANIMATION_DELAY), run(new Runnable() {
			@Override
			public void run() {
				tryToPerformNextTutorialStep();
                inTheDarknessGame.setEventShowAnimationFinished();
			}
		})));
	}
	
	
//////////////////////////////////////////// ACTION STATE
	
	public void enableActions() {
		if (inTheDarknessDifficulty != InTheDarknessDifficulty.TUTORIAL) {
			for (int i = 0; i < actions.size(); i++) {
				Action action = actions.get(i);
				action.enable();
			}
		}
	}
	
	public void disableActions() {
		for (int i = 0; i < actions.size(); i++) {
			Action action = actions.get(i);
			action.disable();
		}
	}
	
	
//////////////////////////////////////////// ACTION LISTENER

	@Override
	public void onActionDrag(Action lastAction) {
		lastAction.setVisible(false);
		Vector2 stageCoordinates = lastAction.localToStageCoordinates(new Vector2());
		InTheDarknessGame.GameLayer.OVERLAY_LAYER.getLayerEntity().stageToLocalCoordinates(stageCoordinates);
		lastAction.setPosition(stageCoordinates.x, stageCoordinates.y);
		InTheDarknessGame.GameLayer.OVERLAY_LAYER.getLayerEntity().addActor(lastAction);
		lastAction.setVisible(true);

		actions.remove(lastAction);
		Action action = createAction(lastAction.getTexturePath(), lastAction.getOrderNumber(), false, inTheDarknessGame, lastAction.getActionNumber());
		prepareActionsReactionAreas(action);
		action.showWithAnimation();
		disableActions();
		tryHideTutorialArrowSprite();
	}
	
	@Override
	public void onActionDrop(Action action, int collidesWithNumber) {
		AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_DROP_DOWN);
		if (collidesWithNumber != Action.NO_COLLISION_NUMBER) {
			actionsStripWidget.addSelectedAction(action.getTexturePath(), collidesWithNumber, inTheDarknessGame, action.getActionNumber());
			currentTutorialStepNumber++;
		}
		tryToPerformNextTutorialStep();
	}

	@Override
	public void onActionDoubleTap(Action action) {

	}


//////////////////////////////////////////// TUTORIAL STEPS
	
	private void tryToPerformNextTutorialStep() {
		if (inTheDarknessDifficulty == InTheDarknessDifficulty.TUTORIAL) {
			if (currentTutorialStepNumber < InTheDarknessGame.TUTORIAL_STEPS.size()) {
        		highliteAction(InTheDarknessGame.TUTORIAL_STEPS.get(currentTutorialStepNumber));
        	} else {
        		InTheDarknessGame.GameLayer.BUTTON_LAYER.sendToFront();
				inTheDarknessGame.disableStartButton(false);
				inTheDarknessGame.highliteStartButton(true);
        	}
        } else {
        	enableActions();
        }
	}
	
	private void tryDimAllActions() {
		if (inTheDarknessDifficulty == InTheDarknessDifficulty.TUTORIAL) {
			inTheDarknessGame.showDimmer();
		}
	}
	
	private void highliteAction(InTheDarknessActionType inTheDarknessActionType) {
		if (inTheDarknessActionType.ordinal() < actions.size()) {
			Action action = actions.get(inTheDarknessActionType.ordinal());
			action.enable();
			showArrowSprite(action.getX() + action.getWidth(), action.getY() - action.getHeight() / 2);
			action.setVisible(false);
			Vector2 stageCoordinates = action.localToStageCoordinates(new Vector2());
			InTheDarknessGame.GameLayer.OVERLAY_LAYER.getLayerEntity().stageToLocalCoordinates(stageCoordinates);
			action.setPosition(stageCoordinates.x, stageCoordinates.y);
			InTheDarknessGame.GameLayer.OVERLAY_LAYER.getLayerEntity().addActor(action);
			action.setVisible(true);
		}
	}
	
	private void showArrowSprite(float positionX, float positionY) {
		if (tutorialArrowImage != null) {
			addActor(tutorialArrowImage);
			tutorialArrowImage.setPosition(positionX + START_ARROW_X_OFFSET, positionY + START_ARROW_Y_OFFSET);
			Vector2 stageCoordinates = tutorialArrowImage.localToStageCoordinates(new Vector2());
			InTheDarknessGame.GameLayer.OVERLAY_LAYER.getLayerEntity().stageToLocalCoordinates(stageCoordinates);
			tutorialArrowImage.setPosition(stageCoordinates.x, stageCoordinates.y);
			InTheDarknessGame.GameLayer.OVERLAY_LAYER.getLayerEntity().addActor(tutorialArrowImage);
			tutorialArrowImage.setVisible(true);
		}
	}
	
	private void tryHideTutorialArrowSprite() {
		if (tutorialArrowImage != null) {
			tutorialArrowImage.remove();
		}
	}

	//Gets for testing
	public List<Action> getAction(){
		return actions;
	}
}
