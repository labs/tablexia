/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.games.in_the_darkness.action.InTheDarknessActionType;
import cz.nic.tablexia.shared.model.resolvers.InTheDarknessScoreResolver;
import cz.nic.tablexia.util.ui.actionwidget.Action;
import cz.nic.tablexia.util.ui.actionwidget.ActionContainer;
import cz.nic.tablexia.util.ui.actionwidget.ActionsStripWidget;
import cz.nic.tablexia.game.games.in_the_darkness.action.widget.ActionsWidget;
import cz.nic.tablexia.game.games.in_the_darkness.assets.InTheDarknessAssets;
import cz.nic.tablexia.game.games.in_the_darkness.creature.Player;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobject.MapObject;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobject.MapObjectType;
import cz.nic.tablexia.game.games.in_the_darkness.map.tile.Tile;
import cz.nic.tablexia.game.games.in_the_darkness.map.widget.MapWidget;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.button.GameImageTablexiaButton;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Matyáš Latner on 7.9.15.
 */
public class InTheDarknessGame extends AbstractTablexiaGame<List<MapWidget>> implements MapWidget.TileMapClickListener {
    public static final String ACTIONS_WIDGET                   = " action widget";
    public static final String ACTIONS_STRIP_MENU               = " action strip menu";
    public static final String START_BUTTON                     = "start button";
    public static final String MAP_WIDGET                       = "map widget";
    public static final String PLAYER                           = "player";

    public static final String EVENT_SHOW_ANIMATION_FINISHED    = "show animation finished";
    public static final String EVENT_SHOW_OTHER_FLOOR           = "show other floor";

    public enum GameLayer {

        BACKGROUND_TEXTURE_LAYER	(0, Touchable.disabled),
        BACKGROUND_DRAWING_LAYER	(1, Touchable.disabled),
        BONUS_LAYER					(2, Touchable.disabled),
        MAP_LAYER					(3, Touchable.enabled),
        BUTTON_LAYER				(4, Touchable.enabled),
        PLAYER_LAYER				(5, Touchable.disabled),
        INFO_LAYER					(6, Touchable.enabled),
        ACTIONS_LAYER				(7, Touchable.enabled),
        FADE_LAYER					(8, Touchable.enabled),
        KEY_LAYER			        (9, Touchable.enabled),
        ACTION_STRIP_LAYER			(10, Touchable.enabled),
        OVERLAY_LAYER			    (11, Touchable.enabled);

        private static final int TOP_Z_INDEX = GameLayer.values().length + 1;

        private Group       layerEntity;
        private int         layerZIndex;
        private Touchable   touchable;

        GameLayer(int layerZIndex, Touchable touchable) {
            this.layerZIndex = layerZIndex;
            this.touchable = touchable;
        }

        private void setLayerEntity(Group layerEntity) {
            this.layerEntity = layerEntity;
            layerEntity.setTouchable(touchable);
        }

        public Group getLayerEntity() {
            return layerEntity;
        }

        public int getLayerZIndex() {
            return layerZIndex;
        }

        public static void attachLayersToScene(Group group) {
            for (GameLayer gameLayer : GameLayer.values()) {
                Group entity = new Group();
                gameLayer.setLayerEntity(entity);
                group.addActor(entity);
                entity.setZIndex(gameLayer.getLayerZIndex());
            }
        }

        public static void resetLayersZIndexes() {
            for (GameLayer gameLayer : GameLayer.values()) {
                gameLayer.getLayerEntity().setZIndex(gameLayer.getLayerZIndex());
            }
        }

        public void sendToFront() {
            getLayerEntity().setZIndex(TOP_Z_INDEX);
        }
    }

    private static final String 		            SCORE_KEY_PLANNING_DURATION		= InTheDarknessScoreResolver.SCORE_KEY_PLANNING_DURATION;
    public  static final String                     SCORE_KEY_ERRORS_COUNT          = InTheDarknessScoreResolver.SCORE_KEY_ERRORS_COUNT;

    private static final Interpolation.Pow          FADE_IN_INTERPOLATION           = Interpolation.pow2;
    private static final float 	                    FADE_DURATION 					= 0.4f;
    private static final float 	                    FADE_ALPHA 					   	= 0.5f;
    private static final Color 	                    FADE_COLOR 					   	= Color.BLACK;

    private static final int 				        GAME_INFO_TILE_SIZE_MULTIPLIER 	= 5;
    private static final float 				        GAME_INFO_APPEAR_DURATION 		= 0.3f;

    private static final float 	                    KEYICON_MOVE_DURATION 			= 0.5f;
    private static final double                     KEY_ICON_TILE_SIZE_RATION_X 	= 0.7;
    private static final double                     KEY_ICON_TILE_SIZE_RATION_Y 	= 0.4;

    private static final int                        START_BUTTON_WIDTH              = 150;
    private static final int                        START_BUTTON_HEIGHT             = 72;
    private static final float                      START_BUTTON_X_OFFSET_RATIO     = 1f / 4;
    private static final float                      START_BUTTON_Y_OFFSET_RATIO     = 1f / 4;
    private static final String                     START_BUTTON_TEXT_KEY           = "complete";
    private static final String                     START_BUTTON_ICON_KEY           = ApplicationInternalTextureManager.BUTTON_YES_ICON;

    public static final float  					    ANIMATION_DURATION      		= 0.25f;
    public static final int 	                    ERROR_ACTION_DELAY 				= 1;
    public static final int 	                    FINISH_ACTION_DELAY 			= 1;

    private static final int 	                    INITIAL_MAP_WIDGET 			   	= 0;

    public static final int                         ACTION_SIZE_BIGGER              = 100;
    public static final int                         ACTION_SIZE_SMALLER             = 70;
    private static final int                        ACTIONS_WIDGET_POSITION_X       = 650;

    private static final int                        ACTION_STRIP_WIDGET_WIDTH       = 155;

    public static final  int                        TILE_SIZE                      = 78;
    public static final  int                        DEFAULT_MAP_START_POSITION_X   = 62;
    public static final  int                        MAP_START_POSITION_Y           = 14;

    private	static final int 						MAXIMUM_ACTIONS_COUNT		    = 200;
    private final int                               MAX_MAP_X_SIZE                  = 7;
    private final int                               START_TILE_OFFSET_X             = 1;
    private final int                               START_TILE_OFFSET_Y             = 2;

    public static final String                      BOOKMARK_FLOOR_TEXT_KEY        = "floor";
    private static final String                     SCORE0_TEXT_KEY                = "score_0";
    private static final String                     SCORE1_TEXT_KEY                = "score_1";
    private static final String                     SCORE2_TEXT_KEY                = "score_2";
    private static final String                     SCORE3_TEXT_KEY                = "score_3";

    private static final String                     GAME_STATE_KEY_SELECTED_ACTIONS     = "selected_actions";
    private static final String                     GAME_STATE_KEY_SCROLL_PANE_POSITION = "scroll_pane_position";

    private static final int                        BACKGROUND_MAP_CORRECTION_OFFSET = -3;

    public static final List<InTheDarknessActionType> TUTORIAL_STEPS = Arrays.asList(new InTheDarknessActionType[]{InTheDarknessActionType.GO,
                                                                                         InTheDarknessActionType.RIGHT,
                                                                                         InTheDarknessActionType.DOOR,
                                                                                         InTheDarknessActionType.GO});


    private Group                   contentGroup;

    private GameImageTablexiaButton startButton;
    private ActionsWidget           actionsWidget;
    private ActionsStripWidget      actionsStripWidget;

    private float                   keyIconPositionX;
    private float                   keyIconPositionY;
    private Image                   keyIcon;

    private Player                  player;
    private int 			        actualMapWidgetNumber;

    private Group                   dimmer;
    private Image                   bottomDimmer;
    private Image                   topDimmer;
    private Image                   leftDimmer;
    private Image                   rightDimmer;
    private Map<String, Image>      gameInfos;
    private InTheDarknessDifficulty inTheDarknessDifficulty;

    private long                    planningStartTime;

    public static int                     mapSizeX;
    public static int                     mapSizeY;
    public static TileMap.TileMapPosition defaultMapStartPosition;

//////////////////////////////////////////// ABSTRACT TABLEXIA GAME

    @Override
    protected void prepareGameSoundAssetNames(List<String> soundsFileNames) {
        soundsFileNames.add(InTheDarknessAssets.SOUND_ERROR);
        soundsFileNames.add(InTheDarknessAssets.SOUND_DOOR);
        soundsFileNames.add(InTheDarknessAssets.SOUND_KEY);
        soundsFileNames.add(InTheDarknessAssets.SOUND_STEPS1);
        soundsFileNames.add(InTheDarknessAssets.SOUND_STEPS2);
        soundsFileNames.add(InTheDarknessAssets.SOUND_STEPS3);
        soundsFileNames.add(InTheDarknessAssets.SOUND_BUMP1);
        soundsFileNames.add(InTheDarknessAssets.SOUND_BUMP2);
        soundsFileNames.add(InTheDarknessAssets.SOUND_BARK1);
        soundsFileNames.add(InTheDarknessAssets.SOUND_BARK2);
        soundsFileNames.add(InTheDarknessAssets.SOUND_JUMP1);
        soundsFileNames.add(InTheDarknessAssets.SOUND_JUMP2);
        soundsFileNames.add(InTheDarknessAssets.SOUND_SAVE);
    }

    @Override
    protected List<MapWidget> prepareGameData(Map<String, String> gameState) {
        inTheDarknessDifficulty = null;
        List<MapWidget> mapWidgets = new ArrayList<MapWidget>();
        try {
            Tile lastFinishPosition = null;

            int floorCount = getInTheDarknessDifficulty().getFloorCount();
            boolean hasKey = getInTheDarknessDifficulty().hasDifficultyActionType(InTheDarknessActionType.KEY);

            for (int i = 0; i < floorCount; i++) {

                mapSizeX = getInTheDarknessDifficulty().getMap().getSizeX();
                mapSizeY = getInTheDarknessDifficulty().getMap().getSizeY();
                defaultMapStartPosition = new TileMap.TileMapPosition(mapSizeX - START_TILE_OFFSET_X, mapSizeY - START_TILE_OFFSET_Y);

                MapObjectType finishMapObjectMapObjectType;
                if (i == (floorCount - 1)) {
                    finishMapObjectMapObjectType = MapObjectType.SAFE;
                } else {
                    finishMapObjectMapObjectType = MapObjectType.STAIRS;
                }
                boolean floorHasKey = hasKey && ((floorCount - 1) / 2) == i;
                Log.info(getClass(), "Preparing map for floor number: " + (i + 1));
                MapWidget mapWidget = new MapWidget(getInTheDarknessDifficulty().getMapProviderNewInstance(getRandom()), lastFinishPosition, finishMapObjectMapObjectType, i, floorCount > 1, getMapStartPositionX(), MAP_START_POSITION_Y, mapSizeX, mapSizeY, floorHasKey, getText(BOOKMARK_FLOOR_TEXT_KEY));
                mapWidgets.add(mapWidget);

                if (i == 0) {
                    mapWidget.setInFront();
                } else {
                    mapWidget.setInBottom();
                }

                mapWidget.setClickable(this);
                mapWidget.setName(MAP_WIDGET + i);
                lastFinishPosition = mapWidget.getTileMap().getTileAtPosition(mapWidget.getTileMap().getMapFinishPosition());

            }
        } catch (Throwable t) {
            throw new RuntimeException("Error during generating map with random seed: " + getRandom(), t);
        }
        return mapWidgets;
    }

    @Override
    protected void gameLoaded(Map<String, String> gameState) {

        // main content group
        contentGroup = new Group();
        getStage().addActor(contentGroup);
        GameLayer.attachLayersToScene(contentGroup);

        actualMapWidgetNumber = 0;
        planningStartTime = 0;

        // screen components
        prepareBackground();
        prepareMapWidgets();

        startButton = showStartButton();
        startButton.setName(START_BUTTON);

        actionsStripWidget = showActionsStripWidget(gameState);
        actionsStripWidget.setStartButton(startButton);
        actionsStripWidget.setName(ACTIONS_STRIP_MENU);
        actionsWidget = showActionsWidget(actionsStripWidget, getInTheDarknessDifficulty());
        actionsWidget.setName(ACTIONS_WIDGET);
        showPlayer();

        prepareGameInfos();
        prepareDimmer();
        prepareKeyIcon();

        mapWidgetToFront(INITIAL_MAP_WIDGET);

        setGameScore(SCORE_KEY_ERRORS_COUNT, 0);

        alignScreenContent();
        resetGameState();
        goToPlanningMode(null);
    }

    @Override
    protected void gameVisible() {
        triggerScenarioStepEvent(EVENT_GAME_READY);
        actionsWidget.showActions();
    }

    @Override
    protected void gamePaused(Map<String, String> gameState) {
        gameState.put(GAME_STATE_KEY_SCROLL_PANE_POSITION, String.valueOf(actionsStripWidget.getScrollPaneActualPosition()));
        gameState.put(GAME_STATE_KEY_SELECTED_ACTIONS, actionsStripWidget.getSelectedActionsString());
    }

    @Override
    protected void gameEnd() {
        for (MapWidget mapWidget: getData()) {
            mapWidget.remove();
        }
    }

    @Override
    protected void screenResized(int width, int height) {
        alignScreenContent();
    }

    @Override
    public void setScrollableLayoutFocus() {
        if(actionsStripWidget != null)
            actionsStripWidget.setScrollFocus();
    }

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(new SummaryMessage(SummaryImage.STATS, getFormattedText("result_errors_count", game.getGameScore(SCORE_KEY_ERRORS_COUNT, "0"))));
    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        switch (gameResult) {
            case THREE_STAR:
                return SCORE3_TEXT_KEY;
            case TWO_STAR:
                return SCORE2_TEXT_KEY;
            case ONE_STAR:
                return SCORE1_TEXT_KEY;
            case NO_STAR:
                return SCORE0_TEXT_KEY;
            default:
                return SCORE0_TEXT_KEY;
        }
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        if(!getGameDifficulty().hasScore()) return InTheDarknessAssets.SOUND_SCORE_TUTORIAL;

        switch (gameResult) {
            case THREE_STAR:
                return InTheDarknessAssets.SOUND_SCORE_3;
            case TWO_STAR:
                return InTheDarknessAssets.SOUND_SCORE_2;
            case ONE_STAR:
                return InTheDarknessAssets.SOUND_SCORE_1;
            case NO_STAR:
                return InTheDarknessAssets.SOUND_SCORE_0;
            default:
                return InTheDarknessAssets.SOUND_SCORE_0;
        }
    }


//////////////////////////////////////////// IN THE DARKNESS DIFFICULTY

    public InTheDarknessDifficulty getInTheDarknessDifficulty() {
        if (inTheDarknessDifficulty == null) {
            inTheDarknessDifficulty = InTheDarknessDifficulty.getInTheDarknessDifficultyForGameDifficulty(getGameDifficulty());
        }
        return inTheDarknessDifficulty;
    }


//////////////////////////////////////////// GAME CONTROL

    private List<ActionContainer>   selectedActions;
    private int 	                selectedActionPosition;
    private float	                secoundElapsed;
    private boolean                 actionFinished;
    private boolean                 sequenceStarted = false;

    private void performNextAction(TileMap tileMap) {
        if (player.hasKey()) {
            showKeyIcon();
        }
        MapObject mapObject = tileMap.getTileAtPosition(player.getActualTileMapPosition()).getMapObject();
        if ((mapObject != null) &&
                (!getInTheDarknessDifficulty().hasDifficultyActionType(InTheDarknessActionType.KEY) || player.hasKey()) &&
                (mapObject.getMapObjectType() == MapObjectType.SAFE) &&
                (selectedActionPosition == selectedActions.size() - 1)) {
                    getSound(InTheDarknessAssets.SOUND_SAVE).play();
                    stopGameSequence();
                    showSafeInfo();

            addAction(delay(FINISH_ACTION_DELAY, run(new Runnable() {
                @Override
                public void run() {
                    gameComplete();
                }
            })));
        }
        selectedActionPosition++;
    }

    private void performErrorAction() {
        setGameScore(SCORE_KEY_ERRORS_COUNT, getGameScoreInteger(SCORE_KEY_ERRORS_COUNT, 0) + 1);
        resetGameState();
        goToPlanningMode(Integer.valueOf(selectedActionPosition));
    }

    @Override
    protected void gameAct(float delta) {
        if (sequenceStarted) {
            if (actionFinished && (secoundElapsed > ANIMATION_DURATION)) {
                secoundElapsed = 0;
                if (selectedActionPosition < selectedActions.size()) {
                    final TileMap tileMap = getData().get(actualMapWidgetNumber).getTileMap();

                    actionFinished = false;
                    actionsStripWidget.displayProcessedActions(selectedActionPosition,
                                                               selectedActionPosition != 0,
                                                               new Runnable() {
                                                                   @Override
                                                                   public void run() {

                                                                       InTheDarknessActionType.values()[selectedActions.get(selectedActionPosition).getAction().getActionNumber()].performActionTypeRule(InTheDarknessGame.this, tileMap, player, getMapStartPositionX(), MAP_START_POSITION_Y, new InTheDarknessActionType.IActionFinishedListener() {

                                                                           @Override
                                                                           public void onActionFinished(boolean result) {
                                                                               try {
                                                                                   if (result) {
                                                                                       performNextAction(tileMap);
                                                                                   } else {
                                                                                       stopGameSequence();
                                                                                       performErrorAction();
                                                                                   }
                                                                                   actionFinished = true;
                                                                               } catch (Throwable t) {
                                                                                   throw new RuntimeException("Error during game cycle with random seed: " + getRandom(), t);
                                                                               }
                                                                           }
                                                                       });
                                                                   }
                                                               });
                } else {
                    stopGameSequence();
                    getSound(InTheDarknessAssets.SOUND_ERROR).play();
                    addAction(delay(FINISH_ACTION_DELAY, run(new Runnable() {
                        @Override
                        public void run() {
                            performErrorAction();
                        }
                    })));
                }
            } else {
                secoundElapsed = secoundElapsed + delta;
            }
        }
    }

    private void startGameSequence() {
        selectedActions = actionsStripWidget.getSelectedActions();
        mapWidgetToFront(player.getActualFloor());
        if (selectedActions.size() > 0) {
            goToPlayMode();
            selectedActionPosition 	= 0;
            secoundElapsed 			= 0;
            actionFinished 			= true;
            sequenceStarted         = true;
        }
    }

    private void stopGameSequence() {
        sequenceStarted = false;
    }

    private void resetGameState() {
        goToFloorNumber(0);
        resetKey();
        resetPlayer();
        hideGameInfos();
        for (MapWidget mapWidget : getData()) {
            mapWidget.getTileMap().reset();
        }

        ApplicationBus.getInstance().publishAsync(new AbstractTablexiaGame.GameFinishedEvent(EVENT_FINISH_GAME));
    }

    private void goToPlanningMode(Integer actionNumber) {
        hideDimmer();
        actionsStripWidget.setStartButtonState();
        actionsWidget.enableActions();
        actionsStripWidget.enableControl();
        actionsStripWidget.displaySelectedActions(actionNumber);
        planningStartTime = System.currentTimeMillis();
    }

    private void goToPlayMode() {
        setGameScore(SCORE_KEY_PLANNING_DURATION, getGameScoreFloat(SCORE_KEY_PLANNING_DURATION, 0f) + (System.currentTimeMillis() - planningStartTime - GameDAO.getAllPausesDuration(getGame())));
        disableStartButton(true);
        actionsWidget.disableActions();
        actionsStripWidget.disableControl();
        showDimmer();
    }


//////////////////////////////////////////// SCREEN CONTENT

    private void alignScreenContent() {
        actionsStripWidget.setSize(ACTION_STRIP_WIDGET_WIDTH, getStage().getHeight());
        actionsStripWidget.setPosition(getStage().getWidth() - actionsStripWidget.getWidth(), getViewportBottomY());
        resizeDimmer();

        if(sequenceStarted){
            stopGameSequence();
            resetGameState();
            goToPlanningMode(Integer.valueOf(selectedActionPosition));
            disableStartButton(false);
        }
    }

    private void prepareBackground() {
        GameLayer.BACKGROUND_TEXTURE_LAYER.getLayerEntity().addActor(ScaleUtil.createNoBlendingImageToWidthWithPosition(getScreenTextureRegion(InTheDarknessAssets.BACKGROUND), getStage().getWidth(), 0, getSceneOuterBottomY()));
        GameLayer.BACKGROUND_DRAWING_LAYER.getLayerEntity().addActor(ScaleUtil.createImageToWidthWithPosition(getScreenTextureRegion(getInTheDarknessDifficulty().getMap().getMapTexture()), TablexiaSettings.getWorldSize() + BACKGROUND_MAP_CORRECTION_OFFSET, 0, getSceneOuterBottomY()));
    }


//////////////////////////////////////////// WIDGETS

    private GameImageTablexiaButton showStartButton() {
        GameImageTablexiaButton startButton = new GameImageTablexiaButton(
                getText(START_BUTTON_TEXT_KEY),
                new Image(ApplicationInternalTextureManager.getInstance().getTexture(START_BUTTON_ICON_KEY))
        );

        startButton.setDisabled();
        startButton.adaptiveSizePositionFix(false);
        startButton.setButtonSize(START_BUTTON_WIDTH, START_BUTTON_HEIGHT);
        startButton.setButtonPosition(
                getMapStartPositionX() + (mapSizeX * TILE_SIZE) + (START_BUTTON_WIDTH * START_BUTTON_X_OFFSET_RATIO),
                MAP_START_POSITION_Y - (START_BUTTON_HEIGHT * START_BUTTON_Y_OFFSET_RATIO));
        startButton.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                startGameSequence();
            }
        });

        GameLayer.BUTTON_LAYER.getLayerEntity().addActor(startButton);
        return startButton;
    }

    private ActionsStripWidget showActionsStripWidget(Map<String, String> gameState) {
        Float scrollPanePosition = gameState.containsKey(GAME_STATE_KEY_SCROLL_PANE_POSITION) ? Float.valueOf(gameState.get(GAME_STATE_KEY_SCROLL_PANE_POSITION)) : null;
        String selectedActionsString = gameState.containsKey(GAME_STATE_KEY_SELECTED_ACTIONS) ? gameState.get(GAME_STATE_KEY_SELECTED_ACTIONS) : null;

        ActionsStripWidget actionStripWidget = new ActionsStripWidget(scrollPanePosition, selectedActionsString, this, ACTION_SIZE_BIGGER, ACTION_SIZE_SMALLER, ANIMATION_DURATION, InTheDarknessAssets.CONTROL_ACTUAL, InTheDarknessAssets.CONTROL_NEXT, MAXIMUM_ACTIONS_COUNT, ActionsStripWidget.StartButtonControl.IS_NOT_EMPTY){

            @Override
            protected Action createAction(String texturePath, final int position, AbstractTablexiaGame tablexiaGame, int actionNumber) {
                Action newAction = new StripWidgetCustomAction(texturePath, position, ACTION_SIZE_BIGGER, 0, 0, true, tablexiaGame, actionNumber) {
                    @Override
                    protected boolean downAction(float positionX, float positionY, boolean center) {

                        if(super.downAction(positionX,positionY,center)) GameLayer.ACTION_STRIP_LAYER.sendToFront();
                        return super.downAction(positionX,positionY,center);
                    }

                    @Override
                    protected void upAction(boolean hideWithAnimation) {
                        InTheDarknessGame.GameLayer.resetLayersZIndexes();
                        super.upAction(hideWithAnimation);
                    }
                };
                return newAction;
            }

            @Override
            public void onActionDoubleTap(Action action) {
                if (!TablexiaSettings.getInstance().isRunningOnMobileDevice() && containsAction(action)) {
                    removeActionAndUpdateView(action.getOrderNumber());
                }
            }

            @Override
            protected void prepareSelectedActions(String selectedActionsString, AbstractTablexiaGame tablexiaGame) {
                if (selectedActionsString != null) {
                    String[] actionStrings = selectedActionsString.split(ActionsStripWidget.SELECTED_ACTIONS_STRING_SEPARATOR);
                    for (int i = 0; i < actionStrings.length; i++) {
                        int actionNumber = Integer.valueOf(actionStrings[i]);
                        addSelectedAction(InTheDarknessActionType.getActionTypesForActionNumber(actionNumber).getTexture(), i, tablexiaGame, actionNumber);
                    }
                }
            }
        };

        actionStripWidget.enableActionSorting(getInTheDarknessDifficulty() != InTheDarknessDifficulty.TUTORIAL);
        actionStripWidget.enableStartButtonControl(getInTheDarknessDifficulty() != InTheDarknessDifficulty.TUTORIAL);
        actionStripWidget.setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
        GameLayer.ACTION_STRIP_LAYER.getLayerEntity().addActor(actionStripWidget);
        return actionStripWidget;
    }

    private ActionsWidget showActionsWidget(ActionsStripWidget actionsStripWidget, InTheDarknessDifficulty inTheDarknessDifficulty) {
        ActionsWidget widget = new ActionsWidget(inTheDarknessDifficulty, actionsStripWidget, this);
        widget.setPosition(ACTIONS_WIDGET_POSITION_X, MAP_START_POSITION_Y + mapSizeY * TILE_SIZE - widget.getHeight());
        GameLayer.ACTIONS_LAYER.getLayerEntity().addActor(widget);

        return widget;
    }

    public void disableStartButton(boolean disabled) {
        startButton.setEnabled(!disabled);
        highliteStartButton(false);
    }

    public void highliteStartButton(boolean enableBlinking) {
        startButton.highliteButton(enableBlinking);
    }


//////////////////////////////////////////// MAP WIDGET

    private void prepareMapWidgets() {
        for (MapWidget mapWidget: getData()) {
            GameLayer.MAP_LAYER.getLayerEntity().addActor(mapWidget.prepareInOpenGLContext(this));
        }
    }

    public void mapWidgetToFront(int mapWidgetNumber) {
        List<MapWidget> mapWidgets = getData();
        for (int i = 0; i < mapWidgets.size(); i++) {
            MapWidget mapWidget = mapWidgets.get(i);

            if (i == mapWidgetNumber) {
                mapWidget.setInFront();
            } else {
                mapWidget.setInBottom();
            }
        }
        GameLayer.PLAYER_LAYER.getLayerEntity().setVisible(player.getActualFloor() == mapWidgetNumber);
        triggerScenarioStepEvent(EVENT_SHOW_OTHER_FLOOR);
    }

    @Override
    public void onBookmarkClick(int mapWidgetNumber) {
        mapWidgetToFront(mapWidgetNumber);
    }


//////////////////////////////////////////// FLOOR CONTROL

    private boolean tryGoToFloor(int floorNumber, TileMap.TileMapPosition mapPosition) {
        if ((floorNumber >= 0) && (floorNumber < getData().size())) {
            MapObject mapObject = getData().get(floorNumber).getTileMap().getTileAtPosition(mapPosition).getMapObject();
            if ((mapObject != null) && (mapObject.getMapObjectType() == MapObjectType.STAIRS)) {
                goToFloorNumber(floorNumber);
                return true;
            }
        }
        return false;
    }

    public boolean goToNextFloor(TileMap.TileMapPosition mapPosition) {
        if (tryGoToFloor(actualMapWidgetNumber + 1, mapPosition)) {
            return true;
        }
        if (tryGoToFloor(actualMapWidgetNumber - 1, mapPosition)) {
            return true;
        }

        return false;
    }

    private void goToFloorNumber(int floorNumber) {
        actualMapWidgetNumber = floorNumber;
        player.setActualFloor(floorNumber);
        player.setPossibleDirection(getData().get(floorNumber).getTileMap());
        mapWidgetToFront(actualMapWidgetNumber);
    }


//////////////////////////////////////////// PLAYER

    private void showPlayer() {
        if (player != null) {
            player.remove();
        }
        player = new Player(INITIAL_MAP_WIDGET, this);
        player.setName(PLAYER);
        GameLayer.PLAYER_LAYER.getLayerEntity().addActor(player);
    }

    private void resetPlayer() {
        TileMap tileMap = getData().get(actualMapWidgetNumber).getTileMap();
        player.resetPlayerMapPosition(tileMap, getMapStartPositionX(), MAP_START_POSITION_Y);
        player.setPossibleDirection(tileMap);
    }


//////////////////////////////////////////// GAME INFO

    private void prepareGameInfos() {
        gameInfos = new HashMap<String, Image>();

        float gameInfoWidth 	= TILE_SIZE * GAME_INFO_TILE_SIZE_MULTIPLIER;
        float gameInfoHeight 	= TILE_SIZE * GAME_INFO_TILE_SIZE_MULTIPLIER;
        float gameInfoPositionX = getMapStartPositionX() + (getData().get(0).getWidth() / 2) - (gameInfoWidth / 2);
        float gameInfoPositionY = MAP_START_POSITION_Y + (getData().get(0).getHeight() / 2) - (gameInfoHeight / 2);
        createInfoSprite(GameLayer.INFO_LAYER, InTheDarknessAssets.INFO_CRASH, gameInfoPositionX, gameInfoPositionY, gameInfoWidth, gameInfoHeight);
        createInfoSprite(GameLayer.INFO_LAYER, InTheDarknessAssets.INFO_SAFE1, gameInfoPositionX, gameInfoPositionY, gameInfoWidth, gameInfoHeight);
        createInfoSprite(GameLayer.INFO_LAYER, InTheDarknessAssets.INFO_SAFE2, gameInfoPositionX, gameInfoPositionY, gameInfoWidth, gameInfoHeight);
    }

    public void showCrashInfo() {
        showGameInfo(InTheDarknessAssets.INFO_CRASH, true, null);
    }

    public void showSafeInfo() {
        showGameInfo(InTheDarknessAssets.INFO_SAFE1, true, new Runnable() {
            @Override
            public void run() {
                showGameInfo(InTheDarknessAssets.INFO_SAFE2, false, null);
            }
        });
    }

    public void hideGameInfo(String gameInfoTextureName, boolean animated, final Runnable finishHandler) {
        final Image gameInfoSprite = gameInfos.get(gameInfoTextureName);
        if (gameInfoSprite.isVisible()) {
            if (animated) {
                gameInfoSprite.addAction(alpha(1));

                AlphaAction fadeOutAction = fadeOut(GAME_INFO_APPEAR_DURATION);
                if (finishHandler != null) {
                    gameInfoSprite.addAction(sequence(fadeOutAction, run(new Runnable() {
                        @Override
                        public void run() {
                            gameInfoSprite.setVisible(false);
                            finishHandler.run();
                        }
                    })));
                } else {
                    gameInfoSprite.addAction(fadeOutAction);
                }
            } else {
                gameInfoSprite.setVisible(false);
            }
        }
    }

    public void showGameInfo(String gameInfoTextureName, boolean animated, Runnable finishHandler) {
        hideGameInfos();

        Image gameInfoImage = gameInfos.get(gameInfoTextureName);
        if (animated) {
            gameInfoImage.addAction(alpha(0));
            gameInfoImage.addAction(scaleTo(0, 0));
            gameInfoImage.setVisible(true);

            ParallelAction actions = parallel(fadeIn(GAME_INFO_APPEAR_DURATION), scaleTo(1, 1, GAME_INFO_APPEAR_DURATION, Interpolation.swingOut));
            if (finishHandler != null) {
                gameInfoImage.addAction(sequence(actions, run(finishHandler)));
            } else {
                gameInfoImage.addAction(actions);
            }
        } else {
            gameInfoImage.setVisible(true);
        }
    }

    private void hideGameInfos() {
        for (Image gameInfoSprite : gameInfos.values()) {
            gameInfoSprite.setVisible(false);
        }
    }

    private void createInfoSprite(GameLayer gameLayer, String textureName, float positionX, float positionY, float width, float height) {
        Image gameInfoSprite = new Image(getScreenTextureRegion(textureName));
        gameInfoSprite.setBounds(positionX, positionY, width, height);
        gameInfoSprite.setVisible(false);
        gameInfoSprite.setOrigin(Align.center);
        gameLayer.getLayerEntity().addActor(gameInfoSprite);
        gameInfos.put(textureName, gameInfoSprite);
    }


//////////////////////////////////////////// KEY ICON

    private void prepareKeyIcon() {
        int keyIconSizeX = (int)(TILE_SIZE * KEY_ICON_TILE_SIZE_RATION_X);
        int keyIconSizeY = (int)(TILE_SIZE * KEY_ICON_TILE_SIZE_RATION_Y);

        keyIconPositionX = (getMapStartPositionX() + getData().get(actualMapWidgetNumber).getWidth()) - keyIconSizeX;
        keyIconPositionY = (MAP_START_POSITION_Y + getData().get(actualMapWidgetNumber).getHeight()) - keyIconSizeY;

        keyIcon = new Image(getScreenTextureRegion(InTheDarknessAssets.CONTROL_KEY));
        keyIcon.setBounds(keyIconPositionX, keyIconPositionY, keyIconSizeX, keyIconSizeY);
        keyIcon.setVisible(false);

        GameLayer.KEY_LAYER.getLayerEntity().addActor(keyIcon);
    }

    private void showKeyIcon() {
        if (!keyIcon.isVisible()) {
            // set map and player layer over key layer for proper shift animation
            GameLayer.MAP_LAYER.sendToFront();
            GameLayer.PLAYER_LAYER.sendToFront();
            keyIcon.setVisible(true);
            keyIcon.setPosition(keyIconPositionX, keyIconPositionY);
            keyIcon.addAction(sequence(moveTo(keyIconPositionX, keyIconPositionY + keyIcon.getHeight(), KEYICON_MOVE_DURATION, Interpolation.swingOut), run(new Runnable() {
                @Override
                public void run() {
                    GameLayer.resetLayersZIndexes();
                }
            })));
        }
    }

    private void hideKeyIcon() {
        if (keyIcon.isVisible()) {
            keyIcon.setVisible(false);
            keyIcon.setPosition(keyIconPositionX, keyIconPositionY);
        }
    }

    private void resetKey() {
        player.setKey(false);
        hideKeyIcon();
    }


//////////////////////////////////////////// DIMMER

    private void prepareDimmer() {
        dimmer = new Group();
        bottomDimmer = new Image(getColorTextureRegion(FADE_COLOR));
        dimmer.addActor(bottomDimmer);

        topDimmer = new Image(getColorTextureRegion(FADE_COLOR));
        dimmer.addActor(topDimmer);

        leftDimmer = new Image(getColorTextureRegion(FADE_COLOR));
        dimmer.addActor(leftDimmer);

        rightDimmer = new Image(getColorTextureRegion(FADE_COLOR));
        dimmer.addActor(rightDimmer);

        dimmer.addAction(alpha(FADE_ALPHA));
        GameLayer.FADE_LAYER.getLayerEntity().addActor(dimmer);
    }

    private void resizeDimmer() {
        float viewportLeftX             = getViewportLeftX();
        float viewportRightX            = getViewportRightX();
        float viewportBottomY           = getViewportBottomY();
        float viewportTopY              = getViewportTopY();

        float sceneInnerBottomY         = getSceneInnerBottomY();
        float sceneLeftX                = getSceneLeftX();

        float mapLeftPositionX          = sceneLeftX + getMapStartPositionX();
        float mapBottomPositionY        = sceneInnerBottomY + MAP_START_POSITION_Y;
        float mapRightPositionX         = sceneLeftX + getMapStartPositionX() + mapSizeX * TILE_SIZE;
        float mapTopPositionY           = sceneInnerBottomY + MAP_START_POSITION_Y + mapSizeY * TILE_SIZE;
        float actionStripLeftPosition   = sceneLeftX + viewportRightX - actionsStripWidget.getWidth();

        bottomDimmer.setBounds(viewportLeftX, viewportBottomY, actionStripLeftPosition, Math.abs(mapBottomPositionY - viewportBottomY));
        topDimmer.setBounds(viewportLeftX, mapTopPositionY, viewportRightX - actionsStripWidget.getWidth(), viewportTopY);
        leftDimmer.setBounds(viewportLeftX, mapBottomPositionY, mapLeftPositionX, mapTopPositionY - mapBottomPositionY);
        rightDimmer.setBounds(mapRightPositionX, mapBottomPositionY, actionStripLeftPosition - mapRightPositionX, mapTopPositionY - mapBottomPositionY);
    }

    public void showDimmer() {
        if (!dimmer.isVisible()) {
            dimmer.addAction(alpha(0));
            dimmer.setVisible(true);
            dimmer.addAction(alpha(FADE_ALPHA, FADE_DURATION, FADE_IN_INTERPOLATION));
        }
    }

    public void hideDimmer() {
        if (dimmer.isVisible()) {
            dimmer.addAction(sequence(alpha(0, FADE_DURATION), run(new Runnable() {
                @Override
                public void run() {
                    dimmer.setVisible(false);
                }
            })));
        }
    }


//////////////////////////////////////////// UTILITY

    public void playRandomSoundFromGroup(String[] soundGroup) {
        playRandomSoundFromGroup(soundGroup, 1f);
    }

    public void playRandomSoundFromGroup(String[] soundGroup, float pitch) {
        getSound(soundGroup[getRandom().nextInt(soundGroup.length)]).play(1f, pitch, 1f);
    }

//////////////////////////////////////////// GETTERS

    public int getMapStartPositionX() {
        return DEFAULT_MAP_START_POSITION_X + (MAX_MAP_X_SIZE - mapSizeX) * TILE_SIZE;
    }

//////////////////////////////////////////// SET EVENTS FOR TEST

    public void setEventShowAnimationFinished(){
        triggerScenarioStepEvent(EVENT_SHOW_ANIMATION_FINISHED);
    }


//////////////////////////////////////////// PRELOADER

    private static final String                             PRELOADER_INFO_IMAGE            = "preloader_info";
    private static final String                             PRELOADER_ANIM_IMAGE            = "preloader_anim";
    private static final int                                PRELOADER_ANIM_FRAMES           = 15;
    private static final float                              PRELOADER_ANIM_FRAME_DURATION   = 0.5f;
    private static final String                             PRELOADER_TEXT1_KEY             = ApplicationTextManager.ApplicationTextsAssets.GAME_IN_THE_DARKNESS_PRELOADER_TEXT1;
    private static final String                             PRELOADER_TEXT2_KEY             = ApplicationTextManager.ApplicationTextsAssets.GAME_IN_THE_DARKNESS_PRELOADER_TEXT2;
    private static final String                             PRELOADER_TEXT2_KEY_DESKTOP     = ApplicationTextManager.ApplicationTextsAssets.GAME_IN_THE_DARKNESS_PRELOADER_DEKSTOP;

    private static final Scaling                            PRELOADER_IMAGE_SCALING             = Scaling.fit;
    private static final int 			                    PRELOADER_TEXT_ALIGN 				= Align.left;
    private static final float 			                    PRELOADER_TEXT_PADDING 				= 10f;
    private static final float 			                    PRELOADER_LEFT_COLUMN_WIDTH_RATIO 	= 1f / 4;
    private static final float 			                    PRELOADER_RIGHT_COLUMN_RATIO 		= 5f / 8;
    private static final float 			                    PRELOADER_ROW_HEIGHT 				= 1f / 3;

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        Image preloaderImage1 = new Image(preloaderAssetsManager.getTextureRegion(PRELOADER_INFO_IMAGE));
        String preloaderText1 = preloaderAssetsManager.getText(PRELOADER_TEXT1_KEY);
        components.add(new TwoColumnContentDialogComponent(
                new ImageContentDialogComponent(preloaderImage1, PRELOADER_IMAGE_SCALING),
                new TextContentDialogComponent(preloaderText1, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_ROW_HEIGHT));

        components.add(new FixedSpaceContentDialogComponent());

        AnimatedImage preloaderImage2 = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_IMAGE, PRELOADER_ANIM_FRAMES, PRELOADER_ANIM_FRAME_DURATION), false);
        preloaderImage2.startAnimationLoop();
        String preloaderText2 = TablexiaSettings.getInstance().isRunningOnMobileDevice() ? preloaderAssetsManager.getText(PRELOADER_TEXT2_KEY) : preloaderAssetsManager.getText(PRELOADER_TEXT2_KEY_DESKTOP);
        components.add(new TwoColumnContentDialogComponent(
                new TextContentDialogComponent(preloaderText2, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                new AnimatedImageContentDialogComponent(preloaderImage2, PRELOADER_IMAGE_SCALING),
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_ROW_HEIGHT));
    }


}
