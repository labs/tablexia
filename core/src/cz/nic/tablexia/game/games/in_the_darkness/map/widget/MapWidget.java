/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness.map.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.map.IMapProvider;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobject.MapObject;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobject.MapObjectType;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobstacle.MapObstacle;
import cz.nic.tablexia.game.games.in_the_darkness.map.tile.Tile;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

public class MapWidget extends Group {
	
	private static final float 								BOOKMARKBUTTON_TILE_SIZE_RATIO_HEIGHT 	= 0.3f;
	private static final float 								BOOKMARKBUTTON_TILE_SIZE_RATIO_WIDTH 	= 1.5f;
	public  static final Color 								BOOKMARKBUTTON_BACKGROUND_COLOR_CHECKED	= new Color(0.428f, 0.369f, 0.350f, 1f);
	public  static final Color 								BOOKMARKBUTTON_BACKGROUND_COLOR_UP 		= new Color(0.588f, 0.529f, 0.510f, 1f);
	private static final Color 								BOOKMARKBUTTON_TEXT_COLOR 				= Color.BLACK;
	private static final float 								BOOKMARKBUTTON_PADDING_RATIO 			= 1f / 20;
	private static final int 								BOOKMARKBUTTON_PADDING_LEFT 			= 30;
	private static final ApplicationFontManager.FontType    BOOKMARK_TITLE_TEXT_FONT 				= ApplicationFontManager.FontType.REGULAR_16;

	private Group 						mapLayer;
	private Group 						backgroundLayer;
	private TablexiaButton 				bookmarkButton;
	
	private TileMap 					tileMap;

	private boolean 					hasBookmark;
	private String 						buttonText;
	private int 						floorNumber;

	private TileMapClickListener 		tileMapClickListener;

	public interface TileMapClickListener {
		void onBookmarkClick(int mapWidgetNumber);
	}
	
	public MapWidget(IMapProvider mapProvider, Tile lastFinishTile, MapObjectType finishMapObjectType, int floorNumber, boolean hasBookmark, int positionX, int positionY, int mapXSize, int mapYSize, boolean hasKey, String buttonText) {
		this.hasBookmark = hasBookmark;
		this.buttonText  = buttonText;
		this.floorNumber = floorNumber;

		setWidth((mapXSize * InTheDarknessGame.TILE_SIZE));
        setHeight((mapYSize * InTheDarknessGame.TILE_SIZE));
		
		backgroundLayer = new Group();
		addActor(backgroundLayer);
		mapLayer = new Group();
		addActor(mapLayer);

		tileMap = mapProvider.prepareMap(lastFinishTile, mapXSize, mapYSize, finishMapObjectType, hasKey);
		setPosition(positionX, positionY);
	}

	public MapWidget prepareInOpenGLContext(InTheDarknessGame inTheDarknessGame) {
		showTileMap(inTheDarknessGame);
		if (hasBookmark) {
			showBookmarkButton();
		}
		return this;
	}
	
	public TileMap getTileMap() {
		return tileMap;
	}
	
	public void setClickable(TileMapClickListener tileMapClickListener) {
		this.tileMapClickListener = tileMapClickListener;
	}
	
	public void setInFront() {
		if (bookmarkButton != null) {
			bookmarkButton.setChecked(true);
		}
		mapLayer.setVisible(true);
	}
	
	public void setInBottom() {
		if (bookmarkButton != null) {
			bookmarkButton.setChecked(false);
		}
		mapLayer.setVisible(false);
	}
	
	private void showBookmarkButton() {
		float bookmarkWidth = InTheDarknessGame.TILE_SIZE * BOOKMARKBUTTON_TILE_SIZE_RATIO_WIDTH;
		float bookmarkHeight = InTheDarknessGame.TILE_SIZE * BOOKMARKBUTTON_TILE_SIZE_RATIO_HEIGHT;

		bookmarkButton = new TablexiaButton(
			(floorNumber + 1) + " " + buttonText,
			false,
			ApplicationAtlasManager.getInstance().getColorTextureRegion(BOOKMARKBUTTON_BACKGROUND_COLOR_UP),
			ApplicationAtlasManager.getInstance().getColorTextureRegion(BOOKMARKBUTTON_BACKGROUND_COLOR_CHECKED),
			null,
			null
		);
		bookmarkButton.adaptiveSize(false);
		bookmarkButton.getLabel().setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(BOOKMARK_TITLE_TEXT_FONT, BOOKMARKBUTTON_TEXT_COLOR));
		bookmarkButton.setSize(bookmarkWidth, bookmarkHeight);
		bookmarkButton.setPosition(BOOKMARKBUTTON_PADDING_LEFT + floorNumber * (bookmarkButton.getWidth() + (bookmarkButton.getWidth() * BOOKMARKBUTTON_PADDING_RATIO)), -bookmarkButton.getHeight());
		bookmarkButton.checkable(true);
		bookmarkButton.adaptiveSize(false);
		bookmarkButton.setInputListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				if (tileMapClickListener != null) {
					tileMapClickListener.onBookmarkClick(MapWidget.this.floorNumber);
				}
			}
		});
		bookmarkButton.setName((floorNumber + 1) + " " + InTheDarknessGame.BOOKMARK_FLOOR_TEXT_KEY);
		backgroundLayer.addActor(bookmarkButton);
	}
	
	private void showTileMap(InTheDarknessGame inTheDarknessGame) {

        Group tilesLayer = new Group();
        Group mapObjectsLayer = new Group();
        Group mapObstaclesLayer = new Group();
        Group rasterLayer = new Group();

		mapLayer.addActor(tilesLayer);
		mapLayer.addActor(mapObjectsLayer);
		mapLayer.addActor(mapObstaclesLayer);
		mapLayer.addActor(rasterLayer);

        for (int i = 0; i < tileMap.getMapXSize(); i++) {
            for (int j = 0; j < tileMap.getMapYSize(); j++) {
                Tile tile = tileMap.getTileAtPosition(i, j);
                if (tile != null) {
                    // tiles
                    tilesLayer.addActor(tile.getTileSprite(inTheDarknessGame));

                    // map objects
                    MapObject mapObject = tile.getMapObject();
                    if (mapObject != null) {
                        mapObjectsLayer.addActor(mapObject.getMapObjectSprite(inTheDarknessGame));
                    }

                    // map obstacles
                    for (MapObstacle mapObstacle : tile.getMapObstacles()) {
                        if (mapObstacle != null) {
                            mapObstaclesLayer.addActor(mapObstacle.getMapObstacleContainer(inTheDarknessGame));
                        }
                    }
                }
            }
        }
    }

}
