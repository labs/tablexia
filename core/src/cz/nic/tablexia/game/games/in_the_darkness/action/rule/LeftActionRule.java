/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness.action.rule;

import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.action.InTheDarknessActionType.IActionFinishedListener;
import cz.nic.tablexia.game.games.in_the_darkness.creature.Player;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap;

/**
 * Rule for LEFT action
 * 
 * @author Matyáš Latner
 *
 */
public class LeftActionRule extends AbstractActionTypeRule {

	@Override
	public void performActionRule(InTheDarknessGame inTheDarknessGame, TileMap tileMap, Player player, int mapStartPositionX, int mapStartPositionY, final IActionFinishedListener finishedListener) {
		player.turnPlayerLeft(new Runnable() {
			@Override
			public void run() {
				performOnActionFinished(finishedListener, true);
			}
		});
	}

}
