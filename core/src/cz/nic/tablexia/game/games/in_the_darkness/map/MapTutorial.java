/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness.map;

import java.util.Random;

import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobject.MapObjectType;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobstacle.MapObstacleType;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobstacle.MapObstacleType.MapObstaclePosition;
import cz.nic.tablexia.game.games.in_the_darkness.map.tile.Tile;
import cz.nic.tablexia.game.games.in_the_darkness.map.tile.TileType;

public class MapTutorial implements IMapProvider {

	public MapTutorial(Random randomAccess) {}

	@Override
	public TileMap prepareMap(Tile lastFinishTile, int mapXSize, int mapYSize, MapObjectType finishMapObject, boolean hasKey) {
		TileMap tileMap = new TileMap(mapXSize, mapYSize);
		
		TileMapPosition position1 = InTheDarknessGame.defaultMapStartPosition;
		TileMapPosition position2 = new TileMapPosition(position1.getPositionX() - 1, position1.getPositionY());
		TileMapPosition position3 = new TileMapPosition(position1.getPositionX() - 1, position1.getPositionY() - 1);
		tileMap.setMapStartPosition(position1);
		tileMap.setMapFinishPosition(position3);
		
		Tile tile1 = new Tile(TileType.TILE_2IH);
		Tile tile2 = new Tile(TileType.TILE_2LA);
		Tile tile3 = new Tile(TileType.TILE_1C);
		tileMap.addTileAtPosition(position1, tile1);
		tileMap.addTileAtPosition(position2, tile2);
		tileMap.addTileAtPosition(position3, tile3);
		
		tile3.setMapObstacle(MapObstaclePosition.BOTTOM_POSITION, MapObstacleType.DOOR_H);
		tile3.setMapObject(finishMapObject);
		
		// fill all map
		for (int i = 0; i < tileMap.getMapXSize(); i++) {
			for (int j = 0; j < tileMap.getMapYSize(); j++) {
				if (!tileMap.isTileAtPosition(i, j)) {
					Tile tile = new Tile(TileType.TILE_0);
					tileMap.addTileAtPosition(i, j, tile);
				}
			}
		}
		
		return tileMap;
	}
	
	

}