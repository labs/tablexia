/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package cz.nic.tablexia.game.games.in_the_darkness.action.rule;

import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.in_the_darkness.action.InTheDarknessActionType.IActionFinishedListener;
import cz.nic.tablexia.game.games.in_the_darkness.action.ActionUtility;
import cz.nic.tablexia.game.games.in_the_darkness.assets.InTheDarknessAssets;
import cz.nic.tablexia.game.games.in_the_darkness.creature.Player;
import cz.nic.tablexia.game.games.in_the_darkness.map.TileMap;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobstacle.MapObstacle;
import cz.nic.tablexia.game.games.in_the_darkness.map.mapobstacle.MapObstacleType;

/**
 * Rule for DOG action
 * 
 * @author Matyáš Latner
 * 
 */
public class DogActionRule extends AbstractActionTypeRule {

    @Override
    public void performActionRule(InTheDarknessGame inTheDarknessGame, TileMap tileMap, Player player, int mapStartPositionX, int mapStartPositionY, final IActionFinishedListener finishedListener) {
        Player.PlayerOrientation actualPlayerOrientation = player.getActualPlayerOrientation();
        TileMap.TileMapPosition actualTileMapPosition = player.getActualTileMapPosition();
        MapObstacle obstacleInDirection = ActionUtility.getObstacleInDirection(tileMap, actualTileMapPosition, actualPlayerOrientation);

        if (obstacleInDirection != null && (obstacleInDirection.getMapObstacleType() == MapObstacleType.DOG_H || obstacleInDirection.getMapObstacleType() == MapObstacleType.DOG_V)) {
            player.jumpToTileMapPosition(inTheDarknessGame, mapStartPositionX, mapStartPositionY, null, new Runnable() {
                @Override
                public void run() {
                    performOnActionFinished(finishedListener, true);
                }
            });
        } else {
            inTheDarknessGame.getSound(InTheDarknessAssets.SOUND_ERROR).play();
        	inTheDarknessGame.showCrashInfo();
        	performOnActionFinishWithDelay(player, finishedListener, false, InTheDarknessGame.ERROR_ACTION_DELAY);
        }
    }

}
