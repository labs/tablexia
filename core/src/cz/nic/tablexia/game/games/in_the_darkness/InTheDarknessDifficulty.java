/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.in_the_darkness;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.in_the_darkness.action.InTheDarknessActionType;
import cz.nic.tablexia.game.games.in_the_darkness.map.IMapProvider;
import cz.nic.tablexia.game.games.in_the_darkness.map.InTheDarknessMap;
import cz.nic.tablexia.game.games.in_the_darkness.map.MapGenerator;
import cz.nic.tablexia.game.games.in_the_darkness.map.MapTutorial;
import cz.nic.tablexia.util.Log;

/**
 * Game InTheDarkness difficulty definition and mapping to game difficulty
 * 
 * @author Matyáš Latner
 *
 */
public enum InTheDarknessDifficulty {
	
	TUTORIAL(GameDifficulty.TUTORIAL,	false,	MapTutorial.class,  1, Arrays.asList(new InTheDarknessActionType[]{}), InTheDarknessMap.SMALL_MAP),
	EASY	(GameDifficulty.EASY, 		true, 	MapGenerator.class, 1, Arrays.asList(new InTheDarknessActionType[]{}), InTheDarknessMap.SMALL_MAP),
	MEDIUM	(GameDifficulty.MEDIUM, 	true, 	MapGenerator.class, 1, Arrays.asList(new InTheDarknessActionType[]{}), InTheDarknessMap.LARGE_MAP),
	HARD	(GameDifficulty.HARD, 		true, 	MapGenerator.class, 1, Arrays.asList(new InTheDarknessActionType[]{InTheDarknessActionType.KEY}), InTheDarknessMap.LARGE_MAP),
	BONUS	(GameDifficulty.BONUS, 		true, 	MapGenerator.class, 2, Arrays.asList(new InTheDarknessActionType[]{InTheDarknessActionType.KEY}), InTheDarknessMap.LARGE_MAP);

	private GameDifficulty gameDifficulty;
	private int floorCount;
	private InTheDarknessMap map;
	private List<InTheDarknessActionType> difficultyInTheDarknessActionTypes;
	private Class<? extends IMapProvider> mapProviderClass;
	private boolean hasResults;

	InTheDarknessDifficulty(GameDifficulty gameDifficulty, boolean hasResults, Class<? extends IMapProvider> mapProviderClass, int floorCount, List<InTheDarknessActionType> difficultyInTheDarknessActionTypes, InTheDarknessMap map) {
		this.gameDifficulty			= gameDifficulty;
		this.hasResults 			= hasResults;
		this.mapProviderClass 		= mapProviderClass;
		this.floorCount 			= floorCount;
		this.difficultyInTheDarknessActionTypes = difficultyInTheDarknessActionTypes;
		this.map = map;
	}
	
	public boolean hasResults() {
		return hasResults;
	}
	
	public int getFloorCount() {
		return floorCount;
	}

	public InTheDarknessMap getMap() {
		return map;
	}

	public boolean hasDifficultyActionType(InTheDarknessActionType inTheDarknessActionType) {
		return difficultyInTheDarknessActionTypes.contains(inTheDarknessActionType);
	}
	
	public static InTheDarknessDifficulty getInTheDarknessDifficultyForGameDifficulty(GameDifficulty gameDifficulty) {
		for (InTheDarknessDifficulty inTheDarknessDifficulty : InTheDarknessDifficulty.values()) {
			if (inTheDarknessDifficulty.gameDifficulty == gameDifficulty) {
				return inTheDarknessDifficulty;
			}
		}
		return null;
	}
	
	public IMapProvider getMapProviderNewInstance(Random random) {
		try {
			return mapProviderClass.getConstructor(Random.class).newInstance(random);
		} catch (Exception e) {
			Log.err(InTheDarknessDifficulty.class, "Cannot create instance for IMapProvider class: " + mapProviderClass, e);
		}
		return null;
	}
	
}
