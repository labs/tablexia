/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.utils;

import com.badlogic.gdx.scenes.scene2d.InputEvent;


import java.util.List;

import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.memory.actors.MediumHardObjectPositioner;
import cz.nic.tablexia.game.games.memory.game_objects.GameObject;
import cz.nic.tablexia.game.games.memory.game_objects.NumberCardGroup;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.entity.Touch;
import cz.nic.tablexia.util.listener.DragActorListener;
import cz.nic.tablexia.game.games.memory.assets.MemoryAssets;


public class MemoryDragActorListener extends DragActorListener {
    private static final int CARD_AREA      = 40;

    private GameObject gameObject;
    private List<NumberCardGroup> numberCards;
    private MemoryGame memoryGame;
    MediumHardObjectPositioner mediumHardBonusObjectPositioner;
    private boolean dragged;
    private boolean moved;
    private GameObject rightCard;

    private Touch lastRecordedTouchDown;
    private boolean lastTapValid;
    private Touch lastRecordedTap;

    public MemoryDragActorListener(MediumHardObjectPositioner mediumHardBonusObjectPositioner, MemoryGame memoryGame, List<NumberCardGroup> numberCards, GameObject gameObject, boolean bounceBack, DragActorCallback dragDropListener) {
        super(gameObject, bounceBack, dragDropListener);

        this.mediumHardBonusObjectPositioner = mediumHardBonusObjectPositioner;
        this.gameObject = gameObject;
        this.numberCards = numberCards;
        this.memoryGame = memoryGame;
        this.dragged = false;
        this.moved = false;
        this.rightCard = null;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if(gameObject==null){
            Log.err(getClass(), "Game object is null!");
            return;
        }
        NumberCardGroup numberCard = isObjectCloseToCard(gameObject);
        if (numberCard != null) {
            memoryGame.getMusic(MemoryAssets.PUT_FIX_SOUND).play();
            Touch touchUp = new Touch(event.getListenerActor(), System.currentTimeMillis());
            lastTapValid = Touch.isTap(lastRecordedTouchDown, touchUp);
            if (lastTapValid) {
                if (lastRecordedTap == null) {
                    lastRecordedTap = touchUp;
                } else if (Touch.isDoubleTap(lastRecordedTap, touchUp)) {
                    mediumHardBonusObjectPositioner.unfixAndReturnObject(gameObject, true, null);
                    lastRecordedTap = null;
                } else {
                    lastRecordedTap = touchUp;
                }
            } else {
                numberCard.fixObjectWithId(gameObject);
                float objectYVariance = numberCard.getHeight() - gameObject.getHeight() / 2;
                float objectYOffset = (objectYVariance > 0) ? objectYVariance : 0;
                float objectNewX = numberCard.getX() + numberCard.getWidth() - gameObject.getWidth() / 2;
                float objectNewY = numberCard.getY() + objectYOffset;
                gameObject.setPosition(objectNewX, objectNewY);
                gameObject.setFixedToCardId(numberCard.getCardNumber());
                mediumHardBonusObjectPositioner.objectFixed();
            }
        } else if (mediumHardBonusObjectPositioner.isObjectInCardWidget(gameObject.getX(), gameObject.getY())) {
            memoryGame.getMusic(MemoryAssets.OBJECT_BACK_SOUND).play();
            mediumHardBonusObjectPositioner.unfixAndReturnObject(gameObject, dragged, rightCard);
        } else {
            memoryGame.getMusic(MemoryAssets.OUT_OF_POS_SOUND).play();
            memoryGame.getMusic(MemoryAssets.OBJECT_BACK2_SOUND).play();
            mediumHardBonusObjectPositioner.unfixAndReturnObject(gameObject, dragged, rightCard);
        }
        if (checkIfAllPositionsHasObjects()){
            mediumHardBonusObjectPositioner.allPositionsHaveObjects(memoryGame);
        }
        else{
            mediumHardBonusObjectPositioner.allPositionsDoesntHaveObjects(memoryGame);
        }
        dragged = false;
        moved = false;
        rightCard = null;
    }

    private boolean checkIfAllPositionsHasObjects() {
        for (NumberCardGroup numberCard : numberCards) {
            if (!numberCard.isFixedObject()) return false;
        }
        return true;
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if(gameObject==null){
            Log.err(getClass(), "Game object is null!");
        }
        else if(mediumHardBonusObjectPositioner.isObjectOutOfCardWidget(gameObject) && !moved) {
            dragged = true;
            gameObject.changeObjectToObject();
            rightCard = mediumHardBonusObjectPositioner.moveRightCards(gameObject);
            moved=true;
        }
        super.touchDragged(event, x, y, pointer);
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        lastRecordedTouchDown = new Touch(event.getListenerActor(), System.currentTimeMillis());
        mediumHardBonusObjectPositioner.checkAndUnfixToCard(gameObject);
        return super.touchDown(event, x, y, pointer, button);
    }


    private NumberCardGroup isObjectCloseToCard(GameObject gameObject) {
        float objectMiddleX = gameObject.getX() + gameObject.getWidth()/2;
        float objectY = gameObject.getY();
        for(NumberCardGroup numberCard: numberCards){
            if(!numberCard.isFixedObject()) {
                if ((numberCard.getX() + numberCard.getWidth()/2 - CARD_AREA < objectMiddleX && objectMiddleX < numberCard.getX() + numberCard.getWidth()/2 + CARD_AREA) && (numberCard.getY()- CARD_AREA < objectY && numberCard.getY() + numberCard.getHeight() + CARD_AREA > objectY)) {
                    return numberCard;
                }
            }
        }
        return null;
    }
}
