/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.model;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.memory.actors.MediumHardObjectPositioner;
import cz.nic.tablexia.game.games.memory.assets.MemoryAssets;
import cz.nic.tablexia.util.ui.button.TablexiaButton;


public class CardWidget extends Group {
    private static final int    MOVE_CARDS_BUTTON_WIDTH     = 25;
    private static final int    BUTTON_OFFSET_X             = 37;
    private static final float  MOVE_BUTTON_SCALE           = 0.3f;
    private static final float  MOVE_BUTTON_Y_OFFSET        = 0.38f;

    private MemoryGame memoryGame;
    MediumHardObjectPositioner mediumHardBonusObjectPositioner;
    private TablexiaButton moveCardsButton;

    public CardWidget(MemoryGame memoryGame, MediumHardObjectPositioner mediumHardBonusObjectPositioner, float cardWidgetX, float cardWidgetY, float cardWidgetWidth, float cardWidgetHeight) {
        setBounds(cardWidgetX, cardWidgetY, cardWidgetWidth, cardWidgetHeight);

        this.memoryGame = memoryGame;
        this.mediumHardBonusObjectPositioner = mediumHardBonusObjectPositioner;

        initMoveCardsButton();
    }

    private void initMoveCardsButton() {
        moveCardsButton = new TablexiaButton(null, false, memoryGame.getScreenTextureRegion(MemoryAssets.MOVE_CARDS_BUTTON), memoryGame.getScreenTextureRegion(MemoryAssets.MOVE_CARDS_BUTTON_PRESSED), null, memoryGame.getScreenTextureRegion(MemoryAssets.MOVE_CARDS_BUTTON_DISABLED));
        moveCardsButton.setSize(MOVE_CARDS_BUTTON_WIDTH, mediumHardBonusObjectPositioner.getCardWidgetImage().getHeight()*MOVE_BUTTON_SCALE);
        moveCardsButton.setPosition(mediumHardBonusObjectPositioner.getCardWidgetImage().getX() + mediumHardBonusObjectPositioner.getCardWidgetImage().getWidth() - BUTTON_OFFSET_X - moveCardsButton.getWidth(), mediumHardBonusObjectPositioner.getCardWidgetImage().getImageY() + mediumHardBonusObjectPositioner.getCardWidgetImage().getHeight()*MOVE_BUTTON_Y_OFFSET);
        moveCardsButton.adaptiveSizePositionFix(false);
        moveCardsButton.setEnabled();

        moveCardsButton.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mediumHardBonusObjectPositioner.moveCardsLeft();
            }
        });

        moveCardsButton.setEnabled(false);

        addActor(moveCardsButton);
    }

    public float getRightArrayX() {
        return (moveCardsButton.getX() - BUTTON_OFFSET_X);
    }

    public void setRightArrayClickable(boolean clickable) {
        if(clickable) moveCardsButton.setEnabled();
        else moveCardsButton.setDisabled();
    }
}
