/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.memory.game_objects.GameObject;
import cz.nic.tablexia.game.games.memory.game_objects.GameObjects;
import cz.nic.tablexia.game.games.memory.game_objects.MemoryImage;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;


public class BonusObjectPositioner extends MediumHardObjectPositioner {
    private static final int  EXTRA_OBJECTS = 2;

    private Group extraObjectGroup;
    private List<GameObject> listOfExtraObjects;
    protected GameObjects[] extraObjects;
    private boolean enoughtFixedObjectsSelected = false;
    private boolean allCardsHaveObject = false;

    public List<GameObject> getListOfExtraObjects() {
        return listOfExtraObjects;
    }

    public void prepare(MemoryGame memoryGame) {
        super.prepare(memoryGame);

        prepareExtraObjects(memoryGame);
        enoughtFixedObjectsSelected = false;
        allCardsHaveObject = false;
    }

    private void prepareExtraObjects(MemoryGame memoryGame) {
        listOfExtraObjects = new ArrayList<GameObject>();
        extraObjectGroup = new Group();

        //extra objects
        for(int i = 0; i < EXTRA_OBJECTS; i++){
            float x = getObjectX(extraObjects[i]);
            float y = getObjectY(extraObjects[i]);
            removePositionIfNeeded(extraObjects[i]);

            GameObject gameObject = new GameObject(memoryGame, extraObjects[i], memoryGame.getSceneLeftX(), memoryGame.getSceneInnerBottomY(), x, y);
            gameObject.setWidthAndHeight(gameObject.getWidth(), gameObject.getHeight());
            gameObject.setName(extraObjects[i].getObjectName());
            listOfExtraObjects.add(gameObject);
            gameObject.setPosition(gameObject.getGameObjectX(), gameObject.getGameObjectY());
            gameObject.setVisible(false);
            extraObjectGroup.addActor(gameObject);
        }
        getStage().addActor(extraObjectGroup);
    }

    @Override
    protected void shuffleAndSelectObjects(MemoryGame memoryGame) {
        List<GameObjects> listOfObjects = schuffleAndSelectOtherAndSelected(memoryGame.getGameDifficulty(), memoryGame);

        List<PositionPlacement> neededPositionOfConfusingObjects = findNeededPositions();

        int i = 0;
        confusingObjects = new GameObjects[numberOfConfusingObjects];

        for(PositionPlacement placement: neededPositionOfConfusingObjects){
            if(i >= numberOfConfusingObjects) break;
            GameObjects objectToRemoveFromList = null;
            for(GameObjects gameObject: listOfObjects){
                if(gameObject.getPlacement().equals(placement)){
                    confusingObjects[i] = gameObject;
                    objectToRemoveFromList = gameObject;
                    i++;
                    break;
                }
            }
            if(objectToRemoveFromList!=null) listOfObjects.remove(objectToRemoveFromList);
        }

        int j = 0;
        extraObjects = new GameObjects[EXTRA_OBJECTS];
        for(GameObjects gameObject: listOfObjects){
            if(listOfObjects.size()<=0) Log.err(getClass(), "List of object does not contain any object, need: " + (numberOfConfusingObjects-i) + " more objects");
            if(j>=EXTRA_OBJECTS){
                if(i>=numberOfConfusingObjects) break;
                confusingObjects[i] = gameObject;
                i++;
            }
            else{
                if(gameObject.getPlacement().equals(PositionPlacement.FIXED) || gameObject.getPlacement().equals(PositionPlacement.FREE)) {
                    extraObjects[j] = gameObject;
                    j++;
                }
                else{
                    if (containsMaxObjectPosition(shuffledOtherObjects, gameObject.getPlacement()) + containsMaxObjectPosition(shuffledSelectedObjects, gameObject.getPlacement()) + containsMaxObjectPosition(extraObjects, gameObject.getPlacement()) < gameObject.getMaximumOfObjectPlacements(memoryGame.getGameDifficulty())) {
                        extraObjects[j] = gameObject;
                        j++;
                    }
                }
            }
        }
    }

    @Override
    public void evaluate(MemoryGame memoryGame) {
        super.evaluate(memoryGame);

        for(GameObject extraObject: listOfExtraObjects){
            if(extraObject.isFixed()){
                extraObject.setColor(Color.GREEN);
            }
        }

        for (MemoryImage fixedGameObject : fixedGameObjects) {
            if(fixedGameObject.isSelected()){
                fixedGameObject.setColor(Color.RED);
                memoryGame.setMistakes(memoryGame.getMistakes() + 1);
            }
        }
    }

    @Override
    public void prepareObjectsForPlayer(MemoryGame memoryGame) {
        super.prepareObjectsForPlayer(memoryGame);
        for(GameObject extraObject: listOfExtraObjects){
            extraObject.setVisible(true);
        }
        addListenersToFixedObjects(memoryGame);
    }

    private void addListenersToFixedObjects(MemoryGame memoryGame) {
        for(GameObject extraObject: listOfExtraObjects){
            extraObject.addListener(
                    new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            if (!extraObject.isFixed()) {
                                memoryGame.setSelectedObjects(memoryGame.getSelectedObjects() + 1);
                                extraObject.setObjectSelected(memoryGame, true);
                            } else {
                                memoryGame.setSelectedObjects(memoryGame.getSelectedObjects() - 1);
                                extraObject.setObjectSelected(memoryGame, false);
                            }
                            if (memoryGame.getSelectedObjects() == EXTRA_OBJECTS) setEnoughtAndChcekDoneButton(true, memoryGame);
                            else setEnoughtAndChcekDoneButton(false, memoryGame);
                        }
                    });
        }

        for (MemoryImage fixedGameObject : fixedGameObjects) {
            fixedGameObject.addListener(
                    new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            if (!fixedGameObject.isSelected()) {
                                memoryGame.setSelectedObjects(memoryGame.getSelectedObjects() + 1);
                                fixedGameObject.setObjectSelected(memoryGame, true);
                            } else {
                                memoryGame.setSelectedObjects(memoryGame.getSelectedObjects() - 1);
                                fixedGameObject.setObjectSelected(memoryGame, false);
                            }
                            if (memoryGame.getSelectedObjects() == EXTRA_OBJECTS) setEnoughtAndChcekDoneButton(true, memoryGame);
                            else setEnoughtAndChcekDoneButton(false, memoryGame);
                        }
                    });
        }
    }

    private void setEnoughtAndChcekDoneButton(boolean enoughtObjectsSelected, MemoryGame memoryGame) {
        enoughtFixedObjectsSelected = enoughtObjectsSelected;
        checkDoneButtonEnabled(memoryGame);
    }

    @Override
    public void allPositionsHaveObjects(MemoryGame memoryGame) {
        allCardsHaveObject = true;
        checkDoneButtonEnabled(memoryGame);
    }

    @Override
    public void allPositionsDoesntHaveObjects(MemoryGame memoryGame) {
        allCardsHaveObject = false;
        memoryGame.disableDoneButton();
    }

    private void checkDoneButtonEnabled(MemoryGame memoryGame) {
        if(allCardsHaveObject && enoughtFixedObjectsSelected) memoryGame.enableDoneButton();
        else memoryGame.disableDoneButton();
    }
}
