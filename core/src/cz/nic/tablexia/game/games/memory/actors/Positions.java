/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.memory.actors;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.difficulty.GameDifficulty;

public enum Positions {
    //EASY POSITIONS
    EASY_ONE(180, 35, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_TWO(240, 30, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_THREE(250, 75, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_FOUR(320, 35, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_FIVE(250, 225, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_SIX(420, 155, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_SEVEN(520, 185, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_EIGHT(560, 30, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_NINE(620, 45, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_TEN(740, 315, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_ELEVEN(830, 170, GameDifficulty.EASY, PositionPlacement.FREE),
    EASY_TWELVE(920, 165, GameDifficulty.EASY, PositionPlacement.FREE),

    EASY_LEFT_WALL_1(50, 155, GameDifficulty.EASY, PositionPlacement.LEFT_WALL),
    EASY_LEFT_WALL_2(130, 235, GameDifficulty.EASY, PositionPlacement.LEFT_WALL),
    EASY_LEFT_WALL_3(70, 335, GameDifficulty.EASY, PositionPlacement.LEFT_WALL),
    EASY_LEFT_WALL_4(230, 325, GameDifficulty.EASY, PositionPlacement.LEFT_WALL),

    EASY_RIGHT_WALL_1(910, 325, GameDifficulty.EASY, PositionPlacement.RIGHT_WALL),

    EASY_SPECIAL_1(390, 60, GameDifficulty.EASY, PositionPlacement.SPECIAL),
    EASY_SPECIAL_2(370, 65, GameDifficulty.EASY, PositionPlacement.SPECIAL),

    EASY_ON_THE_SHELF(815, 305, GameDifficulty.EASY, PositionPlacement.ON_THE_SHELF),

    //MEDIUM POSITIONS
    MEDIUM_ONE(200, 30, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_TWO(130, 115, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_THREE(280, 235, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_FOUR(310, 30, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_FIVE(350, 145, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_SIX(400, 35, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_SEVEN(380, 230, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_EIGHT(460, 165, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_NINE(500, 30, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_TEN(730, 55, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_ELEVEN(680, 30, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_TWELVE(785, 225, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_THIRTEEN(850, 125, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_FOURTEEN(780, 310, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_FIVETEEN(870, 255, GameDifficulty.MEDIUM, PositionPlacement.FREE),
    MEDIUM_SIXNTEEN(870, 335, GameDifficulty.MEDIUM, PositionPlacement.FREE),

    MEDIUM_LEFT_WALL_1(40, 265, GameDifficulty.MEDIUM, PositionPlacement.LEFT_WALL),
    MEDIUM_LEFT_WALL_2(80, 405, GameDifficulty.MEDIUM, PositionPlacement.LEFT_WALL),
    MEDIUM_LEFT_WALL_3(210, 295, GameDifficulty.MEDIUM, PositionPlacement.LEFT_WALL),

    MEDIUM_RIGHT_WALL_3(610, 245, GameDifficulty.MEDIUM, PositionPlacement.RIGHT_WALL),

    //HARD POSITIONS
    HARD_ONE(35, 185, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_TWO(170, 210, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_THREE(180, 30, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_FOUR(300, 65, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_FIVE(110, 165, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_SIX(390, 110, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_SEVEN(500, 160, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_EIGHT(470, 240, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_NINE(580, 255, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_TEN(540, 30, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_ELEVEN(630, 155, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_TWELVE(640, 45, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_THIRTEEN(670, 115, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_FOURTEEN(450, 35, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_FIVETEEN(525, 110, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_SIXTEEN(715, 200, GameDifficulty.HARD, PositionPlacement.FREE),
    HARD_SEVENTEEN(740, 30, GameDifficulty.HARD, PositionPlacement.FREE),

    HARD_LEFT_WALL(900, 255, GameDifficulty.HARD, PositionPlacement.LEFT_WALL),

    HARD_RIGHT_WALL(70, 285, GameDifficulty.HARD, PositionPlacement.RIGHT_WALL),


    //BONUS POSITIONS
    BONUS_ONE(450, 30, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_TWO(405, 70, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_THREE(480, 235, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_FOUR(510, 130, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_FIVE(560, 35, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_SIX(560, 235, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_SEVEN(560, 140, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_EIGHT(645, 165, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_NINE(620, 245, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_TEN(700, 35, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_ELEVEN(800, 145, GameDifficulty.BONUS, PositionPlacement.FREE),
    BONUS_TWELVE(835, 215, GameDifficulty.BONUS, PositionPlacement.FREE),

    BONUS_LEFT_WALL_1(100, 255, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL),
    BONUS_LEFT_WALL_2(300, 255, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL),
    BONUS_LEFT_WALL_3(810, 315, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL),

    BONUS_RIGHT_WALL(830, 445, GameDifficulty.BONUS, PositionPlacement.RIGHT_WALL),

    BONUS_BIG_OBJECT_1(330, 65, GameDifficulty.BONUS, PositionPlacement.BIG_OBJECT),
    BONUS_BIG_OBJECT_2(160, 55, GameDifficulty.BONUS, PositionPlacement.BIG_OBJECT);

    private int x;
    private int y;
    private GameDifficulty gameDifficulty;
    private PositionPlacement placement;

    Positions(int x, int y, GameDifficulty gameDifficulty, PositionPlacement placement) {
        this.x = x;
        this.y = y;
        this.gameDifficulty = gameDifficulty;
        this.placement = placement;
    }

    public static List<Positions> getListOfShuffledPositions(GameDifficulty gameDifficulty, PositionPlacement placement){
        Random random = new Random();

        List<Positions> listOfPositions = new ArrayList<Positions>();
        for(Positions position: Positions.values()){
            if(position.placement.equals(placement) && gameDifficulty == position.gameDifficulty)listOfPositions.add(position);
        }

        if(listOfPositions.size()<=1) return listOfPositions;

        List<Positions> shuffeledPositions = new ArrayList<Positions>();
        int size = listOfPositions.size();
        for (int i = 0; i < size; i++) {
            Positions randomPosition = listOfPositions.get(random.nextInt(listOfPositions.size()));
            shuffeledPositions.add(randomPosition);
            listOfPositions.remove(randomPosition);
        }

        return shuffeledPositions;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
