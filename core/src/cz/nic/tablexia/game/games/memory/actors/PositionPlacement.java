/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.memory.actors;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.util.Log;

public enum PositionPlacement {
    FREE(9,11,13,11),
    FIXED(9,11,13,11),
    LEFT_WALL(2,2,1,3),
    RIGHT_WALL(1,1,1,1),
    BIG_OBJECT(0,0,0,2),
    SPECIAL(1,0,0,0),
    ON_THE_SHELF(1,0,0,0);

    int maxEasy;
    int maxMedium;
    int maxHard;
    int maxBonus;

    PositionPlacement(int maxEasy, int maxMedium, int maxHard, int maxBonus) {
        this.maxEasy = maxEasy;
        this.maxMedium = maxMedium;
        this.maxHard = maxHard;
        this.maxBonus = maxBonus;
    }

    public static int getMaximum(PositionPlacement placement, GameDifficulty gameDifficulty) {
        for(PositionPlacement positionPlacement: PositionPlacement.values()){
            if(placement.equals(positionPlacement)) {
                int max = positionPlacement.getMaxForDifficulty(gameDifficulty);
                return max;
            }
        }
        Log.err("PositionPlacement", "Unknown PositionPlacement!");
        return 0;
    }

    private int getMaxForDifficulty(GameDifficulty gameDifficulty) {
        if(gameDifficulty == GameDifficulty.EASY) return maxEasy;
        if(gameDifficulty == GameDifficulty.MEDIUM) return maxMedium;
        if(gameDifficulty == GameDifficulty.HARD) return maxHard;
        if(gameDifficulty == GameDifficulty.BONUS) return maxBonus;

        Log.err(getClass(), "Unknown game difficulty!");
        return 0;
    }
}
