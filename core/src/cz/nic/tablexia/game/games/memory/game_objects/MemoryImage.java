/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.memory.game_objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.memory.assets.MemoryAssets;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

public class MemoryImage extends Image {
    TextureRegion screenTextureNormal;
    boolean selected;

    public MemoryImage(TextureRegion screenTextureRegionNormal) {
        super(screenTextureRegionNormal);

        this.screenTextureNormal = screenTextureRegionNormal;
        this.selected = false;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setObjectSelected(MemoryGame memoryGame, boolean selected) {
        this.selected = selected;
        if(selected){
            memoryGame.getMusic(MemoryAssets.PUT_FIX_SOUND).play();
            setColor(Color.CYAN);
        }
        else{
            memoryGame.getMusic(MemoryAssets.OBJECT_BACK2_SOUND).play();
            setColor(new Color(1, 1, 1, 1));
        }
        if(getColor().equals(Color.CYAN)) AbstractTablexiaScreen.triggerScenarioStepEvent(GameObject.SCENARIO_STEP_ACTOR_SELECTED);
    }
}
