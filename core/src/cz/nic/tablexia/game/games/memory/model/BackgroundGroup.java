/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.memory.model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.GdxRuntimeException;

import cz.nic.tablexia.util.Log;


public class BackgroundGroup extends Group {
    private ShaderProgram shaderProgram;
    private float grayscale = 0f;
    private boolean useShader = false;

    public BackgroundGroup(){
        super();

        shaderProgram = new ShaderProgram(Shader.VERT, Shader.FRAG);
        if (!shaderProgram.isCompiled()) {
            throw new GdxRuntimeException("Couldn't compile shader: " + shaderProgram.getLog());
        } else {
            Log.info(getClass(), "Shader compiled");
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(useShader) {
            batch.end();
            batch.begin();

            batch.end();

            shaderProgram.begin();
            shaderProgram.setUniformf("grayscale", grayscale);
            shaderProgram.end();
            batch.setShader(shaderProgram);
            batch.begin();
        }

        super.draw(batch, parentAlpha);
    }

    public void enableShader() {
        useShader = true;
    }

    public void disableShader() {
        useShader = false;
    }

    public void disposeShader() {
        shaderProgram.dispose();
    }
}
