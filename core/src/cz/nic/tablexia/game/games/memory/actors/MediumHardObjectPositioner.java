/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.actors;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.memory.assets.MemoryAssets;
import cz.nic.tablexia.game.games.memory.game_objects.GameObject;
import cz.nic.tablexia.game.games.memory.game_objects.GameObjects;
import cz.nic.tablexia.game.games.memory.game_objects.NumberCardGroup;
import cz.nic.tablexia.game.games.memory.model.CardWidget;
import cz.nic.tablexia.game.games.memory.utils.MemoryDragActorListener;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.listener.DragActorListener;

public class MediumHardObjectPositioner extends AbstractObjectPositioner {
    public static final String SCENARIO_DROP_EVENT       = "drop event";

    private static final int OBJECT_CARD_OFFSET_X   = 5;
    private static final int OBJECT_CARD_OFFSET_Y   = 20;
    private static final int CARD_OFFSET            = 30;
    private static final int CARD_WIDGET_WIDTH      = 600;
    private static final int CARD_WIDGET_HEIGHT     = 120;
    private Image cardWidgetImage;
    private CardWidget cardWidget;
    private List<GameObject> cardsInWidget;
    protected GameObjects[] confusingObjects;

    public Image getCardWidgetImage() {
        return cardWidgetImage;
    }

    public List<GameObject> getCardsInWidget() {
        return cardsInWidget;
    }

    public void prepare(MemoryGame memoryGame) {
        initGroupsAndLists(memoryGame);

        shuffleAndSelectObjects(memoryGame);

        //confusing objects
        for(int i = 0; i < numberOfConfusingObjects; i++){
            float x = 0;
            float y = 0;

            GameObject gameObject = new GameObject(memoryGame, confusingObjects[i], memoryGame.getViewportLeftX(), memoryGame.getViewportBottomY(), x, y);
            gameObject.setWidthAndHeight(gameObject.getWidth(), gameObject.getHeight());
            gameObject.setName(confusingObjects[i].getObjectName());
            listOfObjectCards.add(gameObject);
            gameObject.setPosition(gameObject.getGameObjectX(), gameObject.getGameObjectY());
            gameObject.setVisible(false);
            confusingObjectGroup.addActor(gameObject);
        }

        shuffleCardObjects(listOfObjectCards);
    }

    protected void shuffleAndSelectObjects(MemoryGame memoryGame) {
        List<GameObjects> listOfObjects = schuffleAndSelectOtherAndSelected(memoryGame.getGameDifficulty(), memoryGame);
        List<GameObjects> objectToRemove = new ArrayList<GameObjects>();

        List<PositionPlacement> neededPositionOfConfusingObjects = findNeededPositions();

        //confusing objects
        confusingObjects = new GameObjects[numberOfConfusingObjects];
        int i = 0;
        for(PositionPlacement placement: neededPositionOfConfusingObjects){
            if(i >= numberOfConfusingObjects) break;
            for(GameObjects gameObject: listOfObjects){
                if(gameObject.getPlacement().equals(placement)){
                    confusingObjects[i] = gameObject;
                    objectToRemove.add(gameObject);
                    i++;
                    break;
                }
            }
        }

        for(GameObjects gameObject: listOfObjects){
            if(listOfObjects.size() <= 0) Log.err(getClass(), "List of object does not contain any object, need: " + (numberOfConfusingObjects-i) + " more objects");
            if(i >= numberOfConfusingObjects) break;
            if(listOfObjects.size() <= (listOfObjects.indexOf(gameObject) + numberOfConfusingObjects - i) || gameObject.getPlacement().equals(PositionPlacement.FREE) || gameObject.getPlacement().equals(PositionPlacement.FIXED)) {
                confusingObjects[i] = gameObject;
                objectToRemove.add(gameObject);
                i++;
            }
        }

        for(GameObjects gameObjectForRemove: objectToRemove){
            listOfObjects.remove(gameObjectForRemove);
        }
    }

    protected List<PositionPlacement> findNeededPositions() {
        List<PositionPlacement> placements = new ArrayList<PositionPlacement>();
        for(GameObjects selectedObject: shuffledSelectedObjects){
            if(selectedObject==null) break;
            PositionPlacement placement = selectedObject.getPlacement();
            if(!placement.equals(PositionPlacement.FREE) && !placement.equals(PositionPlacement.FIXED)){
                boolean isInList = false;
                for(PositionPlacement posPlacement:placements){
                    if(placement.equals(posPlacement)) isInList = true;
                }
                if(!isInList) placements.add(placement);
            }
        }
        return placements;
    }

    private void shuffleCardObjects(List<GameObject> listOfObjectCards) {
        Random random = new Random();

        shuffledObjectCards = new GameObject[confusingObjects.length + numberOfSelectedObjects];
        for(int i = 0; i< confusingObjects.length + numberOfSelectedObjects; i++){
            GameObject randomObject = listOfObjectCards.get(random.nextInt(listOfObjectCards.size()));
            shuffledObjectCards[i] = randomObject;
            listOfObjectCards.remove(randomObject);
        }
    }

    @Override
    public void evaluate(MemoryGame memoryGame) {
        for (GameObject shuffledObjectCard : shuffledObjectCards) {
            shuffledObjectCard.removeListener(shuffledObjectCard.getDragListener());
        }
        for (NumberCardGroup numberCard : numberCards) {
            if (numberCard.isGameObjectFixedRight())
                paintItGreen(numberCard.getFixedGameObject());
            else {
                paintItRed(numberCard.getFixedGameObject());
                memoryGame.setMistakes(memoryGame.getMistakes() + 1);
            }
        }
    }

    @Override
    public void prepareObjectsForPlayer(MemoryGame memoryGame) {
        cardWidget.setVisible(true);
        cardWidgetImage.setVisible(true);
        moveObjectsToCardWidget(memoryGame);
    }

    private void moveObjectsToCardWidget(MemoryGame memoryGame) {
        float offset = cardWidgetImage.getX() + CARD_OFFSET;
        float y = cardWidget.getY();
        cardsInWidget = new ArrayList<GameObject>();

        for(GameObject gameObject : shuffledObjectCards){
            if(gameObject==null){
                Log.err(getClass(), "Shuffled game object is null!");
                continue;
            }
            gameObject.changeObjectToCard();
            gameObject.setPosition(offset,y + OBJECT_CARD_OFFSET_Y);
            gameObject.setCardInitPosition(gameObject.getX(), gameObject.getY());
            offset += (gameObject.getWidth() + OBJECT_CARD_OFFSET_X);
            if(!gameObject.isVisible()) gameObject.setVisible(true);
            gameObject.setDragListener(new MemoryDragActorListener(this,memoryGame, numberCards, gameObject, true, new DragActorListener.DragListenerAdapter()));
            gameObject.addListener(gameObject.getDragListener());
            cardsInWidget.add(gameObject);
            cardVisible(gameObject);
        }

        setButtonClickableIfNeeded(cardWidget.getRightArrayX());
    }

    public void setButtonClickableIfNeeded(float rightArrayX) {
        boolean buttonForMovingCardsClickable = false;
        for(GameObject cardInWidget: cardsInWidget){
            if(cardInWidget.getX() + cardInWidget.getWidth() > rightArrayX) buttonForMovingCardsClickable = true;
        }
        if(buttonForMovingCardsClickable) cardWidget.setRightArrayClickable(true);
        else cardWidget.setRightArrayClickable(false);
    }

    private void checkWidgetCardsVisibility() {
        for(GameObject cardInWidget: cardsInWidget){
            if(cardInWidget==null){
                Log.err(getClass(), "Card in widget is null!");
                continue;
            }
            cardVisible(cardInWidget);
        }
    }

    private void cardVisible(GameObject gameObject) {
        if(gameObject.getX() + gameObject.getWidth() > cardWidget.getRightArrayX() ) gameObject.setVisible(false);
        else gameObject.setVisible(true);
    }

    @Override
    public Group addOtherButtonsAndWidgets(MemoryGame memoryGame) {
        Group buttonsAndWidgetsGroup = new Group();
        cardWidgetImage = new Image(memoryGame.getScreenTextureRegion(MemoryAssets.CARD_WIDGET));
        cardWidgetImage.setSize(CARD_WIDGET_WIDTH, CARD_WIDGET_HEIGHT);
        cardWidgetImage.setPosition(memoryGame.getViewportLeftX() + (memoryGame.getSceneWidth() - cardWidgetImage.getWidth())/2, memoryGame.getViewportTopY() - cardWidgetImage.getHeight());
        buttonsAndWidgetsGroup.addActor(cardWidgetImage);

        cardWidget = new CardWidget(memoryGame, this, memoryGame.getViewportLeftX(), memoryGame.getViewportTopY() - cardWidgetImage.getHeight(), memoryGame.getViewportWidth(), cardWidgetImage.getHeight());
        buttonsAndWidgetsGroup.addActor(cardWidget);
        cardWidget.setVisible(false);
        cardWidgetImage.setVisible(false);
        return buttonsAndWidgetsGroup;
    }

    public boolean isObjectInCardWidget(float objectX, float objectY) {
        return cardWidget.getX() < objectX && cardWidget.getX() + cardWidget.getWidth() > objectX && cardWidget.getY() - 100 < objectY && cardWidget.getY() + cardWidget.getHeight() > objectY;
    }

    public void unfixAndReturnObject(GameObject gameObject, boolean dragged, GameObject rightCard) {
        checkAndUnfixToCard(gameObject);
        GameObject lastCard = cardsInWidget.get(cardsInWidget.size()-1);
        float x = (dragged && rightCard==null) ? lastCard.getGameObjectCardX() + OBJECT_CARD_OFFSET_X : gameObject.getGameObjectCardX();
        float y = (dragged && rightCard==null) ? lastCard.getGameObjectCardY() : gameObject.getGameObjectCardY();
        if(rightCard!=null) {
            x = rightCard.getGameObjectCardX();
            gameObject.setCardInitPosition(x, y);
            int index = moveRightCardsRight(rightCard);
            cardsInWidget.add(index, gameObject);
        }
        else if(dragged) cardsInWidget.add(gameObject);
        gameObject.goBackToCardWidget(x, y, dragged, rightCard);
        checkWidgetCardsVisibility();
        setButtonClickableIfNeeded(cardWidget.getRightArrayX());
    }

    public void checkAndUnfixToCard(GameObject gameObject) {
        if(gameObject==null){
            Log.err(getClass(), "Game object is null!");
            return;
        }
        if(gameObject.isFixed()) {
            for (NumberCardGroup numberCard : numberCards) {
                if (numberCard.isFixedObject()) {
                    if (numberCard.getCardNumber() == gameObject.getFixedToCardId()){
                        numberCard.unfixObject();
                        return;
                    }
                }
            }
        }
    }

    public GameObject moveRightCards(GameObject gameObject) {
        boolean moveCard = false;
        float x = gameObject.getGameObjectCardX();
        float y = gameObject.getGameObjectCardY();
        GameObject rightCard = null;

        for(GameObject cardInWidget: cardsInWidget) { //cards in widget
            if(!moveCard  && cardInWidget.equals(gameObject)){
                x = cardInWidget.getGameObjectCardX();
                y = cardInWidget.getGameObjectCardY();
                moveCard = true;
            } else if (moveCard){
                if(rightCard == null){
                    rightCard=cardInWidget;
                }
                //move card to left
                float oldX = cardInWidget.getGameObjectCardX();
                float oldY = cardInWidget.getGameObjectCardY();
                cardInWidget.setPosition(x,y);
                cardInWidget.setCardInitPosition(x, y);
                x = oldX;
                y = oldY;
            } else {
                x = cardInWidget.getGameObjectCardX();
                y = cardInWidget.getGameObjectCardY();
            }
        }
        cardsInWidget.remove(gameObject);
        checkWidgetCardsVisibility();
        gameObject.setCardInitPosition(x,y);
        return rightCard;
    }

    public int moveRightCardsRight(GameObject gameObject) {
        boolean moveCard = false;
        float x = gameObject.getGameObjectCardX();
        float y = gameObject.getGameObjectCardY();

        int index = 0;
        int i=0;
        for(GameObject cardInWidget: cardsInWidget) { //cards in widget
            if(!moveCard  && cardInWidget.equals(gameObject)){
                x = cardInWidget.getGameObjectCardX() + (gameObject.getWidth() + OBJECT_CARD_OFFSET_X);
                y = cardInWidget.getGameObjectCardY();
                cardInWidget.setPosition(x,y);
                cardInWidget.setCardInitPosition(x, y);
                moveCard = true;
                index=i;
            } else if (moveCard) {
                //move card to left
                x += (cardInWidget.getWidth() + OBJECT_CARD_OFFSET_X);
                cardInWidget.setPosition(x, y);
                cardInWidget.setCardInitPosition(x, y);
            }
            i++;
        }
        checkWidgetCardsVisibility();
        return index;
    }

    public boolean isObjectOutOfCardWidget(GameObject gameObject) {
        if(gameObject.getY() + gameObject.getHeight()/2 < cardWidget.getY() || gameObject.getX() < cardWidgetImage.getX() || gameObject.getX() > cardWidget.getRightArrayX() ){
            setButtonClickableIfNeeded(cardWidget.getRightArrayX());
            return true;
        }
        return false;
    }

    public void moveCardsLeft() {
        GameObject firstCard = cardsInWidget.get(0);
        GameObject lastCard = cardsInWidget.get(cardsInWidget.size()-1);
        cardsInWidget.remove(0);
        cardsInWidget.add(firstCard);
        firstCard.setX(lastCard.getX() + (lastCard.getWidth() + OBJECT_CARD_OFFSET_X));
        for(GameObject cardInWidget: cardsInWidget){
            cardInWidget.setX(cardInWidget.getX() - (cardInWidget.getWidth() + OBJECT_CARD_OFFSET_X));
            cardInWidget.setCardInitPosition(cardInWidget.getX(), cardInWidget.getY());
        }
        setButtonClickableIfNeeded(cardWidget.getRightArrayX());
        checkWidgetCardsVisibility();
    }

    public void allPositionsHaveObjects(MemoryGame memoryGame) {
        memoryGame.enableDoneButton();
    }

    public void allPositionsDoesntHaveObjects(MemoryGame memoryGame) {
        memoryGame.disableDoneButton();
    }

    public void objectFixed() {
        setButtonClickableIfNeeded(cardWidget.getRightArrayX());
        AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_DROP_EVENT);
    }
}
