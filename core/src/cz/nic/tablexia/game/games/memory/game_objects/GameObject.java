/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.game_objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.memory.assets.MemoryAssets;
import cz.nic.tablexia.game.games.memory.utils.MemoryDragActorListener;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.listener.DragActorListener;

public class GameObject extends Image {
    public static final String SCENARIO_STEP_ACTOR_SELECTED = "actor selected";
    private static final int CARD_SIZE = 90;

    private TextureRegion gameObjectScreenTexture;
    private TextureRegion gameCardScreenTexture;
    private int defaultCardNumber;
    private float gameObjectX;
    private float gameObjectY;
    private float gameObjectCardX;
    private float gameObjectCardY;
    private float gameObjectWidth;
    private float gameObjectHeight;
    private MemoryDragActorListener dragListener;
    private boolean fixed;
    private int fixedToCardId;


    //constructor for selected object (marked by card with number)
    public GameObject(MemoryGame memoryGame, GameObjects gameSelectedObjects, float viewportLeftX, float viewportBottomY, int cardNumber, float x, float y) {
        super(memoryGame.getScreenTextureRegion(gameSelectedObjects.getAssetPath()));

        this.gameObjectScreenTexture = memoryGame.getScreenTextureRegion(gameSelectedObjects.getAssetPath());
        if(gameSelectedObjects.getObjectCardAssetPath() != null) this.gameCardScreenTexture = memoryGame.getScreenTextureRegion(gameSelectedObjects.getObjectCardAssetPath());
        else this.gameCardScreenTexture = null;
        this.defaultCardNumber = cardNumber;
        this.gameObjectX = viewportLeftX + x;
        this.gameObjectY = viewportBottomY + y;
        this.fixed = false;

        if (TablexiaSettings.getInstance().isUseHdAssets()) this.setSize(this.getWidth()/2, this.getHeight()/2);
    }

    //constructor for confusing objects
    public GameObject(MemoryGame memoryGame, GameObjects gameObject, float viewportLeftX, float viewportBottomY, float x, float y) {
        super(memoryGame.getScreenTextureRegion(gameObject.getAssetPath()));

        this.gameObjectScreenTexture = memoryGame.getScreenTextureRegion(gameObject.getAssetPath());
        if(gameObject.getObjectCardAssetPath() != null) this.gameCardScreenTexture = memoryGame.getScreenTextureRegion(gameObject.getObjectCardAssetPath());
        else this.gameCardScreenTexture = null;
        this.defaultCardNumber = 0;
        this.gameObjectX = viewportLeftX + x;
        this.gameObjectY = viewportBottomY + y;
        this.fixed = false;
        this.fixedToCardId = 0;

        if (TablexiaSettings.getInstance().isUseHdAssets()) this.setSize(this.getWidth()/2, this.getHeight()/2);
    }

    public float getGameObjectX() {
        return gameObjectX;
    }

    public float getGameObjectY() {
        return gameObjectY;
    }

    public float getGameObjectCardX() {
        return gameObjectCardX;
    }

    public float getGameObjectCardY() {
        return gameObjectCardY;
    }

    public boolean isFixed() {
        return fixed;
    }

    public void setDragListener(MemoryDragActorListener dragListener){
        this.dragListener = dragListener;
    }

    public DragActorListener getDragListener() {
        return dragListener;
    }

    public int getDefaultCardNumber() {
        return defaultCardNumber;
    }

    public int getFixedToCardId() {
        return fixedToCardId;
    }

    public void setWidthAndHeight(float width, float height) {
        this.gameObjectWidth = width;
        this.gameObjectHeight = height;
    }

    public void setFixedToCardId(int cardId) {
        this.fixed = true;
        this.fixedToCardId = cardId;
    }

    private void setUnfixedToCardId() {
        this.fixed = false;
        this.fixedToCardId = 0;
    }

    public void setCardInitPosition(float x, float y) {
        this.gameObjectCardX = x;
        this.gameObjectCardY = y;
    }

    public void changeObjectToCard(){
        if(gameCardScreenTexture==null) Log.err(getClass(), "Card cannot be created - it has no texture!");
        if(!fixed) {
            setDrawable(new TextureRegionDrawable(gameCardScreenTexture));
            setSize(CARD_SIZE, CARD_SIZE);
        }
    }

    public void changeObjectToObject(){
        setDrawable(new TextureRegionDrawable(gameObjectScreenTexture));
        setSize(gameObjectWidth, gameObjectHeight);
    }

    public void goBackToCardWidget(float x, float y, boolean dragged, GameObject rightCard) {
        this.setUnfixedToCardId();
        this.changeObjectToCard();
        if(dragged && rightCard==null) x += CARD_SIZE;
        this.setPosition(x, y);
        this.setCardInitPosition(x, y);
    }

    public void setObjectSelected(MemoryGame memoryGame, boolean selected) {
        fixed = selected;
        if(selected){
            memoryGame.getMusic(MemoryAssets.PUT_FIX_SOUND).play();
            setColor(Color.CYAN);
        }
        else{
            memoryGame.getMusic(MemoryAssets.OBJECT_BACK2_SOUND).play();
            setColor(new Color(1, 1, 1, 1));
        }
        if(getColor().equals(Color.CYAN)) AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_STEP_ACTOR_SELECTED);
    }
}
