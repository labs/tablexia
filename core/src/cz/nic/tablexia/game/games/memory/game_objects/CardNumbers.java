/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.game_objects;

import cz.nic.tablexia.game.games.memory.assets.MemoryAssets;

public enum CardNumbers {
    ONE(1, MemoryAssets.ONE),
    TWO(2, MemoryAssets.TWO),
    THREE(3, MemoryAssets.THREE),
    FOUR(4, MemoryAssets.FOUR),
    FIVE(5, MemoryAssets.FIVE),
    SIX(6, MemoryAssets.SIX),
    SEVEN(7, MemoryAssets.SEVEN),
    EIGHT(8, MemoryAssets.EIGHT),
    NINE(9, MemoryAssets.NINE),
    ZERO(0, MemoryAssets.ZERO);

    private int id;
    private String numberAssetPath;

    CardNumbers(int id, String numberAssetPath) {
        this.id = id;
        this.numberAssetPath = numberAssetPath;
    }

    public static CardNumbers findAssetById(int searchedId) {
        for (CardNumbers card : CardNumbers.values()) {
            if(card.id == searchedId) return card;
        }
        return CardNumbers.ZERO;
    }

    public int getId() {
        return id;
    }

    public String getNumberAssetPath() {
        return numberAssetPath;
    }
}
