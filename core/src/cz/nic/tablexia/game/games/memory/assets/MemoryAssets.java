/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.assets;

public class MemoryAssets {
    private static final String GFX_PATH            = "gfx/";
    private static final String COMMON              = "common/";
    private static final String SFX_PATH            = "sfx/";
    private static final String OBJECTS             = "objects/";
    private static final String CARDS               = "cards/";
    private static final String OBJECT_CARDS        = "objectCards/";
    private static final String EASY                = "easy/";
    private static final String MEDIUM              = "medium/";
    private static final String HARD                = "hard/";
    private static final String BONUS               = "bonus/";

    public static final String MEMORY_ROOM_EASY             = GFX_PATH + EASY + "background";
    public static final String MEMORY_ROOM_MEDIUM           = GFX_PATH + MEDIUM + "background";
    public static final String MEMORY_ROOM_HARD             = GFX_PATH + HARD + "background";
    public static final String MEMORY_ROOM_BONUS            = GFX_PATH + BONUS + "background";
    public static final String PHOTO_FRAME                  = GFX_PATH + "photo_frame";
    public static final String CARD_WIDGET                  = GFX_PATH + "card_widget";
    public static final String MOVE_CARDS_BUTTON            = GFX_PATH + "move_cards_button";
    public static final String MOVE_CARDS_BUTTON_PRESSED    = GFX_PATH + "move_cards_button_pressed";
    public static final String MOVE_CARDS_BUTTON_DISABLED   = GFX_PATH + "move_cards_button_disabled";

    //======= EASY OBJECTS ==============
    public static final String BASKET               = GFX_PATH + EASY + OBJECTS + "basket";
    public static final String BOWL                 = GFX_PATH + EASY + OBJECTS + "bowl";
    public static final String CAN                  = GFX_PATH + EASY + OBJECTS + "can";
    public static final String CAN2                 = GFX_PATH + EASY + OBJECTS + "can2";
    public static final String CLOCK                = GFX_PATH + EASY + OBJECTS + "clock";
    public static final String CLOCK2               = GFX_PATH + EASY + OBJECTS + "clock2";
    public static final String CLOTH                = GFX_PATH + EASY + OBJECTS + "cloth";
    public static final String CLOTH2               = GFX_PATH + EASY + OBJECTS + "cloth2";
    public static final String CUP                  = GFX_PATH + EASY + OBJECTS + "cup";
    public static final String CHUNK                = GFX_PATH + EASY + OBJECTS + "chunk";
    public static final String KETTLE               = GFX_PATH + EASY + OBJECTS + "kettle";
    public static final String LADDLE               = GFX_PATH + EASY + OBJECTS + "laddle";
    public static final String PICTURE              = GFX_PATH + EASY + OBJECTS + "picture";
    public static final String PICTURE2             = GFX_PATH + EASY + OBJECTS + "picture2";
    public static final String PICTURE3             = GFX_PATH + EASY + OBJECTS + "picture3";
    public static final String PICTURE4             = GFX_PATH + EASY + OBJECTS + "picture4";
    public static final String PICTURE5             = GFX_PATH + EASY + OBJECTS + "picture5";
    public static final String PLATE                = GFX_PATH + EASY + OBJECTS + "plate";
    public static final String PLATE2               = GFX_PATH + EASY + OBJECTS + "plate2";
    public static final String PLATE3               = GFX_PATH + EASY + OBJECTS + "plate3";
    public static final String PLATE4               = GFX_PATH + EASY + OBJECTS + "plate4";
    public static final String POT                  = GFX_PATH + EASY + OBJECTS + "pot";
    public static final String ROLLER               = GFX_PATH + EASY + OBJECTS + "roller";
    public static final String VASE                 = GFX_PATH + EASY + OBJECTS + "vase";

    //======= MEDIUM OBJECTS ==============
    public static final String BOX1                 = GFX_PATH + MEDIUM + OBJECTS + "box1";
    public static final String BOX2                 = GFX_PATH + MEDIUM + OBJECTS + "box2";
    public static final String BOX3                 = GFX_PATH + MEDIUM + OBJECTS + "box3";
    public static final String FRAME                = GFX_PATH + MEDIUM + OBJECTS + "frame";
    public static final String FRAME2               = GFX_PATH + MEDIUM + OBJECTS + "frame2";
    public static final String FRAME3               = GFX_PATH + MEDIUM + OBJECTS + "frame3";
    public static final String FRAME4               = GFX_PATH + MEDIUM + OBJECTS + "frame4";
    public static final String MED_CAN1             = GFX_PATH + MEDIUM + OBJECTS + "can1";
    public static final String MED_CAN2             = GFX_PATH + MEDIUM + OBJECTS + "can2";
    public static final String MED_CLOCK            = GFX_PATH + MEDIUM + OBJECTS + "clock";
    public static final String DUCK_TAPE            = GFX_PATH + MEDIUM + OBJECTS + "duck_tape";
    public static final String LAMP                 = GFX_PATH + MEDIUM + OBJECTS + "lamp";
    public static final String LETTER1              = GFX_PATH + MEDIUM + OBJECTS + "letter1";
    public static final String LETTER2              = GFX_PATH + MEDIUM + OBJECTS + "letter2";
    public static final String LETTER3              = GFX_PATH + MEDIUM + OBJECTS + "letter3";
    public static final String PAPERS               = GFX_PATH + MEDIUM + OBJECTS + "papers";
    public static final String MED_PICTURE1         = GFX_PATH + MEDIUM + OBJECTS + "picture1";
    public static final String MED_PICTURE2         = GFX_PATH + MEDIUM + OBJECTS + "picture2";
    public static final String PICTURE_LETTER       = GFX_PATH + MEDIUM + OBJECTS + "picture_letter";
    public static final String PILLOW               = GFX_PATH + MEDIUM + OBJECTS + "pillow";
    public static final String STAMP                = GFX_PATH + MEDIUM + OBJECTS + "stamp";
    public static final String TWINE                = GFX_PATH + MEDIUM + OBJECTS + "twine";

    //======= HARD OBJECTS ==============
    public static final String BOX                  = GFX_PATH + HARD + OBJECTS + "box";
    public static final String BRICK1               = GFX_PATH + HARD + OBJECTS + "brick1";
    public static final String BRICK2               = GFX_PATH + HARD + OBJECTS + "brick2";
    public static final String BRICK3               = GFX_PATH + HARD + OBJECTS + "brick3";
    public static final String BRICK4               = GFX_PATH + HARD + OBJECTS + "brick4";
    public static final String CERTIFICATE          = GFX_PATH + HARD + OBJECTS + "certificate";
    public static final String CERTIFICATE2         = GFX_PATH + HARD + OBJECTS + "certificate2";
    public static final String CERTIFICATE3         = GFX_PATH + HARD + OBJECTS + "certificate3";
    public static final String COINS1               = GFX_PATH + HARD + OBJECTS + "coins1";
    public static final String COINS2               = GFX_PATH + HARD + OBJECTS + "coins2";
    public static final String CHAQUES              = GFX_PATH + HARD + OBJECTS + "chaques";
    public static final String INSTRUCTION          = GFX_PATH + HARD + OBJECTS + "instruction";
    public static final String INSTRUCTION2         = GFX_PATH + HARD + OBJECTS + "instruction2";
    public static final String INSTRUCTION3         = GFX_PATH + HARD + OBJECTS + "instruction3";
    public static final String HARD_LAMP            = GFX_PATH + HARD + OBJECTS + "lamp";
    public static final String PACK1                = GFX_PATH + HARD + OBJECTS + "pack1";
    public static final String PACK2                = GFX_PATH + HARD + OBJECTS + "pack2";
    public static final String PACK3                = GFX_PATH + HARD + OBJECTS + "pack3";
    public static final String TUBUS1               = GFX_PATH + HARD + OBJECTS + "tubus1";
    public static final String TUBUS2               = GFX_PATH + HARD + OBJECTS + "tubus2";
    public static final String TUBUS3               = GFX_PATH + HARD + OBJECTS + "tubus3";

    //======= BONUS OBJECTS ==============
    public static final String BELL                 = GFX_PATH + BONUS + OBJECTS + "bell";
    public static final String BOOK                 = GFX_PATH + BONUS + OBJECTS + "book";
    public static final String BROKEN_PICTURE       = GFX_PATH + BONUS + OBJECTS + "broken_picture";
    public static final String HELMET               = GFX_PATH + BONUS + OBJECTS + "helmet";
    public static final String MEDALLION            = GFX_PATH + BONUS + OBJECTS + "medallion";
    public static final String MASK                 = GFX_PATH + BONUS + OBJECTS + "mask";
    public static final String BONUS_PICTURE1       = GFX_PATH + BONUS + OBJECTS + "picture1";
    public static final String BONUS_PICTURE        = GFX_PATH + BONUS + OBJECTS + "picture";
    public static final String BONUS_PICTURE2       = GFX_PATH + BONUS + OBJECTS + "picture2";
    public static final String BONUS_PICTURE3       = GFX_PATH + BONUS + OBJECTS + "picture3";
    public static final String BONUS_PICTURE4       = GFX_PATH + BONUS + OBJECTS + "picture4";
    public static final String BONUS_PICTURE5       = GFX_PATH + BONUS + OBJECTS + "picture5";
    public static final String BONUS_PICTURE6       = GFX_PATH + BONUS + OBJECTS + "picture6";
    public static final String BONUS_PICTURE7       = GFX_PATH + BONUS + OBJECTS + "picture7";
    public static final String BONUS_PICTURE8       = GFX_PATH + BONUS + OBJECTS + "picture8";
    public static final String BONUS_PICTURE9       = GFX_PATH + BONUS + OBJECTS + "picture9";
    public static final String BONUS_PICTURE10      = GFX_PATH + BONUS + OBJECTS + "picture10";
    public static final String BONUS_PLATE1         = GFX_PATH + BONUS + OBJECTS + "plate1";
    public static final String BONUS_PLATE2         = GFX_PATH + BONUS + OBJECTS + "plate2";
    public static final String BONUS_PLATE3         = GFX_PATH + BONUS + OBJECTS + "plate3";
    public static final String STATUE               = GFX_PATH + BONUS + OBJECTS + "statue";
    public static final String TUBE                 = GFX_PATH + BONUS + OBJECTS + "tube";
    public static final String VASE1                = GFX_PATH + BONUS + OBJECTS + "vase1";
    public static final String VASE2                = GFX_PATH + BONUS + OBJECTS + "vase2";
    public static final String VASE3                = GFX_PATH + BONUS + OBJECTS + "vase3";
    public static final String VASE4                = GFX_PATH + BONUS + OBJECTS + "vase4";
    public static final String VASE5                = GFX_PATH + BONUS + OBJECTS + "vase5";
    public static final String VASE6                = GFX_PATH + BONUS + OBJECTS + "vase6";
    public static final String VASE7                = GFX_PATH + BONUS + OBJECTS + "vase7";
    public static final String VASE8                = GFX_PATH + BONUS + OBJECTS + "vase8";

    //======= MEDIUM OBJECTS CARDS ==============
    public static final String BOX1_CARD            = GFX_PATH + MEDIUM + OBJECT_CARDS + "box1";
    public static final String BOX2_CARD            = GFX_PATH + MEDIUM + OBJECT_CARDS + "box2";
    public static final String BOX3_CARD            = GFX_PATH + MEDIUM + OBJECT_CARDS + "box3";
    public static final String FRAME_CARD           = GFX_PATH + MEDIUM + OBJECT_CARDS + "frame";
    public static final String FRAME2_CARD          = GFX_PATH + MEDIUM + OBJECT_CARDS + "frame2";
    public static final String FRAME3_CARD          = GFX_PATH + MEDIUM + OBJECT_CARDS + "frame3";
    public static final String FRAME4_CARD          = GFX_PATH + MEDIUM + OBJECT_CARDS + "frame4";
    public static final String MED_CAN1_CARD        = GFX_PATH + MEDIUM + OBJECT_CARDS + "can1";
    public static final String MED_CAN2_CARD        = GFX_PATH + MEDIUM + OBJECT_CARDS + "can2";
    public static final String MED_CLOCK_CARD       = GFX_PATH + MEDIUM + OBJECT_CARDS + "clock";
    public static final String DUCK_TAPE_CARD       = GFX_PATH + MEDIUM + OBJECT_CARDS + "duck_tape";
    public static final String LAMP_CARD            = GFX_PATH + MEDIUM + OBJECT_CARDS + "lamp";
    public static final String LETTER1_CARD         = GFX_PATH + MEDIUM + OBJECT_CARDS + "letter1";
    public static final String LETTER2_CARD         = GFX_PATH + MEDIUM + OBJECT_CARDS + "letter2";
    public static final String LETTER3_CARD         = GFX_PATH + MEDIUM + OBJECT_CARDS + "letter3";
    public static final String PAPERS_CARD          = GFX_PATH + MEDIUM + OBJECT_CARDS + "papers";
    public static final String MED_PICTURE1_CARD    = GFX_PATH + MEDIUM + OBJECT_CARDS + "picture1";
    public static final String MED_PICTURE2_CARD    = GFX_PATH + MEDIUM + OBJECT_CARDS + "picture2";
    public static final String PICTURE_LETTER_CARD  = GFX_PATH + MEDIUM + OBJECT_CARDS + "picture_letter";
    public static final String PILLOW_CARD          = GFX_PATH + MEDIUM + OBJECT_CARDS + "pillow";
    public static final String STAMP_CARD           = GFX_PATH + MEDIUM + OBJECT_CARDS + "stamp";
    public static final String TWINE_CARD           = GFX_PATH + MEDIUM + OBJECT_CARDS + "twine";

    //======= HARD OBJECTS CARDS ==============
    public static final String BOX_CARD             = GFX_PATH + HARD + OBJECT_CARDS + "box";
    public static final String BRICK1_CARD          = GFX_PATH + HARD + OBJECT_CARDS + "brick1";
    public static final String BRICK2_CARD          = GFX_PATH + HARD + OBJECT_CARDS + "brick2";
    public static final String BRICK3_CARD          = GFX_PATH + HARD + OBJECT_CARDS + "brick3";
    public static final String BRICK4_CARD          = GFX_PATH + HARD + OBJECT_CARDS + "brick4";
    public static final String CERTIFICATE_CARD     = GFX_PATH + HARD + OBJECT_CARDS + "certificate";
    public static final String CERTIFICATE2_CARD    = GFX_PATH + HARD + OBJECT_CARDS + "certificate2";
    public static final String CERTIFICATE3_CARD    = GFX_PATH + HARD + OBJECT_CARDS + "certificate3";
    public static final String COINS1_CARD          = GFX_PATH + HARD + OBJECT_CARDS + "coins1";
    public static final String COINS2_CARD          = GFX_PATH + HARD + OBJECT_CARDS + "coins2";
    public static final String CHAQUES_CARD         = GFX_PATH + HARD + OBJECT_CARDS + "chaques";
    public static final String INSTRUCTION_CARD     = GFX_PATH + HARD + OBJECT_CARDS + "instruction";
    public static final String INSTRUCTION2_CARD    = GFX_PATH + HARD + OBJECT_CARDS + "instruction2";
    public static final String INSTRUCTION3_CARD    = GFX_PATH + HARD + OBJECT_CARDS + "instruction3";
    public static final String HARD_LAMP_CARD       = GFX_PATH + HARD + OBJECT_CARDS + "lamp";
    public static final String PACK1_CARD           = GFX_PATH + HARD + OBJECT_CARDS + "pack1";
    public static final String PACK2_CARD           = GFX_PATH + HARD + OBJECT_CARDS + "pack2";
    public static final String PACK3_CARD           = GFX_PATH + HARD + OBJECT_CARDS + "pack3";
    public static final String TUBUS1_CARD          = GFX_PATH + HARD + OBJECT_CARDS + "tubus1";
    public static final String TUBUS2_CARD          = GFX_PATH + HARD + OBJECT_CARDS + "tubus2";
    public static final String TUBUS3_CARD          = GFX_PATH + HARD + OBJECT_CARDS + "tubus3";

    //======= BONUS OBJECTS CARDS ==============
    public static final String BELL_CARD            = GFX_PATH + BONUS + OBJECT_CARDS + "bell";
    public static final String BOOK_CARD            = GFX_PATH + BONUS + OBJECT_CARDS + "book";
    public static final String BROKEN_PICTURE_CARD  = GFX_PATH + BONUS + OBJECT_CARDS + "broken_picture";
    public static final String HELMET_CARD          = GFX_PATH + BONUS + OBJECT_CARDS + "helmet";
    public static final String MEDALLION_CARD       = GFX_PATH + BONUS + OBJECT_CARDS + "medallion";
    public static final String MASK_CARD            = GFX_PATH + BONUS + OBJECT_CARDS + "mask";
    public static final String BONUS_PICTURE_CARD   = GFX_PATH + BONUS + OBJECT_CARDS + "picture";
    public static final String BONUS_PICTURE1_CARD  = GFX_PATH + BONUS + OBJECT_CARDS + "picture1";
    public static final String BONUS_PICTURE2_CARD  = GFX_PATH + BONUS + OBJECT_CARDS + "picture2";
    public static final String BONUS_PICTURE3_CARD  = GFX_PATH + BONUS + OBJECT_CARDS + "picture3";
    public static final String BONUS_PICTURE4_CARD  = GFX_PATH + BONUS + OBJECT_CARDS + "picture4";
    public static final String BONUS_PICTURE5_CARD  = GFX_PATH + BONUS + OBJECT_CARDS + "picture5";
    public static final String BONUS_PICTURE6_CARD  = GFX_PATH + BONUS + OBJECT_CARDS + "picture6";
    public static final String BONUS_PICTURE7_CARD  = GFX_PATH + BONUS + OBJECT_CARDS + "picture7";
    public static final String BONUS_PICTURE8_CARD  = GFX_PATH + BONUS + OBJECT_CARDS + "picture8";
    public static final String BONUS_PICTURE9_CARD  = GFX_PATH + BONUS + OBJECT_CARDS + "picture9";
    public static final String BONUS_PICTURE10_CARD = GFX_PATH + BONUS + OBJECT_CARDS + "picture10";
    public static final String BONUS_PLATE1_CARD    = GFX_PATH + BONUS + OBJECT_CARDS + "plate1";
    public static final String BONUS_PLATE2_CARD    = GFX_PATH + BONUS + OBJECT_CARDS + "plate2";
    public static final String BONUS_PLATE3_CARD    = GFX_PATH + BONUS + OBJECT_CARDS + "plate3";
    public static final String STATUE_CARD          = GFX_PATH + BONUS + OBJECT_CARDS + "statue";
    public static final String TUBE_CARD            = GFX_PATH + BONUS + OBJECT_CARDS + "tube";
    public static final String VASE1_CARD           = GFX_PATH + BONUS + OBJECT_CARDS + "vase1";
    public static final String VASE2_CARD           = GFX_PATH + BONUS + OBJECT_CARDS + "vase2";
    public static final String VASE3_CARD           = GFX_PATH + BONUS + OBJECT_CARDS + "vase3";
    public static final String VASE4_CARD           = GFX_PATH + BONUS + OBJECT_CARDS + "vase4";
    public static final String VASE5_CARD           = GFX_PATH + BONUS + OBJECT_CARDS + "vase5";
    public static final String VASE6_CARD           = GFX_PATH + BONUS + OBJECT_CARDS + "vase6";
    public static final String VASE7_CARD           = GFX_PATH + BONUS + OBJECT_CARDS + "vase7";
    public static final String VASE8_CARD           = GFX_PATH + BONUS + OBJECT_CARDS + "vase8";

    //======= CARDS ==============
    public static final String ZERO                 = GFX_PATH + CARDS + "0";
    public static final String ONE                  = GFX_PATH + CARDS + "1";
    public static final String TWO                  = GFX_PATH + CARDS + "2";
    public static final String THREE                = GFX_PATH + CARDS + "3";
    public static final String FOUR                 = GFX_PATH + CARDS + "4";
    public static final String FIVE                 = GFX_PATH + CARDS + "5";
    public static final String SIX                  = GFX_PATH + CARDS + "6";
    public static final String SEVEN                = GFX_PATH + CARDS + "7";
    public static final String EIGHT                = GFX_PATH + CARDS + "8";
    public static final String NINE                 = GFX_PATH + CARDS + "9";
    public static final String CARD                 = GFX_PATH + CARDS + "card";
    public static final String CARD1                = GFX_PATH + CARDS + "card1";

    //======= TEXT ==============
    public static final String DONE_TEXT             = "memory_complete";
    public static final String SCORE_0_TEXT          = "game_memory_score_0";
    public static final String SCORE_1_TEXT          = "game_memory_score_1";
    public static final String SCORE_2_TEXT          = "game_memory_score_2";
    public static final String SCORE_3_TEXT          = "game_memory_score_3";
    public static final String RESULT_SCORE_TEXT     = "result_score_count";

    //======= SOUNDS ==============
    public static final String BEFORE_PHOTO_SOUND   = COMMON + SFX_PATH + "before_photo.mp3";
    public static final String OBJECT_BACK_SOUND    = COMMON + SFX_PATH + "object_back.mp3";
    public static final String OBJECT_BACK2_SOUND   = COMMON + SFX_PATH + "object_back2.mp3";
    public static final String OUT_OF_POS_SOUND     = COMMON + SFX_PATH + "object_out_of_position.mp3";
    public static final String PUT_FIX_SOUND        = COMMON + SFX_PATH + "put_fix.mp3";

    public static final String SCORE_0_SOUND		= COMMON + SFX_PATH + "result_0.mp3";
    public static final String SCORE_1_SOUND		= COMMON + SFX_PATH + "result_1.mp3";
    public static final String SCORE_2_SOUND		= COMMON + SFX_PATH + "result_2.mp3";
    public static final String SCORE_3_SOUND		= COMMON + SFX_PATH + "result_3.mp3";
}
