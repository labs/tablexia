/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.memory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.memory.actors.AbstractObjectPositioner;
import cz.nic.tablexia.game.games.memory.assets.MemoryAssets;
import cz.nic.tablexia.game.games.memory.game_objects.MemoryImage;
import cz.nic.tablexia.game.games.memory.model.MemoryGameDifficulty;
import cz.nic.tablexia.game.games.memory.model.MemoryGameResultStars;
import cz.nic.tablexia.game.games.memory.model.BackgroundGroup;
import cz.nic.tablexia.game.games.memory.utils.DoneButtonClickListener;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.resolvers.MemoryGameScoreResolver;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.button.ImageTablexiaButton;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

import static cz.nic.tablexia.shared.model.resolvers.MemoryGameScoreResolver.SCORE_COUNT;

public class MemoryGame extends AbstractTablexiaGame<Void> {
    public static final String SCENARIO_STEP_PHOTO_SHOWN = "photo shown";

    private static final float  FADE_IN_OUT_DURATION    = 0.3f;
    private static final float  BLACK_DIMMER_DELAY      = 1f;
    private static final int    DONE_BUTTON_WIDTH       = 150;
    private static final int    DONE_BUTTON_HEIGHT      = 72;
    private static final int    DONE_BUTTON_OFFSET_X    = 200;
    private static final int    DONE_BUTTON_OFFSET_Y    = 10;
    private static final int    EVALUATION_DURATION     = 5;

    private Image frame;
    private MemoryImage roomImage;
    private Image dimmer;
    private BackgroundGroup backgroundGroup;
    private AbstractObjectPositioner objectsPositioner;
    private ImageTablexiaButton doneButton;
    private int mistakes;
    private int selectedObjects;

    public ImageTablexiaButton getDoneButton() {
        return doneButton;
    }

    public int getMistakes() {
        return mistakes;
    }

    public void setMistakes(int mistakes) {
        this.mistakes = mistakes;
    }

    public int getSelectedObjects() {
        return selectedObjects;
    }

    public void setSelectedObjects(int selectedObjects) {
        this.selectedObjects = selectedObjects;
    }

    public AbstractObjectPositioner getObjectsPositioner() {
        return objectsPositioner;
    }

/////////////////////////////// SCREEN LIFECYCLE

    @Override
    protected void gameLoaded(Map<String, String> gameState) {
        mistakes = 0;
        selectedObjects = 0;
        prepareBackground();
        prepareObjects();
        backgroundGroup.enableShader();
        addFrame();
        addDoneBtn();
        createDimmer();
    }

    private void prepareBackground() {
        backgroundGroup = new BackgroundGroup();
        backgroundGroup.setDebug(true);
        getStage().addActor(backgroundGroup);

        roomImage = new MemoryImage(getScreenTextureRegion(getRoomImageForGameDifficulty(getGameDifficulty())));
        roomImage.setSize(getSceneWidth(), getSceneOuterHeight());
        roomImage.setPosition(getSceneLeftX(), getSceneOuterBottomY());
        backgroundGroup.addActor(roomImage);
    }

    private String getRoomImageForGameDifficulty(GameDifficulty gameDifficulty) {
        switch (gameDifficulty){
            case EASY: return MemoryAssets.MEMORY_ROOM_EASY;
            case MEDIUM: return MemoryAssets.MEMORY_ROOM_MEDIUM;
            case HARD: return MemoryAssets.MEMORY_ROOM_HARD;
            case BONUS: return MemoryAssets.MEMORY_ROOM_BONUS;
            default: return MemoryAssets.MEMORY_ROOM_MEDIUM;
        }
    }

    private void prepareObjects() {
        backgroundGroup.addActor(objectsPositioner = MemoryGameDifficulty.getMemoryGameDifficultyForGameDifficulty(getGameDifficulty()).getObjectPositioner());
        objectsPositioner.prepare(this);
        backgroundGroup.addActor(objectsPositioner.addOtherButtonsAndWidgets(this));
        backgroundGroup.addActor(objectsPositioner.getOtherObjectGroup());
        backgroundGroup.addActor(objectsPositioner.getSeletedObjectGroup());
        backgroundGroup.addActor(objectsPositioner.getConfusingObjectGroup());
        backgroundGroup.addActor(objectsPositioner.getGroupOfCards());
    }

    private void addDoneBtn() {
        doneButton = new ImageTablexiaButton(
                getText(MemoryAssets.DONE_TEXT),
                new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_YES_ICON))
        );
        doneButton.setButtonSize(DONE_BUTTON_WIDTH, DONE_BUTTON_HEIGHT)
                .setButtonPosition(getViewportWidth() - DONE_BUTTON_OFFSET_X, getSceneInnerBottomY() + DONE_BUTTON_OFFSET_Y)
                .setInputListener(new DoneButtonClickListener(this));
        doneButton.setDisabled();
        doneButton.setVisible(false);
    }

    private void addFrame() {
        frame = new Image(getScreenTextureRegion(MemoryAssets.PHOTO_FRAME));
        frame.setBounds(getViewportLeftX(), getViewportBottomY(), getViewportWidth(), getViewportHeight());
        getStage().addActor(frame);
    }

    private void createDimmer() {
        dimmer = new Image(ApplicationAtlasManager.getInstance().getColorTexture(Color.BLACK));
        dimmer.setPosition(getViewportLeftX(), getSceneOuterBottomY());
        dimmer.setSize(getViewportWidth(), getSceneOuterHeight());
        getStage().addActor(dimmer);
    }

    public void enableDoneButton() {
        doneButton.setEnabled();
    }

    public void disableDoneButton() {
        doneButton.setDisabled();
    }

    @Override
    protected void gameVisible() {
        waitAndHideScene();
    }

    @Override
    protected void screenResized(int width, int height) {
        if(frame!=null) frame.setBounds(getViewportLeftX(), getViewportBottomY(), getViewportWidth(), getViewportHeight());
    }

    private void waitAndHideScene() {
        MemoryGame memoryGame = this;
        SequenceAction sa = Actions.sequence();
        sa.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                dimmer.addAction(Actions.sequence(Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        getMusic(MemoryAssets.BEFORE_PHOTO_SOUND).play();
                    }
                }), Actions.delay(BLACK_DIMMER_DELAY),Actions.fadeOut(FADE_IN_OUT_DURATION)));
            }
        }));
        sa.addAction(Actions.delay(MemoryGameDifficulty.getMemoryGameDifficultyForGameDifficulty(getGameDifficulty()).getPhotoDelay()));
        sa.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                dimmer.addAction(Actions.sequence(Actions.fadeIn(FADE_IN_OUT_DURATION), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        frame.remove();
                        backgroundGroup.disableShader();
                        backgroundGroup.addActor(doneButton);
                        doneButton.setVisible(true);
                        objectsPositioner.prepareObjectsForPlayer(memoryGame);
                    }
                }), Actions.delay(BLACK_DIMMER_DELAY),Actions.fadeOut(FADE_IN_OUT_DURATION), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        dimmer.remove();
                        AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_STEP_PHOTO_SHOWN);
                    }
                })));
            }
        }));
        this.addAction(sa);
    }

    public void evaluate() {
        doneButton.setDisabled();
        objectsPositioner.evaluate(this);
        setGameScore(SCORE_COUNT, mistakes);
        SequenceAction sa = Actions.sequence();
        sa.addAction(Actions.delay(EVALUATION_DURATION));
        this.addAction(sa);
        sa.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                gameComplete();
            }
        }));
    }

    @Override
    protected void gameDisposed() {
        backgroundGroup.disposeShader();
    }


    @Override
    protected void gameEnd() {
        getStage().clear();
        super.gameEnd();
    }

//////////////////////////// GAME RESULT

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(new SummaryMessage(SummaryImage.STATS, getFormattedText(MemoryAssets.RESULT_SCORE_TEXT, game.getGameScore(MemoryGameScoreResolver.SCORE_COUNT, "0"))));
    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        return MemoryGameResultStars.getTextForGameResult(gameResult);
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        return MemoryGameResultStars.getSoundForGameResult(gameResult);
    }

//////////////////////////// PRELOADER

    private static final String     PRELOADER_ANIM_IMAGE                = "preloader_anim";
    private static final float      PRELOADER_ANIM_FRAME_DURATION       = 0.5f;
    private static final String     PRELOADER_TEXT_EASY_KEY             = ApplicationTextManager.ApplicationTextsAssets.MEMORY_GAME_PRELOADER_EASY_TEXT;
    private static final String     PRELOADER_TEXT_KEY                  = ApplicationTextManager.ApplicationTextsAssets.MEMORY_GAME_PRELOADER_TEXT;
    private static final String     PRELOADER_TEXT_BONUS_KEY            = ApplicationTextManager.ApplicationTextsAssets.MEMORY_GAME_PRELOADER_BONUS_TEXT;

    private static final Scaling    PRELOADER_IMAGE_SCALING             = Scaling.fit;
    private static final int        PRELOADER_TEXT_ALIGN 			    = Align.left;
    private static final float      PRELOADER_TEXT_PADDING 			    = 10f;
    private static final float 	    PRELOADER_LEFT_COLUMN_WIDTH_RATIO 	= 1f / 4;
    private static final float 	    PRELOADER_RIGHT_COLUMN_RATIO 		= 5f / 8;
    private static final float 	    PRELOADER_ROW_HEIGHT 				= 1f / 3;
    private static final Color      PRELOADER_TEXT_COLOR                = Color.DARK_GRAY;

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        PreloaderData preloaderData = PreloaderData.getPreloaderData(getGameDifficulty());

        AnimatedImage preloaderImage = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_IMAGE + preloaderData.suffix, preloaderData.framesCnt, PRELOADER_ANIM_FRAME_DURATION), false);
        preloaderImage.startAnimationLoop();
        String preloaderText = preloaderAssetsManager.getText(preloaderData.preloaderTextPath);
        components.add(new TwoColumnContentDialogComponent(
                new AnimatedImageContentDialogComponent(preloaderImage, PRELOADER_IMAGE_SCALING),
                new TextContentDialogComponent(preloaderText, PRELOADER_TEXT_COLOR, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_ROW_HEIGHT));
    }

    @Override
    protected String preparePreloaderSpeechFileBaseName() {
        PreloaderData preloaderData = PreloaderData.getPreloaderData(getGameDifficulty());
        return super.preparePreloaderSpeechFileBaseName() + preloaderData.suffix;
    }


    private enum  PreloaderData{
        EASY            (PRELOADER_TEXT_EASY_KEY, "_e", 9),
        MEDIUM_HARD     (PRELOADER_TEXT_KEY, "_mh", 8),
        BONUS           (PRELOADER_TEXT_BONUS_KEY, "_b", 10);

        private String preloaderTextPath;
        private String suffix;
        private int framesCnt;

        PreloaderData(String preloaderTextPath, String suffix, int framesCnt){
            this.preloaderTextPath = preloaderTextPath;
            this.suffix = suffix;
            this.framesCnt = framesCnt;
        }

        private static PreloaderData getPreloaderData(GameDifficulty difficulty){
            switch (difficulty){
                case EASY: return EASY;
                case BONUS: return BONUS;
                default: return MEDIUM_HARD;
            }
        }

    }

}
