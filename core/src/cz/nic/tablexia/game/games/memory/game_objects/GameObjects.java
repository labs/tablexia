/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.game_objects;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.memory.actors.PositionPlacement;
import cz.nic.tablexia.game.games.memory.assets.MemoryAssets;

public enum GameObjects {
    //EASY
    BASKET("basket", MemoryAssets.BASKET, null, GameDifficulty.EASY, PositionPlacement.FREE, false),
    BOWL("bowl", MemoryAssets.BOWL, null, GameDifficulty.EASY, PositionPlacement.FREE, false),
    CAN("can", MemoryAssets.CAN, null, GameDifficulty.EASY, PositionPlacement.FREE, false),
    CAN2("can2", MemoryAssets.CAN2, null, GameDifficulty.EASY, PositionPlacement.FREE, false),
    CLOCK("clock", MemoryAssets.CLOCK, null, GameDifficulty.EASY, PositionPlacement.LEFT_WALL, true),
    CLOCK2("clock2", MemoryAssets.CLOCK2, null, GameDifficulty.EASY, PositionPlacement.LEFT_WALL, true),
    CLOTH("cloth", MemoryAssets.CLOTH, null, GameDifficulty.EASY, PositionPlacement.SPECIAL, true),
    CLOTH2("cloth2", MemoryAssets.CLOTH2, null, GameDifficulty.EASY, PositionPlacement.SPECIAL, true),
    CUP("cup", MemoryAssets.CUP, null, GameDifficulty.EASY, PositionPlacement.FREE, false),
    CHUNK("chunk", MemoryAssets.CHUNK, null, GameDifficulty.EASY, 720, 75, PositionPlacement.FIXED, false),
    KETTLE("kettle", MemoryAssets.KETTLE, null, GameDifficulty.EASY, PositionPlacement.FREE, false),
    LADDLE("laddle", MemoryAssets.LADDLE, null, GameDifficulty.EASY, PositionPlacement.SPECIAL, true),
    PICTURE("picture", MemoryAssets.PICTURE, null, GameDifficulty.EASY, PositionPlacement.LEFT_WALL, true),
    PICTURE2("picture2", MemoryAssets.PICTURE2, null, GameDifficulty.EASY, PositionPlacement.RIGHT_WALL, true),
    PICTURE3("picture3", MemoryAssets.PICTURE3, null, GameDifficulty.EASY, PositionPlacement.LEFT_WALL, true),
    PICTURE4("picture4", MemoryAssets.PICTURE4, null, GameDifficulty.EASY, PositionPlacement.RIGHT_WALL, true),
    PICTURE5("picture5", MemoryAssets.PICTURE5, null, GameDifficulty.EASY, PositionPlacement.LEFT_WALL, true),
    PLATE("plate", MemoryAssets.PLATE, null, GameDifficulty.EASY, PositionPlacement.FREE, false),
    PLATE2("plate2", MemoryAssets.PLATE2, null, GameDifficulty.EASY, PositionPlacement.ON_THE_SHELF, false),
    PLATE3("plate3", MemoryAssets.PLATE3, null, GameDifficulty.EASY, PositionPlacement.ON_THE_SHELF, false),
    PLATE4("plate4", MemoryAssets.PLATE4, null, GameDifficulty.EASY, PositionPlacement.ON_THE_SHELF, false),
    POT("pot", MemoryAssets.POT, null, GameDifficulty.EASY, PositionPlacement.FREE, false),
    ROLLER("roller", MemoryAssets.ROLLER, null, GameDifficulty.EASY, PositionPlacement.FREE, false),
    VASE("vase", MemoryAssets.VASE, null, GameDifficulty.EASY, PositionPlacement.FREE, false),

    //MEDIUM
    BOX1("box1", MemoryAssets.BOX1, MemoryAssets.BOX1_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    BOX2("box2", MemoryAssets.BOX2, MemoryAssets.BOX2_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    BOX3("box3", MemoryAssets.BOX3, MemoryAssets.BOX3_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    FRAME("frame", MemoryAssets.FRAME, MemoryAssets.FRAME_CARD, GameDifficulty.MEDIUM, PositionPlacement.LEFT_WALL, true),
    FRAME2("frame2", MemoryAssets.FRAME2, MemoryAssets.FRAME2_CARD, GameDifficulty.MEDIUM, PositionPlacement.LEFT_WALL, true),
    FRAME3("frame3", MemoryAssets.FRAME3, MemoryAssets.FRAME3_CARD, GameDifficulty.MEDIUM, PositionPlacement.RIGHT_WALL, true),
    FRAME4("frame4", MemoryAssets.FRAME4, MemoryAssets.FRAME4_CARD, GameDifficulty.MEDIUM, PositionPlacement.LEFT_WALL, true),
    MED_CAN1("can1", MemoryAssets.MED_CAN1, MemoryAssets.MED_CAN1_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    MED_CAN2("can2", MemoryAssets.MED_CAN2, MemoryAssets.MED_CAN2_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    MED_CLOCK("clock", MemoryAssets.MED_CLOCK, MemoryAssets.MED_CLOCK_CARD, GameDifficulty.MEDIUM, 610, 245, PositionPlacement.RIGHT_WALL, true),
    DUCK_TAPE("duck tape", MemoryAssets.DUCK_TAPE, MemoryAssets.DUCK_TAPE_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    LAMP("lamp", MemoryAssets.LAMP, MemoryAssets.LAMP_CARD, GameDifficulty.MEDIUM, 470, 235, PositionPlacement.FIXED, false),
    LETTER1("letter1", MemoryAssets.LETTER1, MemoryAssets.LETTER1_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    LETTER2("letter2", MemoryAssets.LETTER2, MemoryAssets.LETTER2_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    LETTER3("letter3", MemoryAssets.LETTER3, MemoryAssets.LETTER3_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    PAPERS("papers", MemoryAssets.PAPERS, MemoryAssets.PAPERS_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    PICTURE_LETTER("picture letter", MemoryAssets.PICTURE_LETTER, MemoryAssets.PICTURE_LETTER_CARD, GameDifficulty.MEDIUM, PositionPlacement.LEFT_WALL, false),
    MED_PICTURE1("picture1", MemoryAssets.MED_PICTURE1, MemoryAssets.MED_PICTURE1_CARD, GameDifficulty.MEDIUM, PositionPlacement.LEFT_WALL, true),
    MED_PICTURE2("picture2", MemoryAssets.MED_PICTURE2, MemoryAssets.MED_PICTURE2_CARD, GameDifficulty.MEDIUM, PositionPlacement.LEFT_WALL, true),
    PILLOW("pillow", MemoryAssets.PILLOW, MemoryAssets.PILLOW_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    STAMP("stamp", MemoryAssets.STAMP, MemoryAssets.STAMP_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),
    TWINE("twine", MemoryAssets.TWINE, MemoryAssets.TWINE_CARD, GameDifficulty.MEDIUM, PositionPlacement.FREE, false),

    //HARD
    BOX("box", MemoryAssets.BOX, MemoryAssets.BOX_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    BRICK1("brick1", MemoryAssets.BRICK1, MemoryAssets.BRICK1_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    BRICK2("brick2", MemoryAssets.BRICK2, MemoryAssets.BRICK2_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    BRICK3("brick3", MemoryAssets.BRICK3, MemoryAssets.BRICK3_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    BRICK4("brick4", MemoryAssets.BRICK4, MemoryAssets.BRICK4_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    CERTIFICATE("certificate", MemoryAssets.CERTIFICATE, MemoryAssets.CERTIFICATE_CARD, GameDifficulty.HARD, PositionPlacement.LEFT_WALL, true),
    CERTIFICATE2("certificate2", MemoryAssets.CERTIFICATE2, MemoryAssets.CERTIFICATE2_CARD, GameDifficulty.HARD, PositionPlacement.LEFT_WALL, true),
    CERTIFICATE3("certificate3", MemoryAssets.CERTIFICATE3, MemoryAssets.CERTIFICATE3_CARD, GameDifficulty.HARD, PositionPlacement.LEFT_WALL, true),
    COINS1("coins1", MemoryAssets.COINS1, MemoryAssets.COINS1_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    COINS2("coins2", MemoryAssets.COINS2, MemoryAssets.COINS2_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    CHAQUES("chaques", MemoryAssets.CHAQUES, MemoryAssets.CHAQUES_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    INSTRUCTION("instruction", MemoryAssets.INSTRUCTION, MemoryAssets.INSTRUCTION_CARD, GameDifficulty.HARD, PositionPlacement.RIGHT_WALL, true),
    INSTRUCTION2("instruction2", MemoryAssets.INSTRUCTION2, MemoryAssets.INSTRUCTION2_CARD, GameDifficulty.HARD, PositionPlacement.RIGHT_WALL, true),
    INSTRUCTION3("instruction3", MemoryAssets.INSTRUCTION3, MemoryAssets.INSTRUCTION3_CARD, GameDifficulty.HARD, PositionPlacement.RIGHT_WALL, true),
    HARD_LAMP("lamp", MemoryAssets.HARD_LAMP, MemoryAssets.HARD_LAMP_CARD, GameDifficulty.HARD, 250,215, PositionPlacement.FIXED, false),
    PACK1("pack1", MemoryAssets.PACK1, MemoryAssets.PACK1_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    PACK2("pack2", MemoryAssets.PACK2, MemoryAssets.PACK2_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    PACK3("pack3", MemoryAssets.PACK3, MemoryAssets.PACK3_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    TUBUS1("tubus1", MemoryAssets.TUBUS1, MemoryAssets.TUBUS1_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    TUBUS2("tubus2", MemoryAssets.TUBUS2, MemoryAssets.TUBUS2_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),
    TUBUS3("tubus3", MemoryAssets.TUBUS3, MemoryAssets.TUBUS3_CARD, GameDifficulty.HARD, PositionPlacement.FREE, false),

    //BONUS
    BELL("bell", MemoryAssets.BELL, MemoryAssets.BELL_CARD, GameDifficulty.BONUS, PositionPlacement.FREE, false),
    BOOK("book", MemoryAssets.BOOK, MemoryAssets.BOOK_CARD, GameDifficulty.BONUS, PositionPlacement.FREE, false),
    BROKEN_PICTURE("broken_picture", MemoryAssets.BROKEN_PICTURE, MemoryAssets.BROKEN_PICTURE_CARD,GameDifficulty.BONUS, PositionPlacement.FREE, false),
    HELMET("helmet", MemoryAssets.HELMET, MemoryAssets.HELMET_CARD, GameDifficulty.BONUS, PositionPlacement.FREE, false),
    MEDALLION("medallion", MemoryAssets.MEDALLION, MemoryAssets.MEDALLION_CARD,GameDifficulty.BONUS, PositionPlacement.FREE, false),
    MASK("mask", MemoryAssets.MASK, MemoryAssets.MASK_CARD,GameDifficulty.BONUS, PositionPlacement.LEFT_WALL, true),
    BONUS_PICTURE("picture", MemoryAssets.BONUS_PICTURE, MemoryAssets.BONUS_PICTURE_CARD, GameDifficulty.BONUS, PositionPlacement.RIGHT_WALL, true),
    BONUS_PICTURE1("picture1", MemoryAssets.BONUS_PICTURE1, MemoryAssets.BONUS_PICTURE1_CARD, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL, true),
    BONUS_PICTURE2("picture2", MemoryAssets.BONUS_PICTURE2, MemoryAssets.BONUS_PICTURE2_CARD, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL, true),
    BONUS_PICTURE3("picture3", MemoryAssets.BONUS_PICTURE3, MemoryAssets.BONUS_PICTURE3_CARD, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL, true),
    BONUS_PICTURE4("picture4", MemoryAssets.BONUS_PICTURE4, MemoryAssets.BONUS_PICTURE4_CARD, GameDifficulty.BONUS, PositionPlacement.RIGHT_WALL, true),
    BONUS_PICTURE5("picture5", MemoryAssets.BONUS_PICTURE5, MemoryAssets.BONUS_PICTURE5_CARD, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL, true),
    BONUS_PICTURE6("picture6", MemoryAssets.BONUS_PICTURE6, MemoryAssets.BONUS_PICTURE6_CARD, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL, true),
    BONUS_PICTURE7("picture7", MemoryAssets.BONUS_PICTURE7, MemoryAssets.BONUS_PICTURE7_CARD, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL, true),
    BONUS_PICTURE8("picture8", MemoryAssets.BONUS_PICTURE8, MemoryAssets.BONUS_PICTURE8_CARD, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL, true),
    BONUS_PICTURE9("picture9", MemoryAssets.BONUS_PICTURE9, MemoryAssets.BONUS_PICTURE9_CARD, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL, true),
    BONUS_PICTURE10("picture10", MemoryAssets.BONUS_PICTURE10, MemoryAssets.BONUS_PICTURE10_CARD, GameDifficulty.BONUS, PositionPlacement.LEFT_WALL, true),
    BONUS_PLATE1("plate1", MemoryAssets.BONUS_PLATE1, MemoryAssets.BONUS_PLATE1_CARD, GameDifficulty.BONUS, PositionPlacement.FREE, false),
    BONUS_PLATE2("plate2", MemoryAssets.BONUS_PLATE2, MemoryAssets.BONUS_PLATE2_CARD, GameDifficulty.BONUS, PositionPlacement.FREE, false),
    BONUS_PLATE3("plate3", MemoryAssets.BONUS_PLATE3, MemoryAssets.BONUS_PLATE3_CARD, GameDifficulty.BONUS, PositionPlacement.RIGHT_WALL, true),
    STATUE("statue", MemoryAssets.STATUE, MemoryAssets.STATUE_CARD, GameDifficulty.BONUS, PositionPlacement.FREE, false),
    TUBE("tube", MemoryAssets.TUBE, MemoryAssets.TUBE_CARD, GameDifficulty.BONUS, PositionPlacement.BIG_OBJECT, false),
    VASE1("vase1", MemoryAssets.VASE1, MemoryAssets.VASE1_CARD, GameDifficulty.BONUS, PositionPlacement.BIG_OBJECT, false),
    VASE2("vase2", MemoryAssets.VASE2, MemoryAssets.VASE2_CARD, GameDifficulty.BONUS, PositionPlacement.FREE, false),
    VASE3("vase3", MemoryAssets.VASE3, MemoryAssets.VASE3_CARD, GameDifficulty.BONUS, PositionPlacement.FREE, false),
    VASE4("vase4", MemoryAssets.VASE4, MemoryAssets.VASE4_CARD, GameDifficulty.BONUS, PositionPlacement.BIG_OBJECT, false),
    VASE5("vase5", MemoryAssets.VASE5, MemoryAssets.VASE5_CARD, GameDifficulty.BONUS, PositionPlacement.BIG_OBJECT, false),
    VASE6("vase6", MemoryAssets.VASE6, MemoryAssets.VASE6_CARD, GameDifficulty.BONUS, PositionPlacement.BIG_OBJECT, false),
    VASE7("vase7", MemoryAssets.VASE7, MemoryAssets.VASE7_CARD, GameDifficulty.BONUS, PositionPlacement.BIG_OBJECT, false),
    VASE8("vase8", MemoryAssets.VASE8, MemoryAssets.VASE8_CARD, GameDifficulty.BONUS, PositionPlacement.BIG_OBJECT, false);

    private String objectName;
    private String assetPath;
    private String objectCardAssetPath;
    private GameDifficulty gameDifficulty;

    //if game object position isn't fixed, there should be x=-1, y=-1
    private float fixedPositionX;
    private float fixedPositionY;

    private PositionPlacement placement;
    private boolean inTheAir;

    GameObjects(String objectName, String assetPath, String objectCardAssetPath, GameDifficulty gameDifficulty, float fixedPositionX, float fixedPositionY, PositionPlacement placement, boolean inTheAir) {
        this.objectName = objectName;
        this.assetPath = assetPath;
        this.objectCardAssetPath = objectCardAssetPath;
        this.gameDifficulty = gameDifficulty;
        this.fixedPositionX = fixedPositionX;
        this.fixedPositionY = fixedPositionY;
        this.placement = placement;
        this.inTheAir = inTheAir;
    }

    GameObjects(String objectName, String assetPath, String objectCardAssetPath, GameDifficulty gameDifficulty, PositionPlacement placement, boolean inTheAir) {
        this.objectName = objectName;
        this.assetPath = assetPath;
        this.objectCardAssetPath = objectCardAssetPath;
        this.gameDifficulty = gameDifficulty;
        this.fixedPositionX = -1;
        this.fixedPositionY = -1;
        this.placement = placement;
        this.inTheAir = inTheAir;
    }

    public static List<GameObjects> getGameObjecsForGameDifficulty(GameDifficulty gameDifficulty){
        List<GameObjects> gameObjectsList = new ArrayList<GameObjects>();
        for(GameObjects gameObject: GameObjects.values()){
            if(gameObject.gameDifficulty == gameDifficulty) gameObjectsList.add(gameObject);
        }
        return gameObjectsList;
    }

    public String getObjectName() {
        return objectName;
    }

    public String getAssetPath() {
        return assetPath;
    }

    public String getObjectCardAssetPath() {
        return objectCardAssetPath;
    }

    public float getFixedPositionX() {
        return fixedPositionX;
    }

    public float getFixedPositionY() {
        return fixedPositionY;
    }

    public PositionPlacement getPlacement() {
        return placement;
    }

    public boolean isInTheAir() {
        return inTheAir;
    }

    public int getMaximumOfObjectPlacements(GameDifficulty gameDifficulty) {
        if(this.getPlacement()==null) return 0;
        return PositionPlacement.getMaximum(this.getPlacement(), gameDifficulty);
    }
}
