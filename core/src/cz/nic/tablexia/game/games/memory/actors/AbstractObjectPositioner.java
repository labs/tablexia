/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.memory.game_objects.CardNumbers;
import cz.nic.tablexia.game.games.memory.game_objects.GameObject;
import cz.nic.tablexia.game.games.memory.game_objects.GameObjects;
import cz.nic.tablexia.game.games.memory.game_objects.MemoryImage;
import cz.nic.tablexia.game.games.memory.game_objects.NumberCardGroup;
import cz.nic.tablexia.game.games.memory.model.MemoryGameDifficulty;
import cz.nic.tablexia.util.Log;


public abstract class AbstractObjectPositioner extends Group {
    protected Group otherObjectGroup;
    protected Group seletedObjectGroup;
    protected Group confusingObjectGroup;
    protected Group groupOfCards;
    protected GameObjects[] shuffledOtherObjects;
    protected GameObjects[] shuffledSelectedObjects;
    protected GameObject[] shuffledObjectCards;
    protected int numberOfObjectsOnScene;
    protected int numberOfSelectedObjects;
    protected int numberOfConfusingObjects;
    protected List<NumberCardGroup> numberCards;
    protected List<MemoryImage> fixedGameObjects;
    protected List<GameObject> movableGameObjects;
    protected List<Positions> listOfFreePositions;
    protected List<Positions> listOfLeftWallPositions;
    protected List<Positions> listOfRightWallPositions;
    protected List<Positions> listOfBigObjectsPositions;
    protected List<Positions> listOfSpecialObjectsPositions;
    protected List<Positions> listOfOnTheShelfObjectsPositions;
    protected List<GameObject> listOfObjectCards;

    public List<NumberCardGroup> getNumberCards() {
        return numberCards;
    }

    public Group getOtherObjectGroup() {
        return otherObjectGroup;
    }

    public Group getSeletedObjectGroup() {
        return seletedObjectGroup;
    }

    public Group getConfusingObjectGroup() {
        return confusingObjectGroup;
    }

    public Group getGroupOfCards() {
        return groupOfCards;
    }

    public List<GameObject> getMovableGameObjects() {
        return movableGameObjects;
    }

    public List<MemoryImage> getFixedGameObjects() {
        return fixedGameObjects;
    }

    public abstract void prepare(MemoryGame memoryGame);

    protected abstract void shuffleAndSelectObjects(MemoryGame memoryGame);

    protected void initGroupsAndLists(MemoryGame memoryGame){
        numberOfObjectsOnScene = getNumberOfObjects(memoryGame.getGameDifficulty());
        numberOfSelectedObjects = getNumberOfSelectedObjects(memoryGame.getGameDifficulty());
        numberOfConfusingObjects = getNumberOfConfusingObjects(memoryGame.getGameDifficulty());
        otherObjectGroup = new Group();
        seletedObjectGroup = new Group();
        confusingObjectGroup = new Group();
        groupOfCards = new Group();
        fixedGameObjects = new ArrayList<MemoryImage>();
        movableGameObjects = new ArrayList<GameObject>();
        listOfFreePositions = Positions.getListOfShuffledPositions(memoryGame.getGameDifficulty(), PositionPlacement.FREE);
        listOfLeftWallPositions = Positions.getListOfShuffledPositions(memoryGame.getGameDifficulty(), PositionPlacement.LEFT_WALL);
        listOfRightWallPositions = Positions.getListOfShuffledPositions(memoryGame.getGameDifficulty(), PositionPlacement.RIGHT_WALL);
        listOfBigObjectsPositions = Positions.getListOfShuffledPositions(memoryGame.getGameDifficulty(), PositionPlacement.BIG_OBJECT);
        listOfSpecialObjectsPositions = Positions.getListOfShuffledPositions(memoryGame.getGameDifficulty(), PositionPlacement.SPECIAL);
        listOfOnTheShelfObjectsPositions = Positions.getListOfShuffledPositions(memoryGame.getGameDifficulty(), PositionPlacement.ON_THE_SHELF);
        listOfObjectCards = new ArrayList<GameObject>();
    }

    protected List<GameObjects> schuffleAndSelectOtherAndSelected(GameDifficulty gameDifficulty, MemoryGame memoryGame) {
        TablexiaRandom random = memoryGame.getRandom();

        List<GameObjects> listOfObjects = GameObjects.getGameObjecsForGameDifficulty(gameDifficulty);
        List<GameObjects> listOfRemovedObjects = new ArrayList<GameObjects>();

        //not selected objects
        shuffledOtherObjects = new GameObjects[numberOfObjectsOnScene - numberOfSelectedObjects];
        for (int i = 0; i < (numberOfObjectsOnScene - numberOfSelectedObjects); i++) {
            GameObjects randomObject = listOfObjects.get(random.nextInt(listOfObjects.size()));
            if(randomObject==null){
                Log.err(getClass(), "Random object is null!");
                i--;
                continue;
            }
            if(!randomObject.getPlacement().equals(PositionPlacement.FIXED) && !randomObject.getPlacement().equals(PositionPlacement.FREE)) {
                if (containsMaxObjectPosition(shuffledOtherObjects, randomObject.getPlacement()) >= randomObject.getMaximumOfObjectPlacements(gameDifficulty)) {
                    listOfRemovedObjects.add(randomObject);
                    i--;
                }
                else {
                    createOtherObject(randomObject, memoryGame);
                    shuffledOtherObjects[i] = randomObject;
                }
            }
            else{
                createOtherObject(randomObject, memoryGame);
                shuffledOtherObjects[i] = randomObject;
            }
            listOfObjects.remove(randomObject);
        }

        //selected objects
        int cardNumber = 0;
        numberCards = new ArrayList<NumberCardGroup>();
        shuffledSelectedObjects = new GameObjects[numberOfSelectedObjects];
        for (int i=0; i < numberOfSelectedObjects; i++) {
            GameObjects randomObject = listOfObjects.get(random.nextInt(listOfObjects.size()));
            if(randomObject==null){
                Log.err(getClass(), "Random object is null!");
                i--;
                continue;
            }
            if(!randomObject.getPlacement().equals(PositionPlacement.FIXED) && !randomObject.getPlacement().equals(PositionPlacement.FREE)) {
                if ((containsMaxObjectPosition(shuffledOtherObjects, randomObject.getPlacement()) + containsMaxObjectPosition(shuffledSelectedObjects, randomObject.getPlacement())) >= randomObject.getMaximumOfObjectPlacements(gameDifficulty)) {
                    listOfRemovedObjects.add(randomObject);
                    i--;
                }
                else{
                    createSelectedObject(randomObject, memoryGame, listOfObjectCards, cardNumber);
                    cardNumber++;
                    shuffledSelectedObjects[i] = randomObject;
                }
            }
            else{
                createSelectedObject(randomObject, memoryGame, listOfObjectCards, cardNumber);
                cardNumber++;
                shuffledSelectedObjects[i] = randomObject;
            }
            listOfObjects.remove(randomObject);
        }

        for(GameObjects gameObjects: listOfRemovedObjects){
            listOfObjects.add(gameObjects);
        }

        return listOfObjects;
    }

    private void createSelectedObject(GameObjects selectedGameObject, MemoryGame memoryGame, List<GameObject> listOfObjectCards, int cardNumber){
        float x = getObjectX(selectedGameObject);
        float y = getObjectY(selectedGameObject);
        removePositionIfNeeded(selectedGameObject);

        GameObject gameObject = new GameObject(memoryGame, selectedGameObject, memoryGame.getSceneLeftX(), memoryGame.getSceneInnerBottomY(), cardNumber+1, x, y);
        gameObject.setWidthAndHeight(gameObject.getWidth(), gameObject.getHeight());
        gameObject.setPosition(gameObject.getGameObjectX(), gameObject.getGameObjectY());
        gameObject.setName(selectedGameObject.getObjectName());
        seletedObjectGroup.addActor(gameObject);
        NumberCardGroup numberCard = new NumberCardGroup(memoryGame);
        numberCard.createNumberCard(CardNumbers.findAssetById(cardNumber+1), cardNumber+1, gameObject, selectedGameObject);
        numberCards.add(numberCard);
        groupOfCards.addActor(numberCard);
        listOfObjectCards.add(gameObject);
        movableGameObjects.add(gameObject);
    }

    private void createOtherObject(GameObjects gameObject, MemoryGame memoryGame) {
        float x = getObjectX(gameObject);
        float y = getObjectY(gameObject);
        removePositionIfNeeded(gameObject);

        MemoryImage imageGameObject = new MemoryImage(memoryGame.getScreenTextureRegion(gameObject.getAssetPath()));
        imageGameObject.setPosition(memoryGame.getSceneLeftX() + x, memoryGame.getSceneInnerBottomY() + y);
        if (TablexiaSettings.getInstance().isUseHdAssets()) imageGameObject.setSize(imageGameObject.getWidth()/2, imageGameObject.getHeight()/2);
        imageGameObject.setName(gameObject.getObjectName());
        otherObjectGroup.addActor(imageGameObject);
        fixedGameObjects.add(imageGameObject);
    }

    protected int containsMaxObjectPosition(GameObjects[] shuffledObjects, PositionPlacement objectPlacement){
        int actual = 0;
        for(GameObjects gameObjects: shuffledObjects){
            if(gameObjects==null){
                return actual;
            }
            else {
                if (gameObjects.getPlacement().equals(objectPlacement)) {
                    actual++;
                }
            }
        }
        return actual;
    }

    protected float getObjectX(GameObjects gameObject){
        if(gameObject==null) Log.err(getClass(), "Game object is null!");

        if(gameObject.getPlacement().equals(PositionPlacement.FIXED)) return gameObject.getFixedPositionX();

        if(gameObject.getPlacement().equals(PositionPlacement.LEFT_WALL)){
            if(listOfLeftWallPositions.size() <= 0) Log.err(getClass(), "There is not enought left wall positions! Still remains some objects without position.");
            return listOfLeftWallPositions.get(0).getX();
        }

        if(gameObject.getPlacement().equals(PositionPlacement.RIGHT_WALL)){
            if(listOfRightWallPositions.size() <= 0) Log.err(getClass(), "There is not enought right wall positions! Still remains some objects without position.");
            return listOfRightWallPositions.get(0).getX();
        }

        if(gameObject.getPlacement().equals(PositionPlacement.BIG_OBJECT)){
            if(listOfBigObjectsPositions.size() <= 0) Log.err(getClass(), "There is not enought big objects positions! Still remains some objects without position.");
            return listOfBigObjectsPositions.get(0).getX();
        }

        if(gameObject.getPlacement().equals(PositionPlacement.SPECIAL)){
            if(listOfSpecialObjectsPositions.size() <= 0) Log.err(getClass(), "There is not enought special positions! Still remains some objects without position.");
            return listOfSpecialObjectsPositions.get(0).getX();
        }

        if(gameObject.getPlacement().equals(PositionPlacement.ON_THE_SHELF)){
            if(listOfOnTheShelfObjectsPositions.size() <= 0) Log.err(getClass(), "There is not enought on the shelf positions! Still remains some objects without position.");
            return listOfOnTheShelfObjectsPositions.get(0).getX();
        }

        //Position is free
        if(listOfFreePositions.size() <= 0) Log.err(getClass(), "There is not enought free positions! Still remains some objects without position.");
        return listOfFreePositions.get(0).getX();
    }

    protected float getObjectY(GameObjects gameObject){
        if(gameObject.getPlacement().equals(PositionPlacement.FIXED)) return gameObject.getFixedPositionY();

        if(gameObject.getPlacement().equals(PositionPlacement.LEFT_WALL)){
            if(listOfLeftWallPositions.size() <= 0) Log.err(getClass(), "There is not enought left wall positions! Still remains some objects without position.");
            return listOfLeftWallPositions.get(0).getY();
        }

        if(gameObject.getPlacement().equals(PositionPlacement.RIGHT_WALL)){
            if(listOfRightWallPositions.size() <= 0) Log.err(getClass(), "There is not enought right wall positions! Still remains some objects without position.");
            return listOfRightWallPositions.get(0).getY();
        }

        if(gameObject.getPlacement().equals(PositionPlacement.BIG_OBJECT)){
            if(listOfBigObjectsPositions.size() <= 0) Log.err(getClass(), "There is not enought big object positions! Still remains some objects without position.");
            return listOfBigObjectsPositions.get(0).getY();
        }

        if(gameObject.getPlacement().equals(PositionPlacement.SPECIAL)){
            if(listOfSpecialObjectsPositions.size() <= 0) Log.err(getClass(), "There is not enought special positions! Still remains some objects without position.");
            return listOfSpecialObjectsPositions.get(0).getY();
        }

        if(gameObject.getPlacement().equals(PositionPlacement.ON_THE_SHELF)){
            if(listOfOnTheShelfObjectsPositions.size() <= 0) Log.err(getClass(), "There is not enought on the shelf positions! Still remains some objects without position.");
            return listOfOnTheShelfObjectsPositions.get(0).getY();
        }

        //Position is free
        if(listOfFreePositions.size() <= 0) Log.err(getClass(), "There is not enought positions! Still remains some objects without position.");
        return listOfFreePositions.get(0).getY();
    }

    protected void removePositionIfNeeded(GameObjects gameObject) {
        if(gameObject.getPlacement().equals(PositionPlacement.FREE)) listOfFreePositions.remove(0);
        if(gameObject.getPlacement().equals(PositionPlacement.LEFT_WALL)) listOfLeftWallPositions.remove(0);
        if(gameObject.getPlacement().equals(PositionPlacement.RIGHT_WALL)) listOfRightWallPositions.remove(0);
        if(gameObject.getPlacement().equals(PositionPlacement.BIG_OBJECT)) listOfBigObjectsPositions.remove(0);
        if(gameObject.getPlacement().equals(PositionPlacement.SPECIAL)) listOfSpecialObjectsPositions.remove(0);
        if(gameObject.getPlacement().equals(PositionPlacement.ON_THE_SHELF)) listOfOnTheShelfObjectsPositions.remove(0);
    }

    protected int getNumberOfObjects(GameDifficulty gameDifficulty) {
        return MemoryGameDifficulty.getMemoryGameDifficultyForGameDifficulty(gameDifficulty).getNumberOfObjectsOnScene();
    }

    protected int getNumberOfSelectedObjects(GameDifficulty gameDifficulty) {
        return MemoryGameDifficulty.getMemoryGameDifficultyForGameDifficulty(gameDifficulty).getNumberOfSelectedObjects();
    }

    protected int getNumberOfConfusingObjects(GameDifficulty gameDifficulty) {
        return MemoryGameDifficulty.getMemoryGameDifficultyForGameDifficulty(gameDifficulty).getNumberOfConfusingObjects();
    }

    public abstract void evaluate(MemoryGame memoryGame);

    protected void paintItRed(GameObject fixedGameObject) {
        fixedGameObject.setColor(Color.RED);
    }

    protected void paintItGreen(GameObject fixedGameObject) {
        if(fixedGameObject==null) Log.err(getClass(), "GameObject is null!");
        else fixedGameObject.setColor(Color.GREEN);
    }

    public abstract void prepareObjectsForPlayer(MemoryGame memoryGame);

    public abstract Group addOtherButtonsAndWidgets(MemoryGame memoryGame);
}
