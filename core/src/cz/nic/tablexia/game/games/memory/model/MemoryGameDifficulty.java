/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.model;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.memory.actors.AbstractObjectPositioner;
import cz.nic.tablexia.game.games.memory.actors.BonusObjectPositioner;
import cz.nic.tablexia.game.games.memory.actors.EasyObjectPositioner;
import cz.nic.tablexia.game.games.memory.actors.MediumHardObjectPositioner;

public enum MemoryGameDifficulty {
    EASY(GameDifficulty.EASY, 4, 9, 0, new EasyObjectPositioner(), 5f),
    MEDIUM(GameDifficulty.MEDIUM, 5, 11,4, new MediumHardObjectPositioner(), 10f),
    HARD(GameDifficulty.HARD, 7, 13,4, new MediumHardObjectPositioner(), 10f),
    BONUS(GameDifficulty.BONUS, 5, 11,3, new BonusObjectPositioner(), 15f);

    private GameDifficulty gameDifficulty;
    private int numberOfSelectedObjects;                //objects marked by a card with number
    private int numberOfObjectsOnScene;                 //all objects visible on screen (photography)
    private int numberOfConfusingObjects;               //extra cards for confusing the player
    private AbstractObjectPositioner objectPositioner;
    private float photoDelay;

    MemoryGameDifficulty(GameDifficulty gameDifficulty, int numberOfSelectedObjects, int numberOfObjectsOnScene, int numberOfConfusingObjects, AbstractObjectPositioner objectPositioner, float photoDelay) {
        this.gameDifficulty = gameDifficulty;
        this.numberOfSelectedObjects = numberOfSelectedObjects;
        this.numberOfObjectsOnScene = numberOfObjectsOnScene;
        this.numberOfConfusingObjects = numberOfConfusingObjects;
        this.objectPositioner = objectPositioner;
        this.photoDelay = photoDelay;
    }

    public int getNumberOfSelectedObjects() {
        return numberOfSelectedObjects;
    }

    public int getNumberOfObjectsOnScene() {
        return numberOfObjectsOnScene;
    }

    public int getNumberOfConfusingObjects() {
        return numberOfConfusingObjects;
    }

    public AbstractObjectPositioner getObjectPositioner() {
        return objectPositioner;
    }

    public float getPhotoDelay() {
        return photoDelay;
    }

    public static MemoryGameDifficulty getMemoryGameDifficultyForGameDifficulty(GameDifficulty gameDifficulty) throws IllegalArgumentException {
        for (MemoryGameDifficulty memoryGameDifficulty : MemoryGameDifficulty.values()) {
            if (memoryGameDifficulty.gameDifficulty == gameDifficulty) {
                return memoryGameDifficulty;
            }
        }
        throw new IllegalArgumentException();
    }
}
