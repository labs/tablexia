/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.game_objects;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.memory.assets.MemoryAssets;


public class NumberCardGroup extends Group {
    private static final int CARD_IMAGE_WIDTH       = 29;
    private static final int CARD_IMAGE_HEIGHT      = 35;
    private static final int CARD_NUMBER_WIDTH      = 10;
    private static final int CARD_NUMBER_HEIGHT     = 20;
    private static final int CARD_NUMBER_OFFSET_X   = 7;
    private static final int CARD_NUMBER_OFFSET_Y   = 7;
    private static final int DEFAULT_FIXED_CARD_ID  = -1;

    private MemoryGame memoryGame;
    private int cardNumber;
    private boolean fixedObject;
    private GameObject fixedGameObject;
    private int fixedObjectCardDefaultCardId;
    private MemoryImage cardImage;

    public NumberCardGroup(MemoryGame memoryGame) {
        super();
        this.memoryGame = memoryGame;
        this.fixedObject = false;
        this.fixedObjectCardDefaultCardId = DEFAULT_FIXED_CARD_ID;
        this.fixedGameObject = null;
    }

    public void createNumberCard(CardNumbers card, int cardNumber, GameObject gameObject, GameObjects shuffledSelectedObject) {
        float cardObjectYVariance = CARD_IMAGE_HEIGHT - gameObject.getHeight()/2;
        float cardYOffset = (cardObjectYVariance > 0) ? cardObjectYVariance : 0;
        this.setBounds(gameObject.getGameObjectX() + gameObject.getWidth()/2 - CARD_IMAGE_WIDTH,  gameObject.getGameObjectY() - cardYOffset, CARD_IMAGE_WIDTH, CARD_IMAGE_HEIGHT);
        this.cardNumber = cardNumber;
        if(shuffledSelectedObject.isInTheAir()) cardImage = new MemoryImage(memoryGame.getScreenTextureRegion(MemoryAssets.CARD1));
        else cardImage = new MemoryImage(memoryGame.getScreenTextureRegion(MemoryAssets.CARD));
        cardImage.setSize(CARD_IMAGE_WIDTH,CARD_IMAGE_HEIGHT);
        cardImage.setPosition(0, 0);
        Image number = new Image(memoryGame.getScreenTextureRegion(card.getNumberAssetPath()));
        number.setSize(CARD_NUMBER_WIDTH,CARD_NUMBER_HEIGHT);
        number.setPosition(CARD_NUMBER_OFFSET_X, CARD_NUMBER_OFFSET_Y);
        this.addActor(cardImage);
        this.addActor(number);
    }

    public boolean isFixedObject() {
        return fixedObject;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public GameObject getFixedGameObject() {
        return fixedGameObject;
    }

    public void fixObjectWithId(GameObject gameObject) {
        this.fixedObject = true;
        this.fixedObjectCardDefaultCardId = gameObject.getDefaultCardNumber();
        this.fixedGameObject = gameObject;
    }

    public void unfixObject() {
        this.fixedObject = false;
        this.fixedObjectCardDefaultCardId = DEFAULT_FIXED_CARD_ID;
        this.fixedGameObject = null;
    }

    public boolean isGameObjectFixedRight() {
        if(fixedObjectCardDefaultCardId > 0 && cardNumber == fixedObjectCardDefaultCardId) return true;
        return false;
    }
}
