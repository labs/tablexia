/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.memory.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.memory.game_objects.GameObject;
import cz.nic.tablexia.game.games.memory.game_objects.MemoryImage;
import cz.nic.tablexia.game.games.memory.game_objects.NumberCardGroup;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

public class EasyObjectPositioner extends AbstractObjectPositioner {
    @Override
    public void prepare(MemoryGame memoryGame) {
        initGroupsAndLists(memoryGame);

        shuffleAndSelectObjects(memoryGame);
    }

    protected void shuffleAndSelectObjects(MemoryGame memoryGame) {
        schuffleAndSelectOtherAndSelected(memoryGame.getGameDifficulty(), memoryGame);
    }

    @Override
    public void evaluate(MemoryGame memoryGame) {
        for(GameObject gameObject: movableGameObjects){
            if(gameObject.isFixed()) paintItGreen(gameObject);
        }
        for(MemoryImage gameObject: fixedGameObjects){
            if(gameObject.isSelected()) {
                gameObject.setColor(Color.RED);
                memoryGame.setMistakes(memoryGame.getMistakes() + 1);
            }
        }
    }

    @Override
    public void prepareObjectsForPlayer(MemoryGame memoryGame) {
        addClickListenersToObjects(memoryGame);
        for (NumberCardGroup numberCardGroup : getNumberCards()) {
            numberCardGroup.setVisible(false);
        }
    }

    private void addClickListenersToObjects(MemoryGame memoryGame) {
        for (GameObject movableGameObject : movableGameObjects) {
            movableGameObject.addListener(
                    new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            if(!movableGameObject.isFixed()){
                                memoryGame.setSelectedObjects(memoryGame.getSelectedObjects() + 1);
                                movableGameObject.setObjectSelected(memoryGame, true);
                            }
                            else{
                                memoryGame.setSelectedObjects(memoryGame.getSelectedObjects() - 1);
                                movableGameObject.setObjectSelected(memoryGame, false);
                            }
                            if (memoryGame.getSelectedObjects() == numberOfSelectedObjects) memoryGame.enableDoneButton();
                            else memoryGame.disableDoneButton();
                        }
                    });
        }
        for (MemoryImage fixedGameObject : fixedGameObjects) {
            fixedGameObject.addListener(
                    new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            if (!fixedGameObject.isSelected()) {
                                memoryGame.setSelectedObjects(memoryGame.getSelectedObjects() + 1);
                                fixedGameObject.setObjectSelected(memoryGame, true);
                            } else {
                                memoryGame.setSelectedObjects(memoryGame.getSelectedObjects() - 1);
                                fixedGameObject.setObjectSelected(memoryGame, false);
                            }
                            if (memoryGame.getSelectedObjects() == numberOfSelectedObjects) memoryGame.enableDoneButton();
                            else memoryGame.disableDoneButton();
                        }
                    });
        }
    }

    @Override
    public Group addOtherButtonsAndWidgets(MemoryGame memoryGame) {
        Group buttonsAndWidgetsGroup = new Group();
        return buttonsAndWidgetsGroup;
    }
}
