/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.listeners;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import cz.nic.tablexia.game.games.safe.AbstractMechanics;
import cz.nic.tablexia.game.games.safe.gameobject.CableGroup;

import cz.nic.tablexia.util.listener.DragActorListener;

/**
 * Created by lmarik on 18.7.17.
 */

public class CableDragListener extends DragActorListener {

    private CableGroup cableGroup;
    private Vector2 min = null;
    private Vector2 max = null;
    private float startX;
    private float startY;

    public boolean isDragged() {
        return dragged;
    }

    private boolean dragged;

    public CableDragListener(Actor actor, CableGroup cableGroup) {
        super(actor);
        this.cableGroup = cableGroup;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if(button == Input.Buttons.RIGHT) return false;
        startX = x;
        startY = y;
        dragged = false;
        AbstractMechanics.holding = true;
        cableGroup.disableButtons();
        cableGroup.stopBackAnimation();
        return super.touchDown(event, x, y, pointer, button);
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (startX != x || startY != y) dragged = true;
        if (pointer == 0) {
            float dragX = actor.getX() + (x - grabX) * actor.getScaleX();
            float dragY = actor.getY() + (y - grabY) * actor.getScaleY();

            if (min != null && max != null) {
                //Check X-position
                if (dragX < min.x || (dragX + actor.getWidth()) > max.x) {
                    dragX = actor.getX();
                }

                //Check Y-position
                if (dragY < min.y || (dragY + actor.getHeight()) > max.y) {
                    dragY = actor.getY();
                }
            }

            if (cableGroup.nearFinalSocket(dragX, dragY)) {
                cableGroup.setPositionToFinalSocket();
            } else {
                actor.setPosition(dragX, dragY);
                cableGroup.cablePositionChanges();
                if (cableGroup.isInSocket()) {
                    cableGroup.disconnectConnector();
                    cableGroup.setInSocket(false);
                }
            }

            event.stop();
        }
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        super.touchUp(event, x, y, pointer, button);
        AbstractMechanics.holding = false;

        cableGroup.enableButtons();

        if (!dragged) cableGroup.lightLightAndPlaySound();
        dragged = false;

        if (!cableGroup.isInSocket()) cableGroup.startBackAnimation();
        cableGroup.enableDoneButton();
    }


    public void setLimit(Vector2 min, Vector2 max) {
        this.min = min;
        this.max = max;
    }
}
