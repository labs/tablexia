/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.listeners;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.concurrent.atomic.AtomicReference;

import cz.nic.tablexia.game.games.safe.SafeGame;
import cz.nic.tablexia.util.ui.button.ImageTablexiaButton;

/**
 * Created by aneta on 19.12.17.
 */

public abstract class AbstractButtonClickListener extends ClickListener {

    protected static AtomicReference<ImageTablexiaButton> firstClickedButton = new AtomicReference<>(null);

    protected SafeGame safeGame;

    public AbstractButtonClickListener(SafeGame safeGame) {
        this.safeGame = safeGame;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if(button == Input.Buttons.RIGHT) return false;
        boolean result = super.touchDown(event, x, y, pointer, button);
        if (result && firstClickedButton.get() == null) {
            firstClickedButton.set(getImageTablexiaButton());
            safeGame.makeBtnsAndSafeUntouchable(getImageTablexiaButton());
        }
        return result;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        super.touchUp(event, x, y, pointer, button);
        safeGame.makeBtnUntouchable(getImageTablexiaButton());
    }

    @Override
    public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        if (firstClickedButton != null && firstClickedButton.get().equals(getImageTablexiaButton())) {
            onClicked();
        }
    }

    public abstract void onClicked();


    protected abstract ImageTablexiaButton getImageTablexiaButton();
}
