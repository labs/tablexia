/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.safe.model;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.games.safe.assets.SafeAssets;

/**
 * Created by Aneta Steimarová on 13.2.18.
 */

public enum SafeGameResultStars {
    NO(AbstractTablexiaGame.GameResult.NO_STAR, SafeAssets.SCORE_0_TEXT, SafeAssets.SCORE_0_SOUND),
    ONE(AbstractTablexiaGame.GameResult.ONE_STAR, SafeAssets.SCORE_1_TEXT, SafeAssets.SCORE_1_SOUND),
    TWO(AbstractTablexiaGame.GameResult.TWO_STAR, SafeAssets.SCORE_2_TEXT, SafeAssets.SCORE_2_SOUND),
    THREE(AbstractTablexiaGame.GameResult.THREE_STAR, SafeAssets.SCORE_3_TEXT, SafeAssets.SCORE_3_SOUND);

    private AbstractTablexiaGame.GameResult gameResult;
    private String text;
    private String sound;

    SafeGameResultStars(AbstractTablexiaGame.GameResult gameResult, String text, String sound) {
        this.gameResult = gameResult;
        this.text = text;
        this.sound = sound;
    }


    public static String getTextForGameResult(AbstractTablexiaGame.GameResult gameResult) throws IllegalArgumentException {
        for (SafeGameResultStars safeGameResultStars : SafeGameResultStars.values()) {
            if (safeGameResultStars.gameResult == gameResult) {
                return safeGameResultStars.text;
            }
        }
        throw new IllegalArgumentException();
    }

    public static String getSoundForGameResult(AbstractTablexiaGame.GameResult gameResult) throws IllegalArgumentException {
        for (SafeGameResultStars safeGameResultStars : SafeGameResultStars.values()) {
            if (safeGameResultStars.gameResult == gameResult) {
                return safeGameResultStars.sound;
            }
        }
        throw new IllegalArgumentException();
    }
}
