/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.gameobject;

import cz.nic.tablexia.game.games.safe.assets.SafeAssets;

/**
 * Created by Aneta Steimarová on 27.6.17.
 */

public enum VisualizationObject {
    VISUALIZATION_OBJECT1(0, SafeAssets.VISUALIZATION_OBJECT1),
    VISUALIZATION_OBJECT2(1, SafeAssets.VISUALIZATION_OBJECT2),
    VISUALIZATION_OBJECT3(2, SafeAssets.VISUALIZATION_OBJECT3),
    VISUALIZATION_OBJECT4(3, SafeAssets.VISUALIZATION_OBJECT4);

    int idNumber;
    String image;

    public String getImage() {
        return image;
    }

    VisualizationObject(int idNumber, String image) {
        this.idNumber = idNumber;
        this.image = image;
    }

    public static String getImageForID(int idNumber) {
        for (VisualizationObject object : VisualizationObject.values()) {
            if (object.idNumber == idNumber) return object.image;
        }
        return null;
    }
}
