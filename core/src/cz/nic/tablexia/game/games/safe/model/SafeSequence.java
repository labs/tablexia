/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.games.safe.gameobject.SafeSounds;

/**
 * Created by Aneta Steimarová on 19.5.17.
 */

public class SafeSequence {
    private SafeSounds[] generatedSoundSequence;    //chosen sound sequence
    private SafeSounds[] soundsToSequence;          //all sounds that are in sequence
    private SafeSounds[] soundsToLights;            //selected sounds to show as light
    private SafeSounds expected;                    //expectedInt result - next sound in sequence
    private int playAgainFirstXSounds;              //how many sounds from sequence will be played again when listening

    public SafeSounds[] getGeneratedSoundSequence() {
        return generatedSoundSequence;
    }

    public SafeSounds[] getSoundsToSequence() {
        return soundsToSequence;
    }

    public SafeSounds[] getSoundsToLights() {
        return soundsToLights;
    }

    public SafeSounds getExpected() {
        return expected;
    }

    public int getPlayAgainFirstXSounds() {
        return playAgainFirstXSounds;
    }

    public SafeSequence(SafeSounds[] generatedSoundSequence, SafeSounds[] soundsToSequence, SafeSounds[] soundsToLights, SafeSounds expected, int playAgainFirstXSounds) {
        this.generatedSoundSequence = generatedSoundSequence;
        this.soundsToSequence = soundsToSequence;
        this.soundsToLights = shakeSounds(soundsToLights);
        this.expected = expected;
        this.playAgainFirstXSounds = playAgainFirstXSounds;
    }

    private SafeSounds[] shakeSounds(SafeSounds[] soundsToLights) {
        Random random = new Random();
        int numberOfSounds = soundsToLights.length;
        SafeSounds[] shakedSounds = new SafeSounds[numberOfSounds];
        List<SafeSounds> listOfSounds = new ArrayList<SafeSounds>();
        for (SafeSounds safeSound : soundsToLights) {
            listOfSounds.add(safeSound);
        }

        for (int i = 0; i < numberOfSounds; i++) {
            SafeSounds randomSound = listOfSounds.get(random.nextInt(listOfSounds.size()));
            shakedSounds[i] = randomSound;
            listOfSounds.remove(randomSound);
        }
        return shakedSounds;
    }

    public int findSoundPositionInSoundsToSequence(SafeSounds safeSound) {
        for (int i = 0; i < soundsToSequence.length; i++) {
            if (soundsToSequence[i].equals(safeSound)) return i;
        }
        return -1;
    }
}
