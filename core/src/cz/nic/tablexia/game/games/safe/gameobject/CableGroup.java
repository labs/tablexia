/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.gameobject;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.List;

import cz.nic.tablexia.game.games.safe.AbstractMechanics;
import cz.nic.tablexia.game.games.safe.Cable;
import cz.nic.tablexia.game.games.safe.SafeGame;
import cz.nic.tablexia.game.games.safe.SafeMechanicsMedium;
import cz.nic.tablexia.game.games.safe.listeners.CableDragListener;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;

/**
 * Created by lmarik on 14.7.17.
 */

public class CableGroup extends Group {
    private static final int    SOCKET_WIDTH        = 125;
    private static final int    SOCKET_HEIGHT       = 75;

    private static final int    CONNECTOR_WIDTH     = 80;
    private static final int    CONNECTOR_HEIGHT    = 40;
    private static final int    MAX_CABLE_PARTS     = 18;

    private static final int    SAFE_LIGHT_SIZE     = 40;
    private static final int    SAFE_LIGHT_X_POS    = 50;

    private static final float  LIGHT_DELAY         = 1;

    private SafeGame safeGame;
    private SafeMechanicsMedium mechanicsMedium;
    private CableDragListener limitListener;
    private SafeLightImage lightImage;
    private List<SafeLightImage> listOfLights;

    private Cable cable;
    private Color cableColor;
    private Image connector;
    private Image socket;

    private int id;
    private boolean inSocket;

    private Vector2 startPosition = null;

    public CableGroup(int id, SafeMechanicsMedium mechanicsMedium, SafeGame safeGame, TextureRegion connectorImage, TextureRegion socketImage, TextureRegion cableImage, Color cableColor,
                      String lightGrey, String lightRed, String lightGreen, String lightBlue, Music lightSound, List<SafeLightImage> listOfLights, String lightName, String connectorName) {
        this.id = id;
        this.safeGame = safeGame;
        this.mechanicsMedium = mechanicsMedium;
        this.listOfLights = listOfLights;
        this.cableColor = cableColor;

        inSocket = false;

        setTouchable(Touchable.childrenOnly);
        prepareCableAndSocket(0, 0, SOCKET_WIDTH, 0, socketImage, cableImage);
        prepareConnector(connectorImage, SOCKET_WIDTH, SOCKET_HEIGHT / 2 - CONNECTOR_HEIGHT / 2, connectorName + id);

        lightImage = new SafeLightImage(safeGame, lightGrey, lightRed, lightGreen, lightBlue, lightSound);
        lightImage.setSize(SAFE_LIGHT_SIZE, SAFE_LIGHT_SIZE);
        lightImage.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                lightAndPlay();
            }
        });
        lightImage.addListener(lightImage.getInputListener());
        lightImage.setName(lightName + id);
        listOfLights.add(lightImage);

        addActor(socket);
        addActor(lightImage);
        addActor(connector);
        addActor(cable);
    }

    private void lightAndPlay() {
        addAction(Actions.sequence(Actions.run(new Runnable() {
            @Override
            public void run() {
                lightImage.light(listOfLights, safeGame.getSelectedLight());
            }
        }), Actions.delay(LIGHT_DELAY), Actions.run(new Runnable() {
            @Override
            public void run() {
                if (safeGame.getSelectedLight() != null)
                    safeGame.getSelectedLight().lightWithoutSound(listOfLights, safeGame.getSelectedLight());
                else lightImage.turnOffAllLights(listOfLights);
            }
        })));
    }

    public void setComponentsPosition(float x, float y) {
        socket.setPosition(x, y);

        cable.setStartingPoint(new Vector2(x + SOCKET_WIDTH, y + SOCKET_HEIGHT / 2));
        cable.setEndingPoint(new Vector2(x + SOCKET_WIDTH, y + SOCKET_HEIGHT / 2));

        startPosition = new Vector2(x + SOCKET_WIDTH, y + SOCKET_HEIGHT / 2 - CONNECTOR_HEIGHT / 2);

        connector.setPosition(startPosition.x, startPosition.y);

        lightImage.setPosition(x + SAFE_LIGHT_X_POS, y + SOCKET_HEIGHT / 2 - lightImage.getHeight() / 2);
    }

    public void dispose() {
        if (cable != null) {
            cable.dispose();
        }
    }

    private void prepareCableAndSocket(float startX, float startY, float finishX, float finishY, TextureRegion socketImage, TextureRegion cableImage) {
        socket = new Image(socketImage);
        socket.setBounds(
                startX,
                startY,
                SOCKET_WIDTH,
                SOCKET_HEIGHT
        );

        cable = new Cable(MAX_CABLE_PARTS, cableImage, cableColor);
        cable.setStartingPoint(new Vector2(startX + SOCKET_WIDTH, startY + SOCKET_HEIGHT / 2));
        cable.setEndingPoint(new Vector2(finishX, finishY + SOCKET_HEIGHT / 2));

    }

    private void prepareConnector(TextureRegion connectorImage, float finishCableX, float finishCableY, String connectorName) {
        float startX = finishCableX;
        float startY = finishCableY;

        connector = new Image(connectorImage);
        connector.setBounds(
                startX,
                startY,
                CONNECTOR_WIDTH,
                CONNECTOR_HEIGHT);

        limitListener = new CableDragListener(connector, this);
        connector.setName(connectorName);
        connector.addListener(limitListener);

    }

    public void lightLightAndPlaySound() {
        lightAndPlay();
    }

    public void cablePositionChanges() {
        cable.setCableParts();
        cable.setEndingPoint(new Vector2(connector.getX(), connector.getY() + CONNECTOR_HEIGHT / 2));
    }

    public void setPositionToFinalSocket() {
        if (inSocket)
            return;

        Rectangle finalSocketRect = mechanicsMedium.getFinalSocketDetect();
        float xPosition = finalSocketRect.getX() - connector.getWidth() / 2;
        float yPosition = finalSocketRect.getY() + connector.getHeight() / 2;

        connector.setPosition(xPosition, yPosition);
        cablePositionChanges();

        mechanicsMedium.setMinZIndexForCable(id);
        mechanicsMedium.setSocketCableID(id);
        safeGame.setSelectedLight(lightImage, listOfLights, true);
        setInSocket(true);
        AbstractTablexiaScreen.triggerScenarioStepEvent(AbstractMechanics.LIGHT_LIGHTED_UP);
    }

    public boolean nearFinalSocket(float newXPosition, float newYPosition) {
        Rectangle finalSocketRect = mechanicsMedium.getFinalSocketDetect();

        if (finalSocketRect == null)
            return false;

        return finalSocketRect.getX() < newXPosition + connector.getWidth() && finalSocketRect.getX() + finalSocketRect.getWidth() > newXPosition
                && finalSocketRect.getY() < newYPosition + connector.getHeight() && finalSocketRect.getY() + finalSocketRect.getHeight() > newYPosition;
    }

    public void startBackAnimation() {
        Vector2 connectorPosition = new Vector2(connector.getX(), connector.getY());
        float duration = (connectorPosition.dst(startPosition)) / mechanicsMedium.getWidth();

        connector.addAction(Actions.parallel(Actions.moveTo(startPosition.x, startPosition.y, duration), new Action() {
            @Override
            public boolean act(float delta) {
                cablePositionChanges();
                return true;
            }
        }));
    }

    public void stopBackAnimation() {
        connector.clearActions();
    }

    public void setCableLimit(Vector2 max) {
        limitListener.setLimit(new Vector2(connector.getX(), 0), max);
    }

    public int getId() {
        return id;
    }

    public boolean isInSocket() {
        return inSocket;
    }

    public void disconnectConnector() {
        mechanicsMedium.setMinZIndexForSocket();
        mechanicsMedium.setSocketCableID(-1);
    }

    public void setInSocket(boolean inSocket) {
        this.inSocket = inSocket;
        if (!inSocket) safeGame.setSelectedLight(null, listOfLights, false);
    }

    public void setAllUntouchable() {
        lightImage.clearListeners();
        connector.clearListeners();
    }

    public void setAllTouchable() {
        lightImage.addListener(lightImage.getInputListener());
        connector.addListener(limitListener);
    }

    public void enableButtons() {
        safeGame.getReplayButton().setEnabled();
        if (!safeGame.isUsedHelpButton()) safeGame.getHelpButton().setEnabled();
    }

    public void disableButtons() {
        safeGame.getDoneButton().setDisabled();
        safeGame.getReplayButton().setDisabled();
        safeGame.getHelpButton().setDisabled();
    }

    public void enableDoneButton() {
        if(safeGame.getSelectedLight() != null) safeGame.getDoneButton().setEnabled();
    }

    public boolean getIfIsDragged() {
        return limitListener.isDragged();
    }

    @Override
    public float getHeight() {
        return socket.getHeight();
    }
}
