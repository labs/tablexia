/*
 *
 *  * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.gameobject;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.safe.assets.SafeAssets;

public enum BonusCableTypeDefinition {
    TYPE_1(SafeAssets.BONUS_SOCKET1, 0.26f, SafeAssets.BONUS_SOCKET_FINAL1, 0.35f, SafeAssets.BONUS_CABLE1, SafeAssets.BONUS_CABLE1_CUT, 0.95f, 0.1f, SafeAssets.BONUS_LIGHT1_GREY, SafeAssets.BONUS_LIGHT1_RED, SafeAssets.BONUS_LIGHT1_GREEN, SafeAssets.BONUS_LIGHT1_BLUE, 0.29f, 0.4f, 0.15f),
    TYPE_2(SafeAssets.BONUS_SOCKET2, 0.267f, SafeAssets.BONUS_SOCKET_FINAL2, 0.37f, SafeAssets.BONUS_CABLE2, SafeAssets.BONUS_CABLE2_CUT, 0.9f, 0.1f, SafeAssets.BONUS_LIGHT2_GREY, SafeAssets.BONUS_LIGHT2_RED, SafeAssets.BONUS_LIGHT2_GREEN, SafeAssets.BONUS_LIGHT2_BLUE, 0.43f, 0.31f, 0.4f),
    TYPE_3(SafeAssets.BONUS_SOCKET3, 0.267f, SafeAssets.BONUS_SOCKET_FINAL3, 0.33f, SafeAssets.BONUS_CABLE3, SafeAssets.BONUS_CABLE3_CUT, 0.7f, 0.3f, SafeAssets.BONUS_LIGHT3_GREY, SafeAssets.BONUS_LIGHT3_RED, SafeAssets.BONUS_LIGHT3_GREEN, SafeAssets.BONUS_LIGHT3_BLUE, 0.27f, 0.48f, 0.4f),
    TYPE_4(SafeAssets.BONUS_SOCKET4, 0.276f, SafeAssets.BONUS_SOCKET_FINAL4, 0.298f, SafeAssets.BONUS_CABLE4, SafeAssets.BONUS_CABLE4_CUT, 0.95f, 0.2f, SafeAssets.BONUS_LIGHT4_GREY, SafeAssets.BONUS_LIGHT4_RED, SafeAssets.BONUS_LIGHT4_GREEN, SafeAssets.BONUS_LIGHT4_BLUE, 0.35f, 0.42f, 0.25f);

    private String socket;
    private float socketPercentReduction;
    private String finalSocket;
    private float finalSocketPercentReduction;
    private String cable;
    private String cutCable;
    private float startPosX;
    private float endPosX;
    private String lightGrey;
    private String lightRed;
    private String lightGreen;
    private String lightBlue;
    private float lightSize;
    private float lightX;
    private float lightY;

    BonusCableTypeDefinition(String socket, float socketPercentReduction, String finalSocket, float finalSocketPercentReduction,
                             String cable, String cutCable, float startPosX, float endPosX,
                             String lightGrey, String lightRed, String lightGreen, String lightBlue, float lightSize, float lightX, float lightY) {
        this.socket = socket;
        this.socketPercentReduction = socketPercentReduction;
        this.finalSocket = finalSocket;
        this.finalSocketPercentReduction = finalSocketPercentReduction;
        this.cable = cable;
        this.cutCable = cutCable;
        this.startPosX = startPosX;
        this.endPosX = endPosX;
        this.lightGrey = lightGrey;
        this.lightRed = lightRed;
        this.lightGreen = lightGreen;
        this.lightBlue = lightBlue;
        this.lightSize = lightSize;
        this.lightX = lightX;
        this.lightY = lightY;
    }

    public String getSocket() {
        return socket;
    }

    public float getSocketPercentReduction() {
        if (TablexiaSettings.getInstance().isUseHdAssets()) return socketPercentReduction;
        return 2*socketPercentReduction;
    }

    public String getFinalSocket() {
        return finalSocket;
    }

    public float getFinalSocketPercentReduction() {
        if (TablexiaSettings.getInstance().isUseHdAssets()) return finalSocketPercentReduction;
        return 2*finalSocketPercentReduction;
    }

    public String getCable() {
        return cable;
    }

    public String getCutCable() {
        return cutCable;
    }

    public float getStartPosX() {
        return startPosX;
    }

    public float getEndPosX() {
        return endPosX;
    }

    public String getLightGrey() {
        return lightGrey;
    }

    public String getLightRed() {
        return lightRed;
    }

    public String getLightGreen() {
        return lightGreen;
    }

    public String getLightBlue() {
        return lightBlue;
    }

    public float getLightSize() {
        return lightSize;
    }

    public float getLightX() {
        return lightX;
    }

    public float getLightY() {
        return lightY;
    }
}
