/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.model;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.safe.gameobject.SafeSounds;

/**
 * Created by Aneta Steimarová on 1.6.17.
 */

public enum SafeMediumLevelDefinition implements SafeLevelDefinition {
    //level 1
    LVL_1_A(new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.C}, SafeSounds.D, new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.C, SafeSounds.D}, new SafeSounds[]{SafeSounds.A, SafeSounds.C, SafeSounds.D}),
    LVL_1_B(new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.THREE}, SafeSounds.FOUR, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.THREE, SafeSounds.FOUR}, new SafeSounds[]{SafeSounds.ONE, SafeSounds.THREE, SafeSounds.FOUR}),
    LVL_1_C(new SafeSounds[]{SafeSounds.D, SafeSounds.E, SafeSounds.F}, SafeSounds.G, new SafeSounds[]{SafeSounds.D, SafeSounds.E, SafeSounds.F, SafeSounds.G}, new SafeSounds[]{SafeSounds.G, SafeSounds.B, SafeSounds.H}),
    LVL_1_D(new SafeSounds[]{SafeSounds.SEVEN, SafeSounds.EIGHT, SafeSounds.NINE}, SafeSounds.TEN, new SafeSounds[]{SafeSounds.SEVEN, SafeSounds.EIGHT, SafeSounds.NINE, SafeSounds.TEN}, new SafeSounds[]{SafeSounds.TEN, SafeSounds.ONE, SafeSounds.NINE}),
    LVL_1_E(new SafeSounds[]{SafeSounds.FIVE, SafeSounds.FOUR, SafeSounds.THREE, SafeSounds.TWO}, SafeSounds.ONE, new SafeSounds[]{SafeSounds.FIVE, SafeSounds.FOUR, SafeSounds.THREE, SafeSounds.TWO, SafeSounds.ONE}, new SafeSounds[]{SafeSounds.ONE, SafeSounds.SIX, SafeSounds.TWO}),
    LVL_1_F(new SafeSounds[]{SafeSounds.E, SafeSounds.D, SafeSounds.C, SafeSounds.B}, SafeSounds.A, new SafeSounds[]{SafeSounds.E, SafeSounds.D, SafeSounds.C, SafeSounds.B, SafeSounds.A}, new SafeSounds[]{SafeSounds.A, SafeSounds.E, SafeSounds.F}),
    LVL_1_G(new SafeSounds[]{SafeSounds.I, SafeSounds.J, SafeSounds.K}, SafeSounds.L, new SafeSounds[]{SafeSounds.I, SafeSounds.J, SafeSounds.K, SafeSounds.L}, new SafeSounds[]{SafeSounds.M, SafeSounds.N, SafeSounds.L}),
    LVL_1_H(new SafeSounds[]{SafeSounds.FOUR, SafeSounds.FIVE, SafeSounds.SIX}, SafeSounds.SEVEN, new SafeSounds[]{SafeSounds.FOUR, SafeSounds.FIVE, SafeSounds.SIX, SafeSounds.SEVEN}, new SafeSounds[]{SafeSounds.TWO, SafeSounds.SEVEN, SafeSounds.EIGHT}),

    //level 2
    LVL_2_A(new SafeSounds[]{SafeSounds.A, SafeSounds.A, SafeSounds.B, SafeSounds.B}, SafeSounds.C, new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.C}, new SafeSounds[]{SafeSounds.C, SafeSounds.A, SafeSounds.E}),
    LVL_2_B(new SafeSounds[]{SafeSounds.ONE, SafeSounds.ONE, SafeSounds.TWO, SafeSounds.TWO,}, SafeSounds.THREE, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.THREE}, new SafeSounds[]{SafeSounds.TWO, SafeSounds.THREE, SafeSounds.FIVE}),
    LVL_2_C(new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.ONE, SafeSounds.THREE, SafeSounds.ONE, SafeSounds.FOUR}, SafeSounds.ONE, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.THREE, SafeSounds.FOUR}, new SafeSounds[]{SafeSounds.ONE, SafeSounds.FIVE, SafeSounds.SIX}),
    LVL_2_D(new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.A, SafeSounds.C, SafeSounds.A}, SafeSounds.D, new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.C, SafeSounds.D}, new SafeSounds[]{SafeSounds.A, SafeSounds.D, SafeSounds.E}),
    LVL_2_E(new SafeSounds[]{SafeSounds.THREE, SafeSounds.THREE, SafeSounds.TWO, SafeSounds.TWO, SafeSounds.ONE}, SafeSounds.ONE, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.THREE}, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.THREE}),
    LVL_2_F(new SafeSounds[]{SafeSounds.E, SafeSounds.E, SafeSounds.D, SafeSounds.D, SafeSounds.C}, SafeSounds.C, new SafeSounds[]{SafeSounds.C, SafeSounds.D, SafeSounds.E}, new SafeSounds[]{SafeSounds.B, SafeSounds.C, SafeSounds.E}),
    LVL_2_G(new SafeSounds[]{SafeSounds.X, SafeSounds.X, SafeSounds.Y, SafeSounds.Y, SafeSounds.Z}, SafeSounds.Z, new SafeSounds[]{SafeSounds.X, SafeSounds.Y, SafeSounds.Z}, new SafeSounds[]{SafeSounds.A, SafeSounds.X, SafeSounds.Z}),
    LVL_2_H(new SafeSounds[]{SafeSounds.FIVE, SafeSounds.FIVE, SafeSounds.SIX, SafeSounds.SIX, SafeSounds.SEVEN}, SafeSounds.SEVEN, new SafeSounds[]{SafeSounds.FIVE, SafeSounds.SIX, SafeSounds.SEVEN}, new SafeSounds[]{SafeSounds.FOUR, SafeSounds.SEVEN, SafeSounds.EIGHT}),
    LVL_2_I(new SafeSounds[]{SafeSounds.EIGHT, SafeSounds.EIGHT, SafeSounds.NINE, SafeSounds.NINE, SafeSounds.TEN}, SafeSounds.TEN, new SafeSounds[]{SafeSounds.EIGHT, SafeSounds.NINE, SafeSounds.TEN}, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TEN, SafeSounds.SEVEN}),
    LVL_2_J(new SafeSounds[]{SafeSounds.K, SafeSounds.K, SafeSounds.L, SafeSounds.L, SafeSounds.M}, SafeSounds.M, new SafeSounds[]{SafeSounds.K, SafeSounds.L, SafeSounds.M}, new SafeSounds[]{SafeSounds.M, SafeSounds.N, SafeSounds.J}),

    //level 3
    LVL_3_A(new SafeSounds[]{SafeSounds.ONE, SafeSounds.THREE, SafeSounds.FIVE}, SafeSounds.SEVEN, new SafeSounds[]{SafeSounds.ONE, SafeSounds.THREE, SafeSounds.FIVE, SafeSounds.SEVEN}, new SafeSounds[]{SafeSounds.TWO, SafeSounds.SIX, SafeSounds.SEVEN, SafeSounds.EIGHT}),
    LVL_3_B(new SafeSounds[]{SafeSounds.A, SafeSounds.C, SafeSounds.E}, SafeSounds.G, new SafeSounds[]{SafeSounds.A, SafeSounds.C, SafeSounds.E, SafeSounds.G}, new SafeSounds[]{SafeSounds.B, SafeSounds.F, SafeSounds.G, SafeSounds.H}),
    LVL_3_C(new SafeSounds[]{SafeSounds.TWO, SafeSounds.FOUR, SafeSounds.SIX}, SafeSounds.EIGHT, new SafeSounds[]{SafeSounds.TWO, SafeSounds.FOUR, SafeSounds.SIX, SafeSounds.EIGHT}, new SafeSounds[]{SafeSounds.TWO, SafeSounds.SIX, SafeSounds.TEN, SafeSounds.EIGHT}),
    LVL_3_D(new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.FOUR, SafeSounds.FIVE, SafeSounds.SEVEN, SafeSounds.EIGHT}, SafeSounds.TEN, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.FOUR, SafeSounds.FIVE, SafeSounds.SEVEN, SafeSounds.EIGHT, SafeSounds.TEN}, new SafeSounds[]{SafeSounds.EIGHT, SafeSounds.NINE, SafeSounds.TEN, SafeSounds.ONE}),
    LVL_3_E(new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.D, SafeSounds.E, SafeSounds.G, SafeSounds.H}, SafeSounds.J, new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.D, SafeSounds.E, SafeSounds.G, SafeSounds.H, SafeSounds.J}, new SafeSounds[]{SafeSounds.I, SafeSounds.J, SafeSounds.H, SafeSounds.A}),
    LVL_3_F(new SafeSounds[]{SafeSounds.B, SafeSounds.D, SafeSounds.F}, SafeSounds.H, new SafeSounds[]{SafeSounds.B, SafeSounds.D, SafeSounds.F, SafeSounds.H}, new SafeSounds[]{SafeSounds.G, SafeSounds.H, SafeSounds.E, SafeSounds.C}),
    LVL_3_G(new SafeSounds[]{SafeSounds.K, SafeSounds.M, SafeSounds.O}, SafeSounds.Q, new SafeSounds[]{SafeSounds.K, SafeSounds.M, SafeSounds.O, SafeSounds.Q}, new SafeSounds[]{SafeSounds.P, SafeSounds.Q, SafeSounds.N, SafeSounds.R}),
    LVL_3_H(new SafeSounds[]{SafeSounds.THREE, SafeSounds.FIVE, SafeSounds.SEVEN}, SafeSounds.TEN, new SafeSounds[]{SafeSounds.THREE, SafeSounds.FIVE, SafeSounds.SEVEN, SafeSounds.TEN}, new SafeSounds[]{SafeSounds.NINE, SafeSounds.SIX, SafeSounds.EIGHT, SafeSounds.TEN}),

    //level 4
    LVL_4_A(new SafeSounds[]{SafeSounds.TEN, SafeSounds.FIVE, SafeSounds.NINE, SafeSounds.FOUR, SafeSounds.EIGHT}, SafeSounds.THREE, new SafeSounds[]{SafeSounds.TEN, SafeSounds.FIVE, SafeSounds.NINE, SafeSounds.FOUR, SafeSounds.EIGHT, SafeSounds.THREE}, new SafeSounds[]{SafeSounds.THREE, SafeSounds.SEVEN, SafeSounds.FOUR, SafeSounds.FIVE}),
    LVL_4_B(new SafeSounds[]{SafeSounds.A, SafeSounds.Z, SafeSounds.B, SafeSounds.Y, SafeSounds.C}, SafeSounds.X, new SafeSounds[]{SafeSounds.A, SafeSounds.Z, SafeSounds.B, SafeSounds.Y, SafeSounds.C, SafeSounds.X}, new SafeSounds[]{SafeSounds.D, SafeSounds.X, SafeSounds.W, SafeSounds.B}),
    LVL_4_C(new SafeSounds[]{SafeSounds.ONE, SafeSounds.TEN, SafeSounds.TWO, SafeSounds.NINE, SafeSounds.THREE}, SafeSounds.EIGHT, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TEN, SafeSounds.TWO, SafeSounds.NINE, SafeSounds.THREE, SafeSounds.EIGHT}, new SafeSounds[]{SafeSounds.FOUR, SafeSounds.SEVEN, SafeSounds.EIGHT, SafeSounds.NINE}),
    LVL_4_D(new SafeSounds[]{SafeSounds.J, SafeSounds.E, SafeSounds.I, SafeSounds.D, SafeSounds.H}, SafeSounds.C, new SafeSounds[]{SafeSounds.J, SafeSounds.E, SafeSounds.I, SafeSounds.D, SafeSounds.H, SafeSounds.C}, new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.C, SafeSounds.F}),
    LVL_4_E(new SafeSounds[]{SafeSounds.A, SafeSounds.E, SafeSounds.B, SafeSounds.D, SafeSounds.C}, SafeSounds.C, new SafeSounds[]{SafeSounds.A, SafeSounds.E, SafeSounds.B, SafeSounds.D, SafeSounds.C}, new SafeSounds[]{SafeSounds.D, SafeSounds.C, SafeSounds.F, SafeSounds.E}),
    LVL_4_F(new SafeSounds[]{SafeSounds.ONE, SafeSounds.FIVE, SafeSounds.TWO, SafeSounds.FOUR, SafeSounds.THREE}, SafeSounds.THREE, new SafeSounds[]{SafeSounds.ONE, SafeSounds.FIVE, SafeSounds.TWO, SafeSounds.FOUR, SafeSounds.THREE}, new SafeSounds[]{SafeSounds.THREE, SafeSounds.FOUR, SafeSounds.FIVE, SafeSounds.ONE}),
    LVL_4_G(new SafeSounds[]{SafeSounds.THREE, SafeSounds.FOUR, SafeSounds.TWO, SafeSounds.FIVE, SafeSounds.ONE}, SafeSounds.SIX, new SafeSounds[]{SafeSounds.THREE, SafeSounds.FOUR, SafeSounds.TWO, SafeSounds.FIVE, SafeSounds.ONE, SafeSounds.SIX}, new SafeSounds[]{SafeSounds.SIX, SafeSounds.TWO, SafeSounds.THREE, SafeSounds.SEVEN}),
    LVL_4_H(new SafeSounds[]{SafeSounds.C, SafeSounds.D, SafeSounds.B, SafeSounds.E, SafeSounds.A}, SafeSounds.F, new SafeSounds[]{SafeSounds.C, SafeSounds.D, SafeSounds.B, SafeSounds.E, SafeSounds.A, SafeSounds.F}, new SafeSounds[]{SafeSounds.F, SafeSounds.G, SafeSounds.B, SafeSounds.C});


    private SafeSounds[] soundSequence;
    private SafeSounds expected;
    private SafeSounds[] soundsToSequence;
    private SafeSounds[] soundsToLights;

    SafeMediumLevelDefinition(SafeSounds[] soundSequence, SafeSounds expected, SafeSounds[] soundsToSequence, SafeSounds[] soundsToLights) {
        this.soundSequence = soundSequence;
        this.expected = expected;
        this.soundsToSequence = soundsToSequence;
        this.soundsToLights = soundsToLights;
    }

    public SafeSounds[] getSoundSequence() {
        return soundSequence;
    }

    public SafeSounds getExpectedSound() {
        return expected;
    }

    public SafeSounds[] getSoundsToSequence() {
        return soundsToSequence;
    }

    public SafeSounds[] getSoundsToLights() {
        return soundsToLights;
    }

    public int getPlayAgainFirstXSounds() {
        return 0;
    }

    /**
     * @return possible sequences for round 1
     */
    private static List<SafeMediumLevelDefinition> getSequencesForFirstLevel() {
        List<SafeMediumLevelDefinition> listOfSequences = new ArrayList<SafeMediumLevelDefinition>();
        listOfSequences.add(LVL_1_A);
        listOfSequences.add(LVL_1_B);
        listOfSequences.add(LVL_1_C);
        listOfSequences.add(LVL_1_D);
        listOfSequences.add(LVL_1_E);
        listOfSequences.add(LVL_1_F);
        listOfSequences.add(LVL_1_G);
        listOfSequences.add(LVL_1_H);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 2
     */
    private static List<SafeMediumLevelDefinition> getSequencesForSecondLevel() {
        List<SafeMediumLevelDefinition> listOfSequences = new ArrayList<SafeMediumLevelDefinition>();
        listOfSequences.add(LVL_2_A);
        listOfSequences.add(LVL_2_B);
        listOfSequences.add(LVL_2_C);
        listOfSequences.add(LVL_2_D);
        listOfSequences.add(LVL_2_E);
        listOfSequences.add(LVL_2_F);
        listOfSequences.add(LVL_2_G);
        listOfSequences.add(LVL_2_H);
        listOfSequences.add(LVL_2_I);
        listOfSequences.add(LVL_2_J);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 3
     */
    private static List<SafeMediumLevelDefinition> getSequencesForThirdLevel() {
        List<SafeMediumLevelDefinition> listOfSequences = new ArrayList<SafeMediumLevelDefinition>();
        listOfSequences.add(LVL_3_A);
        listOfSequences.add(LVL_3_B);
        listOfSequences.add(LVL_3_C);
        listOfSequences.add(LVL_3_D);
        listOfSequences.add(LVL_3_E);
        listOfSequences.add(LVL_3_F);
        listOfSequences.add(LVL_3_G);
        listOfSequences.add(LVL_3_H);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 4
     */
    private static List<SafeMediumLevelDefinition> getSequencesForFourthLevel() {
        List<SafeMediumLevelDefinition> listOfSequences = new ArrayList<SafeMediumLevelDefinition>();
        listOfSequences.add(LVL_4_A);
        listOfSequences.add(LVL_4_B);
        listOfSequences.add(LVL_4_C);
        listOfSequences.add(LVL_4_D);
        listOfSequences.add(LVL_4_E);
        listOfSequences.add(LVL_4_F);
        listOfSequences.add(LVL_4_G);
        listOfSequences.add(LVL_4_H);
        return listOfSequences;
    }

    private static List<SafeSequence> makeListOfSequences(List<SafeMediumLevelDefinition> possibilitiesList) {
        List<SafeSequence> listOfSequences = new ArrayList<>();
        for (SafeMediumLevelDefinition easySequence : possibilitiesList) {
            listOfSequences.add(new SafeSequence(easySequence.getSoundSequence(), easySequence.getSoundsToSequence(), easySequence.getSoundsToLights(), easySequence.getExpectedSound(), easySequence.getPlayAgainFirstXSounds()));
        }
        return listOfSequences;
    }

    /**
     * Make list of sound sequences from SafeMediumLevelDefinition for whole levels
     *
     * @return list of sound sequence for for whole levels
     */
    public static ArrayList<List<SafeSequence>> makeArrayOfListOfSequences() {
        ArrayList<List<SafeSequence>> arrayOfListOfSequences = new ArrayList<List<SafeSequence>>();
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForFirstLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForSecondLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForThirdLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForFourthLevel()));
        return arrayOfListOfSequences;
    }
}
