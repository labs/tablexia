/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.games.safe.gameobject.SafeSounds;

/**
 * Created by Aneta Steimarová on 17.5.17.
 */

public enum SafeEasyLevelDefinition implements SafeLevelDefinition {

    //level 1
    LVL_1_A(new int[]{1, 2, 0}, 2, 1, 2),
    LVL_1_B(new int[]{1, 1, 2, 2, 0}, 1, 1, 2),
    LVL_1_C(new int[]{1, 2, 0}, 1, 2, 2),
    LVL_1_D(new int[]{1, 1, 2, 2, 0}, 3, 2, 2),
    LVL_1_E(new int[]{1, 1, 2, 2, 0}, 2, 2, 2),
    LVL_1_F(new int[]{1, 1, 2, 2, 0}, 4, 1, 2),

    //level 2
    LVL_2_A(new int[]{1, 2, 2, 0}, 2, 2, 2),
    LVL_2_B(new int[]{1, 2, 1, 0}, 2, 1, 2),
    LVL_2_C(new int[]{1, 1, 2, 0}, 2, 2, 2),
    LVL_2_D(new int[]{1, 2, 2, 0}, 3, 1, 2),
    LVL_2_E(new int[]{1, 2, 1, 0}, 3, 1, 2),
    LVL_2_F(new int[]{1, 1, 2, 0}, 3, 1, 2),

    //level 3
    LVL_3_A(new int[]{1, 2, 3, 1, 0}, 1, 2, 3),
    LVL_3_B(new int[]{1, 2, 3, 1, 0}, 2, 3, 3),
    LVL_3_C(new int[]{1, 2, 3, 1, 0}, 3, 1, 3),
    LVL_3_D(new int[]{1, 2, 2, 3, 0}, 1, 2, 3),
    LVL_3_E(new int[]{1, 2, 2, 3, 0}, 2, 2, 3),
    LVL_3_F(new int[]{1, 2, 2, 3, 0}, 3, 3, 3),
    LVL_3_G(new int[]{1, 1, 2, 3, 0}, 1, 1, 3),
    LVL_3_H(new int[]{1, 1, 2, 3, 0}, 2, 2, 3),
    LVL_3_I(new int[]{1, 1, 2, 3, 0}, 3, 3, 3),

    //level 4; 0 = space - signalize end
    LVL_4_A(new int[]{1, 2, 3, 0, 1, 1, 2, 2, 3}, 0, 3, 3),
    LVL_4_B(new int[]{1, 2, 2, 3, 0, 1, 2, 2, 2, 3, 0, 1, 2, 2, 2}, 0, 2, 3),
    LVL_4_C(new int[]{1, 1, 2, 3, 0, 1, 1, 1, 2, 3, 0, 1, 1, 1}, 0, 1, 3),
    LVL_4_D(new int[]{1, 1, 1, 2, 3, 0, 1, 1, 2, 2, 3, 0, 1,}, 0, 2, 3),
    LVL_4_E(new int[]{1, 1, 1, 2, 3, 0, 1, 1, 2, 2, 3, 0, 1, 2, 2, 2}, 0, 3, 3),
    LVL_4_F(new int[]{1, 2, 3, 0, 1, 2, 3, 3, 0, 1, 2, 3, 3}, 0, 3, 3),
    LVL_4_G(new int[]{1, 1, 1, 2, 2, 2, 3, 0, 1, 1, 2, 2, 3, 0, 1, 2}, 0, 3, 3);

    private int[] soundSequence;
    private int playAgainFirstXSounds;
    private int expected;
    private int numberOfSoundsInSequence;
    private SafeSounds[] randomPossibleSounds;

    SafeEasyLevelDefinition(int[] soundSequence, int playAgainFirstXSounds, int expected, int numberOfSoundsInSequence) {
        this.soundSequence = soundSequence;
        this.playAgainFirstXSounds = playAgainFirstXSounds;
        this.expected = expected;
        this.numberOfSoundsInSequence = numberOfSoundsInSequence;
        randomPossibleSounds = shakeSounds(new SafeSounds[]{SafeSounds.BELL, SafeSounds.CYMBAL, SafeSounds.DRUM, SafeSounds.GUITAR_1,
                SafeSounds.GUITAR_2, SafeSounds.HI_HAT, SafeSounds.PIANO_1, SafeSounds.PIANO_2, SafeSounds.POT, SafeSounds.RATTLE,
                SafeSounds.SAX, SafeSounds.SYNTH, SafeSounds.TRIANGLE, SafeSounds.WOOD});
    }

    private SafeSounds[] shakeSounds(SafeSounds[] possibleSounds) {
        Random random = new Random();
        List<SafeSounds> listOfSounds = new ArrayList<SafeSounds>(Arrays.asList(possibleSounds));
        SafeSounds[] randomSoundsToSequence = new SafeSounds[possibleSounds.length];

        int numberOfSounds = possibleSounds.length;

        for (int i = 0; i < numberOfSounds; i++) {
            randomSoundsToSequence[i] = listOfSounds.get(random.nextInt(listOfSounds.size()));
            listOfSounds.remove(randomSoundsToSequence[i]);
        }

        return randomSoundsToSequence;
    }

    public SafeSounds[] getSoundSequence() {
        SafeSounds[] generatedSoundSequence = new SafeSounds[soundSequence.length];
        int i = 0;
        for (int soundNumber : soundSequence) {
            if (soundNumber == 0) generatedSoundSequence[i] = SafeSounds.SILENCE;
            else generatedSoundSequence[i] = randomPossibleSounds[soundNumber - 1];
            i++;
        }
        return generatedSoundSequence;
    }

    /**
     * Return array of sounds used in generated sequence
     *
     * @return array of sounds, that are used in sequence
     */
    public SafeSounds[] getSoundsToSequence() {
        SafeSounds[] soundsToSequence = new SafeSounds[numberOfSoundsInSequence];

        for (int i = 0; i < numberOfSoundsInSequence; i++) {
            soundsToSequence[i] = randomPossibleSounds[i];
        }
        return soundsToSequence;
    }

    public SafeSounds[] getSoundsToLights() {
        return getSoundsToSequence();
    }

    public int getPlayAgainFirstXSounds() {
        return playAgainFirstXSounds;
    }

    public SafeSounds getExpectedSound() {
        return randomPossibleSounds[expected - 1];
    }

    /**
     * @return possible sequences for round 1
     */
    private static List<SafeEasyLevelDefinition> getSequencesForFirstLevel() {
        List<SafeEasyLevelDefinition> listOfSequences = new ArrayList<SafeEasyLevelDefinition>();
        listOfSequences.add(LVL_1_A);
        listOfSequences.add(LVL_1_B);
        listOfSequences.add(LVL_1_C);
        listOfSequences.add(LVL_1_D);
        listOfSequences.add(LVL_1_E);
        listOfSequences.add(LVL_1_F);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 2
     */
    private static List<SafeEasyLevelDefinition> getSequencesForSecondLevel() {
        List<SafeEasyLevelDefinition> listOfSequences = new ArrayList<SafeEasyLevelDefinition>();
        listOfSequences.add(LVL_2_A);
        listOfSequences.add(LVL_2_B);
        listOfSequences.add(LVL_2_C);
        listOfSequences.add(LVL_2_D);
        listOfSequences.add(LVL_2_E);
        listOfSequences.add(LVL_2_F);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 3
     */
    private static List<SafeEasyLevelDefinition> getSequencesForThirdLevel() {
        List<SafeEasyLevelDefinition> listOfSequences = new ArrayList<SafeEasyLevelDefinition>();
        listOfSequences.add(LVL_3_A);
        listOfSequences.add(LVL_3_B);
        listOfSequences.add(LVL_3_C);
        listOfSequences.add(LVL_3_D);
        listOfSequences.add(LVL_3_E);
        listOfSequences.add(LVL_3_F);
        listOfSequences.add(LVL_3_G);
        listOfSequences.add(LVL_3_H);
        listOfSequences.add(LVL_3_I);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 4
     */
    private static List<SafeEasyLevelDefinition> getSequencesForFourthLevel() {
        List<SafeEasyLevelDefinition> listOfSequences = new ArrayList<SafeEasyLevelDefinition>();
        listOfSequences.add(LVL_4_A);
        listOfSequences.add(LVL_4_B);
        listOfSequences.add(LVL_4_C);
        listOfSequences.add(LVL_4_D);
        listOfSequences.add(LVL_4_E);
        listOfSequences.add(LVL_4_F);
        listOfSequences.add(LVL_4_G);
        return listOfSequences;
    }

    private static List<SafeSequence> makeListOfSequences(List<SafeEasyLevelDefinition> possibilitiesList) {
        List<SafeSequence> listOfSequences = new ArrayList<>();
        for (SafeEasyLevelDefinition easySequence : possibilitiesList) {
            listOfSequences.add(new SafeSequence(easySequence.getSoundSequence(), easySequence.getSoundsToSequence(), easySequence.getSoundsToLights(), easySequence.getExpectedSound(), easySequence.getPlayAgainFirstXSounds()));
        }
        return listOfSequences;
    }

    /**
     * Make list of sound sequences from SafeEasyLevelDefinition for whole levels
     *
     * @return list of sound sequence for for whole levels
     */
    public static ArrayList<List<SafeSequence>> makeArrayOfListOfSequences() {
        ArrayList<List<SafeSequence>> arrayOfListOfSequences = new ArrayList<List<SafeSequence>>();
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForFirstLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForSecondLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForThirdLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForFourthLevel()));
        return arrayOfListOfSequences;
    }
}
