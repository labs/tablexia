/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.safe;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.safe.assets.SafeAssets;
import cz.nic.tablexia.game.games.safe.gameobject.CableGroup;
import cz.nic.tablexia.game.games.safe.gameobject.CableTypeDefinition;
import cz.nic.tablexia.game.games.safe.gameobject.SafeLightImage;
import cz.nic.tablexia.game.games.safe.model.SafeSequence;

/**
 * Created by Aneta Steimarová on 12.7.17.
 */

public class SafeMechanicsMedium extends AbstractMechanics {
    public static final  String CONNECTOR_NAME      = "connector";
    public static final  String FINAL_SOCKET_NAME   = "final socket";
    private static final int    FINAL_SOCKET_WIDTH  = 180;
    private static final int    FINAL_SOCKET_HEIGHT = 220;

    private List<SafeLightImage> listOfLights;
    private List<CableGroup> cableGroups;

    private Image finalSocket;
    private Rectangle finalSocketDetect;

    private int socketCableID; // -1 empty : >= 0 cable ID

    @Override
    public void dispose() {
        super.dispose();

        if (cableGroups != null) {
            for (CableGroup cableGroup : cableGroups) {
                cableGroup.dispose();
            }
        }
    }

    @Override
    public void setSafeActorsUntouchable() {
        for (CableGroup cableGroup : cableGroups) {
            cableGroup.setAllUntouchable();
        }
    }

    @Override
    public void setSafeActorsTouchable() {
        for (CableGroup cableGroup : cableGroups) {
            cableGroup.setAllTouchable();
        }
    }

    @Override
    public void setDoneButtonDisOrEnabled(SafeGame safeGame) {
        for (CableGroup cableGroup : cableGroups) {
            if (cableGroup.isInSocket()) {
                safeGame.getDoneButton().setEnabled();
            } else if (cableGroup.getIfIsDragged()) cableGroup.startBackAnimation();
        }
    }

    @Override
    public void doBeforeEvaluatingAnimation(SequenceAction sa) {}

    @Override
    void prepare(SafeGame safeGame, SafeSequence safeSequence) {
        safeGame.setSelectedLight(null, listOfLights, false);

        socketCableID = -1;

        listOfLights = null;
        cableGroups = null;

        finalSocket = new Image(safeGame.getScreenTextureRegion(SafeAssets.SOCKET_FINAL));
        finalSocket.setSize(FINAL_SOCKET_WIDTH, FINAL_SOCKET_HEIGHT);
        finalSocket.setPosition(getWidth() - FINAL_SOCKET_WIDTH / 1.1f, getHeight() / 2 - finalSocket.getHeight() / 2);
        finalSocket.setName(FINAL_SOCKET_NAME);
        addActor(finalSocket);

        finalSocketDetect = new Rectangle(
                finalSocket.getX(),
                finalSocket.getY() + finalSocket.getHeight() / 3,
                finalSocket.getWidth(),
                finalSocket.getHeight() / 3);

        int numberOfCables = safeSequence.getSoundsToLights().length;
        float spacing = (getHeight() / numberOfCables);
        float firstposY = spacing / 2;

        listOfLights = new ArrayList<>();
        cableGroups = new ArrayList<>();

        for (int i = 0; i < numberOfCables; i++) {
            CableTypeDefinition cableTypeDefinition = CableTypeDefinition.values()[i];
            CableGroup cableGroup = new CableGroup(i, this, safeGame,
                    safeGame.getScreenTextureRegion(cableTypeDefinition.getTerm()),
                    safeGame.getScreenTextureRegion(cableTypeDefinition.getSocket()),
                    safeGame.getScreenTextureRegion(SafeAssets.CABLE),
                    cableTypeDefinition.getCableColor(),
                    cableTypeDefinition.getLightGrey(),
                    cableTypeDefinition.getLightRed(),
                    cableTypeDefinition.getLightGreen(),
                    cableTypeDefinition.getLightBlue(),
                    safeGame.getMusic(safeSequence.getSoundsToLights()[i].getSoundPath()),
                    listOfLights, LIGHT_NAME, CONNECTOR_NAME);
            cableGroup.setComponentsPosition(0, i * spacing + (spacing / 2 - cableGroup.getHeight() / 2));
            cableGroup.setCableLimit(new Vector2(getWidth(), getHeight()));

            addActor(cableGroup);
            cableGroups.add(cableGroup);
        }
    }

    public void setMinZIndexForSocket() {
        int minCable = getMinCableZIndex();
        int socketIndex = finalSocket.getZIndex();
        finalSocket.setZIndex(cableGroups.get(minCable).getZIndex());
        cableGroups.get(minCable).setZIndex(socketIndex);
    }

    public void setMinZIndexForCable(int cabelIndex) {
        CableGroup cableSocket = cableGroups.get(cabelIndex);
        int minCable = getMinCableZIndex();

        if (minCable == -1)
            return;

        int socketIndex = finalSocket.getZIndex();

        if (socketCableID != -1 && socketCableID != cabelIndex) {
            finalSocket.setZIndex(cableGroups.get(minCable).getZIndex());
            cableGroups.get(minCable).setZIndex(socketIndex);
            cableGroups.get(socketCableID).setInSocket(false);
            cableGroups.get(socketCableID).startBackAnimation();
        }

        if (minCable == cabelIndex) {
            finalSocket.setZIndex(cableSocket.getZIndex());
            cableSocket.setZIndex(socketIndex);
        } else {

            CableGroup cableMin = cableGroups.get(minCable);

            int minCableIndex = cableMin.getZIndex();
            cableMin.setZIndex(cableSocket.getZIndex());
            cableSocket.setZIndex(finalSocket.getZIndex());
            finalSocket.setZIndex(minCableIndex);
        }

    }

    private int getMinCableZIndex() {
        int minimum = -1;
        int id = -1;
        for (CableGroup cableGroup : cableGroups) {
            if (minimum == -1 || minimum > cableGroup.getZIndex()) {
                minimum = cableGroup.getZIndex();
                id = cableGroup.getId();
            }
        }

        return id;
    }

    public Rectangle getFinalSocketDetect() {
        return finalSocketDetect;
    }

    public void setSocketCableID(int socketCableID) {
        this.socketCableID = socketCableID;
    }
}
