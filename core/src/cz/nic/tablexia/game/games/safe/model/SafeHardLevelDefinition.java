/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.model;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.safe.gameobject.SafeSounds;

/**
 * Created by Aneta Steimarová on 1.6.17.
 */

public enum SafeHardLevelDefinition implements SafeLevelDefinition {
    //level 1
    LVL_1_A(new SafeSounds[]{SafeSounds.ONE, SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.THREE}, SafeSounds.C, new SafeSounds[]{SafeSounds.ONE, SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.THREE, SafeSounds.C}, new SafeSounds[]{SafeSounds.C, SafeSounds.FOUR}),
    LVL_1_B(new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.C}, SafeSounds.THREE, new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.C, SafeSounds.THREE}, new SafeSounds[]{SafeSounds.THREE, SafeSounds.B}),
    LVL_1_C(new SafeSounds[]{SafeSounds.F, SafeSounds.SIX, SafeSounds.E, SafeSounds.FIVE, SafeSounds.D}, SafeSounds.FOUR, new SafeSounds[]{SafeSounds.F, SafeSounds.SIX, SafeSounds.E, SafeSounds.FIVE, SafeSounds.D, SafeSounds.FOUR}, new SafeSounds[]{SafeSounds.FOUR, SafeSounds.C}),
    LVL_1_D(new SafeSounds[]{SafeSounds.SIX, SafeSounds.F, SafeSounds.FIVE, SafeSounds.E, SafeSounds.FOUR}, SafeSounds.D, new SafeSounds[]{SafeSounds.SIX, SafeSounds.F, SafeSounds.FIVE, SafeSounds.E, SafeSounds.FOUR, SafeSounds.D}, new SafeSounds[]{SafeSounds.D, SafeSounds.THREE}),
    LVL_1_E(new SafeSounds[]{SafeSounds.ONE, SafeSounds.A, SafeSounds.TWO, SafeSounds.B}, SafeSounds.THREE, new SafeSounds[]{SafeSounds.ONE, SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.THREE}, new SafeSounds[]{SafeSounds.THREE, SafeSounds.C}),
    LVL_1_F(new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO}, SafeSounds.C, new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.C}, new SafeSounds[]{SafeSounds.THREE, SafeSounds.C}),


    //level 2
    LVL_2_A(new SafeSounds[]{SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.D, SafeSounds.THREE, SafeSounds.F, SafeSounds.FOUR}, SafeSounds.H, new SafeSounds[]{SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.D, SafeSounds.THREE, SafeSounds.F, SafeSounds.FOUR, SafeSounds.H}, new SafeSounds[]{SafeSounds.H, SafeSounds.G, SafeSounds.FIVE}),
    LVL_2_B(new SafeSounds[]{SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.FOUR, SafeSounds.C, SafeSounds.SIX, SafeSounds.D}, SafeSounds.EIGHT, new SafeSounds[]{SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.FOUR, SafeSounds.C, SafeSounds.SIX, SafeSounds.D, SafeSounds.EIGHT}, new SafeSounds[]{SafeSounds.SEVEN, SafeSounds.EIGHT, SafeSounds.E}),
    LVL_2_C(new SafeSounds[]{SafeSounds.ONE, SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.B, SafeSounds.THREE, SafeSounds.C, SafeSounds.C}, SafeSounds.C, new SafeSounds[]{SafeSounds.ONE, SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.THREE, SafeSounds.C}, new SafeSounds[]{SafeSounds.C, SafeSounds.FOUR, SafeSounds.ONE}),
    LVL_2_D(new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.TWO, SafeSounds.C, SafeSounds.THREE, SafeSounds.THREE}, SafeSounds.THREE, new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.C, SafeSounds.THREE}, new SafeSounds[]{SafeSounds.THREE, SafeSounds.FOUR, SafeSounds.D}),
    LVL_2_E(new SafeSounds[]{SafeSounds.A, SafeSounds.TEN, SafeSounds.B, SafeSounds.NINE, SafeSounds.C, SafeSounds.EIGHT, SafeSounds.D}, SafeSounds.SEVEN, new SafeSounds[]{SafeSounds.A, SafeSounds.TEN, SafeSounds.B, SafeSounds.NINE, SafeSounds.C, SafeSounds.EIGHT, SafeSounds.D, SafeSounds.SEVEN}, new SafeSounds[]{SafeSounds.SEVEN, SafeSounds.E, SafeSounds.SIX}),
    LVL_2_F(new SafeSounds[]{SafeSounds.ONE, SafeSounds.Z, SafeSounds.TWO, SafeSounds.Y, SafeSounds.THREE, SafeSounds.X, SafeSounds.FOUR}, SafeSounds.W, new SafeSounds[]{SafeSounds.ONE, SafeSounds.Z, SafeSounds.TWO, SafeSounds.Y, SafeSounds.THREE, SafeSounds.X, SafeSounds.FOUR, SafeSounds.W}, new SafeSounds[]{SafeSounds.FIVE, SafeSounds.W, SafeSounds.V}),

    //level 3
    LVL_3_A(new SafeSounds[]{SafeSounds.D, SafeSounds.B, SafeSounds.C, SafeSounds.A, SafeSounds.FOUR, SafeSounds.TWO, SafeSounds.THREE}, SafeSounds.ONE, new SafeSounds[]{SafeSounds.D, SafeSounds.C, SafeSounds.B, SafeSounds.A, SafeSounds.FOUR, SafeSounds.THREE, SafeSounds.TWO, SafeSounds.ONE}, new SafeSounds[]{SafeSounds.D, SafeSounds.FOUR, SafeSounds.ONE}),
    LVL_3_B(new SafeSounds[]{SafeSounds.FIVE, SafeSounds.THREE, SafeSounds.FOUR, SafeSounds.TWO, SafeSounds.F, SafeSounds.C, SafeSounds.D}, SafeSounds.B, new SafeSounds[]{SafeSounds.FIVE, SafeSounds.THREE, SafeSounds.FOUR, SafeSounds.TWO, SafeSounds.F, SafeSounds.C, SafeSounds.D, SafeSounds.B}, new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.ONE}),
    LVL_3_C(new SafeSounds[]{SafeSounds.TEN, SafeSounds.EIGHT, SafeSounds.SIX, SafeSounds.F, SafeSounds.D}, SafeSounds.B, new SafeSounds[]{SafeSounds.TEN, SafeSounds.EIGHT, SafeSounds.SIX, SafeSounds.F, SafeSounds.D, SafeSounds.B}, new SafeSounds[]{SafeSounds.SEVEN, SafeSounds.G, SafeSounds.B}),
    LVL_3_D(new SafeSounds[]{SafeSounds.A, SafeSounds.C, SafeSounds.F, SafeSounds.ONE, SafeSounds.THREE}, SafeSounds.SIX, new SafeSounds[]{SafeSounds.A, SafeSounds.C, SafeSounds.F, SafeSounds.ONE, SafeSounds.THREE, SafeSounds.SIX}, new SafeSounds[]{SafeSounds.EIGHT, SafeSounds.SEVEN, SafeSounds.SIX}),
    LVL_3_E(new SafeSounds[]{SafeSounds.C, SafeSounds.F, SafeSounds.I, SafeSounds.THREE, SafeSounds.SIX}, SafeSounds.NINE, new SafeSounds[]{SafeSounds.C, SafeSounds.F, SafeSounds.I, SafeSounds.THREE, SafeSounds.SIX, SafeSounds.NINE}, new SafeSounds[]{SafeSounds.SIX, SafeSounds.SEVEN, SafeSounds.NINE}),
    LVL_3_F(new SafeSounds[]{SafeSounds.TWO, SafeSounds.FOUR, SafeSounds.SIX, SafeSounds.B, SafeSounds.D}, SafeSounds.F, new SafeSounds[]{SafeSounds.TWO, SafeSounds.FOUR, SafeSounds.SIX, SafeSounds.B, SafeSounds.D, SafeSounds.F}, new SafeSounds[]{SafeSounds.F, SafeSounds.E, SafeSounds.G}),

    //level 4
    LVL_4_A(new SafeSounds[]{SafeSounds.ONE, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.TWO, SafeSounds.D, SafeSounds.THREE, SafeSounds.THREE}, SafeSounds.F, new SafeSounds[]{SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.D, SafeSounds.THREE, SafeSounds.F}, new SafeSounds[]{SafeSounds.G, SafeSounds.F, SafeSounds.FOUR, SafeSounds.FIVE}),
    LVL_4_B(new SafeSounds[]{SafeSounds.A, SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.B, SafeSounds.THREE, SafeSounds.C, SafeSounds.C}, SafeSounds.FIVE, new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.THREE, SafeSounds.C, SafeSounds.FIVE}, new SafeSounds[]{SafeSounds.FOUR, SafeSounds.FIVE, SafeSounds.D, SafeSounds.C}),
    LVL_4_C(new SafeSounds[]{SafeSounds.TWO, SafeSounds.Z, SafeSounds.FOUR, SafeSounds.X, SafeSounds.SIX}, SafeSounds.V, new SafeSounds[]{SafeSounds.TWO, SafeSounds.Z, SafeSounds.FOUR, SafeSounds.X, SafeSounds.SIX, SafeSounds.V}, new SafeSounds[]{SafeSounds.V, SafeSounds.W, SafeSounds.EIGHT, SafeSounds.SEVEN}),
    LVL_4_D(new SafeSounds[]{SafeSounds.A, SafeSounds.TEN, SafeSounds.C, SafeSounds.EIGHT, SafeSounds.E}, SafeSounds.SIX, new SafeSounds[]{SafeSounds.A, SafeSounds.TEN, SafeSounds.C, SafeSounds.EIGHT, SafeSounds.E, SafeSounds.SIX}, new SafeSounds[]{SafeSounds.SIX, SafeSounds.D, SafeSounds.SEVEN, SafeSounds.F}),
    LVL_4_E(new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.I, SafeSounds.THREE, SafeSounds.FOUR, SafeSounds.J, SafeSounds.FIVE, SafeSounds.SIX}, SafeSounds.K, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.I, SafeSounds.THREE, SafeSounds.FOUR, SafeSounds.J, SafeSounds.FIVE, SafeSounds.SIX, SafeSounds.K}, new SafeSounds[]{SafeSounds.SEVEN, SafeSounds.EIGHT, SafeSounds.K, SafeSounds.L}),
    LVL_4_F(new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.FIVE, SafeSounds.C, SafeSounds.D, SafeSounds.SIX, SafeSounds.E, SafeSounds.F}, SafeSounds.SEVEN, new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.FIVE, SafeSounds.C, SafeSounds.D, SafeSounds.SIX, SafeSounds.E, SafeSounds.F, SafeSounds.SEVEN}, new SafeSounds[]{SafeSounds.SEVEN, SafeSounds.EIGHT, SafeSounds.I, SafeSounds.H});

    private SafeSounds[] soundSequence;
    private SafeSounds expected;
    private SafeSounds[] soundsToSequence;
    private SafeSounds[] soundsToLights;

    SafeHardLevelDefinition(SafeSounds[] soundSequence, SafeSounds expected, SafeSounds[] soundsToSequence, SafeSounds[] soundsToLights) {
        this.soundSequence = soundSequence;
        this.expected = expected;
        this.soundsToSequence = soundsToSequence;
        this.soundsToLights = soundsToLights;
    }

    public SafeSounds[] getSoundSequence() {
        return soundSequence;
    }

    public SafeSounds getExpectedSound() {
        return expected;
    }

    public SafeSounds[] getSoundsToSequence() {
        return soundsToSequence;
    }

    public SafeSounds[] getSoundsToLights() {
        return soundsToLights;
    }

    public int getPlayAgainFirstXSounds() {
        return 0;
    }

    /**
     * @return possible sequences for round 1
     */
    private static List<SafeHardLevelDefinition> getSequencesForFirstLevel() {
        List<SafeHardLevelDefinition> listOfSequences = new ArrayList<SafeHardLevelDefinition>();
        listOfSequences.add(LVL_1_A);
        listOfSequences.add(LVL_1_B);
        listOfSequences.add(LVL_1_C);
        listOfSequences.add(LVL_1_D);
        listOfSequences.add(LVL_1_E);
        listOfSequences.add(LVL_1_F);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 2
     */
    private static List<SafeHardLevelDefinition> getSequencesForSecondLevel() {
        List<SafeHardLevelDefinition> listOfSequences = new ArrayList<SafeHardLevelDefinition>();
        listOfSequences.add(LVL_2_A);
        listOfSequences.add(LVL_2_B);
        listOfSequences.add(LVL_2_C);
        listOfSequences.add(LVL_2_D);
        listOfSequences.add(LVL_2_E);
        listOfSequences.add(LVL_2_F);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 3
     */
    private static List<SafeHardLevelDefinition> getSequencesForThirdLevel() {
        List<SafeHardLevelDefinition> listOfSequences = new ArrayList<SafeHardLevelDefinition>();
        listOfSequences.add(LVL_3_A);
        listOfSequences.add(LVL_3_B);
        listOfSequences.add(LVL_3_C);
        listOfSequences.add(LVL_3_D);
        listOfSequences.add(LVL_3_E);
        listOfSequences.add(LVL_3_F);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 4
     */
    private static List<SafeHardLevelDefinition> getSequencesForFourthLevel() {
        List<SafeHardLevelDefinition> listOfSequences = new ArrayList<SafeHardLevelDefinition>();
        listOfSequences.add(LVL_4_A);
        listOfSequences.add(LVL_4_B);
        listOfSequences.add(LVL_4_C);
        listOfSequences.add(LVL_4_D);
        listOfSequences.add(LVL_4_E);
        listOfSequences.add(LVL_4_F);
        return listOfSequences;
    }

    private static List<SafeSequence> makeListOfSequences(List<SafeHardLevelDefinition> possibilitiesList) {
        List<SafeSequence> listOfSequences = new ArrayList<>();
        for (SafeHardLevelDefinition easySequence : possibilitiesList) {
            listOfSequences.add(new SafeSequence(easySequence.getSoundSequence(), easySequence.getSoundsToSequence(), easySequence.getSoundsToLights(), easySequence.getExpectedSound(), easySequence.getPlayAgainFirstXSounds()));
        }
        return listOfSequences;
    }

    /**
     * Make list of sound sequences from SafeHardLevelDefinition for whole levels
     *
     * @return list of sound sequence for for whole levels
     */
    public static ArrayList<List<SafeSequence>> makeArrayOfListOfSequences() {
        ArrayList<List<SafeSequence>> arrayOfListOfSequences = new ArrayList<List<SafeSequence>>();
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForFirstLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForSecondLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForThirdLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForFourthLevel()));
        return arrayOfListOfSequences;
    }
}
