/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.actors;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.safe.SafeGame;
import cz.nic.tablexia.game.games.safe.gameobject.SafeSounds;
import cz.nic.tablexia.game.games.safe.model.SafeDifficulty;
import cz.nic.tablexia.game.games.safe.model.SafeSequence;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.PlayMusicAction;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by Aneta Steimarová on 15.5.17.
 */

public class SequenceGenerator extends Group {
    public static final String SCENARIO_STEP_SEQUENCE_PLAYED                = "sequence played";

    private static final ApplicationFontManager.FontType    FONT_TYPE       = ApplicationFontManager.FontType.REGULAR_30;
    private static final Color                              FONT_COLOR      = Color.WHITE;
    private static final Color                              PLAY_COLOR      = new Color(0x29abe1ff);    //baby blue
    private static final Color                              NORMAL_COLOR    = Color.BLACK;
    private static final float                              SILENCE_DELAY   = 2f;
    private static final float                              SEQUENCE_DELAY  = 1f;
    private static final float                              BONUS_DELAY     = 0.75f;

    private Image earImage, backgroundImage;
    private TablexiaLabel text;
    private static String soundSequence;

    public SequenceGenerator(String text, Texture backgroundTexture, TextureRegion earTexture) {
        this.addActor(backgroundImage = new Image(backgroundTexture));
        this.addActor(earImage = new Image(earTexture));
        this.addActor(this.text = new TablexiaLabel(text, new TablexiaLabel.TablexiaLabelStyle(FONT_TYPE, FONT_COLOR)));
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        backgroundImage.setSize(getWidth(), getHeight());
        earImage.setPosition(getWidth() / 2 - earImage.getWidth() / 2, getHeight() / 2 - earImage.getHeight() / 2);
        text.setPosition(getWidth() / 2 - text.getWidth() / 2, earImage.getY() - 2 * text.getHeight());
    }

    /**
     * Play sound sequence (no visualization)
     *
     * @param safeSequence generated sound sequence
     * @param safegame
     */
    public void playSequence(SafeSequence safeSequence, SafeGame safegame) {
        playSequenceVisualization(safeSequence, safegame, null, Actions.run(new Runnable() {
            @Override
            public void run() {
                SequenceGenerator.this.setVisible(false);
                safegame.getSafeGroup().setSafeActorsTouchable();
                AbstractTablexiaScreen.triggerScenarioStepEvent(SCENARIO_STEP_SEQUENCE_PLAYED);
            }
        }));
    }

    /**
     * Method for play (and visualize) sound sequence
     *
     * @param safeSequence generated sound sequence
     * @param safeGame
     * @param listOfChars  if null -> playing only sound sequence (no visualization); if not null -> it defines characters to visualization
     * @param afterAction  do this after sound sequence has been played
     */
    public void playSequenceVisualization(SafeSequence safeSequence, SafeGame safeGame, List<Actor> listOfChars, RunnableAction afterAction) {
        SequenceGenerator.this.addAction(Actions.sequence(Actions.delay(SEQUENCE_DELAY), soundSequence(safeSequence, safeGame, listOfChars)));
        SequenceGenerator.this.addAction(Actions.after(afterAction));
    }

    private static Action soundSequence(SafeSequence safeSequence, SafeGame safegame, List<Actor> listOfChars) {
        SequenceAction sa = Actions.sequence();
        List<Action> playMusicActionList = createMusicActionList(safeSequence, safegame);

        soundSequence = new String();
        int charNum = 0;
        //generated sequence
        for (SafeSounds safeSequenceSound : safeSequence.getGeneratedSoundSequence()) {
            charNum = addSound(sa, safeSequenceSound, charNum, listOfChars, playMusicActionList, safeSequence, safegame);
        }

        //repeat first x sounds
        for (int i = 0; i < safeSequence.getPlayAgainFirstXSounds(); i++) {
            SafeSounds safeSound = safeSequence.getGeneratedSoundSequence()[i];
            charNum = addSound(sa, safeSound, charNum, listOfChars, playMusicActionList, safeSequence, safegame);
        }
        Log.info("Sequence Generator", soundSequence);
        return sa;
    }

    private static List<Action> createMusicActionList(SafeSequence safeSequence, SafeGame safegame) {
        //actions to play !!!there always has to be only one in one level (but can be used more than one after reset)
        List<Action> playMusicActionList = new ArrayList<Action>();
        for (SafeSounds safeSound : safeSequence.getSoundsToSequence()) {
            Music music = safegame.getMusic(safeSound.getSoundPath());
            safegame.addMusicToMusicList(music);
            PlayMusicAction musicAction = PlayMusicAction.getInstance(music);
            musicAction.reset();
            playMusicActionList.add(musicAction);
        }
        return playMusicActionList;
    }


    private static int addSound(SequenceAction sa, SafeSounds safeSequenceSound, int charNum, List<Actor> listOfChars, List<Action> playMusicActionList, SafeSequence safeSequence, SafeGame safegame) {
        if (safeSequenceSound.equals(SafeSounds.SILENCE)) {
            sa.addAction(Actions.delay(SILENCE_DELAY));
            soundSequence += " - ";
        } else {
            soundSequence += safeSequenceSound.toString() + " ";
            if (listOfChars != null)
                addActionToSequence(safeSequenceSound, sa, playMusicActionList, safeSequence, listOfChars.get(charNum), safegame);
            else
                addActionToSequence(safeSequenceSound, sa, playMusicActionList, safeSequence, null, safegame);
            charNum++;
        }
        return charNum;
    }


    private static void addActionToSequence(SafeSounds safeSequenceSound, SequenceAction sa, List<Action> playMusicActionList, SafeSequence safeSequence, Actor character, SafeGame safegame) {
        Action playMusicAction = findActionForSafeSound(safeSequenceSound, playMusicActionList, safeSequence);
        if (playMusicAction == null)
            Log.err("ERROR", "Well...this should not happen. Try to look at addActionToSequence method in SequenceGenerator.");
        if (character != null) {
            sa.addAction(Actions.run(new Runnable() {
                @Override
                public void run() {
                    setCharacterColor(character, PLAY_COLOR);
                }
            }));
        }
        sa.addAction(Actions.run(new Runnable() {
            @Override
            public void run() {
                playMusicAction.reset();
            }
        }));
        sa.addAction(playMusicAction);
        if (safegame.getGameDifficulty().equals(GameDifficulty.BONUS)){
            sa.addAction(Actions.delay(BONUS_DELAY));
        }
        if (character != null) {
            sa.addAction(Actions.run(new Runnable() {
                @Override
                public void run() {
                    setCharacterColor(character, NORMAL_COLOR);
                }
            }));
        }
    }


    //change color while playing visualization
    private static void setCharacterColor(Actor character, Color color) {
        character.setColor(color);
    }


    public static Action findActionForSafeSound(SafeSounds safeSequenceSound, List<Action> playMusicActionList, SafeSequence safeSequence) {
        for (int i = 0; i < safeSequence.getSoundsToSequence().length; i++) {
            // find action - compare sound with
            if (safeSequenceSound.equals(safeSequence.getSoundsToSequence()[i])) {
                return playMusicActionList.get(i);
            }
        }
        return null;
    }

    public SafeSequence prepareSoundSequence(GameDifficulty gameDifficulty, int level) {

        //sequence rule
        return SafeDifficulty.getGeneratedSequence(gameDifficulty, level);
    }
}
