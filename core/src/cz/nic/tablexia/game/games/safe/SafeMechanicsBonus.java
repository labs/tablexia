/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.safe;

import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.safe.gameobject.BonusCableGroup;
import cz.nic.tablexia.game.games.safe.gameobject.BonusCableTypeDefinition;
import cz.nic.tablexia.game.games.safe.gameobject.SafeLightImage;
import cz.nic.tablexia.game.games.safe.model.SafeSequence;

/**
 * Created by Aneta Steimarová on 14.3.18.
 */

public class SafeMechanicsBonus extends AbstractMechanics {
    private List<SafeLightImage> listOfLights;
    private List<BonusCableGroup> cableGroups;

    @Override
    void prepare(SafeGame safeGame, SafeSequence safeSequence) {
        int numberOfCables = safeSequence.getSoundsToLights().length;

        listOfLights = new ArrayList<>();
        cableGroups = new ArrayList<>();

        float [] spacing2 = {0.2f, 0.6f};
        float [] spacing3 = {0.1f, 0.4f, 0.7f};
        float [] spacing4 = {0.05f, 0.3f, 0.55f, 0.8f};

        for (int i = 0; i < numberOfCables; i++) {
            BonusCableTypeDefinition bonusCableTypeDefinition = BonusCableTypeDefinition.values()[i];
            BonusCableGroup cableGroup = new BonusCableGroup(i, safeGame,
                    safeGame.getScreenTextureRegion(bonusCableTypeDefinition.getSocket()),
                    bonusCableTypeDefinition.getSocketPercentReduction(),
                    safeGame.getScreenTextureRegion(bonusCableTypeDefinition.getCable()),
                    safeGame.getScreenTextureRegion(bonusCableTypeDefinition.getCutCable()),
                    safeGame.getScreenTextureRegion(bonusCableTypeDefinition.getFinalSocket()),
                    bonusCableTypeDefinition.getFinalSocketPercentReduction(),
                    bonusCableTypeDefinition.getLightGrey(),
                    bonusCableTypeDefinition.getLightRed(),
                    bonusCableTypeDefinition.getLightGreen(),
                    bonusCableTypeDefinition.getLightBlue(),
                    bonusCableTypeDefinition.getLightSize(),
                    safeGame.getMusic(safeSequence.getSoundsToLights()[i].getSoundPath()),
                    listOfLights, LIGHT_NAME);
            float [] yspacing;
            if(numberOfCables==2) yspacing = spacing2;
            else if(numberOfCables == 3) yspacing = spacing3;
            else yspacing = spacing4;
            cableGroup.setComponentsPosition(0, yspacing[i] * getHeight(), getWidth(),
                    bonusCableTypeDefinition.getLightX(), bonusCableTypeDefinition.getLightY(), bonusCableTypeDefinition.getStartPosX(),
                    bonusCableTypeDefinition.getEndPosX());

            addActor(cableGroup);
            cableGroups.add(cableGroup);
        }
    }

    @Override
    public void setSafeActorsUntouchable() {
        for (BonusCableGroup cableGroup : cableGroups) {
            cableGroup.setAllUntouchable();
        }
    }

    @Override
    public void setSafeActorsTouchable() {
        for (BonusCableGroup cableGroup: cableGroups) {
            cableGroup.setAllTouchable();
        }
    }

    @Override
    public void setDoneButtonDisOrEnabled(SafeGame safeGame) {
    }

    public void doBeforeEvaluatingAnimation(SequenceAction sa) {
        for (BonusCableGroup cableGroup: cableGroups) {
            if(cableGroup.getLightImage().equals(SafeGame.getSelectedLight())){
                cableGroup.cut(sa);
            }
        }
    }
}
