/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.gameobject;

import cz.nic.tablexia.game.games.safe.assets.SafeAssets;

/**
 * Created by Aneta Steimarová on 28.7.17.
 */

public enum SafeLightHardDefinition {
    TYPE_1(SafeAssets.HARD_LIGHT1, SafeAssets.MEDIUM_HARD_LIGHT1_BLUE, SafeAssets.MEDIUM_HARD_LIGHT1_GREEN, SafeAssets.MEDIUM_HARD_LIGHT1_GREY, SafeAssets.MEDIUM_HARD_LIGHT1_RED, 70),
    TYPE_2(SafeAssets.HARD_LIGHT2, SafeAssets.MEDIUM_HARD_LIGHT2_BLUE, SafeAssets.MEDIUM_HARD_LIGHT2_GREEN, SafeAssets.MEDIUM_HARD_LIGHT2_GREY, SafeAssets.MEDIUM_HARD_LIGHT2_RED, 110),
    TYPE_3(SafeAssets.HARD_LIGHT3, SafeAssets.MEDIUM_HARD_LIGHT3_BLUE, SafeAssets.MEDIUM_HARD_LIGHT3_GREEN, SafeAssets.MEDIUM_HARD_LIGHT3_GREY, SafeAssets.MEDIUM_HARD_LIGHT3_RED, 35),
    TYPE_4(SafeAssets.HARD_LIGHT4, SafeAssets.MEDIUM_HARD_LIGHT4_BLUE, SafeAssets.MEDIUM_HARD_LIGHT4_GREEN, SafeAssets.MEDIUM_HARD_LIGHT4_GREY, SafeAssets.MEDIUM_HARD_LIGHT4_RED, 145);

    String light;
    String lightBlue;
    String lightGreen;
    String lightGrey;
    String lightRed;
    float angle;

    SafeLightHardDefinition(String light, String lightBlue, String lightGreen, String lightGrey, String lightRed, float angle) {
        this.light = light;
        this.lightBlue = lightBlue;
        this.lightGreen = lightGreen;
        this.lightGrey = lightGrey;
        this.lightRed = lightRed;
        this.angle = angle;
    }

    public String getLight() {
        return light;
    }

    public String getGreyTextureRegion() {
        return lightGrey;
    }

    public String getRedTextureRegion() {
        return lightRed;
    }

    public String getGreenTextureRegion() {
        return lightGreen;
    }

    public String getBlueTextureRegion() {
        return lightBlue;
    }

    public float getAngle() {
        return angle;
    }
}
