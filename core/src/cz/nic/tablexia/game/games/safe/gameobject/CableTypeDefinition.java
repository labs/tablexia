/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.safe.gameobject;

import com.badlogic.gdx.graphics.Color;

import cz.nic.tablexia.game.games.safe.assets.SafeAssets;

/**
 * Created by Aneta Steimarová on 17.7.17.
 */

public enum CableTypeDefinition {
    TYPE_1(SafeAssets.TERM1, SafeAssets.SOCKET1, new Color(0x29abe1ff), SafeAssets.MEDIUM_HARD_LIGHT1_GREY, SafeAssets.MEDIUM_HARD_LIGHT1_RED, SafeAssets.MEDIUM_HARD_LIGHT1_GREEN, SafeAssets.MEDIUM_HARD_LIGHT1_BLUE),
    TYPE_2(SafeAssets.TERM2, SafeAssets.SOCKET2, Color.RED, SafeAssets.MEDIUM_HARD_LIGHT2_GREY, SafeAssets.MEDIUM_HARD_LIGHT2_RED, SafeAssets.MEDIUM_HARD_LIGHT2_GREEN, SafeAssets.MEDIUM_HARD_LIGHT2_BLUE),
    TYPE_3(SafeAssets.TERM3, SafeAssets.SOCKET3, Color.FOREST, SafeAssets.MEDIUM_HARD_LIGHT3_GREY, SafeAssets.MEDIUM_HARD_LIGHT3_RED, SafeAssets.MEDIUM_HARD_LIGHT3_GREEN, SafeAssets.MEDIUM_HARD_LIGHT3_BLUE),
    TYPE_4(SafeAssets.TERM4, SafeAssets.SOCKET4, Color.GOLD, SafeAssets.MEDIUM_HARD_LIGHT4_GREY, SafeAssets.MEDIUM_HARD_LIGHT4_RED, SafeAssets.MEDIUM_HARD_LIGHT4_GREEN, SafeAssets.MEDIUM_HARD_LIGHT4_BLUE);

    private String term;
    private String socket;
    private Color cableColor;
    private String lightGrey;
    private String lightRed;
    private String lightGreen;
    private String lightBlue;

    CableTypeDefinition(String term, String socket, Color cableColor, String lightGrey, String lightRed, String lightGreen, String lightBlue) {
        this.term = term;
        this.socket = socket;
        this.cableColor = cableColor;
        this.lightGrey = lightGrey;
        this.lightRed = lightRed;
        this.lightGreen = lightGreen;
        this.lightBlue = lightBlue;
    }

    public String getTerm() {
        return term;
    }

    public String getSocket() {
        return socket;
    }

    public Color getCableColor() {
        return cableColor;
    }

    public String getLightGrey() {
        return lightGrey;
    }

    public String getLightRed() {
        return lightRed;
    }

    public String getLightGreen() {
        return lightGreen;
    }

    public String getLightBlue() {
        return lightBlue;
    }
}
