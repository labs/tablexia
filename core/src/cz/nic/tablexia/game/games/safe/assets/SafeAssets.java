/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.assets;

/**
 * Created by Aneta Steimarová on 11.5.17.
 */

public class SafeAssets {

    private static final String GFX_PATH            = "gfx/";
    private static final String COMMON              = "common/";
    private static final String SFX_PATH            = "sfx/";
    private static final String BACKGROUND          = "background/";
    private static final String EASY                = "easy/";
    private static final String MEDIUM              = "medium/";
    private static final String HARD                = "hard/";
    private static final String BONUS               = "bonus/";
    private static final String TWEEZERS            = "tweezers/";
    private static final String LIGHTS              = "lights/";
    private static final String VISUALIZATION       = "visualization/";
    private static final String ALPHABET            = "alphabet/";
    private static final String NUMBERS             = "numbers/";
    private static final String CS                  = "cs/";
    private static final String SK                  = "sk/";
    private static final String DE                  = "de/";


    public static final String SAFE_BACKGROUND      = GFX_PATH + BACKGROUND + "gray_background";
    public static final String SIDE_BAR             = GFX_PATH + BACKGROUND + "sidebar-background";
    public static final String SIDE_BAR_EDGE        = GFX_PATH + BACKGROUND + "sidebar-background-edge";
    public static final String HELP_BTN_EYE         = GFX_PATH + BACKGROUND + "help_eye";
    public static final String HELP_PAPER           = GFX_PATH + "help_paper";
    public static final String EAR                  = GFX_PATH + "ear";

    //======== SAFE EASY =============
    public static final String CIRCLE            = GFX_PATH + EASY + "circle";
    public static final String CIRCLE_BACKGROUND = GFX_PATH + EASY + "circle_background";
    public static final String EASY_BACKGROUND   = GFX_PATH + EASY + "background";
    
    public static final String LIGHT1_RED         = GFX_PATH + EASY + LIGHTS + "light1_red";
    public static final String LIGHT1_GREEN       = GFX_PATH + EASY + LIGHTS + "light1_green";
    public static final String LIGHT1_BLUE        = GFX_PATH + EASY + LIGHTS + "light1_blue";
    public static final String LIGHT1_GREY        = GFX_PATH + EASY + LIGHTS + "light1_grey";    
    
    public static final String LIGHT2_RED         = GFX_PATH + EASY + LIGHTS + "light2_red";
    public static final String LIGHT2_GREEN       = GFX_PATH + EASY + LIGHTS + "light2_green";
    public static final String LIGHT2_BLUE        = GFX_PATH + EASY + LIGHTS + "light2_blue";
    public static final String LIGHT2_GREY        = GFX_PATH + EASY + LIGHTS + "light2_grey";    
    
    public static final String LIGHT3_RED         = GFX_PATH + EASY + LIGHTS + "light3_red";
    public static final String LIGHT3_GREEN       = GFX_PATH + EASY + LIGHTS + "light3_green";
    public static final String LIGHT3_BLUE        = GFX_PATH + EASY + LIGHTS + "light3_blue";
    public static final String LIGHT3_GREY        = GFX_PATH + EASY + LIGHTS + "light3_grey";

    public static final String VISUALIZATION_OBJECT1 = GFX_PATH + EASY + VISUALIZATION + "1";
    public static final String VISUALIZATION_OBJECT2 = GFX_PATH + EASY + VISUALIZATION + "2";
    public static final String VISUALIZATION_OBJECT3 = GFX_PATH + EASY + VISUALIZATION + "3";
    public static final String VISUALIZATION_OBJECT4 = GFX_PATH + EASY + VISUALIZATION + "4";

    //======== SAFE MEDIUM =============
    public static final String CABLE                       = GFX_PATH + MEDIUM + "cable";

    public static final String TERM1                       = GFX_PATH + MEDIUM + "term1";
    public static final String TERM2                       = GFX_PATH + MEDIUM + "term2";
    public static final String TERM3                       = GFX_PATH + MEDIUM + "term3";
    public static final String TERM4                       = GFX_PATH + MEDIUM + "term4";

    public static final String SOCKET1                      = GFX_PATH + MEDIUM + "socket1";
    public static final String SOCKET2                      = GFX_PATH + MEDIUM + "socket2";
    public static final String SOCKET3                      = GFX_PATH + MEDIUM + "socket3";
    public static final String SOCKET4                      = GFX_PATH + MEDIUM + "socket4";
    public static final String SOCKET_FINAL                 = GFX_PATH + MEDIUM + "socket-final";

    //======== SAFE HARD =============
    public static final String PEAK                         = GFX_PATH + HARD + "peak";
    public static final String POINTER                      = GFX_PATH + HARD + "pointer";
    public static final String HARD_BACKGROUND              = GFX_PATH + HARD + "background";
    public static final String HARD_LIGHT1                  = GFX_PATH + HARD + "1";
    public static final String HARD_LIGHT2                  = GFX_PATH + HARD + "2";
    public static final String HARD_LIGHT3                  = GFX_PATH + HARD + "3";
    public static final String HARD_LIGHT4                  = GFX_PATH + HARD + "4";

    //======== LIGHTS MEDIUM + HARD ==========
    public static final String MEDIUM_BACKGROUND            = GFX_PATH + MEDIUM + "background";
    public static final String MEDIUM_HARD_LIGHT1_RED       = GFX_PATH + LIGHTS + "1red";
    public static final String MEDIUM_HARD_LIGHT1_GREEN     = GFX_PATH + LIGHTS + "1green";
    public static final String MEDIUM_HARD_LIGHT1_BLUE      = GFX_PATH + LIGHTS + "1blue";
    public static final String MEDIUM_HARD_LIGHT1_GREY      = GFX_PATH + LIGHTS + "1grey";

    public static final String MEDIUM_HARD_LIGHT2_RED       = GFX_PATH + LIGHTS + "2red";
    public static final String MEDIUM_HARD_LIGHT2_GREEN     = GFX_PATH + LIGHTS + "2green";
    public static final String MEDIUM_HARD_LIGHT2_BLUE      = GFX_PATH + LIGHTS + "2blue";
    public static final String MEDIUM_HARD_LIGHT2_GREY      = GFX_PATH + LIGHTS + "2grey";

    public static final String MEDIUM_HARD_LIGHT3_RED       = GFX_PATH + LIGHTS + "3red";
    public static final String MEDIUM_HARD_LIGHT3_GREEN     = GFX_PATH + LIGHTS + "3green";
    public static final String MEDIUM_HARD_LIGHT3_BLUE      = GFX_PATH + LIGHTS + "3blue";
    public static final String MEDIUM_HARD_LIGHT3_GREY      = GFX_PATH + LIGHTS + "3grey";

    public static final String MEDIUM_HARD_LIGHT4_RED       = GFX_PATH + LIGHTS + "4red";
    public static final String MEDIUM_HARD_LIGHT4_GREEN     = GFX_PATH + LIGHTS + "4green";
    public static final String MEDIUM_HARD_LIGHT4_BLUE      = GFX_PATH + LIGHTS + "4blue";
    public static final String MEDIUM_HARD_LIGHT4_GREY      = GFX_PATH + LIGHTS + "4grey";

    //======== SAFE BONUS =============
    public static final String BOOM                         = GFX_PATH + BONUS + "boom";
    public static final String BONUS_BACKGROUND             = GFX_PATH + BONUS + "background";

    public static final String BONUS_CABLE1                 = GFX_PATH + BONUS + "cable1";
    public static final String BONUS_CABLE1_CUT             = GFX_PATH + BONUS + "cable1_cut1";
    public static final String BONUS_CABLE2                 = GFX_PATH + BONUS + "cable2";
    public static final String BONUS_CABLE2_CUT             = GFX_PATH + BONUS + "cable2_cut1";
    public static final String BONUS_CABLE3                 = GFX_PATH + BONUS + "cable3";
    public static final String BONUS_CABLE3_CUT             = GFX_PATH + BONUS + "cable3_cut1";
    public static final String BONUS_CABLE4                 = GFX_PATH + BONUS + "cable4";
    public static final String BONUS_CABLE4_CUT             = GFX_PATH + BONUS + "cable4_cut1";

    public static final String BONUS_SOCKET1                = GFX_PATH + BONUS + "socket1";
    public static final String BONUS_SOCKET2                = GFX_PATH + BONUS + "socket2";
    public static final String BONUS_SOCKET3                = GFX_PATH + BONUS + "socket3";
    public static final String BONUS_SOCKET4                = GFX_PATH + BONUS + "socket4";
    public static final String BONUS_SOCKET_FINAL1          = GFX_PATH + BONUS + "socket-final1";
    public static final String BONUS_SOCKET_FINAL2          = GFX_PATH + BONUS + "socket-final2";
    public static final String BONUS_SOCKET_FINAL3          = GFX_PATH + BONUS + "socket-final3";
    public static final String BONUS_SOCKET_FINAL4          = GFX_PATH + BONUS + "socket-final4";

    public static final String BONUS_LIGHT1_RED             = GFX_PATH + BONUS + LIGHTS + "light1_red";
    public static final String BONUS_LIGHT1_GREEN           = GFX_PATH + BONUS + LIGHTS + "light1_green";
    public static final String BONUS_LIGHT1_BLUE            = GFX_PATH + BONUS + LIGHTS + "light1_blue";
    public static final String BONUS_LIGHT1_GREY            = GFX_PATH + BONUS + LIGHTS + "light1_grey";

    public static final String BONUS_LIGHT2_RED             = GFX_PATH + BONUS + LIGHTS + "light2_red";
    public static final String BONUS_LIGHT2_GREEN           = GFX_PATH + BONUS + LIGHTS + "light2_green";
    public static final String BONUS_LIGHT2_BLUE            = GFX_PATH + BONUS + LIGHTS + "light2_blue";
    public static final String BONUS_LIGHT2_GREY            = GFX_PATH + BONUS + LIGHTS + "light2_grey";

    public static final String BONUS_LIGHT3_RED             = GFX_PATH + BONUS + LIGHTS + "light3_red";
    public static final String BONUS_LIGHT3_GREEN           = GFX_PATH + BONUS + LIGHTS + "light3_green";
    public static final String BONUS_LIGHT3_BLUE            = GFX_PATH + BONUS + LIGHTS + "light3_blue";
    public static final String BONUS_LIGHT3_GREY            = GFX_PATH + BONUS + LIGHTS + "light3_grey";

    public static final String BONUS_LIGHT4_RED             = GFX_PATH + BONUS + LIGHTS + "light4_red";
    public static final String BONUS_LIGHT4_GREEN           = GFX_PATH + BONUS + LIGHTS + "light4_green";
    public static final String BONUS_LIGHT4_BLUE            = GFX_PATH + BONUS + LIGHTS + "light4_blue";
    public static final String BONUS_LIGHT4_GREY            = GFX_PATH + BONUS + LIGHTS + "light4_grey";

    public static final String TWEEZERS_NAME                = GFX_PATH + BONUS + TWEEZERS + "tweezers";
    public static final String TWEEZERS1                    = GFX_PATH + BONUS + TWEEZERS + "tweezers1";

    //======== TEXTS =============
    public static final String REPLAY_TEXT                  = "game_safe_replay";
    public static final String SCORE_TEXT                   = "score";
    public static final String SCORE_0_TEXT                 = "game_safe_score_0";
    public static final String SCORE_1_TEXT                 = "game_safe_score_1";
    public static final String SCORE_2_TEXT                 = "game_safe_score_2";
    public static final String SCORE_3_TEXT                 = "game_safe_score_3";
    public static final String DONE_TEXT                    = "game_safe_complete";
    public static final String HELP_EYE_TEXT                = "game_safe_help_eye";
    public static final String RESULT_SCORE_TEXT            = "result_score_count";
    public static final String INSTRUCTIONS_PAY_ATTENTION   = "game_safe_instructions_pay_attention";

    //======= SOUNDS ==============
    public static final String SCORE_0_SOUND		= COMMON + SFX_PATH + "result_0.mp3";
    public static final String SCORE_1_SOUND		= COMMON + SFX_PATH + "result_1.mp3";
    public static final String SCORE_2_SOUND		= COMMON + SFX_PATH + "result_2.mp3";
    public static final String SCORE_3_SOUND		= COMMON + SFX_PATH + "result_3.mp3";

    //======== SOUNDS WITHOUT MEANING =============
    public static final String BELL                         = COMMON + SFX_PATH + "bell.mp3";
    public static final String CYMBAL                       = COMMON + SFX_PATH + "cymbal.mp3";
    public static final String DRUM                         = COMMON + SFX_PATH + "drum.mp3";
    public static final String GUITAR_1                     = COMMON + SFX_PATH + "guitar_1.mp3";
    public static final String GUITAR_2                     = COMMON + SFX_PATH + "guitar_2.mp3";
    public static final String HI_HAT                       = COMMON + SFX_PATH + "hi-hat.mp3";
    public static final String PIANO_1                      = COMMON + SFX_PATH + "piano_1.mp3";
    public static final String PIANO_2                      = COMMON + SFX_PATH + "piano_2.mp3";
    public static final String POT                          = COMMON + SFX_PATH + "pot.mp3";
    public static final String RATTLE                       = COMMON + SFX_PATH + "rattle.mp3";
    public static final String SAX                          = COMMON + SFX_PATH + "sax.mp3";
    public static final String SYNTH                        = COMMON + SFX_PATH + "synth.mp3";
    public static final String TRIANGLE                     = COMMON + SFX_PATH + "triangle.mp3";
    public static final String WOOD                         = COMMON + SFX_PATH + "wood.mp3";


    //======== SOUNDS ALPHABET CS =============
    public static final String A_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "a.mp3";
    public static final String B_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "b.mp3";
    public static final String C_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "c.mp3";
    public static final String D_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "d.mp3";
    public static final String E_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "e.mp3";
    public static final String F_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "f.mp3";
    public static final String G_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "g.mp3";
    public static final String H_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "h.mp3";
    public static final String I_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "i.mp3";
    public static final String J_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "j.mp3";
    public static final String K_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "k.mp3";
    public static final String L_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "l.mp3";
    public static final String M_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "m.mp3";
    public static final String N_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "n.mp3";
    public static final String O_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "o.mp3";
    public static final String P_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "p.mp3";
    public static final String Q_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "q.mp3";
    public static final String R_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "r.mp3";
    public static final String S_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "s.mp3";
    public static final String T_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "t.mp3";
    public static final String U_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "u.mp3";
    public static final String V_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "v.mp3";
    public static final String W_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "w.mp3";
    public static final String X_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "x.mp3";
    public static final String Y_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "y.mp3";
    public static final String Z_CS                         = COMMON + SFX_PATH + ALPHABET + CS + "z.mp3";

    //======== SOUNDS ALPHABET SK =============
    public static final String A_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "a.mp3";
    public static final String B_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "b.mp3";
    public static final String C_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "c.mp3";
    public static final String D_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "d.mp3";
    public static final String E_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "e.mp3";
    public static final String F_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "f.mp3";
    public static final String G_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "g.mp3";
    public static final String H_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "h.mp3";
    public static final String I_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "i.mp3";
    public static final String J_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "j.mp3";
    public static final String K_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "k.mp3";
    public static final String L_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "l.mp3";
    public static final String M_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "m.mp3";
    public static final String N_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "n.mp3";
    public static final String O_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "o.mp3";
    public static final String P_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "p.mp3";
    public static final String Q_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "q.mp3";
    public static final String R_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "r.mp3";
    public static final String S_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "s.mp3";
    public static final String T_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "t.mp3";
    public static final String U_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "u.mp3";
    public static final String V_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "v.mp3";
    public static final String W_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "w.mp3";
    public static final String X_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "x.mp3";
    public static final String Y_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "y.mp3";
    public static final String Z_SK                         = COMMON + SFX_PATH + ALPHABET + SK + "z.mp3";

    //======== SOUNDS ALPHABET DE =============
    public static final String A_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "a.mp3";
    public static final String B_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "b.mp3";
    public static final String C_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "c.mp3";
    public static final String D_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "d.mp3";
    public static final String E_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "e.mp3";
    public static final String F_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "f.mp3";
    public static final String G_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "g.mp3";
    public static final String H_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "h.mp3";
    public static final String I_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "i.mp3";
    public static final String J_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "j.mp3";
    public static final String K_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "k.mp3";
    public static final String L_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "l.mp3";
    public static final String M_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "m.mp3";
    public static final String N_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "n.mp3";
    public static final String O_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "o.mp3";
    public static final String P_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "p.mp3";
    public static final String Q_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "q.mp3";
    public static final String R_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "r.mp3";
    public static final String S_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "s.mp3";
    public static final String T_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "t.mp3";
    public static final String U_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "u.mp3";
    public static final String V_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "v.mp3";
    public static final String W_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "w.mp3";
    public static final String X_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "x.mp3";
    public static final String Y_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "y.mp3";
    public static final String Z_DE                         = COMMON + SFX_PATH + ALPHABET + DE + "z.mp3";

    //======== SOUNDS NUMBERS CS =============
    public static final String ONE_CS                          = COMMON + SFX_PATH + NUMBERS + CS + "1.mp3";
    public static final String TWO_CS                          = COMMON + SFX_PATH + NUMBERS + CS + "2.mp3";
    public static final String THREE_CS                        = COMMON + SFX_PATH + NUMBERS + CS + "3.mp3";
    public static final String FOUR_CS                         = COMMON + SFX_PATH + NUMBERS + CS + "4.mp3";
    public static final String FIVE_CS                         = COMMON + SFX_PATH + NUMBERS + CS + "5.mp3";
    public static final String SIX_CS                          = COMMON + SFX_PATH + NUMBERS + CS + "6.mp3";
    public static final String SEVEN_CS                        = COMMON + SFX_PATH + NUMBERS + CS + "7.mp3";
    public static final String EIGHT_CS                        = COMMON + SFX_PATH + NUMBERS + CS + "8.mp3";
    public static final String NINE_CS                         = COMMON + SFX_PATH + NUMBERS + CS + "9.mp3";
    public static final String TEN_CS                          = COMMON + SFX_PATH + NUMBERS + CS + "10.mp3";
    public static final String ELEVEN_CS                       = COMMON + SFX_PATH + NUMBERS + CS + "11.mp3";
    public static final String TWELVE_CS                       = COMMON + SFX_PATH + NUMBERS + CS + "12.mp3";
    public static final String THIRTEEN_CS                     = COMMON + SFX_PATH + NUMBERS + CS + "13.mp3";
    public static final String FOURTEEN_CS                     = COMMON + SFX_PATH + NUMBERS + CS + "14.mp3";
    public static final String FIVETEEN_CS                     = COMMON + SFX_PATH + NUMBERS + CS + "15.mp3";
    public static final String SIXTEEN_CS                      = COMMON + SFX_PATH + NUMBERS + CS + "16.mp3";
    public static final String SEVENTEEN_CS                    = COMMON + SFX_PATH + NUMBERS + CS + "17.mp3";
    public static final String EIGHTEEN_CS                     = COMMON + SFX_PATH + NUMBERS + CS + "18.mp3";
    public static final String NINETEEN_CS                     = COMMON + SFX_PATH + NUMBERS + CS + "19.mp3";
    public static final String TWENTY_CS                       = COMMON + SFX_PATH + NUMBERS + CS + "20.mp3";
    public static final String TWENTYONE_CS                    = COMMON + SFX_PATH + NUMBERS + CS + "21.mp3";
    public static final String TWENTYTWO_CS                    = COMMON + SFX_PATH + NUMBERS + CS + "22.mp3";
    public static final String TWENTYTHREE_CS                  = COMMON + SFX_PATH + NUMBERS + CS + "23.mp3";
    public static final String TWENTYFOUR_CS                   = COMMON + SFX_PATH + NUMBERS + CS + "24.mp3";
    public static final String TWENTYFIVE_CS                   = COMMON + SFX_PATH + NUMBERS + CS + "25.mp3";
    public static final String TWENTYSIX_CS                    = COMMON + SFX_PATH + NUMBERS + CS + "26.mp3";
    public static final String TWENTYSEVEN_CS                  = COMMON + SFX_PATH + NUMBERS + CS + "27.mp3";
    public static final String TWENTYEIGHT_CS                  = COMMON + SFX_PATH + NUMBERS + CS + "28.mp3";
    public static final String TWENTYNINE_CS                   = COMMON + SFX_PATH + NUMBERS + CS + "29.mp3";
    public static final String THIRTY_CS                       = COMMON + SFX_PATH + NUMBERS + CS + "30.mp3";


    //======== SOUNDS NUMBERS SK =============
    public static final String ONE_SK                          = COMMON + SFX_PATH + NUMBERS + SK + "1.mp3";
    public static final String TWO_SK                          = COMMON + SFX_PATH + NUMBERS + SK + "2.mp3";
    public static final String THREE_SK                        = COMMON + SFX_PATH + NUMBERS + SK + "3.mp3";
    public static final String FOUR_SK                         = COMMON + SFX_PATH + NUMBERS + SK + "4.mp3";
    public static final String FIVE_SK                         = COMMON + SFX_PATH + NUMBERS + SK + "5.mp3";
    public static final String SIX_SK                          = COMMON + SFX_PATH + NUMBERS + SK + "6.mp3";
    public static final String SEVEN_SK                        = COMMON + SFX_PATH + NUMBERS + SK + "7.mp3";
    public static final String EIGHT_SK                        = COMMON + SFX_PATH + NUMBERS + SK + "8.mp3";
    public static final String NINE_SK                         = COMMON + SFX_PATH + NUMBERS + SK + "9.mp3";
    public static final String TEN_SK                          = COMMON + SFX_PATH + NUMBERS + SK + "10.mp3";
    public static final String ELEVEN_SK                       = COMMON + SFX_PATH + NUMBERS + SK + "11.mp3";
    public static final String TWELVE_SK                       = COMMON + SFX_PATH + NUMBERS + SK + "12.mp3";
    public static final String THIRTEEN_SK                     = COMMON + SFX_PATH + NUMBERS + SK + "13.mp3";
    public static final String FOURTEEN_SK                     = COMMON + SFX_PATH + NUMBERS + SK + "14.mp3";
    public static final String FIVETEEN_SK                     = COMMON + SFX_PATH + NUMBERS + SK + "15.mp3";
    public static final String SIXTEEN_SK                      = COMMON + SFX_PATH + NUMBERS + SK + "16.mp3";
    public static final String SEVENTEEN_SK                    = COMMON + SFX_PATH + NUMBERS + SK + "17.mp3";
    public static final String EIGHTEEN_SK                     = COMMON + SFX_PATH + NUMBERS + SK + "18.mp3";
    public static final String NINETEEN_SK                     = COMMON + SFX_PATH + NUMBERS + SK + "19.mp3";
    public static final String TWENTY_SK                       = COMMON + SFX_PATH + NUMBERS + SK + "20.mp3";
    public static final String TWENTYONE_SK                    = COMMON + SFX_PATH + NUMBERS + SK + "21.mp3";
    public static final String TWENTYTWO_SK                    = COMMON + SFX_PATH + NUMBERS + SK + "22.mp3";
    public static final String TWENTYTHREE_SK                  = COMMON + SFX_PATH + NUMBERS + SK + "23.mp3";
    public static final String TWENTYFOUR_SK                   = COMMON + SFX_PATH + NUMBERS + SK + "24.mp3";
    public static final String TWENTYFIVE_SK                   = COMMON + SFX_PATH + NUMBERS + SK + "25.mp3";
    public static final String TWENTYSIX_SK                    = COMMON + SFX_PATH + NUMBERS + SK + "26.mp3";
    public static final String TWENTYSEVEN_SK                  = COMMON + SFX_PATH + NUMBERS + SK + "27.mp3";
    public static final String TWENTYEIGHT_SK                  = COMMON + SFX_PATH + NUMBERS + SK + "28.mp3";
    public static final String TWENTYNINE_SK                   = COMMON + SFX_PATH + NUMBERS + SK + "29.mp3";
    public static final String THIRTY_SK                       = COMMON + SFX_PATH + NUMBERS + SK + "30.mp3";

    //======== SOUNDS NUMBERS DE =============
    public static final String ONE_DE                          = COMMON + SFX_PATH + NUMBERS + DE + "1.mp3";
    public static final String TWO_DE                          = COMMON + SFX_PATH + NUMBERS + DE + "2.mp3";
    public static final String THREE_DE                        = COMMON + SFX_PATH + NUMBERS + DE + "3.mp3";
    public static final String FOUR_DE                         = COMMON + SFX_PATH + NUMBERS + DE + "4.mp3";
    public static final String FIVE_DE                         = COMMON + SFX_PATH + NUMBERS + DE + "5.mp3";
    public static final String SIX_DE                          = COMMON + SFX_PATH + NUMBERS + DE + "6.mp3";
    public static final String SEVEN_DE                        = COMMON + SFX_PATH + NUMBERS + DE + "7.mp3";
    public static final String EIGHT_DE                        = COMMON + SFX_PATH + NUMBERS + DE + "8.mp3";
    public static final String NINE_DE                         = COMMON + SFX_PATH + NUMBERS + DE + "9.mp3";
    public static final String TEN_DE                          = COMMON + SFX_PATH + NUMBERS + DE + "10.mp3";
    public static final String ELEVEN_DE                       = COMMON + SFX_PATH + NUMBERS + DE + "11.mp3";
    public static final String TWELVE_DE                       = COMMON + SFX_PATH + NUMBERS + DE + "12.mp3";
    public static final String THIRTEEN_DE                     = COMMON + SFX_PATH + NUMBERS + DE + "13.mp3";
    public static final String FOURTEEN_DE                     = COMMON + SFX_PATH + NUMBERS + DE + "14.mp3";
    public static final String FIVETEEN_DE                     = COMMON + SFX_PATH + NUMBERS + DE + "15.mp3";
    public static final String SIXTEEN_DE                      = COMMON + SFX_PATH + NUMBERS + DE + "16.mp3";
    public static final String SEVENTEEN_DE                    = COMMON + SFX_PATH + NUMBERS + DE + "17.mp3";
    public static final String EIGHTEEN_DE                     = COMMON + SFX_PATH + NUMBERS + DE + "18.mp3";
    public static final String NINETEEN_DE                     = COMMON + SFX_PATH + NUMBERS + DE + "19.mp3";
    public static final String TWENTY_DE                       = COMMON + SFX_PATH + NUMBERS + DE + "20.mp3";
    public static final String TWENTYONE_DE                    = COMMON + SFX_PATH + NUMBERS + DE + "21.mp3";
    public static final String TWENTYTWO_DE                    = COMMON + SFX_PATH + NUMBERS + DE + "22.mp3";
    public static final String TWENTYTHREE_DE                  = COMMON + SFX_PATH + NUMBERS + DE + "23.mp3";
    public static final String TWENTYFOUR_DE                   = COMMON + SFX_PATH + NUMBERS + DE + "24.mp3";
    public static final String TWENTYFIVE_DE                   = COMMON + SFX_PATH + NUMBERS + DE + "25.mp3";
    public static final String TWENTYSIX_DE                    = COMMON + SFX_PATH + NUMBERS + DE + "26.mp3";
    public static final String TWENTYSEVEN_DE                  = COMMON + SFX_PATH + NUMBERS + DE + "27.mp3";
    public static final String TWENTYEIGHT_DE                  = COMMON + SFX_PATH + NUMBERS + DE + "28.mp3";
    public static final String TWENTYNINE_DE                   = COMMON + SFX_PATH + NUMBERS + DE + "29.mp3";
    public static final String THIRTY_DE                       = COMMON + SFX_PATH + NUMBERS + DE + "30.mp3";

    //======== SILENCE =============
    public static final String SILENCE                      = COMMON + SFX_PATH + "silence.mp3";

    //======== BOOM =================
    public static final String BOOM_SOUND                   = COMMON + SFX_PATH + "explosion.mp3";
}
