/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.common.ui.health_bar.HealthBar;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.safe.actors.SequenceGenerator;
import cz.nic.tablexia.game.games.safe.assets.SafeAssets;
import cz.nic.tablexia.game.games.safe.gameobject.SafeLightImage;
import cz.nic.tablexia.game.games.safe.listeners.DoneButtonClickListener;
import cz.nic.tablexia.game.games.safe.listeners.HelpButtonClickListener;
import cz.nic.tablexia.game.games.safe.listeners.ReplayButtonClickListener;
import cz.nic.tablexia.game.games.safe.model.SafeDifficulty;
import cz.nic.tablexia.game.games.safe.model.SafeGameResultStars;
import cz.nic.tablexia.game.games.safe.model.SafeSequence;
import cz.nic.tablexia.game.games.safe.gameobject.VisualizationDialog;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.resolvers.SafeScoreResolver;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.PlayMusicAction;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.button.ImageTablexiaButton;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

/**
 * Created by Aneta Steimarová on 3.5.17.
 */

public class SafeGame extends AbstractTablexiaGame<Void> {
    public static final String  DONE_BUTTON                 = "done button";
    private static final String SCORE_COUNT                 = SafeScoreResolver.SCORE_COUNT;

    private static final int    NEXT_ROUND_DELAY = 2;
    private static final int    BUTTON_WIDTH                = 187;
    private static final int    BUTTON_HEIGHT               = 72;
    private static final int    MINUS_POINT_OFFSET_X        = 76;
    private static final int    MINUS_POINT_OFFSET_Y        = 1;
    private static final int    SCORE_OFFSET_Y              = 102;
    private static final int    SCORE_OFFSET_X              = 30;
    public static final float   SIDEBAR_RELATIVE_WIDTH      = 0.25f;
    private static final int    REPLAY_BUTTON_OFFSET_X      = 217;
    private static final int    REPLAY_BUTTON_OFFSET_Y      = 193;
    private static final int    DONE_BUTTON_OFFSET_Y        = 10;
    private static final int    HELP_BUTTON_OFFSET_Y        = 112;
    private static final int    BOOM_WIDTH                  = 500;
    private static final int    BOOM_HEIGHT                 = 516;
    private static final String MINUS_ONE_TEXT              = "-1";
    private static final float  DIMMER_ALPHA                = 0.7f;
    private static final Color  VISUALIZATION_DIMMER_COLOR  = new Color(0, 0, 0, DIMMER_ALPHA);

    public static final int    ROUNDS_FOR_ONE_GAME          = 4;
    private static final int   MAX_MISTAKES_ALLOWED         = 2;
    private static final int   MAX_SCORE                    = 10;
    public static final float  FADE_ANIMATION_DURATION      = 1f;
    public static final float  FADE_OUT_ANIMATION_DURATION  = 0.5f;
    private static final float FADE_IN_DIMMER_DURATION      = 0.3f;

    private Group backgroundGroup;
    private AbstractMechanics safeGroup;
    private Image sideBar;
    private Image sideBarEdge;
    private Image safeBackground;
    private HealthBar healthBar;

    private ImageTablexiaButton replayButton;
    private ImageTablexiaButton doneButton;
    private ImageTablexiaButton helpButton;
    private SequenceGenerator sequenceGenerator;
    private SafeSequence safeSequence;
    private boolean usedHelpButton;
    private TablexiaLabel scoreTextLabel;
    private static SafeLightImage selectedLight = null;
    private int round = 0;
    private int score = 0;
    public int level_fails = 0;
    private boolean paused = false;
    private boolean isVisualisationVisible = false;
    private List<Music> musicList;


    public AbstractMechanics getSafeGroup() {
        return safeGroup;
    }

    public SafeSequence getSafeSequence() {
        return safeSequence;
    }

    public SequenceGenerator getSequenceGenerator() {
        return sequenceGenerator;
    }

    public void setVisualizationVisible(boolean trueFalse) {
        isVisualisationVisible = trueFalse;
    }

    public void setSelectedLight(SafeLightImage light, List<SafeLightImage> listOfLights, boolean playSound) {
        selectedLight = light;
        if (null == selectedLight) {
            doneButton.setDisabled();
            selectedLight.turnOffAllLights(listOfLights);
        } else {
            if (!safeGroup.holding) doneButton.setEnabled();
            if (!playSound) selectedLight.lightWithoutSound(listOfLights, null);
            else selectedLight.light(listOfLights, null);
        }
    }

    public static SafeLightImage getSelectedLight() {
        return selectedLight;
    }

    public int getRound() {
        return round;
    }

    private void changeGameScore(int number) {
        score += number;
        scoreTextLabel.setText(getFormattedText(SafeAssets.SCORE_TEXT, score, MAX_SCORE));
    }

    @Override
    public boolean isSoundMandatory() {
        return true;
    }

    @Override
    protected void gameLoaded(Map<String, String> gameState) {
        round = 0;
        score = 0;
        level_fails = 0;
        musicList = new ArrayList<Music>();
        selectedLight = null;
        usedHelpButton = false;
        setGameScore(SCORE_COUNT, score);
        prepareBackground();
        prepareSequenceGenerator();
        prepareSequenceMusic();
        prepareSafe();

        backgroundGroup.addActor(sideBar);
        backgroundGroup.addActor(sideBarEdge);
        backgroundGroup.addActor(healthBar);
        backgroundGroup.addActor(replayButton);
        backgroundGroup.addActor(doneButton);
        backgroundGroup.addActor(helpButton);
        backgroundGroup.addActor(scoreTextLabel);

    }

    private void prepareSafe() {
        backgroundGroup.addActor(safeGroup = SafeDifficulty.getSafeDifficultyForGameDifficulty(getGameDifficulty()).getMechanics());

        safeGroup.setSize(getSceneWidth() - sideBar.getWidth(), getSceneInnerHeight());
        safeGroup.setPosition(getSceneLeftX(), getSceneInnerBottomY());

        generateSafeForRound();
    }

    private void generateSafeForRound() {
        safeGroup.clearChildren();
        safeGroup.prepare(this, safeSequence);
    }

    private void prepareBackground() {
        backgroundGroup = new Group();
        backgroundGroup.setDebug(true);
        getStage().addActor(backgroundGroup);

        safeBackground = getBackgroudForDifficulty(getGameDifficulty());
        safeBackground.setPosition(getViewportLeftX(), getSceneOuterBottomY());
        if(getGameDifficulty()!=GameDifficulty.BONUS)safeBackground.setSize(getViewportWidth() - getViewportWidth() * SIDEBAR_RELATIVE_WIDTH, getSceneOuterHeight());
        else safeBackground.setSize(getViewportWidth(), getSceneOuterHeight());
        backgroundGroup.addActor(safeBackground);

        sideBar = new Image(getScreenTextureRegion(SafeAssets.SIDE_BAR));
        sideBar.setSize(getViewportWidth() * SIDEBAR_RELATIVE_WIDTH, getSceneOuterHeight());
        sideBar.setPosition(getSceneRightX() - sideBar.getWidth(), getSceneOuterBottomY());

        sideBarEdge = new Image(getScreenTextureRegion(SafeAssets.SIDE_BAR_EDGE));
        sideBarEdge.setPosition(sideBar.getX() - (sideBarEdge.getWidth() - 1/*Texture bleeding fix*/), sideBar.getY());

        healthBar = new HealthBar(getGameGlobalTextureRegion(HEART_FULL),
                getGameGlobalTextureRegion(HEART_BROKEN),
                getColorTextureRegion(HealthBar.BACKGROUND_COLOR),
                MAX_MISTAKES_ALLOWED);

        healthBar.setHealthBarDefaultPosition(this);

        addReplayBtn();
        addDoneBtn();
        addHelpBtn();
        addScore();
    }

    private Image getBackgroudForDifficulty(GameDifficulty gameDifficulty) {
        switch (gameDifficulty){
            case EASY: return new Image(getScreenTextureRegion(SafeAssets.EASY_BACKGROUND));
            case HARD: return new Image(getScreenTextureRegion(SafeAssets.HARD_BACKGROUND));
            case MEDIUM: return new Image(getScreenTextureRegion(SafeAssets.MEDIUM_BACKGROUND));
            case BONUS: return new Image(getScreenTextureRegion(SafeAssets.BONUS_BACKGROUND));
            default: return new Image(getScreenTextureRegion(SafeAssets.SAFE_BACKGROUND));
        }
    }

    private void addScore() {
        scoreTextLabel = new TablexiaLabel(getFormattedText(SafeAssets.SCORE_TEXT, score, MAX_SCORE), new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.BOLD_18, Color.BLACK));
        scoreTextLabel.setPosition(getViewportWidth() - SCORE_OFFSET_X - scoreTextLabel.getWidth() - ((BUTTON_WIDTH - scoreTextLabel.getWidth()) / 2), getSceneInnerBottomY() + SCORE_OFFSET_Y);
        scoreTextLabel.setAlignment(Align.center);
    }

    private void addReplayBtn() {
        replayButton = createButton(SafeAssets.REPLAY_TEXT, ApplicationInternalTextureManager.BUTTON_REPEAT_ICON, getViewportWidth() - REPLAY_BUTTON_OFFSET_X, getSceneInnerTopY() - REPLAY_BUTTON_OFFSET_Y,
                new ReplayButtonClickListener(this));
    }

    private ImageTablexiaButton createButton(String buttonText, String buttonIcon, float x, float y, ClickListener clickListener) {
        ImageTablexiaButton imageTablexiaButton = new ImageTablexiaButton(
                getText(buttonText),
                new Image(ApplicationInternalTextureManager.getInstance().getTexture(buttonIcon))
        );

        setButton(imageTablexiaButton, x, y, clickListener);
        return imageTablexiaButton;
    }

    private void setButton(ImageTablexiaButton imageTablexiaButton, float x, float y, ClickListener clickListener) {
        imageTablexiaButton.setButtonSize(BUTTON_WIDTH, BUTTON_HEIGHT)
                .setButtonPosition(x, y)
                .setInputListener(clickListener);
    }

    public void makeButtonsTouchable() {
        if (selectedLight != null) doneButton.setEnabled();
        if (!usedHelpButton) helpButton.setEnabled();
        replayButton.setEnabled();
    }

    public void makeBtnsAndSafeUntouchable(ImageTablexiaButton btn) {
        safeGroup.setSafeActorsUntouchable();
        if (!btn.equals(replayButton)) replayButton.setDisabled();
        if (!btn.equals(doneButton)) doneButton.setDisabled();
        if (!btn.equals(helpButton)) helpButton.setDisabled();
    }

    public void makeBtnUntouchable(ImageTablexiaButton btn) {
        btn.setDisabled();
    }

    public void playSoundSequence(SafeSequence safeSequence) {
        prepareSequenceGenerator();
        sequenceGenerator.playSequence(safeSequence, this);
    }


    private void addDoneBtn() {

        doneButton = createButton(SafeAssets.DONE_TEXT, ApplicationInternalTextureManager.BUTTON_YES_ICON, getViewportWidth() - REPLAY_BUTTON_OFFSET_X, getSceneInnerBottomY() + DONE_BUTTON_OFFSET_Y,
                new DoneButtonClickListener(this));

        doneButton.setDisabled();
        doneButton.setName(DONE_BUTTON);
    }

    public void evaluate(SequenceAction sa) {
        SafeGame safeGame = this;
        if (selectedLight.getSound().equals(getMusic(safeSequence.getExpected().getSoundPath()))) {
            sa.addAction(Actions.run(new Runnable() {
                @Override
                public void run() {
                    changeGameScore(round + 1);
                    selectedLight.lightGreen();
                    setGameScore(SCORE_COUNT, score);
                }
            }));
        } else {
            sa.addAction(Actions.run(new Runnable() {
                @Override
                public void run() {
                    healthBar.hide();
                    level_fails++;
                    selectedLight.lightRed();
                }
            }));
            if (safeGame.getGameDifficulty() == GameDifficulty.BONUS) {
                sa.addAction(makeBoom(safeGame));
                sa.addAction(Actions.delay(2f));
            }
        }
    }

    private RunnableAction makeBoom(SafeGame safeGame) {
        return Actions.run(new Runnable() {
            @Override
            public void run() {
                Image boom = new Image(getScreenTextureRegion(SafeAssets.BOOM));
                boom.setSize(BOOM_WIDTH, BOOM_HEIGHT);
                boom.setOrigin(Align.center);
                boom.setPosition(sideBarEdge.getX()/2 - boom.getWidth()/2, getViewportHeight()/2 - boom.getHeight()/2);
                backgroundGroup.addActor(boom);
                boom.addAction(Actions.alpha(0));
                boom.addAction(Actions.scaleTo(0, 0));
                boom.addAction(Actions.parallel(Actions.fadeIn(1f),
                                                PlayMusicAction.getInstance(safeGame.getMusic(SafeAssets.BOOM_SOUND)),
                                                Actions.sequence(Actions.scaleTo(1, 1, 1f, new Interpolation.SwingOut(2f)), Actions.fadeOut(1f))));
            }
        });
    }

    public void playNextRound() {
        round++;
        // level_fails means how many mistakes player did -> game ends after 2 mistakes
        if (round < ROUNDS_FOR_ONE_GAME && level_fails < MAX_MISTAKES_ALLOWED) {
            prepareNextRound();
            Log.info(getClass(), "Starting round " + (round + 1));
            playSoundSequence(safeSequence);
            makeButtonsTouchable();
        } else {
            disposeMusicInMusicList();
            gameComplete();
        }
    }

    private void prepareNextRound() {
        disposeMusicInMusicList();
        safeSequence = null;
        selectedLight = null;
        prepareSequenceMusic();
        generateSafeForRound();
    }

    private void disposeMusicInMusicList(){
        for(Music music: musicList){
            music.dispose();
        }
        musicList.clear();
    }

    public void addMusicToMusicList(Music sound){
        for(Music music: musicList){
            if(music.equals(sound)) return;
        }
        musicList.add(sound);
    }

    private void addHelpBtn() {
        helpButton = new ImageTablexiaButton(getText(SafeAssets.HELP_EYE_TEXT), new Image(getScreenTextureRegion(SafeAssets.HELP_BTN_EYE)), Align.left, StandardTablexiaButton.TablexiaButtonType.BLUE);
        setButton(helpButton, getViewportWidth() - REPLAY_BUTTON_OFFSET_X, getSceneInnerTopY() - HELP_BUTTON_OFFSET_Y, new HelpButtonClickListener(this));
    }

    public void showVisualization() {
        usedHelpButton = true;
        changeGameScore(-1);
        isVisualisationVisible = true;
        showMinusOnePointAndVisualize();
    }

    private void showMinusOnePointAndVisualize() {
        Group minusPoint = new Group();
        backgroundGroup.addActor(minusPoint);
        TablexiaLabel textLabel = new TablexiaLabel(MINUS_ONE_TEXT, new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.BOLD_20, Color.RED), true);
        textLabel.setPosition(getViewportWidth() - MINUS_POINT_OFFSET_X, scoreTextLabel.getY() + scoreTextLabel.getHeight() + MINUS_POINT_OFFSET_Y);
        minusPoint.addActor(textLabel);
        minusPoint.addAction(Actions.sequence(Actions.fadeIn(FADE_ANIMATION_DURATION),
                Actions.fadeOut(FADE_ANIMATION_DURATION), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        Image dimmer = prepareDimmer();
                        dimmer.addAction(Actions.sequence(Actions.fadeIn(FADE_IN_DIMMER_DURATION), Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                showVizualizationDialog(dimmer);
                            }
                        })));
                    }
                })));
    }


    private void showVizualizationDialog(Image dimmer) {
        new VisualizationDialog(this, dimmer);
    }

    private Image prepareDimmer() {
        Image dimmer = new Image(ApplicationAtlasManager.getInstance().getColorTexture(VISUALIZATION_DIMMER_COLOR));
        dimmer.setPosition(getViewportLeftX(), getSceneOuterBottomY());
        dimmer.setSize(getViewportWidth(), getSceneOuterHeight());
        getStage().addActor(dimmer);
        return dimmer;
    }

    private void prepareSequenceGenerator() {
        sequenceGenerator = new SequenceGenerator(getText(SafeAssets.INSTRUCTIONS_PAY_ATTENTION), ApplicationAtlasManager.getInstance().getColorTexture(Color.BLACK), getScreenTextureRegion(SafeAssets.EAR));
        sequenceGenerator.setBounds(getViewportLeftX(), getSceneOuterBottomY(), getViewportWidth(), getSceneOuterHeight());
        getStage().addActor(sequenceGenerator);
    }

    private void prepareSequenceMusic() {
        safeSequence = sequenceGenerator.prepareSoundSequence(getGameDifficulty(), round);
    }

    public ImageTablexiaButton getDoneButton() {
        return doneButton;
    }

    public ImageTablexiaButton getReplayButton() {
        return replayButton;
    }

    public ImageTablexiaButton getHelpButton() {
        return helpButton;
    }

    public boolean isUsedHelpButton() {
        return usedHelpButton;
    }

    @Override
    protected void gameVisible() {
        Log.info(getClass(), "Starting round " + (round + 1));
        sequenceGenerator.playSequence(safeSequence, this);
    }

    @Override
    protected void gamePaused(Map<String, String> gameState) {
        paused = true;
        super.gamePaused(gameState);
    }


    @Override
    protected void gameResumed() {
        if (paused && !isVisualisationVisible) {
            safeGroup.setButtonsEnabled(this);
            safeGroup.setDoneButtonDisOrEnabled(this);
        }
        paused = false;
        super.gameResumed();
    }

    @Override
    protected String preparePreloaderSpeechFileBaseName() {
        return getGameDifficulty().equals(GameDifficulty.BONUS) ? super.preparePreloaderSpeechFileBaseName() + prepareDifficultySuffix() : super.preparePreloaderSpeechFileBaseName();
    }

    ////////////////////// PRELOAD MESSAGE
    private static final String     PRELOADER_ANIM_IMAGE                    = "preloader_anim1";
    private static final int        PRELOADER_ANIM_FRAMES                   = 17;
    private static final float      PRELOADER_ANIM_FRAME_DURATION           = 0.4f;

    private static final String     PRELOADER_TEXT_HEADPHONES               = ApplicationTextManager.ApplicationTextsAssets.GAME_SAFE_PRELOADER_HEADPHONES;
    private static final Scaling    PRELOADER_IMAGE_SCALING                 = Scaling.fit;
    private static final Color      PRELOADER_TEXT_COLOR                    = Color.DARK_GRAY;
    private static final ApplicationFontManager.FontType FONT_TYPE          = ApplicationFontManager.FontType.REGULAR_18;
    private static final int        PRELOADER_TEXT_ALIGN                    = Align.left;
    private static final float      PRELOADER_TEXT_PADDING 	                = 10f;
    private static final float      PRELOADER_IMAGE_COLUMN_WIDTH_RATIO      = 1f / 4;
    private static final float      PRELOADER_TEXT_COLUMN_WIDTH_RATIO       = 5f / 8;
    private static final float 	    PRELOADER_ROW_HEIGHT 				    = 1f / 3;

    private static final String     PRELOADER_HEADPHONES_IMAGE              = "preloader_headphones";

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        final String preloaderTextDef = definePreloaderText(this);
        AnimatedImage preloaderImage = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_IMAGE, PRELOADER_ANIM_FRAMES, PRELOADER_ANIM_FRAME_DURATION), false);
        preloaderImage.startAnimationLoop();
        String preloaderText = preloaderAssetsManager.getText(preloaderTextDef);
        components.add(
                new TwoColumnContentDialogComponent(
                        new AnimatedImageContentDialogComponent(preloaderImage, PRELOADER_IMAGE_SCALING),
                        new TextContentDialogComponent(preloaderText, FONT_TYPE, PRELOADER_TEXT_COLOR, PRELOADER_TEXT_ALIGN, PRELOADER_TEXT_PADDING),
                        PRELOADER_IMAGE_COLUMN_WIDTH_RATIO,
                        PRELOADER_TEXT_COLUMN_WIDTH_RATIO,
                        PRELOADER_ROW_HEIGHT
                )
        );
        components.add(new FixedSpaceContentDialogComponent());

        Image preloaderImageHeadphones = new Image(preloaderAssetsManager.getTextureRegion(PRELOADER_HEADPHONES_IMAGE));
        String preloaderTextHeadphones = preloaderAssetsManager.getText(PRELOADER_TEXT_HEADPHONES);
        components.add(new TwoColumnContentDialogComponent(
                new TextContentDialogComponent(preloaderTextHeadphones, FONT_TYPE, PRELOADER_TEXT_COLOR, Integer.valueOf(PRELOADER_TEXT_ALIGN), Float.valueOf(PRELOADER_TEXT_PADDING)),
                new ImageContentDialogComponent(preloaderImageHeadphones, PRELOADER_IMAGE_SCALING),
                PRELOADER_TEXT_COLUMN_WIDTH_RATIO,
                PRELOADER_IMAGE_COLUMN_WIDTH_RATIO,
                PRELOADER_ROW_HEIGHT));
    }

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(new SummaryMessage(SummaryImage.STATS, getFormattedText(SafeAssets.RESULT_SCORE_TEXT, game.getGameScore(SafeScoreResolver.SCORE_COUNT, "0"), MAX_SCORE)));
    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        return SafeGameResultStars.getTextForGameResult(gameResult);
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        return SafeGameResultStars.getSoundForGameResult(gameResult);
    }

    public String definePreloaderText(SafeGame safeGame) {
        GameDifficulty gameDifficulty = safeGame.getGameDifficulty();
        if (gameDifficulty==GameDifficulty.BONUS) return ApplicationTextManager.ApplicationTextsAssets.GAME_SAFE_PRELOADER_TEXT_BONUS;
        return ApplicationTextManager.ApplicationTextsAssets.GAME_SAFE_PRELOADER_TEXT;
    }
}
