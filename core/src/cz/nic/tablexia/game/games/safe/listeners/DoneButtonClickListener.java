/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.listeners;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.safe.SafeGame;
import cz.nic.tablexia.util.ui.button.ImageTablexiaButton;

/**
 * Created by aneta on 19.12.17.
 */

public class DoneButtonClickListener extends AbstractButtonClickListener {
    private static final int NEXT_ROUND_DELAY = 2;

    public DoneButtonClickListener(SafeGame safeGame) {
        super(safeGame);
    }

    @Override
    public void onClicked() {
        SequenceAction sa = Actions.sequence();
        if(safeGame.getGameDifficulty() == GameDifficulty.BONUS) preAction(sa);
        safeGame.evaluate(sa);
        sa.addAction(Actions.delay(NEXT_ROUND_DELAY));
        sa.addAction( Actions.run(new Runnable() {
            @Override
            public void run() {
                firstClickedButton.set(null);
                safeGame.playNextRound();
            }
        }));
        safeGame.addAction(sa);
    }

    private void preAction(SequenceAction sa) {
        safeGame.getSafeGroup().doBeforeEvaluatingAnimation(sa);
    }

    @Override
    public ImageTablexiaButton getImageTablexiaButton() {
        return safeGame.getDoneButton();
    }
}
