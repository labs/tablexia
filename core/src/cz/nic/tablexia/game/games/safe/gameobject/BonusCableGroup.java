/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.safe.gameobject;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.List;

import cz.nic.tablexia.game.games.safe.AbstractMechanics;
import cz.nic.tablexia.game.games.safe.SafeGame;
import cz.nic.tablexia.game.games.safe.assets.SafeAssets;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;


/**
 * Created by Aneta Steimarová on 14.3.18.
 */

public class BonusCableGroup extends Group {
    public static final int TWEEZERS_WIDTH = 284;
    public static final int TWEEZERS_HEIGHT = 228;

    private int id;
    private Image finalSocket;
    private SafeGame safeGame;
    private SafeLightImage lightImage;
    private Image cable;
    private TextureRegion cutCableImage;
    private Image socket;

    public SafeLightImage getLightImage() {
        return lightImage;
    }

    public BonusCableGroup(int id, SafeGame safeGame, TextureRegion socketImage, float socketPercentReduction,
                           TextureRegion cableImage, TextureRegion cutCableImage,
                           TextureRegion finalSocketImage, float finalSocketPercentReduction,
                           String lightGrey, String lightRed, String lightGreen, String lightBlue, float lightSize,
                           Music lightSound, List<SafeLightImage> listOfLights, String lightName) {
        this.setDebug(true);
        this.id = id;
        this.safeGame = safeGame;
        this.cutCableImage = cutCableImage;

        setTouchable(Touchable.childrenOnly);

        socket = new Image(socketImage);
        socket.setBounds(
                0,
                0,
                socket.getWidth()*socketPercentReduction,
                socket.getHeight()*socketPercentReduction
        );

        finalSocket = new Image(finalSocketImage);
        finalSocket.setSize(finalSocket.getWidth()*finalSocketPercentReduction, finalSocket.getHeight()*finalSocketPercentReduction);

        cable = new Image(cableImage);

        lightImage = new SafeLightImage(safeGame, lightGrey, lightRed, lightGreen, lightBlue, lightSound);
        lightImage.setSize(lightSize*socket.getWidth(), lightSize*socket.getWidth());
        lightImage.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                safeGame.setSelectedLight(lightImage, listOfLights, true);
                AbstractTablexiaScreen.triggerScenarioStepEvent(AbstractMechanics.LIGHT_LIGHTED_UP);
            }
        });
        lightImage.addListener(lightImage.getInputListener());
        lightImage.setName(lightName + id);
        listOfLights.add(lightImage);

        addActor(cable);
        addActor(socket);
        addActor(lightImage);
        addActor(finalSocket);
    }

    public void setComponentsPosition(float x, float y, float width, float lightX, float lightY, float cableStartX, float cableEndX) {
        socket.setPosition(width - socket.getWidth(), y);
        finalSocket.setPosition(x, y);
        lightImage.setPosition(socket.getX() + lightX*socket.getWidth(), socket.getY() + lightY*socket.getHeight());

        cable.setPosition(x + finalSocket.getWidth()*cableStartX, y);
        cable.setWidth(socket.getX() + socket.getWidth()*cableEndX - (finalSocket.getX() + finalSocket.getWidth()*cableStartX));
        cable.setHeight(socket.getHeight());
    }


    public void setAllTouchable() {
        lightImage.addListener(lightImage.getInputListener());
    }

    public void setAllUntouchable() {
        lightImage.clearListeners();
    }

    private float getCableCutX() {
        switch (id) {
            case 0:
                return cable.getWidth() / 2 + 20;
            case 1:
                return cable.getWidth() / 2 + 30;
            case 2:
                return cable.getWidth() / 2 + 25;
            default:
                return cable.getWidth() / 2 + 10;
        }
    }

    private float getCableCutY() {
        switch (id) {
            case 0:
                return cable.getHeight()/2 - TWEEZERS_HEIGHT/2;
            case 1:
                return cable.getHeight()/2 - TWEEZERS_HEIGHT/2 - 30;
            case 2:
                return cable.getHeight()/2 - TWEEZERS_HEIGHT/2;
            default:
                return cable.getHeight()/2 - TWEEZERS_HEIGHT/2 - 10;
        }
    }

    public void cut(SequenceAction sa) {
        Image tweezers = new Image(safeGame.getScreenTextureRegion(SafeAssets.TWEEZERS1));
        tweezers.setSize(TWEEZERS_WIDTH, TWEEZERS_HEIGHT);
        float tweezersXPos = cable.getX() + cable.getWidth();
        float tweezersYPos = cable.getY() + getCableCutY();
        tweezers.setPosition(tweezersXPos, tweezersYPos);
        addActor(tweezers);
        SequenceAction tweezersAnimation = Actions.sequence();;
        for(int i=2; i<9; i++){
            String assetName = SafeAssets.TWEEZERS_NAME + i;
            tweezersAnimation.addAction(Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            tweezers.setDrawable(new TextureRegionDrawable(safeGame.getScreenTextureRegion(assetName)));
                        }
                    }));
            tweezersAnimation.addAction(Actions.delay(0.1f));
        }

        sa.addAction(Actions.sequence(Actions.run(new Runnable() {
            @Override
            public void run() {
                tweezers.addAction(Actions.moveTo(tweezersXPos - getCableCutX(), tweezersYPos, 1f));
            }
        }), Actions.delay(1f),
                tweezersAnimation,
                Actions.run(new Runnable() {
            @Override
            public void run() {
                cable.setDrawable(new TextureRegionDrawable(cutCableImage));
            }
        })));
    }
}
