/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.safe.model;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.safe.gameobject.SafeSounds;

/**
 * Created by Aneta Steimarová on 14.3.18.
 */

public enum SafeBonusLevelDefinition implements SafeLevelDefinition {
    //level 1
    LVL_1_A(new SafeSounds[]{SafeSounds.ONE, SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.FIVE, SafeSounds.C, SafeSounds.FOUR, SafeSounds.D}, SafeSounds.FIVE, new SafeSounds[]{SafeSounds.ONE, SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.FIVE, SafeSounds.C, SafeSounds.FOUR, SafeSounds.D}, new SafeSounds[]{SafeSounds.TWO, SafeSounds.FIVE}),
    LVL_1_B(new SafeSounds[]{SafeSounds.ONE, SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.THREE,SafeSounds.E, SafeSounds.FOUR, SafeSounds.D}, SafeSounds.E, new SafeSounds[]{SafeSounds.ONE, SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.THREE,SafeSounds.E, SafeSounds.FOUR, SafeSounds.D}, new SafeSounds[]{SafeSounds.B, SafeSounds.E}),
    LVL_1_C(new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.C, SafeSounds.THREE, SafeSounds.D, SafeSounds.SIX}, SafeSounds.SIX, new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.C, SafeSounds.THREE, SafeSounds.D, SafeSounds.SIX}, new SafeSounds[]{SafeSounds.THREE, SafeSounds.SIX}),
    LVL_1_D(new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.C, SafeSounds.THREE, SafeSounds.F, SafeSounds.FOUR}, SafeSounds.F, new SafeSounds[]{SafeSounds.A, SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.C, SafeSounds.THREE, SafeSounds.F, SafeSounds.FOUR}, new SafeSounds[]{SafeSounds.C, SafeSounds.F}),
    LVL_1_E(new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.Z, SafeSounds.TWENTYFIVE, SafeSounds.V, SafeSounds.TWENTYFOUR, SafeSounds.X, SafeSounds.TWENTYTHREE, SafeSounds.W}, SafeSounds.V, new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.Z, SafeSounds.TWENTYFIVE, SafeSounds.V, SafeSounds.TWENTYFOUR, SafeSounds.X, SafeSounds.TWENTYTHREE, SafeSounds.W}, new SafeSounds[]{SafeSounds.V, SafeSounds.X}),
    LVL_1_F(new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.Z, SafeSounds.TWENTYTWO, SafeSounds.Y, SafeSounds.TWENTYFOUR, SafeSounds.X, SafeSounds.TWENTYTHREE, SafeSounds.W}, SafeSounds.TWENTYTWO, new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.Z, SafeSounds.TWENTYTWO, SafeSounds.Y, SafeSounds.TWENTYFOUR, SafeSounds.X, SafeSounds.TWENTYTHREE, SafeSounds.W}, new SafeSounds[]{SafeSounds.TWENTYTWO, SafeSounds.TWENTYFOUR}),
    LVL_1_G(new SafeSounds[]{SafeSounds.Z, SafeSounds.TWENTYSIX, SafeSounds.Y, SafeSounds.TWENTYFIVE, SafeSounds.R, SafeSounds.TWENTYFOUR, SafeSounds.W, SafeSounds.TWENTYTHREE}, SafeSounds.R, new SafeSounds[]{SafeSounds.Z, SafeSounds.TWENTYSIX, SafeSounds.Y, SafeSounds.TWENTYFIVE, SafeSounds.R, SafeSounds.TWENTYFOUR, SafeSounds.W, SafeSounds.TWENTYTHREE}, new SafeSounds[]{SafeSounds.R, SafeSounds.W}),
    LVL_1_H(new SafeSounds[]{SafeSounds.Z, SafeSounds.TWENTYSIX, SafeSounds.Y, SafeSounds.TWENTYONE, SafeSounds.X, SafeSounds.TWENTYFOUR, SafeSounds.W, SafeSounds.TWENTYTHREE}, SafeSounds.TWENTYONE, new SafeSounds[]{SafeSounds.Z, SafeSounds.TWENTYSIX, SafeSounds.Y, SafeSounds.TWENTYONE, SafeSounds.X, SafeSounds.TWENTYFOUR, SafeSounds.W, SafeSounds.TWENTYTHREE}, new SafeSounds[]{SafeSounds.TWENTYONE, SafeSounds.TWENTYTHREE}),

    //level 2
    LVL_2_A(new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.FIVE, SafeSounds.C, SafeSounds.G, SafeSounds.SIX, SafeSounds.E, SafeSounds.F}, SafeSounds.G, new SafeSounds[]{SafeSounds.A, SafeSounds.B, SafeSounds.FIVE, SafeSounds.C, SafeSounds.G, SafeSounds.SIX, SafeSounds.E, SafeSounds.F}, new SafeSounds[]{SafeSounds.C, SafeSounds.G, SafeSounds.B}),
    LVL_2_B(new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.E, SafeSounds.THREE, SafeSounds.SEVEN, SafeSounds.F, SafeSounds.FIVE, SafeSounds.SIX}, SafeSounds.SEVEN, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWO, SafeSounds.E, SafeSounds.THREE, SafeSounds.SEVEN, SafeSounds.F, SafeSounds.FIVE, SafeSounds.SIX}, new SafeSounds[]{SafeSounds.TWO, SafeSounds.THREE, SafeSounds.SEVEN}),
    LVL_2_C(new SafeSounds[]{SafeSounds.Z, SafeSounds.Y, SafeSounds.TWENTYTWO, SafeSounds.X, SafeSounds.W, SafeSounds.TWENTYONE, SafeSounds.T, SafeSounds.U}, SafeSounds.T, new SafeSounds[]{SafeSounds.Z, SafeSounds.Y, SafeSounds.TWENTYTWO, SafeSounds.X, SafeSounds.W, SafeSounds.TWENTYONE, SafeSounds.T, SafeSounds.U}, new SafeSounds[]{SafeSounds.Y, SafeSounds.W, SafeSounds.T}),
    LVL_2_D(new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.TWENTYFIVE, SafeSounds.M, SafeSounds.TWENTYFOUR, SafeSounds.TWENTYTHREE, SafeSounds.L, SafeSounds.TWENTYTWO, SafeSounds.TWENTY}, SafeSounds.TWENTY, new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.TWENTYFIVE, SafeSounds.M, SafeSounds.TWENTYFOUR, SafeSounds.TWENTYTHREE, SafeSounds.L, SafeSounds.TWENTYTWO, SafeSounds.TWENTY}, new SafeSounds[]{SafeSounds.TWENTY, SafeSounds.TWENTYTWO, SafeSounds.TWENTYFOUR}),
    LVL_2_E(new SafeSounds[]{SafeSounds.K, SafeSounds.J, SafeSounds.ELEVEN, SafeSounds.M, SafeSounds.N, SafeSounds.TWELVE, SafeSounds.O, SafeSounds.P}, SafeSounds.J, new SafeSounds[]{SafeSounds.K, SafeSounds.J, SafeSounds.ELEVEN, SafeSounds.M, SafeSounds.N, SafeSounds.TWELVE, SafeSounds.O, SafeSounds.P}, new SafeSounds[]{SafeSounds.J, SafeSounds.M, SafeSounds.O}),
    LVL_2_F(new SafeSounds[]{SafeSounds.ELEVEN, SafeSounds.TEN, SafeSounds.H, SafeSounds.THIRTEEN, SafeSounds.FOURTEEN, SafeSounds.I, SafeSounds.FIVETEEN, SafeSounds.SIXTEEN}, SafeSounds.TEN, new SafeSounds[]{SafeSounds.ELEVEN, SafeSounds.TEN, SafeSounds.H, SafeSounds.THIRTEEN, SafeSounds.FOURTEEN, SafeSounds.I, SafeSounds.FIVETEEN, SafeSounds.SIXTEEN}, new SafeSounds[]{SafeSounds.TEN, SafeSounds.THIRTEEN, SafeSounds.SIXTEEN}),
    LVL_2_G(new SafeSounds[]{SafeSounds.FOUR, SafeSounds.SIX, SafeSounds.A, SafeSounds.SEVEN, SafeSounds.EIGHT, SafeSounds.B, SafeSounds.NINE, SafeSounds.TEN}, SafeSounds.FOUR, new SafeSounds[]{SafeSounds.FOUR, SafeSounds.SIX, SafeSounds.A, SafeSounds.SEVEN, SafeSounds.EIGHT, SafeSounds.B, SafeSounds.NINE, SafeSounds.TEN}, new SafeSounds[]{SafeSounds.FOUR, SafeSounds.EIGHT, SafeSounds.NINE}),
    LVL_2_H(new SafeSounds[]{SafeSounds.D, SafeSounds.F, SafeSounds.ONE, SafeSounds.G, SafeSounds.H, SafeSounds.TWO, SafeSounds.I, SafeSounds.J}, SafeSounds.D, new SafeSounds[]{SafeSounds.D, SafeSounds.F, SafeSounds.ONE, SafeSounds.G, SafeSounds.H, SafeSounds.TWO, SafeSounds.I, SafeSounds.J}, new SafeSounds[]{SafeSounds.D, SafeSounds.H, SafeSounds.J}),

    //level 3
    LVL_3_A(new SafeSounds[]{SafeSounds.D, SafeSounds.B, SafeSounds.C, SafeSounds.A, SafeSounds.FOUR, SafeSounds.TWO, SafeSounds.FIVE, SafeSounds.ONE}, SafeSounds.FIVE, new SafeSounds[]{SafeSounds.D, SafeSounds.B, SafeSounds.C, SafeSounds.A, SafeSounds.FOUR, SafeSounds.TWO, SafeSounds.FIVE, SafeSounds.ONE}, new SafeSounds[]{SafeSounds.FOUR, SafeSounds.TWO, SafeSounds.FIVE}),
    LVL_3_B(new SafeSounds[]{SafeSounds.FOUR, SafeSounds.TWO, SafeSounds.THREE, SafeSounds.ONE, SafeSounds.D, SafeSounds.B, SafeSounds.E, SafeSounds.A}, SafeSounds.E, new SafeSounds[]{SafeSounds.FOUR, SafeSounds.TWO, SafeSounds.THREE, SafeSounds.ONE, SafeSounds.D, SafeSounds.B, SafeSounds.E, SafeSounds.A}, new SafeSounds[]{SafeSounds.D, SafeSounds.E, SafeSounds.A}),
    LVL_3_C(new SafeSounds[]{SafeSounds.TEN, SafeSounds.EIGHT, SafeSounds.NINE, SafeSounds.SEVEN, SafeSounds.J, SafeSounds.H, SafeSounds.I, SafeSounds.F}, SafeSounds.F, new SafeSounds[]{SafeSounds.TEN, SafeSounds.EIGHT, SafeSounds.NINE, SafeSounds.SEVEN, SafeSounds.J, SafeSounds.H, SafeSounds.I, SafeSounds.F}, new SafeSounds[]{SafeSounds.F, SafeSounds.H, SafeSounds.J}),
    LVL_3_D(new SafeSounds[]{SafeSounds.J, SafeSounds.H, SafeSounds.I, SafeSounds.G, SafeSounds.TEN, SafeSounds.EIGHT, SafeSounds.NINE, SafeSounds.SIX}, SafeSounds.SIX, new SafeSounds[]{SafeSounds.J, SafeSounds.H, SafeSounds.I, SafeSounds.G, SafeSounds.TEN, SafeSounds.EIGHT, SafeSounds.NINE, SafeSounds.SIX}, new SafeSounds[]{SafeSounds.SIX, SafeSounds.EIGHT, SafeSounds.TEN}),
    LVL_3_E(new SafeSounds[]{SafeSounds.A, SafeSounds.Z, SafeSounds.B, SafeSounds.Y, SafeSounds.ONE, SafeSounds.TWENTYSIX, SafeSounds.TWO, SafeSounds.TWENTYFOUR}, SafeSounds.TWENTYFOUR, new SafeSounds[]{SafeSounds.A, SafeSounds.Z, SafeSounds.B, SafeSounds.Y, SafeSounds.ONE, SafeSounds.TWENTYSIX, SafeSounds.TWO, SafeSounds.TWENTYFOUR}, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWENTYFOUR, SafeSounds.TWENTYSIX}),
    LVL_3_F(new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWENTYSIX, SafeSounds.TWO, SafeSounds.TWENTYFIVE, SafeSounds.A, SafeSounds.Z, SafeSounds.C, SafeSounds.Y}, SafeSounds.C, new SafeSounds[]{SafeSounds.ONE, SafeSounds.TWENTYSIX, SafeSounds.TWO, SafeSounds.TWENTYFIVE, SafeSounds.A, SafeSounds.Z, SafeSounds.C, SafeSounds.Y}, new SafeSounds[]{SafeSounds.Z, SafeSounds.Y, SafeSounds.C}),
    LVL_3_G(new SafeSounds[]{SafeSounds.Z, SafeSounds.A, SafeSounds.B, SafeSounds.Y, SafeSounds.TWENTYSIX, SafeSounds.THREE, SafeSounds.TWENTYFIVE, SafeSounds.TWO}, SafeSounds.THREE, new SafeSounds[]{SafeSounds.Z, SafeSounds.A, SafeSounds.B, SafeSounds.Y, SafeSounds.TWENTYSIX, SafeSounds.THREE, SafeSounds.TWENTYFIVE, SafeSounds.TWO}, new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.TWO, SafeSounds.THREE}),
    LVL_3_H(new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.ONE, SafeSounds.TWENTYFIVE, SafeSounds.TWO, SafeSounds.Z, SafeSounds.D, SafeSounds.Y, SafeSounds.B}, SafeSounds.D, new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.ONE, SafeSounds.TWENTYFIVE, SafeSounds.TWO, SafeSounds.Z, SafeSounds.D, SafeSounds.Y, SafeSounds.B}, new SafeSounds[]{SafeSounds.B, SafeSounds.D, SafeSounds.Z}),

    //level 4
    LVL_4_A(new SafeSounds[]{SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.D, SafeSounds.THREE, SafeSounds.E, SafeSounds.FOUR, SafeSounds.H}, SafeSounds.E, new SafeSounds[]{SafeSounds.ONE, SafeSounds.B, SafeSounds.TWO, SafeSounds.D, SafeSounds.THREE, SafeSounds.E, SafeSounds.FOUR, SafeSounds.H}, new SafeSounds[]{SafeSounds.B, SafeSounds.D, SafeSounds.E, SafeSounds.H}),
    LVL_4_B(new SafeSounds[]{SafeSounds.B, SafeSounds.ONE, SafeSounds.D, SafeSounds.TWO, SafeSounds.F, SafeSounds.THREE, SafeSounds.G, SafeSounds.FOUR}, SafeSounds.G, new SafeSounds[]{SafeSounds.B, SafeSounds.ONE, SafeSounds.D, SafeSounds.TWO, SafeSounds.F, SafeSounds.THREE, SafeSounds.G, SafeSounds.FOUR}, new SafeSounds[]{SafeSounds.B, SafeSounds.D, SafeSounds.F, SafeSounds.G}),
    LVL_4_C(new SafeSounds[]{SafeSounds.TWO, SafeSounds.A, SafeSounds.FOUR, SafeSounds.B, SafeSounds.SEVEN, SafeSounds.C, SafeSounds.EIGHT, SafeSounds.D}, SafeSounds.SEVEN, new SafeSounds[]{SafeSounds.TWO, SafeSounds.A, SafeSounds.FOUR, SafeSounds.B, SafeSounds.SEVEN, SafeSounds.C, SafeSounds.EIGHT, SafeSounds.D}, new SafeSounds[]{SafeSounds.TWO, SafeSounds.FOUR, SafeSounds.SEVEN, SafeSounds.EIGHT}),
    LVL_4_D(new SafeSounds[]{SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.FOUR, SafeSounds.C, SafeSounds.FIVE, SafeSounds.D, SafeSounds.EIGHT}, SafeSounds.FIVE, new SafeSounds[]{SafeSounds.A, SafeSounds.TWO, SafeSounds.B, SafeSounds.FOUR, SafeSounds.C, SafeSounds.FIVE, SafeSounds.D, SafeSounds.EIGHT}, new SafeSounds[]{SafeSounds.TWO, SafeSounds.FOUR, SafeSounds.FIVE, SafeSounds.EIGHT}),
    LVL_4_E(new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.Z, SafeSounds.TWENTYFIVE, SafeSounds.Y, SafeSounds.TWENTYFOUR, SafeSounds.V, SafeSounds.TWENTYTHREE, SafeSounds.T}, SafeSounds.Y, new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.Z, SafeSounds.TWENTYFIVE, SafeSounds.Y, SafeSounds.TWENTYFOUR, SafeSounds.V, SafeSounds.TWENTYTHREE, SafeSounds.T}, new SafeSounds[]{SafeSounds.Z, SafeSounds.Y, SafeSounds.V, SafeSounds.T}),
    LVL_4_F(new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.Z, SafeSounds.TWENTYFIVE, SafeSounds.Y, SafeSounds.TWENTYTWO, SafeSounds.X, SafeSounds.TWENTY, SafeSounds.W}, SafeSounds.TWENTYFIVE, new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.Z, SafeSounds.TWENTYFIVE, SafeSounds.Y, SafeSounds.TWENTYTWO, SafeSounds.X, SafeSounds.TWENTY, SafeSounds.W}, new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.TWENTYFIVE, SafeSounds.TWENTYTWO, SafeSounds.TWENTY}),
    LVL_4_G(new SafeSounds[]{SafeSounds.Z, SafeSounds.TWENTYSIX, SafeSounds.X, SafeSounds.TWENTYFIVE, SafeSounds.V, SafeSounds.TWENTYFOUR, SafeSounds.W, SafeSounds.TWENTYTHREE}, SafeSounds.W, new SafeSounds[]{SafeSounds.Z, SafeSounds.TWENTYSIX, SafeSounds.X, SafeSounds.TWENTYFIVE, SafeSounds.V, SafeSounds.TWENTYFOUR, SafeSounds.W, SafeSounds.TWENTYTHREE}, new SafeSounds[]{SafeSounds.Z, SafeSounds.X, SafeSounds.V, SafeSounds.W}),
    LVL_4_H(new SafeSounds[]{SafeSounds.Z, SafeSounds.TWENTYSIX, SafeSounds.Y, SafeSounds.TWENTYFOUR, SafeSounds.X, SafeSounds.TWENTYONE, SafeSounds.W, SafeSounds.TWENTY}, SafeSounds.TWENTYONE, new SafeSounds[]{SafeSounds.Z, SafeSounds.TWENTYSIX, SafeSounds.Y, SafeSounds.TWENTYFOUR, SafeSounds.X, SafeSounds.TWENTYONE, SafeSounds.W, SafeSounds.TWENTY}, new SafeSounds[]{SafeSounds.TWENTYSIX, SafeSounds.TWENTYFOUR, SafeSounds.TWENTYONE, SafeSounds.TWENTY});

    private SafeSounds[] soundSequence;
    private SafeSounds expected;
    private SafeSounds[] soundsToSequence;
    private SafeSounds[] soundsToLights;

    SafeBonusLevelDefinition(SafeSounds[] soundSequence, SafeSounds expected, SafeSounds[] soundsToSequence, SafeSounds[] soundsToLights) {
        this.soundSequence = soundSequence;
        this.expected = expected;
        this.soundsToSequence = soundsToSequence;
        this.soundsToLights = soundsToLights;
    }

    @Override
    public SafeSounds[] getSoundSequence() {
        return soundSequence;
    }

    @Override
    public SafeSounds[] getSoundsToSequence() {
        return soundsToSequence;
    }

    @Override
    public SafeSounds[] getSoundsToLights() {
        return soundsToLights;
    }

    @Override
    public SafeSounds getExpectedSound() {
        return expected;
    }

    @Override
    public int getPlayAgainFirstXSounds() {
        return 0;
    }

    /**
     * @return possible sequences for round 1
     */
    private static List<SafeBonusLevelDefinition> getSequencesForFirstLevel() {
        List<SafeBonusLevelDefinition> listOfSequences = new ArrayList<SafeBonusLevelDefinition>();
        listOfSequences.add(LVL_1_A);
        listOfSequences.add(LVL_1_B);
        listOfSequences.add(LVL_1_C);
        listOfSequences.add(LVL_1_D);
        listOfSequences.add(LVL_1_E);
        listOfSequences.add(LVL_1_F);
        listOfSequences.add(LVL_1_G);
        listOfSequences.add(LVL_1_H);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 2
     */
    private static List<SafeBonusLevelDefinition> getSequencesForSecondLevel() {
        List<SafeBonusLevelDefinition> listOfSequences = new ArrayList<SafeBonusLevelDefinition>();
        listOfSequences.add(LVL_2_A);
        listOfSequences.add(LVL_2_B);
        listOfSequences.add(LVL_2_C);
        listOfSequences.add(LVL_2_D);
        listOfSequences.add(LVL_2_E);
        listOfSequences.add(LVL_2_F);
        listOfSequences.add(LVL_2_G);
        listOfSequences.add(LVL_2_H);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 3
     */
    private static List<SafeBonusLevelDefinition> getSequencesForThirdLevel() {
        List<SafeBonusLevelDefinition> listOfSequences = new ArrayList<SafeBonusLevelDefinition>();
        listOfSequences.add(LVL_3_A);
        listOfSequences.add(LVL_3_B);
        listOfSequences.add(LVL_3_C);
        listOfSequences.add(LVL_3_D);
        listOfSequences.add(LVL_3_E);
        listOfSequences.add(LVL_3_F);
        listOfSequences.add(LVL_3_G);
        listOfSequences.add(LVL_3_H);
        return listOfSequences;
    }

    /**
     * @return possible sequences for round 4
     */
    private static List<SafeBonusLevelDefinition> getSequencesForFourthLevel() {
        List<SafeBonusLevelDefinition> listOfSequences = new ArrayList<SafeBonusLevelDefinition>();
        listOfSequences.add(LVL_4_A);
        listOfSequences.add(LVL_4_B);
        listOfSequences.add(LVL_4_C);
        listOfSequences.add(LVL_4_D);
        listOfSequences.add(LVL_4_E);
        listOfSequences.add(LVL_4_F);
        listOfSequences.add(LVL_4_G);
        listOfSequences.add(LVL_4_H);
        return listOfSequences;
    }

    private static List<SafeSequence> makeListOfSequences(List<SafeBonusLevelDefinition> possibilitiesList) {
        List<SafeSequence> listOfSequences = new ArrayList<>();
        for (SafeBonusLevelDefinition bonusSequence : possibilitiesList) {
            listOfSequences.add(new SafeSequence(bonusSequence.getSoundSequence(), bonusSequence.getSoundsToSequence(), bonusSequence.getSoundsToLights(), bonusSequence.getExpectedSound(), bonusSequence.getPlayAgainFirstXSounds()));
        }
        return listOfSequences;
    }

    /**
     * Make list of sound sequences from SafeBonusLevelDefinition for whole levels
     *
     * @return list of sound sequence for for whole levels
     */
    public static ArrayList<List<SafeSequence>> makeArrayOfListOfSequences() {
        ArrayList<List<SafeSequence>> arrayOfListOfSequences = new ArrayList<List<SafeSequence>>();
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForFirstLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForSecondLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForThirdLevel()));
        arrayOfListOfSequences.add(makeListOfSequences(getSequencesForFourthLevel()));
        return arrayOfListOfSequences;
    }
}
