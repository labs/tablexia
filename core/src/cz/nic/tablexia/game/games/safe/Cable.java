/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.CatmullRomSpline;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.util.Log;

/**
 * Created by drahomir on 6/12/17.
 */

public class Cable extends Group {
    private static final int BUILD_INTERVAL_IN_MS        = 30;
    private static final int MINUMUM_CABLE_PARTS         = 24;
    private static final int PREGENERATED_CABLE_PARTS    = 2;
    private static final int PREFFERED_CABLE_PART_LENGTH = 25;
    private static final int MAX_SHAPING_FORCE           = 600;
    private static final int MIN_SHAPING_FORCE           = 300;
    private static final int INC_SHAPING                 = 100;
    private static final int CNT_PARTS_MIN_SHAPING       = 5;
    private static final int MIN_CABLE_LENGTH            = 30;
    private static final int SAMPLES                     = 10;
    private static final int FULL_DEGREES                = 360;
    private static final int QUARTER_OF_CIRCLE           = 90;
    public static final int  CABLE_WIDTH                 = 20;

    private static final float UV_TEXTURE_BLEEDING_FIX   = 0.01f;

    private int maxCableParts;
    private TextureRegion cableTextureRegion;

    private Vector2[] splineVectors = new Vector2[4];

    private boolean startingPointsSet = false;
    private boolean endingPointsSet = false;

    //Cable rendering fields
    private CatmullRomSpline<Vector2> spline;

    private int cableParts = 1;

    private Long lastTimeBuild = 0L;
    private boolean changed = true;
    private CableSpriteBatch cableSpriteBatch;

    private final float color;

    private final List<Float> distances;
    private final List<Vector2> verticesList;
    private final Vector2 tempPoint = new Vector2();
    private final Vector2 tempDir = new Vector2();
    private final Vector2 prevPoint = new Vector2();

    public Cable(int maxCableParts, TextureRegion cableTextureRegion, Color cableColor) {
        this.maxCableParts = maxCableParts;
        this.cableTextureRegion = cableTextureRegion;

        this.spline = new CatmullRomSpline<>();
        this.verticesList = new ArrayList<>();
        this.distances = new ArrayList<>();

        color = cableColor.toFloatBits();

        cableSpriteBatch = new CableSpriteBatch(this);
        cableSpriteBatch.setColor(Color.BLUE);
    }

    public void setStartingPoint(Vector2 point) {
        if (point == null) return;

        if (splineVectors[0] == null) splineVectors[0] = new Vector2();
        splineVectors[0].set(point.x - MIN_SHAPING_FORCE, point.y);

        if (splineVectors[1] == null) splineVectors[1] = new Vector2();
        splineVectors[1].set(point.x, point.y);

        startingPointsSet = true;
        changed = true;
    }

    public void setEndingPoint(Vector2 point) {
        if (point == null) return;

        int shaping = getShapingByParts();

        if (splineVectors[0] != null && splineVectors[1] != null) {
            splineVectors[0].x = splineVectors[1].x - shaping;
        }

        if (splineVectors[3] == null) splineVectors[3] = new Vector2();
        splineVectors[3].set(point.x + shaping, point.y);

        if (splineVectors[2] == null) splineVectors[2] = new Vector2();
        splineVectors[2].set(point.x, point.y);

        endingPointsSet = true;
        changed = true;
    }

    public double getCableLength() {
        if (!startingPointsSet || !endingPointsSet) {
            return 0;
        }

        Vector2 startPoint = splineVectors[1];
        Vector2 finishPoint = splineVectors[2];

        return Math.sqrt(Math.pow(finishPoint.x - startPoint.x, 2) + Math.pow(finishPoint.y - startPoint.y, 2));
    }

    public int getMaxCableParts() {
        return maxCableParts;
    }

    private int getCableParts() {
        return cableParts;
    }

    public void setCableParts() {
        cableParts = (int) (getCableLength() / CABLE_WIDTH) + 1;

        if (cableParts >= maxCableParts) cableParts = maxCableParts;
        else if (cableParts < 0) cableParts = 1;
    }

    private int getShapingByParts() {
        if (cableParts < CNT_PARTS_MIN_SHAPING)
            return MINUMUM_CABLE_PARTS;

        int shaping = (cableParts * INC_SHAPING);
        return shaping < MAX_SHAPING_FORCE ? shaping : MAX_SHAPING_FORCE;
    }

    public TextureRegion getCableTextureRegion() {
        return cableTextureRegion;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (changed && System.currentTimeMillis() - lastTimeBuild >= BUILD_INTERVAL_IN_MS) {
            buildCable();
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        ShaderProgram shader = batch.getShader();
        cableSpriteBatch.setTransformMatrix(batch.getTransformMatrix());
        cableSpriteBatch.setProjectionMatrix(batch.getProjectionMatrix());
        cableSpriteBatch.begin();
        cableSpriteBatch.draw();
        cableSpriteBatch.end();
        shader.begin();
    }

    private void buildCable() {
        if (!startingPointsSet || !endingPointsSet) {
            Log.debug(getClass(), "Can't build a cable. Set Starting and Ending points first!");
            return;
        }

        cableSpriteBatch.reset();

        spline.set(splineVectors, false);
        float approxLength = spline.approxLength(SAMPLES);
        int minParts = getCableLength() > MIN_CABLE_LENGTH ? MINUMUM_CABLE_PARTS : 2;

        int cableSegments = (int) (approxLength / PREFFERED_CABLE_PART_LENGTH);
        cableSegments = MathUtils.clamp(cableSegments, minParts, getCableParts() - PREGENERATED_CABLE_PARTS);

        float part = 1f / cableSegments;

        //FIRST POINT
        spline.valueAt(tempPoint, 0);
        verticesList.add(new Vector2(tempPoint.x, tempPoint.y + CABLE_WIDTH / 2));
        verticesList.add(new Vector2(tempPoint.x, tempPoint.y - CABLE_WIDTH / 2));

        for (int i = 1; i < cableSegments; i++) {
            float progress = MathUtils.clamp(i * part, 0, 1);
            prevPoint.set(tempPoint.x, tempPoint.y);
            spline.valueAt(tempPoint, progress);
            spline.derivativeAt(tempDir, progress);
            distances.add(prevPoint.dst(tempPoint));
            float angle = (tempDir.angle() + QUARTER_OF_CIRCLE) % FULL_DEGREES;

            float cos = MathUtils.cosDeg(angle) * CABLE_WIDTH / 2;
            float sin = MathUtils.sinDeg(angle) * CABLE_WIDTH / 2;

            verticesList.add(new Vector2(tempPoint.x + cos, tempPoint.y + sin));
            verticesList.add(new Vector2(tempPoint.x - cos, tempPoint.y - sin));
        }

        //LAST POINT
        prevPoint.set(tempPoint.x, tempPoint.y);
        spline.valueAt(tempPoint, 1);
        distances.add(prevPoint.dst(tempPoint));

        verticesList.add(new Vector2(tempPoint.x, tempPoint.y + CABLE_WIDTH / 2));
        verticesList.add(new Vector2(tempPoint.x, tempPoint.y - CABLE_WIDTH / 2));

        buildMeshFromVertices();
        lastTimeBuild = System.currentTimeMillis();
        changed = false;
    }

    private void buildMeshFromVertices() {
        int distanceIndex = 0;
        int distanceCount = 0;
        int regionY = cableTextureRegion.getRegionY();
        int textureHeight = cableTextureRegion.getRegionHeight();
        TextureRegion texturePart = new TextureRegion(cableTextureRegion);
        texturePart.setRegionY(regionY);
        int cableTextureMaxLenght = regionY + textureHeight;

        for (int i = 0; i < verticesList.size() - 2; i += 2) {
            Vector2 vec1 = verticesList.get(i);
            Vector2 vec2 = verticesList.get(i + 1);
            Vector2 vec3 = verticesList.get(i + 2);
            Vector2 vec4 = verticesList.get(i + 3);

            int distance = (distances.get(distanceIndex).intValue());
            if((texturePart.getRegionY() + distanceCount + distance -2) >= cableTextureMaxLenght){
                distanceCount = 0;
            }
            if(distance > textureHeight){
                distanceCount = 0;
                distance = textureHeight - 1;
            }

            texturePart.setRegionHeight(distance);
            texturePart.setRegionY(regionY + distanceCount);
            float invTextureWidth = 1f / texturePart.getTexture().getWidth();
            float invTextureHeight = 1f / texturePart.getTexture().getHeight();
            texturePart.setRegion(   (texturePart.getRegionX() + UV_TEXTURE_BLEEDING_FIX) * invTextureWidth,
                    (texturePart.getRegionY() + UV_TEXTURE_BLEEDING_FIX) * invTextureHeight,
                    (texturePart.getRegionX() + texturePart.getRegionWidth() - UV_TEXTURE_BLEEDING_FIX) * invTextureWidth,
                    (texturePart.getRegionY() + texturePart.getRegionHeight() - UV_TEXTURE_BLEEDING_FIX) * invTextureHeight);
            distanceIndex++;
            cableSpriteBatch.draw(new float[]{
                            vec1.x, vec1.y,
                            color,
                            texturePart.getU(), texturePart.getV(),

                            vec2.x, vec2.y,
                            color,
                            texturePart.getU2(), texturePart.getV(),

                            vec4.x, vec4.y,
                            color,
                            texturePart.getU2(), texturePart.getV2(),

                            vec3.x, vec3.y,
                            color,
                            texturePart.getU(), texturePart.getV2()
                    },
                    0,
                    CableSpriteBatch.CABLE_PART_SIZE);
            distanceCount += distance;
        }
        verticesList.clear();
        distances.clear();
    }

    public void dispose() {
        if (cableSpriteBatch != null) {
            cableSpriteBatch.dispose();
        }
    }
}
