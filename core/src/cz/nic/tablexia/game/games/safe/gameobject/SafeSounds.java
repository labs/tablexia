/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.gameobject;

import java.util.Locale;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.common.media.AssetDescription;
import cz.nic.tablexia.game.games.safe.assets.SafeAssets;

/**
 * Created by Aneta Steimarová on 5.6.17.
 */

public enum SafeSounds implements AssetDescription {
    //SOUNDS WITHOUT MEANING
    BELL(SafeAssets.BELL, SafeAssets.BELL, SafeAssets.BELL, "bell"),
    CYMBAL(SafeAssets.CYMBAL, SafeAssets.CYMBAL, SafeAssets.CYMBAL, "cymbal"),
    DRUM(SafeAssets.DRUM, SafeAssets.DRUM, SafeAssets.DRUM, "drum"),
    GUITAR_1(SafeAssets.GUITAR_1, SafeAssets.GUITAR_1, SafeAssets.GUITAR_1, "guitar_1"),
    GUITAR_2(SafeAssets.GUITAR_2, SafeAssets.GUITAR_2, SafeAssets.GUITAR_2, "guitar_2"),
    HI_HAT(SafeAssets.HI_HAT, SafeAssets.HI_HAT, SafeAssets.HI_HAT, "hi-hat"),
    PIANO_1(SafeAssets.PIANO_1, SafeAssets.PIANO_1, SafeAssets.PIANO_1, "piano_1"),
    PIANO_2(SafeAssets.PIANO_2, SafeAssets.PIANO_2, SafeAssets.PIANO_2, "piano_2"),
    POT(SafeAssets.POT, SafeAssets.POT, SafeAssets.POT, "pot"),
    RATTLE(SafeAssets.RATTLE, SafeAssets.RATTLE, SafeAssets.RATTLE, "rattle"),
    SAX(SafeAssets.SAX, SafeAssets.SAX, SafeAssets.SAX, "sax"),
    SYNTH(SafeAssets.SYNTH, SafeAssets.SYNTH, SafeAssets.SYNTH, "synth"),
    TRIANGLE(SafeAssets.TRIANGLE, SafeAssets.TRIANGLE, SafeAssets.TRIANGLE, "triangle"),
    WOOD(SafeAssets.WOOD, SafeAssets.WOOD, SafeAssets.WOOD, "wood"),

    //NUMBERS
    ONE(SafeAssets.ONE_CS, SafeAssets.ONE_SK, SafeAssets.ONE_DE, "1"),
    TWO(SafeAssets.TWO_CS, SafeAssets.TWO_SK, SafeAssets.TWO_DE, "2"),
    THREE(SafeAssets.THREE_CS, SafeAssets.THREE_SK, SafeAssets.THREE_DE, "3"),
    FOUR(SafeAssets.FOUR_CS, SafeAssets.FOUR_SK, SafeAssets.FOUR_DE, "4"),
    FIVE(SafeAssets.FIVE_CS, SafeAssets.FIVE_SK, SafeAssets.FIVE_DE, "5"),
    SIX(SafeAssets.SIX_CS, SafeAssets.SIX_SK, SafeAssets.SIX_DE, "6"),
    SEVEN(SafeAssets.SEVEN_CS, SafeAssets.SEVEN_SK, SafeAssets.SEVEN_DE, "7"),
    EIGHT(SafeAssets.EIGHT_CS, SafeAssets.EIGHT_SK, SafeAssets.EIGHT_DE, "8"),
    NINE(SafeAssets.NINE_CS, SafeAssets.NINE_SK, SafeAssets.NINE_DE, "9"),
    TEN(SafeAssets.TEN_CS, SafeAssets.TEN_SK, SafeAssets.TEN_DE, "10"),
    ELEVEN(SafeAssets.ELEVEN_CS, SafeAssets.ELEVEN_SK, SafeAssets.ELEVEN_DE, "11"),
    TWELVE(SafeAssets.TWELVE_CS, SafeAssets.TWELVE_SK, SafeAssets.TWELVE_DE, "12"),
    THIRTEEN(SafeAssets.THIRTEEN_CS, SafeAssets.THIRTEEN_SK, SafeAssets.THIRTEEN_DE, "13"),
    FOURTEEN(SafeAssets.FOURTEEN_CS, SafeAssets.FOURTEEN_SK, SafeAssets.FOURTEEN_DE, "14"),
    FIVETEEN(SafeAssets.FIVETEEN_CS, SafeAssets.FIVETEEN_SK, SafeAssets.FIVETEEN_DE, "15"),
    SIXTEEN(SafeAssets.SIXTEEN_CS, SafeAssets.SIXTEEN_SK, SafeAssets.SIXTEEN_DE, "16"),
    SEVENTEEN(SafeAssets.SEVENTEEN_CS, SafeAssets.SEVENTEEN_SK, SafeAssets.SEVENTEEN_DE, "17"),
    EIGHTEEN(SafeAssets.EIGHTEEN_CS, SafeAssets.EIGHTEEN_SK, SafeAssets.EIGHTEEN_DE, "18"),
    NINETEEN(SafeAssets.NINETEEN_CS, SafeAssets.NINETEEN_SK, SafeAssets.NINETEEN_DE, "19"),
    TWENTY(SafeAssets.TWENTY_CS, SafeAssets.TWENTY_SK, SafeAssets.TWENTY_DE, "20"),
    TWENTYONE(SafeAssets.TWENTYONE_CS, SafeAssets.TWENTYONE_SK, SafeAssets.TWENTYONE_DE, "21"),
    TWENTYTWO(SafeAssets.TWENTYTWO_CS, SafeAssets.TWENTYTWO_SK, SafeAssets.TWENTYTWO_DE, "22"),
    TWENTYTHREE(SafeAssets.TWENTYTHREE_CS, SafeAssets.TWENTYTHREE_SK, SafeAssets.TWENTYTHREE_DE, "23"),
    TWENTYFOUR(SafeAssets.TWENTYFOUR_CS, SafeAssets.TWENTYFOUR_SK, SafeAssets.TWENTYFOUR_DE, "24"),
    TWENTYFIVE(SafeAssets.TWENTYFIVE_CS, SafeAssets.TWENTYFIVE_SK, SafeAssets.TWENTYFIVE_DE, "25"),
    TWENTYSIX(SafeAssets.TWENTYSIX_CS, SafeAssets.TWENTYSIX_SK, SafeAssets.TWENTYSIX_DE, "26"),
    TWENTYSEVEN(SafeAssets.TWENTYSEVEN_CS, SafeAssets.TWENTYSEVEN_SK, SafeAssets.TWENTYSEVEN_DE, "27"),
    TWENTYEIGHT(SafeAssets.TWENTYEIGHT_CS, SafeAssets.TWENTYEIGHT_SK, SafeAssets.TWENTYEIGHT_DE, "28"),
    TWENTYNINE(SafeAssets.TWENTYNINE_CS, SafeAssets.TWENTYNINE_SK, SafeAssets.TWENTYNINE_DE, "29"),
    THIRTY(SafeAssets.THIRTY_CS, SafeAssets.THIRTY_SK, SafeAssets.THIRTY_DE, "30"),

    //ALPHABET
    A(SafeAssets.A_CS, SafeAssets.A_SK, SafeAssets.A_DE, "A"),
    B(SafeAssets.B_CS, SafeAssets.B_SK, SafeAssets.B_DE, "B"),
    C(SafeAssets.C_CS, SafeAssets.C_SK, SafeAssets.C_DE, "C"),
    D(SafeAssets.D_CS, SafeAssets.D_SK, SafeAssets.D_DE, "D"),
    E(SafeAssets.E_CS, SafeAssets.E_SK, SafeAssets.E_DE, "E"),
    F(SafeAssets.F_CS, SafeAssets.F_SK, SafeAssets.F_DE, "F"),
    G(SafeAssets.G_CS, SafeAssets.G_SK, SafeAssets.G_DE, "G"),
    H(SafeAssets.H_CS, SafeAssets.H_SK, SafeAssets.H_DE, "H"),
    I(SafeAssets.I_CS, SafeAssets.I_SK, SafeAssets.I_DE, "I"),
    J(SafeAssets.J_CS, SafeAssets.J_SK, SafeAssets.J_DE, "J"),
    K(SafeAssets.K_CS, SafeAssets.K_SK, SafeAssets.K_DE, "K"),
    L(SafeAssets.L_CS, SafeAssets.L_SK, SafeAssets.L_DE, "L"),
    M(SafeAssets.M_CS, SafeAssets.M_SK, SafeAssets.M_DE, "M"),
    N(SafeAssets.N_CS, SafeAssets.N_SK, SafeAssets.N_DE, "N"),
    O(SafeAssets.O_CS, SafeAssets.O_SK, SafeAssets.O_DE, "O"),
    P(SafeAssets.P_CS, SafeAssets.P_SK, SafeAssets.P_DE, "P"),
    Q(SafeAssets.Q_CS, SafeAssets.Q_SK, SafeAssets.Q_DE, "Q"),
    R(SafeAssets.R_CS, SafeAssets.R_SK, SafeAssets.R_DE, "R"),
    S(SafeAssets.S_CS, SafeAssets.S_SK, SafeAssets.S_DE, "S"),
    T(SafeAssets.T_CS, SafeAssets.T_SK, SafeAssets.T_DE, "T"),
    U(SafeAssets.U_CS, SafeAssets.U_SK, SafeAssets.U_DE, "U"),
    V(SafeAssets.V_CS, SafeAssets.V_SK, SafeAssets.V_DE, "V"),
    W(SafeAssets.W_CS, SafeAssets.W_SK, SafeAssets.W_DE, "W"),
    X(SafeAssets.X_CS, SafeAssets.X_SK, SafeAssets.X_DE, "X"),
    Y(SafeAssets.Y_CS, SafeAssets.Y_SK, SafeAssets.Y_DE, "Y"),
    Z(SafeAssets.Z_CS, SafeAssets.Z_SK, SafeAssets.Z_DE, "Z"),

    //SILENCE SOUND
    SILENCE(SafeAssets.SILENCE, SafeAssets.SILENCE, SafeAssets.SILENCE, "    ");

    private String soundPathCS;
    private String soundPathSK;
    private String soundPathDE;
    private String soundName;

    SafeSounds(String soundPathCS, String soundPathSK, String soundPathDE, String soundName) {
        this.soundPathCS = soundPathCS;
        this.soundPathSK = soundPathSK;
        this.soundPathDE = soundPathDE;
        this.soundName = soundName;
    }

    public String getSoundName() {
        return soundName;
    }

    @Override
    public String getResource() {
        return getSoundPath();
    }

    public String getSoundPath() {
        return LocaleSafeSounds.getSoundPathByLocale(TablexiaSettings.getInstance().getLocale(), this);
    }

    enum LocaleSafeSounds {
        CS(TablexiaSettings.LocaleDefinition.cs_CZ.getLocale()) {
            @Override
            public String getSoundPathFromSounds(SafeSounds safeSounds) {
                return safeSounds.soundPathCS;
            }
        },
        SK(TablexiaSettings.LocaleDefinition.sk_SK.getLocale()) {
            @Override
            public String getSoundPathFromSounds(SafeSounds safeSounds) {
                return safeSounds.soundPathSK;
            }
        },
        DE(TablexiaSettings.LocaleDefinition.de_DE.getLocale()) {
            @Override
            public String getSoundPathFromSounds(SafeSounds safeSounds) {
                return safeSounds.soundPathDE;
            }
        };

        Locale locale;

        LocaleSafeSounds(Locale locale) {
            this.locale = locale;
        }

        public static String getSoundPathByLocale(Locale locale, SafeSounds safeSounds) {
            for (LocaleSafeSounds current : values()) {
                if (locale == current.locale)
                    return current.getSoundPathFromSounds(safeSounds);
            }

            throw new RuntimeException("Couldn't get a sound path for " + safeSounds + " in locale " + locale);
        }

        public String getSoundPathFromSounds(SafeSounds safeSounds) {
            throw new RuntimeException("Path for sound " + safeSounds + " doesn't exists.");
        }
    }
}
