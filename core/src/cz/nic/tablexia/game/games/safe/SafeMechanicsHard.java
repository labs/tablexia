/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.pursuit.helper.ArithmeticsHelper;
import cz.nic.tablexia.game.games.safe.assets.SafeAssets;
import cz.nic.tablexia.game.games.safe.gameobject.SafeLightHardDefinition;
import cz.nic.tablexia.game.games.safe.gameobject.SafeLightImage;
import cz.nic.tablexia.game.games.safe.model.SafeSequence;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Point;

/**
 * Created by Aneta Steimarová on 28.7.17.
 */

public class SafeMechanicsHard extends AbstractMechanics {
    private static final int    SAFE_POINTER_WIDTH      = 50;
    private static final int    SAFE_POINTER_HEIGHT     = 250;
    private static final int    SAFE_PEAK_WIDTH         = 398;
    private static final int    SAFE_PEAK_HEIGHT        = 565;
    private static final int    SAFE_LIGHT_SIZE         = 50;
    private static final int    SAFE_LIGHT_OFFSET       = 20;
    private static final int    MIN_POINTER_Y           = 130;
    private static final int    SAFE_POINTER_CENTER     = 2;
    private static final float  SAFE_POINTER_ORIGIN     = 0.1f;
    private static final float  SAFE_POINTER_HEIGH      = 0.6f;


    private List<SafeLightImage> listOfLights;
    private Image safePointer;
    private DragListener dragListener;

    @Override
    void prepare(SafeGame safeGame, SafeSequence safeSequence) {
        Image safePeak = new Image(safeGame.getScreenTextureRegion(SafeAssets.PEAK));
        safePeak.setSize(SAFE_PEAK_WIDTH, SAFE_PEAK_HEIGHT);
        safePeak.setPosition(getWidth() / 2 - safePeak.getWidth() / 2, safeGame.getSceneOuterBottomY());
        addActor(safePeak);

        safePointer = new Image(safeGame.getScreenTextureRegion(SafeAssets.POINTER));
        safePointer.setSize(SAFE_POINTER_WIDTH, SAFE_POINTER_HEIGHT);
        safePointer.setOrigin(safePointer.getWidth() / 2, safePointer.getHeight() * SAFE_POINTER_ORIGIN);
        safePointer.setPosition((safePeak.getX() + safePeak.getWidth() / 2 - safePointer.getWidth() / 2) - SAFE_POINTER_CENTER, safePeak.getY() + safePeak.getHeight() * SAFE_POINTER_HEIGH);
        safePointer.addListener(dragListener = new DragListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                holding = true;
                setButtonsDisabled(safeGame);
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void drag(InputEvent event, float x, float y, int pointer) {
                super.drag(event, x, y, pointer);
                Vector2 fingerCoords = safePointer.localToStageCoordinates(new Vector2(x, y));
                if (fingerCoords.y > MIN_POINTER_Y) {
                    rotateSafeCircleTo(fingerCoords, safePointer);
                    compareFingerCoordsWithLightPositions(fingerCoords, safeGame, listOfLights);
                }
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                holding = false;
                setButtonsEnabled(safeGame);
                if (safeGame.getSelectedLight() != null) {
                    rotateSafeCircleTo(safeGame.getSelectedLight().localToStageCoordinates(new Vector2(safeGame.getSelectedLight().getWidth() / 2, safeGame.getSelectedLight().getHeight() / 2)), safePointer);
                    safeGame.getDoneButton().setEnabled();
                }
            }
        });

        Point circleCenter = new Point(safePointer.getX() + safePointer.getWidth() / 2, safePointer.getY() + 10);
        listOfLights = new ArrayList<>();
        for (int i = 0; i < safeSequence.getSoundsToLights().length; i++) {
            SafeLightHardDefinition safeLightHardDefinition = SafeLightHardDefinition.values()[i];
            float angle = safeLightHardDefinition.getAngle();
            Image safeLight = new Image(safeGame.getScreenTextureRegion(safeLightHardDefinition.getLight()));
            safeLight.setSize(SAFE_LIGHT_SIZE + 50, SAFE_LIGHT_SIZE + 50);
            Point lightPosition = ArithmeticsHelper.getPointOnCircle(circleCenter, safePointer.getHeight() + SAFE_LIGHT_OFFSET, angle);
            safeLight.setPosition(lightPosition.x - safeLight.getWidth() / 2, lightPosition.y - safeLight.getHeight() / 2);
            SafeLightImage lightImage = new SafeLightImage(
                    safeGame,
                    safeLightHardDefinition.getGreyTextureRegion(),
                    safeLightHardDefinition.getRedTextureRegion(),
                    safeLightHardDefinition.getGreenTextureRegion(),
                    safeLightHardDefinition.getBlueTextureRegion(),
                    safeGame.getMusic(safeSequence.getSoundsToLights()[i].getSoundPath()),
                    angle);
            lightImage.setSize(SAFE_LIGHT_SIZE, SAFE_LIGHT_SIZE);
            lightImage.setPosition(safeLight.getX() + safeLight.getWidth() / 2 - lightImage.getWidth() / 2, safeLight.getY() + safeLight.getHeight() / 2 - lightImage.getHeight() / 2);
            lightImage.setInputListener(new InputListener() {
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
                    lightTheLight(safeGame, lightImage);
                }

                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if(button == Input.Buttons.RIGHT) return false;
                    return true;
                }
            });
            lightImage.addListener(lightImage.getInputListener());
            lightImage.setName(LIGHT_NAME + i);
            listOfLights.add(lightImage);
            addActor(safeLight);
            addActor(lightImage);
        }

        addActor(safePointer);
    }

    @Override
    public void setSafeActorsUntouchable() {
        for (SafeLightImage safeLightImage : listOfLights) {
            safeLightImage.clearListeners();
        }
        safePointer.clearListeners();
    }

    @Override
    public void setSafeActorsTouchable() {
        for (SafeLightImage safeLightImage : listOfLights) {
            safeLightImage.addListener(safeLightImage.getInputListener());
        }
        safePointer.addListener(dragListener);
    }

    @Override
    public void setDoneButtonDisOrEnabled(SafeGame safeGame) {
        if (safeGame.getSelectedLight() != null) {
            rotateSafeCircleTo(safeGame.getSelectedLight().localToStageCoordinates(new Vector2(safeGame.getSelectedLight().getWidth() / 2, safeGame.getSelectedLight().getHeight() / 2)), safePointer);
            safeGame.getDoneButton().setEnabled();
        }
    }

    @Override
    public void doBeforeEvaluatingAnimation(SequenceAction sa) {}

    public void lightTheLight(SafeGame safeGame, SafeLightImage lightImage) {
        safeGame.setSelectedLight(lightImage, listOfLights, true);

        Vector2 lightImageCoords = lightImage.localToStageCoordinates(new Vector2(lightImage.getWidth() / 2, lightImage.getHeight() / 2));
        rotateSafeCircleTo(lightImageCoords, safePointer);
        AbstractTablexiaScreen.triggerScenarioStepEvent(LIGHT_LIGHTED_UP);
    }
}
