/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.safe;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.List;

import cz.nic.tablexia.game.games.safe.gameobject.SafeLightImage;
import cz.nic.tablexia.game.games.safe.model.SafeSequence;

public abstract class AbstractMechanics extends Group {
    private static final int        ANGLE_SPREAD            = 17;
    private static final int        FULL_DEGREES            = 360;
    private static final int        QUARTER_OF_CIRCLE       = 90;
    public static final String      LIGHT_NAME              = "light";
    public static final String      LIGHT_LIGHTED_UP        = "light lighted up";
    public static boolean holding = false;

    abstract void prepare(SafeGame safeGame, SafeSequence sequence);

    public void dispose() {

    }

    protected void compareFingerCoordsWithLightPositions(Vector2 fingerCoords, SafeGame safeGame, List<SafeLightImage> listOfLights) {
        float angle = fingerCoords.angle();
        boolean foundValid = false;
        for (SafeLightImage lightImage : listOfLights) {
            float testedAngle = lightImage.getAngle();
            if (testedAngle < 0) testedAngle += FULL_DEGREES;
            if (testedAngle - ANGLE_SPREAD <= angle && testedAngle + ANGLE_SPREAD > angle) {
                foundValid = true;
                if (!lightImage.equals(safeGame.getSelectedLight())) {
                    SafeLightImage.turnOffAllLights(listOfLights);
                    safeGame.setSelectedLight(lightImage, listOfLights, true);
                    return;
                }
            }
        }

        if (!foundValid) {
            SafeLightImage.turnOffAllLights(listOfLights);
            safeGame.setSelectedLight(null, listOfLights, false);
        }
    }

    protected void rotateSafeCircleTo(Vector2 dest, Image rotationObject) {
        Vector2 safeOrigin = rotationObject.localToStageCoordinates(new Vector2(rotationObject.getOriginX(), rotationObject.getOriginY()));
        float angle = dest.sub(safeOrigin).angle() - QUARTER_OF_CIRCLE;
        rotationObject.setRotation(angle);
    }

    public abstract void setSafeActorsUntouchable();

    public abstract void setSafeActorsTouchable();

    protected void setButtonsDisabled(SafeGame safeGame) {
        safeGame.getDoneButton().setDisabled();
        safeGame.getReplayButton().setDisabled();
        safeGame.getHelpButton().setDisabled();
    }

    public void setButtonsEnabled(SafeGame safeGame) {
        safeGame.getReplayButton().setEnabled();
        if (!safeGame.isUsedHelpButton()) safeGame.getHelpButton().setEnabled();
    }

    public abstract void setDoneButtonDisOrEnabled(SafeGame safeGame);

    public abstract void doBeforeEvaluatingAnimation(SequenceAction sa);
}