/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.gameobject;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.List;

import cz.nic.tablexia.game.games.safe.SafeGame;

/**
 * Created by Aneta Steimarová on 25.5.17.
 */

public class SafeLightImage extends Image {
    private static final float  DEFAULT_ANGLE   = -1;
    private static final String GREY            = "grey";
    private static final String RED             = "red";
    private static final String BLUE            = "blue";
    private static final String GREEN           = "green";

    private TextureRegion screenTextureGrey;
    private TextureRegion screenTextureRed;
    private TextureRegion screenTextureGreen;
    private TextureRegion screenTextureBlue;
    private Music sound;
    private float angle;
    private InputListener inputListener;
    private String textureColor;

    public void setInputListener(InputListener inputListener) {
        this.inputListener = inputListener;
    }

    public InputListener getInputListener() {
        return inputListener;
    }

    public SafeLightImage(SafeGame safeGame, String greyTextureRegion, String redTextureRegion, String greenTextureRegion, String blueTextureRegion, Music music, float angle) {
        super(safeGame.getScreenTextureRegion(greyTextureRegion));
        this.screenTextureGrey = safeGame.getScreenTextureRegion(greyTextureRegion);
        this.screenTextureRed = safeGame.getScreenTextureRegion(redTextureRegion);
        this.screenTextureGreen = safeGame.getScreenTextureRegion(greenTextureRegion);
        this.screenTextureBlue = safeGame.getScreenTextureRegion(blueTextureRegion);
        this.sound = music;
        this.angle = angle;
        this.textureColor = GREY;
        safeGame.addMusicToMusicList(sound);
    }

    public SafeLightImage(SafeGame safeGame, String greyTextureRegion, String redTextureRegion, String greenTextureRegion, String blueTextureRegion, Music music) {
        super(safeGame.getScreenTextureRegion(greyTextureRegion));
        this.screenTextureGrey = safeGame.getScreenTextureRegion(greyTextureRegion);
        this.screenTextureRed = safeGame.getScreenTextureRegion(redTextureRegion);
        this.screenTextureGreen = safeGame.getScreenTextureRegion(greenTextureRegion);
        this.screenTextureBlue = safeGame.getScreenTextureRegion(blueTextureRegion);
        this.sound = music;
        this.angle = DEFAULT_ANGLE;
        this.textureColor = GREY;
        safeGame.addMusicToMusicList(sound);
    }

    public Music getSound() {
        return sound;
    }

    public float getAngle() {
        return angle;
    }

    public void lightGreen() {
        textureColor = GREEN;
        setDrawable(new TextureRegionDrawable(screenTextureGreen));
    }

    public void lightRed() {
        textureColor = RED;
        setDrawable(new TextureRegionDrawable(screenTextureRed));
    }

    public void lightBlue() {
        if(this.textureColor == GREY){
            textureColor = BLUE;
            setDrawable(new TextureRegionDrawable(screenTextureBlue));
        }
    }

    public void lightGrey() {
        textureColor = GREY;
        setDrawable(new TextureRegionDrawable(screenTextureGrey));
    }

    public void light(List<SafeLightImage> listOfLightsToTurnOff, SafeLightImage lightToStaysLight) {
        lightWithoutSound(listOfLightsToTurnOff, lightToStaysLight);
        this.getSound().play();
    }

    /**
     * Light the selected light
     *
     * @param listOfLightsToTurnOff = list of all lights that will be turn off (set texture to grey)
     * @param lightToStaysLight     = light that will stay turned on (null if no light should stay turn on)
     */
    public void lightWithoutSound(List<SafeLightImage> listOfLightsToTurnOff, SafeLightImage lightToStaysLight) {
        this.lightBlue();
        for (SafeLightImage lightImage : listOfLightsToTurnOff) {
            if (this != null && !this.equals(lightImage)) {
                if (lightToStaysLight == null || !lightToStaysLight.equals(lightImage))
                    lightImage.lightGrey();
            }
        }
    }

    public static void turnOffAllLights(List<SafeLightImage> listOfLightsToTurnOff) {
        if (listOfLightsToTurnOff != null) {
            for (SafeLightImage lightImage : listOfLightsToTurnOff) {
                lightImage.lightGrey();
            }
        }
    }
}
