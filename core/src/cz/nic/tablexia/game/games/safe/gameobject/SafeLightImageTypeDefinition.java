/*
 *
 *  * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  
 */

package cz.nic.tablexia.game.games.safe.gameobject;

import cz.nic.tablexia.game.games.safe.assets.SafeAssets;

public enum SafeLightImageTypeDefinition {

    TYPE_1(SafeAssets.LIGHT1_GREY, SafeAssets.LIGHT1_RED, SafeAssets.LIGHT1_GREEN, SafeAssets.LIGHT1_BLUE),
    TYPE_2(SafeAssets.LIGHT2_GREY, SafeAssets.LIGHT2_RED, SafeAssets.LIGHT2_GREEN, SafeAssets.LIGHT2_BLUE),
    TYPE_3(SafeAssets.LIGHT3_GREY, SafeAssets.LIGHT3_RED, SafeAssets.LIGHT3_GREEN, SafeAssets.LIGHT3_BLUE);

    private String greyTextureRegion;
    private String redTextureRegion;
    private String greenTextureRegion;
    private String blueTextureRegion;

    SafeLightImageTypeDefinition(String greyTextureRegion, String redTextureRegion, String greenTextureRegion, String blueTextureRegion) {
        this.greyTextureRegion = greyTextureRegion;
        this.redTextureRegion = redTextureRegion;
        this.greenTextureRegion = greenTextureRegion;
        this.blueTextureRegion = blueTextureRegion;
    }


    public String getGreyTextureRegion() {
        return greyTextureRegion;
    }

    public String getBlueTextureRegion() {
        return blueTextureRegion;
    }

    public String getRedTextureRegion() {
        return redTextureRegion;
    }

    public String getGreenTextureRegion() {
        return greenTextureRegion;
    }
}