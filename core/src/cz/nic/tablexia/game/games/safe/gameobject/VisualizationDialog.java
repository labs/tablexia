/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.safe.gameobject;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.safe.SafeGame;
import cz.nic.tablexia.game.games.safe.assets.SafeAssets;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.button.TablexiaButton;


/**
 * Created by Aneta Steimarová on 4.1.18.
 */

public class VisualizationDialog {
    private static final int    VISUALIZATION_OK_WIDTH              = 50;
    private static final int    VISUALIZATION_OK_HEIGHT             = 50;
    private static final String VISUALIZATION_OK_TEXT               = "OK";

    private static final float  BUTTON_Y_POSITION_RATIO             = 0.15f;
    private static final float  VIZUALIZATION_PAPER_WIDTH           = 0.95f;
    private static final float  VIZUALIZATION_PAPER_HEIGHT          = 0.4f;
    private static final int    VIZUALIZATION_PAPER_LEFT_BORDER     = 5;
    private static final float  VISUALIZATION_DIALOG_PAPER_PART     = 0.85f;

    private static final float  CHARACTER_SPACE                     = 40;
    private static final float  ONE_IMAGE_WITH_SPACE                = 40;
    private static final float  ONE_IMAGE_WIDTH_AND_HEIGHT          = 30;
    private static final float  IMAGE_SPACE                         = 10;
    private static final Color  CHARACTER_COLOR                     = Color.BLACK;

    private static final int    ALPHA                               = 0;

    SafeGame safeGame;
    Group paperVizualizationGroup;

    public VisualizationDialog(SafeGame safeGame, Image dimmer) {
        this.safeGame = safeGame;
        Group vizualizationGroup = new Group();
        Image paper = new Image(safeGame.getScreenTextureRegion(SafeAssets.HELP_PAPER));
        float paperWidth = safeGame.getSceneWidth() * VIZUALIZATION_PAPER_WIDTH;
        float paperHeight = safeGame.getSceneInnerHeight() * VIZUALIZATION_PAPER_HEIGHT;

        float paperX = (safeGame.getSceneWidth() / 2) - (paperWidth / 2);
        float paperY = (safeGame.getSceneInnerHeight() / 2) - (paperHeight / 2);

        paper.setBounds(paperX, paperY, paperWidth, paperHeight);
        vizualizationGroup.addActor(paper);

        paperVizualizationGroup = new Group();  //part of paper for writing help into
        paperVizualizationGroup.setBounds(paper.getX(), paper.getY(), VISUALIZATION_DIALOG_PAPER_PART * paperWidth, paperHeight);
        vizualizationGroup.addActor(paperVizualizationGroup);

        List<Actor> listOfChars = new ArrayList<Actor>();
        if (safeGame.getGameDifficulty().equals(GameDifficulty.EASY))
            addImagesToSounds(vizualizationGroup, paper, listOfChars);
        else
            addCharactersToSounds(vizualizationGroup, paper, listOfChars);


        TablexiaButton buttonOK = new StandardTablexiaButton(VISUALIZATION_OK_TEXT);
        buttonOK.setSize(VISUALIZATION_OK_WIDTH, VISUALIZATION_OK_HEIGHT);
        buttonOK.setPosition(
                paper.getX() + ((paperVizualizationGroup.getWidth() / 2) - (buttonOK.getWidth() / 2)),
                paperY + (paperHeight * BUTTON_Y_POSITION_RATIO)
        );
        buttonOK.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hideDialogAndSetButtonsEnabled(vizualizationGroup);
                dimmer.addAction(Actions.sequence(Actions.fadeOut(SafeGame.FADE_ANIMATION_DURATION), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        dimmer.remove();
                    }
                })));
            }
        });
        buttonOK.setDisabled();

        vizualizationGroup.addActor(buttonOK);
        vizualizationGroup.setVisible(false);
        safeGame.getStage().addActor(vizualizationGroup);

        vizualizationGroup.addAction(Actions.sequence(Actions.alpha(ALPHA), Actions.visible(true), Actions.fadeIn(SafeGame.FADE_ANIMATION_DURATION), Actions.run(new Runnable() {
            @Override
            public void run() {
                safeGame.getSequenceGenerator().playSequenceVisualization(safeGame.getSafeSequence(), safeGame, listOfChars, Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        buttonOK.setEnabled();
                    }
                }));
            }
        })));
    }

    private void addImagesToSounds(Group vizualizationGroup, Image paper, List<Actor> listOfChars) {
        Group soundImageGroup = new Group();
        int space = VIZUALIZATION_PAPER_LEFT_BORDER;
        for (SafeSounds safeSound : safeGame.getSafeSequence().getGeneratedSoundSequence()) {
            if (safeSound.equals(SafeSounds.SILENCE)) {
                space += ONE_IMAGE_WITH_SPACE;
            } else
                space = createAndAddImage(safeSound, soundImageGroup, space, ONE_IMAGE_WIDTH_AND_HEIGHT, IMAGE_SPACE, listOfChars);
        }

        for (int i = 0; i < safeGame.getSafeSequence().getPlayAgainFirstXSounds(); i++) {
            SafeSounds safeSound = safeGame.getSafeSequence().getGeneratedSoundSequence()[i];
            if (safeSound.equals(SafeSounds.SILENCE)) {
                space += ONE_IMAGE_WITH_SPACE;
            } else
                space = createAndAddImage(safeSound, soundImageGroup, space, ONE_IMAGE_WIDTH_AND_HEIGHT, IMAGE_SPACE, listOfChars);
        }
        soundImageGroup.setWidth(space);
        makePositioning(soundImageGroup, paper, space, IMAGE_SPACE);
        vizualizationGroup.addActor(soundImageGroup);
    }

    private void makePositioning(Group soundImageGroup, Image paper, int space, float imageSpace) {
        soundImageGroup.setPosition(paper.getX() + paperVizualizationGroup.getWidth() / 2 - soundImageGroup.getWidth() / 2, paper.getY() + paper.getHeight() / 2);
    }

    private int createAndAddImage(SafeSounds safeSound, Group soundImageGroup, int space, float oneImageWidthanHeight, float imageSpace, List<Actor> listOfChars) {
        int imageID = safeGame.getSafeSequence().findSoundPositionInSoundsToSequence(safeSound);
        if (imageID < 0) {
            Log.err(getClass(), "Could not found ID for sound " + safeSound.getSoundPath());
            return space;
        }
        Image soundImage = new Image(safeGame.getScreenTextureRegion(VisualizationObject.getImageForID(imageID)));
        soundImage.setColor(CHARACTER_COLOR);
        soundImage.setWidth(oneImageWidthanHeight);
        soundImage.setHeight(oneImageWidthanHeight);
        soundImage.setPosition(space + soundImage.getWidth() / 2, 0);
        listOfChars.add(soundImage);
        space += oneImageWidthanHeight + imageSpace;
        soundImageGroup.addActor(soundImage);
        return space;
    }

    private void addCharactersToSounds(Group vizualizationGroup, Image paper, List<Actor> listOfChars) {
        Group soundImageGroup = new Group();
        int space = VIZUALIZATION_PAPER_LEFT_BORDER;
        for (SafeSounds safeSound : safeGame.getSafeSequence().getGeneratedSoundSequence()) {
            TablexiaLabel textLabel = new TablexiaLabel(safeSound.getSoundName(), new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_35, Color.WHITE), false);
            textLabel.setColor(CHARACTER_COLOR);
            textLabel.setPosition(space + textLabel.getWidth() / 2, 0);
            space += textLabel.getWidth() + CHARACTER_SPACE;
            soundImageGroup.addActor(textLabel);
            listOfChars.add(textLabel);
        }
        soundImageGroup.setWidth(space);
        makePositioning(soundImageGroup, paper, space, CHARACTER_SPACE);
        vizualizationGroup.addActor(soundImageGroup);
    }

    private void hideDialogAndSetButtonsEnabled(Group vizualizationGroup) {
        vizualizationGroup.addAction(Actions.sequence(Actions.fadeOut(SafeGame.FADE_OUT_ANIMATION_DURATION), Actions.after(Actions.run(new Runnable() {
            @Override
            public void run() {
                vizualizationGroup.clearChildren();
                if (safeGame.getSelectedLight() != null) safeGame.getDoneButton().setEnabled();
                safeGame.getReplayButton().setEnabled();
                safeGame.getSafeGroup().setSafeActorsTouchable();
                safeGame.setVisualizationVisible(false);
            }
        }))));
    }
}
