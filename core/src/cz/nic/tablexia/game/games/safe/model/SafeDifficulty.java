/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.games.safe.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.safe.AbstractMechanics;
import cz.nic.tablexia.game.games.safe.SafeMechanicsBonus;
import cz.nic.tablexia.game.games.safe.SafeMechanicsEasy;
import cz.nic.tablexia.game.games.safe.SafeMechanicsHard;
import cz.nic.tablexia.game.games.safe.SafeMechanicsMedium;
import cz.nic.tablexia.util.Log;

/**
 * Created by Aneta Steimarová on 17.5.17.
 */

public enum SafeDifficulty {
    EASY(GameDifficulty.EASY, new SafeMechanicsEasy(), SafeEasyLevelDefinition.makeArrayOfListOfSequences()),
    MEDIUM(GameDifficulty.MEDIUM, new SafeMechanicsMedium(), SafeMediumLevelDefinition.makeArrayOfListOfSequences()),
    HARD(GameDifficulty.HARD, new SafeMechanicsHard(), SafeHardLevelDefinition.makeArrayOfListOfSequences()),
    BONUS(GameDifficulty.BONUS, new SafeMechanicsBonus(), SafeBonusLevelDefinition.makeArrayOfListOfSequences());

    private GameDifficulty gameDifficulty;
    private AbstractMechanics mechanics;
    private ArrayList<List<SafeSequence>> arrayListOfsafeSequences;

    SafeDifficulty(GameDifficulty gameDifficulty, AbstractMechanics mechanics, ArrayList<List<SafeSequence>> arrayListOfsafeSequences) {
        this.gameDifficulty = gameDifficulty;
        this.mechanics = mechanics;
        this.arrayListOfsafeSequences = arrayListOfsafeSequences;
    }

    public GameDifficulty getGameDifficulty() {
        return gameDifficulty;
    }

    public AbstractMechanics getMechanics() {
        return mechanics;
    }

    public static SafeDifficulty getSafeDifficultyForGameDifficulty(GameDifficulty gameDifficulty) throws IllegalArgumentException {
        for (SafeDifficulty safeDifficulty : SafeDifficulty.values()) {
            if (safeDifficulty.gameDifficulty == gameDifficulty) {
                return safeDifficulty;
            }
        }
        throw new IllegalArgumentException();
    }

    /**
     * Generate random safe sequence for specific difficulty and level
     *
     * @param gameDifficulty
     * @param level
     * @return
     */
    public static SafeSequence getGeneratedSequence(GameDifficulty gameDifficulty, int level) {
        Random random = new Random();
        List<SafeSequence> listOfSafeSequences;
        for (SafeDifficulty safeDifficulty : SafeDifficulty.values()) {
            if (safeDifficulty.getGameDifficulty().equals(gameDifficulty)) {
                listOfSafeSequences = safeDifficulty.arrayListOfsafeSequences.get(level);
                return listOfSafeSequences.get(random.nextInt(listOfSafeSequences.size()));
            }
        }
        Log.err("getGeneratedSequence", "Undefined safe difficulty!");
        throw new IllegalArgumentException();
    }
}
