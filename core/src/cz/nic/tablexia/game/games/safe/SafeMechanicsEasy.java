/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.safe;


import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.games.pursuit.helper.ArithmeticsHelper;
import cz.nic.tablexia.game.games.safe.assets.SafeAssets;
import cz.nic.tablexia.game.games.safe.gameobject.SafeLightImage;
import cz.nic.tablexia.game.games.safe.gameobject.SafeLightImageTypeDefinition;
import cz.nic.tablexia.game.games.safe.model.SafeSequence;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Point;

public class SafeMechanicsEasy extends AbstractMechanics {

    private static final int    SAFE_BACKGROUND_OFFSET  = 230;
    private static final int    SAFE_SIDE_OFFSET        = 250;
    private static final int    SAFE_LIGHT_OFFSET       = 30;
    private static final int    SAFE_LIGHT_SIZE         = 50;
    private static final float  ANGLE_INCREASE          = 45;
    private static final float  STARTING_ANGLE          = 60;

    private List<SafeLightImage> listOfLights;
    private Image safeCircle;
    private DragListener dragListenerForSafeCircle;

    @Override
    void prepare(SafeGame safeGame, SafeSequence safeSequence) {
        safeGame.setSelectedLight(null, listOfLights, false);
        listOfLights = null;
        Image safeBackground = new Image(safeGame.getScreenTextureRegion(SafeAssets.CIRCLE_BACKGROUND));
        safeBackground.setSize(getHeight() - SAFE_BACKGROUND_OFFSET, getHeight() - SAFE_BACKGROUND_OFFSET);
        safeBackground.setPosition(getWidth() / 2 - safeBackground.getWidth() / 2, getHeight() / 2 - safeBackground.getHeight() / 2);
        addActor(safeBackground);

        safeCircle = new Image(safeGame.getScreenTextureRegion(SafeAssets.CIRCLE));

        safeCircle.setSize(getHeight() - SAFE_SIDE_OFFSET, getHeight() - SAFE_SIDE_OFFSET);
        safeCircle.setPosition(getWidth() / 2 - safeCircle.getWidth() / 2, getHeight() / 2 - safeCircle.getHeight() / 2);

        safeCircle.setOrigin(safeCircle.getWidth() / 2, safeCircle.getWidth() / 2);

        safeCircle.addListener(dragListenerForSafeCircle = new DragListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(button == Input.Buttons.RIGHT) return false;
                holding = true;
                setButtonsDisabled(safeGame);
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void drag(InputEvent event, float x, float y, int pointer) {
                super.drag(event, x, y, pointer);
                Vector2 fingerCoords = safeCircle.localToStageCoordinates(new Vector2(x, y));
                rotateSafeCircleTo(fingerCoords, safeCircle);
                compareFingerCoordsWithLightPositions(fingerCoords, safeGame, listOfLights);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                holding = false;
                setButtonsEnabled(safeGame);
                if (safeGame.getSelectedLight() != null) {
                    rotateSafeCircleTo(safeGame.getSelectedLight().localToStageCoordinates(new Vector2(safeGame.getSelectedLight().getWidth() / 2, safeGame.getSelectedLight().getHeight() / 2)), safeCircle);
                    safeGame.getDoneButton().setEnabled();
                    ;
                }
            }
        });

        addActor(safeCircle);

        listOfLights = new ArrayList<>();
        Point circleCenter = new Point(safeCircle.getX() + safeCircle.getOriginX(), safeCircle.getY() + safeCircle.getOriginY());
        for (int i = 0; i < safeSequence.getSoundsToLights().length; i++) {
            float angle = STARTING_ANGLE - ANGLE_INCREASE * i;
            SafeLightImageTypeDefinition safeLightImageTypeDefinition = SafeLightImageTypeDefinition.values()[i % SafeLightImageTypeDefinition.values().length];
            SafeLightImage lightImage = new SafeLightImage(
                    safeGame,
                    safeLightImageTypeDefinition.getGreyTextureRegion(),
                    safeLightImageTypeDefinition.getRedTextureRegion(),
                    safeLightImageTypeDefinition.getGreenTextureRegion(),
                    safeLightImageTypeDefinition.getBlueTextureRegion(),
                    safeGame.getMusic(safeSequence.getSoundsToLights()[i].getSoundPath()),
                    angle);

            lightImage.setSize(SAFE_LIGHT_SIZE, SAFE_LIGHT_SIZE);
            Point lightPosition = ArithmeticsHelper.getPointOnCircle(circleCenter, safeBackground.getWidth() / 2 + SAFE_LIGHT_OFFSET, angle);
            lightImage.setPosition(lightPosition.x - lightImage.getWidth() / 2, lightPosition.y - lightImage.getHeight() / 2);
            lightImage.setInputListener(new InputListener() {
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
                    lightTheLight(safeGame, lightImage);
                }

                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if(button == Input.Buttons.RIGHT) return false;
                    return true;
                }
            });
            lightImage.addListener(lightImage.getInputListener());
            lightImage.setName(LIGHT_NAME + i);
            listOfLights.add(lightImage);
            addActor(lightImage);
        }
    }

    @Override
    public void setSafeActorsUntouchable() {
        for (SafeLightImage safeLightImage : listOfLights) {
            safeLightImage.clearListeners();
        }
        safeCircle.clearListeners();
    }

    @Override
    public void setSafeActorsTouchable() {
        for (SafeLightImage safeLightImage : listOfLights) {
            safeLightImage.addListener(safeLightImage.getInputListener());
        }
        safeCircle.addListener(dragListenerForSafeCircle);
    }

    @Override
    public void setDoneButtonDisOrEnabled(SafeGame safeGame) {
        if (safeGame.getSelectedLight() != null) {
            rotateSafeCircleTo(safeGame.getSelectedLight().localToStageCoordinates(new Vector2(safeGame.getSelectedLight().getWidth() / 2, safeGame.getSelectedLight().getHeight() / 2)), safeCircle);
            safeGame.getDoneButton().setEnabled();
            ;
        }
    }

    @Override
    public void doBeforeEvaluatingAnimation(SequenceAction sa) {}

    public void lightTheLight(SafeGame safeGame, SafeLightImage lightImage) {
        safeGame.setSelectedLight(lightImage, listOfLights, true);

        Vector2 lightImageCoords = lightImage.localToStageCoordinates(new Vector2(lightImage.getWidth() / 2, lightImage.getHeight() / 2));
        rotateSafeCircleTo(lightImageCoords, safeCircle);
        AbstractTablexiaScreen.triggerScenarioStepEvent(LIGHT_LIGHTED_UP);
    }
}
