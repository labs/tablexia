/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.assets;

public class OnTheTrailAssets {
    private static final String     GFX_PATH                = "gfx/";
    private static final String     SFX_PATH                = "sfx/";
    private static final String    COMMON                  = "common/";


    public static final String     MAP_PATH                = GFX_PATH + "maps/";
    public static final String     BUTTON_PATH             = GFX_PATH + "buttons/";
    private static final String     FIGURE_PATH             = GFX_PATH + "figures/";
    private static final String     HELP_PATH               = COMMON + SFX_PATH + "help/";

//-------------------------------------- COMMON --------------------------------------

    public static final String     MENU_BACKGROUND         = GFX_PATH + "menu_background";
    public static final String     VIGNETTING              = GFX_PATH + "vignetting";

    public static final String     B_DOWN                  = BUTTON_PATH + "down";
    public static final String     B_LEFT                  = BUTTON_PATH + "left";
    public static final String     B_RIGHT                 = BUTTON_PATH + "right";
    public static final String     B_UP                    = BUTTON_PATH + "up";

    public static final String     B_CIRCLE_DOWN_LEFT      = BUTTON_PATH + "circle_down_left";
    public static final String     B_CIRCLE_DOWN_RIGHT     = BUTTON_PATH + "circle_down_right";
    public static final String     B_CIRCLE_UP_LEFT        = BUTTON_PATH + "circle_up_left";
    public static final String     B_CIRCLE_UP_RIGHT       = BUTTON_PATH + "circle_up_right";

    public static final String     B_SLOPING_DOWN_LEFT      = BUTTON_PATH + "sloping_down_left";
    public static final String     B_SLOPING_DOWN_RIGHT     = BUTTON_PATH + "sloping_down_right";
    public static final String     B_SLOPING_UP_LEFT        = BUTTON_PATH + "sloping_up_left";
    public static final String     B_SLOPING_UP_RIGHT       = BUTTON_PATH + "sloping_up_right";

    public static final String     FOOTSTEP_LEFT            = GFX_PATH + "footstep_left";
    public static final String     FOOTSTEP_RIGHT           = GFX_PATH + "footstep_right";

    public static final String     ROBBER                  = FIGURE_PATH + "robber";
    public static final String     DETECTIVE               = FIGURE_PATH + "detective";


//////////////////////////// SOUND
    public static final String      VICTORY_NO_STAR_SOUND    = COMMON + SFX_PATH + "result_0.mp3";
    public static final String      VICTORY_ONE_STAR_SOUND   = COMMON + SFX_PATH + "result_1.mp3";
    public static final String      VICTORY_TWO_STAR_SOUND   = COMMON + SFX_PATH + "result_2.mp3";
    public static final String      VICTORY_THREE_STAR_SOUND = COMMON + SFX_PATH + "result_3.mp3";

    public static final String      CITY_ATMO                = COMMON + SFX_PATH + "city_atmo.mp3";
    public static final String      DETECTIVE_STEP_L        = COMMON + SFX_PATH + "detectiv_footsteps_long.mp3";
    public static final String      DETECTIVE_STEP_S        = COMMON + SFX_PATH + "detectiv_footsteps_short.mp3";
    public static final String      HANDCUFFS               = COMMON + SFX_PATH + "handcuffs.mp3";
    public static final String      ROBBER_STEP_L           = COMMON + SFX_PATH + "thief_footsteps_long.mp3";
    public static final String      ROBBER_STEP_S           = COMMON + SFX_PATH + "thief_footsteps_short.mp3";

    public static final String      HELP_BONUS_SOUND = HELP_PATH + "rule_bonus.mp3";

//////////////////////////// TEXT

    public static final String      VICTORY_NO_STAR_TEXT    = "game_on_the_trail_result_0";
    public static final String      VICTORY_ONE_STAR_TEXT   = "game_on_the_trail_result_1";
    public static final String      VICTORY_TWO_STAR_TEXT   = "game_on_the_trail_result_2";
    public static final String      VICTORY_THREE_STAR_TEXT = "game_on_the_trail_result_3";

    public static final String      SUMMARY_TEXT            = "game_on_the_trail_summary_text";

    public static final String      FINISH_BUT_TEXT         = "game_on_the_trail_finish_button";
    public static final String      UNDERSTAND_TEXT         = "game_on_the_trail_rule_message_understand";
    public static final String      RULE_TEXT               = "game_bonus_rule";


//--------------------------------------- BONUS ---------------------------------------

    public static final String      KEYBOARD                = GFX_PATH + "keyboard";
    public static final String      HELP                    = GFX_PATH + "help";

}
