/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.controller;

import com.badlogic.gdx.math.Vector2;

import java.util.HashMap;
import java.util.Map;


public class Crossroad {
    private Vector2 center;
    private Map<Direction, Integer> neighbors;
    private int id;
    private int cID;

    public Crossroad(int id, Vector2 center){
        this(id, center, -1);
    }

    public Crossroad(int id, Vector2 center, int cID){
        neighbors = new HashMap<>();
        this.center = center;
        this.id = id;
        this.cID = cID;
    }

    public void putNeighbor(Direction direction, Integer nID){
        neighbors.put(direction, nID);
    }

    public Vector2 getCenter() {
        return center;
    }

    public Map<Direction, Integer> getNeighbors() {
        return neighbors;
    }

    public int getcID() {
        return cID;
    }

    public int getID(){
        return id;
    }

    public float getCenterX(){
        return center.x;
    }

    public float getCenterY(){
        return center.y;
    }

    public void setCenter(Vector2 center){
        this.center = center;
    }

    @Override
    public String toString() {
        String neighborsString = "";

        for (Direction direction: neighbors.keySet()){
            neighborsString += direction +"("+ direction.ordinal() +")" + " - " + neighbors.get(direction) + "\n";
        }

        return "CROSSROAD ID: " + getID() + " x: " + center.x + " y: " + center.y + "\n" + ((neighborsString.length() == 0) ? " no neighbors" : neighborsString);
    }
}
