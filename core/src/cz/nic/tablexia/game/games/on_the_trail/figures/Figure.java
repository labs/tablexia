/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.figures;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import cz.nic.tablexia.game.games.on_the_trail.assets.OnTheTrailAssets;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Direction;

public class Figure extends Sprite {
    public enum FigureType{
        ROBBER      (OnTheTrailAssets.ROBBER, OnTheTrailAssets.ROBBER_STEP_L, OnTheTrailAssets.ROBBER_STEP_S, 4, 10),
        DETECTIVE   (OnTheTrailAssets.DETECTIVE, OnTheTrailAssets.DETECTIVE_STEP_L, OnTheTrailAssets.DETECTIVE_STEP_S, 4, 8);
        
        private String texturePath;
        private String soundFootstepsLPath;
        private String soundFootstepsSPath;
        private int row;
        private int column;

        
        FigureType(String texturePath, String soundFootstepsLPath, String soundFootstepsSPath, int row, int column){
            this.texturePath = texturePath;
            this.soundFootstepsLPath = soundFootstepsLPath;
            this.soundFootstepsSPath = soundFootstepsSPath;
            this.row = row;
            this.column = column;
        }

        public String getTexturePath() {
            return texturePath;
        }

        public int getRow() {
            return row;
        }

        public int getColumn() {
            return column;
        }

        public String getSoundFootstepsLPath() {
            return soundFootstepsLPath;
        }

        public String getSoundFootstepsSPath() {
            return soundFootstepsSPath;
        }
    }
    
    private static final int ANIM_DURATION = 40;

    private TextureRegion[][] frames;

    private int xF = 1;
    private int yF = 0;

    private Direction direction;
    private long lastTime;
    
    private int row;
    private int column;
    
    public Figure(TextureRegion texture, int row, int column){
        this.row = row;
        this.column = column;
        
        frames = texture.split(texture.getRegionWidth() /  column, texture.getRegionHeight() / row);
        stopMoving();
    }

    public void startMoving(Direction direction){
        this.direction = direction;
    }

    public void stopMoving(){
        direction = null;
        yF = column - 1;
    }

    public void update(){
        if(direction == null)
            return;

        switch (direction) {
            case RIGHT:
            case SLOPING_DOWN_RIGHT:
            case SLOPING_UP_RIGHT:
            case CIRCLE_DOWN_RIGHT:
            case CIRCLE_UP_RIGHT:
                xF = 1; break;
            case DOWN: xF = 2; break;
            case UP: xF = 3; break;
            default:
                xF = 0;
        }

        long time = System.currentTimeMillis();
        if(time - lastTime >= ANIM_DURATION){
            yF = yF > 0 ? yF - 1 : column - 1;
            lastTime = time;
        }
    }

    public TextureRegion getActualFrame(){
        return frames[xF][yF];
    }

}
