/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.way;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.List;

import cz.nic.tablexia.game.games.on_the_trail.assets.TextureDefinition;


public class AbstractWay extends Group {
    static final float NEXT_STEP_RATIO = 0.85f;
    private static final float SPACE_STEP_RATIO = 1.4f;

    private TextureRegion stepL;
    private TextureRegion stepR;

    private TextureDefinition.ObjectSize sizeL;
    private TextureDefinition.ObjectSize sizeR;


    private SpriteBatch spriteBatch;
    ShaderProgram shaderProgram;


    AbstractWay(TextureRegion stepL, TextureRegion stepR, ShaderProgram shaderProgram, TextureDefinition.ObjectSize sizeL, TextureDefinition.ObjectSize sizeR){
        this.stepL = stepL;
        this.stepR = stepR;
        this.sizeL = sizeL;
        this.sizeR = sizeR;

        this.shaderProgram = shaderProgram;

        spriteBatch = new SpriteBatch();
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);
        if (stage != null) {
            spriteBatch.setProjectionMatrix(stage.getCamera().combined);
        }
    }

    Step createStep(float x, float y, float rotation, boolean left, Vector2 previous, boolean vertical, Step.StepStatus stepStatus) {
        float h = getStepWidth(left);

        if (vertical) {
            float s = (h / SPACE_STEP_RATIO) * (x < previous.x ? -1 : 1);
            y = left ? y + s : y - s;
        } else {
            float s = (h / SPACE_STEP_RATIO) * (y > previous.y ? -1 : 1);
            x = left ? x + s : x - s;
        }

        return new Step(
                new Vector2(x, y),
                rotation,
                1.f,
                left,
                getStepWidth(left),
                getStepHeight(left),
                stepStatus
        );
    }


    void drawSteps(List<Step> steps, Vector3 colorV3, OrthographicCamera camera, boolean useShader){
        if(useShader && shaderProgram != null) {
            shaderProgram.begin();
            shaderProgram.setUniformf("u_color", colorV3);
            shaderProgram.end();
            spriteBatch.setShader(shaderProgram);
        }else {
            spriteBatch.setShader(null);
        }

        spriteBatch.begin();
        Color c = spriteBatch.getColor();


        if(camera != null)
            spriteBatch.setProjectionMatrix(camera.combined);

        if(!steps.isEmpty()) {
            for (Step step : steps){
                Vector2 position = localToStageCoordinates(new Vector2(step.getPosition()));
                float w = step.getWidth();
                float h = step.getHeight();

                spriteBatch.setColor(c.r, c.g, c.b, step.getAlpha());
                spriteBatch.draw(
                        step.isLeft() ? stepL : stepR,
                        position.x,
                        position.y,
                        w / 2,
                        h / 2,
                        w,
                        h,
                        1,
                        1,
                        step.getRotation()
                );

            }
        }
        spriteBatch.end();
    }

    float getStepWidth(boolean left){
        return left ? sizeL.getWidth() : sizeR.getWidth();
    }

    float getStepHeight(boolean left){
        return left ? sizeL.getHeight() : sizeR.getHeight();
    }
}
