/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.util;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.game.games.on_the_trail.OnTheTrailGame;
import cz.nic.tablexia.game.games.on_the_trail.map.PositionPoint;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Circle;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Direction;

public class ActionMoveCircular extends TemporalAction {
    private Vector2 center;

    private float finishX;
    private float finishY;

    private float rx;
    private float ry;

    private float finalAngle;
    private float startAngle;

    private Direction direction;
    private Circle circle;
    private PositionPoint positionPoint;
    private boolean menuBorder;

    private float moveDuration;
    private float moveDurationRatio;

    public static ActionMoveCircular moveCircular(float finishX, float finishY, Direction direction, Circle circle, PositionPoint positionPoint, boolean menuBorder, float moveDuration, float moveDurationRatio){
        return new ActionMoveCircular(finishX, finishY, direction, circle, positionPoint, menuBorder, moveDuration, moveDurationRatio);
    }

    private ActionMoveCircular(float finishX, float finishY, Direction direction, Circle circle, PositionPoint positionPoint, boolean menuBorder, float moveDuration, float moveDurationRatio){
        super();
        this.finishX = finishX;
        this.finishY = finishY;
        this.direction = direction;
        this.circle = circle;
        this.positionPoint = positionPoint;
        this.menuBorder = menuBorder;
        this.moveDuration = moveDuration;
        this.moveDurationRatio = moveDurationRatio;
    }

    @Override
    protected void begin() {
        float startX = target.getX(Align.bottomLeft);
        float startY = target.getY(Align.bottomLeft);

        center = new Vector2(
                circle.getCenterX() + (menuBorder ? OnTheTrailGame.PANEL_WIDTH / 2 : 0),
                circle.getCenterY()
        );

        rx = circle.getRX();
        ry = circle.getRY();

        float startAngleDegrees = new Vector2(startX + positionPoint.getWidth() / 2, startY + positionPoint.getHeight() / 4).sub(center).angle();
        float finalAngleDegrees = new Vector2(finishX, finishY ).sub(center).angle();

        if(direction == Direction.CIRCLE_UP_LEFT && startAngleDegrees > finalAngleDegrees) startAngleDegrees -= 360;
        else if(direction == Direction.CIRCLE_UP_RIGHT && finalAngleDegrees > startAngleDegrees) finalAngleDegrees -= 360;
        else if(direction == Direction.CIRCLE_DOWN_LEFT && finalAngleDegrees > startAngleDegrees) finalAngleDegrees -= 360;
        else if(direction == Direction.CIRCLE_DOWN_RIGHT && finalAngleDegrees < startAngleDegrees) startAngleDegrees -= 360;

        startAngle = (float)Math.toRadians(startAngleDegrees);
        finalAngle = (float)Math.toRadians(finalAngleDegrees);

        float duration = moveDuration + Math.abs(finalAngle - startAngle) * moveDurationRatio;
        setDuration(duration);
    }

    @Override
    protected void update(float percent) {
        double angle = ((finalAngle - startAngle) * percent) + startAngle;
        Vector2 position = new Vector2(center.x + rx*(float)Math.cos(angle), center.y + ry*(float)Math.sin(angle));
        actor.setPosition(position.x - positionPoint.getWidth() / 2, position.y - positionPoint.getHeight() / 4);
    }

}
