/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.controller;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.util.Log;

public class Solution {
    private List<Integer> ids;
    private List<Direction> moves;

    public Solution(){
        ids = new ArrayList<>();
        moves = new ArrayList<>();
    }

    public void addWay(Integer id, Direction move){
        ids.add(id);
        moves.add(move);
    }

    public void clear(){
        ids.clear();
        moves.clear();
    }

    public boolean checkMove(Integer position, Direction direction){
        for(int i = 0; i < ids.size(); i++){
            if(position.equals(ids.get(i)) && direction.equals(moves.get(i)))
                return true;
        }

        return false;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public List<Direction> getMoves() {
        return moves;
    }


}
