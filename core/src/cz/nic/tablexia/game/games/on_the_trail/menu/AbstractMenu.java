/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.menu;


import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import cz.nic.tablexia.game.games.on_the_trail.map.ActionListener;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.util.ui.button.GameImageTablexiaButton;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

import static cz.nic.tablexia.game.games.on_the_trail.OnTheTrailGame.FINISH_BUT;

public abstract class AbstractMenu extends Group {
    public abstract float getMenuSize();

    public final static String          BUTTON_NAME_SUFFIX          = "_button";
    private static final int            FINISH_BUTTON_WIDTH         = 145;
    private   static final int          FINISH_BUTTON_HEIGHT        = 72;
    protected static final int          FINIS_BOTTOM_PADDING        = 6;
    protected final static float        MENU_PADDING                = 7f;


    protected MenuItem[] menuItems;
    protected ActionListener actionListener;

    protected FinishListener finishListener;
    protected GameImageTablexiaButton finishButton;

    protected AbstractMenu(FinishListener finishListener, ActionListener actionListener, MenuItem[] menuItems){
        this.actionListener = actionListener;
        this.menuItems = menuItems;
        this.finishListener = finishListener;

    }

    protected TablexiaButton createMenuButton(MenuItem item){
        return new TablexiaButton(
                null,
                false,
                item.getUnpressed(),
                item.getPressed(),
                null,
                null).sound(false);
    }

    void initFinishButton(String finishText){
        finishButton = new GameImageTablexiaButton(finishText, new Image(ApplicationInternalTextureManager.getInstance().getTexture(ApplicationInternalTextureManager.BUTTON_YES_ICON)));
        finishButton.setSize(FINISH_BUTTON_WIDTH, FINISH_BUTTON_HEIGHT);
        finishButton.setEnabled(false);

        finishButton.setName(FINISH_BUT);
        finishButton.setInputListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                finishListener.finish();
            }
        });

    }

    public void finishEnable(boolean enabled){
        finishButton.setEnabled(enabled);
    }


    public void setComponentsPositionAndSize(){ }
}
