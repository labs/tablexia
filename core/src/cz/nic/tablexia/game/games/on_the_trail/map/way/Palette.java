/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.way;

import com.badlogic.gdx.math.Vector3;
import cz.nic.tablexia.game.difficulty.GameDifficulty;

public enum Palette {
    P1 (new Vector3(110 / 255f, 66 / 255f, 25 / 255f), new Vector3(79 / 255f, 101 / 255f, 0 / 255f)),
    P2 (new Vector3(97 / 255f, 52 / 255f, 43 / 255f), new Vector3(71 / 255f, 76 / 255f, 0 / 255f));

    public static final Vector3 WRONG_COLOR_V3 = new Vector3(161 / 255f, 29 / 255f, 16 / 255f);
    public static final Vector3 ROBBERY_COLOR_V3 = new Vector3(0,0,0);

    private final Vector3 detectiveColor;
    private final Vector3 correctColor;

    Palette(Vector3 detectiveColor, Vector3 correctColor){
        this.detectiveColor = detectiveColor;
        this.correctColor = correctColor;
    }

    public Vector3 getCorrectColor(){
        return correctColor;
    }

    public Vector3 getDetectiveColor(){
        return detectiveColor;
    }

    public static Palette getPalette(GameDifficulty difficulty){
        if(difficulty == GameDifficulty.HARD)
            return P2;

        return P1;
    }

}
