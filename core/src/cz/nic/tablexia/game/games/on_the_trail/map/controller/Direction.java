/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.controller;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import cz.nic.tablexia.game.difficulty.GameDifficulty;

public enum Direction {
    LEFT,               //0
    RIGHT,              //1
    UP,                 //2
    DOWN,               //3

    SLOPING_UP_LEFT,    //4
    SLOPING_UP_RIGHT,   //5
    SLOPING_DOWN_LEFT,  //6
    SLOPING_DOWN_RIGHT, //7

    CIRCLE_UP_LEFT,     //8
    CIRCLE_UP_RIGHT,    //9
    CIRCLE_DOWN_LEFT,   //10
    CIRCLE_DOWN_RIGHT;   //11


    public static List<Direction> getDirectionByDifficulty(GameDifficulty difficulty){
        int finalIndex;

        switch (difficulty){
            case HARD: finalIndex = 12; break;
            case MEDIUM: finalIndex = 8; break;
            default: finalIndex = 4;
        }

        return new LinkedList<>(Arrays.asList(values()).subList(0, finalIndex));
    }

    public static Direction getOppositeDirection(Direction direction) {
        switch (direction) {
            case RIGHT:
                return Direction.LEFT;
            case LEFT:
                return Direction.RIGHT;
            case UP:
                return Direction.DOWN;
            case SLOPING_DOWN_LEFT:
                return Direction.SLOPING_UP_RIGHT;
            case SLOPING_DOWN_RIGHT:
                return Direction.SLOPING_UP_LEFT;
            case SLOPING_UP_LEFT:
                return Direction.SLOPING_DOWN_RIGHT;
            case SLOPING_UP_RIGHT:
                return Direction.SLOPING_DOWN_LEFT;
            case CIRCLE_DOWN_LEFT:
                return Direction.CIRCLE_DOWN_RIGHT;
            case CIRCLE_DOWN_RIGHT:
                return Direction.CIRCLE_DOWN_LEFT;
            case CIRCLE_UP_LEFT:
                return Direction.CIRCLE_UP_RIGHT;
            case CIRCLE_UP_RIGHT:
                return Direction.CIRCLE_UP_LEFT;
            default:
                return Direction.UP;
        }
    }
}
