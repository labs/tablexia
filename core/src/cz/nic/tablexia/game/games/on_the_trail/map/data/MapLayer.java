/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.data;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.scenes.scene2d.Actor;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.on_the_trail.OnTheTrailGame;
import cz.nic.tablexia.game.games.on_the_trail.assets.OnTheTrailAssets;

public class MapLayer extends Actor {
    private static final int TILE_CNT_M = 12;
    private static final int TILE_CNT = 10;

    private OrthographicCamera camera;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    private OnTheTrailGame onTheTrailGame;

    private int tileSize;
    private float mapSize;
    private boolean bonus;

    public MapLayer(OrthographicCamera camera, OnTheTrailGame onTheTrailGame){
        this.camera = camera;
        this.onTheTrailGame = onTheTrailGame;
        tileSize = onTheTrailGame.getData().getTextureDefinition().getMapTileSize();

        bonus = onTheTrailGame.getGameDifficulty() == GameDifficulty.BONUS;

        camera.update();

        createMap();
        renderer = new OrthogonalTiledMapRenderer(map);
    }

    private void createMap(){
        map = new TiledMap();

        String difficulty = MapData.getPathDifficulty(onTheTrailGame.getGameDifficulty());

        int tilesCnt = getTileCnt();
        int tilesSize = getTileSize();

        mapSize = tilesCnt * tilesSize;

        TiledMapTileLayer layer = new TiledMapTileLayer(tilesCnt,tilesCnt, tilesSize, tilesSize);


        for(int r = 0; r < tilesCnt; r++){

            for(int c = 0; c < tilesCnt; c++){
                TextureRegion textureRegion = onTheTrailGame.getSpecifyScreenTextureRegion(OnTheTrailAssets.MAP_PATH + "map" + difficulty + "c" + c + "r" + r, bonus);
                StaticTiledMapTile staticTiledMapTile = new StaticTiledMapTile(textureRegion);
                TiledMapTileLayer.Cell cell = new TiledMapTileLayer.Cell();
                cell.setTile(staticTiledMapTile);
                layer.setCell( c, r, cell);
            }
        }

        map.getLayers().add(layer);

    }

    private int getTileCnt(){
        GameDifficulty difficulty = onTheTrailGame.getGameDifficulty();
        return difficulty == GameDifficulty.MEDIUM || difficulty == GameDifficulty.BONUS ? TILE_CNT_M : TILE_CNT;
    }

    private int getTileSize(){
        return tileSize - 1;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.end();

        camera.update();
        renderer.setView(camera);
        renderer.render();

        batch.begin();

    }

    public float getMapSize() {
        return mapSize;
    }
}
