/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.way;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.on_the_trail.OnTheTrailGame;
import cz.nic.tablexia.game.games.on_the_trail.assets.TextureDefinition;
import cz.nic.tablexia.game.games.on_the_trail.map.PositionPoint;

public class CameraWay extends AbstractWay {
    private static final int ALPHA_TIME_DURATION = 90; //ms
    private static final int START_TIME_DURATION = 2000;
    private Camera camera;

    //Round data
    private List<Step> crimeSteps;
    private List<Step> detectiveSteps;

    //Solution Data
    private Map<Long, List<Step>> solutionStep;
    private List<Step> correctSteps;
    private List<Step> wrongSteps;


    private Vector2 last;

    private boolean left = false;
    private boolean moving = false;
    private boolean detectiveRound = false;

    private long lastTime = 0;
    private boolean alphaAnimStart = false;
    private boolean showSolution = false;

    private long actualWayID;
    private PositionPoint positionPoint;

    private Float minSX = null;
    private Float minSY = null;
    private Float maxSX = null;
    private Float maxSY = null;

    private Palette palette;
    private boolean menuBorder = false;

    public CameraWay(Camera camera, TextureRegion stepL, TextureRegion stepR, ShaderProgram shaderProgram, PositionPoint positionPoint, GameDifficulty difficulty, TextureDefinition.ObjectSize sizeL, TextureDefinition.ObjectSize sizeR){
        super(stepL, stepR, shaderProgram, sizeL, sizeR);
        this.camera = camera;
        this.shaderProgram = shaderProgram;
        this.positionPoint = positionPoint;
        crimeSteps = new ArrayList<>();
        detectiveSteps = new ArrayList<>();

        solutionStep = new HashMap<>();
        correctSteps = new ArrayList<>();
        wrongSteps = new ArrayList<>();

        palette = Palette.getPalette(difficulty);
    }

    public void newCrimeWay(float x, float y){
        crimeSteps.clear();

        moving = false;
        left = false;
        last = new Vector2(x, y);
    }

    public void newDetectiveWay(float x, float y){
        detectiveSteps.clear();
        left = false;
        last = new Vector2(x, y);
    }

    public void detectiveRound(){
        float border = camera.getMapTileSize() / 2f;
        camera.prepareDetectiveRound(new Vector2(minSX - border, minSY - border), new Vector2(maxSX + border, maxSY + border));
        detectiveRound = true;
    }

    public void showSolution(){
        float border = camera.getMapTileSize() / 2f;

        float maxX = maxSX + border;
        float maxY = maxSY + border;
        float minX = minSX - border;
        float minY = minSY - border;

        float dX = Math.abs(maxX - minX);
        float dY = Math.abs(maxY - minY);

        camera.computeZoom(Math.max(dX, dY));
        camera.setMinMax();
        camera.position.x = camera.computeX(minX + dX / 2);
        camera.position.y = camera.computeY(minY + dY / 2);
        camera.update();

        showSolution = true;
    }

    public void createWay(long actualWayID){
        this.actualWayID = actualWayID;
        solutionStep.put(actualWayID, new ArrayList<>());
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(showSolution){
            return;
        }

        if(moving && last != null) {

            Vector2 cameraPosition =  new Vector2(
                    positionPoint.getX() - (menuBorder ? OnTheTrailGame.PANEL_WIDTH / 2 : 0) + positionPoint.getWidth() / 2 - getStepWidth(false) / 2,
                    positionPoint.getY());

            float nextStep = getStepHeight(left) * NEXT_STEP_RATIO;
            if(cameraPosition.dst(last) >= nextStep) {
                Step.StepStatus status = Step.StepStatus.NONE;

                if(detectiveRound){
                    status = positionPoint.isCorrectWay() ? Step.StepStatus.CORRECT : Step.StepStatus.WRONG;
                }

                addSteps(detectiveRound ? detectiveSteps : crimeSteps, cameraPosition, last, status);
                last = cameraPosition;

                if(maxSX == null ||maxSX < last.x) maxSX = last.x;
                if(maxSY == null ||maxSY < last.y) maxSY = last.y;
                if(minSX == null ||minSX > last.x) minSX = last.x;
                if(minSY == null ||minSY > last.y) minSY = last.y;

            }

        }

        if(detectiveRound && !crimeSteps.isEmpty()){
            long time = System.currentTimeMillis();

            if(!alphaAnimStart && time - lastTime >= START_TIME_DURATION){
                alphaAnimStart = true;
            }

            if(alphaAnimStart && time - lastTime >= ALPHA_TIME_DURATION){

                float actualAlpha = crimeSteps.get(0).getAlpha();
                actualAlpha -= 0.1f;

                if(actualAlpha <= 0f){
                    if(crimeSteps.size() > 1) {
                        crimeSteps.remove(crimeSteps.size() - 1);
                    }
                    crimeSteps.remove(0);
                }else {
                    if(crimeSteps.size() > 1) {
                        crimeSteps.get(crimeSteps.size() - 1).setAlpha(actualAlpha);
                    }
                    crimeSteps.get(0).setAlpha(actualAlpha);
                }

                lastTime = time;
            }

        }
    }

    private void addSteps(List<Step> steps, Vector2 position, Vector2 previous, Step.StepStatus status){
        Step step = createStep(position.x, position.y, new Vector2(previous).sub(position).angle() + 90, left, previous,Math.abs(previous.x - position.x) > Math.abs(previous.y - position.y), status);
        steps.add(step);


        if(!detectiveRound && solutionStep.containsKey(actualWayID)){
            solutionStep.get(actualWayID).add(createStep(position.x, position.y, new Vector2(previous).sub(position).angle() + 90, left, previous,Math.abs(previous.x - position.x) > Math.abs(previous.y - position.y), status));
        }else if(status == Step.StepStatus.CORRECT){
            correctSteps.add(step);
        }else if(status == Step.StepStatus.WRONG){
            wrongSteps.add(step);
        }

        left = !left;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.end();
        if(!showSolution) {
            drawSteps(crimeSteps, Palette.ROBBERY_COLOR_V3, camera, false);
            drawSteps(detectiveSteps, palette.getDetectiveColor(), camera, true);
        }else {

            for (List<Step> steps: solutionStep.values()){
                drawSteps(steps, Palette.ROBBERY_COLOR_V3, camera, true);
            }

            drawSteps(wrongSteps, Palette.WRONG_COLOR_V3, camera, true);
            drawSteps(correctSteps, palette.getCorrectColor(), camera, true);
        }
        batch.begin();
    }

    public void removeWay(long wayID){
        solutionStep.remove(wayID);
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public boolean isDetectiveRound() {
        return detectiveRound;
    }

    public void setPositionPoint(PositionPoint positionPoint) {
        this.positionPoint = positionPoint;
    }

    public boolean isMenuBorder() {
        return menuBorder;
    }

    public void setMenuBorder(boolean menuBorder) {
        this.menuBorder = menuBorder;
    }
}
