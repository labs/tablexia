/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.GdxRuntimeException;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.on_the_trail.OnTheTrailGame;
import cz.nic.tablexia.game.games.on_the_trail.assets.OnTheTrailAssets;
import cz.nic.tablexia.game.games.on_the_trail.figures.Figure;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Circle;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Crossroad;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Direction;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.MapController;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Solution;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.WayController;
import cz.nic.tablexia.game.games.on_the_trail.map.data.MapLayer;
import cz.nic.tablexia.game.games.on_the_trail.map.way.Camera;
import cz.nic.tablexia.game.games.on_the_trail.map.way.CameraWay;
import cz.nic.tablexia.game.games.on_the_trail.map.way.Shader;
import cz.nic.tablexia.game.games.on_the_trail.assets.TextureDefinition;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;


public class CityMap extends Group implements ActionListener {
    private final static String TAG = "CITY_MAP";
    public  final static String DETECTIVE_NAME = "detective";
    public  final static String MOVE_FINISHED = "move_finish";
    private final static float MAP_SIZE_RATIO = 1.3f;

    private final static float ANIMATION_DURATION = 0.5f;
    private final static float MAP_TEXTURE_SIZE = 1000;


    private OnTheTrailGame game;
    private TablexiaRandom random;
    private Crossroad actualCross;
    private int startID;
    private MapController mapController;
    private TextureDefinition textureDefinition;

    private int animCount = 0;
    private int crossCounter = 0;

    private boolean wayAnimation;
    private boolean finish;
    private boolean detectiveRound = false;

    private ShaderProgram shaderProgram;
    private Camera camera;
    private CameraWay cameraWay;

    private PositionPoint robber;
    private PositionPoint detective;

    private WayController wayController;

    private boolean finalPosition;
    private FinalWay finalWay;
    private Sound hancuffs;

    public CityMap(MapController mapController, OnTheTrailGame onTheTrailGame){
        this.game = onTheTrailGame;
        this.random = game.getRandom();
        this.mapController = mapController;

        textureDefinition = onTheTrailGame.getData().getTextureDefinition();
        hancuffs = game.getSound(OnTheTrailAssets.HANDCUFFS);

        TextureDefinition.ObjectSize stepL = textureDefinition.getStepL();
        TextureDefinition.ObjectSize stepR = textureDefinition.getStepR();

        wayController = new WayController(stepR.getHeight());

        shaderProgram = new ShaderProgram(Shader.VERTEX_SHADER, Shader.FRAGMENT_SHADER);
        if (!shaderProgram.isCompiled()) {
            throw new GdxRuntimeException("Couldn't compile shader: " + shaderProgram.getLog());
        } else {
            Log.info(getClass(), "Shader compiled");
        }

        camera = new Camera(game.getGameDifficulty(), textureDefinition);
        Figure.FigureType dF = Figure.FigureType.DETECTIVE;
        Figure.FigureType rF = Figure.FigureType.ROBBER;

        TextureDefinition.ObjectSize detectiveSize = textureDefinition.getDetectiveSize();
        TextureDefinition.ObjectSize robberSize = textureDefinition.getRobberSize();

        detective = new PositionPoint(
                camera,
                new Figure(game.getScreenTextureRegion(dF.getTexturePath()), dF.getRow(), dF.getColumn()),
                game.getSound(dF.getSoundFootstepsSPath()),
                game.getSound(dF.getSoundFootstepsLPath()),
                detectiveSize.getWidth(),
                detectiveSize.getHeight(),
                textureDefinition.getMoveDuration(),
                textureDefinition.getMoveDurationRatio(),
                textureDefinition.getMoveCircleDuration(),
                textureDefinition.getMoveCircleDurationRatio());

        detective.setName(DETECTIVE_NAME);
        robber = new PositionPoint(
                camera,
                new Figure(game.getScreenTextureRegion(rF.getTexturePath()), rF.getRow(), rF.getColumn()),
                game.getSound(rF.getSoundFootstepsSPath()),
                game.getSound(rF.getSoundFootstepsLPath()),
                robberSize.getWidth(),
                robberSize.getHeight(),
                textureDefinition.getMoveDuration(),
                textureDefinition.getMoveDurationRatio(),
                textureDefinition.getMoveCircleDuration(),
                textureDefinition.getMoveCircleDurationRatio());

        //Anim
        MapLayer mapLayer = new MapLayer(camera, game);
        cameraWay = new CameraWay(camera, game.getScreenTextureRegion(OnTheTrailAssets.FOOTSTEP_LEFT), game.getScreenTextureRegion(OnTheTrailAssets.FOOTSTEP_RIGHT), shaderProgram, robber, game.getGameDifficulty(), stepL, stepR);
        camera.setMapSize(mapLayer.getMapSize());

        detective.setVisible(false);
        robber.setVisible(true);

        addActor(mapLayer);
        addActor(cameraWay);
        addActor(robber);
        addActor(detective);
    }

    public void setComponentsPositionAndSize(){

        //Vignetting
        cameraWay.setSize(getWidth(), getHeight());

        //camera
        if(camera.viewportWidth == 0 || camera.viewportHeight == 0) {
            camera.setToOrtho(false, getWidth(), getHeight());
            camera.setMinMax();
            camera.setxBorderMenu(game.getMenu().getMenuSize());
        }

    }

    public void setCrimeWay(int crossRoadsCnt){
        actualCross = mapController.getRandomCrossRoads(random);

        startID = actualCross.getID();

        Log.info(TAG, actualCross.toString());

        mapController.generateWay(actualCross, random, crossRoadsCnt);
        wayController.addWayData(null, actualCross.getID(), true, -1);
        wayController.setRobberWay(mapController.getSolution());

        //Anim components
        Vector2 startCameraPosition = middleCameraPosition(actualCross.getCenter());
        robber.setPosition(startCameraPosition.x - robber.getWidth() / 2, startCameraPosition.y - robber.getHeight() / 4);
        camera.position.x = robber.getX();
        camera.position.y = robber.getY();
        camera.update();
    }

    public void playHandCuffs(){
        if(hancuffs != null){
            hancuffs.play();
        }
    }

    public void finished(){
        finish = true;
        detective.setVisible(false);
        game.getData().setScore(wayController.getFinalScore());
        cameraWay.showSolution();
        camera.setMinMax();
    }

    public void disposed(){
       shaderProgram.dispose();
       detective.dispose();
       robber.dispose();

       if(hancuffs != null){
           hancuffs.stop();
           hancuffs.dispose();
       }
    }

//////////////////////////// ANIMATION


    public void startWayAnimation(){
        cameraWay.newCrimeWay(actualCross.getCenterX(), actualCross.getCenterY());

        wayAnimation = true;
        finish = false;

        animCount = 0;
        crossCounter = 0;

        Log.info(TAG, mapController.getSolution().toString());

        SequenceAction animation = new SequenceAction();

        Solution solution = mapController.getSolution();
        Vector2 previous = null;
        Integer pID = null;

        robber.setActualPosition(actualCross.getID());

        for(int i = 0; i < solution.getIds().size(); i++){
            Integer id = solution.getIds().get(i);
            Direction direction = solution.getMoves().get(i);
            Crossroad next = mapController.getCrossRoads().get(id);
            Vector2 nextPosition = middleCameraPosition(next.getCenter());

            if(direction != null && previous != null && pID != null) {
                Action moveAction;
                final boolean isFinal = i == solution.getIds().size() - 1;
                float diverceDistance = 0;
                //last
                if(isFinal){
                    finalWay = new FinalWay(pID, id, direction);
                    wayController.setFinalPosition(previous, nextPosition, next, direction, random, game.getGameDifficulty());

                    nextPosition = wayController.getFinalPosition();
                    diverceDistance = wayController.getDiversionPosition().dst(nextPosition);

                    wayController.setFinalDirection(direction);
                    wayController.setFinalID(id);
                }

                float distance = nextPosition.dst(previous);

                if(direction.ordinal() <= Direction.SLOPING_DOWN_RIGHT.ordinal()){
                    moveAction = robber.getMoveAction(nextPosition, distance);
                }else {
                    Circle circle = mapController.getCircles().get(next.getcID());
                    moveAction = robber.getCircleMoveAction(direction, nextPosition, circle);
                }


                Long wayID = getWayID(pID, id);

                float finalDiverceDistance = diverceDistance;
                animation.addAction(Actions.sequence(
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                cameraWay.createWay(wayID);
                                robber.figureStartMoving(direction);
                                robber.playStep(distance);
                            }
                        }),
                        moveAction,

                        //Final Diversion
                        (isFinal) ? Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                     robber.figureStartMoving(wayController.getDiversionDirection());
                                     robber.playShort();
                            }
                        }) : Actions.sequence(),
                        (isFinal) ? robber.getMoveAction(wayController.getDiversionPosition(), wayController.getDiversionPosition().dst(nextPosition)) : Actions.sequence() ,
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                robber.figureStopMoving();
                                robber.stopStep(distance);
                            }
                        }),
                        //

                        Actions.delay(ANIMATION_DURATION)
                ));


            }

            pID = id;
            previous = nextPosition;
        }

        animation.addAction(Actions.run(
                        new Runnable() {
                                @Override
                                public void run() {
                                    robber.stopShort();
                                    cameraWay.setMoving(false);
                                   animFinished();
                                   game.detectiveStart();
                               }
                            }));

        cameraWay.setMoving(true);
        robber.addAction(animation);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(finish)
            return;

        camera.act(cameraWay.isDetectiveRound() ? detective : robber, cameraWay.isDetectiveRound());
    }

    public Vector2 middleCameraPosition(Vector2 position){
        return new Vector2(
                position.x  + (useMenuBorder() ? OnTheTrailGame.PANEL_WIDTH / 2 : 0),
                position.y);
    }

    private void animFinished(){
        detectiveRound = true;
        boolean menuBorder = useMenuBorder();

        if(menuBorder){
            wayController.editFinalAndDeiversionPosition();
        }

        detective.setMenuBorder(menuBorder);
        cameraWay.setMenuBorder(menuBorder);

        robber.setVisible(false);
        detective.setVisible(true);

        actualCross = mapController.getCrossRoads().get(startID);
        Vector2 startPosition = middleCameraPosition(actualCross.getCenter());
        detective.setPosition(startPosition.x - detective.getWidth() / 2, startPosition.y - detective.getHeight() / 4);
        detective.setActualPosition(actualCross.getID());

        cameraWay.newDetectiveWay(startPosition.x, startPosition.y);
        cameraWay.detectiveRound();
        cameraWay.setPositionPoint(detective);
        camera.setMinMax();

        animCount++;
        wayAnimation = false;
    }

    private Long getWayID(Integer previous, Integer next){
        return previous != null ? previous + 1000L *next : null;
    }

//////////////////////////// POSITION ACTION


    @Override
    public void moveTo(Direction direction) {
        if(finish || wayAnimation || detective.hasActions() || finalPosition)
            return;


        Log.info(TAG,"CLICK ACTION: " + direction);
        Integer nextCrossID = actualCross.getNeighbors().get(direction);

        if(nextCrossID != null){

            Crossroad next = mapController.getCrossRoads().get(nextCrossID);
            detective.setCorrectWay(game.isCorrectMove(nextCrossID, direction));
            if(detective.isCorrectWay()){
                cameraWay.removeWay(getWayID(actualCross.getID(), nextCrossID));
            }


            if(next != null){
                finalPosition = wayController.isFinal(next.getID(), direction) && detective.isCorrectWay();
                Log.info(getClass(), "FINAL " + finalPosition);
                Runnable finished = new Runnable() {
                    @Override
                    public void run() {
                        cameraWay.setMoving(false);
                        crossCounter++;

                        if(crossCounter == mapController.getSolution().getIds().size() || finalPosition){
                            game.setFinishButtonEnable(true);
                        }

                        game.setResult(detective.isCorrectWay());
                        int previous = actualCross.getID();

                        wayController.addWayData(direction, next.getID(), detective.isCorrectWay(), previous);

                        if(finalPosition){
                            cameraWay.setMoving(true);
                            detective.clearActions();
                            detective.moveToPosition(nextCrossID, wayController.getDiversionDirection(), wayController.getDiversionPosition(), new Runnable() {
                                @Override
                                public void run() {
                                    playHandCuffs();
                                    cameraWay.setMoving(false);
                                    AbstractTablexiaScreen.triggerScenarioStepEvent(MOVE_FINISHED + wayController.getDetectiveMoveCnt());
                                }
                            });
                            return;
                        }

                        actualCross = next;

                        Log.info(TAG, actualCross.toString());
                        AbstractTablexiaScreen.triggerScenarioStepEvent(MOVE_FINISHED + wayController.getDetectiveMoveCnt());

                    }
                };

                cameraWay.setMoving(true);

                Vector2 nextCenter = middleCameraPosition(next.getCenter());
                wayController.setNext(next.getID());

                if(finalPosition){
                    nextCenter = wayController.getFinalPosition();
                }

                if(direction.ordinal() <= Direction.SLOPING_DOWN_RIGHT.ordinal()) {
                    detective.moveToPosition(nextCrossID, direction, nextCenter, finished);
                }else{
                    Circle circle = mapController.getCircles().get(next.getcID());
                    Log.info(TAG,"cID: " + circle.getId());
                    detective.moveToPositionCircle(nextCrossID, direction, nextCenter, circle, finished);
                }

            }else {
                Log.err(TAG, "Cannot find crossRoad by id: " + nextCrossID);
            }

        }else {
            Log.err(TAG, "Cannot move to this direction " + direction);
            game.wrongMove();
        }
    }


//////////////////////////// GETTERS


    public int getAnimCount() {
        return animCount;
    }

    public boolean isFinish() {
        return finish;
    }

    public boolean useMenuBorder(){
        if(game.getGameDifficulty() == GameDifficulty.BONUS) return false;

        return detectiveRound;
    }

//////////////////////////// TEST

    public Vector2 getDiversionPosition(){
        return wayController.getDiversionPosition();
    }

    public FinalWay getFinalWay(){
        return finalWay;
    }

    public class FinalWay{
        private Integer start;
        private Integer finish;
        private Direction direction;

        FinalWay(Integer start, Integer finish, Direction direction){
            this.start = start;
            this.finish = finish;
            this.direction = direction;
        }

        public Integer getStart() {
            return start;
        }

        public Integer getFinish() {
            return finish;
        }

        public Direction getDirection() {
            return direction;
        }
    }

}
