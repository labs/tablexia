/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.menu;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.game.games.on_the_trail.OnTheTrailGame;
import cz.nic.tablexia.game.games.on_the_trail.map.ActionListener;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

public class MainMenu extends AbstractMenu {
    private final static float BUTTONS_W_RATIO_BIG      = 0.5f;
    private final static float BUTTONS_W_RATIO_SMALL    = 0.3f;

    private Image background;
    private Table menu;
    private TablexiaButton[] buttons;


    public MainMenu(FinishListener finishListener, ActionListener actionListener, MenuItem[] menuItems, TextureRegion backgroundTexture, String finishText){
        super(finishListener, actionListener, menuItems);

        this.actionListener = actionListener;
        this.menuItems = menuItems;

        background = new Image(backgroundTexture);
        addActor(background);
        initFinishButton(finishText);

        initMenu();
        addActor(menu);
    }

///////////////// INIT COMPONENTS

    private void initMenu(){
        menu = new Table();
        buttons = new TablexiaButton[menuItems.length];

        boolean big = menuItems.length <= 4;

        for (int i = 0; i < menuItems.length; i++){
            MenuItem item = menuItems[i];

            TablexiaButton button = createMenuButton(item);
            button.setName(item.getDirection().name() + BUTTON_NAME_SUFFIX);
            button.setInputListener(new MenuClickListener(item.getDirection(), actionListener));

            buttons[i] = button;

            if(big) {
                menu.add(button).pad(MENU_PADDING);
            }else if(i < menuItems.length){
                i++;

                HorizontalGroup group = new HorizontalGroup();
                MenuItem item2 = menuItems[i];
                TablexiaButton button2 = createMenuButton(item2);
                button2.setInputListener(new MenuClickListener(item2.getDirection(), actionListener));
                button2.setName(item2.getDirection().name() + BUTTON_NAME_SUFFIX);

                buttons[i] = button2;

                group.align(Align.center);
                group.addActor(button);
                //group.padLeft(MENU_PADDING);
                group.addActor(button2);
                menu.add(group).pad(MENU_PADDING).align(Align.center);
            }

            menu.row();
        }
        menu.align(Align.center);

        menu.row();
        menu.add(finishButton).pad(MENU_PADDING);

    }

    @Override
    public void setComponentsPositionAndSize(){
        background.setBounds(0, 0, getWidth(), getHeight());
        boolean big = menuItems.length <= 4;

        float buttonsSize = getWidth() * (big ? BUTTONS_W_RATIO_BIG : BUTTONS_W_RATIO_SMALL);
        float menuSize = big ? buttonsSize : buttonsSize * 2 + MENU_PADDING;

        menu.setSize(menuSize, getHeight());
        menu.setPosition(getWidth() / 2 - menu.getWidth() / 2,0);

        finishButton.setX(menu.getWidth() /2 - finishButton.getWidth() /2);

        for (TablexiaButton button : buttons) {
            button.setSize(
                    buttonsSize,
                    buttonsSize
            );
        }
    }

    @Override
    public float getMenuSize() {
        return OnTheTrailGame.PANEL_WIDTH / 2f;
    }
}
