/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.controller;


import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.on_the_trail.OnTheTrailGame;
import cz.nic.tablexia.game.games.on_the_trail.map.way.AbstractWay;
import cz.nic.tablexia.game.games.on_the_trail.model.OnTheTrailGameState;
import cz.nic.tablexia.util.Log;

public class WayController {
    class WayData{
       private Direction direction;
       private int crossID;
       private int previous;
       private int next;
       private boolean correct;

       WayData(Direction direction, int crossID ,boolean correct, int previous){
           this.direction = direction;
           this.crossID = crossID;
           this.correct = correct;
           this.previous = previous;
       }

    }

    private float minDistance;

    private LinkedList<WayData> detectiveWay;
    private List<Integer> robberWay;
    private Map<Integer, Integer> solutionCounter = new HashMap<>();

    private List<Integer> remain;

    private Vector2 finalPosition = null;
    private Vector2 diversionPosition = null;
    private Direction diversionDirection = null;

    private Direction finalDirection;
    private int finalID;
    private float stepHR;

    public WayController(float stepHR){
        this.stepHR = stepHR;

        minDistance = stepHR * 5;

        detectiveWay = new LinkedList<>();
        robberWay = new ArrayList<>();
        remain = new ArrayList<>();
    }

    public void setRobberWay(Solution solution){
        robberWay.addAll(solution.getIds());
        remain.addAll(solution.getIds());

        for(Integer id: robberWay) {
            solutionCounter.put(id, solutionCounter.containsKey(id) ? solutionCounter.get(id) + 1 : 1);
        }
    }

    public void addWayData(Direction direction, int crossID, boolean correct, int previous){
        detectiveWay.add(new WayData(direction, crossID, correct, previous));

        if(solutionCounter.containsKey(crossID)){
            solutionCounter.put(crossID, solutionCounter.get(crossID) - 1);
        }
    }

    public boolean isFinal(int crossID, Direction direction){
        Log.info(getClass(), solutionCounter.toString());
        return crossID == finalID && direction == finalDirection && solutionCounter.containsKey(crossID) && solutionCounter.get(crossID) <= 1;
    }

    public void setNext(int next){
        detectiveWay.getLast().next = next;
    }

    //FINAL

    public void  setFinalPosition (Vector2 p, Vector2 n, Crossroad nC, Direction direction, TablexiaRandom random, GameDifficulty difficulty){
        float distance = p.dst(n);
        float difX = p.x - n.x;
        float difY = p.y - n.y;
        float size = stepHR * 2.5f;


        if(difX == 0 ) difX = random.nextInt(1) == 1 ? 1 : -1;
        if(difY == 0 ) difY = random.nextInt(1) == 1 ? 1 : -1;

        if(distance > minDistance) {
            int r = random.nextInt(3 - 1) + 1;
            float percent = 0.3f + 0.1f * r;

            finalPosition = new Vector2(
                    n.x + difX * percent,
                    n.y + difY * percent
            );

            //TODO i pro stredni a tezkou uroven
            diversionPosition = new Vector2(
                    direction == Direction.LEFT || direction == Direction.RIGHT ? finalPosition.x : finalPosition.x + size * (difX < 0 ? -1 : 1 ),
                    direction == Direction.UP || direction == Direction.DOWN  ? finalPosition.y : finalPosition.y + size * (difY > 0 ? -1  : 1)
            );

            diversionDirection = difX < 0 ? Direction.LEFT : difX > 0 ? Direction.RIGHT : difY < 0 ? Direction.UP : Direction.DOWN;
        }else {
            List<Direction> nextDirections = new ArrayList<>(nC.getNeighbors().keySet());
            List<Direction> possibleDirections = Direction.getDirectionByDifficulty(difficulty);

            for (Direction d: nextDirections){
                possibleDirections.remove(d);
            }

            diversionDirection = possibleDirections.get(random.nextInt(possibleDirections.size()));

            finalPosition = new Vector2(n);

            //TODO i tezkou uroven
            float diversionX = finalPosition.x;
            float diversionY = finalPosition.y;

            if(diversionDirection == Direction.LEFT || diversionDirection == Direction.SLOPING_DOWN_LEFT || diversionDirection == Direction.SLOPING_UP_LEFT){
                diversionX -= size;
            }else if(diversionDirection == Direction.RIGHT || diversionDirection == Direction.SLOPING_DOWN_RIGHT || diversionDirection == Direction.SLOPING_UP_RIGHT){
                diversionX += size;
            }

            if(diversionDirection == Direction.UP || diversionDirection == Direction.SLOPING_UP_LEFT || diversionDirection == Direction.SLOPING_UP_RIGHT){
                diversionY += size;
            }else if (diversionDirection == Direction.DOWN || diversionDirection == Direction.SLOPING_DOWN_LEFT || diversionDirection == Direction.SLOPING_DOWN_RIGHT){
                diversionY -= size;
            }

            diversionPosition = new Vector2(diversionX, diversionY);
        }
    }

    public Vector2 getFinalPosition() {
        return finalPosition;
    }

    public Vector2 getDiversionPosition() {
        return diversionPosition;
    }

    public int getFinalScore(){
        int score = OnTheTrailGameState.CROSSROADS_CNT;

        Map<Integer, List<WayData>> wrongWay = new HashMap<>();
        LinkedList<Integer> last = new LinkedList<>();
        for (WayData data : detectiveWay) {

            //Start cross
            if (data.direction == null) {
                remain.remove((Integer) data.crossID);
                continue;
            }

            boolean remove = false;

            //Control correct back way
            if(wrongWay.containsKey(data.crossID)){
                remove = true;
                List<WayData> wayData = wrongWay.get(data.crossID);

                Map<Integer, Integer> backData = new HashMap<>();
                for(WayData wd: wayData) {
                    backData.put(wd.crossID, backData.containsKey(wd.crossID) ? backData.get(wd.crossID) + 1 : 1);
                }

                Log.info(getClass(), backData.toString());

                int wrong = 0;
                int end = 0;
                boolean back = true;
                int even = 0;
                int odd = 0;

                for(WayData wd: wayData){
                    Integer counter = backData.get(wd.crossID);
                    if(counter == 1 && wd.previous != wd.next && !robberWay.contains(wd.crossID)) {
                        wrong++;
                        back = false;
                    }

                    if(counter %2 == 0) even ++;
                    else odd ++;

                    if(wd.previous == wd.next)
                        end++;
                }

                if(end == 0){
                    score -= odd;
                    score -= even;
                }else {
                    score = back ? score - end : score - (wrong + end);
                }

                wrongWay.remove(data.crossID);
                last.remove((Integer) data.crossID);
            }


            if (data.correct) {

                if(remain.contains(data.crossID)) {
                    remain.remove((Integer) data.crossID);
                }

            }else if(!remove) {
                if(!wrongWay.containsKey(data.crossID)){

                    if((last.isEmpty()|| !containsInList(data, wrongWay.get(last.getLast()))) && robberWay.contains(data.previous) && !wrongWay.containsKey(data.previous)){
                        wrongWay.put(data.previous, new ArrayList<>());
                        last.add(data.previous);
                    }

                }


                if(!last.isEmpty())
                    wrongWay.get(last.getLast()).add(data);

            }


        }

        //Decrement wrong crossroads
        for(Integer wrong: wrongWay.keySet()){
            for (WayData wd: wrongWay.get(wrong)){
                if(wd.correct)
                    break;

                score--;
            }
        }


        //Decrement missed robber crossroads
        if(!remain.isEmpty()) {
            score -= remain.size();
        }

        if(score < 0) score = 0;

        Log.info(getClass(), "Final score is: " + score);
        return score;
    }

    public boolean containsInList(WayData data, List<WayData> wayDataList){
        for(WayData wd: wayDataList){
            if(wd.crossID == data.previous)
                return true;
        }

        return false;
    }

    public void setFinalDirection(Direction finalDirection) {
        this.finalDirection = finalDirection;
    }

    public void setFinalID(int finalID) {
        this.finalID = finalID;
    }

    public Direction getDiversionDirection() {
        return diversionDirection;
    }

    public void editFinalAndDeiversionPosition(){
        diversionPosition.x += OnTheTrailGame.PANEL_WIDTH / 2;
        finalPosition.x += OnTheTrailGame.PANEL_WIDTH / 2;
    }


////////////////////// TEST

    public int getDetectiveMoveCnt(){
        return detectiveWay.size() - 1;
    }
}
