/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.menu;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import cz.nic.tablexia.game.games.on_the_trail.map.controller.Direction;

public class MenuItem {
    private TextureRegion unpressed;
    private TextureRegion pressed;
    private Direction direction;

    public MenuItem(Direction direction, TextureRegion unpressed, TextureRegion pressed){
        this.direction = direction;
        this.unpressed = unpressed;
        this.pressed = pressed;
    }

    public TextureRegion getUnpressed() {
        return unpressed;
    }

    public TextureRegion getPressed() {
        return pressed;
    }

    public Direction getDirection() {
        return direction;
    }
}
