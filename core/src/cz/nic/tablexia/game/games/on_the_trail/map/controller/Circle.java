/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.controller;

public class Circle {

    private int id;

    private float centerX;
    private float centerY;

    private float rX;
    private float rY;

    private float rXPosition;
    private float rYPosition;

    Circle(int id, float centerX, float centerY, float rXPosition, float rYPosition){
        this.id = id;
        this.centerX = centerX;
        this.centerY = centerY;

        this.rXPosition = rXPosition;
        this.rYPosition = rYPosition;

        this.rX = Math.abs((rXPosition) - centerX);
        this.rY = Math.abs(rYPosition - centerY);
    }

    public int getId() {
        return id;
    }

    public float getCenterX() {
        return centerX;
    }

    public float getCenterY() {
        return centerY;
    }

    public float getRX() {
        return rX;
    }

    public float getRY() {
        return rY;
    }

    public float getrXPosition() {
        return rXPosition;
    }

    public float getrYPosition() {
        return rYPosition;
    }
}
