/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import cz.nic.tablexia.game.games.on_the_trail.OnTheTrailGame;
import cz.nic.tablexia.game.games.on_the_trail.figures.Figure;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Circle;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Direction;
import cz.nic.tablexia.game.games.on_the_trail.util.ActionMoveCircular;
import cz.nic.tablexia.util.Log;

public class PositionPoint extends Actor {
    public static final float SHORT_DISTANCE = 50;

    private  boolean correctWay;

    private OrthographicCamera camera;
    private Figure figure;
    private SpriteBatch spriteBatch;

    private Sound longStepSound;
    private Sound shortStepSound;

    private float width;
    private float height;
    private Integer actualPosition;
    private boolean menuBorder = false;

    private float moveDuration;
    private float moveDurationRatio;
    private float circleMoveDuration;
    private float circleMoveDurationRatio;

    private Direction moveDirection = null;
    private Vector2 startMovePosition = null;

    PositionPoint(OrthographicCamera camera, Figure figure, Sound shortStepSound, Sound longStepSound, float width, float height, float moveDuration, float moveDurationRatio, float circleMoveDuration, float circleMoveDurationRatio){
        this.figure = figure;
        this.camera = camera;
        this.shortStepSound = shortStepSound;
        this.longStepSound = longStepSound;
        this.width = width;
        this.height = height;
        this.moveDuration = moveDuration;
        this.moveDurationRatio = moveDurationRatio;
        this.circleMoveDuration = circleMoveDuration;
        this.circleMoveDurationRatio = circleMoveDurationRatio;

        spriteBatch = new SpriteBatch();

        setSize(width, height);

        correctWay = true;
    }

    public void dispose(){
        if (longStepSound != null){
            longStepSound.stop();
            longStepSound.dispose();
        }

        if (shortStepSound != null){
            shortStepSound.stop();
            shortStepSound.dispose();
        }
    }

    void moveToPositionCircle(Integer cID, Direction direction, Vector2 position, Circle circle, Runnable finished){
        Action moveAction = getCircleMoveAction(direction, position, circle);
        startMoveAction(
                direction,
                moveAction,
                cID,
                position.dst(new Vector2(getX() - width / 2, getY()   - height / 4)),
                finished);
    }

    Action getCircleMoveAction(Direction direction, Vector2 position, Circle circle){
        return ActionMoveCircular.moveCircular(
                position.x,
                position.y,
                direction, circle, this, menuBorder, circleMoveDuration, circleMoveDurationRatio);
    }

    void moveToPosition(Integer cID, Direction direction, Vector2 position, Runnable finished){
        float distance = position.dst(new Vector2(getX() - width / 2, getY()   - height / 4));

        Action moveAction = getMoveAction(position, distance);
        startMoveAction(direction, moveAction, cID, distance, finished);

    }

    Action getMoveAction(Vector2 position, float distance){
        float duration = moveDuration + distance * moveDurationRatio;

        return Actions.moveTo(
                position.x - width / 2,
                position.y - height / 4,
                duration);
    }

    public void playStep(float distance){
        Sound footStep = distance < SHORT_DISTANCE ? shortStepSound : longStepSound;
        footStep.loop();
    }

    public  void stopStep(float distance){
        Sound footStep = distance < SHORT_DISTANCE ? shortStepSound : longStepSound;
        footStep.stop();
    }

    public void  playShort(){
        shortStepSound.loop();
    }

    public void stopShort(){
        shortStepSound.stop();
    }

    private void startMoveAction(Direction direction, Action moveAction, Integer endID, float distance, Runnable finished){
        if(hasActions())
            return;

        figureStartMoving(direction);
        playStep(distance);

        addAction(Actions.sequence(moveAction, Actions.run(new Runnable() {
            @Override
            public void run() {
                stopStep(distance);
                figureStopMoving();
                setActualPosition(endID);
                if(finished != null)
                    finished.run();
            }
        })));
    }

    void figureStartMoving(Direction direction){
        moveDirection = direction;
        startMovePosition = new Vector2(getX(), getY());
        figure.startMoving(direction);
    }

    void figureStopMoving(){
        moveDirection = null;
        startMovePosition = null;
        figure.stopMoving();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        figure.update();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.end();

        spriteBatch.begin();
        spriteBatch.setProjectionMatrix(camera.combined);
        Vector2 position = localToStageCoordinates(new Vector2(menuBorder ? -OnTheTrailGame.PANEL_WIDTH / 2 : 0, 0));
        spriteBatch.draw(figure.getActualFrame(), position.x, position.y, width, height);
        spriteBatch.end();

        batch.begin();
    }

    public boolean isCorrectWay() {
        return correctWay;
    }

    public void setCorrectWay(boolean correctWay) {
        this.correctWay = correctWay;
    }

    public Integer getActualPosition() {
        return actualPosition;
    }

    public void setActualPosition(Integer actualPosition) {
        this.actualPosition = actualPosition;
    }

    public void setMenuBorder(boolean menuBorder) {
        this.menuBorder = menuBorder;
    }

    public boolean isMoving(){
        return moveDirection != null;
    }

    public boolean isCamUp(){
        return getY() > startMovePosition.y;
    }

    public boolean isCamDown(){
        return getY() < startMovePosition.y;
    }

    public boolean isCamLeft(){
        return getX() < startMovePosition.x;
    }

    public boolean isCamRight(){
        return getX() > startMovePosition.x;
    }
}
