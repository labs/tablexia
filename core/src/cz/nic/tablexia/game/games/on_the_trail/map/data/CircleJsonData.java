/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.data;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

public class CircleJsonData implements Json.Serializable{

    private int cID;
    private int centerX;
    private int centerY;
    private int rX;
    private int rY;


    @Override
    public void write(Json json) {
        json.writeValue("cID", cID);
        json.writeValue("centerX", centerX);
        json.writeValue("centerY", centerY);
        json.writeValue("rx", rX);
        json.writeValue("ry", rY);
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        cID = jsonData.getInt("cID");
        centerX = jsonData.getInt("centerX");
        centerY = jsonData.getInt("centerY");
        rX = jsonData.getInt("rx");
        rY = jsonData.getInt("ry");
    }

    public int getcID() {
        return cID;
    }

    public void setcID(int cID) {
        this.cID = cID;
    }

    public int getCenterX() {
        return centerX;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    public int getrX() {
        return rX;
    }

    public void setrX(int rX) {
        this.rX = rX;
    }

    public int getrY() {
        return rY;
    }

    public void setrY(int rY) {
        this.rY = rY;
    }
}
