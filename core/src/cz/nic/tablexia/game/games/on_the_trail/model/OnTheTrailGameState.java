/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.model;

import cz.nic.tablexia.game.games.on_the_trail.map.controller.MapController;
import cz.nic.tablexia.game.games.on_the_trail.assets.TextureDefinition;

public class OnTheTrailGameState {
    public static final int CROSSROADS_CNT  = 10;

    private int correctPositions = 0;
    private int wrongPositions = 0;
    private int movesCnt = 0;
    private int score;

    private MapController mapController;
    private TextureDefinition textureDefinition;

    private OnTheTrailGameState(TextureDefinition textureDefinition){
        this.textureDefinition = textureDefinition;

        score = CROSSROADS_CNT;
    }

    public void correctMove() {
        correctPositions++;
    }

    public void wrongMove(){
        wrongPositions++;
    }

    public void movesCntInc(){
        movesCnt++;
    }


/////////////////////////////// GAME STATE FACTORY

    public static class ProtocolGameStateFactory {
        public static OnTheTrailGameState createInstance(TextureDefinition definition) {
            return new OnTheTrailGameState(definition);
        }
    }

/////////////////////////////// GETTERS SETTERS

    public int getCorrectPositions() {
        return correctPositions;
    }

    public int getMovesCnt() {
        return movesCnt;
    }

    public int getWrongPositions() {
        return wrongPositions;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public TextureDefinition getTextureDefinition() {
        return textureDefinition;
    }

    public MapController getMapController() {
        return mapController;
    }

    public void setMapController(MapController mapController) {
        this.mapController = mapController;
    }
}
