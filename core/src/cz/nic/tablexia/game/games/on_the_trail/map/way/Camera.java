/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.way;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.on_the_trail.map.PositionPoint;
import cz.nic.tablexia.game.games.on_the_trail.assets.TextureDefinition;

public class Camera extends OrthographicCamera {
    private static final float TILE_RATIO   = 2.5f;
    private static final float RIGHT_BORDER_RATIO_SD = 1.5f;
    private static final float RIGHT_BORDER_RATIO_HD = 2f;

    private GameDifficulty difficulty;

    private float minX;
    private float minY;

    private float maxX;
    private float maxY;

    private float minVisibleX;
    private float minVisibleY;

    private float maxVisibleX;
    private float maxVisibleY;

    private float mapSize;
    private float xBorderMenu = 0;

    private float maxZoom;
    private float minZoom;
    private TextureDefinition textureDefinition;

    public Camera(GameDifficulty difficulty, TextureDefinition textureDefinition){
        this.difficulty = difficulty;
        this.textureDefinition = textureDefinition;

        maxZoom = getMaxZoom();
        minZoom = getMinZoom();
        zoom = minZoom;
        update();
    }

    public void setMinMax(){
        minX = (zoom * viewportWidth / 2f);
        minY = (zoom * viewportHeight / 2f);
        maxX = mapSize - minX + xBorderMenu;
        maxY = mapSize - minY;

        if(maxX < minX) maxX = minX;
        if(maxY < minY) maxY = minY;
    }

    void computeZoom(float distance){
        this.zoom = getZoomByDistance(distance);
    }

    private float getZoomByDistance(float distance){
        distance += textureDefinition.getMapTileSize();
        float zoom = (distance * (maxZoom + minZoom)) / mapSize;

        if(zoom > maxZoom) zoom = maxZoom;
        else if(zoom < minZoom) zoom = minZoom;


        return zoom;
    }

    private float getMaxZoom(){
        return textureDefinition.getMaxZoom(difficulty);
    }

    private float getMinZoom(){
        return textureDefinition.getMinZoom(difficulty);
    }

    void prepareDetectiveRound(Vector2 minRobber, Vector2 maxRobber){
        float dX = Math.abs(maxRobber.x - minRobber.x);
        float dY = Math.abs(maxRobber.y - minRobber.y);

        computeZoom(Math.max(dX, dY));

        setMinMax();

        //Camera Position
        position.x = computeX(minRobber.x + dX / 2);
        position.y = computeY(minRobber.y + dY / 2);
        setMinMaxVisible();

        update();
    }

    private void setMinMaxVisible(){
        float xBorder = (zoom * viewportWidth / 2f);
        float yBorder =  (zoom * viewportHeight / 2f);

        float tile = textureDefinition.getMapTileSize() / TILE_RATIO;
        minVisibleX = position.x - xBorder + tile * 2;
        minVisibleY = position.y - yBorder + tile;
        maxVisibleX = position.x + xBorder - (tile * (textureDefinition.ordinal() == TextureDefinition.HD.ordinal() ? RIGHT_BORDER_RATIO_HD : RIGHT_BORDER_RATIO_SD)) -  zoom * xBorderMenu;
        maxVisibleY = position.y + yBorder - tile;
    }

    public void act(PositionPoint point, boolean detectiveRound){
        if(!point.isMoving())
            return;

        if(!detectiveRound){
            position.x = point.getX();
            position.y = point.getY();
            update();
            return;
        }


        if(checkMinMaxPosition(point.getX(), point.getY(), point)) {
            setMinMax();
            setMinMaxVisible();
        }

        update();
    }

    private boolean checkMinMaxPosition(float xPosition, float yPosition, PositionPoint point){
        boolean newBorder = false;

        // X
        if (point.isCamLeft() && xPosition <= minVisibleX) { position.x = computeX(position.x - (minVisibleX - xPosition)); newBorder = true;}
        else if (point.isCamRight() && xPosition >= maxVisibleX) { position.x = computeX(position.x + (xPosition - maxVisibleX)); newBorder = true;}

        // Y
        if (point.isCamDown() && yPosition <= minVisibleY) { position.y = computeY(position.y - (minVisibleY - yPosition)); newBorder = true;}
        else if (point.isCamUp() && yPosition >= maxVisibleY) { position.y = computeY(position.y + (yPosition - maxVisibleY)); newBorder = true;}

        return newBorder;
    }

    public void setxBorderMenu(float xBorderMenu) {
        this.xBorderMenu = xBorderMenu;
    }

    float computeX(float xPosition){
        return xPosition < minX ? minX : xPosition > maxX ? maxX : xPosition;
    }

    float computeY(float yPosition){
        return yPosition < minY ? minY : yPosition > maxY ? maxY : yPosition;
    }

    public void setMapSize(float mapSize) {
        this.mapSize = mapSize;
    }

    int getMapTileSize() {
        return textureDefinition.getMapTileSize();
    }
}
