/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.data;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;


public class CrossJsonData implements Json.Serializable {

    private int id;
    private int cID;
    private int x;
    private int y;
    private Array<NeighborJsonData> neighbors;



    public void addNeighbor(Integer direction, Integer nID){
        if(neighbors == null) neighbors = new Array<>();

        NeighborJsonData neighborJsonData = new NeighborJsonData();
        neighborJsonData.setDirection(direction);
        neighborJsonData.setnID(nID);
        neighbors.add(neighborJsonData);
    }

    @Override
    public void write(Json json) {
        json.writeValue("id", id);
        json.writeValue("x", x);
        json.writeValue("y", y);
        json.writeValue("neighbors", neighbors);

        if(cID != -1)
            json.writeValue("cID", cID);
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        id = jsonData.getInt("id");
        x = jsonData.getInt("x");
        y = jsonData.getInt("y");
        neighbors = json.readValue(Array.class, jsonData.get("neighbors"));

        cID = jsonData.getInt("cID", -1);
    }

    public int getcID() {
        return cID;
    }

    public void setcID(int cID) {
        this.cID = cID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Array<NeighborJsonData> getNeighbors() {
        return neighbors;
    }
}


