/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.menu;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.on_the_trail.assets.OnTheTrailAssets;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Direction;

public enum MenuType {
    EASY    (GameDifficulty.EASY,    new String[]{OnTheTrailAssets.B_UP, OnTheTrailAssets.B_DOWN, OnTheTrailAssets.B_LEFT, OnTheTrailAssets.B_RIGHT}, new Direction[]{Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT}),
    MEDIUM  (GameDifficulty.MEDIUM,  new String[]{OnTheTrailAssets.B_UP, OnTheTrailAssets.B_DOWN, OnTheTrailAssets.B_LEFT, OnTheTrailAssets.B_RIGHT, OnTheTrailAssets.B_SLOPING_UP_LEFT, OnTheTrailAssets.B_SLOPING_UP_RIGHT, OnTheTrailAssets.B_SLOPING_DOWN_LEFT, OnTheTrailAssets.B_SLOPING_DOWN_RIGHT}, new Direction[]{Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT, Direction.SLOPING_UP_LEFT, Direction.SLOPING_UP_RIGHT, Direction.SLOPING_DOWN_LEFT, Direction.SLOPING_DOWN_RIGHT}),
    HARD    (GameDifficulty.HARD,    new String[]{OnTheTrailAssets.B_UP, OnTheTrailAssets.B_DOWN, OnTheTrailAssets.B_LEFT, OnTheTrailAssets.B_RIGHT, OnTheTrailAssets.B_SLOPING_UP_LEFT, OnTheTrailAssets.B_SLOPING_UP_RIGHT, OnTheTrailAssets.B_SLOPING_DOWN_LEFT, OnTheTrailAssets.B_SLOPING_DOWN_RIGHT, OnTheTrailAssets.B_CIRCLE_UP_LEFT, OnTheTrailAssets.B_CIRCLE_UP_RIGHT, OnTheTrailAssets.B_CIRCLE_DOWN_LEFT, OnTheTrailAssets.B_CIRCLE_DOWN_RIGHT}, new Direction[]{Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT, Direction.SLOPING_UP_LEFT, Direction.SLOPING_UP_RIGHT, Direction.SLOPING_DOWN_LEFT, Direction.SLOPING_DOWN_RIGHT, Direction.CIRCLE_UP_LEFT, Direction.CIRCLE_UP_RIGHT, Direction.CIRCLE_DOWN_LEFT, Direction.CIRCLE_DOWN_RIGHT}),
    BONUS   (GameDifficulty.BONUS,   new String[]{"1", "2", "3", "4", "6", "7", "8", "9"}, new Direction[]{Direction.SLOPING_UP_LEFT, Direction.UP, Direction.SLOPING_UP_RIGHT, Direction.LEFT, Direction.RIGHT, Direction.SLOPING_DOWN_LEFT, Direction.DOWN, Direction.SLOPING_DOWN_RIGHT});

    private GameDifficulty difficulty;
    private String[] buttonsTexture;
    private Direction[] actions;

    MenuType(GameDifficulty difficulty, String[] buttonsTexture, Direction[] actions){
        this.difficulty = difficulty;
        this.buttonsTexture = buttonsTexture;
        this.actions = actions;
    }

    public static MenuType getMenuType(GameDifficulty difficulty){
        switch (difficulty){
            case MEDIUM: return MEDIUM;
            case HARD: return HARD;
            case BONUS: return BONUS;
            default: return EASY;
        }
    }

    public GameDifficulty getDifficulty() {
        return difficulty;
    }

    public String[] getButtonsTexture() {
        return buttonsTexture;
    }

    public Direction[] getDirections() {
        return actions;
    }
}
