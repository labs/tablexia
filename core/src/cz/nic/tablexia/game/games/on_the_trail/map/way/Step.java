/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.way;

import com.badlogic.gdx.math.Vector2;

public class Step {

    public enum StepStatus{ NONE, CORRECT, WRONG}

    private boolean left;
    private Vector2 position;
    private float rotation;
    private float alpha;

    private float width;
    private float height;

    private StepStatus status;

    Step(Vector2 position, float rotation, float alpha, boolean left, float width, float height){
        this(position, rotation, alpha, left, width, height, StepStatus.NONE);
    }

    Step(Vector2 position, float rotation, float alpha, boolean left, float width, float height, StepStatus status){
        this.position = position;
        this.rotation = rotation;
        this.alpha = alpha;
        this.left = left;
        this.width = width;
        this.height = height;
        this.status = status;
    }

    public StepStatus getStatus() {
        return status;
    }

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
