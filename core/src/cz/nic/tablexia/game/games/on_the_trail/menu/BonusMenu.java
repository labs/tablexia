/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.menu;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.games.on_the_trail.map.ActionListener;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

public class BonusMenu extends AbstractMenu {
    public static final float MENU_W_RATIO = 0.1f;
    private static final float KEYBOARD_SIZE_RATIO = 1.5f;
    private static final float KEYBOARD_HOLDER_RATIO = 0.03f;
    private static final int ROW_CNT = 3;

    private Image help;
    private Image keyboard;

    private Group keyboardGroup;
    private Group buttonsGroup;
    private TablexiaButton[] buttons;

    public BonusMenu(FinishListener finishListener, ActionListener actionListener, MenuItem[] menuItems, TextureRegion help, TextureRegion keyboard, String finishText) {
        super(finishListener, actionListener, menuItems);

        this.keyboard = new Image(keyboard);
        this.help = new Image(help);

        initKeyboard();
        initFinishButton(finishText);

        addActor(finishButton);
        addActor(keyboardGroup);
        addActor(this.help);
    }

    private void initKeyboard(){
        keyboardGroup = new Group();
        buttonsGroup = new Group();

        buttons = new TablexiaButton[menuItems.length];

        for(int i = 0; i < menuItems.length; i++){
            MenuItem menuItem = menuItems[i];
            TablexiaButton button = createMenuButton(menuItem);
            button.setName(menuItem.getDirection().name() + BUTTON_NAME_SUFFIX);
            button.setInputListener(new MenuClickListener(menuItem.getDirection(), actionListener));

            buttons[i] = button;
            buttonsGroup.addActor(button);
        }

        keyboardGroup.addActor(keyboard);
        keyboardGroup.addActor(buttonsGroup);
    }


    @Override
    public void setComponentsPositionAndSize() {
        help.setBounds(
                0,
                getHeight() - getWidth(),
                getWidth(),
                getWidth());


        float keyboardHeight = getWidth() * KEYBOARD_SIZE_RATIO;
        float holder = keyboardHeight * KEYBOARD_HOLDER_RATIO;
        float keyboardWidth = keyboardHeight + holder;

        keyboardGroup.setBounds(
                (getWidth() - keyboardWidth),
                getHeight() - help.getHeight() - help.getHeight() / 3 - keyboardHeight,
                keyboardWidth,
                keyboardHeight
        );

        keyboard.setSize(keyboardGroup.getWidth(), keyboardGroup.getWidth());

        float buttonsGroupSize = keyboardHeight - holder * 2.5f;
        float buttonsGroupPosition = keyboardHeight / 2 - buttonsGroupSize / 2;

        float padding = holder * 2f;

        buttonsGroup.setBounds(
                buttonsGroupPosition,
                buttonsGroupPosition + padding,
                buttonsGroupSize,
                buttonsGroupSize);

        float buttonW = buttonsGroupSize * 0.28f;
        float buttonH = buttonsGroupSize * 0.35f;


        float xR = 0;
        float yR = ROW_CNT - 1;


        for (TablexiaButton button : buttons) {
            button.setBounds(
                    buttonW * xR +  padding * xR,
                    buttonH * yR,
                    buttonW,
                    buttonH
            );

            xR++;


            if (yR == ROW_CNT - 2 && xR == 1) {
                xR++;
            }

            if (xR == ROW_CNT) {
                xR = 0;
                yR--;
            }
        }

        finishButton.setPosition(
                getWidth() / 2f - finishButton.getWidth() / 2f - MENU_PADDING * 2,
                FINIS_BOTTOM_PADDING + finishButton.getHeight() / 2 - MENU_PADDING
        );
    }

    @Override
    public float getMenuSize() {
        return keyboardGroup.getWidth();
    }
}
