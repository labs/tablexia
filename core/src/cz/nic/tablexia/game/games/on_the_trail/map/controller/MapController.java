/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map.controller;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;

import java.util.HashMap;
import java.util.Map;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.on_the_trail.map.data.CircleJsonData;
import cz.nic.tablexia.game.games.on_the_trail.map.data.CrossJsonData;
import cz.nic.tablexia.game.games.on_the_trail.map.data.MapData;
import cz.nic.tablexia.game.games.on_the_trail.map.data.NeighborJsonData;
import cz.nic.tablexia.util.Log;

public class MapController {
    private Pixmap pixmap;

    //Map data
    private Map<Integer, Crossroad> crossRoads;
    private Map<Integer, Circle> circles;

    //Solution
    private Solution solution;

    public MapController() {
        crossRoads = new HashMap<>();
        circles = new HashMap<>();
        solution = new Solution();
    }

///////////////////////////////  ROUND ACTION


    public void generateWay(Crossroad start, TablexiaRandom random, int crossRoadsCnt) {
        solution.clear();

        Direction previousMove = null;
        Crossroad cross = start;
        int part = 0;

        solution.addWay(cross.getID(), null);

        while (part < crossRoadsCnt) {
            Map<Direction, Integer> neighbors = cross.getNeighbors();
            Direction move = chooseNeighbor(random, new HashMap<>(neighbors), previousMove);
            int next = neighbors.get(move);

            solution.addWay(next, move);

            previousMove = move;
            cross = crossRoads.get(next);

            //not
            if(cross.getNeighbors().size() > 2)
                part++;
        }

        Log.info(getClass(), "Generate way " + solution.getIds().size());
    }

    /**
     * Return neighbors, who not be in solution. If all neighbors are in solution return them.
     * @param crossNeighbors
     * @return
     */
    private Direction chooseNeighbor(TablexiaRandom random, Map<Direction, Integer> crossNeighbors, Direction previous){

        //dead end street
        if(crossNeighbors.size() <= 1)
            return (Direction) crossNeighbors.keySet().toArray()[0];


        //remove back way
        if (previous != null) {
            crossNeighbors.remove(Direction.getOppositeDirection(previous));
        }

        Map<Direction, Integer> preferred = new HashMap<>();
        Map<Direction, Integer> sufficient = new HashMap<>();

        for(Direction direction: crossNeighbors.keySet()){
            int id = crossNeighbors.get(direction);
            if(!solution.getIds().contains(id)){
                boolean prefer = false;

                Crossroad cr = crossRoads.get(id);
                Direction opposite = Direction.getOppositeDirection(direction);
                for(Direction d: cr.getNeighbors().keySet()){
                    if(d == opposite)
                        continue;

                    int nID = cr.getNeighbors().get(d);

                    if(!solution.getIds().contains(nID)){
                        prefer = true;
                        break;
                    }
                }

                if(prefer) preferred.put(direction, id);
                else sufficient.put(direction, id);
            }
        }

        Map<Direction, Integer> finalMap = !preferred.isEmpty() ? preferred : !sufficient.isEmpty() ? sufficient : crossNeighbors;

        return (Direction) finalMap.keySet().toArray()[random.nextInt(finalMap.size())];
    }


    public Crossroad getRandomCrossRoads(TablexiaRandom random) {
        return (Crossroad) crossRoads.values().toArray()[random.nextInt(crossRoads.size())];
    }

/////////////////////////////// MAP PRE-PROCESSING


    public void setMapData(GameDifficulty difficulty, float positionScale) {
        String mapData = MapData.getMapData(difficulty);
        MapData jsonData = new Json().fromJson(MapData.class, mapData);
        Direction[] directions = Direction.values();

        for (CrossJsonData crossJsonData : jsonData.getCrossRoads()) {
            Crossroad crossroad = new Crossroad(
                    crossJsonData.getId(),
                    new Vector2(
                            crossJsonData.getX()* positionScale,
                            crossJsonData.getY() * positionScale
                    ),
                    crossJsonData.getcID()
            );

            for (NeighborJsonData neighborData : crossJsonData.getNeighbors()) {
                Direction direction = directions[neighborData.getDirection()];
                crossroad.putNeighbor(direction, neighborData.getnID());
            }

            crossRoads.put(crossJsonData.getId(), crossroad);
        }


        if (jsonData.getCircles() != null) {
            for (CircleJsonData circleJsonData : jsonData.getCircles()) {
                Circle circle = new Circle(
                    circleJsonData.getcID(),
                    circleJsonData.getCenterX() * positionScale,
                    circleJsonData.getCenterY() * positionScale,
                    circleJsonData.getrX() * positionScale,
                    circleJsonData.getrY() * positionScale
                );

                circles.put(circle.getId(), circle);
            }
        }

    }


/////////////////////////////// GETTERS SETTERS

    public Map<Integer, Crossroad> getCrossRoads() {
        return crossRoads;
    }

    public Pixmap getPixmap() {
        return pixmap;
    }

    public void setPixmap(Pixmap pixmap) {
        this.pixmap = pixmap;
    }

    public Solution getSolution() {
        return solution;
    }

    public boolean containsCircle() {
        return circles != null && !circles.isEmpty();
    }

    public Map<Integer, Circle> getCircles() {
        return circles;
    }

}