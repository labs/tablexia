/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail;


import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;


import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.MusicUtil;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaNoBlendingImage;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.button.TablexiaButton;

public class BonusRuleGroup extends Group {
    public static final String RULE_BUTTON                     = "rule button";
    public static final String VISIBLE_EVENT                   = "understand visible";

    private static final    float   BUTTON_WIDTH                    = 170;
    private static final    float   BUTTON_HEIGHT                   = 80;
    private static final    float   BUTTON_X_POSITION_RATIO         = 1f/2;
    private static final    float   BUTTON_Y_POSITION_RATIO         = 9f/40;
    private static final    float   PAPER_HEIGHT_RATIO              = 19f/20;

    private static final 	Color   LABEL_TEXT_COLOR                = Color.GRAY;

    private static final float RULE_MESSAGE_PAPER_WIDTH  =	0.6f;
    private static final float RULE_MESSAGE_TEXT_PADDING =  0.1f;

    private static final float RULE_SOUND_DELAY 		  = 1.0f;
    private static final float RULE_SOUND_FADE_OUT_TIME   =	0.5f;
    private static final float RULE_MESSAGE_FADE_OUT_TIME =	0.5f;

    private Image background;
    private Image paper;
    private TablexiaButton understand;
    private TablexiaLabel message;

    private Music ruleSound;

    public BonusRuleGroup(TextureRegion backgroundTexture, TextureRegion paperTexture, Music roleSound, String ruleText, String buttonText, final Runnable startGame){
        this.ruleSound = roleSound;

        background = new TablexiaNoBlendingImage(backgroundTexture);
        paper = new Image(paperTexture);

        understand = new StandardTablexiaButton(buttonText, StandardTablexiaButton.TablexiaButtonType.GREEN);
        understand.setName(RULE_BUTTON);
        understand.setVisible(false);
        understand.useOnce(true);

        understand.setInputListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                stopRuleSound();
                hide(startGame);
            }
        });

        ApplicationFontManager.FontType fontType = ApplicationFontManager.FontType.REGULAR_18;
        message = new TablexiaLabel(ruleText, new TablexiaLabel.TablexiaLabelStyle(fontType, LABEL_TEXT_COLOR), true);
        message.setWrap(true);
        message.setAlignment(Align.center);

        addActor(background);
        addActor(paper);
        addActor(message);
        addActor(understand);
    }

    private void hide(final Runnable finish){
        addAction(Actions.sequence(
                Actions.fadeOut(RULE_MESSAGE_FADE_OUT_TIME),
                Actions.visible(false),
                Actions.run(finish)
        ));
    }

    public void playRuleSound(){
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                understand.setVisible(true);
                AbstractTablexiaGame.triggerScenarioStepEvent(VISIBLE_EVENT);
                ruleSound.play();

                //TODO TEST EVENT
            }
        }, RULE_SOUND_DELAY);
    }

    private void stopRuleSound(){
        if(ruleSound!= null && ruleSound.isPlaying()){
            MusicUtil.fadeOut(ruleSound, RULE_SOUND_FADE_OUT_TIME);
        }
    }

    public void setComponentsPositionAndSize(float sceneW, float sceneH, float viewportX, float viewportY, float viewportW, float viewportH){
        background.setBounds(viewportX, viewportY, viewportW, viewportH);
        float paperRatio = (sceneH * PAPER_HEIGHT_RATIO) / paper.getHeight();
        float paperWidth = paper.getWidth() * paperRatio;
        float paperHeight = paper.getHeight() * paperRatio;
        paper.setSize(paperWidth, paperHeight);
        paper.setPosition(getX() + sceneW / 2 - paperWidth / 2, getY() + (sceneH / 2 - paperHeight / 2));

        understand.setButtonBounds(
                paper.getX() + (paper.getWidth() * BUTTON_X_POSITION_RATIO) - (BUTTON_WIDTH / 2),
                paper.getY() + (paper.getHeight() * BUTTON_Y_POSITION_RATIO) - (BUTTON_HEIGHT / 2),
                BUTTON_WIDTH,
                BUTTON_HEIGHT
                );

        message.setWidth(sceneW * RULE_MESSAGE_PAPER_WIDTH - (2 * (sceneW * RULE_MESSAGE_TEXT_PADDING)));

        message.setPosition(getX() + sceneW / 2 - message.getWidth() / 2,
                getY() + sceneH / 2 - message.getHeight() / 2);
    }

}
