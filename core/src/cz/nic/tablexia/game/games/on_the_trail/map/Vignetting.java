/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.map;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class Vignetting extends Group {
    private final static float SIZE_RATIO = 0.98f;
    private final static float BORDER_SIZE_RATIO = 0.97f;


    private Image vignetting;
    private Image border;
    private boolean menuBorder = false;

    public Vignetting(TextureRegion textureRegion){
        vignetting = new Image(textureRegion);
        addActor(vignetting);
    }


    public void setComponentsPositionAndSize(float x, float y, float w, float h, float sceneW, float sceneH){
        if(border != null)
            removeActor(border);

        super.setBounds(x, y, w, h);

        float mapSize = Math.min(sceneW * SIZE_RATIO, sceneH * SIZE_RATIO);
        float borderSize = Math.min(sceneW * BORDER_SIZE_RATIO, sceneH * BORDER_SIZE_RATIO);

        vignetting.setBounds(getWidth() / 2 - mapSize / 2, getHeight() / 2 - mapSize / 2, mapSize, mapSize);

        Rectangle r = new Rectangle(getWidth() / 2 - borderSize / 2, getHeight() / 2 - borderSize / 2, borderSize, borderSize);

        border = createBorder(w, h, r);
        border.setBounds(0, 0, w, h);
        addActor(border);

    }

    private Image createBorder(float width, float height, Rectangle rectangle){
        int w = (int) width;
        int h = (int) height;

        Pixmap pixmap = new Pixmap(w,h, Pixmap.Format.RGBA8888);
        int black = Color.rgba8888(Color.BLACK);
        pixmap.setColor(Color.CLEAR);

        for(int x = 0; x < w; x++){
            for (int y = 0; y < h; y++){
                if(!rectangle.contains(x, y)){
                    pixmap.drawPixel(x, y, black);
                }
            }
        }

        return new Image(new Texture(pixmap));
    }


    public boolean isMenuBorder() {
        return menuBorder;
    }

    public void setMenuBorder(boolean menuBorder) {
        this.menuBorder = menuBorder;
    }
}
