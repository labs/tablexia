/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail.assets;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.difficulty.GameDifficulty;

public enum TextureDefinition {
    SD (1.f, 250, 3.3f, 0.6f, 4.0f, 0.65f, new ObjectSize(6f, 14f), new ObjectSize(4f, 15f), new ObjectSize(40f, 60f), new ObjectSize(50f, 70f), 0.15f, 0.002f, 0.2f, 0.25f),
    HD (2.6f, 650, 8.4f, 1.3f, 10.4f, 1.6f, new ObjectSize(18f, 32f), new ObjectSize(14f, 34f), new ObjectSize(106f, 160f), new ObjectSize(155f, 200f), 0.1f, 0.001f, 0.15f, 0.2f);

    public static class ObjectSize{
        private float width;
        private float height;

        private ObjectSize(float width, float height){
            this.width = width;
            this.height = height;
        }

        public float getWidth() {
            return width;
        }

        public float getHeight() {
            return height;
        }
    }


    //Map
    private float mapScale;
    private int mapTileSize;

    //Camera
    private float maxZoom;
    private float minZoom;
    private float maxZoomM;
    private float minZoomM;

    //Steps
    private ObjectSize stepL;
    private ObjectSize stepR;

    //Figure
    private ObjectSize detectiveSize;
    private ObjectSize robberSize;

    private float moveDuration;
    private float moveDurationRatio;
    private float moveCircleDuration;
    private float moveCircleDurationRatio;

    TextureDefinition(float mapScale, int mapTileSize, float maxZoom, float minZoom, float maxZoomM, float minZoomM, ObjectSize stepL, ObjectSize stepR, ObjectSize robberSize, ObjectSize detectiveSize, float moveDuration, float moveDurationRatio, float moveCircleDuration, float moveCircleDurationRatio){
        this.mapScale = mapScale;
        this.mapTileSize = mapTileSize;
        this.maxZoom = maxZoom;
        this.minZoom = minZoom;
        this.maxZoomM = maxZoomM;
        this.minZoomM = minZoomM;
        this.stepL = stepL;
        this.stepR = stepR;
        this.robberSize = robberSize;
        this.detectiveSize = detectiveSize;
        this.moveDuration = moveDuration;
        this.moveDurationRatio = moveDurationRatio;
        this.moveCircleDuration = moveCircleDuration;
        this.moveCircleDurationRatio = moveCircleDurationRatio;
    }

    public static TextureDefinition getDefinition(TablexiaSettings settings){
        return settings.isUseHdAssets() ? HD : SD;
    }

    public float getMapScale() {
        return mapScale;
    }

    public int getMapTileSize() {
        return mapTileSize;
    }

    public float getMaxZoom(GameDifficulty difficulty){
        return (difficulty == GameDifficulty.MEDIUM || difficulty == GameDifficulty.BONUS) ? maxZoomM : maxZoom;
    }

    public float getMinZoom(GameDifficulty difficulty){
        return (difficulty == GameDifficulty.MEDIUM || difficulty == GameDifficulty.BONUS) ? minZoomM : minZoom;
    }

    public ObjectSize getStepL() {
        return stepL;
    }

    public ObjectSize getStepR() {
        return stepR;
    }

    public ObjectSize getDetectiveSize() {
        return detectiveSize;
    }

    public ObjectSize getRobberSize() {
        return robberSize;
    }

    public float getMoveDuration() {
        return moveDuration;
    }

    public float getMoveDurationRatio() {
        return moveDurationRatio;
    }

    public float getMoveCircleDuration() {
        return moveCircleDuration;
    }

    public float getMoveCircleDurationRatio() {
        return moveCircleDurationRatio;
    }
}
