/*
 * Copyright (C) 2019 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.on_the_trail;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.on_the_trail.assets.OnTheTrailAssets;
import cz.nic.tablexia.game.games.on_the_trail.map.CityMap;
import cz.nic.tablexia.game.games.on_the_trail.map.Vignetting;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Direction;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.MapController;
import cz.nic.tablexia.game.games.on_the_trail.map.controller.Solution;
import cz.nic.tablexia.game.games.on_the_trail.menu.BonusMenu;
import cz.nic.tablexia.game.games.on_the_trail.menu.FinishListener;
import cz.nic.tablexia.game.games.on_the_trail.menu.MenuItem;
import cz.nic.tablexia.game.games.on_the_trail.menu.MenuType;
import cz.nic.tablexia.game.games.on_the_trail.menu.AbstractMenu;
import cz.nic.tablexia.game.games.on_the_trail.menu.MainMenu;
import cz.nic.tablexia.game.games.on_the_trail.model.OnTheTrailGameState;
import cz.nic.tablexia.game.games.on_the_trail.assets.TextureDefinition;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.resolvers.OnTheTrailScoreResolver;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

public class OnTheTrailGame extends AbstractTablexiaGame<OnTheTrailGameState> implements FinishListener {
    public static final String      MENU       = "menu";
    public static final String      CITY       = "city";
    public static final String      FINISH_BUT = "finish_but";
    public static final String      EVENT_ANIM_ROBBER = "robber finish";

    private enum ResultMapping{
        NO_STAR     (GameResult.NO_STAR, OnTheTrailAssets.VICTORY_NO_STAR_TEXT, OnTheTrailAssets.VICTORY_NO_STAR_SOUND),
        ONE_STAR    (GameResult.ONE_STAR, OnTheTrailAssets.VICTORY_ONE_STAR_TEXT, OnTheTrailAssets.VICTORY_ONE_STAR_SOUND),
        TWO_STAR    (GameResult.TWO_STAR, OnTheTrailAssets.VICTORY_TWO_STAR_TEXT, OnTheTrailAssets.VICTORY_TWO_STAR_SOUND),
        THREE_STAR  (GameResult.THREE_STAR, OnTheTrailAssets.VICTORY_THREE_STAR_TEXT, OnTheTrailAssets.VICTORY_THREE_STAR_SOUND);

        private GameResult result;
        private String text;
        private String sound;

        ResultMapping(GameResult result, String text, String sound){
            this.result = result;
            this.text = text;
            this.sound = sound;
        }

        public String getText() {
            return text;
        }

        public String getSound() {
            return sound;
        }

        public static ResultMapping getResult(GameResult result){
            for (ResultMapping resultMapping : ResultMapping.values()) {
                if (resultMapping.result.equals(result)) {
                    return resultMapping;
                }
            }
            return null;
        }
    }

    public  static final float         PANEL_WIDTH                     = 175f;
    private static final float         FINISH_ACTION_DURATION          = 3f;

    private AbstractMenu menu;
    private CityMap cityMap;
    private Vignetting vignetting;
    private Music cityAtmo;

    //Bonus
    private BonusRuleGroup ruleGroup;

    private boolean ruleShowed = false;
    private boolean finishAction = false;
    private float elapsedTime;

/////////////////////////////// SCREEN LOADERS


    @Override
    protected void prepareGameSoundAssetNames(List<String> soundsFileNames) {
        soundsFileNames.add(OnTheTrailAssets.HANDCUFFS);
        soundsFileNames.add(OnTheTrailAssets.DETECTIVE_STEP_L);
        soundsFileNames.add(OnTheTrailAssets.DETECTIVE_STEP_S);
        soundsFileNames.add(OnTheTrailAssets.ROBBER_STEP_L);
        soundsFileNames.add(OnTheTrailAssets.ROBBER_STEP_S);
    }

    @Override
    protected OnTheTrailGameState prepareGameData(Map<String, String> gameState) {
        TextureDefinition definition = TextureDefinition.getDefinition(TablexiaSettings.getInstance());
        OnTheTrailGameState state = OnTheTrailGameState.ProtocolGameStateFactory.createInstance(definition);

        MapController mapController = new MapController();
        mapController.setMapData(getGameDifficulty(), definition.getMapScale());

        state.setMapController(mapController);

        return  state;
    }

    @Override
    protected void prepareScreenAtlases(List<String> atlasesNames) {
        if(getGameDifficulty() == GameDifficulty.BONUS) atlasesNames.add(getGameDifficultyAtlasPath(GameDifficulty.MEDIUM));
        super.prepareScreenAtlases(atlasesNames);
    }

    public TextureRegion getSpecifyScreenTextureRegion(String regionName, boolean specify) {
        if(specify)
            return getTextureRegionForAtlas(getGameDifficultyAtlasPath(GameDifficulty.MEDIUM), regionName, null);

        return getScreenTextureRegion(regionName);
    }

/////////////////////////////// GAME LIFECYCLE

    @Override
    protected void gameLoaded(Map<String, String> gameState) {
        iniMap();
        initMenu();

        cityAtmo = getMusic(OnTheTrailAssets.CITY_ATMO);


        getStage().addActor(cityMap);
        getStage().addActor(menu);
        getStage().addActor(vignetting);

        if(getGameDifficulty() == GameDifficulty.BONUS && !ruleShowed)
            prepareBonusRule();

        setComponentsPositionAndSize();

        cityMap.setCrimeWay(OnTheTrailGameState.CROSSROADS_CNT);
    }

    private void setGameComponentVisible(boolean visible){
        cityMap.setVisible(visible);
        menu.setVisible(visible);
        vignetting.setVisible(visible);
    }

    private void prepareBonusRule(){
        setGameComponentVisible(false);
        ruleGroup = new BonusRuleGroup(
                getApplicationTextureRegion(ApplicationAtlasManager.BACKGROUND_WOODEN),
                getGameGlobalTextureRegion(GAME_RULE_BACKGROUND),
                getMusic(OnTheTrailAssets.HELP_BONUS_SOUND),
                getText(OnTheTrailAssets.RULE_TEXT),
                getText(OnTheTrailAssets.UNDERSTAND_TEXT),
                new Runnable() {
                    @Override
                    public void run() {
                        startBonusGame();
                    }
                }
        );

        getStage().addActor(ruleGroup);
    }

    public void startBonusGame(){
        setGameComponentVisible(true);
        startWayAnimation();
    }

    public boolean isCorrectMove(Integer position, Direction direction){
        Solution solution = getData().getMapController().getSolution();
        getData().movesCntInc();
        return solution.checkMove(position, direction);
    }

    public void setResult(boolean correctMove){
        if(correctMove){
            correctMove();
        }else {
            wrongMove();
        }
    }

    public void wrongMove(){
        getData().wrongMove();
    }

    private void correctMove(){
        getData().correctMove();
    }

    private void gameFinished() {
       //setGameScore
        int wrongPosition = getData().getWrongPositions();

        setGameScore(OnTheTrailScoreResolver.SCORE, getData().getScore());
        setGameScore(OnTheTrailScoreResolver.CORRECT_POSITIONS, getData().getCorrectPositions());
        setGameScore(OnTheTrailScoreResolver.WRONG_POSITIONS, wrongPosition);
        setGameScore(OnTheTrailScoreResolver.MOVES_CNT, getData().getMovesCnt());

        endGame();
        showGameResultDialog();
    }

    @Override
    protected void gameVisible() {
        if(getGameDifficulty() != GameDifficulty.BONUS || ruleShowed){
           startWayAnimation();
        } else if(ruleGroup != null){
            ruleShowed = true;
            ruleGroup.playRuleSound();
        }

        triggerScenarioStepEvent(EVENT_GAME_READY);
    }

    private void startWayAnimation(){
        if(cityMap != null && cityMap.getAnimCount() == 0) {
            finishAction = false;
            cityMap.startWayAnimation();
        }
    }


    @Override
    protected void gamePaused(Map<String, String> gameState) {
        //TODO pause
    }

    @Override
    protected void gameResumed() {
        //TODO resume
    }

    @Override
    protected void gameDisposed() {
        if(cityMap != null)
            cityMap.disposed();

        if(cityAtmo != null){
            cityAtmo.stop();
            cityAtmo.dispose();
        }
    }

    @Override
    protected void gameEnd() {
       getStage().clear();
    }

    @Override
    protected void gameAct(float delta) {
        super.gameAct(delta);
        if(finishAction){
            if(elapsedTime > FINISH_ACTION_DURATION){
                finishAction = false;
                menu.finishEnable(false);
                gameFinished();
            }else
                elapsedTime += delta;
        }
    }

/////////////////////////////// INIT SCREEN COMPONENT

    private void iniMap(){
        this.vignetting = new Vignetting(getScreenTextureRegion(OnTheTrailAssets.VIGNETTING));
        cityMap = new CityMap(getData().getMapController(), this);
        cityMap.setName(CITY);
    }

    private void initMenu(){
        MenuType menuType = MenuType.getMenuType(getGameDifficulty());

        MenuItem[] menuItems = new MenuItem[menuType.getDirections().length];

        for (int i = 0; i < menuItems.length; i++){
            String buttonName = getGameDifficulty() != GameDifficulty.BONUS ? menuType.getButtonsTexture()[i] : OnTheTrailAssets.BUTTON_PATH + menuType.getButtonsTexture()[i];

            menuItems[i] = new MenuItem(
                    menuType.getDirections()[i],
                    getScreenTextureRegion(buttonName),
                    getScreenTextureRegion(buttonName + "_press"));
        }

        //TODO bonus menu
        menu = getGameDifficulty() == GameDifficulty.BONUS ?
                new BonusMenu(this, cityMap, menuItems, getScreenTextureRegion(OnTheTrailAssets.HELP), getScreenTextureRegion(OnTheTrailAssets.KEYBOARD), getText(OnTheTrailAssets.FINISH_BUT_TEXT)) :
                new MainMenu(this, cityMap, menuItems, getScreenTextureRegion(OnTheTrailAssets.MENU_BACKGROUND), getText(OnTheTrailAssets.FINISH_BUT_TEXT));
        menu.setName(MENU);
        menu.setVisible(false);
    }

    public AbstractMenu getMenu() {
        return menu;
    }


    @Override
    public void finish(){
        if(cityAtmo.isPlaying())
            cityAtmo.stop();

        if(!cityMap.isFinish()) {
            finishAction = true;
            elapsedTime = 0;
            cityMap.finished();
        }else {
            finishAction = false;
            menu.finishEnable(false);
            gameFinished();
        }
    }


    public void detectiveStart(){
        cityAtmo.setLooping(true);
        cityAtmo.play();

        setComponentsPositionAndSize();
        menu.setVisible(true);
        vignetting.setVisible(false);
        triggerScenarioStepEvent(EVENT_ANIM_ROBBER);
    }

    public void setFinishButtonEnable(boolean enable){
        menu.finishEnable(enable);
    }

    private void setComponentsPositionAndSize(){
        float menuWidth = getGameDifficulty() == GameDifficulty.BONUS ? getViewportWidth() * BonusMenu.MENU_W_RATIO : PANEL_WIDTH;
        menu.setBounds(getViewportWidth() - menuWidth, getViewportBottomY(), menuWidth, getViewportHeight());
        menu.setComponentsPositionAndSize();

        vignetting.setComponentsPositionAndSize(0, getViewportBottomY(), getViewportWidth(), getViewportHeight(), getSceneWidth(), getSceneInnerHeight());

        cityMap.setBounds(getSceneLeftX(), getSceneInnerBottomY(), getSceneWidth() - (cityMap.useMenuBorder() ? PANEL_WIDTH : 0), getSceneInnerHeight());
        cityMap.setComponentsPositionAndSize();


        if(ruleGroup != null){
            ScaleUtil.setFullInnerScene(ruleGroup, getStage());
            ruleGroup.setComponentsPositionAndSize(getSceneWidth(), getSceneInnerHeight(), getViewportLeftX(), getViewportBottomY(), getViewportWidth(), getViewportHeight());
        }
    }

    @Override
    public void screenResized(int width, int height) {
        setComponentsPositionAndSize();
    }


//////////////////////////// GAME RESULT

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Collections.singletonList(new SummaryMessage(
                SummaryImage.STATS,
                getFormattedText(OnTheTrailAssets.SUMMARY_TEXT, getData().getScore(), OnTheTrailGameState.CROSSROADS_CNT)));
    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        return ResultMapping.getResult(gameResult).getText();
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        return ResultMapping.getResult(gameResult).getSound();
    }

//////////////////////////// PRELOADER

    private static final String     PRELOADER_ANIM_IMAGE                = "preloader_anim";
    private static final int        PRELOADER_ANIM_FRAMES            = 23;
    private static final float      PRELOADER_ANIM_FRAME_DURATION    = 0.3f;


    private static final String     PRELOADER_TEXT_KEY                  = ApplicationTextManager.ApplicationTextsAssets.GAME_ON_THE_TRAIL_PRELOADER_TEXT;
    private static final Scaling    PRELOADER_IMAGE_SCALING             = Scaling.fit;
    private static final int        PRELOADER_TEXT_ALIGN 			    = Align.left;
    private static final float      PRELOADER_TEXT_PADDING 			    = 10f;
    private static final float 	    PRELOADER_LEFT_COLUMN_WIDTH_RATIO 	= 1f / 4;
    private static final float 	    PRELOADER_RIGHT_COLUMN_RATIO 		= 5f / 8;
    private static final float 	    PRELOADER_ROW_HEIGHT 				= 1f / 3;
    private static final Color      PRELOADER_TEXT_COLOR                = Color.DARK_GRAY;

    @Override
    public void preparePreloaderContent(float width, float height, TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        AnimatedImage preloaderImage = new AnimatedImage(preloaderAssetsManager.getAnimation(PRELOADER_ANIM_IMAGE, PRELOADER_ANIM_FRAMES, PRELOADER_ANIM_FRAME_DURATION), false);
        preloaderImage.startAnimationLoop();
        String preloaderText = preloaderAssetsManager.getText(PRELOADER_TEXT_KEY);
        components.add(new TwoColumnContentDialogComponent(
                new AnimatedImageContentDialogComponent(preloaderImage, PRELOADER_IMAGE_SCALING),
                new TextContentDialogComponent(preloaderText, PRELOADER_TEXT_COLOR, PRELOADER_TEXT_ALIGN, PRELOADER_TEXT_PADDING),
                PRELOADER_LEFT_COLUMN_WIDTH_RATIO,
                PRELOADER_RIGHT_COLUMN_RATIO,
                PRELOADER_ROW_HEIGHT));
    }

}
