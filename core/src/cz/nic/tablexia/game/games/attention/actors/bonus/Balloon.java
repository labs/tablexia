/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.actors.bonus;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.actors.AbstractItem;
import cz.nic.tablexia.game.games.attention.model.CollisionListener;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.model.OnSceneEventListener;

enum BalloonPositionDefinition {
    BAG(GameObjectDefinition.BAG, 8f, 7f, true),
    DOCUMENTS(GameObjectDefinition.DOCUMENTS, 5f, 15f, false),
    KEY(GameObjectDefinition.KEY, 13f, 7f, true);

    private GameObjectDefinition objectDefinition;
    private float offsetX, offsetY;
    private boolean single;

    BalloonPositionDefinition(GameObjectDefinition objectDefinition, float offsetX, float offsetY, boolean single) {
        this.objectDefinition = objectDefinition;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.single = single;
    }

    public static BalloonPositionDefinition getOffsetDefinitionForObjectDefinition(GameObjectDefinition itemDefinition) {
        for (BalloonPositionDefinition offsetDefinition : BalloonPositionDefinition.values()) {
            if (offsetDefinition.getObjectDefinition().equals(itemDefinition))
                return offsetDefinition;
        }
        return null;
    }

    public GameObjectDefinition getObjectDefinition() {
        return objectDefinition;
    }

    public boolean isSingle() {
        return single;
    }

    public float getOffsetX() {
        return offsetX;
    }

    public float getOffsetY() {
        return offsetY;
    }
}

public class Balloon extends AbstractItem {

    private TextureRegion balloonTextureRegion;
    private GameObjectDefinition balloonObjectDefinition;
    private float balloonOffsetX, balloonOffsetY;
    private TextureRegion itemTextureRegion;

    public Balloon(
            AttentionGame attentionGame,
            GameObjectDefinition itemDefinition,
            TextureRegion itemTextureRegion,
            Music collideSound,
            float startPositionX,
            float yPos,
            CollisionListener collisionListener,
            OnSceneEventListener onSceneEventListener,
            float screenWidth,
            float speed) {
        super(
                attentionGame,
                itemDefinition,
                itemTextureRegion,
                collideSound,
                startPositionX,
                yPos,
                collisionListener,
                onSceneEventListener,
                screenWidth,
                speed);
        this.attentionGame = attentionGame;
        BalloonPositionDefinition positionDefinition = BalloonPositionDefinition.getOffsetDefinitionForObjectDefinition(itemDefinition);
        this.balloonObjectDefinition = positionDefinition.isSingle() ? GameObjectDefinition.BALLOON : GameObjectDefinition.BALLOONS;
        this.balloonTextureRegion = attentionGame.getTextureRegionForAtlas(attentionGame.getAtlasPathForDifficulty(attentionGame.getGameDifficulty()),
                balloonObjectDefinition.getTextureName());
        this.itemTextureRegion = itemTextureRegion;
        balloonOffsetX = positionDefinition.getOffsetX();
        balloonOffsetY = getHeight() - positionDefinition.getOffsetY();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(
                balloonTextureRegion,
                getX() + balloonOffsetX,
                getY() + balloonOffsetY,
                balloonObjectDefinition.getPreferredWidth(),
                balloonObjectDefinition.getPreferredHeight());
        batch.draw(itemTextureRegion, getX(), getY(), getWidth(), getHeight());
    }

    @Override
    protected void updatePosition(float delta) {
        moveBy(-delta * speed, ZERO_POSITION_CHANGE);
    }

    @Override
    protected void onCollide(boolean withSound) {
        addAction(Actions.sequence(Actions.fadeOut(FADE_OUT_DURATION), Actions.removeActor()));
        if (collideSound != null && withSound)
            collideSound.play();
    }

    @Override
    protected void checkRemoveCondition() {
        if (!used
                && collisionListener.doesCollideWithDetectiveStrict(
                getX(), getY(), getWidth(), getHeight())) {
            onCollide(!attentionGame.getDoneItems().contains(objectDefinition));
            onSceneEventListener.onPickedUp(this);
            used = true;
        }
    }

    @Override
    protected void checkUsedCondition() {
        if (getX() <= HORIZONTAL_THRESHOLD) {
            clearActions();
            remove();
        }
    }
}
