/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.actors;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.model.CollisionListener;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.model.GameObjectTypeDefinition;
import cz.nic.tablexia.game.games.attention.model.OnSceneEventListener;
import cz.nic.tablexia.util.ScaleUtil;

public abstract class AbstractGameObject extends Image {

    protected static final float FADE_OUT_DURATION = 0.1f;
    protected static final float HORIZONTAL_THRESHOLD = -50f;
    protected static final float DEFAULT_WIDTH = 55;
    protected static final float DEFAULT_HEIGHT = 55;
    protected static final float ZERO_POSITION_CHANGE = 0f;

    protected CollisionListener collisionListener;
    protected OnSceneEventListener onSceneEventListener;
    protected boolean used;
    protected float speed;
    protected float screenWidth;
    protected GameObjectTypeDefinition objectTypeDefinition;
    protected GameObjectDefinition objectDefinition;
    protected TextureRegion textureRegion;
    protected AttentionGame attentionGame;
    protected Music collideSound;

    public AbstractGameObject(
            AttentionGame attentionGame,
            GameObjectDefinition objectDefinition,
            TextureRegion textureRegion,
            Music collideSound,
            float startPositionX,
            float yPos,
            CollisionListener collisionListener,
            OnSceneEventListener onSceneEventListener,
            float screenWidth,
            float speed) {
        super(textureRegion);
        this.attentionGame = attentionGame;
        this.objectDefinition = objectDefinition;
        this.collisionListener = collisionListener;
        this.onSceneEventListener = onSceneEventListener;
        this.speed = speed;
        this.screenWidth = screenWidth;
        this.textureRegion = textureRegion;
        this.collideSound = collideSound;
        this.used = false;
        setFitSize();
        setPosition(startPositionX, yPos);
        setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());
    }

    public AbstractGameObject(
            AttentionGame attentionGame,
            GameObjectDefinition objectDefinition,
            Music collideSound,
            float startPositionX,
            float yPos,
            CollisionListener collisionListener,
            OnSceneEventListener onSceneEventListener,
            float sceenWidth,
            float speed) {
        super();
        this.attentionGame = attentionGame;
        this.collisionListener = collisionListener;
        this.onSceneEventListener = onSceneEventListener;
        this.used = used;
        this.speed = speed;
        this.screenWidth = screenWidth;
        this.objectDefinition = objectDefinition;
        this.collideSound = collideSound;
        setFitSize();
        setPosition(startPositionX, yPos);
    }

    protected void setFitSize() {
        if (objectDefinition.getPreferredHeight() != 0f && objectDefinition.getPreferredWidth() != 0f) {
            setSize(objectDefinition.getPreferredWidth(), objectDefinition.getPreferredHeight());
        } else {
            if (textureRegion.getRegionWidth() > textureRegion.getRegionHeight()) {
                setSize(
                        DEFAULT_WIDTH,
                        ScaleUtil.getHeight(
                                textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), DEFAULT_WIDTH));
            } else if (textureRegion.getRegionWidth() == textureRegion.getRegionHeight()) {
                setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
            } else {
                setSize(
                        ScaleUtil.getWidth(
                                textureRegion.getRegionWidth(), textureRegion.getRegionHeight(), DEFAULT_HEIGHT),
                        DEFAULT_HEIGHT);
            }
        }
    }

    public boolean isUsed() {
        return used;
    }

    public float getSpeed() {
        return speed;
    }

    public GameObjectTypeDefinition getObjectTypeDefinition() {
        return objectTypeDefinition;
    }

    public GameObjectDefinition getObjectDefinition() {
        return objectDefinition;
    }

    @Override
    public void act(float delta) {
        if (!attentionGame.isScreenPaused() && attentionGame.isGameStarted() && !attentionGame.isGameFinished()) {
            super.act(delta);
            updatePosition(delta);
            checkUsedCondition();
            checkRemoveCondition();
        }
    }

    protected abstract void updatePosition(float delta);

    protected abstract void onCollide(boolean withSound);

    protected abstract void checkRemoveCondition();

    protected abstract void checkUsedCondition();
}
