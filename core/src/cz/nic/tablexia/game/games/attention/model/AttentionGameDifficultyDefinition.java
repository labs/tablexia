/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.model;

import java.util.ArrayList;

import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.attention.AttentionGameAssets;
import cz.nic.tablexia.game.games.attention.AttentionGameProperties;
import cz.nic.tablexia.game.games.attention.scene.AbstractAttentionScreen;
import cz.nic.tablexia.game.games.attention.scene.BonusScreen;
import cz.nic.tablexia.game.games.attention.scene.EasyScreen;
import cz.nic.tablexia.game.games.attention.scene.HardScreen;
import cz.nic.tablexia.game.games.attention.scene.MediumScreen;

import static cz.nic.tablexia.game.games.attention.AttentionGameProperties.PICKUP_RULES_EASY;

public enum AttentionGameDifficultyDefinition {
    EASY(
            GameDifficulty.EASY,
            EasyScreen.class,
            AttentionGameAssets.RUNNING_ANIMATION,
            null,
            AttentionGameAssets.RUNNING_ANIMATION_FRAMES_COUNT,
            0,
            AttentionGameProperties.ANIMATION_FRAME_DURATION_EASY,
            AttentionGameProperties.BACKGROUND_SPEED_EASY,
            AttentionGameProperties.FOREGROUND_SPEED_EASY,
            PICKUP_RULES_EASY,
            false,
            false,
            215f,
            303f,
            AttentionGameProperties.OBSTACLES_EASY,
            220f,
            130f,
            180f,
            4f,
            true,
            15,
            false,
            new String[]{AttentionGameAssets.PLATFORM_EASY_1, AttentionGameAssets.PLATFORM_EASY_2}),

    MEDIUM(
            GameDifficulty.MEDIUM,
            MediumScreen.class,
            AttentionGameAssets.BICYCLE_ANIMATION,
            AttentionGameAssets.BICYCLE_JUMP_ANIMATION,
            AttentionGameAssets.BICYCLE_ANIMATION_FRAMES_COUNT,
            AttentionGameAssets.BICYCLE_JUMP_ANIMATION_FRAMES_COUNT,
            AttentionGameProperties.ANIMATION_FRAME_DURATION_MEDIUM,
            AttentionGameProperties.BACKGROUND_SPEED_MEDIUM,
            AttentionGameProperties.FOREGROUND_SPEED_MEDIUM,
            AttentionGameProperties.PICKUP_RULES_MEDIUM,
            true,
            false,
            215f,
            303f,
            AttentionGameProperties.OBSTACLES_MEDIUM,
            245f,
            200f,
            180f,
            4f,
            true,
            13f,
            false,
            new String[]{AttentionGameAssets.PLATFORM_MEDIUM_1, AttentionGameAssets.PLATFORM_MEDIUM_2}),

    HARD(
            GameDifficulty.HARD,
            HardScreen.class,
            AttentionGameAssets.BIKE_ANIMATION,
            AttentionGameAssets.BIKE_JUMP_ANIMATION,
            AttentionGameAssets.BIKE_ANIMATION_FRAMES_COUNT,
            AttentionGameAssets.BIKE_JUMP_ANIMATION_FRAMES_COUNT,
            AttentionGameProperties.ANIMATION_FRAME_DURATION_HARD,
            AttentionGameProperties.BACKGROUND_SPEED_HARD,
            AttentionGameProperties.FOREGROUND_SPEED_HARD,
            AttentionGameProperties.PICKUP_RULES_HARD,
            true,
            true,
            250f,
            310f,
            AttentionGameProperties.OBSTACLES_HARD,
            245f,
            200f,
            180f,
            4f,
            true,
            20f,
            true,
            new String[]{AttentionGameAssets.PLATFORM_HARD}),

    BONUS(
            GameDifficulty.BONUS,
            BonusScreen.class,
            null,
            null,
            0,
            0,
            AttentionGameProperties.ANIMATION_FRAME_DURATION_MEDIUM,
            AttentionGameProperties.BACKGROUND_SPEED_MEDIUM,
            AttentionGameProperties.FOREGROUND_SPEED_MEDIUM,
            AttentionGameProperties.PICKUP_RULES_BONUS,
            true,
            false,
            180,
            260,
            AttentionGameProperties.OBSTACLES_BONUS,
            245f,
            200f,
            180f,
            0f,
            true,
            0f,
            false,
            null);

    public static final float DONE_ITEM_SPAWN_PROBABILITY = 0.3f;
    private GameDifficulty gameDifficulty;
    private Class<? extends AbstractAttentionScreen> screenClass;
    private PickupRule[] pickupRules;
    private float detectiveAnimationSpeed;
    private String detectiveRunAnimation;
    private String detectiveJumpAnimation;
    private int detectiveRunAnimationFramesCount;
    private int detectiveJumpAnimationFramesCount;
    private int backgroundSpeed;
    private int foregroundSpeed;
    private boolean rotatingOnPlatform;
    private boolean rotatingOnJump;
    private float foregroundHeight;
    private float platformHeight;
    private GameObjectDefinition[] obstacles;
    private float platformLeave;
    private float detectiveWidth;
    private float detectiveHeight;
    private float obstacleSpawnRate;
    private boolean platformSpawn;
    private float platformSpawnRate;
    private boolean rampSpawn;
    private String[] platformTextures;

    AttentionGameDifficultyDefinition(
            GameDifficulty gameDifficulty,
            Class<? extends AbstractAttentionScreen> screenClass,
            String detectiveRunAnimation,
            String detectiveJumpAnimation,
            int detectiveRunAnimationFramesCount,
            int detectiveJumpAnimationFramesCount,
            float detectiveAnimationSpeed,
            int backgroundSpeed,
            int foregroundSpeed,
            PickupRule[] pickupRules,
            boolean rotatingOnPlatform,
            boolean rotatingOnJump,
            float foregroundHeight,
            float platformHeight,
            GameObjectDefinition[] obstacles,
            float platformLeave,
            float detectiveWidth,
            float detectiveHeight,
            float obstacleSpawnRate,
            boolean platformSpawn,
            float platformSpawnRate,
            boolean rampSpawn,
            String[] platformTextures) {

        this.gameDifficulty = gameDifficulty;
        this.screenClass = screenClass;
        this.pickupRules = pickupRules;
        this.detectiveAnimationSpeed = detectiveAnimationSpeed;
        this.detectiveRunAnimation = detectiveRunAnimation;
        this.detectiveJumpAnimation = detectiveJumpAnimation;
        this.detectiveRunAnimationFramesCount = detectiveRunAnimationFramesCount;
        this.detectiveJumpAnimationFramesCount = detectiveJumpAnimationFramesCount;
        this.backgroundSpeed = backgroundSpeed;
        this.foregroundSpeed = foregroundSpeed;
        this.rotatingOnPlatform = rotatingOnPlatform;
        this.rotatingOnJump = rotatingOnJump;
        this.foregroundHeight = foregroundHeight;
        this.platformHeight = platformHeight;
        this.obstacles = obstacles;
        this.platformLeave = platformLeave;
        this.detectiveWidth = detectiveWidth;
        this.detectiveHeight = detectiveHeight;
        this.obstacleSpawnRate = obstacleSpawnRate;
        this.platformSpawn = platformSpawn;
        this.platformSpawnRate = platformSpawnRate;
        this.rampSpawn = rampSpawn;
        this.platformTextures = platformTextures;
    }

    public static AttentionGameDifficultyDefinition getDefinitionByGameDifficulty(
            GameDifficulty gameDifficulty) {
        for (int i = 0; i < AttentionGameDifficultyDefinition.values().length; i++) {
            if (AttentionGameDifficultyDefinition.values()[i].getGameDifficulty().equals(gameDifficulty))
                return AttentionGameDifficultyDefinition.values()[i];
        }

        return null;
    }

    public static GameObjectDefinition getRandomItemDefinition(TablexiaRandom random,
                                                               AttentionGameDifficultyDefinition difficultyDefinition,
                                                               ArrayList<GameObjectDefinition> doneList) {
        int randomIndex = random.nextInt(difficultyDefinition.pickupRules.length);
        GameObjectDefinition resultItemDefinition = null;
        while (resultItemDefinition == null) {
            if (doneList.contains(difficultyDefinition.pickupRules[randomIndex].getItemDefinition())) {
                if (random.nextFloat() < DONE_ITEM_SPAWN_PROBABILITY) {
                    resultItemDefinition = difficultyDefinition.pickupRules[randomIndex].getItemDefinition();
                } else {
                    randomIndex = (randomIndex + 1) % difficultyDefinition.pickupRules.length;
                }
            } else {
                resultItemDefinition = difficultyDefinition.pickupRules[randomIndex].getItemDefinition();
            }
        }

        return resultItemDefinition;
    }

    public static GameObjectDefinition getRandomItemDefinition(TablexiaRandom random,
                                                               AttentionGameDifficultyDefinition difficultyDefinition,
                                                               ArrayList<GameObjectDefinition> doneList, PositionDefinition forcedPosition) {
        ArrayList<GameObjectDefinition> availableItems = getItemsForPosition(difficultyDefinition.getPickupRules(), forcedPosition);

        int randomIndex = random.nextInt(availableItems.size());
        GameObjectDefinition resultItemDefinition = null;
        while (resultItemDefinition == null) {
            if (doneList.contains(availableItems.get(randomIndex))) {
                if (random.nextFloat() < DONE_ITEM_SPAWN_PROBABILITY) {
                    resultItemDefinition = availableItems.get(randomIndex);
                } else {
                    randomIndex = (randomIndex + 1) % availableItems.size();
                }
            } else {
                resultItemDefinition = availableItems.get(randomIndex);
            }
        }

        return resultItemDefinition;
    }

    private static ArrayList<GameObjectDefinition> getItemsForPosition(PickupRule[] pickupRules, PositionDefinition forcedPosition) {
        ArrayList itemsForPosition = new ArrayList();
        for(PickupRule pickupRule : pickupRules){
            if (pickupRule.getItemDefinition().getDisabledPositions() == null || containsDisabledPosition(pickupRule.getItemDefinition().getDisabledPositions(), forcedPosition)){
                itemsForPosition.add(pickupRule.getItemDefinition());
            }
        }

        return itemsForPosition;
    }

    private static boolean containsDisabledPosition(PositionDefinition[] disabledPositions, PositionDefinition forcedPosition){
        for(PositionDefinition disabledPosition : disabledPositions){
            if (disabledPosition == forcedPosition)
                return true;
        }
        return false;
    }

    public GameDifficulty getGameDifficulty() {
        return gameDifficulty;
    }

    public Class<? extends AbstractAttentionScreen> getScreenClass() {
        return screenClass;
    }

    public String getDetectiveRunAnimation() {
        return detectiveRunAnimation;
    }

    public String getDetectiveJumpAnimation() {
        return detectiveJumpAnimation;
    }

    public int getDetectiveRunAnimationFramesCount() {
        return detectiveRunAnimationFramesCount;
    }

    public int getDetectiveJumpAnimationFramesCount() {
        return detectiveJumpAnimationFramesCount;
    }

    public float getDetectiveAnimationSpeed() {
        return detectiveAnimationSpeed;
    }

    public int getBackgroundSpeed() {
        return backgroundSpeed;
    }

    public int getForegroundSpeed() {
        return foregroundSpeed;
    }

    public PickupRule[] getPickupRules() {
        return pickupRules;
    }

    public boolean isRotatingOnPlatform() {
        return rotatingOnPlatform;
    }

    public boolean isRotatingOnJump() {
        return rotatingOnJump;
    }

    public float getForegroundHeight() {
        return foregroundHeight;
    }

    public float getPlatformHeight() {
        return platformHeight;
    }

    public GameObjectDefinition[] getObstacles() {
        return obstacles;
    }

    public float getPlatformLeave() {
        return platformLeave;
    }

    public float getDetectiveWidth() {
        return detectiveWidth;
    }

    public float getDetectiveHeight() {
        return detectiveHeight;
    }

    public float getObstacleSpawnRate() {
        return obstacleSpawnRate;
    }

    public boolean isPlatformSpawn() {
        return platformSpawn;
    }

    public float getPlatformSpawnRate() {
        return platformSpawnRate;
    }

    public boolean isRampSpawn() {
        return rampSpawn;
    }

    public String[] getPlatformTextures() {
        return platformTextures;
    }
}
