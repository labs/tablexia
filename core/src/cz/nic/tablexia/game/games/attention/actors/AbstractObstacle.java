/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.actors;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.model.CollisionListener;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.model.GameObjectTypeDefinition;
import cz.nic.tablexia.game.games.attention.model.OnSceneEventListener;

public abstract class AbstractObstacle extends AbstractGameObject {

    public AbstractObstacle(
            AttentionGame attentionGame,
            GameObjectDefinition objectDefinition,
            TextureRegion textureRegion,
            Music collideSound,
            float startPositionX,
            float yPos,
            CollisionListener collisionListener,
            OnSceneEventListener onSceneEventListener,
            float screenWidth,
            float speed) {
        super(
                attentionGame,
                objectDefinition,
                textureRegion,
                collideSound,
                startPositionX,
                yPos,
                collisionListener,
                onSceneEventListener,
                screenWidth,
                speed);
        this.objectTypeDefinition = GameObjectTypeDefinition.OBSTACLE;
    }

    public AbstractObstacle(
            AttentionGame attentionGame,
            GameObjectDefinition objectDefinition,
            Music collideSound,
            float startPositionX,
            float yPos,
            CollisionListener collisionListener,
            OnSceneEventListener onSceneEventListener,
            float screenWidth,
            float speed) {
        super(
                attentionGame,
                objectDefinition,
                collideSound,
                startPositionX,
                yPos,
                collisionListener,
                onSceneEventListener,
                screenWidth,
                speed);
        this.objectTypeDefinition = GameObjectTypeDefinition.OBSTACLE;
    }
}
