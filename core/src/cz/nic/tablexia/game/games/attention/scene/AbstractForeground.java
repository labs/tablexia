/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.ArrayList;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.AttentionGameAssets;
import cz.nic.tablexia.game.games.attention.model.CollisionListener;
import cz.nic.tablexia.game.games.attention.model.ForegroundPieceTypeDefinition;
import cz.nic.tablexia.game.games.attention.model.OnSceneEventListener;

public abstract class AbstractForeground extends Actor {

    public static final float PLATFORM_OUT_OF_SCENE_X = 245f;
    public static final float PLATFORM_HEIGHT = 180f;
    public static final float PLATFORM_COLLIDE_THRESHOLD = 0.3f;
    protected static final int PLATFORM_SPAWN_RATE = 3;
    protected TextureRegion foregroundTextureRegion;
    protected TextureRegion[] platformFullTextureRegions;
    protected float width;
    protected float foregroundHeight;
    protected float yPos;
    protected int speed;
    protected boolean stop;
    protected boolean createPlatform;
    protected AttentionGame attentionGame;
    protected int foregrounds = 0;

    protected Rectangle bounds1;
    protected Rectangle bounds2;
    protected Rectangle bounds3;
    protected Rectangle bounds4;

    protected ForegroundPiece foregroundPiece1;
    protected ForegroundPiece foregroundPiece2;
    protected ForegroundPiece foregroundPiece3;
    protected ForegroundPiece foregroundPiece4;

    protected ArrayList<ForegroundPiece> foregroundPieces;
    protected CollisionListener collisionListener;
    protected OnSceneEventListener onSceneEventListener;
    protected ForegroundPiece platformPiece;
    protected float platformCollideTimeout;
    protected float platformLeaveThreshold;

    public AbstractForeground(
            AttentionGame attentionGame,
            CollisionListener collisionListener,
            OnSceneEventListener onSceneEventListener,
            TextureRegion[] platforms,
            float yPos,
            int speed) {
        this.foregroundTextureRegion =
                attentionGame.getScreenTextureRegion(AttentionGameAssets.FOREGROUND);
        this.platformFullTextureRegions = platforms;
        this.width = attentionGame.getSceneWidth() / 2f;
        this.yPos = yPos;
        this.collisionListener = collisionListener;
        this.onSceneEventListener = onSceneEventListener;
        setY(yPos);
        this.speed = speed;
        this.stop = false;
        this.attentionGame = attentionGame;
        this.platformCollideTimeout = 0f;
        foregroundPieces = new ArrayList<>();
        createPlatform = false;
        this.platformLeaveThreshold = attentionGame.getDifficultyDefinition().getPlatformLeave();

        float foregroundHeight = attentionGame.getDifficultyDefinition().getForegroundHeight();
        float platformHeight = attentionGame.getDifficultyDefinition().getPlatformHeight();

        setHeight(foregroundHeight);
        initBounds();

        foregroundPiece1 =
                new ForegroundPiece(
                        bounds1,
                        foregroundTextureRegion,
                        ForegroundPieceTypeDefinition.FOREGROUND,
                        foregroundHeight,
                        platformHeight);
        foregroundPiece2 =
                new ForegroundPiece(
                        bounds2,
                        foregroundTextureRegion,
                        ForegroundPieceTypeDefinition.FOREGROUND,
                        foregroundHeight,
                        platformHeight);
        foregroundPiece3 =
                new ForegroundPiece(
                        bounds3,
                        foregroundTextureRegion,
                        ForegroundPieceTypeDefinition.FOREGROUND,
                        foregroundHeight,
                        platformHeight);
        foregroundPiece4 =
                new ForegroundPiece(
                        bounds4,
                        foregroundTextureRegion,
                        ForegroundPieceTypeDefinition.FOREGROUND,
                        foregroundHeight,
                        platformHeight);

        foregroundPieces.add(foregroundPiece1);
        foregroundPieces.add(foregroundPiece2);
        foregroundPieces.add(foregroundPiece3);
        foregroundPieces.add(foregroundPiece4);
    }

    @Override
    public void act(float delta) {
        if (!stop && !attentionGame.isGameFinished() && !attentionGame.isScreenPaused()) {
            if (isBoundReached(delta)) {
                resetBounds();
                updatePlatformPieceReference();
            } else {
                updateXBounds(delta);
                if (platformPiece != null) {
                    platformCollideTimeout += delta;
                    if (platformCollideTimeout >= PLATFORM_COLLIDE_THRESHOLD) {
                        platformCollideTimeout = 0f;
                        if (doesCollideWithForegroundPiece(platformPiece)
                                && platformPiece.getPieceBounds().x >= 70f) {
                            onSceneEventListener.onPlatformHit();
                        }
                    }
                    if (isPlatformLeave()) {
                        onSceneEventListener.onPlatformLeave();
                    }
                }
            }
            super.act(delta);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(
                foregroundPiece1.getPieceTextureRegion(),
                foregroundPiece1.getPieceBounds().x,
                foregroundPiece1.getYPos(),
                foregroundPiece1.getPieceBounds().width,
                foregroundPiece1.getHeight());
        batch.draw(
                foregroundPiece2.getPieceTextureRegion(),
                foregroundPiece2.getPieceBounds().x,
                foregroundPiece2.getYPos(),
                foregroundPiece2.getPieceBounds().width,
                foregroundPiece2.getHeight());
        batch.draw(
                foregroundPiece3.getPieceTextureRegion(),
                foregroundPiece3.getPieceBounds().x,
                foregroundPiece3.getYPos(),
                foregroundPiece3.getPieceBounds().width,
                foregroundPiece3.getHeight());
        batch.draw(
                foregroundPiece4.getPieceTextureRegion(),
                foregroundPiece4.getPieceBounds().x,
                foregroundPiece4.getYPos(),
                foregroundPiece4.getPieceBounds().width,
                foregroundPiece4.getHeight());
    }

    public void stop() {
        stop = true;
    }

    protected abstract boolean isBoundReached(float delta);

    protected abstract Rectangle getNewBounds();

    protected abstract boolean isPlatformLeave();

    protected abstract void initBounds();

    protected abstract void updateXBounds(float delta);

    protected abstract boolean doesCollideWithForegroundPiece(ForegroundPiece foregroundPiece);

    public ForegroundPiece getPlatformPiece() {
        return platformPiece;
    }

    protected void resetBounds() {
        bounds1 = bounds2;
        bounds2 = bounds3;
        bounds3 = bounds4;
        bounds4 = getNewBounds();

        foregroundPiece1.setPieceBounds(bounds1);
        foregroundPiece2.setPieceBounds(bounds2);
        foregroundPiece3.setPieceBounds(bounds3);
        foregroundPiece4.setPieceBounds(bounds4);

        foregroundPiece1.swapWithPiece(foregroundPiece2);
        foregroundPiece2.swapWithPiece(foregroundPiece3);
        foregroundPiece3.swapWithPiece(foregroundPiece4);

        if (createPlatform) {
            foregroundPiece4.setPieceTextureRegion(
                    platformFullTextureRegions[
                            attentionGame.getRandom().nextInt(platformFullTextureRegions.length)]);
            foregroundPiece4.setTypeDefinition(ForegroundPieceTypeDefinition.PLATFORM);
            onSceneEventListener.onPlatformCreated();
            createPlatform = false;
        } else {
            foregroundPiece4.setPieceTextureRegion(foregroundTextureRegion);
            foregroundPiece4.setTypeDefinition(ForegroundPieceTypeDefinition.FOREGROUND);
            foregrounds++;
        }
    }

    protected void updatePlatformPieceReference() {
        platformPiece = null;
        for (ForegroundPiece foregroundPiece : foregroundPieces) {
            if (foregroundPiece.getTypeDefinition().equals(ForegroundPieceTypeDefinition.PLATFORM)) {
                platformPiece = foregroundPiece;
            }
        }
    }

    public void createPlatform() {
        createPlatform = true;
    }

    @Override
    public float getY() {
        return yPos;
    }
}
