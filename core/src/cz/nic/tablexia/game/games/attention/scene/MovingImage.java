/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

import cz.nic.tablexia.game.games.attention.AttentionGame;

public class MovingImage extends Actor {

    private static final int DEFAULT_SPEED = 150;

    private TextureRegion textureRegion;
    private float width;
    private int speed;
    private boolean disableBlending;
    private Rectangle bounds1;
    private Rectangle bounds2;
    private Rectangle bounds3;
    private Rectangle bounds4;
    private boolean stop;
    private AttentionGame attentionGame;

    public MovingImage(
            AttentionGame attentionGame,
            TextureRegion textureRegion,
            float width,
            float height,
            float yPos,
            boolean disableBlending) {
        this(attentionGame, textureRegion, width, height, yPos, disableBlending, DEFAULT_SPEED);
    }

    public MovingImage(
            AttentionGame attentionGame,
            TextureRegion textureRegion,
            float width,
            float height,
            float yPos,
            boolean disableBlending,
            int speed) {
        this.attentionGame = attentionGame;
        this.textureRegion = textureRegion;
        this.width = width / 2f;
        setHeight(height);
        setY(yPos);
        this.disableBlending = disableBlending;
        this.speed = speed;
        this.stop = false;

        bounds1 = new Rectangle(-this.width, getY(), this.width, getHeight());
        bounds2 = new Rectangle(bounds1.x + bounds1.width, getY(), this.width, getHeight());
        bounds3 = new Rectangle(bounds2.x + bounds2.width, getY(), this.width, getHeight());
        bounds4 = new Rectangle(bounds3.x + bounds3.width, getY(), this.width, getHeight());
    }

    @Override
    public void act(float delta) {
        if (!stop && !attentionGame.isScreenPaused() && !attentionGame.isGameFinished()) {
            if (leftBoundsReached(delta)) {
                resetBounds();
            } else {
                updateXBounds(-delta);
            }
        }
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (disableBlending) batch.disableBlending();
        batch.draw(textureRegion, bounds1.x, bounds1.y, width, getHeight());
        batch.draw(textureRegion, bounds2.x, bounds2.y, width, getHeight());
        batch.draw(textureRegion, bounds3.x, bounds3.y, width, getHeight());
        batch.draw(textureRegion, bounds4.x, bounds4.y, width, getHeight());
        if (disableBlending) batch.enableBlending();
    }

    public void stop() {
        stop = true;
    }

    private boolean leftBoundsReached(float delta) {
        return (bounds3.x - (delta * speed)) <= 0;
    }

    private void updateXBounds(float delta) {
        bounds1.x += delta * speed;
        bounds2.x += delta * speed;
        bounds3.x += delta * speed;
        bounds4.x += delta * speed;
    }

    private void resetBounds() {
        bounds1 = bounds2;
        bounds2 = bounds3;
        bounds3 = bounds4;
        bounds4 = new Rectangle(bounds3.x + bounds3.width, getY(), width, getHeight());
    }
}
