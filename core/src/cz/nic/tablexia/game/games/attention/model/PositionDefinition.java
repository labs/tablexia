/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.model;

import java.util.Random;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.attention.AttentionGameProperties;

public enum PositionDefinition {
    BOTTOM(AttentionGameProperties.POSITION_BOTTOM, AttentionGameProperties.POSITION_BOTTOM_MOBILE),
    MID(AttentionGameProperties.POSITION_MID, AttentionGameProperties.POSITION_MID_MOBILE),
    TOP(AttentionGameProperties.POSITION_TOP, AttentionGameProperties.POSITION_TOP_MOBILE);

    private float positionY;
    private float mobilePositionY;

    PositionDefinition(float positionY, float mobilePositionY) {
        this.positionY = positionY;
        this.mobilePositionY = mobilePositionY;
    }

    public static PositionDefinition getRandomPosition(Random r) {
        return PositionDefinition.values()[r.nextInt(PositionDefinition.values().length)];
    }

    public static PositionDefinition getRandomAccessiblePosition(Random r) {
        return PositionDefinition.values()[r.nextInt(PositionDefinition.values().length - 1)];
    }

    public float getPositionY() {
        return TablexiaSettings.getInstance().isRunningOnMobileDevice() ? mobilePositionY : positionY;
    }
}
