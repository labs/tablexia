/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.AttentionGameAssets;
import cz.nic.tablexia.game.games.attention.model.PositionHelper;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.util.MusicUtil;

public class Detective extends Actor {

    private static final int PLATFORM_ROTATION = 20;
    private static final float ROTATION_ENDING_DURATION = 0.2f;
    private static final float ROTATION_START_DURATION = 0.2f;
    private static final float ROTATION_DELAY_DURATION = 0.2f;
    private static final float JUMP_HEIGHT = 130f;
    private static final float BIG_JUMP_HEIGHT = 210f;
    private static final float JUMP_ACTION_DURATION = 0.5f;
    private static final float BIG_JUMP_ACTION_DURATION = 1.6f;
    private static final float PLATFORM_ACTION_CALL_RESET_TIMEOUT = 3f;
    private static final float STAIRS_ACTION_Y = 140f;
    private static final float STAIRS_ACTION_DURATION = 0.6f;
    private static final float FROM_PLATFORM_FALL_Y = JUMP_HEIGHT + STAIRS_ACTION_Y;
    private static final float DETECTIVE_POSITION_X = 50f;
    private static final Interpolation JUMP_INTERPOLATION = Interpolation.linear;

    private Animation animation;
    private Animation jumpAnimation;
    private AttentionGame attentionGame;
    private boolean jumping;
    private float stateTime;
    private float jumpStateTime;
    private PositionHelper positionHelper;
    private boolean onPlatform;
    private boolean upstairsActionCalled;
    private float platformActionCounter = 0f;
    private boolean rotationOnPlatform;
    private boolean rotationOnJump;
    private Music runningSound;
    private Music jumpingSound;
    private Music stairsSound;
    private boolean runningSoundPlaying = false;
    private boolean jumpingSoundPlaying = false;
    private boolean stepsSoundPlaying = false;
    private boolean hasStairsSound;

    public Detective(AttentionGame attentionGame, PositionHelper positionHelper, float y, boolean hasStairsSound) {
        this.attentionGame = attentionGame;
        this.positionHelper = positionHelper;
        this.hasStairsSound = hasStairsSound;
        stateTime = 1f;
        jumpStateTime = 0f;
        rotationOnPlatform = attentionGame.getDifficultyDefinition().isRotatingOnPlatform();
        rotationOnJump = attentionGame.getDifficultyDefinition().isRotatingOnJump();
        animation = attentionGame.getRunningAnimation();
        jumpAnimation = attentionGame.getJumpingAnimation();
        runningSound = attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + AttentionGameAssets.DETECTIVE_MOVING_SOUND);
        jumpingSound = attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + AttentionGameAssets.DETECTIVE_JUMP_SOUND);
        stairsSound = hasStairsSound ? attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + AttentionGameAssets.DETECTIVE_STEPS_SOUND) : null;
        setPosition(DETECTIVE_POSITION_X, y);
        setSize(
                attentionGame.getDifficultyDefinition().getDetectiveWidth(),
                attentionGame.getDifficultyDefinition().getDetectiveHeight());
        setDebug(TablexiaSettings.getInstance().isShowBoundingBoxes());

        runningSound.setLooping(true);
    }

    public void jump() {
        if (!jumping) {
            jumping = true;
            runningSound.pause();
            jumpingSound.stop();
            jumpingSound.play();
            if (!onPlatform) {
                if (rotationOnJump) {
                    // TODO add own class
                    addRotatingJumpAction();
                } else {
                    addJumpAction();
                }
            } else {
                onPlatform = false;
                if (positionHelper.willLandOnPlatform(attentionGame.getSceneRightX())) {
                    addJumpAndLandAction();
                } else {
                    addJumpAndFallAction();
                }
            }
        }
    }

    public void bigJump() {
        if (!jumping && upstairsActionCalled) {
            jumping = true;
            Music rampSound = attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + AttentionGameAssets.RAMP_SOUND);
            rampSound.setOnCompletionListener(new Music.OnCompletionListener() {
                @Override
                public void onCompletion(Music music) {
                    rampSound.dispose();
                }
            });
            runningSound.pause();
            // TODO: 8/11/20 ramp sound
            rampSound.play();
            addAction(
                    Actions.sequence(
                            Actions.delay(0.1f),
                            Actions.parallel(
                                    Actions.moveBy(
                                            0, BIG_JUMP_HEIGHT, BIG_JUMP_ACTION_DURATION * 0.5f, JUMP_INTERPOLATION),
                                    Actions.sequence(
                                            Actions.rotateBy(15f, BIG_JUMP_ACTION_DURATION * 0.25f),
                                            Actions.rotateBy(-10, BIG_JUMP_ACTION_DURATION * 0.25f))),
                            Actions.delay(BIG_JUMP_ACTION_DURATION * 0.05f),
                            Actions.parallel(
                                    Actions.moveBy(
                                            0, -BIG_JUMP_HEIGHT, BIG_JUMP_ACTION_DURATION * 0.4f, JUMP_INTERPOLATION),
                                    Actions.sequence(
                                            Actions.delay(BIG_JUMP_ACTION_DURATION * 0.25f),
                                            Actions.rotateBy(-5f, 0.05f))),
                            Actions.run(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            jumping = false;
                                            runningSound.play();
                                            resetGoingUpstairsAction();
                                            jumpStateTime = 0f;
                                        }
                                    })));
        }
    }

    public void goUpstairs() {
        if (!jumping && upstairsActionCalled) {
            if (hasStairsSound && stairsSound != null) {
                runningSound.pause();
                stairsSound.play();
            }
            onPlatform = true;
            if (rotationOnPlatform) {
                addAction(
                        Actions.parallel(
                                Actions.moveBy(0, STAIRS_ACTION_Y, STAIRS_ACTION_DURATION),
                                Actions.sequence(
                                        Actions.rotateBy(PLATFORM_ROTATION, ROTATION_START_DURATION),
                                        Actions.delay(ROTATION_DELAY_DURATION),
                                        Actions.rotateBy(-PLATFORM_ROTATION, ROTATION_ENDING_DURATION))));
            } else {
                addAction(Actions.moveBy(0, STAIRS_ACTION_Y, STAIRS_ACTION_DURATION));
            }
        }
    }

    public void goDownstairs() {
        if (!jumping && onPlatform) {
            onPlatform = false;
            if(hasStairsSound && stairsSound != null) {
                stairsSound.play();
                stairsSound.setOnCompletionListener(new Music.OnCompletionListener() {
                    @Override
                    public void onCompletion(Music music) {
                        runningSound.play();
                    }
                });
            }
            if (rotationOnPlatform) {
                addAction(
                        Actions.parallel(
                                Actions.moveBy(0, -STAIRS_ACTION_Y, STAIRS_ACTION_DURATION),
                                Actions.sequence(
                                        Actions.rotateBy(-PLATFORM_ROTATION, ROTATION_START_DURATION),
                                        Actions.delay(ROTATION_DELAY_DURATION),
                                        Actions.rotateBy(PLATFORM_ROTATION, ROTATION_ENDING_DURATION))));
            } else {
                addAction(Actions.moveBy(0, -STAIRS_ACTION_Y, STAIRS_ACTION_DURATION));
            }
        }
    }

    public boolean isOnPlatform() {
        return onPlatform;
    }

    public void setOnPlatform(boolean onPlatform) {
        this.onPlatform = onPlatform;
    }

    public void resetGoingUpstairsAction() {
        upstairsActionCalled = false;
    }

    public void setGoingUpstairsAction() {
        upstairsActionCalled = true;
    }

    public void playRunningSound() {
        if (runningSound != null) {
            runningSound.play();
        }
    }

    public void pauseSounds() {
        if (runningSound != null) {
            runningSoundPlaying = runningSound.isPlaying();
            runningSound.pause();
        }

        if(jumpingSound != null){
            jumpingSoundPlaying = jumpingSound.isPlaying();
            jumpingSound.pause();
        }

        if(stairsSound != null){
            stepsSoundPlaying = stairsSound.isPlaying();
            stairsSound.pause();
        }
    }

    public void resumeSounds() {
        if (runningSound != null && runningSoundPlaying) {
            runningSound.play();
        }

        if (jumpingSound != null && jumpingSoundPlaying) {
            jumpingSound.play();
        }

        if (stairsSound != null && stepsSoundPlaying) {
            stairsSound.play();
        }
    }

    @Override
    public void act(float delta) {
        if (!attentionGame.isScreenPaused() && !attentionGame.isGameFinished()) {
            super.act(delta);

            if (upstairsActionCalled) {
                platformActionCounter += delta;
                if (platformActionCounter >= PLATFORM_ACTION_CALL_RESET_TIMEOUT) {
                    resetGoingUpstairsAction();
                    platformActionCounter = 0f;
                }
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (jumping && jumpAnimation != null) {
            batch.draw(
                    (TextureRegion) jumpAnimation.getKeyFrame(jumpStateTime, false),
                    getX(),
                    getY(),
                    getOriginX(),
                    getOriginY(),
                    getWidth(),
                    getHeight(),
                    getScaleX(),
                    getScaleY(),
                    getRotation());
            if (!attentionGame.isScreenPaused() && !attentionGame.isGameFinished())
                jumpStateTime += Gdx.graphics.getDeltaTime();
        } else {
            if (!attentionGame.isScreenPaused() && !attentionGame.isGameFinished())
                stateTime += Gdx.graphics.getDeltaTime();
            batch.draw(
                    (TextureRegion) animation.getKeyFrame(stateTime, true),
                    getX(),
                    getY(),
                    getOriginX(),
                    getOriginY(),
                    getWidth(),
                    getHeight(),
                    getScaleX(),
                    getScaleY(),
                    getRotation());
        }
    }

    private void addJumpAndFallAction() {
        addAction(
                Actions.sequence(
                        Actions.moveBy(0, JUMP_HEIGHT, JUMP_ACTION_DURATION, JUMP_INTERPOLATION),
                        Actions.moveBy(
                                0, -FROM_PLATFORM_FALL_Y, JUMP_ACTION_DURATION * 1.5f, JUMP_INTERPOLATION),
                        Actions.run(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        jumping = false;
                                        jumpStateTime = 0f;
                                        runningSound.play();
                                    }
                                })));
    }

    private void addJumpAndLandAction() {
        addAction(
                Actions.sequence(
                        Actions.moveBy(0, JUMP_HEIGHT, JUMP_ACTION_DURATION, JUMP_INTERPOLATION),
                        Actions.moveBy(0, -JUMP_HEIGHT, JUMP_ACTION_DURATION, JUMP_INTERPOLATION),
                        Actions.run(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        onPlatform = true;
                                        jumping = false;
                                        jumpStateTime = 0f;
                                        runningSound.play();
                                    }
                                })));
    }

    private void addJumpAction() {
        addAction(
                Actions.sequence(
                        Actions.moveBy(0, JUMP_HEIGHT, JUMP_ACTION_DURATION, JUMP_INTERPOLATION),
                        Actions.moveBy(0, -JUMP_HEIGHT, JUMP_ACTION_DURATION, JUMP_INTERPOLATION),
                        Actions.run(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        jumping = false;
                                        jumpStateTime = 0f;
                                        runningSound.play();
                                    }
                                })));
    }


    private void addRotatingJumpAction() {
        addAction(
                Actions.sequence(
                        Actions.parallel(Actions.moveBy(0, JUMP_HEIGHT, JUMP_ACTION_DURATION, JUMP_INTERPOLATION), Actions.sequence(Actions.rotateBy(10f, JUMP_ACTION_DURATION / 2f), Actions.rotateBy(-10f, JUMP_ACTION_DURATION / 2f))),
                        Actions.parallel(Actions.moveBy(0, -JUMP_HEIGHT, JUMP_ACTION_DURATION, JUMP_INTERPOLATION), Actions.sequence(Actions.rotateBy(-10f, JUMP_ACTION_DURATION / 2f), Actions.rotateBy(10f, JUMP_ACTION_DURATION / 2f))),
                        Actions.run(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        jumping = false;
                                        jumpStateTime = 0f;
                                        runningSound.play();
                                    }
                                })));
    }

    @Override
    public boolean remove() {
        if (runningSound != null) runningSound.dispose();
        if (jumpingSound != null) jumpingSound.dispose();
        if (stairsSound != null) stairsSound.dispose();
        return super.remove();
    }
}
