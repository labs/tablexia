/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.game.games.attention.AttentionGame;

public class JailAnimation extends Image {

    public static final float ENDING_DELAY_DURATION = 1f;
    private float animationTimer = 0f;
    private AttentionGame attentionGame;
    private Animation animation;
    private TextureRegion animationBackground;

    public JailAnimation(Animation animation, AttentionGame attentionGame) {
        this.animation = animation;
        this.attentionGame = attentionGame;
        this.animationBackground = attentionGame.getColorTextureRegion(Color.BLACK);
    }


    @Override
    public void act(float delta) {
        if (!attentionGame.isScreenPaused()) animationTimer += Gdx.graphics.getDeltaTime();
        if (animation.isAnimationFinished(animationTimer)) {
            addAction(Actions.sequence(Actions.delay(ENDING_DELAY_DURATION), Actions.run(new Runnable() {
                @Override
                public void run() {
                    attentionGame.gameComplete();
                    remove();
                }
            })));
        }
        super.act(delta);
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(animationBackground, getX(), getY(), getWidth(), getHeight());
        batch.draw(
                (TextureRegion) animation.getKeyFrame(animationTimer, false),
                getX(),
                getY(),
                getOriginX(),
                getOriginY(),
                getWidth(),
                getHeight(),
                getScaleX(),
                getScaleY(),
                getRotation());
    }
}
