/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;

import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;

import java.util.ArrayList;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.model.PickupRule;

public class BottomBar extends HorizontalGroup {

    private static final float DEFAULT_SPACE = 20f;

    private AttentionGame attentionGame;
    private PickupRule[] pickupRules;
    private ArrayList<BoardItem> boardItems;
    private ArrayList<GameObjectDefinition> doneItems;

    public BottomBar(AttentionGame attentionGame, PickupRule[] pickupRules) {
        this.attentionGame = attentionGame;
        this.pickupRules = pickupRules;
        this.boardItems = new ArrayList<>();
        this.doneItems = new ArrayList<>();
        for (int i = 0; i < pickupRules.length; i++) {
            boardItems.add(
                    new BoardItem(
                            attentionGame.getScreenTextureRegion(
                                    pickupRules[i].getItemDefinition().getTextureName()),
                            pickupRules[i].getItemDefinition(),
                            pickupRules[i].getCount()));
        }

        space(DEFAULT_SPACE);
        initItems();
    }

    public boolean removeItem(GameObjectDefinition itemDefinition) {
        getBoardItemByDefinition(itemDefinition).decreaseCount();
        if (getBoardItemByDefinition(itemDefinition).isDone()){
            doneItems.add(itemDefinition);
        }
       return getBoardItemByDefinition(itemDefinition).isDone();
    }

    private BoardItem getBoardItemByDefinition(GameObjectDefinition itemDefinition) {
        for (BoardItem boardItem : boardItems) {
            if (boardItem.getItemDefinition().equals(itemDefinition)) return boardItem;
        }

        return null;
    }

    public boolean isRequiredPickedUp() {
        int left = 0;
        for (BoardItem boardItem : boardItems) {
            left += boardItem.getCount();
        }

        return left == 0;
    }

    public ArrayList<GameObjectDefinition> getDoneItems() {
        return doneItems;
    }

    private void initItems() {
        for (BoardItem boardItem : boardItems) {
            addActor(boardItem);
        }
    }
}
