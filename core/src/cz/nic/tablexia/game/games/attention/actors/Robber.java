/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.AttentionGameAssets;
import cz.nic.tablexia.util.MusicUtil;

public class Robber extends Actor {

    private Animation animation;
    private float stateTime;
    private AttentionGame attentionGame;
    private Music runningMusic;

    public Robber(Animation animation, AttentionGame attentionGame) {
        this.animation = animation;
        this.stateTime = 1f;
        this.attentionGame = attentionGame;
        this.runningMusic = attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + AttentionGameAssets.ROBBER_RUNNING_SOUND);
        runningMusic.setLooping(true);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (!attentionGame.isScreenPaused() && !attentionGame.isGameFinished())
            stateTime += Gdx.graphics.getDeltaTime();
        batch.draw(
                (TextureRegion) animation.getKeyFrame(stateTime, true),
                getX(),
                getY(),
                getWidth(),
                getHeight());
    }


    public void playMusic() {
        runningMusic.play();
    }

    public void fadeOutMusic(){
        MusicUtil.fadeOut(runningMusic, 0.5f);
    }

    public void pauseMusic(){
        runningMusic.pause();
    }
}
