/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.model.CollisionListener;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.model.OnSceneEventListener;
import cz.nic.tablexia.util.Log;

public class AnimatedObstacle extends AbstractObstacle {

    private Animation animation;
    private float animationTimer;
    private Music collideSound;

    public AnimatedObstacle(
            AttentionGame attentionGame,
            GameObjectDefinition objectDefinition,
            Animation animation,
            Music collideSound,
            float startPositionX,
            float yPos,
            CollisionListener collisionListener,
            OnSceneEventListener onSceneEventListener,
            float screenWidth,
            float speed) {
        super(
                attentionGame,
                objectDefinition,
                collideSound,
                startPositionX,
                yPos,
                collisionListener,
                onSceneEventListener,
                screenWidth,
                speed);
        this.animation = animation;
        this.animationTimer = 0f;
        this.collideSound = collideSound;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        animationTimer += Gdx.graphics.getDeltaTime();
        batch.draw(
                (TextureRegion) animation.getKeyFrame(animationTimer, true),
                getX(),
                getY(),
                getWidth(),
                getHeight());
    }

    @Override
    protected void updatePosition(float delta) {
        moveBy(-delta * speed, ZERO_POSITION_CHANGE);
    }

    @Override
    protected void onCollide(boolean withSound) {
        Log.debug(getClass().getSimpleName(), "collide goose");
        if(withSound) collideSound.play();
        addAction(Actions.fadeOut(FADE_OUT_DURATION));
    }

    @Override
    protected void checkRemoveCondition() {
        if (getX() <= -getWidth()) {
            clearActions();
            remove();
        }
    }

    @Override
    protected void checkUsedCondition() {
        if (!used
                && collisionListener.doesCollideWithDetectiveFavored(
                getX(), getY(), getWidth(), getHeight())) {
            onCollide(true);
            onSceneEventListener.onHit(this);
            used = true;
            remove();
        }
    }
}
