/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.model.CollisionListener;
import cz.nic.tablexia.game.games.attention.model.OnSceneEventListener;

public class Foreground extends AbstractForeground {
    public Foreground(
            AttentionGame attentionGame,
            CollisionListener collisionListener,
            OnSceneEventListener onSceneEventListener,
            TextureRegion[] textureRegions,
            float yPos,
            int speed) {
        super(attentionGame, collisionListener, onSceneEventListener, textureRegions, yPos, speed);
    }

    @Override
    protected boolean isBoundReached(float delta) {
        return (bounds3.x - (delta * speed)) <= 0;
    }

    @Override
    protected Rectangle getNewBounds() {
        return new Rectangle(bounds3.x + bounds3.width, yPos, this.width, foregroundHeight);
    }

    @Override
    protected boolean isPlatformLeave() {
        return platformPiece.getPieceBounds().x < -platformLeaveThreshold;
    }

    @Override
    protected void initBounds() {
        bounds1 = new Rectangle(-this.width, yPos, this.width, foregroundHeight);
        bounds2 = new Rectangle(bounds1.x + bounds1.width, yPos, this.width, foregroundHeight);
        bounds3 = new Rectangle(bounds2.x + bounds2.width, yPos, this.width, foregroundHeight);
        bounds4 = new Rectangle(bounds3.x + bounds3.width, yPos, this.width, foregroundHeight);
    }

    @Override
    protected void updateXBounds(float delta) {
        bounds1.x -= delta * speed;
        bounds2.x -= delta * speed;
        bounds3.x -= delta * speed;
        bounds4.x -= delta * speed;
    }

    @Override
    protected boolean doesCollideWithForegroundPiece(ForegroundPiece foregroundPiece) {
        return collisionListener.doesCollideWithDetectiveFavored(
                foregroundPiece.getPieceBounds().x,
                foregroundPiece.getPieceBounds().y + foregroundPiece.getHeight() / 2,
                foregroundPiece.getPieceBounds().width,
                foregroundPiece.getHeight());
    }
}
