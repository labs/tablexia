/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.HashMap;
import java.util.Map;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.AttentionGameAssets;
import cz.nic.tablexia.game.games.attention.actors.AbstractItem;
import cz.nic.tablexia.game.games.attention.actors.AbstractObstacle;
import cz.nic.tablexia.game.games.attention.actors.Detective;
import cz.nic.tablexia.game.games.attention.model.AttentionGameDifficultyDefinition;
import cz.nic.tablexia.game.games.attention.model.CollisionListener;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.model.OnSceneEventListener;
import cz.nic.tablexia.game.games.attention.model.PickupRule;
import cz.nic.tablexia.game.games.attention.model.PositionDefinition;
import cz.nic.tablexia.game.games.attention.model.PositionHelper;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ScaleUtil;

public abstract class AbstractAttentionScreen extends AbstractTablexiaScreen
        implements OnSceneEventListener, CollisionListener {

    public static final float SCENE_FADE_OUT_DURATION = 0.2f;
    protected static final float FAVORED_OFFSET_X = 40f;
    protected static final float FAVORED_OFFSET_Y = 27f;
    protected static final float ROBBER_WIDTH = 140;
    protected static final float ROBBER_HEIGHT = 170;
    protected static final float ROBBER_OFFSET_X = 20f;
    protected static final float ROBBER_RELATIVE_OFFSET_Y = 0.2f;
    protected static final float ROBBER_FRAME_DURATION = 0.05f;
    protected static final int ROBBER_FRAMES_COUNT = 12;
    private static final int Y_SWIPE_THRESHOLD = 150;
    private static final float BACKGROUND_RELATIVE_Y = 0.4f;
    private static final String SCORE_TOTAL = "score_total";
    private static final float DETECTIVE_RELATIVE_POSITION_Y = 0.20f;
    private static final float FOREGROUND_OFFSET_Y = 30f;
    protected AttentionGame attentionGame;
    protected AttentionGameDifficultyDefinition difficultyDefinition;
    protected Detective detective;
    protected PositionHelper positionHelper;
    protected Foreground foreground;
    protected MovingImage background;
    protected Group itemsGroup;
    protected Group detectiveGroup;
    protected HashMap<GameObjectDefinition, Music> levelMusic;
    private Image sky;

    public AbstractAttentionScreen(AttentionGame attentionGame) {
        this.attentionGame = attentionGame;
        this.difficultyDefinition = attentionGame.getDifficultyDefinition();
        this.detectiveGroup = new Group();
        this.itemsGroup = new Group();
        this.levelMusic = new HashMap<>();
    }

    @Override
    protected void screenLoaded(Map screenState) {
        addSky();
        addBackground();
        addForeground();
        positionHelper = new PositionHelper(foreground);
        levelMusic = new HashMap<>();
        getStage().addActor(itemsGroup);
        getStage().addActor(detectiveGroup);
        addDetective();
        loadDifficultySounds();
        getStage()
                .addListener(
                        new InputListener() {
                            float startX, startY;

                            @Override
                            public boolean touchDown(
                                    InputEvent event, float x, float y, int pointer, int button) {
                                startX = x;
                                startY = y;
                                return true;
                            }

                            @Override
                            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                                float yDiff = y - startY;
                                if (yDiff > Y_SWIPE_THRESHOLD) {
                                    detective.setGoingUpstairsAction();
                                } else {
                                    detective.jump();
                                }
                                super.touchUp(event, x, y, pointer, button);
                            }

                            @Override
                            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                                super.touchDragged(event, x, y, pointer);
                            }
                        });

        super.screenLoaded(screenState);
    }

    private void addSky() {
        sky = new Image(getScreenTextureRegion(AttentionGameAssets.SKY));
        sky.setSize(getViewportWidth(), getViewportHeight());
        sky.setPosition(getViewportLeftX(), getViewportBottomY());
        getStage().addActor(sky);
    }

    @Override
    protected void screenResized(int width, int height) {
        super.screenResized(width, height);
        if (sky != null) {
            sky.setSize(getViewportWidth(), getViewportHeight());
            sky.setPosition(getViewportLeftX(), getViewportBottomY());
        }
    }

    private void addBackground() {
        TextureRegion backgroundTextureRegion = getScreenTextureRegion(AttentionGameAssets.BACKGROUND);
        float backgroundHeight =
                ScaleUtil.getHeight(
                        backgroundTextureRegion.getRegionWidth(),
                        backgroundTextureRegion.getRegionHeight(),
                        getSceneWidth() / 2);

        Log.debug(getClass(), "" + getViewportHeight() * 0.28f);

        getStage()
                .addActor(
                        background =
                                new MovingImage(
                                        attentionGame,
                                        backgroundTextureRegion,
                                        getSceneWidth(),
                                        backgroundHeight,
                                        attentionGame.getBoardTop() + Math.min(150, getViewportHeight() * 0.28f),
                                        false,
                                        difficultyDefinition.getBackgroundSpeed()));
    }

    private void addForeground() {
        TextureRegion foregroundTextureRegion = getScreenTextureRegion(AttentionGameAssets.FOREGROUND);
        float foregroundHeight =
                ScaleUtil.getHeight(
                        foregroundTextureRegion.getRegionWidth(),
                        foregroundTextureRegion.getRegionHeight(),
                        getSceneWidth() / 2);
        float offset = getForegroundOffset();
        getStage()
                .addActor(
                        foreground =
                                new Foreground(
                                        attentionGame,
                                        this,
                                        this,
                                        attentionGame.getPlatformTextureRegions(),
                                        background.getY() - foregroundHeight + offset,
                                        difficultyDefinition.getForegroundSpeed()));
    }

    private void addDetective() {
        detective =
                new Detective(
                        attentionGame,
                        positionHelper,
                        getUpdatedPosition(PositionDefinition.BOTTOM),
                        difficultyDefinition.equals(AttentionGameDifficultyDefinition.EASY));
        detectiveGroup.addActor(detective);
    }

    public float getUpdatedPosition(PositionDefinition positionDefinition){
        return attentionGame.getBoardTop() + positionDefinition.getPositionY() + Math.min(40, getViewportHeight() * 0.05f);
    }

    public void onGamePaused() {
        if (detective != null)
            detective.pauseSounds();
    }

    public void onGameResumed() {
        if (detective != null)
            detective.resumeSounds();
    }

    public AttentionGame getAttentionGame() {
        return attentionGame;
    }

    protected float getForegroundOffset() {
        return FOREGROUND_OFFSET_Y;
    }

    protected GameObjectDefinition getNextItemDefinition() {
        return AttentionGameDifficultyDefinition.getRandomItemDefinition(attentionGame.getRandom(), difficultyDefinition, attentionGame.getData().getDoneItems());
    }

    protected GameObjectDefinition getNextItemDefinition(PositionDefinition forcedPosition) {
        return AttentionGameDifficultyDefinition.getRandomItemDefinition(attentionGame.getRandom(), difficultyDefinition, attentionGame.getData().getDoneItems(), forcedPosition);
    }

    public void stopAnimation() {
        detectiveGroup.addAction(Actions.sequence(Actions.fadeOut(SCENE_FADE_OUT_DURATION), Actions.removeActor()));
        itemsGroup.addAction(Actions.sequence(Actions.fadeOut(SCENE_FADE_OUT_DURATION), Actions.removeActor()));
        if (foreground != null) foreground.stop();
        if (background != null) background.stop();
    }


    public abstract void onGameVisible();

    protected void loadDifficultySounds() {
        for (PickupRule pickupRule : attentionGame.getDifficultyDefinition().getPickupRules()) {
            Music collideSound = attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + pickupRule.getItemDefinition().getCollideSoundName());
            levelMusic.put(pickupRule.getItemDefinition(), collideSound);
            addDisposable(collideSound);
        }

        for (GameObjectDefinition objectDefinition : attentionGame.getDifficultyDefinition().getObstacles()) {
            Music collideSound = attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + objectDefinition.getCollideSoundName());
            levelMusic.put(objectDefinition, collideSound);
            addDisposable(collideSound);
        }
    }

    @Override
    public void onPickedUp(AbstractItem objectDefinition) {
        attentionGame.pickup(objectDefinition);
    }

    @Override
    public void onHit(AbstractObstacle obstacleDefinition) {
        if (!attentionGame.isGameFinished()) attentionGame.hit();
    }

    @Override
    public void onPlatformHit() {
        if (!detective.isOnPlatform()) {
            detective.goUpstairs();
        }
    }

    @Override
    public void onPlatformLeave() {
        if (detective.isOnPlatform()) {
            detective.goDownstairs();
        }
    }

    @Override
    public void onRampHit() {
        detective.bigJump();
    }


    protected abstract void addItem(PositionDefinition positionDefinition, GameObjectDefinition objectDefinition, float offsetX);


    @Override
    public void onPlatformCreated() {
        int randIndex = attentionGame.getRandom().nextInt(difficultyDefinition.getPickupRules().length);
        GameObjectDefinition firstDefinition = difficultyDefinition.getPickupRules()[randIndex].getItemDefinition();
        GameObjectDefinition secondDefinition = difficultyDefinition.getPickupRules()[(randIndex + 1) % difficultyDefinition.getPickupRules().length].getItemDefinition();
        addItem(PositionDefinition.TOP, firstDefinition, 220f);
        addItem(PositionDefinition.BOTTOM, secondDefinition, 220f);
    }

    @Override
    public boolean doesCollideWithDetectiveFavored(float x, float y, float width, float height) {
        Actor actor = detective;

        return !(actor.getX() > x + width - FAVORED_OFFSET_X)
                && !(x > actor.getRight() - FAVORED_OFFSET_X)
                && !(actor.getTop() < y + FAVORED_OFFSET_Y)
                && !(y + height < actor.getY() + FAVORED_OFFSET_Y);
    }

    @Override
    public boolean doesCollideWithDetectiveStrict(float x, float y, float width, float height) {
        Actor actor = detective;
        return !(actor.getX() > x + width)
                && !(x > actor.getRight())
                && !(actor.getTop() < y)
                && !(y + height < actor.getY());
    }

    @Override
    protected String prepareScreenAtlasPath(String screenAssetsPath, String screenName) {
        // no atlas loading
        return null;
    }

    @Override
    protected String prepareScreenTextResourcesAssetName() {
        // no text loading
        return null;
    }

    @Override
    public void backButtonPressed() {
        // disabled method
    }

    //////////////////////////// ASSETS ACCESS

    @Override
    public TextureRegion getTextureRegionForAtlas(String atlasName, String regionName) {
        return attentionGame.getTextureRegionForAtlas(atlasName, regionName);
    }

    @Override
    public Animation getScreenAnimation(String regionName, int framesCount, float frameDuration) {
        return attentionGame.getScreenAnimation(regionName, framesCount, frameDuration);
    }

    @Override
    public TextureRegion getScreenTextureRegion(String regionName) {
        return attentionGame.getScreenTextureRegion(regionName);
    }

    @Override
    public Sound getSound(String soundName) {
        return attentionGame.getSound(soundName);
    }

    @Override
    public String getText(String key) {
        return attentionGame.getText(key);
    }

    @Override
    public String getFormattedText(String key, Object... args) {
        return attentionGame.getFormattedText(key, args);
    }

    protected TextureRegion getGameGlobalTextureRegion(String regionName) {
        return attentionGame.getGameGlobalTextureRegion(regionName);
    }
}
