/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.model;

import java.util.Random;

import cz.nic.tablexia.game.games.attention.AttentionGameAssets;

public enum GameObjectDefinition {
    BAG(AttentionGameAssets.BAG, AttentionGameAssets.BAG_SOUND, GameObjectTypeDefinition.ITEM),
    DOCUMENTS(AttentionGameAssets.DOCUMENTS, AttentionGameAssets.DOCUMENTS_SOUND, GameObjectTypeDefinition.ITEM),
    FINGER(AttentionGameAssets.FINGER, AttentionGameAssets.FINGER_SOUND, GameObjectTypeDefinition.ITEM),
    FOOTPRINT(AttentionGameAssets.FOOTPRINT, AttentionGameAssets.FOOTPRINT_SOUND, GameObjectTypeDefinition.ITEM, false, false, 0f, 0f, 0f, new PositionDefinition[]{PositionDefinition.BOTTOM}),
    KEY(AttentionGameAssets.KEY, AttentionGameAssets.KEY_SOUND, GameObjectTypeDefinition.ITEM),
    MAGNIFIER(AttentionGameAssets.MAGNIFIER, AttentionGameAssets.MAGNIFIER_SOUND, GameObjectTypeDefinition.ITEM),
    MUGSHOT(AttentionGameAssets.MUGSHOT, AttentionGameAssets.MUGSHOT_SOUND, GameObjectTypeDefinition.ITEM),
    BANANA(AttentionGameAssets.BANANA, AttentionGameAssets.BANANA_SOUND, GameObjectTypeDefinition.OBSTACLE, false, false, 60f, 35f, 10f),
    BOMB(AttentionGameAssets.BOMB, AttentionGameAssets.BOMB_SOUND, GameObjectTypeDefinition.OBSTACLE, false),
    TRAP(AttentionGameAssets.TRAP, AttentionGameAssets.TRAP_SOUND, GameObjectTypeDefinition.OBSTACLE, false, 60f, 45f, 0f),
    BALLOON(
            AttentionGameAssets.BALLOON, null, GameObjectTypeDefinition.OBSTACLE, false, false, 40f, 60f, 0f),
    BALLOONS(
            AttentionGameAssets.BALLOONS, null, GameObjectTypeDefinition.OBSTACLE, false, false, 70, 60f, 0f),
    CHIMNEY(
            AttentionGameAssets.CHIMNEY,
            AttentionGameAssets.CHIMNEY_SOUND,
            GameObjectTypeDefinition.OBSTACLE,
            false,
            false,
            80f,
            230f,
            -70f),
    CLOUD(AttentionGameAssets.CLOUD, AttentionGameAssets.CHIMNEY_SOUND, GameObjectTypeDefinition.OBSTACLE, false, false, 150f, 80f, 0f),
    NEST(AttentionGameAssets.NEST, AttentionGameAssets.NEST_SOUND, GameObjectTypeDefinition.OBSTACLE, false, false, 80f, 260f, -70f),
    WIND_INDICATOR(
            AttentionGameAssets.WIND_INDICATOR,
            AttentionGameAssets.CHIMNEY_SOUND,
            GameObjectTypeDefinition.OBSTACLE,
            false,
            false,
            80f,
            240f,
            -70f),
    GOOSE(
            AttentionGameAssets.GOOSE_ANIMATION,
            AttentionGameAssets.GOOSE_SOUND,
            GameObjectTypeDefinition.OBSTACLE,
            true,
            false,
            120f,
            70f,
            30f),
    HOLE(AttentionGameAssets.HOLE, AttentionGameAssets.HOLE_SOUND, GameObjectTypeDefinition.OBSTACLE, false, false, 130f, 130f, -35f),
    RAMP(AttentionGameAssets.RAMP, AttentionGameAssets.RAMP_SOUND, GameObjectTypeDefinition.SCENE, false, false, 140f, 140f, -30f);

    private static final int ITEMS_COUNT = 7;
    private static final int OBJECTS_COUNT = 3;

    private String textureName;
    private String collideSoundName;
    private GameObjectTypeDefinition typeDefinition;
    private boolean animated;
    private boolean shared;
    private float preferredWidth;
    private float preferredHeight;
    private float offsetY;
    private PositionDefinition[] disabledPositions;

    GameObjectDefinition(
            String textureName,
            String collideSoundName,
            GameObjectTypeDefinition typeDefinition,
            boolean animated,
            boolean shared,
            float preferredWidth,
            float preferredHeight,
            float offsetY,
            PositionDefinition[] disabledPositions) {
        this.textureName = textureName;
        this.collideSoundName = collideSoundName;
        this.typeDefinition = typeDefinition;
        this.animated = animated;
        this.shared = shared;
        this.preferredHeight = preferredHeight;
        this.preferredWidth = preferredWidth;
        this.offsetY = offsetY;
        this.disabledPositions = disabledPositions;
    }

    GameObjectDefinition(
            String textureName,
            String collideSoundName,
            GameObjectTypeDefinition typeDefinition,
            boolean animated,
            boolean shared,
            float preferredWidth,
            float preferredHeight,
            float offsetY) {
        this(textureName, collideSoundName, typeDefinition, animated, shared, preferredWidth, preferredHeight, offsetY, null);
    }

    GameObjectDefinition(
            String textureName,
            String collideSoundName,
            GameObjectTypeDefinition typeDefinition,
            boolean shared,
            float preferredWidth,
            float preferredHeight,
            float offsetY) {
        this(textureName, collideSoundName, typeDefinition, false, shared, preferredWidth, preferredHeight, offsetY, null);
    }

    GameObjectDefinition(
            String textureName, String collideSoundName, GameObjectTypeDefinition typeDefinition, boolean shared) {
        this(textureName, collideSoundName, typeDefinition, false, shared, 0, 0, 0, null);
    }

    GameObjectDefinition(String textureName, String collideSoundName, GameObjectTypeDefinition typeDefinition) {
        this(textureName, collideSoundName, typeDefinition, true);
    }

    public static final GameObjectDefinition getRandomItem(Random r) {
        return values()[r.nextInt(ITEMS_COUNT)];
    }

    public static final GameObjectDefinition getRandomObstacle(
            AttentionGameDifficultyDefinition difficultyDefinition, Random r) {
        return difficultyDefinition
                .getObstacles()[r.nextInt(difficultyDefinition.getObstacles().length)];
    }

    public PositionDefinition[] getDisabledPositions() {
        return disabledPositions;
    }

    public float getPreferredWidth() {
        return preferredWidth;
    }

    public float getPreferredHeight() {
        return preferredHeight;
    }

    public float getOffsetY() {
        return offsetY;
    }

    public String getTextureName() {
        return textureName;
    }

    public String getCollideSoundName() {
        return collideSoundName;
    }

    public boolean isAnimated() {
        return animated;
    }

    public GameObjectTypeDefinition getTypeDefinition() {
        return typeDefinition;
    }

    public boolean isShared() {
        return shared;
    }
}
