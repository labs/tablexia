/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention;

import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.model.PickupRule;

public class AttentionGameProperties {

    public static final float POSITION_TOP = 320f;
    public static final float POSITION_TOP_MOBILE = 320f;
    public static final float POSITION_MID = 230f;
    public static final float POSITION_MID_MOBILE = 230f;
    public static final float POSITION_BOTTOM = 0f;
    public static final float POSITION_BOTTOM_MOBILE = 0f;

    public static final float POSITION_TOP_BONUS = 0.75f;
    public static final float POSITION_MIDDLE_BONUS = 0.6f;
    public static final float POSITION_BOTTOM_BONUS = 0.3f;

    public static final String SCORE_TOTAL = "score_total";

    public static final float ANIMATION_FRAME_DURATION_EASY = 0.062f;
    public static final float ANIMATION_FRAME_DURATION_MEDIUM = 0.055f;
    public static final float ANIMATION_FRAME_DURATION_HARD = 0.05f;

    public static final int BACKGROUND_SPEED_EASY = 130;
    public static final int BACKGROUND_SPEED_MEDIUM = 150;
    public static final int BACKGROUND_SPEED_HARD = 140;

    public static final int FOREGROUND_SPEED_EASY = 250;
    public static final int FOREGROUND_SPEED_MEDIUM = 300;
    public static final int FOREGROUND_SPEED_HARD = 400;
    public static final String[] RESULT_TEXT =
            new String[]{
                    "game_attention_result_0",
                    "game_attention_result_1",
                    "game_attention_result_2",
                    "game_attention_result_3"
            };
    public static final String RESULT_TEXT_SUMMARY = "game_attention_stats";
    // TODO: 5/22/20 change pickup rules
    public static final PickupRule[] PICKUP_RULES_EASY =
            new PickupRule[]{
                    new PickupRule(GameObjectDefinition.KEY, 9),
                    new PickupRule(GameObjectDefinition.DOCUMENTS, 12),
                    new PickupRule(GameObjectDefinition.FINGER, 9)
            };
    public static final PickupRule[] PICKUP_RULES_MEDIUM =
            new PickupRule[]{
                    new PickupRule(GameObjectDefinition.KEY, 8),
                    new PickupRule(GameObjectDefinition.MUGSHOT, 9),
                    new PickupRule(GameObjectDefinition.MAGNIFIER, 7),
                    new PickupRule(GameObjectDefinition.BAG, 8)
            };
    public static final PickupRule[] PICKUP_RULES_HARD =
            new PickupRule[]{
                    new PickupRule(GameObjectDefinition.FINGER, 12),
                    new PickupRule(GameObjectDefinition.BAG, 12),
                    new PickupRule(GameObjectDefinition.FOOTPRINT, 11)
            };
    public static final PickupRule[] PICKUP_RULES_BONUS =
            new PickupRule[]{
                    new PickupRule(GameObjectDefinition.KEY, 11),
                    new PickupRule(GameObjectDefinition.BAG, 12),
                    new PickupRule(GameObjectDefinition.DOCUMENTS, 12)
            };
    public static final GameObjectDefinition[] OBSTACLES_EASY =
            new GameObjectDefinition[]{
                    GameObjectDefinition.BANANA, GameObjectDefinition.BOMB, GameObjectDefinition.TRAP
            };
    public static final GameObjectDefinition[] OBSTACLES_MEDIUM = OBSTACLES_EASY;
    public static final GameObjectDefinition[] OBSTACLES_HARD =
            new GameObjectDefinition[]{GameObjectDefinition.BOMB, GameObjectDefinition.TRAP};
    public static final GameObjectDefinition[] OBSTACLES_BONUS =
            new GameObjectDefinition[]{
                    GameObjectDefinition.CHIMNEY, GameObjectDefinition.NEST, GameObjectDefinition.WIND_INDICATOR
            };
    private static final String SFX_PATH = "common/sfx/";
    public static final String[] RESULT_SOUNDS =
            new String[]{
                    SFX_PATH + "result_0.mp3",
                    SFX_PATH + "result_1.mp3",
                    SFX_PATH + "result_2.mp3",
                    SFX_PATH + "result_3.mp3"
            };
}
