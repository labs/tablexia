/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.actors.bonus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.AttentionGameAssets;

public class BonusDetective extends Actor {

    public static final float FALLING_VALUE_Y = 2f;
    public static final float ELEVATE_MOVE_DURATION = 0.1f;
    public static final float ELEVATE_MOVE_VALUE = 45f;
    public static final float OUT_OF_SCENE_THRESHOLD_BOTTOM = 50f;
    public static final float OUT_OF_SCENE_THRESHOLD_TOP = 520f;

    private Animation animation;
    private AttentionGame attentionGame;
    private float animationTimer = 0f;
    private Music movingSound;
    private Music elevatingSound;

    public BonusDetective(
            Animation animation,
            AttentionGame attentionGame,
            float x,
            float y,
            float width,
            float height) {
        this.animation = animation;
        this.attentionGame = attentionGame;
        this.movingSound = attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + AttentionGameAssets.DETECTIVE_MOVING_SOUND);
        this.elevatingSound = attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + AttentionGameAssets.ELEVATING_SOUND);
        movingSound.setLooping(true);
        attentionGame.setScreenEnabled(false);
        setPosition(x, y);
        setSize(width, height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (!attentionGame.isScreenPaused()) animationTimer += Gdx.graphics.getDeltaTime();
        batch.draw(
                (TextureRegion) animation.getKeyFrame(animationTimer, true),
                getX(),
                getY(),
                getOriginX(),
                getOriginY(),
                getWidth(),
                getHeight(),
                getScaleX(),
                getScaleY(),
                getRotation());
    }

    public void playElevatingSound() {
        elevatingSound.play();
        movingSound.pause();
        elevatingSound.setOnCompletionListener(new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music music) {
                movingSound.play();
            }
        });
    }

    public void startMovingSound() {
        movingSound.play();
    }

    public void stopMovingSound() {
        movingSound.pause();
    }

    @Override
    public void act(float delta) {
        if (attentionGame.isSceneEnabled()
                && attentionGame.isGameStarted()
                && !attentionGame.isScreenPaused()) {
            if (getActions().size == 0) {
                moveBy(0, -FALLING_VALUE_Y);
            }
            super.act(delta);
        }

        if (getY() < OUT_OF_SCENE_THRESHOLD_BOTTOM || getY() > OUT_OF_SCENE_THRESHOLD_TOP) {
            attentionGame.finishGameWithNoHealth();
        }
    }

    public void elevate() {
        addAction(
                Actions.sequence(Actions.moveBy(0, ELEVATE_MOVE_VALUE, ELEVATE_MOVE_DURATION)));

    }

    @Override
    public void moveBy(float x, float y) {
        super.moveBy(x, getY() + y >= OUT_OF_SCENE_THRESHOLD_TOP ? 0 : y);
    }
}
