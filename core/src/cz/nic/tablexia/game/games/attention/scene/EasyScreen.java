/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import java.util.Map;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.AttentionGameAssets;
import cz.nic.tablexia.game.games.attention.actors.AbstractItem;
import cz.nic.tablexia.game.games.attention.actors.AbstractObstacle;
import cz.nic.tablexia.game.games.attention.actors.Item;
import cz.nic.tablexia.game.games.attention.actors.Obstacle;
import cz.nic.tablexia.game.games.attention.actors.Robber;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.model.PositionDefinition;

public class EasyScreen extends AbstractAttentionScreen {

    private static final float ITEM_START_POSITION_OFFSET_X = 10f;
    private static final float ITEM_UPDATE_TIMEOUT = 1.3f;

    private Robber robber;
    private PositionDefinition lastItemPosition;
    private float timeSinceLastObstacle = 0f;
    private float timeSinceLastObject = 0f;
    private float timeSincePlatform = 0f;

    public EasyScreen(AttentionGame attentionGame) {
        super(attentionGame);
    }

    @Override
    protected void screenLoaded(Map screenState) {
        super.screenLoaded(screenState);
        addRobber();
    }

    @Override
    protected void screenAct(float delta) {
        if (!attentionGame.isScreenPaused() && attentionGame.isGameStarted() && !attentionGame.isGameFinished()) {
            timeSinceLastObstacle += delta;
            timeSinceLastObject += delta;
            timeSincePlatform += delta;
            updateScene();
            super.screenAct(delta);
        }
    }

    private void addRobber() {
        detectiveGroup
                .addActor(
                        robber =
                                new Robber(
                                        attentionGame.getScreenAnimation(
                                                AttentionGameAssets.ROBBER_ANIMATION,
                                                ROBBER_FRAMES_COUNT,
                                                ROBBER_FRAME_DURATION),
                                        attentionGame));
        robber.setSize(ROBBER_WIDTH, ROBBER_HEIGHT);
        robber.setPosition(
                getSceneWidth() - robber.getWidth() - ROBBER_OFFSET_X,
                getUpdatedPosition(PositionDefinition.BOTTOM));
    }

    private void updateScene() {
        if (timeSincePlatform > difficultyDefinition.getPlatformSpawnRate()) {
            foreground.createPlatform();
            timeSincePlatform = 0f;
        }


        if (timeSinceLastObject > ITEM_UPDATE_TIMEOUT && platformBoundsCheck()) {
            if (timeSinceLastObstacle >= difficultyDefinition.getObstacleSpawnRate()) {
                addObstacle();
                timeSinceLastObstacle = 0f;
            } else {
                addItem(null, null, 0f);
            }
            timeSinceLastObject = 0f;
        }
    }

    private boolean platformBoundsCheck() {
        return timeSincePlatform >= 5f && timeSincePlatform <= 14f;
    }

    @Override
    protected void addItem(PositionDefinition forcedPosition, GameObjectDefinition objectDefinition, float offsetX) {
        GameObjectDefinition itemDefinition = objectDefinition == null ? getNextItemDefinition() : objectDefinition;
        PositionDefinition positionDefinition = forcedPosition != null ? forcedPosition :
                getNextItemPosition();

        AbstractItem item =
                new Item(
                        attentionGame,
                        itemDefinition,
                        getScreenTextureRegion(itemDefinition.getTextureName()),
                        levelMusic.get(itemDefinition),
                        getSceneRightX() + ITEM_START_POSITION_OFFSET_X + offsetX,
                        getUpdatedPosition(positionDefinition) + itemDefinition.getOffsetY(),
                        this,
                        this,
                        getSceneWidth(),
                        difficultyDefinition.getForegroundSpeed());
        itemsGroup.addActor(item);
    }

    @Override
    public void onGameVisible() {
        if (robber != null) {
            robber.playMusic();
            robber.addAction(Actions.sequence(Actions.delay(2f), Actions.moveBy(100f, 0f, 1f), Actions.run(new Runnable() {
                @Override
                public void run() {
                    robber.fadeOutMusic();
                }
            }), Actions.removeActor()));
        }
        detective.playRunningSound();
    }

    private PositionDefinition getNextItemPosition() {
        PositionDefinition positionDefinition =
                PositionDefinition.getRandomAccessiblePosition(attentionGame.getRandom());
        lastItemPosition = positionDefinition;
        return positionDefinition;
    }

    private void addObstacle() {
        GameObjectDefinition obstacleDefinition =
                GameObjectDefinition.getRandomObstacle(difficultyDefinition, attentionGame.getRandom());
        PositionDefinition positionDefinition = PositionDefinition.BOTTOM;
        TextureRegion textureRegion =
                obstacleDefinition.isShared()
                        ? getScreenTextureRegion(obstacleDefinition.getTextureName())
                        : getTextureRegionForAtlas(
                        attentionGame.getAtlasPathForDifficulty(difficultyDefinition.getGameDifficulty()),
                        obstacleDefinition.getTextureName());
        AbstractObstacle obstacle =
                new Obstacle(
                        attentionGame,
                        obstacleDefinition,
                        textureRegion,
                        levelMusic.get(obstacleDefinition),
                        getSceneRightX() + ITEM_START_POSITION_OFFSET_X,
                        getUpdatedPosition(positionDefinition) + obstacleDefinition.getOffsetY(),
                        this,
                        this,
                        getSceneWidth(),
                        difficultyDefinition.getForegroundSpeed());
        itemsGroup.addActor(obstacle);
    }
}
