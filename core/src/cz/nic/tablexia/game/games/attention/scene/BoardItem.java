/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.ui.TablexiaLabel;

public class BoardItem extends Group {

    private static final Color EMPTY_COLOR = Color.GRAY;
    private static final Color FULL_COLOR = Color.WHITE;
    private static final float EMPTY_ALPHA = 0.3f;
    private static final float FULL_ALPHA = 1f;
    private static final float DEFAULT_SPACING = 5f;
    private static final float ITEM_DEFAULT_SIZE = 55f;
    private static final TablexiaLabel.TablexiaLabelStyle DEFAULT_LABEL_STYLE =
            new TablexiaLabel.TablexiaLabelStyle(
                    ApplicationFontManager.FontType.REGULAR_14, TablexiaSettings.getDefaultFontColor());

    private Image itemImage;
    private TablexiaLabel countLabel;
    private GameObjectDefinition itemDefinition;
    private int requiredCount;

    public BoardItem(
            TextureRegion textureRegion, GameObjectDefinition itemDefinition, int requiredCount) {
        itemImage = new Image(textureRegion);
        sizeUpItem();
        this.itemDefinition = itemDefinition;
        this.requiredCount = requiredCount;
        init();
    }

    private void init() {
        itemImage.setColor(FULL_COLOR.r, FULL_COLOR.g, FULL_COLOR.b, FULL_ALPHA);
        countLabel = new TablexiaLabel(String.valueOf(requiredCount), DEFAULT_LABEL_STYLE);
        addActor(itemImage);
        countLabel.setPosition(itemImage.getWidth() + DEFAULT_SPACING, 0f);
        addActor(countLabel);
    }

    public boolean decreaseCount() {
        boolean decreased = requiredCount > 0;
        requiredCount = Math.max(requiredCount - 1, 0);
        if (requiredCount == 0) {
            itemImage.setColor(EMPTY_COLOR.r, EMPTY_COLOR.g, EMPTY_COLOR.b, EMPTY_ALPHA);
        }
        countLabel.setText(String.valueOf(requiredCount));
        return decreased;
    }

    private void sizeUpItem() {

        float newWidth, newHeight;

        if (itemImage.getWidth() > itemImage.getHeight()) {
            newWidth = ITEM_DEFAULT_SIZE;
            newHeight = ScaleUtil.getHeight(itemImage.getWidth(), itemImage.getHeight(), newWidth);
        } else {
            newHeight = ITEM_DEFAULT_SIZE;
            newWidth = ScaleUtil.getWidth(itemImage.getWidth(), itemImage.getHeight(), newHeight);
        }

        itemImage.setSize(newWidth, newHeight);
    }

    @Override
    public float getWidth() {
        return itemImage.getWidth() + DEFAULT_SPACING + countLabel.getPrefWidth();
    }

    public boolean isDone(){
        return requiredCount == 0;
    }

    @Override
    public float getHeight() {
        return itemImage.getHeight();
    }

    public GameObjectDefinition getItemDefinition() {
        return itemDefinition;
    }

    public int getCount() {
        return requiredCount;
    }
}
