/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.common.ui.health_bar.HealthBar;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.attention.actors.AbstractItem;
import cz.nic.tablexia.game.games.attention.model.AttentionGameDifficultyDefinition;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.scene.AbstractAttentionScreen;
import cz.nic.tablexia.game.games.attention.scene.BottomBar;
import cz.nic.tablexia.game.games.attention.scene.JailAnimation;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.dialog.components.AnimatedImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoColumnContentDialogComponent;

enum PreloaderAnimationDefinition {
    EASY(7, 7, GameDifficulty.EASY),
    MEDIUM(9, 9, GameDifficulty.MEDIUM),
    HARD(10, 8, GameDifficulty.HARD),
    BONUS(5, 0, GameDifficulty.BONUS);

    private int fistAnimationFramesCount;
    private int secondAnimationFramesCount;
    private GameDifficulty gameDifficulty;

    PreloaderAnimationDefinition(
            int fistAnimationFramesCount, int secondAnimationFramesCount, GameDifficulty gameDifficulty) {
        this.fistAnimationFramesCount = fistAnimationFramesCount;
        this.secondAnimationFramesCount = secondAnimationFramesCount;
        this.gameDifficulty = gameDifficulty;
    }

    public static PreloaderAnimationDefinition getPreloeaderAnimationForDifficulty(
            GameDifficulty gameDifficulty) {
        for (int i = 0; i < PreloaderAnimationDefinition.values().length; i++) {
            if (PreloaderAnimationDefinition.values()[i].getGameDifficulty().equals(gameDifficulty))
                return PreloaderAnimationDefinition.values()[i];
        }
        return null;
    }

    public int getFistAnimationFramesCount() {
        return fistAnimationFramesCount;
    }

    public int getSecondAnimationFramesCount() {
        return secondAnimationFramesCount;
    }

    public GameDifficulty getGameDifficulty() {
        return gameDifficulty;
    }
}

public class AttentionGame extends AbstractTablexiaGame<AttentionGameState> {

    public static final int JAIL_ANIMATION_WIDTH = 210;
    public static final int JAIL_ANIMATION_HEIGHT = 350;
    private static final float JUMPING_ANIMATION_FRAME_DURATION = 0.3f;
    private static final float BOTTOM_BAR_OFFSET_Y = 5f;
    private static final float HEALTH_BAR_PADDING_Y = 15f;
    private static final String SCORE_TOTAL = "score_total";
    private static final int HEALTH_COUNT = 3;
    private static final float PRELOADER_ANIM_FRAME_DURATION = 0.3f;
    private static final float PRELOADER_TEXT_PADDING = 10f;
    private static final float PRELOADER_IMAGE_COLUMN_WIDTH_RATIO = 3f / 8;
    private static final float PRELOADER_TEXT_COLUMN_WIDTH_RATIO = 5f / 8;
    private static final float PRELOADER_ROW_HEIGHT = 1f / 3;
    private static final String PRELOADER_TEXT_KEY_1 = ApplicationTextManager.ApplicationTextsAssets.ATTENTION_GAME_PRELOADER_1;
    private static final String PRELOADER_TEXT_KEY_1_BONUS = ApplicationTextManager.ApplicationTextsAssets.ATTENTION_GAME_PRELOADER_1_BONUS;
    private static final Scaling PRELOADER_IMAGE_SCALING = Scaling.fit;
    private static final String PRELOADER_ANIM_IMAGE = "preloader_animation";
    private static final String PRELOADER_HOP = "hop/";
    private static final String PRELOADER_ELEVATE = "elevate/";
    private static final String PRELOADER_SWIPE = "swipe/";
    private static final int PRELOADER_TEXT_ALIGN = Align.left;
    private static final Color PRELOADER_TEXT_COLOR = Color.DARK_GRAY;
    public static final float BOTTOM_BOARD_HEIGHT = 0.18f;
    private HealthBar healthBar;
    private boolean sceneEnabled = true;
    private boolean gameStarted;
    private BottomBar bottomBar;
    private Image board;
    private AttentionGameDifficultyDefinition difficultyDefinition;
    private boolean finished;
    private AbstractAttentionScreen actualScreen;
    private InputMultiplexer inputMultiplexer;

    public AttentionGame() {
        inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(getStage());
    }

    @Override
    protected AttentionGameState prepareGameData(Map<String, String> gameState) {
        return new AttentionGameState(
                AttentionGameDifficultyDefinition.getDefinitionByGameDifficulty(getGameDifficulty()));
    }

    @Override
    protected void gameLoaded(Map<String, String> gameState) {
        difficultyDefinition =
                AttentionGameDifficultyDefinition.getDefinitionByGameDifficulty(getGameDifficulty());
        gameStarted = false;
        finished = false;
        processNewScreen(initScreen());
        addBottomBar();
        healthBar =
                new HealthBar(
                        getGameGlobalTextureRegion(AbstractTablexiaGame.HEART_FULL),
                        getGameGlobalTextureRegion(AbstractTablexiaGame.HEART_BROKEN),
                        getScreenTextureRegion(AttentionGameAssets.HEALTHS_BACKGROUND),
                        HEALTH_COUNT);
        healthBar.setPosition(
                getSceneWidth() - healthBar.getWidth(),
                getViewportTopY() - healthBar.getHeight() - HEALTH_BAR_PADDING_Y);
        getStage().addActor(healthBar);
        actualScreen.show();
        setGameScore(SCORE_TOTAL, getData().getHealth());
        super.gameLoaded(gameState);
    }

    private AbstractAttentionScreen initScreen() {
        Class<? extends AbstractAttentionScreen> screenClass = difficultyDefinition.getScreenClass();
        Constructor<? extends AbstractAttentionScreen> constructor = null;
        Object screenInstance = null;
        try {
            constructor = screenClass.getConstructor(AttentionGame.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        try {
            screenInstance = constructor.newInstance(this);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (AbstractAttentionScreen) screenInstance;
    }

    @Override
    protected void gameRender(float delta) {
        if (actualScreen != null) {
            actualScreen.render(delta);
        }
    }

    @Override
    public InputProcessor getInputProcessor() {
        return inputMultiplexer;
    }

    private void addBottomBar() {
        TextureRegion textureRegion = getScreenTextureRegion(AttentionGameAssets.BOARD);
        board = new Image(textureRegion);
        board.setSize(
                getSceneWidth(),
                getViewportHeight() * BOTTOM_BOARD_HEIGHT);
        board.setPosition(0, getViewportBottomY());
        getStage().addActor(board);
        bottomBar = new BottomBar(this, getDifficultyDefinition().getPickupRules());
        bottomBar.setPosition(
                getSceneWidth() / 2 - bottomBar.getPrefWidth() / 2,
                board.getTop() - bottomBar.getPrefHeight() - BOTTOM_BAR_OFFSET_Y);
        getStage().addActor(bottomBar);
    }

    public AttentionGameDifficultyDefinition getDifficultyDefinition() {
        return difficultyDefinition;
    }

    public float getBoardTop(){
        return board.getTop();
    }

    @Override
    protected void gameAct(float delta) {
        if (gameStarted && !isScreenPaused() && sceneEnabled) {
            super.gameAct(delta);
        }
    }

    @Override
    protected void gamePaused(Map<String, String> gameState) {
        super.gamePaused(gameState);
        actualScreen.onGamePaused();
    }

    @Override
    protected void gameResumed() {
        super.gameResumed();
        actualScreen.onGameResumed();
    }

    @Override
    protected void screenResized(int width, int height) {
        if (actualScreen != null) {
            actualScreen.resize(width, height);
        }
    }

    public TextureRegion[] getPlatformTextureRegions() {
        TextureRegion[] regions =
                new TextureRegion[getDifficultyDefinition().getPlatformTextures().length];
        for (int i = 0; i < regions.length; i++) {
            regions[i] =
                    getTextureRegionForAtlas(
                            getGameDifficultyAtlasPath(), getDifficultyDefinition().getPlatformTextures()[i]);
        }

        return regions;
    }

    public String getAtlasPathForDifficulty(GameDifficulty gameDifficulty) {
        return getGameDifficultyAtlasPath(gameDifficulty);
    }

    @Override
    public void startNewGame() {
        super.startNewGame();
    }

    @Override
    protected void gameDisposed() {
        processLastScreen(actualScreen);
        super.gameDisposed();
    }

    private void processNewScreen(AbstractAttentionScreen newScreen) {
        AbstractAttentionScreen lastScreen = actualScreen;
        actualScreen = newScreen;


        inputMultiplexer.addProcessor(newScreen.getInputProcessor());
        newScreen.show();
        if (lastScreen != null) {
            processLastScreen(lastScreen);
        }
    }

    private void processLastScreen(AbstractTablexiaScreen<Void> lastScreen) {
        inputMultiplexer.removeProcessor(lastScreen.getInputProcessor());
        lastScreen.hide();
        lastScreen.dispose();
    }

    public void finishGameWithNoHealth() {
        getData().removeHealth();
        healthBar.removeAllHealth();
        setGameScore(SCORE_TOTAL, getData().getHealth());
        finishGame();
    }

    public void finishGame() {
        if (!finished) {
            finished = true;
            sceneEnabled = false;
            actualScreen.stopAnimation();
            actualScreen.onGamePaused();
            gameComplete();
        }
    }

    @Override
    public void gameComplete() {
        endGame();
        showGameResultDialog();
    }

    private void startEndingAnimation() {
        // TODO: 6/23/20 add
        if (!finished) {
            finished = true;
            sceneEnabled = false;
            actualScreen.stopAnimation();
            actualScreen.onGamePaused();
            Music jailMusic = getMusic("common/" + AttentionGameAssets.JAIL_SOUND);
            JailAnimation jailAnimation = new JailAnimation(getScreenAnimation(AttentionGameAssets.JAIL_ANIMATION, AttentionGameAssets.JAIL_FRAMES_COUNT, AttentionGameAssets.JAIL_FRAME_TIME), this);
            jailAnimation.setSize(JAIL_ANIMATION_WIDTH, JAIL_ANIMATION_HEIGHT);
            jailAnimation.setPosition(getSceneWidth() / 2f - jailAnimation.getWidth() / 2f, getViewportHeight() / 2f - jailAnimation.getHeight() / 2f);
            getStage().addActor(jailAnimation);
            jailMusic.play();
        }
    }

    public boolean isSceneEnabled() {
        return sceneEnabled;
    }

    public void setScreenEnabled(boolean screenEnabled) {
        this.sceneEnabled = screenEnabled;
    }

    private void startGame() {
        gameStarted = true;
    }

    public boolean isGameStarted() {
        return gameStarted;
    }

    @Override
    protected void gameVisible() {
        super.gameVisible();
        startGame();
        actualScreen.onGameVisible();
    }

    public ArrayList<GameObjectDefinition> getDoneItems() {
        return bottomBar.getDoneItems();
    }

    public Animation getRunningAnimation() {
        return getAnimationForAtlas(
                getGameDifficultyAtlasPath(),
                getDifficultyDefinition().getDetectiveRunAnimation(),
                getDifficultyDefinition().getDetectiveRunAnimationFramesCount(),
                getDifficultyDefinition().getDetectiveAnimationSpeed());
    }

    public Animation getJumpingAnimation() {
        if (getDifficultyDefinition().getDetectiveJumpAnimation() != null) {
            return getAnimationForAtlas(
                    getGameDifficultyAtlasPath(),
                    getDifficultyDefinition().getDetectiveJumpAnimation(),
                    getDifficultyDefinition().getDetectiveJumpAnimationFramesCount(),
                    JUMPING_ANIMATION_FRAME_DURATION);
        }
        return null;
    }

    public void hit() {
        healthBar.hide();
        getData().addHit();
        setGameScore(SCORE_TOTAL, getData().getHealth());
        if (!healthBar.hasHealthsLeft() && !finished) {
            finishGame();
        }
    }

    public void pickup(AbstractItem itemDefinition) {
        if (bottomBar.removeItem(itemDefinition.getObjectDefinition())) {
            getData().addScore();
        } else {
            getData().addDoneItem(itemDefinition.getObjectDefinition());
        }
        if (!finished && bottomBar.isRequiredPickedUp()) {
            startEndingAnimation();
        }
    }

    @Override
    protected List<SummaryMessage> getSummaryMessageForGameResult(Game game) {
        return Arrays.asList(
                new SummaryMessage(
                        SummaryImage.STATS,
                        getFormattedText(
                                AttentionGameProperties.RESULT_TEXT_SUMMARY,
                                game.getGameScore(AttentionGameProperties.SCORE_TOTAL, "0"))));
    }

    @Override
    protected String getTextKeyForGameResult(GameResult gameResult) {
        return AttentionGameProperties.RESULT_TEXT[gameResult.getStarCount()];
    }

    @Override
    protected String getSoundNameForGameResult(GameResult gameResult) {
        return AttentionGameProperties.RESULT_SOUNDS[gameResult.getStarCount()];
    }

    private void preparePreloaderDialog(TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        AnimatedImage preloaderHopImage =
                new AnimatedImage(
                        preloaderAssetsManager.getAnimation(
                                getGameDifficulty().name().toLowerCase()
                                        + "/"
                                        + PRELOADER_HOP
                                        + PRELOADER_ANIM_IMAGE,
                                PreloaderAnimationDefinition.getPreloeaderAnimationForDifficulty(
                                        getGameDifficulty())
                                        .getFistAnimationFramesCount(),
                                PRELOADER_ANIM_FRAME_DURATION),
                        false);
        AnimatedImage preloaderSwipeImage =
                new AnimatedImage(
                        preloaderAssetsManager.getAnimation(
                                getGameDifficulty().name().toLowerCase()
                                        + "/"
                                        + PRELOADER_SWIPE
                                        + PRELOADER_ANIM_IMAGE,
                                PreloaderAnimationDefinition.getPreloeaderAnimationForDifficulty(
                                        getGameDifficulty())
                                        .getSecondAnimationFramesCount(),
                                PRELOADER_ANIM_FRAME_DURATION),
                        false);
        preloaderHopImage.startAnimationLoop();
        preloaderSwipeImage.startAnimationLoop();

        String preloaderText1 = ApplicationTextManager.getInstance().getText(PRELOADER_TEXT_KEY_1);

        components.add(
                new TwoColumnContentDialogComponent(
                        new AnimatedImageContentDialogComponent(preloaderHopImage, PRELOADER_IMAGE_SCALING),
                        new AnimatedImageContentDialogComponent(preloaderSwipeImage, PRELOADER_IMAGE_SCALING),
                        PRELOADER_IMAGE_COLUMN_WIDTH_RATIO,
                        PRELOADER_IMAGE_COLUMN_WIDTH_RATIO,
                        PRELOADER_ROW_HEIGHT));

        components.add(
                new TextContentDialogComponent(
                        preloaderText1, PRELOADER_TEXT_COLOR, PRELOADER_TEXT_ALIGN, PRELOADER_TEXT_PADDING));
    }

    private void prepareBonusPreloaderDialog(TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager, List<TablexiaDialogComponentAdapter> components) {
        AnimatedImage preloaderElevateImage =
                new AnimatedImage(
                        preloaderAssetsManager.getAnimation(
                                getGameDifficulty().name().toLowerCase()
                                        + "/"
                                        + PRELOADER_ANIM_IMAGE,
                                PreloaderAnimationDefinition.getPreloeaderAnimationForDifficulty(
                                        getGameDifficulty())
                                        .getFistAnimationFramesCount(),
                                PRELOADER_ANIM_FRAME_DURATION),
                        false);
        preloaderElevateImage.startAnimationLoop();

        String preloaderText1 = ApplicationTextManager.getInstance().getText(PRELOADER_TEXT_KEY_1_BONUS);

        components.add(
                new TwoColumnContentDialogComponent(
                        new AnimatedImageContentDialogComponent(preloaderElevateImage, PRELOADER_IMAGE_SCALING),
                        new TextContentDialogComponent(
                                preloaderText1, PRELOADER_TEXT_COLOR, PRELOADER_TEXT_ALIGN, PRELOADER_TEXT_PADDING),
                        PRELOADER_IMAGE_COLUMN_WIDTH_RATIO,
                        PRELOADER_TEXT_COLUMN_WIDTH_RATIO,
                        PRELOADER_ROW_HEIGHT));
    }

    @Override
    public void preparePreloaderContent(
            float width,
            float height,
            TablexiaApplication.PreloaderAssetsManager preloaderAssetsManager,
            List<TablexiaDialogComponentAdapter> components) {
        if (!getGameDifficulty().equals(GameDifficulty.BONUS)) {
            preparePreloaderDialog(preloaderAssetsManager, components);
        } else {
            prepareBonusPreloaderDialog(preloaderAssetsManager, components);
        }
    }

}
