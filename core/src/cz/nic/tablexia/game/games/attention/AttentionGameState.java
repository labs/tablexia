/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention;

import java.util.ArrayList;

import cz.nic.tablexia.game.games.attention.model.AttentionGameDifficultyDefinition;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;

public class AttentionGameState {

    private int score;
    private int hits;
    private int health;
    private AttentionGameDifficultyDefinition difficultyDefinition;
    private ArrayList<GameObjectDefinition> doneItems;

    public AttentionGameState(AttentionGameDifficultyDefinition difficultyDefinition) {
        this.score = 0;
        this.hits = 0;
        this.health = 3;
        this.difficultyDefinition = difficultyDefinition;

        doneItems = new ArrayList<>();
    }

    public void addScore(int score) {
        this.score += score;
    }

    public void addScore() {
        addScore(1);
    }

    public void addHit() {
        health--;
        hits++;
    }

    public int getHealth() {
        return health;
    }

    public void removeHealth(){
        health = 0;
    }

    public void addDoneItem(GameObjectDefinition objectDefinition){
        if(!doneItems.contains(objectDefinition))
            doneItems.add(objectDefinition);
    }

    public ArrayList<GameObjectDefinition> getDoneItems() {
        return doneItems;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public AttentionGameDifficultyDefinition getDifficultyDefinition() {
        return difficultyDefinition;
    }
}
