/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.model;

import cz.nic.tablexia.game.games.attention.scene.AbstractForeground;

public class PositionHelper extends AbstractPositionHelper {

    public static final float SAFE_ZONE = 50f;
    public static final float PLATFORM_LANDING_EDGE_CONDITION = -50f;
    protected static final int TOP_POSITION_THRESHOLD_X = 60;

    public PositionHelper(AbstractForeground foreground) {
        super(foreground);
    }

    @Override
    public boolean canCreateObstacle(float rightX) {
        if (foreground.getPlatformPiece() == null) return true;
        float platformLeftX = foreground.getPlatformPiece().getPieceBounds().x;
        float platformRightX = platformLeftX + foreground.getPlatformPiece().getPieceBounds().width;
        return platformRightX > 0 && platformRightX + SAFE_ZONE < rightX;
    }

    @Override
    public boolean willLandOnPlatform(float rightX) {
        if (foreground.getPlatformPiece() != null) {
            float platformLeftX = foreground.getPlatformPiece().getPieceBounds().x;
            return platformLeftX > PLATFORM_LANDING_EDGE_CONDITION;
        }

        return false;
    }
}
