/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

import cz.nic.tablexia.game.games.attention.model.ForegroundPieceTypeDefinition;

public class ForegroundPiece {
    private static final float FOREGROUND = 180f;
    private static final float PLATFORM = 240;

    private Rectangle pieceBounds;
    private TextureRegion pieceTextureRegion;
    private ForegroundPieceTypeDefinition typeDefinition;
    private float height, platformHeight;

    public ForegroundPiece(
            Rectangle pieceBounds,
            TextureRegion pieceTextureRegion,
            ForegroundPieceTypeDefinition typeDefinition,
            float height,
            float platformHeight) {
        this.pieceBounds = pieceBounds;
        this.pieceTextureRegion = pieceTextureRegion;
        this.typeDefinition = typeDefinition;
        this.height = height;
        this.platformHeight = platformHeight;
    }

    public Rectangle getPieceBounds() {
        return pieceBounds;
    }

    public void setPieceBounds(Rectangle pieceBounds) {
        this.pieceBounds = pieceBounds;
    }

    public TextureRegion getPieceTextureRegion() {
        return pieceTextureRegion;
    }

    public void setPieceTextureRegion(TextureRegion pieceTextureRegion) {
        this.pieceTextureRegion = pieceTextureRegion;
    }

    public ForegroundPieceTypeDefinition getTypeDefinition() {
        return typeDefinition;
    }

    public void setTypeDefinition(ForegroundPieceTypeDefinition typeDefinition) {
        this.typeDefinition = typeDefinition;
    }

    public float getHeight() {
        return typeDefinition.equals(ForegroundPieceTypeDefinition.FOREGROUND)
                ? height
                : platformHeight;
    }

    public float getYPos() {
        //        return typeDefinition.equals(ForegroundPieceTypeDefinition.FOREGROUND) ? pieceBounds.y
        // : pieceBounds.y - 40f;
        return typeDefinition.equals(ForegroundPieceTypeDefinition.FOREGROUND)
                ? pieceBounds.y
                : pieceBounds.y;
    }

    public void swapWithPiece(ForegroundPiece foregroundPiece) {
        setPieceTextureRegion(foregroundPiece.getPieceTextureRegion());
        setTypeDefinition(foregroundPiece.getTypeDefinition());
    }

    public void updateX(float delta) {
        pieceBounds.x += delta;
    }
}
