/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention;

public class AttentionGameAssets {

    private static final String GFX_PATH = "gfx/";
    private static final String ANIMATION_PATH = GFX_PATH + "animation/";
    private static final String BUTTONS_PATH = GFX_PATH + "buttons/";
    private static final String SCENE_PATH = GFX_PATH + "scene/";
    private static final String PICKUPS_PATH = GFX_PATH + "pickups/";
    private static final String OBSTACLES_PATH = GFX_PATH + "obstacles/";

    public static final String RUNNING_ANIMATION = ANIMATION_PATH + "player_running";
    public static final String BICYCLE_ANIMATION = ANIMATION_PATH + "bicycle";
    public static final String BICYCLE_JUMP_ANIMATION = ANIMATION_PATH + "jump";
    public static final String BIKE_ANIMATION = ANIMATION_PATH + "bike";
    public static final String BIKE_JUMP_ANIMATION = ANIMATION_PATH + "bike_jump";
    public static final String ROBBER_ANIMATION = ANIMATION_PATH + "robber_running";
    public static final String GOOSE_ANIMATION = ANIMATION_PATH + "goose";
    public static final String FLYING = ANIMATION_PATH + "flying";
    public static final String JAIL_ANIMATION = ANIMATION_PATH + "jail";


    public static final int RUNNING_ANIMATION_FRAMES_COUNT = 10;
    public static final int BIKE_ANIMATION_FRAMES_COUNT = 2;
    public static final int BIKE_JUMP_ANIMATION_FRAMES_COUNT = 1;
    public static final int BICYCLE_ANIMATION_FRAMES_COUNT = 8;
    public static final int BICYCLE_JUMP_ANIMATION_FRAMES_COUNT = 3;
    public static final int GOOSE_FRAMES_COUNT = 8;
    public static final int JAIL_FRAMES_COUNT = 7;

    public static final float JAIL_FRAME_TIME = 0.17f;

    public static final String BOARD = GFX_PATH + "board";
    public static final String HEALTHS_BACKGROUND = GFX_PATH + "hearths_bg";

    public static final String BANANA = OBSTACLES_PATH + "banana";
    public static final String BOMB = OBSTACLES_PATH + "bomb";
    public static final String TRAP = OBSTACLES_PATH + "trap";
    public static final String BALLOON = OBSTACLES_PATH + "baloon";
    public static final String BALLOONS = OBSTACLES_PATH + "baloons";
    public static final String CHIMNEY = OBSTACLES_PATH + "chimney";
    public static final String CLOUD = OBSTACLES_PATH + "cloud";
    public static final String NEST = OBSTACLES_PATH + "nest";
    public static final String WIND_INDICATOR = OBSTACLES_PATH + "wind_indicator";
    public static final String HOLE = OBSTACLES_PATH + "hole";
    public static final String RAMP = OBSTACLES_PATH + "ramp";
    public static final String BAG = PICKUPS_PATH + "bag";

    public static final String DOCUMENTS = PICKUPS_PATH + "documents";
    public static final String FINGER = PICKUPS_PATH + "finger";
    public static final String FOOTPRINT = PICKUPS_PATH + "footprint";
    public static final String KEY = PICKUPS_PATH + "key";
    public static final String MAGNIFIER = PICKUPS_PATH + "magnifier";
    public static final String MUGSHOT = PICKUPS_PATH + "mugshot";

    public static final String BACKGROUND = SCENE_PATH + "background";
    public static final String BONUS_SKY = SCENE_PATH + "background_sky";
    public static final String FOREGROUND = SCENE_PATH + "foreground";
    public static final String SKY = SCENE_PATH + "sky";
    public static final String PLATFORM_EASY_1 = SCENE_PATH + "platform";
    public static final String PLATFORM_EASY_2 = SCENE_PATH + "platform_canal";
    public static final String PLATFORM_MEDIUM_1 = SCENE_PATH + "platform";
    public static final String PLATFORM_MEDIUM_2 = SCENE_PATH + "platform_canal";
    public static final String PLATFORM_HARD = SCENE_PATH + "platform";

    public static final String BUTTON_UP = BUTTONS_PATH + "up";
    public static final String BUTTON_UP_PRESSED = BUTTONS_PATH + "up_press";
    public static final String BUTTON_UP_RIGHT = BUTTONS_PATH + "sloping_up_right";
    public static final String BUTTON_UP_RIGHT_PRESSED = BUTTONS_PATH + "sloping_up_right_press";

    public static final String TOOLTIP_TEXT = "tooltip_text";

    ///// sounds

    private static final String SOUND_PATH = "sfx/";
    private static final String SOUND_EXTENSION = ".mp3";
    public static final String BAG_SOUND = SOUND_PATH + "bag" + SOUND_EXTENSION;
    public static final String BANANA_SOUND = SOUND_PATH + "banana" + SOUND_EXTENSION;
    public static final String BOMB_SOUND = SOUND_PATH + "bomb" + SOUND_EXTENSION;
    public static final String DETECTIVE_JUMP_SOUND = SOUND_PATH + "detective_jump" + SOUND_EXTENSION;
    public static final String DETECTIVE_MOVING_SOUND = SOUND_PATH + "detective_moving" + SOUND_EXTENSION;
    public static final String DETECTIVE_STEPS_SOUND = SOUND_PATH + "detective_steps" + SOUND_EXTENSION;
    public static final String DOCUMENTS_SOUND = SOUND_PATH + "documents" + SOUND_EXTENSION;
    public static final String MUGSHOT_SOUND = SOUND_PATH + "mugshot" + SOUND_EXTENSION;
    public static final String FINGER_SOUND = SOUND_PATH + "finger" + SOUND_EXTENSION;
    public static final String KEY_SOUND = SOUND_PATH + "key" + SOUND_EXTENSION;
    public static final String MAGNIFIER_SOUND = SOUND_PATH + "magnifier" + SOUND_EXTENSION;
    public static final String ROBBER_RUNNING_SOUND = SOUND_PATH + "robber_running" + SOUND_EXTENSION;
    public static final String TRAP_SOUND = SOUND_PATH + "trap" + SOUND_EXTENSION;
    public static final String FOOTPRINT_SOUND = SOUND_PATH + "footprint" + SOUND_EXTENSION;
    public static final String HOLE_SOUND = SOUND_PATH + "hole" + SOUND_EXTENSION;
    public static final String MOTORCYCLE_JUMP_SOUND = SOUND_PATH + "motorcycle_jump" + SOUND_EXTENSION;
    public static final String MOTORCYCLE_MOVING_SOUND = SOUND_PATH + "motorcycle_moving" + SOUND_EXTENSION;
    public static final String RAMP_SOUND = SOUND_PATH + "ramp" + SOUND_EXTENSION;

    public static final String BALLOON_BAG_SOUND = SOUND_PATH + "bag" + SOUND_EXTENSION;
    public static final String BALLOON_DOCUMENTS_SOUND = SOUND_PATH + "documents" + SOUND_EXTENSION;
    public static final String BALLOON_KEY_SOUND = SOUND_PATH + "key" + SOUND_EXTENSION;
    public static final String FLYING_SOUND = SOUND_PATH + "idle" + SOUND_EXTENSION;
    public static final String CHIMNEY_SOUND = SOUND_PATH + "chimney" + SOUND_EXTENSION;
    public static final String ELEVATING_SOUND = SOUND_PATH + "elevating" + SOUND_EXTENSION;
    public static final String NEST_SOUND = SOUND_PATH + "nest" + SOUND_EXTENSION;
    public static final String FAIL_SOUND = SOUND_PATH + "fail" + SOUND_EXTENSION;
    public static final String GOOSE_SOUND = SOUND_PATH + "geese" + SOUND_EXTENSION;
    public static final String JAIL_SOUND = SOUND_PATH + "jail" + SOUND_EXTENSION;


    public static String[] getAnimationFrames() {
        String[] frames = new String[RUNNING_ANIMATION_FRAMES_COUNT];
        for (int i = 0; i < frames.length; i++) {
            frames[i] = GFX_PATH + RUNNING_ANIMATION_FRAMES_COUNT + i;
        }
        return frames;
    }
}
