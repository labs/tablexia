/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import java.util.Map;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.AttentionGameAssets;
import cz.nic.tablexia.game.games.attention.AttentionGameProperties;
import cz.nic.tablexia.game.games.attention.actors.AnimatedObstacle;
import cz.nic.tablexia.game.games.attention.actors.Robber;
import cz.nic.tablexia.game.games.attention.actors.bonus.Balloon;
import cz.nic.tablexia.game.games.attention.actors.bonus.BonusDetective;
import cz.nic.tablexia.game.games.attention.actors.bonus.BonusObstacle;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.model.PositionDefinition;
import cz.nic.tablexia.game.games.attention.model.PositionHelper;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.util.ui.TablexiaLabel;

public class BonusScreen extends AbstractAttentionScreen {

    private static final float BONUS_DETECTIVE_OFFSET_Y = 100f;
    private static final float GOOSE_CHANCE = 0.3f;
    private static final float TOOLTIP_ALPHA = 0.4f;
    private static final float ITEM_START_POSITION_OFFSET_X = 10f;
    private static final float BONUS_DETECTIVE_FRAME_DURATION = 0.05f;
    private static final float BONUS_DETECTIVE_HEIGHT = 80f;
    private static final float BONUS_DETECTIVE_WIDTH = 160f;
    private static final float BONUS_DETECTIVE_OFFSET_X = 30f;
    private static final float GOOSE_FRAME_DURATION = 0.05f;
    private static final float ITEM_CHANCE = 0.7f;
    private static final float BONUS_BOTTOM_OBSTACLE_CHANCE = 0.4f;
    private static final float BONUS_ITEM_TOP_POSITION_CHANCE = 0.4f;
    private static final float BONUS_TOP_POSITION_OFFSET_Y = 0f;
    private static final float BONUS_ITEM_BOTTOM_POSITION_CHANCE = 0.3f;
    private static final float BONUS_BOTTOM_POSITION_OFFSET_Y = 0f;
    private static final float BONUS_TOOLTIP_FADE_OUT_DURATION = 0.3f;
    private static final float ITEM_UPDATE_TIMEOUT = 1.3f;
    private static final int BONUS_DETECTIVE_FRAMES_COUNT = 2;

    private BonusDetective detective;
    private Robber robber;
    private float timeSinceLastObstacle = 0f;
    private float timeSinceLastObject = 0f;
    private GameObjectDefinition lastBottomObstacleDefinition;
    private TablexiaLabel tooltipLabel;

    public BonusScreen(AttentionGame attentionGame) {
        super(attentionGame);
    }

    @Override
    protected void screenLoaded(Map screenState) {
        addBackground();
        positionHelper = new PositionHelper(foreground);
        getStage().addActor(itemsGroup);
        getStage().addActor(detectiveGroup);
        addDetective();
        addRobber();
        addTooltip();
        loadDifficultySounds();

        getStage().addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!attentionGame.isSceneEnabled()) {
                    attentionGame.setScreenEnabled(true);
                    if (tooltipLabel != null)
                        tooltipLabel.addAction(Actions.fadeOut(BONUS_TOOLTIP_FADE_OUT_DURATION));
                    if (robber != null) {
                        robber.playMusic();
                        robber.addAction(Actions.sequence(Actions.moveBy(100f, 0f, 1f), Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                robber.fadeOutMusic();
                            }
                        }), Actions.removeActor()));
                    }
                    detective.playElevatingSound();
                }
                detective.elevate();
                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }

    private void addBackground() {
        TextureRegion backgroundTextureRegion =
                getTextureRegionForAtlas(
                        attentionGame.getAtlasPathForDifficulty(difficultyDefinition.getGameDifficulty()),
                        AttentionGameAssets.BONUS_SKY);
        getStage()
                .addActor(
                        background =
                                new MovingImage(
                                        attentionGame,
                                        backgroundTextureRegion,
                                        getSceneWidth(),
                                        getViewportHeight(),
                                        getViewportBottomY(),
                                        true,
                                        difficultyDefinition.getBackgroundSpeed()));
    }

    @Override
    protected void screenAct(float delta) {
        if (!attentionGame.isScreenPaused() && attentionGame.isGameStarted() && !attentionGame.isGameFinished() && attentionGame.isSceneEnabled()) {
            timeSinceLastObstacle += delta;
            timeSinceLastObject += delta;
            updateScene();
            super.screenAct(delta);
        }
    }

    private void updateScene() {
        if (timeSinceLastObject > ITEM_UPDATE_TIMEOUT) {
            if (timeSinceLastObstacle >= difficultyDefinition.getObstacleSpawnRate()) {
                addItem(null, null, 0f);
                timeSinceLastObstacle = 0f;
                timeSinceLastObject = 0f;
            }
        }
    }

    private void addDetective() {
        detective =
                new BonusDetective(
                        attentionGame.getAnimationForAtlas(
                                attentionGame.getAtlasPathForDifficulty(difficultyDefinition.getGameDifficulty()),
                                AttentionGameAssets.FLYING,
                                BONUS_DETECTIVE_FRAMES_COUNT,
                                BONUS_DETECTIVE_FRAME_DURATION),
                        attentionGame,
                        BONUS_DETECTIVE_OFFSET_X,
                        getViewportHeight() / 2f - BONUS_DETECTIVE_OFFSET_Y,
                        BONUS_DETECTIVE_WIDTH,
                        BONUS_DETECTIVE_HEIGHT);
        detectiveGroup.addActor(detective);
    }

    private void addRobber() {
        detectiveGroup
                .addActor(
                        robber =
                                new Robber(
                                        attentionGame.getScreenAnimation(
                                                AttentionGameAssets.ROBBER_ANIMATION,
                                                ROBBER_FRAMES_COUNT,
                                                ROBBER_FRAME_DURATION),
                                        attentionGame));
        robber.setSize(ROBBER_WIDTH * 0.5f, ROBBER_HEIGHT * 0.5f);
        robber.setPosition(
                getSceneWidth() - robber.getWidth() - ROBBER_OFFSET_X,
                attentionGame.getBoardTop());
    }

    // TODO: 9/7/20 hide on game start

    @Override
    public void onGameVisible() {
        detective.startMovingSound();
    }


    @Override
    public void onGamePaused() {
        detective.stopMovingSound();
        if (robber != null) {
            robber.pauseMusic();
        }
    }

    @Override
    protected void addItem(PositionDefinition positionDefinition, GameObjectDefinition objectDefinition, float offsetX) {
        AnimatedObstacle goose = null;
        BonusObstacle bottomObstacle = null;

        if (attentionGame.getRandom().nextFloat() <= BONUS_BOTTOM_OBSTACLE_CHANCE) {
            GameObjectDefinition obstacleDefinition =
                    GameObjectDefinition.getRandomObstacle(difficultyDefinition, attentionGame.getRandom());
            if (lastBottomObstacleDefinition != null && obstacleDefinition.equals(lastBottomObstacleDefinition)) {
                obstacleDefinition =
                        difficultyDefinition
                                .getObstacles()[
                                (obstacleDefinition.ordinal() + 1) % difficultyDefinition.getObstacles().length];
            }
            PositionDefinition itemPositionDefinition = PositionDefinition.BOTTOM;
            TextureRegion textureRegion =
                    getTextureRegionForAtlas(
                            attentionGame.getAtlasPathForDifficulty(difficultyDefinition.getGameDifficulty()),
                            obstacleDefinition.getTextureName());
            bottomObstacle =
                    new BonusObstacle(
                            attentionGame,
                            obstacleDefinition,
                            textureRegion,
                            levelMusic.get(obstacleDefinition),
                            getSceneRightX() + ITEM_START_POSITION_OFFSET_X,
                            attentionGame.getBoardTop()
                                    + obstacleDefinition.getOffsetY() - Math.min(30, getViewportHeight() * 0.05f),
                            this,
                            this,
                            getSceneWidth(),
                            difficultyDefinition.getForegroundSpeed());
            getStage().addActor(bottomObstacle);
            lastBottomObstacleDefinition = bottomObstacle.getObjectDefinition();
        }

        if (attentionGame.getRandom().nextFloat() <= GOOSE_CHANCE) {
            goose =
                    new AnimatedObstacle(
                            attentionGame,
                            GameObjectDefinition.GOOSE,
                            attentionGame.getAnimationForAtlas(
                                    attentionGame.getAtlasPathForDifficulty(difficultyDefinition.getGameDifficulty()),
                                    AttentionGameAssets.GOOSE_ANIMATION,
                                    AttentionGameAssets.GOOSE_FRAMES_COUNT,
                                    GOOSE_FRAME_DURATION),
                            levelMusic.get(GameObjectDefinition.GOOSE),
                            getSceneRightX() + ITEM_START_POSITION_OFFSET_X,
                            getViewportHeight() * AttentionGameProperties.POSITION_TOP_BONUS + GameObjectDefinition.GOOSE.getOffsetY() - Math.min(30, getViewportHeight() * 0.05f),
                            this,
                            this,
                            getSceneWidth(),
                            difficultyDefinition.getForegroundSpeed());
            itemsGroup.addActor(goose);
        }

        if ((goose == null || bottomObstacle == null)
                && attentionGame.getRandom().nextFloat() < ITEM_CHANCE) {
            GameObjectDefinition itemDefinition = getNextItemDefinition();
            float itemPositionY = getViewportHeight() * AttentionGameProperties.POSITION_MIDDLE_BONUS;
            if (goose == null && attentionGame.getRandom().nextFloat() < BONUS_ITEM_TOP_POSITION_CHANCE) {
                itemPositionY = getViewportHeight() * AttentionGameProperties.POSITION_TOP_BONUS;
            } else if (bottomObstacle == null
                    && attentionGame.getRandom().nextFloat() < BONUS_ITEM_BOTTOM_POSITION_CHANCE) {
                itemPositionY = getViewportHeight() * AttentionGameProperties.POSITION_BOTTOM_BONUS;
            }

            Balloon balloon =
                    new Balloon(
                            attentionGame,
                            itemDefinition,
                            getScreenTextureRegion(itemDefinition.getTextureName()),
                            levelMusic.get(itemDefinition),
                            getSceneRightX() + ITEM_START_POSITION_OFFSET_X,
                            itemPositionY + GameObjectDefinition.BALLOON.getOffsetY() - Math.min(30, getViewportHeight() * 0.05f),
                            this,
                            this,
                            getSceneWidth(),
                            difficultyDefinition.getForegroundSpeed());
            itemsGroup.addActor(balloon);
        }

    }

    private void addTooltip() {
        tooltipLabel =
                new TablexiaLabel(
                        getText(AttentionGameAssets.TOOLTIP_TEXT),
                        new TablexiaLabel.TablexiaLabelStyle(
                                ApplicationFontManager.FontType.REGULAR_26, new Color(1f, 1f, 1f, TOOLTIP_ALPHA)));
        tooltipLabel.setPosition(
                getViewportWidth() / 2f - tooltipLabel.getWidth() / 2f,
                getViewportHeight() / 2f - tooltipLabel.getHeight() / 2f);
        getStage().addActor(tooltipLabel);
    }

    @Override
    public boolean doesCollideWithDetectiveFavored(float x, float y, float width, float height) {
        return !(detective.getX() > x + width - FAVORED_OFFSET_X)
                && !(x > detective.getRight() - FAVORED_OFFSET_X)
                && !(detective.getTop() < y + FAVORED_OFFSET_Y)
                && !(y + height < detective.getY() + FAVORED_OFFSET_Y);
    }

    @Override
    public boolean doesCollideWithDetectiveStrict(float x, float y, float width, float height) {
        return !(detective.getX() > x + width)
                && !(x > detective.getRight())
                && !(detective.getTop() < y)
                && !(y + height < detective.getY());
    }

    @Override
    public void onPlatformCreated() {

    }

    @Override
    public void loadDifficultySounds() {
        super.loadDifficultySounds();
        Music gooseMusic = attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + GameObjectDefinition.GOOSE.getCollideSoundName());
        levelMusic.put(GameObjectDefinition.GOOSE, gooseMusic);
        addDisposable(gooseMusic);
    }
}
