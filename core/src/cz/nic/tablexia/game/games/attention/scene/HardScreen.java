/*
 * Copyright (C) 2020 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.games.attention.scene;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import java.util.Map;

import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.attention.AttentionGameAssets;
import cz.nic.tablexia.game.games.attention.actors.AbstractItem;
import cz.nic.tablexia.game.games.attention.actors.AbstractObstacle;
import cz.nic.tablexia.game.games.attention.actors.Item;
import cz.nic.tablexia.game.games.attention.actors.Obstacle;
import cz.nic.tablexia.game.games.attention.actors.Ramp;
import cz.nic.tablexia.game.games.attention.actors.Robber;
import cz.nic.tablexia.game.games.attention.model.GameObjectDefinition;
import cz.nic.tablexia.game.games.attention.model.PositionDefinition;

public class HardScreen extends AbstractAttentionScreen {

    private static final float PLATFORM_LIMIT_LEFT = 7f;
    private static final float ITEM_START_POSITION_OFFSET_X = 10f;
    private static final float PLATFORM_LIMIT_RIGHT = 12f;
    private static final float RAMP_TIMEOUT = 5f;
    private static final float RAMP_SPAWN_RATE = 6f;
    private static final float PIT_OFFSET_X = 10f;
    private static final float ITEM_UPDATE_TIMEOUT = 1.3f;
    private static final float FOREGROUND_OFFSET_Y_HARD = -20f;

    private PositionDefinition lastItemPosition;
    private float timeSinceLastObstacle = 0f;
    private float timeSinceLastObject = 0f;
    private float timeSincePlatform = 0f;
    private float timeSinceRamp = 0f;
    private Robber robber;

    public HardScreen(AttentionGame attentionGame) {
        super(attentionGame);
    }

    @Override
    protected void screenLoaded(Map screenState) {
        super.screenLoaded(screenState);
        showRobber();
    }

    @Override
    public float getForegroundOffset() {
        return FOREGROUND_OFFSET_Y_HARD;
    }

    @Override
    protected void screenAct(float delta) {
        if (!attentionGame.isScreenPaused() && attentionGame.isGameStarted() && !attentionGame.isGameFinished()) {
            timeSinceLastObstacle += delta;
            timeSinceLastObject += delta;
            timeSincePlatform += delta;
            timeSinceRamp += delta;
            updateScene();
            super.screenAct(delta);
        }
    }

    @Override
    public void onGameVisible() {
        if (robber != null) {
            robber.playMusic();
            robber.addAction(Actions.sequence(Actions.delay(2f), Actions.moveBy(100f, 0f, 1f), Actions.run(new Runnable() {
                @Override
                public void run() {
                    robber.fadeOutMusic();
                }
            }), Actions.removeActor()));
        }
        detective.playRunningSound();

    }

    @Override
    public void onGamePaused() {
        super.onGamePaused();
        if (robber != null) {
            robber.pauseMusic();
        }
    }

    @Override
    public void loadDifficultySounds() {
        super.loadDifficultySounds();
        Music holeMusic = attentionGame.getMusic(attentionGame.getGameDifficulty().name().toLowerCase() + "/" + GameObjectDefinition.HOLE.getCollideSoundName());
        levelMusic.put(GameObjectDefinition.HOLE, holeMusic);
        addDisposable(holeMusic);
    }

    private void showRobber() {
        detectiveGroup
                .addActor(
                        robber =
                                new Robber(
                                        attentionGame.getScreenAnimation(
                                                AttentionGameAssets.ROBBER_ANIMATION,
                                                ROBBER_FRAMES_COUNT,
                                                ROBBER_FRAME_DURATION),
                                        attentionGame));
        robber.setSize(ROBBER_WIDTH, ROBBER_HEIGHT);
        robber.setPosition(
                getSceneWidth() - robber.getWidth() - ROBBER_OFFSET_X,
                getUpdatedPosition(PositionDefinition.BOTTOM));
    }

    private void updateScene() {
        if (timeSinceLastObject > ITEM_UPDATE_TIMEOUT) {
            if (timeSinceRamp > RAMP_SPAWN_RATE
                    && timeSincePlatform >= 5f
                    && timeSincePlatform <= 15f) {
                addRamp();
                timeSinceLastObstacle = 0f;
                timeSinceLastObject = 0f;
                timeSinceRamp = 0f;
            } else {
                if (timeSincePlatform > difficultyDefinition.getPlatformSpawnRate()
                        && timeSinceRamp > RAMP_TIMEOUT) {
                    foreground.createPlatform();
                    timeSincePlatform = 0f;
                } else {
                    if (timeSincePlatform >= 4f && timeSincePlatform <= 20f) {
                        if (timeSinceLastObstacle >= difficultyDefinition.getObstacleSpawnRate()) {
                            addObstacle();
                            timeSinceLastObstacle = 0f;
                        } else {
                            addItem(null, null, 0f);
                        }
                    }
                }
                timeSinceLastObject = 0f;
            }
        }
    }

    @Override
    public void onPlatformCreated() {
        int randIndex = attentionGame.getRandom().nextInt(difficultyDefinition.getPickupRules().length);
        GameObjectDefinition firstDefinition = difficultyDefinition.getPickupRules()[randIndex].getItemDefinition();
        GameObjectDefinition secondDefinition = difficultyDefinition.getPickupRules()[(randIndex + 1) % difficultyDefinition.getPickupRules().length].getItemDefinition();
        addItem(PositionDefinition.TOP, firstDefinition.equals(GameObjectDefinition.FOOTPRINT) ?
                        difficultyDefinition.getPickupRules()[(randIndex + 2) % difficultyDefinition.getPickupRules().length].getItemDefinition()
                        : firstDefinition,
                220f);
        addItem(PositionDefinition.BOTTOM, secondDefinition, 220f);
    }

    @Override
    protected void addItem(PositionDefinition forcedPosition, GameObjectDefinition objectDefinition, float offsetX) {
        GameObjectDefinition itemDefinition = objectDefinition == null ? getNextItemDefinition() : objectDefinition;
        PositionDefinition positionDefinition = forcedPosition != null ? forcedPosition :
                getNextItemPosition(itemDefinition);

        AbstractItem item =
                new Item(
                        attentionGame,
                        itemDefinition,
                        getScreenTextureRegion(itemDefinition.getTextureName()),
                        levelMusic.get(itemDefinition),
                        getSceneRightX() + ITEM_START_POSITION_OFFSET_X + offsetX,
                        getUpdatedPosition(positionDefinition) + itemDefinition.getOffsetY(),
                        this,
                        this,
                        getSceneWidth(),
                        difficultyDefinition.getForegroundSpeed());
        itemsGroup.addActor(item);
    }

    private void addRamp() {
        Ramp ramp =
                new Ramp(
                        attentionGame,
                        GameObjectDefinition.RAMP,
                        getTextureRegionForAtlas(
                                attentionGame.getAtlasPathForDifficulty(difficultyDefinition.getGameDifficulty()),
                                AttentionGameAssets.RAMP),
                        getSceneRightX() + ITEM_START_POSITION_OFFSET_X,
                        getUpdatedPosition(PositionDefinition.BOTTOM)
                                + GameObjectDefinition.RAMP.getOffsetY(),
                        this,
                        this,
                        getSceneWidth(),
                        difficultyDefinition.getForegroundSpeed());
        Obstacle pit =
                new Obstacle(
                        attentionGame,
                        GameObjectDefinition.HOLE,
                        getTextureRegionForAtlas(
                                attentionGame.getAtlasPathForDifficulty(difficultyDefinition.getGameDifficulty()),
                                AttentionGameAssets.HOLE),
                        levelMusic.get(GameObjectDefinition.HOLE),
                        ramp.getRight() + PIT_OFFSET_X,
                        getUpdatedPosition(PositionDefinition.BOTTOM)
                                + GameObjectDefinition.RAMP.getOffsetY(),
                        this,
                        this,
                        getSceneWidth(),
                        difficultyDefinition.getForegroundSpeed());
        itemsGroup.addActor(ramp);
        itemsGroup.addActor(pit);
    }

    private PositionDefinition getNextItemPosition(GameObjectDefinition itemDefinition) {
        PositionDefinition positionDefinition;
        if (itemDefinition.equals(GameObjectDefinition.FOOTPRINT)) return PositionDefinition.BOTTOM;
        positionDefinition =
                PositionDefinition.getRandomAccessiblePosition(attentionGame.getRandom());
        lastItemPosition = positionDefinition;
        return positionDefinition;
    }


    private void addObstacle() {
        GameObjectDefinition obstacleDefinition =
                GameObjectDefinition.getRandomObstacle(difficultyDefinition, attentionGame.getRandom());
        PositionDefinition positionDefinition = PositionDefinition.BOTTOM;
        TextureRegion textureRegion =
                obstacleDefinition.isShared()
                        ? getScreenTextureRegion(obstacleDefinition.getTextureName())
                        : getTextureRegionForAtlas(
                        attentionGame.getAtlasPathForDifficulty(difficultyDefinition.getGameDifficulty()),
                        obstacleDefinition.getTextureName());
        AbstractObstacle obstacle =
                new Obstacle(
                        attentionGame,
                        obstacleDefinition,
                        textureRegion,
                        levelMusic.get(obstacleDefinition),
                        getSceneRightX() + ITEM_START_POSITION_OFFSET_X,
                        getUpdatedPosition(positionDefinition) + obstacleDefinition.getOffsetY(),
                        this,
                        this,
                        getSceneWidth(),
                        difficultyDefinition.getForegroundSpeed());
        itemsGroup.addActor(obstacle);
    }


}
