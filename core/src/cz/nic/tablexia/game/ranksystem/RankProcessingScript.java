/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.ranksystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;

/**
 * Created by drahomir on 8/11/16.
 */
public enum RankProcessingScript {
    BEFORE_3_3(Game.BACK_COMPATIBILITY_321_RANK_SYSTEM_VERSION_CODE),
    V3_3(
        2,
        new GameDefinition[] { //New games for V3.3
            GameDefinition.RUNES,
            GameDefinition.CRIME_SCENE
        }
    ),
    V3_5(3, new GameDefinition[]{
            GameDefinition.PROTOCOL,
            GameDefinition.SAFE,
            GameDefinition.ON_THE_TRAIL,
            GameDefinition.MEMORY_GAME,
            GameDefinition.ATTENTION_GAME
    });

    /**
     * Games that were part of the game before additionally added games
     */
    private static final GameDefinition[] ORIGINAL_GAMES = {
        GameDefinition.ROBBERY,
        GameDefinition.PURSUIT,
        GameDefinition.KIDNAPPING,
        GameDefinition.NIGHT_WATCH,
        GameDefinition.SHOOTING_RANGE,
        GameDefinition.IN_THE_DARKNESS
    };

    private final int scriptVersion;

    //Games that are already in the game. Need to fill progress to 100 percent for each of these to rank up.
    private GameDefinition[] includedGames;
    private GameDefinition[] newGames;

    RankProcessingScript(int scriptVersion) {
        this(scriptVersion, null);
    }

    RankProcessingScript(int scriptVersion, GameDefinition[] newGames) {
        this.scriptVersion = scriptVersion;
        this.newGames = newGames;
    }

    public static void initialize() {
        for(RankProcessingScript script : RankProcessingScript.values()) {
            script.prepareIncludedGames();
        }
    }

    public static RankProcessingScript getCurrentRankProcessingScript() {
        RankProcessingScript result = RankProcessingScript.BEFORE_3_3;

        for(RankProcessingScript script : values()) {
            if(script.getScriptVersion() > result.getScriptVersion()) {
                result = script;
            }
        }

        return result;
    }

    /**
     * Included games include Original games + new games of this script + new games of every older script.
     */
    private void prepareIncludedGames() {
        List<GameDefinition> includedGamesList = new ArrayList<GameDefinition>();
        Collections.addAll(includedGamesList, ORIGINAL_GAMES);
        if(hasNewGames()) Collections.addAll(includedGamesList, newGames);

        for(RankProcessingScript script : values()) {
            if(this.getScriptVersion() > script.getScriptVersion() && script.hasNewGames()) {
                Collections.addAll(includedGamesList, script.getNewGames());
            }
        }

        includedGames = includedGamesList.toArray(new GameDefinition[includedGamesList.size()]);
    }

    public interface RankGamesRetriever {
        List<Game> getGames(User user, RankProcessingScript rankProcessingScript);
    }

    /**
     * Basically iterates through all the games and calculates users rank using defined enum values above.
     * The result of this method is instance of RankComputeData class, which stores all the necessary stuff
     * from the calculations. Its recommended to cache returned instance of RankComputeData as this method
     * can be pretty heavy on resources and take a "long" time for many games...
     * @param user
     * @return
     */
    public static UserRankManager.RankComputeData calculateUsersRank(User user, RankGamesRetriever rankGamesRetriever) {
        UserRankManager.RankComputeData rankComputeData = new UserRankManager.RankComputeData();

        RankProcessingScript[] rankProcessingScripts = RankProcessingScript.values();

        Arrays.sort(rankProcessingScripts, new Comparator<RankProcessingScript>() {
            @Override
            public int compare(RankProcessingScript o1, RankProcessingScript o2) {
                return o1.getScriptVersion() - o2.getScriptVersion();
            }
        });

        for(RankProcessingScript script : rankProcessingScripts) {
            script.runRankProcessingScript(user, rankGamesRetriever, rankComputeData);
        }

        return rankComputeData;
    }

    public static void addGameToRankData(UserRankManager.RankComputeData rankComputeData, Game game) {
        RankProcessingScript script = RankProcessingScript.getCurrentRankProcessingScript();

        script.forEachGame(rankComputeData, game);
        script.afterRankCompute(rankComputeData);
    }

    public int getScriptVersion() {
        return scriptVersion;
    }

    public boolean hasNewGames() {
        return newGames != null && newGames.length > 0;
    }

    public GameDefinition[] getNewGames() {
        return newGames;
    }

    private static int getExperiencePoints(int cupsCount, GameDifficulty difficulty) {
        return cupsCount * difficulty.getExperiencePointsMultiplier();
    }

    private void runRankProcessingScript(User user, RankGamesRetriever gamesRetriever, UserRankManager.RankComputeData rankComputeData) {
        List<Game> games = prepareGames(user, gamesRetriever);

        beforeRankCompute(rankComputeData, games);

        for(Game game : games) {
            forEachGame(rankComputeData, game);
        }

        afterRankCompute(rankComputeData);
    }

    private List<Game> prepareGames(User user, RankGamesRetriever gamesRetriever) {
        return gamesRetriever.getGames(user, this);
    }

    private void beforeRankCompute(UserRankManager.RankComputeData rankComputeData, List<Game> games) {
        //Has user played any previous versions of the game ?
        if(this != getCurrentRankProcessingScript() && games.size() > 0) rankComputeData.setNewUser(false);

        UserRankManager.UserRank prevRank = UserRankManager.UserRank.getPreviousRank(rankComputeData.getCurrentRank());
        UserRankManager.UserRank nextRank = rankComputeData.getNextRank();

        if(hasNewGames()) {
            for(GameDefinition game : getNewGames()) {
                rankComputeData.getRankProgress(game.getGameNumber()).setMinXP(prevRank.getExperiencePointsNeeded());
                rankComputeData.getRankProgress(game.getGameNumber()).setCurrXP(prevRank.getExperiencePointsNeeded());
                rankComputeData.getRankProgress(game.getGameNumber()).setNextXP(nextRank.getExperiencePointsNeeded());

                rankComputeData.setNewGame(game, true);
            }
        }
    }

    private void forEachGame(UserRankManager.RankComputeData rankComputeData, Game game) {
        if(!game.isFinished()) return;
        if(game.getGameDifficulty() == GameDifficulty.TUTORIAL.getDifficultyNumber()) return;

        AbstractTablexiaGame.GameResult gameResult = GameDAO.getGameResult(game);
        int experiencePoints = getExperiencePoints(gameResult.getStarCount(), GameDifficulty.getByGame(game));
        int gameNumber = game.getGameNumber();

        //Increases games cups by starCount
        rankComputeData.getRankProgress(gameNumber).increaseCurrXP(experiencePoints);

        //Ignore experience points If user got more than is required for the next rank
        rankComputeData.getRankProgress(gameNumber).capRankProgress();

        //Checking if user ranked up after this game
        if(checkUserRankUp(rankComputeData))
            onRankUp(rankComputeData, game);
    }

    private boolean checkUserRankUp(UserRankManager.RankComputeData rankComputeData) {
        if(rankComputeData.getCurrentRank() == UserRankManager.getInstance().getHighestRank()) return false;
        for(GameDefinition gameDefinition : includedGames) {
            if(!rankComputeData.getRankProgress(gameDefinition.getGameNumber()).isComplete()) return false;
        }
        return true;
    }

    private void onRankUp(UserRankManager.RankComputeData rankComputeData, Game game) {
        rankComputeData.setCurrentRank(rankComputeData.getNextRank());
        rankComputeData.setNextRank(UserRankManager.UserRank.getNextRank(rankComputeData.getCurrentRank()));
        rankComputeData.setLastRankUp(game.getEndTime());

        for(UserRankManager.RankProgress rankProgresses : rankComputeData.getRankProgresses()) {
            rankProgresses.minXP = rankComputeData.getCurrentRank().getExperiencePointsNeeded();
            rankProgresses.nextXP = rankComputeData.getNextRank().getExperiencePointsNeeded();
        }

        rankComputeData.clearNewGames();
    }

    private void afterRankCompute(UserRankManager.RankComputeData rankComputeData) {
        //If user is at last rank
        if(rankComputeData.getCurrentRank() == rankComputeData.getNextRank()) {
            for(UserRankManager.RankProgress rankProgress : rankComputeData.getRankProgresses()) {
                rankProgress.setCurrXP(rankProgress.getNextXP());
            }
        }
    }
}