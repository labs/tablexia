/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.ranksystem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.trophy.GameTrophyDefinition;
import cz.nic.tablexia.game.trophy.UserTrophyDefinition;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.game.GameMenuItemGroup;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.screen.halloffame.assets.HallOfFameAssets;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.Log;

/**
 * Created by bcx on 4/15/16.
 */
public class UserRankManager {
    public enum UserRank {
        RANK_NONE ( 0, false, false, null,                                                       ApplicationAtlasManager.BADGE_NONE_ICON, HallOfFameAssets.BADGE_NONE,   0),
        RANK_I    ( 1, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_1,  ApplicationAtlasManager.BADGE_1_ICON,    HallOfFameAssets.BADGE_1,      3),
        RANK_II   ( 2, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_2,  ApplicationAtlasManager.BADGE_2_ICON,    HallOfFameAssets.BADGE_2,      9),
        RANK_III  ( 3, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_3,  ApplicationAtlasManager.BADGE_3_ICON,    HallOfFameAssets.BADGE_3,     18),
        RANK_IV   ( 4, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_4,  ApplicationAtlasManager.BADGE_4_ICON,    HallOfFameAssets.BADGE_4,     30),
        RANK_V    ( 5, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_5,  ApplicationAtlasManager.BADGE_5_ICON,    HallOfFameAssets.BADGE_5,     48),
        RANK_VI   ( 6, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_6,  ApplicationAtlasManager.BADGE_6_ICON,    HallOfFameAssets.BADGE_6,     73),
        RANK_VII  ( 7, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_7,  ApplicationAtlasManager.BADGE_7_ICON,    HallOfFameAssets.BADGE_7,    105),
        RANK_VIII ( 8, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_8,  ApplicationAtlasManager.BADGE_8_ICON,    HallOfFameAssets.BADGE_8,    145),
        RANK_IX   ( 9, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_9,  ApplicationAtlasManager.BADGE_9_ICON,    HallOfFameAssets.BADGE_9,    193),
        RANK_X    (10, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_10, ApplicationAtlasManager.BADGE_10_ICON,   HallOfFameAssets.BADGE_10,   248),
        RANK_XI   (11, true,  true,  ApplicationTextManager.ApplicationTextsAssets.USER_RANK_11, ApplicationAtlasManager.BADGE_11_ICON,   HallOfFameAssets.BADGE_11,   316);

        private int id;
        private String nameKey;
        private String badgeIcon;
        private final String hallOfFameBadge;

        //This value is not final as it can be influenced by RankComputingScripts
        private int experiencePointsNeeded;

        private boolean userMenu, badgeBoard;

        UserRank(int id, boolean userMenu, boolean badgeBoard, String nameKey, String badgeIcon, String hallOfFameBadge, int experiencePointsNeeded) {
            this.id = id;
            this.nameKey = nameKey;
            this.badgeIcon = badgeIcon;
            this.hallOfFameBadge = hallOfFameBadge;
            this.experiencePointsNeeded = experiencePointsNeeded;

            this.userMenu = userMenu;
            this.badgeBoard = badgeBoard;
        }

        public int getId() {
            return id;
        }

        public static UserRank getInitialRank() {
            return RANK_NONE;
        }

        public static UserRank getRank(int id) {
            for(UserRank rank : values()) {
                if(rank.getId() == id)
                    return rank;
            }

            return null;
        }

        public static UserRank getNextRank(UserRank rank) {
            UserRank next = UserRank.getRank(rank.getId() + 1);
            return next != null ? next : rank;
        }

        public static UserRank getPreviousRank(UserRank rank) {
            UserRank prev = UserRank.getRank(rank.getId() - 1);
            return prev != null ? prev : rank;
        }

        public boolean hasBadgeName() {
            return nameKey != null;
        }

        public boolean isShownOnUserMenu() {
            return userMenu;
        }

        public boolean isShownOnBadgeBoard() {
            return badgeBoard;
        }

        public String getBadgeIconKey() {
            return badgeIcon;
        }

        public String getHallOfFameBadge() {
            return hallOfFameBadge;
        }

        public String getNameKey() {
            return nameKey;
        }

        public int getExperiencePointsNeeded() {
            return experiencePointsNeeded;
        }
    }

    public static class RankProgress {
        int minXP, currXP, nextXP;

        public RankProgress(int minXP, int currXP, int nextXP) {
            this.minXP = minXP;
            this.currXP = currXP;
            this.nextXP = nextXP;
        }

        public int getMinXP() {
            return minXP;
        }

        public void setMinXP(int minXP) {
            this.minXP = minXP;
        }

        public int getCurrXP() {
            return currXP;
        }

        public void setCurrXP(int currXP) {
            this.currXP = currXP;
        }

        public int getNextXP() {
            return nextXP;
        }

        public void setNextXP(int nextXP) {
            this.nextXP = nextXP;
        }

        public void increaseCurrXP(int amount) {
            currXP += amount;
        }

        /**
         * Makes this progress 100 percent complete
         */
        public void capRankProgress() {
            if(currXP > nextXP) currXP = nextXP;
        }

        public boolean isComplete() {
            return currXP >= nextXP;
        }

        public float getPercentDone() {
            if(nextXP - minXP == 0) return 1f; //divide by zero case
            return (currXP - minXP) / (float) (nextXP - minXP);
        }

        @Override
        public String toString() {
            return (getCurrXP() - getMinXP()) + "/" + (getNextXP() - getMinXP());
        }
    }

    /**
     * RankComputeData class carries all values about users rank progress and is used
     * to get/calculate all the necessary stuff about users progress...
     */
    public static class RankComputeData {
        private UserRankManager.UserRank currentRank;
        private UserRankManager.UserRank nextRank;
        private RankProgress[] rankProgresses;
        private Long lastRankUp;

        private boolean newUser = true;
        private boolean[] newGames;

        public RankComputeData() {
            currentRank = UserRankManager.UserRank.RANK_NONE;
            nextRank = UserRankManager.UserRank.getNextRank(currentRank);
            lastRankUp = 0L;

            initializeRankProgresses();
            initializeNewGames();
        }

        private void initializeRankProgresses() {
            //Creates rank progress for every game available
            this.rankProgresses = new RankProgress[GameDefinition.values().length];

            for(int i = 0; i < rankProgresses.length; i++) {
                rankProgresses[i] = new RankProgress(currentRank.experiencePointsNeeded, currentRank.experiencePointsNeeded, nextRank.experiencePointsNeeded);
            }
        }

        private void initializeNewGames() {
            this.newGames = new boolean[GameDefinition.values().length];
            clearNewGames();
        }

        public void clearNewGames() {
            for(int i = 0; i < newGames.length; i++) {
                newGames[i] = false;
            }
        }

        public void setNewGame(GameDefinition gameDefinition, boolean value) {
            this.newGames[gameDefinition.getGameNumber() - 1] = value;
        }

        public void setNewUser(boolean newUser) {
            this.newUser = newUser;
        }

        public boolean isNewGame(GameDefinition gameDefinition) {
            return !isNewUser() && newGames[gameDefinition.getGameNumber() - 1];
        }

        public boolean isNewUser() {
            return newUser;
        }

        public Long getLastRankUp() {
            return lastRankUp;
        }

        public void setLastRankUp(Long lastRankUp) {
            this.lastRankUp = lastRankUp;
        }

        public UserRank getCurrentRank() {
            return currentRank;
        }

        public void setCurrentRank(UserRank currentRank) {
            this.currentRank = currentRank;
        }

        public UserRank getNextRank() {
            return nextRank;
        }

        public void setNextRank(UserRank nextRank) {
            this.nextRank = nextRank;
        }

        public RankProgress[] getRankProgresses() {
            return rankProgresses;
        }

        public RankProgress getRankProgress(int gameNumber) {
            return rankProgresses[gameNumber - 1];
        }
    }

    //Singleton's instance
    private static UserRankManager instance;

    //Keeps track of users rank compute data
    private Map<Long, RankComputeData> rankComputeDataMap;

    private UserRankManager() {
        rankComputeDataMap  = new HashMap<Long, RankComputeData>();
    }

    public static UserRankManager getInstance() {
        if(instance == null) Log.info(UserRankManager.class, "UserRankManager is not initialized!");

        return instance;
    }

    public static void initialize() {
        if(instance == null)  instance = new UserRankManager();

        RankProcessingScript.initialize();
        computeHighestRank();
    }

    private static UserRank highestRank;

    private static void computeHighestRank() {
        highestRank = UserRank.RANK_NONE;
        for(UserRank rank : UserRank.values()) {
            if(rank.getId() > highestRank.getId())
                highestRank = rank;
        }
    }

    protected UserRank getHighestRank() {
        return highestRank;
    }

    public RankProgress[] getGameRankProgresses(User user) {
        return getRankComputeData(user).getRankProgresses();
    }

    public RankProgress getGameRankProgress(User user, GameDefinition game) {
        return getGameRankProgresses(user)[game.getGameNumber() - 1];
    }

    public RankProgress getTotalUserRankProgress(User user) {
        RankProgress resultRankProgress = new RankProgress(0, 0, 0);

        RankProgress[] gameRankProgresses =  getGameRankProgresses(user);

        for(RankProgress rankProgress : gameRankProgresses) {
            int min = rankProgress.getMinXP();
            int curr = rankProgress.getCurrXP() > rankProgress.getNextXP() ? rankProgress.getNextXP() : rankProgress.getCurrXP();
            int next = rankProgress.getNextXP();

            resultRankProgress.setMinXP(resultRankProgress.getMinXP() + min);
            resultRankProgress.setCurrXP(resultRankProgress.getCurrXP() + curr);
            resultRankProgress.setNextXP(resultRankProgress.getNextXP() + next);
        }

        return resultRankProgress;
    }

    public UserRank getRank(User user) {
        if(rankComputeDataMap == null || !rankComputeDataMap.containsKey(user.getId()))
            return UserRank.RANK_NONE;

        return getRankComputeData(user).currentRank;
    }

    public RankComputeData getRankComputeData(User user) {
        return rankComputeDataMap.get(user.getId());
    }

    public long getLastTimeRankedUp(User user) {
        return getRankComputeData(user).lastRankUp;
    }

    public boolean isNewGameForUser(User user, GameDefinition gameDefinition) {
        return getRankComputeData(user).isNewGame(gameDefinition);
    }

    public synchronized boolean addGameToRankData(User user, Game game, boolean gotNewTrophy) {
        RankComputeData rankComputeData = getRankComputeData(user);

        long lastRankUp = rankComputeData.getLastRankUp();
        RankProcessingScript.addGameToRankData(rankComputeData, game);

        boolean gotAllTrophiesAnimation = checkIfGotAllTrophies(user, gotNewTrophy);
        if(lastRankUp < rankComputeData.lastRankUp && gotAllTrophiesAnimation) {
            ApplicationBus.getInstance().post(new AllTrophiesAndUserRankUpEvent(user, UserRank.getPreviousRank(rankComputeData.currentRank), rankComputeData.currentRank)).asynchronously();
            return true;
        }
        else if(lastRankUp < rankComputeData.lastRankUp) {
            ApplicationBus.getInstance().post(new UserRankUpEvent(user, UserRank.getPreviousRank(rankComputeData.currentRank), rankComputeData.currentRank)).asynchronously();
            return true;
        }
        else if(gotAllTrophiesAnimation){
            ApplicationBus.getInstance().post(new AllTrophiesEvent(user)).asynchronously();
            return true;
        }
        return false;
    }

    private boolean checkIfGotAllTrophies(User user, boolean gotNewTrophy) {
        if(gotNewTrophy) {
            for (GameTrophyDefinition gameTrophyDefinition : GameTrophyDefinition.values()) {
                if (!gameTrophyDefinition.hasTrophy(user)) {
                    return false;
                }
            }
            for (UserTrophyDefinition userTrophyDefinition : UserTrophyDefinition.values()) {
                if (!userTrophyDefinition.hasTrophy(user)) {
                    return false;
                }
            }
            return true;
        }
        else return false;
    }


    public synchronized void refreshUserRankFull() {
        List<User> activeUsers = UserDAO.selectActiveUsers();
        for(User user : activeUsers) {
            refreshUserRank(user);
        }
    }

    public synchronized void onNewUser(User user) {
        rankComputeDataMap.put(user.getId(), new RankComputeData());
    }

    public synchronized void refreshUserRank(User user) {
        //Calculates actual rank and stores it in rankComputeDataMap
        Log.info(getClass(), "Calculating rank for: " + user.getName() + "");
        RankComputeData rankComputeData = RankProcessingScript.calculateUsersRank(user, new RankProcessingScript.RankGamesRetriever() {
            @Override
            public List<Game> getGames(User user, RankProcessingScript rankProcessingScript) {
                return GameDAO.selectUserGamesForRankManager(user.getId(), rankProcessingScript.getScriptVersion());
            }
        });
        rankComputeDataMap.put(user.getId(), rankComputeData);
        
        ApplicationBus.getInstance().post(new GameMenuItemGroup.RefreshMainMenuProgressBarsEvent()).asynchronously();

    }

    public static class UserRankUpEvent implements ApplicationBus.ApplicationEvent {
        private User user;
        private UserRank previousRank, newRank;

        private UserRankUpEvent(User user, UserRank previousRank, UserRank newRank) {
            this.previousRank = previousRank;
            this.newRank = newRank;
            this.user = user;
        }

        public User getUser() {
            return user;
        }

        public UserRank getPreviousRank() {
            return previousRank;
        }

        public UserRank getNewRank() {
            return newRank;
        }
    }

    public static class AllTrophiesEvent implements ApplicationBus.ApplicationEvent {
        private User user;

        private AllTrophiesEvent(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }
    }

    public static class AllTrophiesAndUserRankUpEvent implements ApplicationBus.ApplicationEvent {
        private User user;
        private UserRank previousRank, newRank;

        private AllTrophiesAndUserRankUpEvent(User user, UserRank previousRank, UserRank newRank) {
            this.previousRank = previousRank;
            this.newRank = newRank;
            this.user = user;
        }

        public User getUser() {
            return user;
        }

        public UserRank getPreviousRank() {
            return previousRank;
        }

        public UserRank getNewRank() {
            return newRank;
        }
    }
}