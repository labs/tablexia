/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.ranksystem;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.TablexiaBadgesManager;
import cz.nic.tablexia.loader.application.ApplicationExternalSoundManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ScaleUtil;
import cz.nic.tablexia.util.ui.TablexiaLabel;

/**
 * Created by drahomir on 6/8/16.
 */
public class RankAnimation extends Group {
    private static final ApplicationFontManager.FontType     RANK_NAME_LABEL_FONT  = ApplicationFontManager.FontType.BOLD_26;
    private static final Color                               RANK_NAME_LABEL_COLOR = Color.WHITE;

    private static final float   DIMMER_ALPHA                           = 0.7f;
    private static final Color   DIMMER_COLOR                           = new Color(0, 0, 0, DIMMER_ALPHA);

    private static final int     BACKGROUND_SHINE_COUNT                 = 3;
    private static final int     BACKGROUND_SHINE_RAYS_COUNT            = 6;
    private static final float   BACKGROUND_SHINE_RAY_SIZE              = 0.52f;

    private static final float   BACKGROUND_SHINE_ROTATION_SPEED        = 9;
    private static final float   BACKGROUND_SHINE_ROTATION_SPEED_DIFF   = 2;
    private static final float   BACKGROUND_SHINE_FADEIN_DURATION       = 0.5f;

    private static final float   BACKGROUND_SHINE_ALPHA                 = 0.65f;
    private static final Color[] BACKGROUND_SHINE_COLORS                = new Color[] {
            new Color(1, 1, 190/255f, BACKGROUND_SHINE_ALPHA),
            new Color(1, 190/255f, 1, BACKGROUND_SHINE_ALPHA),
            new Color(190/255f, 1, 1, BACKGROUND_SHINE_ALPHA)
    };

    private static final float   BADGE_HEIGHT                           = 0.31f;
    private static final float   BADGE_SLIDE_DISTANCE                   = 0.6f;
    private static final float   RANK_NAME_LABEL_Y_OFFSET               = 0.6f;

    private static final float   PROMOTION_TEXT_OFFSET                  = 0.24f;
    private static final float   PROMOTION_TEXT_WIDTH                   = 0.35f;

    private static final float   HIDE_FADEOUT_DURATION                  = 1f;
    private static final float   SHOW_FADEIN_DURATION                   = 1f;

    private static final float         SLIDE_ANIMATION_TIME            = 1.0f;
    private static final Interpolation SLIDE_ANIMATION_INTERPOLATION   = Interpolation.pow4;


    private final Group oldBadgeGroup, newBadgeGroup;

    private final Group shinesGroup;
    private final UserRankManager.UserRank oldRank, newRank;

    private boolean hidden = true;

    private RankAnimation(float width, float height, UserRankManager.UserRank oldRank, UserRankManager.UserRank newRank) {
        setSize(width, height);
        this.oldRank = oldRank;
        this.newRank = newRank;

        //Creating
        shinesGroup   = new Group();

        oldBadgeGroup = new Group();
        newBadgeGroup = new Group();

        addAction(Actions.alpha(0));
    }

    public static final RankAnimation createRankAnimation(Stage stage, UserRankManager.UserRank oldRank, UserRankManager.UserRank newRank) {
        float width  = TablexiaSettings.getViewportWidth(stage);
        float height = TablexiaSettings.getViewportHeight(stage);
        float posX   = TablexiaSettings.getViewportLeftX(stage);
        float posY   = TablexiaSettings.getViewportBottomY(stage);

        RankAnimation rankAnimation = new RankAnimation(width, height, oldRank, newRank);
        rankAnimation.setPosition(posX, posY);

        rankAnimation.prepareDimmer();
        rankAnimation.prepareShines();
        rankAnimation.preparePromotionImage();
        rankAnimation.prepareOldBadge();
        rankAnimation.prepareNewBadge();

        return rankAnimation;
    }

    private void prepareDimmer() {
        Image dimmer = new Image(ApplicationAtlasManager.getInstance().getColorTexture(DIMMER_COLOR));
        dimmer.setSize(this.getWidth(), this.getHeight());
        this.addActor(dimmer);
    }

    private void prepareShines() {
        shinesGroup.setSize(this.getWidth(), this.getHeight());

        for(int i = 0; i < BACKGROUND_SHINE_COUNT; i++) {
            Group singleShinesGroup = new Group();
            singleShinesGroup.setSize(this.getWidth(), this.getHeight());

            float angle = 360 / BACKGROUND_SHINE_RAYS_COUNT;
            for(int j = 0; j < BACKGROUND_SHINE_RAYS_COUNT; j++) {
                Image ray = ScaleUtil.createImageToHeight(
                        ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.RANK_UP_BACKGROUND),
                        singleShinesGroup.getHeight() * BACKGROUND_SHINE_RAY_SIZE
                );
                ray.setPosition(singleShinesGroup.getWidth() / 2 - ray.getWidth() / 2, singleShinesGroup.getHeight() / 2);
                ray.setOrigin(Align.bottom);
                ray.setRotation(angle * j);
                ray.setColor(BACKGROUND_SHINE_COLORS[i % BACKGROUND_SHINE_COLORS.length]);
                singleShinesGroup.addActor(ray);
            }

            angle = 360 / BACKGROUND_SHINE_COUNT;
            singleShinesGroup.setOrigin(Align.center);
            singleShinesGroup.setRotation(i * angle);

            int direction = i % 2 == 0 ? 1 : -1;
            singleShinesGroup.addAction(Actions.forever(Actions.rotateBy(direction * 360, BACKGROUND_SHINE_ROTATION_SPEED + i * BACKGROUND_SHINE_ROTATION_SPEED_DIFF)));

            shinesGroup.addActor(singleShinesGroup);
        }

        //Initially hidden
        shinesGroup.addAction(Actions.alpha(0f));
        this.addActor(shinesGroup);
    }

    private void preparePromotionImage() {
        Image promotionImage = ScaleUtil.createImageToWidth(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.RANK_UP_PROMOTION), PROMOTION_TEXT_WIDTH * this.getWidth());
        promotionImage.setPosition(this.getWidth() / 2 - promotionImage.getWidth() / 2, this.getHeight() / 2 - promotionImage.getHeight() / 2 + PROMOTION_TEXT_OFFSET * this.getHeight());
        this.addActor(promotionImage);
    }

    private void prepareOldBadge() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                prepareBadgeGroup(oldBadgeGroup, oldRank, TablexiaBadgesManager.getInstance().getCurrentRankBadgeTexture());
            }
        });
        this.addActor(oldBadgeGroup);
    }

    private void prepareNewBadge() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                prepareBadgeGroup(newBadgeGroup, newRank, TablexiaBadgesManager.getInstance().getNextRankBadgeTexture());
            }
        });

        newBadgeGroup.addAction(Actions.alpha(0));
        newBadgeGroup.addAction(Actions.moveBy(BADGE_SLIDE_DISTANCE * this.getWidth(), 0));

        this.addActor(newBadgeGroup);
    }

    private void prepareBadgeGroup(Group group, UserRankManager.UserRank userRank, Texture badgeTexture) {
        Image badgeImage = createBadgeImage(badgeTexture);
        group.setSize(badgeImage.getWidth(), badgeImage.getHeight());
        group.setPosition(this.getWidth() / 2f - group.getWidth() / 2f, this.getHeight() / 2f - group.getHeight() / 2f);
        group.addActor(badgeImage);

        if(userRank.hasBadgeName()) {
            TablexiaLabel badgeLabel = createBadgeLabel(userRank);
            badgeLabel.setPosition(badgeImage.getX() + badgeImage.getWidth() / 2f - badgeLabel.getWidth() / 2f, badgeImage.getY() - badgeLabel.getHeight() - badgeLabel.getHeight() * RANK_NAME_LABEL_Y_OFFSET);
            group.addActor(badgeLabel);
        }
    }

    private TablexiaLabel createBadgeLabel(UserRankManager.UserRank userRank) {
        TablexiaLabel badgeLabel = new TablexiaLabel(
                ApplicationTextManager.getInstance().getText(userRank.getNameKey()),
                new TablexiaLabel.TablexiaLabelStyle(RANK_NAME_LABEL_FONT, RANK_NAME_LABEL_COLOR)
        );

        badgeLabel.setAlignment(Align.center);
        return  badgeLabel;
    }

    private Image createBadgeImage(Texture badgeTexture) {
        return ScaleUtil.createImageToHeight(badgeTexture, this.getHeight() * BADGE_HEIGHT);
    }

    private void animateBadges() {
        oldBadgeGroup.addAction(
            new ParallelAction(
                Actions.moveBy(-BADGE_SLIDE_DISTANCE * this.getWidth(), 0, SLIDE_ANIMATION_TIME, SLIDE_ANIMATION_INTERPOLATION),
                Actions.fadeOut(SLIDE_ANIMATION_TIME, SLIDE_ANIMATION_INTERPOLATION)
            )
        );

        newBadgeGroup.addAction(
                new ParallelAction(
                        Actions.moveBy(-BADGE_SLIDE_DISTANCE * this.getWidth(), 0, SLIDE_ANIMATION_TIME, SLIDE_ANIMATION_INTERPOLATION),
                        Actions.fadeIn(SLIDE_ANIMATION_TIME, SLIDE_ANIMATION_INTERPOLATION)
                )
        );

        shinesGroup.addAction(
            new SequenceAction(
                Actions.delay(SLIDE_ANIMATION_TIME),
                new ParallelAction(
                    Actions.fadeIn(BACKGROUND_SHINE_FADEIN_DURATION),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            ApplicationExternalSoundManager.getInstance().getSound(ApplicationExternalSoundManager.RANKUP).play();
                        }
                    })
                )
            )
        );
    }

    ////////////////////////////////
    /////    PUBLIC METHODS    /////
    ////////////////////////////////

    public void hide() {
        hidden = true;

        this.addAction(new SequenceAction(
                Actions.fadeOut(HIDE_FADEOUT_DURATION),
                Actions.removeActor()
        ));
    }

    public void show() {
        if(getStage() == null) {
            Log.err(getClass(), "Couldn't show Rank Animation, because there is no stage!");
        }

        hidden = false;

        this.addAction(new SequenceAction(
            Actions.fadeIn(SHOW_FADEIN_DURATION),
            Actions.run(new Runnable() {
                @Override
                public void run() {
                    animateBadges();
                }
            })
        ));
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setNewBadgeEventListener(EventListener eventListener) {
        if(eventListener == null) return;

        newBadgeGroup.clearListeners();
        newBadgeGroup.addListener(eventListener);
    }
}