/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game;

import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.ApplicationBus.ApplicationEvent;
import cz.nic.tablexia.game.games.attention.AttentionGame;
import cz.nic.tablexia.game.games.crime_scene.CrimeSceneGame;
import cz.nic.tablexia.game.games.in_the_darkness.InTheDarknessGame;
import cz.nic.tablexia.game.games.kidnapping.KidnappingGame;
import cz.nic.tablexia.game.games.memory.MemoryGame;
import cz.nic.tablexia.game.games.night_watch.NightWatchGame;
import cz.nic.tablexia.game.games.protocol.ProtocolGame;
import cz.nic.tablexia.game.games.pursuit.PursuitGame;
import cz.nic.tablexia.game.games.robbery.RobberyGame;
import cz.nic.tablexia.game.games.on_the_trail.OnTheTrailGame;
import cz.nic.tablexia.game.games.runes.RunesGame;
import cz.nic.tablexia.game.games.safe.SafeGame;
import cz.nic.tablexia.game.games.shooting_range.ShootingRangeGame;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.IMenuItem;
import cz.nic.tablexia.shared.model.definitions.INumberedDefinition;
import cz.nic.tablexia.shared.model.resolvers.AbstractGameScoreResolver;

public enum GameDefinition implements ApplicationEvent, IMenuItem, INumberedDefinition {

    ROBBERY         (cz.nic.tablexia.shared.model.definitions.GameDefinition.ROBBERY, ApplicationTextManager.ApplicationTextsAssets.GAME_ROBBERY_TITLE, ApplicationTextManager.ApplicationTextsAssets.GAME_ROBBERY_DESCRIPTION, RobberyGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE),
    PURSUIT         (cz.nic.tablexia.shared.model.definitions.GameDefinition.PURSUIT, ApplicationTextManager.ApplicationTextsAssets.GAME_PURSUIT_TITLE, ApplicationTextManager.ApplicationTextsAssets.GAME_PURSUIT_DESCRIPTION, PursuitGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE_DURATION, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE_DURATION),
    KIDNAPPING      (cz.nic.tablexia.shared.model.definitions.GameDefinition.KIDNAPPING, ApplicationTextManager.ApplicationTextsAssets.GAME_KIDNAPPING_TITLE, ApplicationTextManager.ApplicationTextsAssets.GAME_KIDNAPPING_DESCRIPTION, KidnappingGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE_ERROR, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE_ERROR),
    NIGHT_WATCH     (cz.nic.tablexia.shared.model.definitions.GameDefinition.NIGHT_WATCH, ApplicationTextManager.ApplicationTextsAssets.GAME_NIGHT_WATCH_TITLE, ApplicationTextManager.ApplicationTextsAssets.GAME_NIGHT_WATCH_DESCRIPTION, NightWatchGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE),
    SHOOTING_RANGE  (cz.nic.tablexia.shared.model.definitions.GameDefinition.SHOOTING_RANGE, ApplicationTextManager.ApplicationTextsAssets.GAME_SHOOTING_RANGE_TITLE, ApplicationTextManager.ApplicationTextsAssets.GAME_SHOOTING_RANGE_DESCRIPTION, ShootingRangeGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE),
    IN_THE_DARKNESS (cz.nic.tablexia.shared.model.definitions.GameDefinition.IN_THE_DARKNESS, ApplicationTextManager.ApplicationTextsAssets.GAME_IN_THE_DARKNESS_TITLE, ApplicationTextManager.ApplicationTextsAssets.GAME_IN_THE_DARKNESS_DESCRIPTION, InTheDarknessGame.class, true, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE_ERROR, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE_ERROR),
    RUNES           (cz.nic.tablexia.shared.model.definitions.GameDefinition.RUNES, ApplicationTextManager.ApplicationTextsAssets.GAME_RUNES_TITLE, ApplicationTextManager.ApplicationTextsAssets.GAME_RUNES_DESCRIPTION, RunesGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE),
    CRIME_SCENE     (cz.nic.tablexia.shared.model.definitions.GameDefinition.CRIME_SCENE, ApplicationTextManager.ApplicationTextsAssets.GAME_CRIME_SCENE_TITLE, ApplicationTextManager.ApplicationTextsAssets.GAME_CRIME_SCENE_DESCRIPTION, CrimeSceneGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE),
    PROTOCOL        (cz.nic.tablexia.shared.model.definitions.GameDefinition.PROTOCOL,ApplicationTextManager.ApplicationTextsAssets.GAME_PROTOCOL_TITLE,ApplicationTextManager.ApplicationTextsAssets.GAME_PROTOCOL_DESCRIPTION, ProtocolGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE,ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE),
    SAFE            (cz.nic.tablexia.shared.model.definitions.GameDefinition.SAFE, ApplicationTextManager.ApplicationTextsAssets.GAME_SAFE_TITLE, ApplicationTextManager.ApplicationTextsAssets.GAME_SAFE_DESCRIPTION, SafeGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE),
    ON_THE_TRAIL    (cz.nic.tablexia.shared.model.definitions.GameDefinition.ON_THE_TRAIL, ApplicationTextManager.ApplicationTextsAssets.GAME_ON_THE_TRAIL_TITLE, ApplicationTextManager.ApplicationTextsAssets.GAME_ON_THE_TRAIL_DESCRIPTION, OnTheTrailGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE),
    MEMORY_GAME     (cz.nic.tablexia.shared.model.definitions.GameDefinition.MEMORY_GAME, ApplicationTextManager.ApplicationTextsAssets.MEMORY_GAME_TITLE, ApplicationTextManager.ApplicationTextsAssets.MEMORY_GAME_DESCRIPTION, MemoryGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE_ERROR, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE_ERROR),
    ATTENTION_GAME  (cz.nic.tablexia.shared.model.definitions.GameDefinition.ATTENTION_GAME, ApplicationTextManager.ApplicationTextsAssets.ATTENTION_GAME_TITLE, ApplicationTextManager.ApplicationTextsAssets.ATTENTION_GAME_DESCRIPTION, AttentionGame.class, false, ApplicationTextManager.ApplicationTextsAssets.GAME_SCORE, ApplicationTextManager.ApplicationTextsAssets.GAME_AVERAGE_SCORE);

    private final cz.nic.tablexia.shared.model.definitions.GameDefinition   modelGameDefinition;
    private String                                   menuTextKey;
    private String                                   descriptionResource;
    private Class<? extends AbstractTablexiaGame<?>> screenClass;
    private boolean                                  hasTutorial;
    private String                                   statisticsScoreDialogText;
    private String                                   statisticsAverageScoreDialogText;

    GameDefinition(cz.nic.tablexia.shared.model.definitions.GameDefinition modelGameDefinition, String nameResource, String descriptionResource, Class<? extends AbstractTablexiaGame<?>> screenClass, boolean hasTutorial, String statisticsDialogText, String statisticsAverageScoreDialogText) {
        this(modelGameDefinition, nameResource, descriptionResource, screenClass, hasTutorial, false, statisticsDialogText, statisticsAverageScoreDialogText);
    }

    GameDefinition(cz.nic.tablexia.shared.model.definitions.GameDefinition modelGameDefinition, String nameResource, String descriptionResource, Class<? extends AbstractTablexiaGame<?>> screenClass, boolean hasTutorial, boolean hasBonus, String statisticsDialogText, String statisticsAverageScoreDialogText) {
        this.modelGameDefinition = modelGameDefinition;
        this.menuTextKey = nameResource;
        this.descriptionResource = descriptionResource;
        this.screenClass = screenClass;
        this.hasTutorial = hasTutorial;
        this.statisticsScoreDialogText = statisticsDialogText;
        this.statisticsAverageScoreDialogText = statisticsAverageScoreDialogText;
    }

    public int getGameNumber() {
        return modelGameDefinition.number();
    }

    @Override
    public String getTitle() {
        return ApplicationTextManager.getInstance().getResult().get(menuTextKey);
    }

    @Override
    public String getDescription() {
        return ApplicationTextManager.getInstance().getResult().get(descriptionResource);
    }


    public String getStatisticsScoreDialogText() {
        return statisticsScoreDialogText;
    }


    public String getStatisticsAverageScoreDialogText() {
        return statisticsAverageScoreDialogText;
    }

    public Class<? extends AbstractTablexiaGame<?>> getScreenClass() {
        return screenClass;
    }

    public boolean hasTutorial() {
        return hasTutorial;
    }

    @Override
    public void performAction() {
        ApplicationBus.getInstance().publishAsync(this);
        if (TablexiaApplication.getActualScreenClass() != GameDefinition.this.getScreenClass()) {
            ApplicationBus.getInstance().publishAsync(new Tablexia.ChangeScreenEvent(GameDefinition.this.getScreenClass(), TablexiaApplication.ScreenTransaction.FADE));
        }
    }

    @Override
    public AbstractMenu.MenuAction getMenuAction() {
        return AbstractMenu.MenuAction.CLOSE;
    }

    @Override
    public IMenuItem[] getSubmenu() {
        return null;
    }

    public AbstractGameScoreResolver getGameResultResolver(int resolverVersion) {
        return modelGameDefinition.getGameScoreResolver(resolverVersion);
    }

    public Integer getGameScoreResolverVersion() {
        return modelGameDefinition.getCurrentGameScoreResolverVersion();
    }

    public AbstractGameScoreResolver getCurrentGameScoreResolver() {
        return modelGameDefinition.getCurrentGameScoreResolver();
    }

    public static List<GameDefinition> getSortedGameDefinitionList(){
        List<GameDefinition> sorted = Arrays.asList(values());
        Collections.sort(sorted, new INumberComparator());
        return sorted;
    }

    public static GameDefinition getGameDefinitionForClass(Class<? extends AbstractTablexiaGame> screenClass) {
        for (GameDefinition gameDefinition : GameDefinition.values()) {
            if (gameDefinition.screenClass.equals(screenClass)) {
                return gameDefinition;
            }
        }
        return null;
    }

    public static GameDefinition getGameDefinitionForGameNumber(int gameNumber) {
        for (GameDefinition gameDefinition : GameDefinition.values()) {
            if (gameDefinition.modelGameDefinition.number() == gameNumber) {
                return gameDefinition;
            }
        }
        return null;
    }

    @Override
    public Class<? extends Group> getItemGroupClass() {
        return null; // TODO
    }

    @Override
    public String[] getIcons() {
        return new String[0]; // TODO
    }

    @Override
    public int number() {
        return modelGameDefinition.number();
    }
}
