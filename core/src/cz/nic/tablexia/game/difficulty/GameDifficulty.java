/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.nic.tablexia.game.difficulty;


import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;

/**
 * Game difficulty types
 * 
 * @author Matyáš Latner
 */
public enum GameDifficulty {

    TUTORIAL(DifficultyDefinition.TUTORIAL,  "gamedifficulty_tutorial",  false, false),
    EASY    (DifficultyDefinition.EASY,      "gamedifficulty_easy",      true, true),
    MEDIUM  (DifficultyDefinition.MEDIUM,    "gamedifficulty_medium",    true, true),
    HARD    (DifficultyDefinition.HARD,      "gamedifficulty_hard",      true, true),
    BONUS   (DifficultyDefinition.BONUS,     "gamedifficulty_bonus",     true, true);

    public static final GameDifficulty  DEFAULT_DIFFICULTY = EASY;
    public static final int             BONUS_DIFFICULTY_UNLOCK_THRESHOLD = 10;

    private final DifficultyDefinition  difficultyDefinition;
    private final String                descriptionResourceKey;
    private final boolean               visible;
    private final boolean               hasScore;

    GameDifficulty(DifficultyDefinition difficultyDefinition, String descriptionResourceKey, boolean visible, boolean hasScore) {
        this.difficultyDefinition = difficultyDefinition;
        this.descriptionResourceKey = descriptionResourceKey;
        this.visible = visible;
        this.hasScore = hasScore;
    }

    public DifficultyDefinition getDifficultyDefinition() {
        return difficultyDefinition;
    }

    public int getDifficultyNumber() {
        return difficultyDefinition.number();
    }

    public String getTextDescription() {
        return ApplicationTextManager.getInstance().getText(getDescriptionResourceKey());
    }

    public boolean isVisible() {
        return visible;
    }

    public static List<GameDifficulty> getVisibleGameDifficultyList() {
        List<GameDifficulty> gameDifficultyList = new ArrayList<GameDifficulty>();
        for (GameDifficulty gameDifficulty: GameDifficulty.values()) {
            if (gameDifficulty.isVisible()) {
                gameDifficultyList.add(gameDifficulty);
            }
        }
        return gameDifficultyList;
    }

    /**
     * Returns key of description for current difficulty
     * 
     * @return key of description string resource
     */
    public String getDescriptionResourceKey() {
        return descriptionResourceKey;
    }

    public static GameDifficulty getGameDifficultyForDifficultyNumber(int difficultyNumber) {
        for (GameDifficulty gameDifficulty: GameDifficulty.values()) {
            if (gameDifficulty.difficultyDefinition.number() == difficultyNumber) {
                return gameDifficulty;
            }
        }
        return null;
    }

    public static GameDifficulty getByGame(Game game) {
        return getGameDifficultyForDifficultyNumber(game.getGameDifficulty());
    }

    public int getExperiencePointsMultiplier() {
        return difficultyDefinition.getExperiencePointsMultiplier();
    }

    public boolean hasScore() {
        return hasScore;
    }
}
