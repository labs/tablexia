/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import net.engio.mbassy.listener.Handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaApplication;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.event.DimmerControlEvent;
import cz.nic.tablexia.bus.event.MenuControlEvent;
import cz.nic.tablexia.bus.event.StartIncrementalSynchronizationEvent;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.kidnapping.actions.WhileAction;
import cz.nic.tablexia.game.ranksystem.RankAnimation;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.game.trophy.AllTrophiesAnimation;
import cz.nic.tablexia.game.trophy.ITrophyDefinition;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationInternalSoundManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.loader.application.TablexiaBadgesManager;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.game.GameMenuItemGroup;
import cz.nic.tablexia.menu.main.MainMenu;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.model.UserDifficultySettingsDAO;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.gamemenu.gamepages.GamePageScreen;
import cz.nic.tablexia.screen.halloffame.HallOfFameScreen;
import cz.nic.tablexia.screen.halloffame.trophy.TrophyHelper;
import cz.nic.tablexia.screen.profile.ProfileScreen;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.MusicUtil;
import cz.nic.tablexia.util.actions.MoveAlongAction;
import cz.nic.tablexia.util.ui.ClickListenerWithSound;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;
import cz.nic.tablexia.util.ui.TablexiaLabel;
import cz.nic.tablexia.util.ui.TablexiaProgressBar;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.button.TablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AdaptiveSizeDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DimmerDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ImageContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TouchCloseDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TwoButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ViewportMaximumSizeComponent;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public abstract class AbstractTablexiaGame<T> extends AbstractTablexiaScreen<T> {

    protected enum GamePhase {

        PREPARED(false),
        STARTED(true),
        FINISHED(false);

        private boolean pause;

        GamePhase(boolean pause) {
            this.pause = pause;
        }

        public boolean isPause() {
            return pause;
        }
    }

    public enum GameResult {

        NO_STAR     (GameResultDefinition.NO_STAR,    null),
        ONE_STAR    (GameResultDefinition.ONE_STAR,   RATINGSTAR1_SOUND),
        TWO_STAR    (GameResultDefinition.TWO_STAR,   RATINGSTAR2_SOUND),
        THREE_STAR  (GameResultDefinition.THREE_STAR, RATINGSTAR3_SOUND);

        private GameResultDefinition gameResultDefinition;
        private String startSoundName;

        GameResult(GameResultDefinition gameResultDefinition, String startSoundName) {
            this.gameResultDefinition = gameResultDefinition;
            this.startSoundName = startSoundName;
        }

        public int getStarCount() {
            return gameResultDefinition.number();
        }

        public String getStartSoundName() {
            return startSoundName;
        }

        public GameResultDefinition getGameResultDefinition() {
            return gameResultDefinition;
        }

        public static GameResult getGameResultForStartCount(int starCount) {
            for (GameResult gameResult : GameResult.values()) {
                if (gameResult.getStarCount() == starCount) {
                    return gameResult;
                }
            }
            return null;
        }

        public static GameResult getGameResultForGameResultDefinition(GameResultDefinition gameCupsResult) {
            for(GameResult gameResult : values()) {
                if(gameResult.gameResultDefinition == gameCupsResult) {
                    return gameResult;
                }
            }
            return null;
        }
    }

    private static final String GAME_PATH         = "game/";
    private static final String GLOBAL_PATH       = "_global/";
    private static final String LIFE_PATH         = "life/";

    public static final String HEART_FULL         = LIFE_PATH + "heart_full";
    public static final String HEART_BROKEN       = LIFE_PATH + "heart_broken";

    private static final String RATINGSTAR1_SOUND = GAME_PATH + GLOBAL_PATH + "victoryscreen_star1.mp3";
    private static final String RATINGSTAR2_SOUND = GAME_PATH + GLOBAL_PATH + "victoryscreen_star2.mp3";
    private static final String RATINGSTAR3_SOUND = GAME_PATH + GLOBAL_PATH + "victoryscreen_star3.mp3";
    private static final String VICTORY_DIALOG_SHOW_SOUND = GAME_PATH + GLOBAL_PATH + "victoryscreen_show.mp3";

    public static final String EVENT_FINISH_GAME        = "finish game";
    public static final String EVENT_GAME_READY         = "game ready";
    public static final String EVENT_ANIMATION_FINISHED = "event animation victory finished";

    public static final String VICTORY_DIALOG = "victory dialog";
    public static final String EVENT_VICTORY_DIALOG_READY = "victory dialog ready";
    public static final String ALL_TROPHY_ANIMATION_SHOWN = "all trophy animation shown";
    public static final String VICTORY_PROGRESS_BAR = "victory progress bar";
    public static final String IMAGE_CUBS = "image cubs";
    public static final String RESULT_TABLE = "result table";

    public static final String GAME_RULE_BACKGROUND                 = "rule_background";
    public static final String VICTORY_DIALOG_RESULTSBANNER         = "resultsbanner";
    public static final String VICTORY_DIALOG_RATINGSTAR_DISABLED   = "ratingstar_disabled";
    public static final String VICTORY_DIALOG_RATINGSTAR_ENABLED    = "ratingstar_enabled";
    public static final String VICTORY_DIALOG_SUMMARY_ICON_STATS    = "stats";
    public static final String VICTORY_DIALOG_SUMMARY_ICON_TIME     = "time";
    private static final String VICTORY_DIALOG_TUTORIAL_TEXT        = "victoryscreen_text_tutorial";

    private static final int   VICTORY_DIALOG_SHOW_SOUND_DELAY      = 2;
    public  static final float VICTORY_SPEECH_FADE_OUT_TIME         = 0.5f;
    private static final float VICTORY_DIALOG_RATINGSTARS_DELAY     = 0.5f;
    private static final float VICTORY_DIALOG_TEXT_PADDING          = 30f;
    private static final Color VICTORY_DIALOG_SUMMARY_FONT_COLOR    = Color.GRAY;

    private static final String PRELOADER_DIFFICULTY_SUFFIX_SEPARATOR = "_";

    private static final float  ANIMATION_DELAY                       = 3 / 2f;

    public static final String RANDOM_SEED_SCREEN_INFO_LABEL = "Random Seed";
    public static final String GAME_DIFFICULTY_SCREEN_INFO_LABEL = "Game Difficulty";

    public static final String ATLAS_SUFFIX      = ".atlas";
    public static final String GLOBAL_ATLAS_NAME = GAME_PATH + GLOBAL_PATH + "_global" + ATLAS_SUFFIX;

    public static final String GAME_STATE_GAME_ID = "game_id";

    //Lower equals faster (how many seconds does it take from 0% to 100%
    private static final float   PROGRESS_BAR_SPEED  = 1f;

    //Experience Stars
    private static final Vector2[]      EXPERIENCE_STAR_PATH = new Vector2[] {
            //Path (without final destination), which every star will follow (in stars coords system)
            new Vector2(0, 0), //Starts at star
            new Vector2(50, 80),
            new Vector2(-10, -90),
    };

    //Position of a star on a cup
    private static final float          EXPERIENCE_STAR_X_TO_CUP                = 0.5f;
    private static final float          EXPERIENCE_STAR_Y_TO_CUP                = 0.68f;

    //Star path variables
    private static final float          EXPERIENCE_STAR_PATH_DURATION           = 2f;
    private static final float          EXPERIENCE_STAR_PATH_SCALE              = 1.25f;
    private static final float          EXPERIENCE_STAR_PATH_ROTATION           = 2f;

    //Star variables
    private static final float          EXPERIENCE_STAR_WIDTH_TO_CUP            = 3f / 10;
    private static final float          EXPERIENCE_STAR_DELAY                   = 1f / 2;
    private static final float          EXPERIENCE_STAR_FADE_IN                 = 1f / 2;
    private static final Interpolation  EXPERIENCE_STAR_FADE_IN_INTERPOLATION   = Interpolation.pow3;

    private static final float          EXPERIENCE_STAR_FADE_OUT_DELAY          = 1f / 4;
    private static final float          EXPERIENCE_STAR_FADE_OUT                = 3f / 4;
    private static final float          EXPERIENCE_STAR_FADE_OUT_SCALE          = 1.25f;
    private static final float          EXPERIENCE_STAR_FADE_OUT_SCALE_TIME     = 1f;
    private static final Interpolation  EXPERIENCE_STAR_FADE_OUT_INTERPOLATION  = Interpolation.pow3Out;

    private static final float          DEBUG_END_GAME_BUTTON_SIZE      = 48f;
    private static final float          DEBUG_END_BUTTON_Y_OFFSET       = 0.04f;

    private Game game;
    private TablexiaRandom random;
    private GamePhase gamePhase;
    private boolean inGameLoading;
    private Map<ITrophyDefinition, Boolean> hasTrophies;

    private boolean menuPause   = false;
    private boolean dialogPause = false;

    private boolean playAgainButtonClicked = false;

    private ArrayList<TablexiaComponentDialog> receivedTrophyDialogs;
    private TablexiaComponentDialog victoryDialog;
    private RankAnimation rankAnimation;
    private AllTrophiesAnimation allTrophiesAnimation;
    private LinkedList<ExperienceStar> experienceStars;

    private static TablexiaButton debugEndGameButton;

    private Color pauseGameColor = new Color(0,0,0,0.99f);


    public AbstractTablexiaGame() {
        inGameLoading = false;
        gamePhase = GamePhase.PREPARED;
        hasTrophies = new HashMap<ITrophyDefinition, Boolean>();
        receivedTrophyDialogs = new ArrayList<TablexiaComponentDialog>();
    }


//////////////////////////// QUIT DIALOG

    private boolean gameQuitDialogAccepted = false;

    @Override
    public StopScreenExitReason getStopScreenExitReason(AbstractTablexiaScreen<?> nextScreen) {
        //Is game currently running?
        if((gamePhase == GamePhase.STARTED || gamePhase == GamePhase.PREPARED) && !gameQuitDialogAccepted) return StopScreenExitReason.RunningGameQuit;
        else return super.getStopScreenExitReason(nextScreen);
    }

    @Override
    public String prepareExitDialogText(AbstractTablexiaScreen<?> nextScreen, StopScreenExitReason reason) {
        //Prepare text for game currently running exit dialog.
        if(reason == StopScreenExitReason.RunningGameQuit) return ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.GAME_QUIT_QUESTION);
        else return super.prepareExitDialogText(nextScreen, reason);
    }

    @Override
    protected void onExitDialogConfirmed(StopScreenExitReason reason) {
        if(reason == StopScreenExitReason.RunningGameQuit) {
            //User accepted the game running exit dialog. Continue with the action.
            gameQuitDialogAccepted = true;
            runAfterScreenExitAction();
        }
        else super.onExitDialogCanceled(reason);
    }

    @Override
    protected void onExitDialogCanceled(StopScreenExitReason reason) {
        //User canceled the game running exit dialog. Continue with the game.
        if(reason == StopScreenExitReason.RunningGameQuit) resetExitDialogState();
        else super.onExitDialogCanceled(reason);
    }

    @Override
    public void resetExitDialogState() {
        gameQuitDialogAccepted = false;
        super.resetExitDialogState();
    }

    //////////////////////////// PRELOADER

    public String prepareDifficultySuffix() {
        return PRELOADER_DIFFICULTY_SUFFIX_SEPARATOR + getGameDifficulty().name().toLowerCase();
    }

    @Override
    public boolean hasPreloader() {
        return true;
    }

    @Override
    public boolean preloaderHasSpeech() {
        return true;
    }


//////////////////////////// SCREEN PAUSE
    @Override
    public Color getPauseColor(){
        return pauseGameColor;
    }

    @Override
    public boolean canBePaused() {
        return gamePhase.isPause();
    }

    @Override
    public boolean canStopActions() {
        return true;
    }

    @Override
    protected boolean canSyncOnResume() {
        return false;
    }

    protected GamePhase getGamePhase() {
        return gamePhase;
    }

    @Handler
    public void handleMenuPauseEvent(AbstractMenu.MenuPauseEvent menuPauseEvent) {
        if (canBePaused()) {
            menuPause = menuPauseEvent.isPause();
            performPauseResumeAction();
        }
    }

    @Handler
    public void handleDialogVisibleEvent(TablexiaComponentDialog.DialogVisibleEvent dialogVisibleEvent) {
        if (canBePaused()) {
            dialogPause = dialogVisibleEvent.isDialogVisible();
            performPauseResumeAction();
        }
    }

    private void performPauseResumeAction() {
        if (menuPause || dialogPause) {
            performScreenPaused();
        } else {
            performScreenResumed();
        }
    }


//////////////////////////// ABSTRACT TABLEXIA SCREEN LIFECYCLE

    @Override
    protected final void screenLoaded(final Map<String, String> screenState) {
        try {
            if(debugEndGameButton == null && TablexiaSettings.getInstance().getBuildType().isShowDebugButtons()) {
                createDebugEndGameButton();
            }

            TablexiaBadgesManager.getInstance().refreshBadges();

            performGameLoaded(screenState);
        } catch (Throwable t) {
            Log.err(getClass(), "Cannot resume screen state!", t);
            TablexiaStorage.getInstance().resetScreenState(this);
        }
        if (inGameLoading) {
            performShowTransaction(new Runnable() {
                @Override
                public void run() {
                    performScreenVisible();
                }
            });
        }
    }

    private void createDebugEndGameButton() {
        debugEndGameButton = new TablexiaButton(
                null,
                false,
                ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.DEBUG_CUP_BUTTON_RELEASED),
                ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.DEBUG_CUP_BUTTON_PRESSED),
                null,
                null
        );

        TablexiaComponentDialogFactory.getDialogStage().addActor(debugEndGameButton);

        debugEndGameButton.setSize(DEBUG_END_GAME_BUTTON_SIZE, DEBUG_END_GAME_BUTTON_SIZE);
        debugEndGameButton.setPosition(TablexiaComponentDialogFactory.getDialogStage().getWidth() - debugEndGameButton.getWidth(), getViewportBottomY() + (DEBUG_END_BUTTON_Y_OFFSET * getViewportHeight()));
        debugEndGameButton.setVisible(false);
    }

    @Override
    protected final void screenVisible(Map<String, String> screenState) {
        performGameVisible();

        gamePhase = GamePhase.STARTED;
        // if game is restored -> open menu and pause game
		if (screenState.size() > 0) {
            ApplicationBus.getInstance().publishAsync(new MenuControlEvent(MainMenu.class, AbstractMenu.MenuAction.OPEN, true));
        }
    }

    @Override
    protected final void screenResumed() {
        if (gamePhase == GamePhase.STARTED) {
            if (game.isStarted()) {
                Long resumeTime = GameDAO.resumeGame(game);
                Log.info(getClass(), "[DB] GAME object resumed: " + resumeTime);
            } else {
                GameDAO.startGame(game);
                Log.info(getClass(), "[DB] Started game: " + game);
            }
            performGameResumed();
        }
    }

    @Override
    protected final void screenRender(float delta) {
        gameRender(delta);
    }

    @Override
    protected final void screenAct(float delta) {
        gameAct(delta);
    }

    @Override
    protected final void screenPaused(Map<String, String> screenState) {
        if (gamePhase == GamePhase.STARTED) {
            ApplicationBus.getInstance().post(new MenuControlEvent(MainMenu.class, AbstractMenu.MenuAction.OPEN, true)).asynchronously();
            performGamePaused(screenState);
            screenState.put(GAME_STATE_GAME_ID, String.valueOf(game.getId()));
            Log.info(getClass(), "[DB] Stored game object to screen state: " + game);
            Long endTime = GameDAO.pauseGame(game);
            Log.info(getClass(), "[DB] GAME object paused: " + endTime);
        }
    }

    @Override
    protected final void screenDisposed() {
        performGameDisposed();
    }

    @Override
    protected final void prepareScreenSoundAssetNames(List<String> soundsFileNames) {
        prepareGameSoundAssetNames(soundsFileNames);
        loadSound(VICTORY_DIALOG_SHOW_SOUND);
        loadSound(RATINGSTAR1_SOUND);
        loadSound(RATINGSTAR2_SOUND);
        loadSound(RATINGSTAR3_SOUND);
    }

    protected String getGameDifficultyAtlasPath() {
        return prepareScreenAssetsPath(prepareScreenName()) + getGameDifficulty().name().toLowerCase() + ATLAS_SUFFIX;
    }

    protected String getGameDifficultyAtlasPath(GameDifficulty gameDifficulty) {
        return prepareScreenAssetsPath(prepareScreenName()) + gameDifficulty.name().toLowerCase() + ATLAS_SUFFIX;
    }


    @Override
    protected void prepareScreenAtlases(List<String> atlasesNames) {
        atlasesNames.add(GLOBAL_ATLAS_NAME);
        atlasesNames.add(getGameDifficultyAtlasPath());
        super.prepareScreenAtlases(atlasesNames);
    }

    @Override
    protected final T prepareScreenData(Map<String, String> screenState) {
        game = null;

        //load game db object from screen state
        if (screenState.size() > 0) {
            // restore from game state
            String gameIdStr = screenState.get(GAME_STATE_GAME_ID);
            game = gameIdStr != null ? GameDAO.getGameForId(Long.valueOf(gameIdStr)) : null;
        }
        if (game == null) {
            // create new
            random = TablexiaSettings.GAMES_RANDOM_SEED == null ? new TablexiaRandom() : new TablexiaRandom(TablexiaSettings.GAMES_RANDOM_SEED);
            TablexiaSettings.getInstance().setLastSeed(random.getSeed());
            GameDefinition gameDefinition = GameDefinition.getGameDefinitionForClass(AbstractTablexiaGame.this.getClass());
            GameDifficulty gameDifficulty = gameDefinition.hasTutorial() && UserDAO.isTutorialForGameDefinition(gameDefinition, getSelectedUser()) ? GameDifficulty.TUTORIAL : getGameDifficulty();
            game = GameDAO.createGame(getSelectedUser(), gameDifficulty, gameDefinition, random, UserRankManager.getInstance().getRank(getSelectedUser()));
            Log.info(getClass(), "[DB] Created new GAME object: " + game);
        } else {
            Log.info(getClass(), "[DB] Restored last GAME object: " + game);
        }

        printScreenInfo(RANDOM_SEED_SCREEN_INFO_LABEL, "" + getRandom().getSeed());
        printScreenInfo(GAME_DIFFICULTY_SCREEN_INFO_LABEL, "" + getGameDifficulty().name());
        Log.info(getClass(), "Start game data loading with: " + getRandom().toString() + " and difficulty: " + getGameDifficulty());
        return prepareGameData(screenState);
    }

    @Override
    protected String prepareScreenName() {
        String className = getClass().getName().toLowerCase();
        Pattern p = Pattern.compile("(\\w+)game");
        Matcher m = p.matcher(className);
        if (m.find()) {
            return m.group(1);
        } else {
            throw new RuntimeException("Invalid game class name! " + className);
        }
    }

    @Override
    protected String prepareScreenAssetsPath(String screenName) {
        return "game/" + screenName + "/";
    }

    @Override
    public String prepareSoundMutedQuestion() {
        return ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SOUND_MUTED_GAME_QUESTION);
    }

    @Override
    public void setScreenViewCount(long userId, String screenName, long time) {
    }

    public boolean isGameFinished() {
        return getGamePhase() == GamePhase.FINISHED;
    }

//////////////////////////// ABSTRACT TABLEXIA GAME LIFECYCLE

    private final void performGameLoaded(Map<String, String> gameState) {
        Log.info(getClass(), "[ ------- Game Loaded ------- ]");
        gameLoaded(gameState);
        prepareGameTrophies();

        if(debugEndGameButton != null)
            showDebugEndButton();
    }

    private void showDebugEndButton() {
        debugEndGameButton.removeListener();
        debugEndGameButton.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(getGamePhase() == GamePhase.STARTED) {
                    forceGameEnd(GameResultDefinition.THREE_STAR);
                    debugEndGameButton.setDisabled();
                }
            }
        });
        debugEndGameButton.setEnabled();
        debugEndGameButton.setVisible(true);
    }

    public final void performGameVisible() {
        Log.info(getClass(), "[ ------- Game Visible ------- ]");
        gameVisible();
    }

    private final void performGamePaused(Map<String, String> gameState) {
        Log.info(getClass(), "[ ------- Game Paused ------- ]");
        gamePaused(gameState);
    }

    private final void performGameEnd() {
        Log.info(getClass(), "[ ------- Game End ------- ]");
        gameEnd();

        if(debugEndGameButton != null)
            debugEndGameButton.setVisible(false);
    }

    private final void performGameDisposed() {
        performGameEnd();
        Log.info(getClass(), "[ ------- Game Disposed ------- ]");
        gameDisposed();
    }

    private final void performGameResumed() {
        Log.info(getClass(), "[ ------- Game Resumed ------- ]");
        gameResumed();
    }

    private final void prepareGameTrophies() {
        List<ITrophyDefinition> trophies = TrophyHelper.getPossibleTrophiesForGame(GameDefinition.getGameDefinitionForGameNumber(getGame().getGameNumber()));
        for (ITrophyDefinition trophy : trophies) {
            hasTrophies.put(trophy, trophy.hasTrophy(getSelectedUser()));
        }
    }

    protected void gameLoaded(Map<String, String> gameState) {
    }

    protected void gameVisible() {
    }

    protected void gameResumed() {
    }

    protected void gameRender(float delta) {
    }

    protected void gameAct(float delta) {
    }

    protected void gamePaused(Map<String, String> gameState) {
    }

    protected void gameEnd() {
    }

    protected void gameDisposed() {
    }

    protected T prepareGameData(Map<String, String> gameState) {
        return null;
    }

    @Override
    public TextureRegion getScreenTextureRegion(String regionName) {
        return getScreenTextureRegion(regionName, null);
    }

    @Override
    public TextureRegion getScreenTextureRegion(String regionName, Integer index) {
        TextureRegion textureRegion = getTextureRegionForAtlas(getGameDifficultyAtlasPath(), regionName, index, false);
        if (textureRegion == null) {
            textureRegion = super.getScreenTextureRegion(regionName, index);
        }
        return textureRegion;
    }

    public TextureRegion getGameGlobalTextureRegion(String regionName) {
        return getTextureRegionForAtlas(GLOBAL_ATLAS_NAME, regionName);
    }

    protected void prepareGameSoundAssetNames(List<String> soundsFileNames) {
    }


//////////////////////////// ABSTRACT TABLEXIA GAME

    private final float             DIALOG_HEIGHT               = 460;
    private final float             DIALOG_WIDTH                = 500;

    private final float             TROPHY_DIALOG_WIDTH         = 0.18f;
    private final int               TROPHY_DIALOG_MAX_COUNT     = 4;
    private final float             TROPHY_DIALOG_IMAGE_SCALE   = 1f;
    private final float             TROPHY_DIALOG_MAX_WIDTH     = 0.8f; //Percent of dialogs width
    private final float             TROPHY_DIALOG_MOBILE_SCALE  = 0.7f;
    private final float             TROPHY_DIALOG_HEIGHT_SCALE  = 1.2f;

    private static final float      VICTORY_DIALOG_TOP_PADDING_HEIGHT_RATIO     = 1f / 20;
    private static final float      VICTORY_DIALOG_TITLE_IMAGE_WIDTH_RATIO      = 2.5f / 6;
    private static final float      VICTORY_DIALOG_TITLE_IMAGE_HEIGHT_RATIO     = 1f / 6;
    private static final Scaling    VICTORY_DIALOG_TITLE_IMAGE_SCALING          = Scaling.fill;

    private static final ApplicationFontManager.FontType DEFAULT_REGULAR_FONT = ApplicationFontManager.FontType.REGULAR_12;
    private static final ApplicationFontManager.FontType DEFAULT_BOLD_FONT   = ApplicationFontManager.FontType.BOLD_14;
    private static final ApplicationFontManager.FontType MOBILE_BOLD_FONT    = ApplicationFontManager.FontType.BOLD_12;
    
    private boolean isUnderThreshold = ComponentScaleUtil.isUnderThreshold();

    public enum SummaryImage {

        STATS(VICTORY_DIALOG_SUMMARY_ICON_STATS),
        TIME(VICTORY_DIALOG_SUMMARY_ICON_TIME);

        private String imageName;

        SummaryImage(String imageName) {
            this.imageName = imageName;
        }

        public String getImageName() {
            return imageName;
        }
    }

    public static class SummaryMessage {

        private final SummaryImage summaryImage;
        private final String text;

        public SummaryMessage(SummaryImage summaryImage, String text) {
            this.summaryImage = summaryImage;
            this.text = text;
        }

        public SummaryImage getSummaryImage() {
            return summaryImage;
        }

        public String getText() {
            return text;
        }
    }

    private void goToGamePage() {
        GameDefinition gameDefinition = GameDefinition.getGameDefinitionForClass(getClass());
        if(gameDefinition == null) {
            Log.info(getClass(), "Cannot get a GameDefinition for class: " + getClass());
            return;
        }

        ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(GamePageScreen.class, TablexiaApplication.ScreenTransaction.FADE, null, gameDefinition)).asynchronously();
    }

    public TablexiaRandom getRandom() {
        if (random == null) {
            random = new TablexiaRandom(game.getRandomSeed());
        }
        return random;
    }

    public GameDifficulty getGameDifficulty() {
        return game == null ? UserDifficultySettingsDAO.getUserSettingsByGame(getSelectedUser().getId(), GameDefinition.getGameDefinitionForClass(AbstractTablexiaGame.this.getClass()).getGameNumber()) : GameDifficulty.getGameDifficultyForDifficultyNumber(game.getGameDifficulty());
    }

    protected Game getGame() {
        return game;
    }

    protected void showGameResultDialog() {
        List<TablexiaDialogComponentAdapter> adapters = new ArrayList<TablexiaDialogComponentAdapter>();

        //Result Banner
        adapters.add(new AdaptiveSizeDialogComponent());
        adapters.add(new CenterPositionDialogComponent());
        adapters.add(new DimmerDialogComponent());
        adapters.add(new ViewportMaximumSizeComponent());

        adapters.add(new FixedSpaceContentDialogComponent(VICTORY_DIALOG_TOP_PADDING_HEIGHT_RATIO));
        adapters.add(new ImageContentDialogComponent(new Image(getGameGlobalTextureRegion(VICTORY_DIALOG_RESULTSBANNER)), VICTORY_DIALOG_TITLE_IMAGE_SCALING, VICTORY_DIALOG_TITLE_IMAGE_WIDTH_RATIO, VICTORY_DIALOG_TITLE_IMAGE_HEIGHT_RATIO));

        //Cups
        final GameResult gameResult = GameDAO.getGameResult(game);
        final Image[] cupsImages = new Image[gameResult.getStarCount()];

        if(getGameDifficulty().hasScore()) {
            TablexiaDialogComponentAdapter cups = new TablexiaDialogComponentAdapter() {
                private static final int CUPS_TABLE_SPACING = 4;
                private static final float CUP_WIDTH_RATIO = 1f / 8;
                private static final float CUP_HEIGHT_RATIO = 1f / 6;

                private List<Cell> cupCells;

                @Override
                public void prepareContent(Cell content) {
                    Table table = new Table();
                    table.defaults().space(CUPS_TABLE_SPACING);

                    cupCells = new ArrayList<Cell>();
                    content.setActor(table);

                    for (int i = 0; i < GameResult.values()[GameResult.values().length - 1].getStarCount(); i++) {
                        Image cup;
                        cup = new Image(getGameGlobalTextureRegion(VICTORY_DIALOG_RATINGSTAR_DISABLED));
                        cup.setScaling(Scaling.fit);


                        //Saves the cups which are changed during the animation
                        if (i < gameResult.getStarCount()) {
                            cupsImages[i] = cup;
                        }
                        cupCells.add(table.add(cup));
                    }

                    resize();
                }

                @Override
                public void sizeChanged() {
                    resize();
                }

                private void resize() {
                    for (Cell cell : cupCells) {
                        cell.width(getDialog().getWidth() * CUP_WIDTH_RATIO).height(getDialog().getHeight() * CUP_HEIGHT_RATIO);
                    }
                }
            };
            adapters.add(new ResizableSpaceContentDialogComponent());
            adapters.add(cups);
        }

        //ProtocolText
        String text = "";
        if(getGameDifficulty() == GameDifficulty.TUTORIAL) {
            text = ApplicationTextManager.getInstance().getText(VICTORY_DIALOG_TUTORIAL_TEXT);
        } else {
            String textKey = getTextKeyForGameResult(gameResult);
            text = textKey != null ? getText(textKey) : "";
        }

        adapters.add(new ResizableSpaceContentDialogComponent());
        adapters.add(new TextContentDialogComponent(text, Align.center, VICTORY_DIALOG_TEXT_PADDING));

        //Game Summary
        final List<SummaryMessage> summaryMessages = getSummaryMessageForGameResult(game);
        TablexiaDialogComponentAdapter gameSummaryComponent = new TablexiaDialogComponentAdapter() {

            private static final int    SEPARATOR_WIDTH                 = 12;
            private static final float  CUP_WIDTH_RATIO                 = 1f / 20;
            private static final float  CUP_HEIGHT_RATIO                = 1f / 15;
            private static final float  CUP_PADDING_RATIO               = 1f / 80;

            public ArrayList<Cell<Image>> summaryImageCells;

            private ApplicationFontManager.FontType fontType = ApplicationFontManager.FontType.REGULAR_14;
            private ApplicationFontManager.FontType actualFontType = ApplicationFontManager.FontType.REGULAR_14;

            private List<TablexiaLabel> textLabels = new ArrayList<TablexiaLabel>();
            private List<Image> images = new ArrayList<Image>();

            private boolean isScaled = false;

            @Override
            public void adaptiveSizeThresholdChanged() {
                if (ComponentScaleUtil.isUnderThreshold()) {
                    if (!isScaled) {
                        actualFontType = ApplicationFontManager.FontType.getProximateFontTypeForSize(fontType.getSize() + TextContentDialogComponent.FONT_SIZE_RESIZE_THRESHOLD, fontType.isBold());
                        isScaled = true;
                    }
                } else {
                    if (isScaled) {
                        actualFontType = fontType;
                        isScaled = false;
                    }
                }
                for (TablexiaLabel label: textLabels) {
                    label.setTablexiaLabelStyle(new TablexiaLabel.TablexiaLabelStyle(actualFontType, VICTORY_DIALOG_SUMMARY_FONT_COLOR));
                }
            }

            @Override
            public void beforeDraw() {
                ApplicationFontManager.getInstance().getDistanceFieldFont(actualFontType).getData().markupEnabled = true;
            }

            @Override
            public void afterDraw() {
                ApplicationFontManager.getInstance().getDistanceFieldFont(actualFontType).getData().markupEnabled = false;
            }

            @Override
            public void prepareContent(Cell content) {
                Table table = new Table();
                content.setActor(table);

                summaryImageCells = new ArrayList<Cell<Image>>();

                Iterator<SummaryMessage> iterator = summaryMessages.iterator();

                while(iterator.hasNext()) {
                    SummaryMessage message = iterator.next();

                    Image image = new Image(getGameGlobalTextureRegion(message.getSummaryImage().imageName));
                    image.setScaling(Scaling.fit);
                    images.add(image);
                    summaryImageCells.add(table.add(image).align(Align.center));

                    TablexiaLabel labelMsg = new TablexiaLabel(message.getText(), new TablexiaLabel.TablexiaLabelStyle(actualFontType, VICTORY_DIALOG_SUMMARY_FONT_COLOR), true);
                    textLabels.add(labelMsg);
                    table.add(labelMsg).align(Align.center);
                    table.setName(RESULT_TABLE);
                    //Add separator if there is another summary message
                    if(iterator.hasNext()) {
                        Actor separator = new Actor();
                        separator.setWidth(SEPARATOR_WIDTH);
                        table.add(separator);
                    }
                }
                resize();
            }

            @Override
            public void sizeChanged() {
                resize();
            }

            private void resize() {
                for (Cell<Image> cell: summaryImageCells) {
                    cell.width(getDialog().getWidth() * CUP_WIDTH_RATIO).height(getDialog().getHeight() * CUP_HEIGHT_RATIO).pad(getDialog().getWidth() * CUP_PADDING_RATIO);
                }
            }
        };
        adapters.add(new ResizableSpaceContentDialogComponent());
        adapters.add(gameSummaryComponent);

        playAgainButtonClicked = false;

        adapters.add(new ResizableSpaceContentDialogComponent());

        final UserRankManager.RankProgress rankProgress = UserRankManager.getInstance().getGameRankProgress(getSelectedUser(), GameDefinition.getGameDefinitionForGameNumber(game.getGameNumber()));
        final TablexiaProgressBar progressBar = new TablexiaProgressBar(
                ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.PROGRESS_BAR),
                ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.PROGRESS_BAR_FILL)
        );
        progressBar.setName(VICTORY_PROGRESS_BAR);
        progressBar.addListener(new ClickListenerWithSound() {
            @Override
            public void onClick(InputEvent event, float x, float y) {
                hideDialogsAndChangeScreen(ProfileScreen.class, TablexiaApplication.ScreenTransaction.FADE);
            }
        });

        adapters.add(new TablexiaDialogComponentAdapter() {
            private static final float  PROGRESS_BAR_WIDTH  = 0.8f;

            Cell<TablexiaProgressBar> progressCell;

            @Override
            public void sizeChanged() {
                progressCell.expand();
                progressCell.getActor().setWidth(getDialog().getInnerWidth() * PROGRESS_BAR_WIDTH);
            }

            @Override
            public void prepareContent(Cell content) {
                progressBar.setSmooth(false);
                progressBar.setPercent(rankProgress.getPercentDone());

                content.setActor(progressBar);

                progressCell = content;
            }
        });

        //Buttons
        adapters.add(new ResizableSpaceContentDialogComponent());
        adapters.add(new TwoButtonContentDialogComponent(
                ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.VICTORYSCREEN_BUTTON_FINISH),
                ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.VICTORYSCREEN_BUTTON_REPLAY),
                StandardTablexiaButton.TablexiaButtonType.BLUE,
                StandardTablexiaButton.TablexiaButtonType.GREEN,
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        clearReceivedTrophyDialogs();
                        hideVictoryDialogAndStars();
                        goToGamePage();
                    }
                },
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        clearReceivedTrophyDialogs();
                        hideVictoryDialogAndStars();
                        startNewGame();

                        playAgainButtonClicked = true;

                        if(getMusic(getSoundNameForGameResult(gameResult)).isPlaying()) {
                            MusicUtil.fadeOut(getMusic(getSoundNameForGameResult(gameResult)), VICTORY_SPEECH_FADE_OUT_TIME);
                        }
                    }
                }));
        adapters.add(new FixedSpaceContentDialogComponent());
        adapters.add(new TablexiaDialogComponentAdapter() {
            private boolean sizeChanged = true;

            @Override
            public void beforeDraw() {
                if(sizeChanged) {
                    positionReceivedTrophyDialogs(
                            getDialog().getInnerLeftX(),
                            getDialog().getInnerBottomY(),
                            getDialog().getInnerWidth(),
                            getDialog().getInnerHeight()
                    );
                    sizeChanged = false;
                }
            }

            @Override
            public void sizeChanged() {
                sizeChanged = true;
            }

            @Override
            public void show() {
                super.show();
                showReceivedTrophyDialogs();

                triggerScenarioStepEvent(EVENT_VICTORY_DIALOG_READY);
            }
        });

        //Create the dialog
        victoryDialog = TablexiaComponentDialogFactory.getInstance().createDialog(
                adapters.toArray(new TablexiaDialogComponentAdapter[]{}));

        boolean gotNewTrophy = handleTrophies();
        victoryDialog.setName(VICTORY_DIALOG);
        victoryDialog.show(DIALOG_WIDTH, DIALOG_HEIGHT);

		victoryDialog.setComponentsSizeComputedCallback(new Runnable() {
			@Override
			public void run() {
                //victory dialog show sound
                getSound(VICTORY_DIALOG_SHOW_SOUND, false).play();

                playDialogResultSound(gameResult);

                if(getGameDifficulty().hasScore()) {
                    animateGameResultCups(gameResult, cupsImages);
                    animateProgressBarAndExperienceStars(cupsImages, GameDifficulty.getByGame(game), rankProgress, progressBar);

                    if(UserRankManager.getInstance().addGameToRankData(getSelectedUser(), game, gotNewTrophy)){
                        for(TablexiaDialogComponentAdapter component : victoryDialog.getDialogComponents()){
                            if(component instanceof TwoButtonContentDialogComponent){
                                TwoButtonContentDialogComponent buttons = (TwoButtonContentDialogComponent) component;
                                buttons.setButtonsDisabled();
                                break;
                            }
                        }
                    }
                    ApplicationBus.getInstance().post(new GameMenuItemGroup.RefreshMainMenuProgressBarsEvent()).asynchronously();
                }
			}
		});
    }

    private void playDialogResultSound(GameResult gameResult) {
        //victory speech
        final String soundNameForGameResult = getSoundNameForGameResult(gameResult);
        if (soundNameForGameResult != null) {
            addAction(sequence(delay(VICTORY_DIALOG_SHOW_SOUND_DELAY), run(new Runnable() {
                @Override
                public void run() {
                    Music victorySpeech = getMusic(soundNameForGameResult);
                    if (victorySpeech != null && !playAgainButtonClicked) {
                        victorySpeech.play();
                    }
                }
            })));
        }
    }

    private void animateGameResultCups(GameResult gameResult, Image[] cups) {
        // rating cups
        for (int i = 0; i < cups.length; i++) {
            final Image cup = cups[i];
            final int index = i + 1;
            cup.setName(IMAGE_CUBS + index);
            addAction(sequence(delay((index) * VICTORY_DIALOG_RATINGSTARS_DELAY), run(new Runnable() {
                @Override
                public void run() {
                    cup.setDrawable(new Image(getGameGlobalTextureRegion(VICTORY_DIALOG_RATINGSTAR_ENABLED)).getDrawable());
                    getSound(GameResult.getGameResultForStartCount(index).getStartSoundName(), false).play();

                    //for test
                    if(index == cups.length){
                        AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_ANIMATION_FINISHED);
                    }
                }
            })));
        }
    }

    private boolean handleTrophies() {
        boolean gotTrophy = false;
        List<ITrophyDefinition> allTrophies = TrophyHelper.getPossibleTrophiesForGame(
                GameDefinition.getGameDefinitionForGameNumber(getGame().getGameNumber())
        );
        List<ITrophyDefinition> received = new ArrayList<ITrophyDefinition>();
        for (ITrophyDefinition trophy : allTrophies) {
            if ((!hasTrophies.containsKey(trophy) || !hasTrophies.get(trophy)) && trophy.hasTrophy(getSelectedUser())) {
                Log.debug(this.getClass(), "has new trophy: " + trophy.getTrophyName());
                received.add(trophy);
                gotTrophy = true;
            }
        }

        for(ITrophyDefinition trophy : received) {
            addReceivedTrophyDialog(trophy);
        }
        return gotTrophy;
    }

    private void showReceivedTrophyDialogs() {
        float sceneWidh = TablexiaSettings.getSceneWidth(TablexiaComponentDialogFactory.getDialogStage());
        float dialogWidth = isUnderThreshold ? TROPHY_DIALOG_WIDTH * TROPHY_DIALOG_MOBILE_SCALE : TROPHY_DIALOG_WIDTH;
        dialogWidth *= sceneWidh;
        float dialogHeight = dialogWidth * TROPHY_DIALOG_HEIGHT_SCALE;
        for (TablexiaComponentDialog dialog : receivedTrophyDialogs) {
            dialog.show(dialogWidth, dialogHeight);
        }
    }

    private void positionReceivedTrophyDialogs(float dialogX, float dialogY, float dialogWidth, float dialogHeight) {
        int trophyCount = receivedTrophyDialogs.size() > TROPHY_DIALOG_MAX_COUNT ? TROPHY_DIALOG_MAX_COUNT : receivedTrophyDialogs.size();
        for (int i = 0; i < trophyCount; i++) {
            float x = TrophyDialogPositionDefinition.values()[i].getX(dialogX,dialogWidth, receivedTrophyDialogs.get(i).getInnerWidth(), isUnderThreshold);
            float y = TrophyDialogPositionDefinition.values()[i].getY(dialogY,dialogHeight, receivedTrophyDialogs.get(i).getInnerHeight(), isUnderThreshold);
            receivedTrophyDialogs.get(i).setPosition(x,y);
        }
    }

    private void addReceivedTrophyDialog(final ITrophyDefinition trophyDefinition) {
        TextureRegion trophyTexture = getGameGlobalTextureRegion(TrophyHelper.getFullPath(trophyDefinition.getTrophyNameKey(), TrophyHelper.TrophyType.SMALL));
        String name = trophyDefinition.getTrophyName();

        ArrayList<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>();
        components.add(new AdaptiveSizeDialogComponent());
        components.add(new TouchCloseDialogComponent(new TouchCloseDialogComponent.TouchListener() {
            @Override
            public boolean dialogTouched(float x, float y, TablexiaComponentDialog dialog) {
                //AABB intersect test - WhetherA or not user clicked on the dialog
                if( x > dialog.getInnerLeftX() &&
                    x < dialog.getInnerLeftX() + dialog.getInnerWidth() &&
                    y > dialog.getInnerBottomY() &&
                    y < dialog.getInnerBottomY() + dialog.getInnerHeight()
                ) {
                    //TODO - Use invisible TablexiaButton ???
                    ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.BUTTON_CLICKED).play();

                    HashMap<String, String> screenState = new HashMap<String, String>();
                    screenState.put(HallOfFameScreen.SCROLL_TO_TROPHY_KEY, trophyDefinition.name());
                    ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(HallOfFameScreen.class, TablexiaApplication.ScreenTransaction.FADE, screenState)).asynchronously();

                    hideVictoryDialogAndStars();
                    clearReceivedTrophyDialogs();
                }
                return false;
            }
        }));

        final ImageContentDialogComponent trophyImageComponent;

        components.add(new FixedSpaceContentDialogComponent());
        if (!isUnderThreshold) {
            //desktop/tablet layout
            String title = ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.VICTORYSCREEN_NEW_TROPHY);
            String desc = trophyDefinition.getTrophyDescription();

            components.add(new TextContentDialogComponent(title, DEFAULT_BOLD_FONT, true, true));
            components.add(new ResizableSpaceContentDialogComponent());
            components.add(trophyImageComponent = new ImageContentDialogComponent(trophyTexture, Scaling.fit, TROPHY_DIALOG_IMAGE_SCALE));
            components.add(new ResizableSpaceContentDialogComponent());
            components.add(new TextContentDialogComponent(name, DEFAULT_REGULAR_FONT, true, true));
            components.add(new ResizableSpaceContentDialogComponent());
            components.add(new TextContentDialogComponent(desc, DEFAULT_REGULAR_FONT, Color.GRAY));
        } else {
            //mobile layout
            components.add(trophyImageComponent = new ImageContentDialogComponent(trophyTexture, Scaling.fit, TROPHY_DIALOG_IMAGE_SCALE));
            components.add(new ResizableSpaceContentDialogComponent());
            components.add(new TextContentDialogComponent(name, MOBILE_BOLD_FONT, true, true));
            components.add(new ResizableSpaceContentDialogComponent());
        }

        components.add(new FixedSpaceContentDialogComponent());

        final TablexiaComponentDialog tcd = TablexiaComponentDialogFactory.getInstance().createDialog(
                TablexiaComponentDialog.TablexiaDialogType.DIALOG_SQUARE,
                components.toArray(new TablexiaDialogComponentAdapter[]{}));
        receivedTrophyDialogs.add(tcd);

        //Makes sure that trophy image is not wider than TROPHY_DIALOG_MAX_WIDTH
        tcd.setComponentsSizeComputedCallback(new Runnable() {
            @Override
            public void run() {
                Cell<Image> trophyImageCell = trophyImageComponent.getImageCell();

                float maxWidth = tcd.getInnerWidth() * TROPHY_DIALOG_MAX_WIDTH;

                if(trophyImageCell.getActorWidth() > maxWidth) {
                    float ratio = trophyImageCell.getActorHeight() / trophyImageCell.getActorWidth();
                    trophyImageCell.width(maxWidth);
                    trophyImageCell.height(maxWidth * ratio);
                }
            }
        });
    }

    private void hideVictoryDialogAndStars() {
        if(victoryDialog != null) victoryDialog.hide();

        if(experienceStars != null) {
            for(ExperienceStar star : experienceStars) star.hide();
        }
    }

    private void clearReceivedTrophyDialogs() {
        if(receivedTrophyDialogs == null) return;

        for (TablexiaComponentDialog dialog : receivedTrophyDialogs) {
            if(dialog != null) dialog.hide();
        }

        receivedTrophyDialogs.clear();
    }

    public void gameComplete() {
        endGame();
        showGameResultDialog();
        ApplicationBus.getInstance().publishAsync(new AbstractTablexiaGame.GameFinishedEvent(EVENT_FINISH_GAME));
    }

    public interface ExperienceStarListener {
        void onStarPathCompleted(ExperienceStar experienceStar);
    }

    public static class ExperienceStar extends Image {
        private static final float FADE_OUT_DURATION = 0.5f;

        private Vector2 relativeEndPosition;
        private ExperienceStarListener listener;

        public ExperienceStar(float starWidth, Vector2 relativeEndPosition) {
            super(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.RANK_UP_STAR));

            this.setSize(starWidth, starWidth * (getWidth() / getHeight()));
            this.setOrigin(Align.center);
            this.relativeEndPosition = relativeEndPosition;

            this.addAction(Actions.alpha(0));
        }

        private Vector2[] prepareActualPath() {
            int arraySize = EXPERIENCE_STAR_PATH.length;
            if(this.relativeEndPosition != null) arraySize++;

            Vector2[] actualPath = new Vector2[arraySize];

            for(int i = 0; i < EXPERIENCE_STAR_PATH.length; i++) {
                actualPath[i] = EXPERIENCE_STAR_PATH[i];
            }

            if(this.relativeEndPosition != null)
                actualPath[arraySize - 1] = this.relativeEndPosition;

            return actualPath;
        }

        public void startAnimation(float delay, float fadeIn, float duration, Interpolation interpolation) {
            Vector2[] path = prepareActualPath();

            Bezier<Vector2> bezier = new Bezier<Vector2>(path);
            MoveAlongAction moveAlongAction = new MoveAlongAction(bezier, duration);
            moveAlongAction.setInterpolation(interpolation);

            ParallelAction moveRotateScaleAction = new ParallelAction(
                new WhileAction(
                    moveAlongAction,
                    Actions.rotateBy(360, EXPERIENCE_STAR_PATH_ROTATION)
                ),
                new SequenceAction(
                    Actions.scaleBy( EXPERIENCE_STAR_PATH_SCALE,  EXPERIENCE_STAR_PATH_SCALE, duration / 2f),
                    Actions.scaleBy(-EXPERIENCE_STAR_PATH_SCALE, -EXPERIENCE_STAR_PATH_SCALE, duration / 2f)
                )
            );

            final ExperienceStar thisExperienceStar = this;
            addAction(
                new SequenceAction(
                    Actions.delay(delay),
                    Actions.fadeIn(fadeIn),
                    moveRotateScaleAction,
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            if(listener != null)
                                listener.onStarPathCompleted(thisExperienceStar);
                        }
                    })
                )
            );
        }

        public void setStarListener(ExperienceStarListener starListener) {
            this.listener = starListener;
        }

        public void hide() {
            this.addAction(new SequenceAction(Actions.fadeOut(FADE_OUT_DURATION), Actions.removeActor()));
        }
    }
    private void animateProgressBarAndExperienceStars(Image[] cupsImages, final GameDifficulty gameDifficulty, final UserRankManager.RankProgress rankProgress, final TablexiaProgressBar progressBar) {
        final UserRankManager.RankProgress gameRankProgress = new UserRankManager.RankProgress(rankProgress.getMinXP(), rankProgress.getCurrXP(), rankProgress.getNextXP());
        final UserRankManager.RankProgress tempRankProgress = new UserRankManager.RankProgress(rankProgress.getMinXP(), rankProgress.getCurrXP(), rankProgress.getNextXP());

        Vector2 progressBarStagePosition = progressBar.localToStageCoordinates(new Vector2(0, progressBar.getHeight() / 2));
        progressBar.setSmooth(true);
        progressBar.setSpeed(PROGRESS_BAR_SPEED);

        int starCountPerCup = gameDifficulty.getExperiencePointsMultiplier();

        if(experienceStars == null) experienceStars = new LinkedList<ExperienceStar>();
        experienceStars.clear();

        //Listener for all stars...
        final ExperienceStarListener starListener = new ExperienceStarListener() {
            @Override
            public void onStarPathCompleted(ExperienceStar experienceStar) {
                experienceStar.addAction(
                    new SequenceAction(
                        new ParallelAction(
                                new SequenceAction(
                                        Actions.delay(EXPERIENCE_STAR_FADE_OUT_DELAY),
                                        Actions.fadeOut(EXPERIENCE_STAR_FADE_OUT, EXPERIENCE_STAR_FADE_OUT_INTERPOLATION)
                                ),
                                Actions.scaleBy(EXPERIENCE_STAR_FADE_OUT_SCALE, EXPERIENCE_STAR_FADE_OUT_SCALE, EXPERIENCE_STAR_FADE_OUT_SCALE_TIME, EXPERIENCE_STAR_FADE_OUT_INTERPOLATION)
                        ),
                        Actions.removeActor()
                    )
                );

                //TODO - different sound...
//                ApplicationInternalSoundManager.getInstance().getSound(ApplicationInternalSoundManager.ALERT).play();

                //Updates progress bar
                gameRankProgress.setCurrXP(gameRankProgress.getCurrXP() + 1);
                progressBar.setPercent(gameRankProgress.getPercentDone());


            }
        };

        //RankProgress to compute stars final destination on the progress bar
        GameDefinition gd = GameDefinition.getGameDefinitionForClass(getClass());
        if(gd == null) throw new RuntimeException("Couldn't find game definition for class: " + getClass());

        //Iterate through all the achieved cups
        for(int cupsAchievedIndex = 0; cupsAchievedIndex < cupsImages.length; cupsAchievedIndex++) {
            float cupWidth = cupsImages[cupsAchievedIndex].getWidth();
            float starWidth = cupWidth * EXPERIENCE_STAR_WIDTH_TO_CUP;

            //Star position for particular cup
            Vector2 starPosition = cupsImages[cupsAchievedIndex].localToStageCoordinates(
                    new Vector2(
                            EXPERIENCE_STAR_X_TO_CUP * cupsImages[cupsAchievedIndex].getWidth(),
                            EXPERIENCE_STAR_Y_TO_CUP * cupsImages[cupsAchievedIndex].getHeight()
                    )
            );

            for(int starIndex = 0; starIndex < starCountPerCup; starIndex++) {
                float gamePercent = tempRankProgress.getPercentDone();
                if(gamePercent >= 1) return;  //ProgressBar is already at (100%)

                //Final destination point for stars in stars coordinate system
                Vector2 lastPoint = new Vector2(
                        progressBarStagePosition.x + progressBar.getFillOffsetX() + (progressBar.getWidth() - 2*progressBar.getFillOffsetX()) * gamePercent - starPosition.x,
                        progressBarStagePosition.y - starPosition.y
                );

                //Actual star
                ExperienceStar experienceStar = new ExperienceStar(starWidth, lastPoint);

                experienceStar.setTouchable(Touchable.disabled);
                experienceStar.setPosition(starPosition.x - experienceStar.getWidth() / 2f, starPosition.y - experienceStar.getHeight() / 2f);
                experienceStar.setStarListener(starListener);

                //Starting animation
                float cupDelay = cupsAchievedIndex * starCountPerCup * EXPERIENCE_STAR_DELAY;
                experienceStar.startAnimation(cupDelay + (EXPERIENCE_STAR_DELAY * starIndex), EXPERIENCE_STAR_FADE_IN, EXPERIENCE_STAR_PATH_DURATION, EXPERIENCE_STAR_FADE_IN_INTERPOLATION);

                tempRankProgress.setCurrXP(tempRankProgress.getCurrXP() + 1);
                TablexiaComponentDialogFactory.getDialogStage().addActor(experienceStar);

                experienceStars.add(experienceStar);
            }
        }
    }

    public void onRankUp(UserRankManager.UserRank previousRank, UserRankManager.UserRank newRank) {
        final Stage dialogStage = TablexiaComponentDialogFactory.getDialogStage();
        rankAnimation = RankAnimation.createRankAnimation(
                dialogStage,
                previousRank,
                newRank
        );

        //Clicked the Badge...
        rankAnimation.setNewBadgeEventListener(new ClickListenerWithSound() {
            @Override
            public void onClick(InputEvent event, float x, float y) {
                hideDialogsAndChangeScreen(HallOfFameScreen.class, TablexiaApplication.ScreenTransaction.FADE);
            }
        });

        //Clicked the rank up animation
        rankAnimation.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                rankAnimation.hide();
            }
        });

        getStage().addAction( new SequenceAction(
            Actions.delay(ANIMATION_DELAY),
            Actions.run(new Runnable() {
                @Override
                public void run() {
                    dialogStage.addActor(rankAnimation);
                    rankAnimation.show();
                }}),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        for(TablexiaDialogComponentAdapter component : victoryDialog.getDialogComponents()){
                            if(component instanceof TwoButtonContentDialogComponent){
                                TwoButtonContentDialogComponent buttons = (TwoButtonContentDialogComponent) component;
                                buttons.setButtonsEnabled();
                                break;
                            }
                        }
                    }
                })
        ));
    }

    public void onAllTrophies(boolean rankUpNext, UserRankManager.UserRank previousRank, UserRankManager.UserRank newRank) {
        final Stage dialogStage = TablexiaComponentDialogFactory.getDialogStage();
        allTrophiesAnimation = AllTrophiesAnimation.createAllTrophyAnimation(dialogStage);

        allTrophiesAnimation.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(rankUpNext){
                    allTrophiesAnimation.hide();
                    onRankUp(previousRank, newRank);
                }
                else allTrophiesAnimation.hide();
            }
        });


        getStage().addAction( new SequenceAction(
                Actions.delay(ANIMATION_DELAY),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        dialogStage.addActor(allTrophiesAnimation);
                        allTrophiesAnimation.show();
                        AbstractTablexiaScreen.triggerScenarioStepEvent(ALL_TROPHY_ANIMATION_SHOWN);
                    }}),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        if(!rankUpNext) {
                            for (TablexiaDialogComponentAdapter component : victoryDialog.getDialogComponents()) {
                                if (component instanceof TwoButtonContentDialogComponent) {
                                    TwoButtonContentDialogComponent buttons = (TwoButtonContentDialogComponent) component;
                                    buttons.setButtonsEnabled();
                                    break;
                                }
                            }
                        }
                    }
                })
        ));
    }

    private void hideDialogsAndChangeScreen(Class<? extends AbstractTablexiaScreen<?>> screen, TablexiaApplication.ScreenTransaction screenTransaction) {
        ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(screen, screenTransaction)).asynchronously();

        hideVictoryDialogAndStars();
        clearReceivedTrophyDialogs();
    }

    @Handler
    public void onUserRankUpEvent(final UserRankManager.UserRankUpEvent userRankUpEvent) {
        onRankUp(userRankUpEvent.getPreviousRank(), userRankUpEvent.getNewRank());
    }

    @Handler
    public void onAllTrophiesEvent(final UserRankManager.AllTrophiesEvent allTrophiesEvent) {
        onAllTrophies(false, null, null);
    }

    @Handler
    public void onAllTrophiesAndRankUpEvent(final UserRankManager.AllTrophiesAndUserRankUpEvent allTrophiesAnduserRankUpEvent) {
        onAllTrophiesAndRankUp(allTrophiesAnduserRankUpEvent.getPreviousRank(), allTrophiesAnduserRankUpEvent.getNewRank());
    }

    private void onAllTrophiesAndRankUp(UserRankManager.UserRank previousRank, UserRankManager.UserRank newRank) {
        onAllTrophies(true, previousRank, newRank);
    }

    public void endGame() {
		gamePhase = GamePhase.FINISHED;
        GameDAO.endGame(game);
        waitForActualGameScore();
		Log.info(getClass(), "[DB] Ended game: " + game);

		ApplicationBus.getInstance().publishAsync(new StartIncrementalSynchronizationEvent(getSelectedUser().getId()));

    }

    private void forceGameEnd(GameResultDefinition gameResult) {
        List<GameScore> gameScores = GameDefinition.getGameDefinitionForClass(getClass()).getCurrentGameScoreResolver().getExampleScoreForGameResult(getGameDifficulty().getDifficultyDefinition(), gameResult);
        setGameScores(gameScores);
        waitForActualGameScore();

        onForceGameEnd(gameResult);
    }

    protected void onForceGameEnd(GameResultDefinition gameResult) {
        endGame();
        showGameResultDialog();
    }

    public void startNewGame() {
		performHideTransaction(new Runnable() {
            @Override
            public void run() {
                //remove old screen state
                TablexiaStorage.getInstance().resetScreenState(AbstractTablexiaGame.this);
                inGameLoading = true;
                performGameEnd();
                startLoading();
            }
        });
    }

    protected abstract List<SummaryMessage> getSummaryMessageForGameResult(Game game);

    protected abstract String getTextKeyForGameResult(GameResult gameResult);

    protected abstract String getSoundNameForGameResult(GameResult gameResult);


//////////////////////////// HIDE/SHOW TRANSACTIONS

    private Object screenDimmerHandler;

    protected void performHideTransaction(final Runnable onHide) {
        screenDimmerHandler = new Object() {

            @Handler
            public void handleScreenDimmerHiddenEvent(TablexiaApplication.ScreenDimmerShownEvent screenDimmerShownEvent) {
                ApplicationBus.getInstance().unsubscribe(screenDimmerHandler);
                screenDimmerHandler = null;
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        onHide.run();
                    }
                });
            }

        };
        ApplicationBus.getInstance().subscribe(screenDimmerHandler);
        ApplicationBus.getInstance().post(new DimmerControlEvent(true)).asynchronously();
    }

    protected void performShowTransaction(final Runnable onShow) {
        screenDimmerHandler = new Object() {

            @Handler
            public void handleScreenDimmerHiddenEvent(TablexiaApplication.ScreenDimmerHiddenEvent screenDimmerHiddenEvent) {
                ApplicationBus.getInstance().unsubscribe(screenDimmerHandler);
                screenDimmerHandler = null;
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        onShow.run();
                    }
                });
            }

        };
        ApplicationBus.getInstance().subscribe(screenDimmerHandler);
        ApplicationBus.getInstance().post(new DimmerControlEvent(false)).asynchronously();
    }


//////////////////////////// BACK BUTTON CALLBACK

    @Override
    public void backButtonPressed() {
        if(rankAnimation != null && !rankAnimation.isHidden())
            rankAnimation.hide();
        else if(allTrophiesAnimation != null && !allTrophiesAnimation.isHidden()){
            allTrophiesAnimation.hide();
        }
        else {
            hideVictoryDialogAndStars();
            clearReceivedTrophyDialogs();
            goToGamePage();
        }
    }

//////////////////////////// GAME SCORE

    public <T> void setGameScore(String key, T value) {
        if (game != null) {
            GameDAO.setGameScore(game, key, String.valueOf(value));
        }
    }

    public void setGameScores(List<GameScore> gameScores) {
        if(game != null)
            GameDAO.setGameScores(game, gameScores);
    }

    /**
     * Thread blocking method which waits for all GameScores to be set and ready.
     */
    public void waitForActualGameScore() {
        int waitTime    = 50; //50ms
        int waitCounter = 0;

        while(!GameDAO.isGameScoreActual()) {
            try {
                Thread.sleep(waitTime);
                waitCounter++;
            } catch (InterruptedException e) {
                Thread.yield();
            }
        }

        if(waitCounter > 0)
            Log.info(getClass(), "Game has been waiting for " + (waitCounter * waitTime) + " ms for GameDAO to set all game scores.");
    }

    public String getGameScoreString(String key, String nullValue) {
        if (game != null) {
            String gameScoreStr = game.getGameScoreValue(key);
            return gameScoreStr != null ? gameScoreStr : nullValue;
        }
        return nullValue;
    }

    public Integer getGameScoreInteger(String key, Integer nullValue) {
        if (game != null) {
            String gameScoreStr = game.getGameScoreValue(key);
            return gameScoreStr != null ? Integer.valueOf(gameScoreStr) : nullValue;
        }
        return nullValue;
    }

    public Float getGameScoreFloat(String key, Float nullValue) {
        if (game != null) {
            String gameScoreStr = game.getGameScoreValue(key);
            return gameScoreStr != null ? Float.valueOf(gameScoreStr) : nullValue;
        }
        return nullValue;
    }

    public Double getGameScoreDouble(String key, Double nullValue) {
        if (game != null) {
            String gameScoreStr = game.getGameScoreValue(key);
            return gameScoreStr != null ? Double.valueOf(gameScoreStr) : nullValue;
        }
        return nullValue;
    }

    public Boolean getGameScoreBoolean(String key, Boolean nullValue) {
        if (game != null) {
            String gameScoreStr = game.getGameScoreValue(key);
            return gameScoreStr != null ? Boolean.valueOf(gameScoreStr) : nullValue;
        }
        return nullValue;
    }

    enum TrophyDialogPositionDefinition {

        RIGHT_TOP(1,1, 0f, 1.2f, 0.2f, 1f),
        RIGHT_BOTTOM(1,0, 0f, 0.3f, 0.2f, 0f),
        LEFT_TOP(0,1,1.2f,1.2f, 0.8f, 1f),
        LEFT_BOTTOM(0,0, 1.2f,0.3f, 0.8f, 0f);

        private int xStart, yStart;
        private float xShift, yShift;
        private float mobileShiftX, mobileShiftY;

        TrophyDialogPositionDefinition(int xStart, int yStart, float xShift, float yShift, float mobileShiftX, float mobileShiftY) {
            this.xStart = xStart;
            this.yStart = yStart;
            this.xShift = xShift;
            this.yShift = yShift;
            this.mobileShiftX = mobileShiftX;
            this.mobileShiftY = mobileShiftY;
        }

        public float getX(float x, float width, float dialogWidth, boolean isMobile){
            float shift = isMobile ? mobileShiftX : xShift; 
            return x + width* xStart - shift*dialogWidth;
        }

        public float getY(float y, float height, float dialogHeight, boolean isMobile){
            float shift = isMobile ? mobileShiftY : yShift;
            return y + height* yStart - shift*dialogHeight;
        }


    }

    public class GameFinishedEvent implements ApplicationBus.ApplicationEvent{
        String nameEvent;

        public GameFinishedEvent(String nameEvent){
            this.nameEvent = nameEvent;
        }
    }
    
}