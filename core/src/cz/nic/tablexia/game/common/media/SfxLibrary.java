/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.common.media;


import com.badlogic.gdx.audio.Sound;

import java.util.HashMap;

import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.util.Log;


/**
 * @author lhoracek
 */
public class SfxLibrary extends HashMap<AssetDescription, String> {

    private static final String SOUNDS_DIR       = "common/sfx/";
    public static final  String SOUNDS_EXTENSION = ".mp3";

    private AbstractTablexiaGame abstractTablexiaGame;

    public SfxLibrary(AbstractTablexiaGame abstractTablexiaGame, AssetDescription[] assetDescriptions) {
        this.abstractTablexiaGame = abstractTablexiaGame;
        for (AssetDescription assetDescription : assetDescriptions) {
            String path = SOUNDS_DIR + assetDescription.getResource() + SOUNDS_EXTENSION;
            Log.info(getClass(), "Adding sound " + path);
            put(assetDescription, path);
        }
    }

    public Sound getSound(AssetDescription assetDescription) {
        return abstractTablexiaGame.getSound(get(assetDescription));
    }
}
