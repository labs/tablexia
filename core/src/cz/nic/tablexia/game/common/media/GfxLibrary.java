/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.common.media;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;

import cz.nic.tablexia.game.AbstractTablexiaGame;


/**
 * Library of all kidnapping assets except tiles
 *
 * @author lhoracek
 */
public class GfxLibrary extends HashMap<AssetDescription, String> {

    private static final long   serialVersionUID   = -4016629019721509061L;
    private static final String DEFAULT_TEXTURE_DIR = "gfx/";
    private final        String TEXTURE_DIR;

    AbstractTablexiaGame abstractTablexiaGame;

    public GfxLibrary(AbstractTablexiaGame abstractTablexiaGame, AssetDescription[] textures, String dir) {
        this.abstractTablexiaGame = abstractTablexiaGame;
        this.TEXTURE_DIR = dir == null ? DEFAULT_TEXTURE_DIR : dir;
        for (AssetDescription assetDescription : textures) {
            String path = TEXTURE_DIR + assetDescription.getResource();
            put(assetDescription, path);
        }
    }

    public GfxLibrary(AbstractTablexiaGame abstractTablexiaGame, AssetDescription[] textures) {
        this(abstractTablexiaGame, textures, null);
    }

    public TextureRegion getTextureRegion(AssetDescription assetDescription) {
        return abstractTablexiaGame.getScreenTextureRegion(get(assetDescription));
    }
}
