/*
 *
 *  * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.common.ui.health_bar;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class FadingIndicator extends Indicator {

    private static final float FADE_ANIMATION_DURATION = 0.5f;

    public FadingIndicator(TextureRegion textureRegion) {
        super(textureRegion);
        addActor(indicatorImage);
        setSize(indicatorImage.getWidth(), indicatorImage.getHeight());
    }

    @Override
    void disable() {
        if (enabled) {
            enabled = false;
            indicatorImage.addAction(Actions.sequence(Actions.fadeOut(FADE_ANIMATION_DURATION), Actions.delay(FADE_ANIMATION_DURATION, Actions.hide())));
        }
    }

    @Override
    void enable() {
        if (!enabled) {
            enabled = true;
            indicatorImage.addAction(Actions.sequence(Actions.alpha(0f), Actions.show(), Actions.fadeIn(FADE_ANIMATION_DURATION)));
        }
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        indicatorImage.setSize(width, height);
    }

}



