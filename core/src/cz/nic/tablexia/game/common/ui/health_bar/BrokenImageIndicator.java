/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.common.ui.health_bar;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class BrokenImageIndicator extends Indicator {

    private static final float SCALE_WIDTH      = 0.8f;
    private static final float SCALE_HEIGHT     = 0.7f;

    private static final float BROKE_DURATION   = 0.095f;

    private Image brokenImage;

    public BrokenImageIndicator(TextureRegion indicatorTexture, TextureRegion brokenTexture){
        super(indicatorTexture);
        this.brokenImage = new Image(brokenTexture);
        brokenImage.setVisible(false);

        addActor(indicatorImage);
        addActor(brokenImage);

        setSize(this.indicatorImage.getWidth(), this.indicatorImage.getHeight());
    }


    @Override
    public void disable() {
        if (enabled) {
            enabled = false;
            float xMove = (indicatorImage.getWidth() / 2) * (1.f - SCALE_WIDTH);
            float yMove = (indicatorImage.getHeight() /2) * (1.f - SCALE_HEIGHT);

            indicatorImage.addAction(Actions.sequence(
                    Actions.parallel(
                            Actions.scaleTo(SCALE_WIDTH, SCALE_HEIGHT, BROKE_DURATION),
                            Actions.moveTo(xMove, yMove, BROKE_DURATION)),
                    Actions.run(new Runnable() {
                @Override
                public void run() {
                    indicatorImage.setVisible(false);
                    brokenImage.setVisible(true);
                }
            })));
        }
    }

    @Override
    public void enable() {
        if (!enabled) {
            enabled = true;
            brokenImage.addAction(Actions.sequence(Actions.fadeIn(0.2f), Actions.run(new Runnable() {
                @Override
                public void run() {
                    brokenImage.setVisible(false);
                }
            })));
        }
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        indicatorImage.setSize(width, height);
        brokenImage.setSize(width, height);
    }
}
