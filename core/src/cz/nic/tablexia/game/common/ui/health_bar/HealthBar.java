/*
 *
 *  * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.common.ui.health_bar;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.ArrayList;

import cz.nic.tablexia.game.AbstractTablexiaGame;

/**
 * Created by Vitaliy Vashchenko on 25.7.16.
 */
public class HealthBar extends Group {

    public static final Color           BACKGROUND_COLOR        = new Color(0, 0, 0, 0.5f);

    private static final int            DEFAULT_HEALTH_COUNT    = 3;
    private static final float          BACKGROUND_PADDING      = 10f;
    private static final DisableType    DEFAULT_DISABLE_TYPE    = DisableType.BROKEN_IMAGE;

    private static final int            DEFAULT_HEIGHT          = 30;
    private static final float          DEFAULT_OVERLAP         = 0.3f;
    private static final float          DEFAULT_ASCEND_STEP     = 0f;

    private static final float          HEALTH_BAR_PADDING      = 10f;

    private int healthCount;
    private TextureRegion heartFull;
    private TextureRegion heartBroken;

    private Group indicatorsGroup;
    private float height;
    private ArrayList<Indicator> indicators;
    private float overlapping;
    private float ascend;
    private DisableType disablingType;

    public HealthBar(TextureRegion heartFull, TextureRegion heartBroken) {
        this(heartFull, heartBroken, DEFAULT_HEALTH_COUNT, null, DEFAULT_HEIGHT, DEFAULT_ASCEND_STEP, DEFAULT_OVERLAP);
    }

    public HealthBar(TextureRegion heartFull,  TextureRegion heartBroken, TextureRegion background){
        this(heartFull, heartBroken, DEFAULT_HEALTH_COUNT, background, DEFAULT_HEIGHT, DEFAULT_ASCEND_STEP, DEFAULT_OVERLAP);
    }

    public HealthBar(TextureRegion heartFull, TextureRegion heartBroken, TextureRegion background, int healthCount){
        this(heartFull, heartBroken, healthCount, background, DEFAULT_HEIGHT, DEFAULT_ASCEND_STEP, DEFAULT_OVERLAP);
    }

    public HealthBar(TextureRegion heartFull, TextureRegion heartBroken, int healthCount, TextureRegion background, float height, float ascend, float overlapping) {
        this(heartFull, heartBroken, healthCount, background, height, ascend, overlapping, DEFAULT_DISABLE_TYPE);
    }


    public HealthBar(TextureRegion heartFull, TextureRegion heartBroken, int healthCount, TextureRegion background, float height, float ascend, float overlapping, DisableType disablingType) {
        this.healthCount = healthCount;
        this.heartFull = heartFull;
        this.heartBroken = heartBroken;
        this.height = height;
        this.ascend = ascend;
        this.overlapping = 1f - overlapping;
        this.disablingType = disablingType;

        if (background != null) {
            Image backgroundImage = new Image(background);
            backgroundImage.setFillParent(true);
            addActor(backgroundImage);
        }

        initHealthBar();

    }

    private void initHealthBar() {
        indicatorsGroup = new Group();
        indicators = new ArrayList<>();
        for (int i = 0; i < healthCount; i++) {
            Indicator healthIndicator = getIndicator();
            float ratio = healthIndicator.getWidth() / healthIndicator.getHeight();
            healthIndicator.setSize(height * ratio, height);
            healthIndicator.setPosition(i * overlapping * healthIndicator.getWidth(), i * ascend);
            indicators.add(healthIndicator);
        }

        for (int i = indicators.size() - 1; i >= 0; i--) {
            indicatorsGroup.addActor(indicators.get(i));
        }

        Image temp = new Image(heartFull);
        float ratio = temp.getWidth() / temp.getHeight();
        temp.setSize(height * ratio, height);

        setSize(((healthCount * temp.getWidth()) - (healthCount - 1) * (1f - overlapping) * temp.getWidth()) + BACKGROUND_PADDING, height + (ascend * (healthCount - 1)) + BACKGROUND_PADDING);
        indicatorsGroup.setPosition(BACKGROUND_PADDING / 2, BACKGROUND_PADDING / 2);
        addActor(indicatorsGroup);
    }

    private Indicator getIndicator(){
        switch (disablingType){
            case BROKEN_IMAGE:
                return disablingType.getIndicatorInstance(heartFull, heartBroken);
            case DISABLED_IMAGE:
                return disablingType.getIndicatorInstance(heartFull, heartBroken);
             default:
                 return disablingType.getIndicatorInstance(heartFull);
        }
    }

    public void hide() {
        healthCount--;
        for (int i = 0; i < indicators.size(); i++) {
            if (hideVisibleIndicator(indicators.get(i))) return;
        }
    }

    public void removeAllHealth(){
        healthCount = 0;
        for (int i = 0; i < indicators.size(); i++) {
            hideVisibleIndicator(indicators.get(i));
        }
    }


    private boolean hideVisibleIndicator(Indicator indicator) {
        if (indicator.isEnabled()) {
            indicator.disable();
            return true;
        }
        return false;
    }

    public boolean hasHealthsLeft() {
        return healthCount > 0;
    }

    public void setHealthBarDefaultPosition(AbstractTablexiaGame game){
        this.setPosition(game.getSceneRightX() - this.getWidth() - HEALTH_BAR_PADDING, game.getSceneInnerTopY() - this.getHeight());
    }

}
