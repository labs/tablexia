/*
 *
 *  * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.game.common.ui.health_bar;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;

import cz.nic.tablexia.util.Log;

public enum DisableType {

    FADE(FadingIndicator.class),
    GREY_SCALING(GreyScaleIndicator.class),
    DISABLED_IMAGE(DisabledImageIndicator.class),
    BROKEN_IMAGE(BrokenImageIndicator.class);

    Class<? extends Indicator> indicatorClassDefinition;

    DisableType(Class<? extends Indicator> indicatorClassDefinition) {
        this.indicatorClassDefinition = indicatorClassDefinition;
    }

    public Class<? extends Indicator> getIndicatorClassDefinition() {
        return indicatorClassDefinition;
    }

    public Indicator getIndicatorInstance(TextureRegion textureRegion) {
        try {
            return (Indicator) ClassReflection.getConstructor(getIndicatorClassDefinition(), TextureRegion.class).newInstance(textureRegion);
        } catch (ReflectionException e) {
            Log.err(getClass(), "Cannot create instance of health indicator: " + getIndicatorClassDefinition(), e);
        }
        return null;
    }

    public Indicator getIndicatorInstance(TextureRegion indicatorImage, TextureRegion disableImage){
        try {
            return (Indicator) ClassReflection.getConstructor(getIndicatorClassDefinition(), TextureRegion.class, TextureRegion.class).newInstance(indicatorImage, disableImage);
        } catch (ReflectionException e) {
            Log.err(getClass(), "Cannot create instance of health indicator: " + getIndicatorClassDefinition(), e);
        }
        return null;
    }
}