/*
 * Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.common.ui.health_bar;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class DisabledImageIndicator extends Indicator {
    private static final float DEFAULT_FADE_DURATION = 0.3f;

    private Image disabledImage;

    public DisabledImageIndicator(TextureRegion indicatorTexture, TextureRegion disabledTexture) {
        super(indicatorTexture);
        this.disabledImage = new Image(disabledTexture);
        disabledImage.setVisible(false);

        addActor(disabledImage);
        addActor(this.indicatorImage);

        setSize(this.indicatorImage.getWidth(), this.indicatorImage.getHeight());
    }

    @Override
    public void disable() {
        if (enabled) {
            enabled = false;
            disabledImage.setVisible(true);
            indicatorImage.addAction(Actions.fadeOut(DEFAULT_FADE_DURATION));
        }
    }

    @Override
    public void enable() {
        if (!enabled) {
            enabled = true;
            indicatorImage.addAction(Actions.sequence(Actions.fadeIn(DEFAULT_FADE_DURATION), Actions.run(new Runnable() {
                @Override
                public void run() {
                    disabledImage.setVisible(false);
                }
            })));
        }
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        indicatorImage.setSize(width, height);
        disabledImage.setSize(width, height);
    }
}
