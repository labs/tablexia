/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.trophy;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.model.game.UserTrophy;
import cz.nic.tablexia.shared.model.User;

/**
 * Definitions of user trophies
 *
 * @author Matyáš Latner
 */
public enum UserTrophyDefinition implements ITrophyDefinition {

    CONSECUTIVELY_DAYSBACK_3(UserTrophyTypeDefinition.CONSECUTIVELY_DAYSBACK, "consecutively3Days", "user_consecutively3days", "user_consecutively3days_description", 3, new UserTrophy.ConsecutivelyGamePlayed()),
    CONSECUTIVELY_DAYSBACK_5(UserTrophyTypeDefinition.CONSECUTIVELY_DAYSBACK, "consecutively5Days", "user_consecutively5days", "user_consecutively5days_description", 5, new UserTrophy.ConsecutivelyGamePlayed()),
    CONSECUTIVELY_DAYSBACK_8(UserTrophyTypeDefinition.CONSECUTIVELY_DAYSBACK, "consecutively8Days", "user_consecutively8days", "user_consecutively8days_description", 8, new UserTrophy.ConsecutivelyGamePlayed()),
    ONEDAYALLGAMES_0STARS(UserTrophyTypeDefinition.ONEDAYALLGAMES, "consecutivelyAllGames0Stars", "user_consecutivelyallgames0stars", "user_consecutivelyallgames0stars_description", 0, new UserTrophy.AllGamesInOneDay()),
    CONSECUTIVELYALLGAMES_MEDIUMDIFFICULTY(UserTrophyTypeDefinition.CONSECUTIVELY_ALLGAMES, "consecutivelyAllGames2Stars", "user_consecutivelyallgames2stars", "user_consecutivelyallgames2stars_description", GameDifficulty.MEDIUM.getDifficultyNumber(), new UserTrophy.AllStarsGame()),
    CONSECUTIVELYALLGAMES_HARDDIFFICULTY(UserTrophyTypeDefinition.CONSECUTIVELY_ALLGAMES, "consecutivelyAllGames3Stars", "user_consecutivelyallgames3stars", "user_consecutivelyallgames3stars_description", GameDifficulty.HARD.getDifficultyNumber(), new UserTrophy.AllStarsGame());

    private UserTrophyTypeDefinition        trophyTypeDefinition;
    private String                          dbFieldName;
    private String                          trophyName;
    private String                          trophyDescription;
    private int                             limit;
    private UserTrophy.UserTrophyResolver   userTrophyResolver;

    UserTrophyDefinition(UserTrophyTypeDefinition trophyTypeDefinition, String dbFieldName, String trophyName, String trophyDescription, int limit, UserTrophy.UserTrophyResolver userTrophyResolver) {
        this.trophyTypeDefinition = trophyTypeDefinition;
        this.dbFieldName = dbFieldName;
        this.trophyName = trophyName;
        this.trophyDescription = trophyDescription;
        this.limit = limit;
        this.userTrophyResolver = userTrophyResolver;
    }


    public UserTrophyTypeDefinition getTrophyTypeDefinition() {
        return trophyTypeDefinition;
    }

    public String getDbFieldName() {
        return dbFieldName;
    }

    @Override
    public String getTrophyNameKey() {
        return trophyName;
    }

    @Override
    public String getTrophyName() {
        return ApplicationTextManager.getInstance().getText(getTrophyNameKey());
    }

    @Override
    public String getTrophyDescriptionKey() {
        return trophyDescription;
    }

    @Override
    public String getTrophyDescription() {
        return ApplicationTextManager.getInstance().getText(getTrophyDescriptionKey());
    }

    public int getLimit() {
        return limit;
    }

    public boolean hasTrophy(User user) {
        return userTrophyResolver.hasGameTrophy(user, this);
    }

    public static List<UserTrophyDefinition> getTrophyDefinitionByUserTrophyDefinition(UserTrophyTypeDefinition trophyTypeDefinition) {
        List<UserTrophyDefinition> result = new ArrayList<UserTrophyDefinition>();
        for (UserTrophyDefinition trophyDefinition : UserTrophyDefinition.values()) {
            if (trophyDefinition.getTrophyTypeDefinition() == trophyTypeDefinition) {
                result.add(trophyDefinition);
            }
        }
        return result;
    }
}
