/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.trophy;

import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.model.game.GameTrophy;

/**
 * Definitions of game trophies
 *
 * @author Matyáš Latner, Vaclav Tarantik
 */
public enum GameTrophyDefinition implements ITrophyDefinition {

    ROBBERY_PLAY1(GameDefinition.ROBBERY, GameTrophyTypeDefinition.PLAY_LIMIT_1, "robbery_1_finished", "robbery_1_finished_description", 1, new GameTrophy.NumberOfTimesFinished()), //0
    ROBBERY_PLAY2(GameDefinition.ROBBERY, GameTrophyTypeDefinition.PLAY_LIMIT_2, "robbery_5_finished", "robbery_5_finished_description", 5, new GameTrophy.NumberOfTimesFinished()), //1
    ROBBERY_PLAY3(GameDefinition.ROBBERY, GameTrophyTypeDefinition.PLAY_LIMIT_3, "robbery_10_finished", "robbery_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //2
    ROBBERY_DIFF1(GameDefinition.ROBBERY, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "robbery_1_limit", "robbery_1_limit_description", GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //3
    ROBBERY_DIFF2(GameDefinition.ROBBERY, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "robbery_2_limit", "robbery_2_limit_description", GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//4
    ROBBERY_BONUS(GameDefinition.ROBBERY, GameTrophyTypeDefinition.BONUS_PLAYED, "robbery_bonus_finished", "robbery_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//5

    PURSUIT_PLAY1(GameDefinition.PURSUIT, GameTrophyTypeDefinition.PLAY_LIMIT_1, "pursuit_1_finished",  "pursuit_1_finished_description", 1, new GameTrophy.NumberOfTimesFinished()), //6
    PURSUIT_PLAY2(GameDefinition.PURSUIT, GameTrophyTypeDefinition.PLAY_LIMIT_2, "pursuit_5_finished",  "pursuit_5_finished_description", 5, new GameTrophy.NumberOfTimesFinished()), //7
    PURSUIT_PLAY3(GameDefinition.PURSUIT, GameTrophyTypeDefinition.PLAY_LIMIT_3, "pursuit_10_finished",  "pursuit_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //8
    PURSUIT_DIFF1(GameDefinition.PURSUIT, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "pursuit_1_limit", "pursuit_1_limit_description", GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //9
    PURSUIT_DIFF2(GameDefinition.PURSUIT, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "pursuit_2_limit", "pursuit_2_limit_description", GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//10
    PURSUIT_BONUS(GameDefinition.PURSUIT, GameTrophyTypeDefinition.BONUS_PLAYED, "pursuit_bonus_finished", "pursuit_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//11

    KIDNAPPING_PLAY1(GameDefinition.KIDNAPPING, GameTrophyTypeDefinition.PLAY_LIMIT_1, "kidnapping_1_finished", "kidnapping_1_finished_description", 1, new GameTrophy.NumberOfTimesFinished()), //12
    KIDNAPPING_PLAY2(GameDefinition.KIDNAPPING, GameTrophyTypeDefinition.PLAY_LIMIT_2, "kidnapping_5_finished", "kidnapping_5_finished_description", 5, new GameTrophy.NumberOfTimesFinished()), //13
    KIDNAPPING_PLAY3(GameDefinition.KIDNAPPING, GameTrophyTypeDefinition.PLAY_LIMIT_3, "kidnapping_10_finished", "kidnapping_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //14
    KIDNAPPING_DIFF1(GameDefinition.KIDNAPPING, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "kidnapping_1_limit",  "kidnapping_1_limit_description", GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //15
    KIDNAPPING_DIFF2(GameDefinition.KIDNAPPING, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "kidnapping_2_limit", "kidnapping_2_limit_description", GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//16
    KIDNAPPING_BONUS(GameDefinition.KIDNAPPING, GameTrophyTypeDefinition.BONUS_PLAYED, "kidnapping_bonus_finished", "kidnapping_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//17


    NIGHT_WATCH_PLAY1(GameDefinition.NIGHT_WATCH, GameTrophyTypeDefinition.PLAY_LIMIT_1, "night_watch_1_finished", "night_watch_1_finished_description", 1, new GameTrophy.NumberOfTimesFinished()), //18
    NIGHT_WATCH_PLAY2(GameDefinition.NIGHT_WATCH, GameTrophyTypeDefinition.PLAY_LIMIT_2, "night_watch_5_finished", "night_watch_5_finished_description",  5, new GameTrophy.NumberOfTimesFinished()), //19
    NIGHT_WATCH_PLAY3(GameDefinition.NIGHT_WATCH, GameTrophyTypeDefinition.PLAY_LIMIT_3, "night_watch_10_finished", "night_watch_10_finished_description",10, new GameTrophy.NumberOfTimesFinished()), //20
    NIGHT_WATCH_DIFF1(GameDefinition.NIGHT_WATCH, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "night_watch_1_limit", "night_watch_1_limit_description",  GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //21
    NIGHT_WATCH_DIFF2(GameDefinition.NIGHT_WATCH, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "night_watch_2_limit", "night_watch_2_limit_description",GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//22
    NIGHT_WATCH_BONUS(GameDefinition.NIGHT_WATCH, GameTrophyTypeDefinition.BONUS_PLAYED, "night_watch_bonus_finished", "night_watch_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//23


    SHOOTING_RANGE_PLAY1(GameDefinition.SHOOTING_RANGE, GameTrophyTypeDefinition.PLAY_LIMIT_1, "shooting_range_1_finished", "shooting_range_1_finished_description", 1, new GameTrophy.NumberOfTimesFinished()), //24
    SHOOTING_RANGE_PLAY2(GameDefinition.SHOOTING_RANGE, GameTrophyTypeDefinition.PLAY_LIMIT_2, "shooting_range_5_finished","shooting_range_5_finished_description", 5, new GameTrophy.NumberOfTimesFinished()), //25
    SHOOTING_RANGE_PLAY3(GameDefinition.SHOOTING_RANGE, GameTrophyTypeDefinition.PLAY_LIMIT_3, "shooting_range_10_finished", "shooting_range_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //26
    SHOOTING_RANGE_DIFF1(GameDefinition.SHOOTING_RANGE, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "shooting_range_1_limit",  "shooting_range_1_limit_description", GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //27
    SHOOTING_RANGE_DIFF2(GameDefinition.SHOOTING_RANGE, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "shooting_range_2_limit", "shooting_range_2_limit_description", GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//28
    SHOOTING_RANGE_BONUS(GameDefinition.SHOOTING_RANGE, GameTrophyTypeDefinition.BONUS_PLAYED, "shooting_range_bonus_finished", "shooting_range_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//29

    IN_THE_DARKNESS_PLAY1(GameDefinition.IN_THE_DARKNESS, GameTrophyTypeDefinition.PLAY_LIMIT_1, "in_the_darkness_1_finished", "in_the_darkness_1_finished_description", 1, new GameTrophy.NumberOfTimesFinished()), //30
    IN_THE_DARKNESS_PLAY2(GameDefinition.IN_THE_DARKNESS, GameTrophyTypeDefinition.PLAY_LIMIT_2, "in_the_darkness_5_finished","in_the_darkness_5_finished_description", 5, new GameTrophy.NumberOfTimesFinished()), //31
    IN_THE_DARKNESS_PLAY3(GameDefinition.IN_THE_DARKNESS, GameTrophyTypeDefinition.PLAY_LIMIT_3, "in_the_darkness_10_finished", "in_the_darkness_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //32
    IN_THE_DARKNESS_DIFF1(GameDefinition.IN_THE_DARKNESS, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "in_the_darkness_1_limit", "in_the_darkness_1_limit_description", GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //33
    IN_THE_DARKNESS_DIFF2(GameDefinition.IN_THE_DARKNESS, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "in_the_darkness_2_limit", "in_the_darkness_2_limit_description", GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//34
    IN_THE_DARKNESS_BONUS(GameDefinition.IN_THE_DARKNESS, GameTrophyTypeDefinition.BONUS_PLAYED, "in_the_darkness_bonus_finished", "in_the_darkness_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//35

    CRIME_SCENE_PLAY1(GameDefinition.CRIME_SCENE, GameTrophyTypeDefinition.PLAY_LIMIT_1,       "crime_scene_1_finished",  "crime_scene_1_finished_description",  1, new GameTrophy.NumberOfTimesFinished()), //36
    CRIME_SCENE_PLAY2(GameDefinition.CRIME_SCENE, GameTrophyTypeDefinition.PLAY_LIMIT_2,       "crime_scene_5_finished",  "crime_scene_5_finished_description",  5, new GameTrophy.NumberOfTimesFinished()), //37
    CRIME_SCENE_PLAY3(GameDefinition.CRIME_SCENE, GameTrophyTypeDefinition.PLAY_LIMIT_3,       "crime_scene_10_finished", "crime_scene_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //38
    CRIME_SCENE_DIFF1(GameDefinition.CRIME_SCENE, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "crime_scene_1_limit",     "crime_scene_1_limit_description",     GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //39
    CRIME_SCENE_DIFF2(GameDefinition.CRIME_SCENE, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "crime_scene_2_limit",     "crime_scene_2_limit_description",     GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //40
    CRIME_SCENE_BONUS(GameDefinition.CRIME_SCENE, GameTrophyTypeDefinition.BONUS_PLAYED, "crime_scene_bonus_finished", "crime_scene_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//41


    RUNES_PLAY1(GameDefinition.RUNES, GameTrophyTypeDefinition.PLAY_LIMIT_1, "runes_1_finished", "runes_1_finished_description", 1, new GameTrophy.NumberOfTimesFinished()), //42
    RUNES_PLAY2(GameDefinition.RUNES, GameTrophyTypeDefinition.PLAY_LIMIT_2, "runes_5_finished","runes_5_finished_description", 5, new GameTrophy.NumberOfTimesFinished()), //43
    RUNES_PLAY3(GameDefinition.RUNES, GameTrophyTypeDefinition.PLAY_LIMIT_3, "runes_10_finished", "runes_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //44
    RUNES_DIFF1(GameDefinition.RUNES, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "runes_1_limit", "runes_1_limit_description", GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //45
    RUNES_DIFF2(GameDefinition.RUNES, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "runes_2_limit", "runes_2_limit_description", GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//46
    RUNES_BONUS(GameDefinition.RUNES, GameTrophyTypeDefinition.BONUS_PLAYED, "runes_bonus_finished", "runes_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//47


    PROTOCOL_PLAY1(GameDefinition.PROTOCOL, GameTrophyTypeDefinition.PLAY_LIMIT_1, "protocol_1_finished",   "protocol_1_finished_description",  1,  new GameTrophy.NumberOfTimesFinished()), //48
    PROTOCOL_PLAY2(GameDefinition.PROTOCOL, GameTrophyTypeDefinition.PLAY_LIMIT_2, "protocol_5_finished",   "protocol_5_finished_description",  5,  new GameTrophy.NumberOfTimesFinished()), //49
    PROTOCOL_PLAY3(GameDefinition.PROTOCOL, GameTrophyTypeDefinition.PLAY_LIMIT_3, "protocol_10_finished",  "protocol_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //50
    PROTOCOL_DIFF1(GameDefinition.PROTOCOL, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "protocol_1_limit", "protocol_1_limit_description",GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //51
    PROTOCOL_DIFF2(GameDefinition.PROTOCOL, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "protocol_2_limit", "protocol_2_limit_description",GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),   //52
    PROTOCOL_BONUS(GameDefinition.PROTOCOL, GameTrophyTypeDefinition.BONUS_PLAYED, "protocol_bonus_finished", "protocol_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),//53


    SAFE_PLAY1(GameDefinition.SAFE, GameTrophyTypeDefinition.PLAY_LIMIT_1, "safe_1_finished",   "safe_1_finished_description",  1,  new GameTrophy.NumberOfTimesFinished()), //54
    SAFE_PLAY2(GameDefinition.SAFE, GameTrophyTypeDefinition.PLAY_LIMIT_2, "safe_5_finished",   "safe_5_finished_description",  5,  new GameTrophy.NumberOfTimesFinished()), //55
    SAFE_PLAY3(GameDefinition.SAFE, GameTrophyTypeDefinition.PLAY_LIMIT_3, "safe_10_finished",  "safe_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //56
    SAFE_DIFF1(GameDefinition.SAFE, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "safe_1_limit", "safe_1_limit_description",GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //57
    SAFE_DIFF2(GameDefinition.SAFE, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "safe_2_limit", "safe_2_limit_description",GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),   //58
    SAFE_BONUS(GameDefinition.SAFE, GameTrophyTypeDefinition.BONUS_PLAYED, "safe_bonus_finished", "safe_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //59

    ON_THE_TRAIL_PLAY1(GameDefinition.ON_THE_TRAIL, GameTrophyTypeDefinition.PLAY_LIMIT_1, "on_the_trail_1_finished",   "on_the_trail_1_finished_description",  1,  new GameTrophy.NumberOfTimesFinished()), //60
    ON_THE_TRAIL_PLAY2(GameDefinition.ON_THE_TRAIL, GameTrophyTypeDefinition.PLAY_LIMIT_2, "on_the_trail_5_finished",   "on_the_trail_5_finished_description",  5,  new GameTrophy.NumberOfTimesFinished()), //61
    ON_THE_TRAIL_PLAY3(GameDefinition.ON_THE_TRAIL, GameTrophyTypeDefinition.PLAY_LIMIT_3, "on_the_trail_10_finished",  "on_the_trail_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //62
    ON_THE_TRAIL_DIFF1(GameDefinition.ON_THE_TRAIL, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "on_the_trail_1_limit", "on_the_trail_1_limit_description",GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //63
    ON_THE_TRAIL_DIFF2(GameDefinition.ON_THE_TRAIL, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "on_the_trail_2_limit", "on_the_trail_2_limit_description",GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),   //64
    ON_THE_TRAIL_BONUS(GameDefinition.ON_THE_TRAIL, GameTrophyTypeDefinition.BONUS_PLAYED, "on_the_trail_bonus_finished", "on_the_trail_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //65

    MEMORY_GAME_PLAY1(GameDefinition.MEMORY_GAME, GameTrophyTypeDefinition.PLAY_LIMIT_1, "memory_game_1_finished",   "memory_game_1_finished_description",  1,  new GameTrophy.NumberOfTimesFinished()), //66
    MEMORY_GAME_PLAY2(GameDefinition.MEMORY_GAME, GameTrophyTypeDefinition.PLAY_LIMIT_2, "memory_game_5_finished",   "memory_game_5_finished_description",  5,  new GameTrophy.NumberOfTimesFinished()), //67
    MEMORY_GAME_PLAY3(GameDefinition.MEMORY_GAME, GameTrophyTypeDefinition.PLAY_LIMIT_3, "memory_game_10_finished",  "memory_game_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //68
    MEMORY_GAME_DIFF1(GameDefinition.MEMORY_GAME, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "memory_game_1_limit", "memory_game_1_limit_description",GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //69
    MEMORY_GAME_DIFF2(GameDefinition.MEMORY_GAME, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "memory_game_2_limit", "memory_game_2_limit_description",GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),   //70
    MEMORY_GAME_BONUS(GameDefinition.MEMORY_GAME, GameTrophyTypeDefinition.BONUS_PLAYED, "memory_game_bonus_finished", "memory_game_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //71

    ATTENTION_GAME_PLAY1(GameDefinition.ATTENTION_GAME, GameTrophyTypeDefinition.PLAY_LIMIT_1, "attention_1_finished", "attention_1_finished_description",  1,  new GameTrophy.NumberOfTimesFinished()), //72
    ATTENTION_GAME_PLAY2(GameDefinition.ATTENTION_GAME, GameTrophyTypeDefinition.PLAY_LIMIT_2, "attention_5_finished", "attention_5_finished_description",  5,  new GameTrophy.NumberOfTimesFinished()), //73
    ATTENTION_GAME_PLAY3(GameDefinition.ATTENTION_GAME, GameTrophyTypeDefinition.PLAY_LIMIT_3, "attention_10_finished", "attention_10_finished_description", 10, new GameTrophy.NumberOfTimesFinished()), //74
    ATTENTION_GAME_DIFF1(GameDefinition.ATTENTION_GAME, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, "attention_1_limit", "attention_1_limit_description",GameDifficulty.MEDIUM.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()), //75
    ATTENTION_GAME_DIFF2(GameDefinition.ATTENTION_GAME, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, "attention_2_limit", "attention_2_limit_description",GameDifficulty.HARD.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()),   //76
    ATTENTION_GAME_BONUS(GameDefinition.ATTENTION_GAME, GameTrophyTypeDefinition.BONUS_PLAYED, "attention_bonus_finished", "attention_bonus_finished_description", GameDifficulty.BONUS.getDifficultyNumber(), new GameTrophy.ThreeStarsReceived()); //77



    private GameDefinition                  gameDefinition;
    private GameTrophyTypeDefinition        trophyTypeDefinition;
    private String                          trophyName;
    private String                          trophyDescription;
    private int                             limit;
    private GameTrophy.GameTrophyResolver   gameTrophyResolver;

    GameTrophyDefinition(GameDefinition gameDefinition, GameTrophyTypeDefinition trophyTypeDefinition, String trophyName, String trophyDescription, int limit, GameTrophy.GameTrophyResolver gameTrophyResolver) {
        this.gameDefinition = gameDefinition;
        this.trophyTypeDefinition = trophyTypeDefinition;
        this.trophyName = trophyName;
        this.trophyDescription = trophyDescription;
        this.limit = limit;
        this.gameTrophyResolver = gameTrophyResolver;
    }

    public GameDefinition getGameDefinition() {
        return gameDefinition;
    }

    public GameTrophyTypeDefinition getTrophyTypeDefinition() {
        return trophyTypeDefinition;
    }

    @Override
    public String getTrophyNameKey() {
        return trophyName;
    }

    @Override
    public String getTrophyName() {
        return ApplicationTextManager.getInstance().getText(getTrophyNameKey());
    }

    @Override
    public String getTrophyDescriptionKey() {
        return trophyDescription;
    }

    @Override
    public String getTrophyDescription() {
        return String.format("%s %s", ApplicationTextManager.getInstance().getText(getTrophyDescriptionKey()), getGameDefinition().getTitle());
    }

    public int getLimit() {
        return limit;
    }

    public boolean hasTrophy(User user) {
        return gameTrophyResolver.hasGameTrophy(user, this);
    }

    public static GameTrophyDefinition getTrophyDefinitionByGameAndTrophyType(GameDefinition gameDefinition, GameTrophyTypeDefinition trophyTypeDefinition) {
        for (GameTrophyDefinition trophyDefinition : GameTrophyDefinition.values()) {
            if (trophyDefinition.getGameDefinition().equals(gameDefinition) && (trophyDefinition.getTrophyTypeDefinition() == trophyTypeDefinition)) {
                return trophyDefinition;
            }
        }
        return null;
    }

    public static GameTrophyDefinition getTrophyDefinitionByGameAndLimit(GameDefinition gameDefinition, TrophyGlobalTypeDefinition trophyGlobalTypeDefinition, int limit) {
        for (GameTrophyDefinition trophyDefinition : GameTrophyDefinition.values()) {
            if (trophyDefinition.getGameDefinition().equals(gameDefinition) && (trophyDefinition.getTrophyTypeDefinition().getTrophyGlobalTypeDefinition() == trophyGlobalTypeDefinition) && (trophyDefinition.getLimit() == limit)) {
                return trophyDefinition;
            }
        }
        return null;
    }
}
