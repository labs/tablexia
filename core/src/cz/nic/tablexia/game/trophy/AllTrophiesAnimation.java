/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.game.trophy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ScaleUtil;


public class AllTrophiesAnimation extends Group {
    public static final String ALL_TROPHY_ANIMATION_FINISHED = "all trophy animation finished";

    private static final Color  DIMMER_COLOR                = new Color(0, 0, 0, 0.7f);

    private static final float  ANIMATION_IMAGE_WIDTH       = 0.7f;
    private static final float  HIDE_FADEOUT_DURATION       = 0.5f;
    private static final float  SHOW_FADE_IN_DURATION       = 1f;


    private final Group animationGroup;
    private Image animationImage;

    private boolean hidden = true;

    private AllTrophiesAnimation(float width, float height) {
        setSize(width, height);

        //Creating
        animationGroup  = new Group();

        addAction(Actions.alpha(0));
    }

    public static final AllTrophiesAnimation createAllTrophyAnimation(Stage stage) {
        float width  = TablexiaSettings.getViewportWidth(stage);
        float height = TablexiaSettings.getViewportHeight(stage);
        float posX   = TablexiaSettings.getViewportLeftX(stage);
        float posY   = TablexiaSettings.getViewportBottomY(stage);

        AllTrophiesAnimation allTrophyAnimation = new AllTrophiesAnimation(width, height);
        allTrophyAnimation.setPosition(posX, posY);

        allTrophyAnimation.prepareDimmer();
        allTrophyAnimation.prepareFullTrophyAnimation();

        return allTrophyAnimation;
    }

    private void prepareDimmer() {
        Image dimmer = new Image(ApplicationAtlasManager.getInstance().getColorTexture(DIMMER_COLOR));
        dimmer.setSize(this.getWidth(), this.getHeight());
        this.addActor(dimmer);
    }

    private void prepareFullTrophyAnimation() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                prepareAnimationImage(animationGroup);
            }
        });

        this.addActor(animationGroup);
    }

    private void prepareAnimationImage(Group group) {
        animationImage = ScaleUtil.createImageToWidth(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.TROPHY_ANIMATION + 1), this.getWidth() * ANIMATION_IMAGE_WIDTH);
        group.setSize(animationImage.getWidth(), animationImage.getHeight());
        group.setPosition(this.getWidth() / 2f - group.getWidth() / 2f, this.getHeight() / 2f - group.getHeight() / 2f);
        group.addActor(animationImage);
    }

    private void animateFullTrophyAnimation() {
        SequenceAction sa = new SequenceAction();
        for(int i=2; i<20; i++){
            final int frameNumber = i;
            sa.addAction(Actions.delay(0.07f));
            sa.addAction(Actions.run(new Runnable() {
                @Override
                public void run() {
                    changeAnimationImage(frameNumber);
                }
            }));
        }
        for(int j=0; j<3; j++) {
            for (int i = 20; i < 37; i++) {
                final int frameNumber = i;
                sa.addAction(Actions.delay(0.07f));
                sa.addAction(Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        changeAnimationImage(frameNumber);
                    }
                }));
            }
        }
        for(int i=71; i<78; i++){
            final int frameNumber = i;
            sa.addAction(Actions.delay(0.07f));
            sa.addAction(Actions.run(new Runnable() {
                @Override
                public void run() {
                    changeAnimationImage(frameNumber);
                    AbstractTablexiaScreen.triggerScenarioStepEvent(ALL_TROPHY_ANIMATION_FINISHED);
                }
            }));
        }


        animationGroup.addAction(sa);
    }

    private void changeAnimationImage(int frameNumber){
        animationImage.setDrawable(new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.TROPHY_ANIMATION + frameNumber)));
    }

    ////////////////////////////////
    /////    PUBLIC METHODS    /////
    ////////////////////////////////

    public void hide() {
        hidden = true;

        this.addAction(new SequenceAction(
                Actions.fadeOut(HIDE_FADEOUT_DURATION),
                Actions.removeActor()));
    }

    public void show() {
        if(getStage() == null) {
            Log.err(getClass(), "Couldn't show All Trophy Animation, because there is no stage!");
        }

        hidden = false;

        this.addAction(new SequenceAction(
                Actions.fadeIn(SHOW_FADE_IN_DURATION),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        animateFullTrophyAnimation();
                    }
                })
        ));
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setNewBadgeEventListener(EventListener eventListener) {
        if(eventListener == null) return;

        animationGroup.clearListeners();
        animationGroup.addListener(eventListener);
    }
}
