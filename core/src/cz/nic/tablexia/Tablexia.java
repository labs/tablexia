/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.Timer;

import net.engio.mbassy.bus.error.IPublicationErrorHandler;
import net.engio.mbassy.bus.error.PublicationError;
import net.engio.mbassy.listener.Handler;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.ApplicationBus.ApplicationEvent;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationAvatarManager;
import cz.nic.tablexia.loader.application.TablexiaBadgesManager;
import cz.nic.tablexia.loader.application.ApplicationExternalSoundManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationInternalSoundManager;
import cz.nic.tablexia.loader.application.ApplicationInternalTextureManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.loader.zip.TablexiaAssetsManager;
import cz.nic.tablexia.menu.MenuController;
import cz.nic.tablexia.model.DatabaseManager;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.loader.IConnectionManager;
import cz.nic.tablexia.screen.loader.ILegacyManager;
import cz.nic.tablexia.util.CameraOpener;
import cz.nic.tablexia.util.IFileSystemManager;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.TeeStream;
import cz.nic.tablexia.util.Utility;
import cz.nic.tablexia.util.ui.button.StandardTablexiaButton;
import cz.nic.tablexia.util.ui.QRCodeScanner;
import cz.nic.tablexia.util.ui.button.TablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AdaptiveSizeDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.AlertOnShowDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.BackButtonHideComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.DimmerDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.PositiveNegativeButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.SingleButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;

/**
 *
 * @author Matyáš Latner
 */
public class Tablexia extends TablexiaApplication {

    public static final String  INTERNAL_GFX_ASSETS_PATH                = "gfx/";
    public static final String  ERROR_DIALOG_BACKGROUND_INTERNAL        = INTERNAL_GFX_ASSETS_PATH + "dialog_square_borderlines.9.png";
    public static final String  LANGUAGE_REQUEST                        = "languageRequest";
    public static final String  BACK_BUTTON_NAME                        = "back button name";
    public static final int     ERROR_DIALOG_WIDTH                      = 460;
    public static final int     ERROR_DIALOG_HEIGHT                     = 300;
    public static final int     LOCALE_DIALOG_WIDTH                     = 320;
    public static final int     LOCALE_DIALOG_HEIGHT                    = 320;
    public static final float   ERROR_DIALOG_DIMMER                     = 0.7f;

    public static final int     BACK_BUTTON_SIZE                       = 50;
    public static final float   BACK_BUTTON_LEFT_OFFSET_RATIO          = 0.045f;
    public static final float   BACK_BUTTON_ADAPTIVE_SIZE_RATIO        = 1.7f;
    public static final float   BACK_BUTTON_BOTTOM_OFFSET              = 0.04f;
    public static final boolean BACK_BUTTON_ADAPTIVE_SIZE_POSITION_FIX = true;
    public static final float   SHOW_BACK_BUTTON_DURATION              = 1f;
    public static final float   HIDE_BACK_BUTTON_DURATION              = 1f;

    private static final boolean LOADER_ERROR_DIALOG_HIDE_ANIMATION     = false;
    private static final float   LOCALE_CHANGE_DELAY                    = 1f;

    private static final boolean DEFAULT_HD_ASSETS                      = false;


    private static final String  LOG_FILE_NAME                          = "session.log";

    private static IConnectionManager connectionManager;
    private static ILegacyManager     legacyManager;
    private static CameraOpener       cameraOpener;
    private static IFileSystemManager fileSystemManager;
    private static QRCodeScanner      qrCodeScanner;
    private static Net                net;


    private final SQLConnectionType     sqlConnectionType;
    private MenuController              menuController;
    private TablexiaButton              backButton;
    private boolean                     backButtonVisibility;
    private TablexiaAssetsManager       assetsManager;
    private ScreenshotListener          screenshotListener;
    private boolean                     reset;
    private boolean                     loadingComplete = false;
    private boolean                     errorDialogShown = false;
    private boolean                     languageDialogShown = false;
    private TablexiaComponentDialog     errorDialog;
    private TablexiaComponentDialog     languageDialog;

    private String                      sentryDSN;

    public static class SQLConnectionType {

        private String driver;
        private String connectionString;

        public SQLConnectionType(String driver, String connectionString) {
            this.driver = driver;
            this.connectionString = connectionString;
        }

        public String getDriver() {
            return driver;
        }

        public String getConnectionString() {
            return connectionString;
        }
    }

    public Tablexia(String buildType, Locale systemLocale, SQLConnectionType sqlConnectionType, IConnectionManager connManager, ILegacyManager legacyManager, CameraOpener cameraOpener, IFileSystemManager fileSystemManager,
                    QRCodeScanner qrCodeScanner, String sentryDSN, boolean hasSoftBackButton, boolean supportAlternativeControls, boolean reset, String hwSerial) {
        this(buildType, systemLocale, sqlConnectionType, connManager, legacyManager, cameraOpener, fileSystemManager, qrCodeScanner, sentryDSN, hasSoftBackButton, supportAlternativeControls, reset, hwSerial, DEFAULT_HD_ASSETS, Gdx.net);
    }

    public Tablexia(String buildType, Locale systemLocale, SQLConnectionType sqlConnectionType, IConnectionManager connManager, ILegacyManager legacyManager, CameraOpener cameraOpener, IFileSystemManager fileSystemManager,
                    QRCodeScanner qrCodeScanner, String sentryDSN, boolean hasSoftBackButton, boolean supportAlternativeControls, boolean reset, String hwSerial, boolean hdAssets) {
        this(buildType, systemLocale, sqlConnectionType, connManager, legacyManager, cameraOpener, fileSystemManager, qrCodeScanner, sentryDSN, hasSoftBackButton, supportAlternativeControls, reset, hwSerial, hdAssets, Gdx.net);
    }

    public Tablexia(String buildType, Locale systemLocale, SQLConnectionType sqlConnectionType, IConnectionManager connManager, ILegacyManager legacyManager, CameraOpener cameraOpener, IFileSystemManager fileSystemManager,
                    QRCodeScanner qrCodeScanner, String sentryDSN, boolean hasSoftBackButton, boolean supportAlternativeControls, boolean reset, String hwSerial, boolean hdAssets, Net netImplementation) {
        this.reset = reset;
		this.connectionManager = validateConnectionManager(connManager);
        this.fileSystemManager = validateFileSystemManager(fileSystemManager);
        this.legacyManager = legacyManager;
        this.qrCodeScanner = qrCodeScanner;
        this.sqlConnectionType = sqlConnectionType;
        this.cameraOpener = cameraOpener;
        this.sentryDSN = sentryDSN;
        this.net = netImplementation;
        TablexiaSettings.init(buildType, systemLocale, hasSoftBackButton, supportAlternativeControls, hwSerial, hdAssets);
    }

    private void initializeBugReporting() {
        if(TablexiaSettings.getInstance().getBuildType().isBugReport()) {
            @SuppressWarnings("ConstantConditions")
            String DSN = (TablexiaBuildConfig.SENTRY_DSN_FALLBACK == null || sentryDSN == null || TablexiaBuildConfig.SENTRY_DSN_FALLBACK.equals(sentryDSN)) ? null : sentryDSN;
            TablexiaSentry.start(DSN);

            // register error handler for exceptions in event bus handler methods
            ApplicationBus.getInstance().addErrorHandler(new IPublicationErrorHandler() {
                @Override
                public void handleError(PublicationError error) {
                    Log.err(ApplicationBus.class, error.getMessage(), error.getCause());
                    TablexiaSentry.sendCustomException(TablexiaSentry.ExceptionType.JavaApplicationBusException, error.getMessage(), error.getCause());
                }
            });
        }
    }

    private void initializeLogging() {
        if (TablexiaSettings.getInstance().getBuildType().isLogFile()) {
            try {
                File file = TablexiaAbstractFileManager.RootStorageType.LOCAL.getResolver().resolve(TablexiaAbstractFileManager.RootStorageType.LOCAL.getStoragePath() + LOG_FILE_NAME).file();
                if (file.exists()) file.delete();
                file.getParentFile().mkdirs();
                file.createNewFile();
                PrintStream stream = new PrintStream(new FileOutputStream(file));
                TeeStream teeStream = new TeeStream(System.out, stream);
                System.setOut(new PrintStream(teeStream));
            } catch (IOException e) {
                Log.info(getClass(), "Cannot create a log file!");
                e.printStackTrace();
            }
        }
    }

    private void loadingComplete() {
        if (!loadingComplete) {
            loadingComplete = true;
            ApplicationBus.getInstance().publishAsync(new ApplicationLoadingCompleteEvent());
        }
    }

    private Class<? extends AbstractTablexiaScreen> showLastOrInitialScreen() {
        AbstractTablexiaScreen<?> screenToChange = TablexiaStorage.getInstance().getSavedScreen(TablexiaSettings.getInstance().getSelectedUser());
        if (screenToChange != null) {
            setScreenIfIsDifferent(screenToChange, ScreenTransaction.FADE);
        } else {
            ScreenTransaction screenTransaction = ScreenTransaction.FADE;
            if (TablexiaSettings.LOADER_SCREEN.equals(TablexiaApplication.getActualScreenClass())) {
                screenTransaction = ScreenTransaction.MOVE_LEFT;
            }
            screenToChange = Utility.getScreenForScreenClass(TablexiaSettings.INITIAL_SCREEN, null);
            setScreenIfIsDifferent(screenToChange, screenTransaction);
        }
        return screenToChange.getClass();
    }

    private void prepareUserRankManager() {
        UserRankManager.getInstance().refreshUserRankFull();
    }

    private void prepareMenu() {
        menuController = new MenuController(TablexiaSettings.getViewportWidth(getStage()), TablexiaSettings.getViewportHeight(getStage()));
        setBackButtonPosition();
        setMenuControllerBounds();
        addMenuController(menuController);
    }

    private void setMenuControllerBounds() {
        if(menuController != null) {
            menuController.setPosition(TablexiaSettings.getViewportLeftX(getStage()), TablexiaSettings.getViewportBottomY(getStage()));
            menuController.setSize(TablexiaSettings.getViewportWidth(getStage()), TablexiaSettings.getViewportHeight(getStage()));
        }
    }

    private void prepareBackButton() {
        backButton = new TablexiaButton(
                "",
                false,
                ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.BACK_BUTTON),
                ApplicationAtlasManager.getInstance().getTextureRegion(ApplicationAtlasManager.BACK_BUTTON_PRESSED),
                null,
                null);

        backButton.setName(BACK_BUTTON_NAME);
        setBackButtonPosition();

        backButton.setVisible(false);
        backButton.setInputListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ApplicationBus.getInstance().post(new BackButtonPressed()).asynchronously();
            }
        });

        setBackButtonVisibility(backButtonVisibility);
        setBackButton(backButton);
    }

    private void setBackButtonPosition() {
        if(backButton == null) return;

        backButton.setButtonBounds(
                    TablexiaSettings.getViewportLeftX(getStage()) + TablexiaSettings.getViewportWidth(getStage()) * BACK_BUTTON_LEFT_OFFSET_RATIO,
                    TablexiaSettings.getViewportBottomY(getStage()) + TablexiaSettings.getViewportHeight(getStage()) * BACK_BUTTON_BOTTOM_OFFSET,
                    BACK_BUTTON_SIZE,
                    BACK_BUTTON_SIZE)
            .adaptiveSizeRatio(BACK_BUTTON_ADAPTIVE_SIZE_RATIO)
            .adaptiveSizePositionFix(BACK_BUTTON_ADAPTIVE_SIZE_POSITION_FIX);
    }

    private void setBackButtonVisibility(boolean hasBackButton) {
        backButtonVisibility = hasBackButton;
        if (backButton != null) {
            if (hasBackButton) {
                if (!backButton.isVisible()) {
                    backButton.addAction(Actions.alpha(0f));
                    backButton.setVisible(true);
                    backButton.addAction(Actions.sequence(Actions.fadeIn(SHOW_BACK_BUTTON_DURATION)));
                }
            } else{
                if (backButton.isVisible()) {
                    backButton.addAction(Actions.sequence(Actions.fadeOut(HIDE_BACK_BUTTON_DURATION), Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            backButton.setVisible(false);
                        }
                    })));
                }
            }
        }
    }

    @Override
    protected void setMenuDimmerColor(Color color){
        if(menuController != null)
            menuController.setDimmerColor(color);
    }

    @Override
    protected void processNewScreen(AbstractTablexiaScreen<?> newScreen) {
        super.processNewScreen(newScreen);
        setBackButtonVisibility(newScreen.hasSoftBackButton());
    }

	@Override
    public void resize(int width, int height) {
        super.resize(width, height);
        setMenuControllerBounds();
        setBackButtonPosition();
    }

    private void startLoading(Locale locale) {
        // Prepare fonts (Fonts need to be loaded before any screen is active)
        ApplicationFontManager.getInstance().load();

        // sync loaded screen with loader image
        setScreen(Utility.getScreenForScreenClass(TablexiaSettings.LOADER_SCREEN, null));

        // async internal assets loading
		ApplicationTextManager.getInstance().load(locale);
        ApplicationInternalSoundManager.getInstance().load();
        ApplicationInternalTextureManager.getInstance().load();

        //Needs to be initialized before assetsManager starts loading.
        UserRankManager.initialize();
        TablexiaBadgesManager.initialize();

		// async zip extraction
        assetsManager = new TablexiaAssetsManager();

        if (TablexiaSettings.getInstance().isHdAssets() && TablexiaBuildConfig.ASSETS_HD_CHECKSUM == ""){
            Log.err(getClass(), "Application set to use HD assets, but doesn't have HD assets checksum --> Using DEFAULT pack instead.");
        }
        assetsManager.load(locale, Utility.createChecksumMapFromString(
                TablexiaSettings.getInstance().isUseHdAssets() ?
                        TablexiaBuildConfig.ASSETS_HD_CHECKSUM :
                        TablexiaBuildConfig.ASSETS_CHECKSUM));

		// async external assets loading
		ApplicationExternalSoundManager.getInstance().load();
		ApplicationAtlasManager.getInstance().load();

        loadingComplete = false;
    }

    private void clearLoadedData() {
        // clear main menu
        if (menuController != null) {
            menuController.dispose();
            menuController.remove();
            menuController = null;
        }

        // dispose all loaders
        ApplicationFontManager.getInstance().dispose();
        ApplicationTextManager.getInstance().dispose();
		ApplicationExternalSoundManager.getInstance().dispose();
        ApplicationAvatarManager.getInstance().dispose();
		ApplicationAtlasManager.getInstance().dispose();
		ApplicationInternalSoundManager.getInstance().dispose();
        ApplicationInternalTextureManager.getInstance().dispose();
    }


//////////////////////////// APPLICATION FLOW HANDLERS

    @Handler
    public void handleApplicationLoadingCompleteEvent(Tablexia.ApplicationLoadingCompleteEvent applicationLoadingCompleteEvent) {
        if (TablexiaSettings.getInstance().getSelectedUser() != null) {
            Gdx.app.postRunnable(new Runnable() {

                @Override
                public void run() {
                    showLastOrInitialScreen();
                }
            });
        }
    }

    @Handler
    public void handleSelectedUserEvent(final TablexiaSettings.SelectedUserEvent selectedUserEvent) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                if (selectedUserEvent.isUserSelected()) {
                    showLastOrInitialScreen();
                }
            }
        });
    }


//////////////////////////// LOCALE CHANGED HANDLER

    @Handler
    public void handleLocaleChangedEvent(TablexiaSettings.LocaleChangedEvent localeChangedEvent) {
        Gdx.app.postRunnable(new Runnable() {

            @Override
            public void run() {
                clearLoadedData();
                startLoading(TablexiaSettings.getInstance().getLocale());
            }
        });
    }


//////////////////////////// LIBGDX LIFECYCLE

    @Override
    public void create() {
        super.create();

        initializeBugReporting();
        // TODO: 2/28/19 add dialog on denied access
        initializeLogging();

        TablexiaSentry.sendSavedReports();

        // init data storage
        TablexiaStorage.init(sqlConnectionType, reset);

        // init database
        DatabaseManager.initializeDatabase();

        // init dialog factory
        TablexiaComponentDialogFactory.init(getStage());

        TablexiaSettings.getInstance().loadPreferences(reset);
        Log.setLoglevel(TablexiaSettings.getInstance().getLogLevel());
		
		// init event bus handlers
		ApplicationBus.getInstance().subscribe(this);

        // start loading application scope data
        startLoading(TablexiaSettings.getInstance().getLocale());
    }

    @Override
    public void render() {
        // render other screens
        super.render();

        if (screenshotListener != null) {
            obtainScreenshot();
            screenshotListener = null;
        }

        // process loading
        if (!loadingComplete) {
            // load internal assets
			if (!ApplicationFontManager.getInstance().update()) return;
            if (!ApplicationInternalSoundManager.getInstance().update()) return;
            if (!ApplicationInternalTextureManager.getInstance().update()) return;
            if (!processLanguageChoose()) return;
            if (!ApplicationTextManager.getInstance().update()) return;
            if (!ApplicationAvatarManager.getInstance().update()) return;

            if(!assetsManager.update()) return;

            // load external assets
            if (!ApplicationExternalSoundManager.getInstance().update()) return;
			if (!ApplicationAtlasManager.getInstance().update()) return;

            prepareBackButton();
            prepareUserRankManager();
            prepareMenu();
            loadingComplete();
        }
    }

    /**
     * Queues request for screenshot with this listener, it is called after obtaining screenshot.
     *
     * @param listener
     */
    public void takeScreenShot(ScreenshotListener listener) {
        this.screenshotListener = listener;
    }

    /**
     * Listener interface, that gets called after creating screenshot array
     */
    public interface ScreenshotListener {
        void screenshotTaken(Pixmap screen);
    }

    /**
     * Method launched during render phase before cleaning the buffer, creates bytearray of screen pixels and passes it to the listener
     */
    private void obtainScreenshot() {
		Gdx.gl.glFinish();
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();
		if (screenshotListener != null) {
			screenshotListener.screenshotTaken(ScreenUtils.getFrameBufferPixmap(0, 0, width, height));
		}
    }

    @Override
    public void dispose() {
        super.dispose();
        TablexiaStorage.getInstance().dispose();
        TablexiaSettings.getInstance().dispose();
        TablexiaBadgesManager.getInstance().dispose();
        TablexiaComponentDialogFactory.getInstance().dispose();
        clearLoadedData();
    }

//////////////////////////// Connection Manager

	private static IConnectionManager validateConnectionManager(IConnectionManager connectionManager) {
		if(connectionManager == null)
			throw new RuntimeException(Tablexia.class.getName() + ": Connection Manager equals to " + connectionManager);

		return connectionManager;
	}

	public static IConnectionManager getConnectionManager() {
		return connectionManager;
	}

//////////////////////////// File System Manager
    private static IFileSystemManager validateFileSystemManager(IFileSystemManager fileSystemManager) {
        if(fileSystemManager == null)
            throw new RuntimeException(Tablexia.class.getName() + ": File System Manager equals to " + fileSystemManager);

        return fileSystemManager;
    }

    public static IFileSystemManager getFileSystemManager() {
        return fileSystemManager;
    }

//////////////////////////// QR CODE READER
    public static boolean hasCameraOpener() {
        return cameraOpener != null;
    }

    public static CameraOpener getCameraOpener() {
        return cameraOpener;
    }

    public static boolean hasQRCodeScanner() {
        return cameraOpener != null && qrCodeScanner != null;
    }
    public static QRCodeScanner getQRCodeScanner() {
        return qrCodeScanner;
    }

//////////////////////////// Custom Net implementation for Android > 8.0
    public static Net getNet() {
        return net == null ? Gdx.net : net;
    }

    public static ILegacyManager getLegacyManager() {return legacyManager;}

//////////////////////////// APPLICATION LOADERS

    public void showLoaderErrorDialog(int dialogWidth, int dialogHeight, String loaderError) {
        ArrayList<TablexiaDialogComponentAdapter> components = new ArrayList<TablexiaDialogComponentAdapter>();

        components.add(new CenterPositionDialogComponent());
        components.add(new AdaptiveSizeDialogComponent());
        components.add(new DimmerDialogComponent(ERROR_DIALOG_DIMMER));
        components.add(new BackButtonHideComponent(LOADER_ERROR_DIALOG_HIDE_ANIMATION) {
            @Override
            public void onBackButtonPress(BackButtonPressed event) {
                super.onBackButtonPress(event);
                returnToLastLocale();
            }
        });

        components.add(new FixedSpaceContentDialogComponent());
        components.add(new ResizableSpaceContentDialogComponent());

        components.add(new TextContentDialogComponent(loaderError));

        components.add(new FixedSpaceContentDialogComponent());
        components.add(new ResizableSpaceContentDialogComponent());
        components.add(new PositiveNegativeButtonContentDialogComponent(
				new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						errorDialogShown = false;
					}
				},
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        returnToLastLocale();
                    }
                },
				TablexiaSettings.getInstance().hasLocaleChanged() ? PositiveNegativeButtonContentDialogComponent.PositiveNegativeButtonType.AGAIN_BACK : PositiveNegativeButtonContentDialogComponent.PositiveNegativeButtonType.AGAIN_EXIT
        ) {
            @Override
            protected boolean hasHideAnimation() {
                return LOADER_ERROR_DIALOG_HIDE_ANIMATION;
            }
        });
        components.add(new FixedSpaceContentDialogComponent());
        errorDialog = TablexiaComponentDialogFactory.getInstance().createDialog(components.toArray(new TablexiaDialogComponentAdapter[]{}));
        errorDialog.show(dialogWidth, dialogHeight);
    }
    
    private void showLanguageChooseDialog(List<TablexiaSettings.LocaleDefinition> availableLocale){
        languageDialogShown  = true;
        languageDialog = createLanguageChooseDialog(availableLocale);
        languageDialog.setName(LANGUAGE_REQUEST);
        languageDialog.show(LOCALE_DIALOG_WIDTH, LOCALE_DIALOG_HEIGHT);
    }
    
    private TablexiaComponentDialog createLanguageChooseDialog(List<TablexiaSettings.LocaleDefinition> localeDefinitions){
        ArrayList<TablexiaDialogComponentAdapter> adapters = new ArrayList<>();
        adapters.add(new CenterPositionDialogComponent());
        adapters.add(new BackButtonHideComponent(LOADER_ERROR_DIALOG_HIDE_ANIMATION) {
            @Override
            public void onBackButtonPress(BackButtonPressed event) {
                super.onBackButtonPress(event);
                Gdx.app.exit();
            }
        });

        for(final TablexiaSettings.LocaleDefinition localeDefinition : TablexiaSettings.LocaleDefinition.values()) {
            boolean isAvailable = localeDefinitions.contains(localeDefinition);
            if(localeDefinition.getNativeLocaleName() != null) {
                adapters.add(new SingleButtonContentDialogComponent(
                        new StandardTablexiaButton( localeDefinition.getNativeLocaleName(), StandardTablexiaButton.TablexiaButtonType.GREEN, false),
                        new ClickListener() {
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                Timer.schedule(new Timer.Task() {
                                    @Override
                                    public void run() {
                                        TablexiaSettings.getInstance().setLocale(localeDefinition);
                                        TablexiaSettings.languageChosen = true;
                                    }
                                }, LOCALE_CHANGE_DELAY);
                                languageDialog.hide();
                            }
                        },
                        isAvailable
                ));
            }
        }

        return TablexiaComponentDialogFactory.getInstance().createDialog(adapters.toArray(new TablexiaDialogComponentAdapter[]{}));
    }

    private void returnToLastLocale() {
        if (TablexiaSettings.getInstance().hasLocaleChanged()) {
            TablexiaSettings.getInstance().returnToLastLocale();
            errorDialogShown = false;
        } else {
            Gdx.app.exit();
        }
    }

    private boolean processLoaderError(String loaderError) {
        if (loaderError != null && !errorDialogShown) {
            showLoaderErrorDialog(ERROR_DIALOG_WIDTH, ERROR_DIALOG_HEIGHT, loaderError);
            errorDialogShown = true;
        } else if (loaderError == null && errorDialog != null) {
			errorDialog.hide();
		}
        return errorDialogShown;
    }

    private boolean processLanguageChoose() {
        if(!languageDialogShown && !TablexiaSettings.languageChosen) {
            showLanguageChooseDialog(TablexiaSettings.LocaleDefinition.getSelectableLocaleDefinitions());
        }
        return TablexiaSettings.languageChosen;
    }

    public static class ApplicationLoadingCompleteEvent implements ApplicationEvent {
        private ApplicationLoadingCompleteEvent(){}
    }
    
    public static class ApplicationLocaleResetEvent implements ApplicationEvent {
        public ApplicationLocaleResetEvent(){}
    }
    
    private static class ApplicationReturnToLastLocaleEvent implements ApplicationEvent {
        private ApplicationReturnToLastLocaleEvent(){} 
    }

    private void showNoValidPackageDialog(){
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                ArrayList<TablexiaDialogComponentAdapter> adapters = new ArrayList<>();

                adapters.add(new ResizableSpaceContentDialogComponent());
                adapters.add(
                        new TextContentDialogComponent(
                                ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.ASSETS_MANAGER_NO_VALID_PACKAGE),
                                true,
                                true));

                adapters.add(new ResizableSpaceContentDialogComponent());
                adapters.add(new SingleButtonContentDialogComponent(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_UNDERSTAND),
                        StandardTablexiaButton.TablexiaButtonType.RED,
                        new ClickListener(){
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                super.clicked(event, x, y);
                                Gdx.app.exit();
                            }
                        }));
                adapters.add(new BackButtonHideComponent(){
                    @Override
                    public void onBackButtonPress(BackButtonPressed event) {
                        Gdx.app.exit();
                    }
                });
                adapters.add(new FixedSpaceContentDialogComponent(1f/40));

                adapters.add(new AlertOnShowDialogComponent());
                adapters.add(new AdaptiveSizeDialogComponent());
                adapters.add(new CenterPositionDialogComponent());
                adapters.add(new DimmerDialogComponent());

                TablexiaComponentDialogFactory.getInstance().createDialog(adapters.toArray(new TablexiaDialogComponentAdapter[]{}))
                        .show(Tablexia.ERROR_DIALOG_WIDTH, Tablexia.ERROR_DIALOG_HEIGHT);
            }
        });
    }


//////////////////////////// CHANGE SCREEN EVENT

    public static class ChangeScreenEvent implements ApplicationEvent {

        private Class<? extends AbstractTablexiaScreen<?>>  screen;
        private ScreenTransaction                           screenTransaction;
        private Map<String, String>                         screenInitialState;

        private final Object[]                              parameters;

        public ChangeScreenEvent(Class<? extends AbstractTablexiaScreen<?>> screen, ScreenTransaction screenTransaction) {
            this(screen, screenTransaction, null);
        }

        public ChangeScreenEvent(Class<? extends AbstractTablexiaScreen<?>> screen, ScreenTransaction screenTransaction, Map<String, String> screenInitialState) {
            this(screen, screenTransaction, screenInitialState, new Object[]{});
        }

        public ChangeScreenEvent(Class<? extends AbstractTablexiaScreen<?>> screen, ScreenTransaction screenTransaction, Map<String, String> screenInitialState, Object ... parameters) {
            this.screen = screen;
            this.screenTransaction = screenTransaction;
            this.screenInitialState = screenInitialState;
            this.parameters = parameters;
        }

        public Class<? extends AbstractTablexiaScreen<?>> getScreen() {
            return screen;
        }

        public ScreenTransaction getScreenTransaction() {
            return screenTransaction;
        }

        public Map<String, String> getScreenInitialState() {
            return screenInitialState;
        }

        public Object[] getParameters() {
            return parameters;
        }
    }

    @Handler
    public void handleChangeScreenEvent(final ChangeScreenEvent changeScreenEvent) {
        final Class<? extends AbstractTablexiaScreen<?>> screenClass = changeScreenEvent.getScreen();
        if (!loadingComplete) {
            Log.err(((Object) this).getClass(), "Cannot change screen -> Application loading not complete!");
            return;
        }
        if (screenClass == null) {
            Log.err(((Object) this).getClass(), "Cannot change screen -> Received empty screen class!");
            return;
        }

        final AbstractTablexiaScreen<?>[] nextScreen = new AbstractTablexiaScreen<?>[1];
        final CountDownLatch countDownLatch = new CountDownLatch(1);

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                nextScreen[0] = Utility.getScreenForScreenClass(screenClass, changeScreenEvent.getScreenInitialState(), changeScreenEvent.getParameters());
                countDownLatch.countDown();
            }
        });

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            Log.info(getClass(), "Interrupted while waiting for screen object from class: " + screenClass, e);
            return;
        }

        if(getActualScreen().handleScreenExit(nextScreen[0], new Runnable() {
            @Override
            public void run() {
                ApplicationBus.getInstance().post(changeScreenEvent).asynchronously();
            }
        })) return;

        // create new screen on GL thread
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                setScreenIfIsDifferent(nextScreen[0], changeScreenEvent.getScreenTransaction());
            }
        });
    }
    
    @Handler
    public void handleApplicationLocaleResetEvent(ApplicationLocaleResetEvent applicationLocaleResetEvent){
        List<TablexiaSettings.LocaleDefinition> availableLocale = assetsManager.checkAvailablePackages();
        if (availableLocale.size() > 0){
            Log.debug(getClass().getSimpleName(), ": some lang. packs were found");

            ApplicationBus.getInstance().post(TablexiaAssetsManager.AssetsManagerEvent.loadFinished()).asynchronously();
            showLanguageChooseDialog(availableLocale);
        }else {
            Log.debug(getClass().getSimpleName(), ": nothing found");
            showNoValidPackageDialog();
        }
    }
    
    @Handler
    public void handleApplicationReturnToLastLocaleEvent(ApplicationReturnToLastLocaleEvent applicationReturnToLastLocaleEvent){
        TablexiaSettings.getInstance().returnToLastLocale();
    }
}