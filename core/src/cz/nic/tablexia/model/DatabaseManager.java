/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.game.ranksystem.RankProcessingScript;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.definitions.GameDefinition;
import cz.nic.tablexia.util.Log;

/**
 * Created by drahomir on 6/14/16.
 */
public class DatabaseManager {
    private interface VersionScriptExecutor {
        void executeVersionScript(TablexiaStorage storage, VersionScript versionScript) throws SQLException;
    }

    private static final VersionScriptExecutor DEFAULT_SCRIPT_EXECUTOR = new VersionScriptExecutor() {
        @Override
        public void executeVersionScript(TablexiaStorage storage, VersionScript versionScript) throws SQLException {
            Statement statement = storage.createStatement();
            statement.execute(versionScript.sqlStatement);
            statement.close();
        }
    };

    private enum VersionScript {
        VERSION_01( 1, "ALTER TABLE game ADD locale INTEGER NOT NULL DEFAULT " + Game.DEFAULT_GAME_LOCALE),
        VERSION_02( 2, "ALTER TABLE game ADD application_version_name TEXT NOT NULL DEFAULT '"  + Game.DEFAULT_APPLICATION_VERSION_NAME + "'"),
        VERSION_03( 3, "ALTER TABLE game ADD application_version_code INTEGER NOT NULL DEFAULT "  + Game.DEFAULT_APPLICATION_VERSION_CODE),
        VERSION_04( 4, "ALTER TABLE game ADD model_version_name TEXT NOT NULL DEFAULT '"  + Game.DEFAULT_MODEL_VERSION_NAME + "'"),
        VERSION_05( 5, "ALTER TABLE game ADD model_version_code INTEGER NOT NULL DEFAULT "  + Game.DEFAULT_MODE_VERSION_CODE),
        VERSION_06( 6, "ALTER TABLE game ADD build_type INTEGER NOT NULL DEFAULT " + Game.DEFAULT_BUILD_TYPE),
        VERSION_07( 7, "ALTER TABLE game ADD platform INTEGER NOT NULL DEFAULT " + Game.DEFAULT_PLATFORM),
        VERSION_08( 8, "ALTER TABLE game ADD hw_serial_number TEXT"),
        VERSION_09( 9, "ALTER TABLE game ADD rank_system_version_code INTEGER NOT NULL DEFAULT '" + Game.BACK_COMPATIBILITY_321_RANK_SYSTEM_VERSION_CODE + "'"),
        VERSION_10(10, "ALTER TABLE game ADD game_score_resolver_version INTEGER NOT NULL DEFAULT '" + GameDefinition.DEFAULT_GAME_RESOLVER_VERSION + "'"),
        VERSION_11(11, "CREATE TABLE custom_avatars (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER NOT NULL, data BLOB DEFAULT NULL, sync_at INTEGER, FOREIGN KEY(user_id) REFERENCES user(id))"),
        VERSION_12(12, "ALTER TABLE game ADD user_rank INTEGER NOT NULL DEFAULT " + Game.DEFAULT_USER_RANK),
        VERSION_13(13, "ALTER TABLE game ADD user_age INTEGER NOT NULL DEFAULT " + Game.DEFAULT_USER_AGE);


        private int version;
        private String sqlStatement;
        private VersionScriptExecutor versionScriptExecutor;

        VersionScript(int version, String sqlStatement) {
            this.version = version;
            this.sqlStatement = sqlStatement;
            this.versionScriptExecutor = DEFAULT_SCRIPT_EXECUTOR;
        }

        VersionScript(int version, String sqlStatement, VersionScriptExecutor versionScriptExecutor) {
            this.version = version;
            this.sqlStatement = sqlStatement;
            this.versionScriptExecutor = versionScriptExecutor;
        }

        public void execute(TablexiaStorage storage) throws SQLException{
            versionScriptExecutor.executeVersionScript(storage, this);
        }

        /**
         * Gets all version scripts newer than the current version (current version not included)
         * @param version
         * @return
         */
        public static List<VersionScript> getVersionScriptsFromVersion(int version) {
            List<VersionScript> versionScripts = new ArrayList<VersionScript>();

            for(VersionScript script  : VersionScript.values()) {
                if(script.version > version) versionScripts.add(script);
            }

            Collections.sort(versionScripts, new Comparator<VersionScript>() {
                @Override
                public int compare(VersionScript o1, VersionScript o2) {
                    return o1.version - o2.version;
                }
            });

            return versionScripts;
        }

        public static int getLastVersion() {
            int max = Integer.MIN_VALUE;

            for(VersionScript script : VersionScript.values()) {
                if(script.version > max)
                    max = script.version;
            }

            return max;
        }
    }

    private static final int   INITIAL_DATABASE_VERSION      = 0;

    public static final String CREATE_TABLE_DATABASE_VERSION = "CREATE TABLE IF NOT EXISTS dbversion (version INTEGER NOT NULL DEFAULT " + INITIAL_DATABASE_VERSION + ")";
    public static final String INSERT_DATABASE_VERSION       = "INSERT INTO dbversion(version) VALUES(?)";
    public static final String SELECT_DATABASE_VERSION       = "SELECT max(version) FROM dbversion";
    public static final String SELECT_DATABASE_VERSION_ALL   = "SELECT version FROM dbversion"; //Used to find out if there is any database version

    public static final String UPDATE_DATABASE_VERSION       = "UPDATE dbversion SET version = ?";

    private static final void convertDatabase() throws SQLException {
        int dbVersion = getDatabaseVersion();

        List<VersionScript> versionScripts = VersionScript.getVersionScriptsFromVersion(dbVersion);

        if(versionScripts.isEmpty()) {
            Log.info(DatabaseManager.class, "Version database is actual!");
            return;
        }

        try {
            Log.info(DatabaseManager.class, "Current database version is: " + dbVersion + ". Converting database to version: " + VersionScript.getLastVersion());

            TablexiaStorage.getInstance().setAutoCommit(false);

            for (VersionScript currVersionScript : versionScripts) {
                Log.info(DatabaseManager.class, "Executing version script: " + currVersionScript);
                currVersionScript.execute(TablexiaStorage.getInstance()); //Execute the script
            }

            Log.info(DatabaseManager.class, "All version scripts were executed successfully, committing changes!");
            TablexiaStorage.getInstance().commit();
            TablexiaStorage.getInstance().setAutoCommit(true);

            //Update DB version
            updateDatabaseVersion(VersionScript.getLastVersion());
        }
        catch (SQLException e) {
            //Something went wrong, rollbaaaack!
            TablexiaStorage.getInstance().rollback();
            e.printStackTrace();
        }
    }

    public static final void initializeDatabase() {
        try {
            Statement statement = TablexiaStorage.getInstance().createStatement();
            statement.execute(CREATE_TABLE_DATABASE_VERSION);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(!hasDatabaseVersion())
            insertInitialDatabaseVersion();

        try {
            convertDatabase();
        } catch (SQLException e) {
            throw new IllegalStateException("Couldn't migrate database to actual version. Reason: " + e.getMessage(), e);
        }
    }

    private static final void updateDatabaseVersion(int version) {
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(UPDATE_DATABASE_VERSION);
            statement.setInt(1, version);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static final void insertInitialDatabaseVersion() {
        Log.info(DatabaseManager.class, "Inserting initial database version into DB!");

        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(INSERT_DATABASE_VERSION);
            statement.setInt(1, INITIAL_DATABASE_VERSION);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            Log.err(Game.class, "Cannot insert database version into DB!", e);
        }
    }

    private static final boolean hasDatabaseVersion() {
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SELECT_DATABASE_VERSION_ALL);

            //Get result
            ResultSet resultSet = statement.executeQuery();

            boolean result = resultSet.next();

            //Clean
            statement.close();
            resultSet.close();

            return result;
        } catch (SQLException e) {
            Log.err(Game.class, "Cannot get database version from DB!", e);
        }
        return false;
    }

    private static final int getDatabaseVersion () {
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SELECT_DATABASE_VERSION);
            statement.execute();

            //Get result
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next()) throw new SQLException();

            int result = resultSet.getInt(1);

            //Clean
            statement.close();
            resultSet.close();

            return result;
        } catch (SQLException e) {
            Log.err(Game.class, "Cannot get database version from DB!", e);
        }

        throw new IllegalStateException("Cannot get database version!");
    }
}