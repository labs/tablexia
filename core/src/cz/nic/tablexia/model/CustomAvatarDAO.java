/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.util.Log;

/**
 * Created by drahomir on 1/10/17.
 */

public class CustomAvatarDAO {
    public static class CustomAvatarNotFoundException extends Exception {}

    private static final String INSERT_CUSTOM_AVATAR     = "INSERT INTO custom_avatars (user_id, data, sync_at) VALUES (?, ?, ?)";
    private static final String INSERT_NEW_CUSTOM_AVATAR = "INSERT INTO custom_avatars (user_id, data) VALUES (?, ?)";
    private static final String DELETE_CUSTOM_AVATAR     = "DELETE FROM custom_avatars WHERE user_id = ?";
    private static final String SELECT_CUSTOM_AVATAR     = "SELECT data FROM custom_avatars WHERE user_id = ?";

    private static final String MARK_AVATAR_AS_SYNC      = "UPDATE custom_avatars SET sync_at = ? WHERE user_id = ?";
    private static final String SELECT_AVATAR_SYNC_AT    = "SELECT sync_at FROM custom_avatars WHERE user_id = ?";

    public static void insertNewCustomAvatar(long userId, byte[] data) {
        try {
            deleteCustomAvatar(userId);

            PreparedStatement insertStatement = TablexiaStorage.getInstance().prepareStatement(INSERT_NEW_CUSTOM_AVATAR);
            insertStatement.setLong(1, userId);
            insertStatement.setBytes(2, data);

            insertStatement.executeUpdate();
            insertStatement.close();
        } catch (SQLException e) {
            Log.err(CustomAvatarDAO.INSERT_NEW_CUSTOM_AVATAR, "Couldn't insert custom avatar into database!", e);
        }
    }

    public static void deleteCustomAvatar(long userId) {
        try {
            PreparedStatement deleteStatement = TablexiaStorage.getInstance().prepareStatement(DELETE_CUSTOM_AVATAR);
            deleteStatement.setLong(1, userId);
            deleteStatement.execute();
            deleteStatement.close();
        } catch (SQLException e) {
            Log.err(CustomAvatarDAO.DELETE_CUSTOM_AVATAR, "Couldn't insert custom avatar into database!", e);
        }
    }

    public static void insertCustomAvatar(long userId, byte[] data, long syncAt) {
        try {
            PreparedStatement insertStatement = TablexiaStorage.getInstance().prepareStatement(INSERT_CUSTOM_AVATAR);
            insertStatement.setLong(1, userId);
            insertStatement.setBytes(2, data);
            insertStatement.setLong(3, syncAt);

            insertStatement.executeUpdate();
            insertStatement.close();
        } catch (SQLException e) {
            Log.err(CustomAvatarDAO.INSERT_CUSTOM_AVATAR, "Couldn't insert custom avatar into database!", e);
        }
    }

    public static boolean markAvatarAsSync(long userId, long syncAt) {
        try {
            PreparedStatement updateStatement = TablexiaStorage.getInstance().prepareStatement(MARK_AVATAR_AS_SYNC);
            updateStatement.setLong(1, syncAt);
            updateStatement.setLong(2, userId);
            int result = updateStatement.executeUpdate();
            updateStatement.close();
            return result == 1;
        }
        catch (SQLException e) {
            Log.err(CustomAvatarDAO.class, " Cannot mark avatar for user: " + userId + " as synchronized! syncAt = " + syncAt);
        }
        return false;
    }

    public static byte[] getCustomAvatarForID(long userId) {
        try {
            byte[] result = null;

            PreparedStatement selectStatement = TablexiaStorage.getInstance().prepareStatement(SELECT_CUSTOM_AVATAR);
            selectStatement.setLong(1, userId);

            ResultSet resultSet = selectStatement.executeQuery();
            if(resultSet.next()) {
                result = resultSet.getBytes("data");
            }
            selectStatement.close();

            if(result == null || result.length == 0) return null;

            return result;
        } catch (SQLException e) {
            Log.err(CustomAvatarDAO.class, "Couldn't get custom avatar for user id: " + userId, e);
        }
        return null;
    }

    public static Long getUserAvatarSyncAtTime(long userId) throws CustomAvatarNotFoundException {
        try {
            Long result;

            PreparedStatement selectStatement = TablexiaStorage.getInstance().prepareStatement(SELECT_AVATAR_SYNC_AT);
            selectStatement.setLong(1, userId);

            ResultSet resultSet = selectStatement.executeQuery();
            if(resultSet.next()) {
                long value = resultSet.getLong("sync_at"); //Returns 0 for null
                result = (value == 0) ? null : value; //Sets result to null if value == 0
            }
            else throw new CustomAvatarNotFoundException();

            selectStatement.close();
            return result;
        } catch (SQLException e) {
            Log.err(CustomAvatarDAO.class, "Couldn't get custom avatar sync at time for user id: " + userId, e);
        }
        return null;
    }
}