/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.model;

import io.sentry.event.Event;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaSentry;
import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.menu.user.UserAvatarDefinition;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.model.game.GamePauseDAO;
import cz.nic.tablexia.model.game.GameScoreDAO;
import cz.nic.tablexia.model.screen.ScreenDAO;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GamePause;
import cz.nic.tablexia.shared.model.Screen;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.UserDifficultySettings;
import cz.nic.tablexia.shared.model.definitions.GenderDefinition;
import cz.nic.tablexia.util.Log;

/**
 * Created by lhoracek, Matyáš Latner
 */
public class UserDAO {
    private static final int TRIES_COUNT_FOR_USER_CREATION = 3;

//////////////////////////// DB ACCESS

    public static final String CREATE_TABLE                 = "CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, age INTEGER NOT NULL, gender INTEGER NOT NULL, avatar TEXT NOT NULL, signature TEXT NOT NULL, deleted INTEGER NOT NULL DEFAULT 0, help INTEGER NOT NULL DEFAULT 1, intro INTEGER NOT NULL DEFAULT 1, uuid TEXT)";
    public static final String UPDATE_USER                  = "UPDATE user SET help = ?, intro = ?, deleted = ?, age = ?, avatar = ? WHERE id = ?";
    public static final String INSERT_USER                  = "INSERT INTO user (name, age, gender, avatar, signature, uuid) VALUES (?, ?, ?, ?, ?, ?)";
    public static final String SELECT_USER_FOR_ID           = "SELECT id, name, age, gender, avatar, signature, deleted, help, intro, uuid FROM user WHERE id = ?";
    public static final String SELECT_USER_FOR_UUID         = "SELECT id, name, age, gender, avatar, signature, deleted, help, intro, uuid FROM user WHERE uuid = ?";
    public static final String SELECT_LAST_USER             = "SELECT id, name, age, gender, avatar, signature, deleted, help, intro, uuid FROM user WHERE id IN (SELECT max(id) FROM user)";
    public static final String SELECT_ACTIVE_USERS          = "SELECT id, name, age, gender, avatar, signature, deleted, help, intro, uuid FROM user WHERE deleted = 0";
    public static final String SELECT_USER_ALREADY_EXISTS   = "SELECT 1 FROM user WHERE name = ?";
    public static final String UPDATE_USER_UUID             = "UPDATE user SET uuid = ? WHERE id = ?";

    public static final String SELECT_USERS_WITH_CUSTOM_AVATARS = "SELECT id, name, age, gender, avatar, signature, deleted, help, intro, uuid FROM user WHERE deleted = 0 AND avatar = " + UserAvatarDefinition.CUSTOM_AVATAR.number();

    public static List<User> selectActiveUsers() {
        ArrayList<User> users = new ArrayList<User>();
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SELECT_ACTIVE_USERS);
            try {
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    User user = fetchUser(resultSet);
                    users.add(user);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select active users from DB!", e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select active users from DB!", e);
        }
        return users;
    }

    public static User createUser(String name, int age, GenderDefinition gender, String avatar, String signature) {
        return createUser(name, age, gender, avatar, signature, null);
    }

    /**
     * Tries to create user, returns null on failure
     * @return created user or null on failure
     */
    public static User createUser(String name, int age, GenderDefinition gender, String avatar, String signature, String uuid) {
        User user = null;
        for (int currentTry = 1; currentTry <= TRIES_COUNT_FOR_USER_CREATION; currentTry++) {
            try {
                PreparedStatement insertStatement = TablexiaStorage.getInstance().prepareStatement(INSERT_USER);
                insertStatement.setString(1, name);
                insertStatement.setInt(2, age);
                insertStatement.setInt(3, gender.number());
                insertStatement.setString(4, avatar);
                insertStatement.setString(5, signature);
                insertStatement.setString(6, uuid);
                insertStatement.executeUpdate();
                insertStatement.close();
                // insertStatement.getGeneratedKeys().getLong(1) cannot be used -> getGeneratedKeys() is not implemented in SQLDroid
                user = selectLastUser();
                if (user != null) {
                    ApplicationBus.getInstance().post(new CreatedUserEvent(user)).asynchronously();
                }

                return user;
            }
            catch (Exception e) {
                //Prevents IllegalMonitorStateException on IOS devices, which is caused by
                //bad implementation of SQL JDBC Driver for IOS
                currentTry++;
            }
        }

        if (user == null) {
            TablexiaSentry.sendCustomMessage(UserDAO.class, "Couldn't create new user!", Event.Level.WARNING);
        }

        return user;
    }

    public static boolean updateUser(User user) {
        try {
            PreparedStatement updateStatement = TablexiaStorage.getInstance().prepareStatement(UPDATE_USER);
            updateStatement.setInt(1, user.isHelp() ? 1 : 0);
            updateStatement.setInt(2, user.isIntro() ? 1 : 0);
            updateStatement.setInt(3, user.isDeleted() ? 1 : 0);
            updateStatement.setInt(4, user.getAge());
            updateStatement.setInt(5, Integer.valueOf(user.getAvatar()));
            updateStatement.setLong(6, user.getId());
            updateStatement.executeUpdate();
            updateStatement.close();
            return true;
        } catch (SQLException e) {
            Log.err(User.class, "Cannot update user " + user.toString() + " in DB!", e);
            return false;
        }
    }

    public static User selectUser(long id) {
        User selectedUser = null;
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SELECT_USER_FOR_ID);
            statement.setLong(1, id);
            try {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    selectedUser = fetchUser(resultSet);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select user with id: " + id, e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select user with id: " + id, e);
        }
        return selectedUser;
    }

    public static User selectUserByUuid(String uuid) {
        User selectedUser = null;
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SELECT_USER_FOR_UUID);
            statement.setString(1, uuid);
            try {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    selectedUser = fetchUser(resultSet);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select user with uuid: " + uuid, e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select user with uuid: " + uuid, e);
        }
        return selectedUser;
    }

    public static User selectLastUser() {
        User selectedUser = null;
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SELECT_LAST_USER);
            try {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    selectedUser = fetchUser(resultSet);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select last user from DB", e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select last user from DB", e);
        }
        return selectedUser;
    }

    public static boolean updateUserUuid(long userId, String userUuid) {
        PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(UPDATE_USER_UUID);
        try {
            statement.setString(1, userUuid);
            statement.setLong(2, userId);
            int result = statement.executeUpdate();
            statement.close();
            return result == 1;
        } catch (SQLException e) {
            Log.err(UserDAO.class, "Failed to store user REST uuid", e);
        }

        return false;
    }

    public static boolean isTutorialForGameDefinition(GameDefinition gameDefinition, User user) {
        return GameDAO.getNumberOfCompleteGamesForGameDefinitionAndDifficulty(gameDefinition, GameDifficulty.TUTORIAL, user) < 1;
    }

    public static boolean userNameExists(String name) {
        boolean exists = false;
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SELECT_USER_ALREADY_EXISTS);
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                exists = (resultSet.getInt(1) == 1);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot check if user with username exists", e);
        }
        return exists;
    }

    private static User fetchUser(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        String name = resultSet.getString("name");
        int age = resultSet.getInt("age");
        GenderDefinition gender = GenderDefinition.getGenderDefinitionForGenderNumber(resultSet.getInt("gender"));
        String avatar = resultSet.getString("avatar");
        String signature = resultSet.getString("signature");
        boolean deleted = resultSet.getInt("deleted") != 0;
        boolean help = resultSet.getInt("help") != 0;
        boolean intro = resultSet.getInt("intro") != 0;
        String uuid = resultSet.getString("uuid");

        User user = new User(id, name, age, gender, avatar, signature, deleted, help, intro);
        user.setUuid(uuid);

        return user;
    }

    public static Long importUser(User user) {
       return importUser(user, true);
    }

    public static Long importUser(User user, boolean overwriteLocalDifficultySettings) {
        try {
            TablexiaStorage.getInstance().setAutoCommit(false);

            User dbUser = UserDAO.selectUserByUuid(user.getUuid());
            if (dbUser == null) {
                dbUser = createUser(user.getName(), user.getAge(), user.getGender(), user.getAvatar(), user.getSignature(), user.getUuid());
                Log.debug(UserDAO.class, "Creating new user with id: " + dbUser.getId());
            } else {
                if(dbUser.isDeleted()) {
                    Log.debug(UserDAO.class, "Recovering user that already exists with id: " + dbUser.getId());
                    dbUser.setDeleted(false);
                    updateUser(dbUser);
                } else {
                    Log.debug(UserDAO.class, "User already exists in tablexia with id: " + dbUser.getId());
                }
            }

            // fill in tablexia DB id to originally received user
            user.setId(dbUser.getId());

            if (user.getGames() != null) {
                for (Game game : user.getGames()) {

                    if (game.getStartTime() == null || game.getGameStartTime() == 0 || GameDAO.selectGameByTimes(dbUser.getId(), game.getGameStartTime(), game.getEndTime()) != null) {
                        continue;
                    }

                    Long gameId = GameDAO.importGame(dbUser, game);

                    if (gameId == null) {
                        continue;
                    }

                    if (game.getGamePauses() != null) {
                        for (GamePause pause : game.getGamePauses()) {
                            GamePauseDAO.importPause(gameId, pause.getStartTime(), pause.getEndTime());
                        }
                    }

                    if (game.getGameScoreMap() != null) {
                        GameScoreDAO.batchInsertGameScore(gameId, game.getGameScoreMap());
                    }
                }
            }

            if (user.getScreens() != null) {
                for (Screen screen : user.getScreens()) {
                    ScreenDAO.importScreen(dbUser.getId(), screen.getScreenName(), screen.getTime(), System.currentTimeMillis());
                }
            }

			if (user.getDifficultySettings() != null && overwriteLocalDifficultySettings) {
				for (UserDifficultySettings setting : user.getDifficultySettings()) {
					UserDifficultySettingsDAO.saveSettingsForUser(dbUser.getId(), setting.getGameNumber(), setting.getGameDifficulty());
				}
			}

            TablexiaStorage.getInstance().commit();
            TablexiaStorage.getInstance().setAutoCommit(true);

            return dbUser.getId();

        } catch (SQLException ex) {
            Log.err(UserDAO.class, "Failed to import user", ex);
            try {
                TablexiaStorage.getInstance().rollback();
            } catch (SQLException rollEx) {
                Log.err(UserDAO.class, "Failed to import user", rollEx);
            }
        }

        return null;
    }

    public static List<User> selectActiveUsersWithCustomAvatars() {
        ArrayList<User> users = new ArrayList<User>();
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SELECT_USERS_WITH_CUSTOM_AVATARS);
            try {
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    User user = fetchUser(resultSet);
                    users.add(user);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select active users from DB!", e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select active users from DB!", e);
        }
        return users;
    }

//////////////////////////// EVENT

    public static class CreatedUserEvent implements ApplicationBus.ApplicationEvent {
        private User user;

        private CreatedUserEvent(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }
    }
}