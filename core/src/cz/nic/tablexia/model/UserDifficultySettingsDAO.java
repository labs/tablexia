/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.UserDifficultySettings;
import cz.nic.tablexia.util.Log;

/**
 * Created by frantisek on 24.3.16.
 */
public class UserDifficultySettingsDAO {

//////////////////////////// DB ACCESS

	public static final String CREATE_TABLE							= "CREATE TABLE IF NOT EXISTS user_difficulty_setting (user_id INTEGER NOT NULL, game_number INTEGER NOT NULL, difficulty_number INTEGER NOT NULL, FOREIGN KEY(user_id) REFERENCES user(id), PRIMARY KEY(user_id, game_number))";
	public static final String SELECT_SETTINGS_FOR_USER				= "SELECT game_number, difficulty_number FROM user_difficulty_setting WHERE user_id = ?";
	public static final String SELECT_SETTINGS_FOR_USER_AND_GAME	= "SELECT difficulty_number FROM user_difficulty_setting WHERE user_id = ? AND game_number = ?";
	public static final String INSERT_SETTING_FOR_USER				= "REPLACE INTO user_difficulty_setting(user_id, game_number, difficulty_number) VALUES (?, ?, ?)";

	public static List<UserDifficultySettings> getUserSettings(long userId) {
		ArrayList<UserDifficultySettings> settings = new ArrayList<UserDifficultySettings>();
		try {
			PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SELECT_SETTINGS_FOR_USER);
			statement.setLong(1, userId);
			try {
				ResultSet resultSet = statement.executeQuery();
				while (resultSet.next()) {
					settings.add(new UserDifficultySettings(userId, resultSet.getInt("game_number"), resultSet.getInt("difficulty_number")));
				}
				resultSet.close();
			} catch (SQLException e) {
				Log.err(User.class, "Cannot select users game settings from DB!", e);
			}
			statement.close();
		} catch (SQLException e) {
			Log.err(User.class, "Cannot select users game settings from DB!", e);
		}
		return settings;
	}

	public static GameDifficulty getUserSettingsByGame(long userId, int gameNumber) {
		GameDifficulty difficulty = GameDifficulty.EASY;
		try {
			PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SELECT_SETTINGS_FOR_USER_AND_GAME);
			statement.setLong(1, userId);
			statement.setInt(2, gameNumber);
			try {
				ResultSet resultSet = statement.executeQuery();
				while (resultSet.next()) {
					difficulty = GameDifficulty.getGameDifficultyForDifficultyNumber(resultSet.getInt("difficulty_number"));
				}
			} catch (SQLException e) {
				Log.err(User.class, "Cannot select users game settings from DB!", e);
			}
			statement.close();
		} catch (SQLException e) {
			Log.err(User.class, "Cannot select users game settings from DB!", e);
		}

		return difficulty;
	}

	public static void saveSettingsForUser(long userId, int gameNumber, int difficultyNumber) {
		try {
			PreparedStatement insertStatement = TablexiaStorage.getInstance().prepareStatement(INSERT_SETTING_FOR_USER);
			insertStatement.setLong(1, userId);
			insertStatement.setInt(2, gameNumber);
			insertStatement.setInt(3, difficultyNumber);
			insertStatement.executeUpdate();
			insertStatement.close();

		} catch (SQLException ex) {
			Log.err(UserDifficultySettingsDAO.class, "Cannot insert new user difficulty settings record into DB!", ex);
		}
	}
}
