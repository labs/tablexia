/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.model.game;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.util.Log;

/**
 * Created by Matyáš Latner
 */
public class GameScoreDAO {

    private GameScoreDAO() {}

//////////////////////////// GAME SCORE API

    public static boolean createGameScore(Game game, GameScore score) {
        if (game != null && score.getKey() != null  && score.getValue() != null) {
            if (insertGameScore(game.getId(), score.getKey(), score.getValue())) {
                return true;
            }
        }
        return false;
    }

    public static boolean updateGameScore(Game game, GameScore score) {
        if (game != null && score.getKey() != null  && score.getValue() != null) {
            if (updateGameScore(game.getId(), score.getKey(), score.getValue())) {
                return true;
            }
        }
        return false;
    }


//////////////////////////// DB ACCESS

    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS game_score (game_id INTEGER NOT NULL, key TEXT NOT NULL, value TEXT NOT NULL, FOREIGN KEY(game_id) REFERENCES game(id), PRIMARY KEY(game_id, key))";
    public static final String GAME_SCORE_INSERT = "INSERT INTO game_score (game_id, key, value) VALUES (?, ?, ?)";
    public static final String GAME_SCORE_UPDATE = "UPDATE game_score SET value = ? WHERE game_id = ? AND key = ?";
    public static final String GAME_SCORE_SELECT = "SELECT key, value FROM game_score WHERE game_id = ?";

    public static boolean insertGameScore(long gameId, String key, String value) {
        try {
            PreparedStatement insertStatement = TablexiaStorage.getInstance().prepareStatement(GAME_SCORE_INSERT);
            insertStatement.setLong(1, gameId);
            insertStatement.setString(2, key);
            insertStatement.setString(3, value);
            float rowCount = insertStatement.executeUpdate();
            insertStatement.close();
            return rowCount > 0;
        } catch (SQLException e) {
            Log.err(GameScoreDAO.class, "Cannot insert new game score record into DB!", e);
        }
        return false;
    }

    public static void batchInsertGameScore(long gameId, List<GameScore> scores) {
        try {
            PreparedStatement insertStatement = TablexiaStorage.getInstance().prepareStatement(GAME_SCORE_INSERT);
            for (GameScore score : scores) {
                insertStatement.setLong(1, gameId);
                insertStatement.setString(2, score.getKey());
                insertStatement.setString(3, score.getValue());
                insertStatement.addBatch();
                Log.debug(GameScoreDAO.class, "importing game score for game id: " + gameId + ", " + score.getKey() + " - " + score.getValue());
            }
            insertStatement.executeBatch();
            insertStatement.close();
        } catch (SQLException e) {
            Log.err(GameScoreDAO.class, "Cannot insert new game score record into DB!", e);
        }
    }

    public static boolean updateGameScore(long gameId, String key, String value) {
        try {
            PreparedStatement updateStatement = TablexiaStorage.getInstance().prepareStatement(GAME_SCORE_UPDATE);
            updateStatement.setString(1, value);
            updateStatement.setLong(2, gameId);
            updateStatement.setString(3, key);
            float rowCount = updateStatement.executeUpdate();
            updateStatement.close();
            if (rowCount > 0) {
                return true;
            }
        } catch (SQLException e) {
            Log.err(GameScoreDAO.class, "Cannot update game score in DB!", e);
        }
        return false;
    }

    public static List<GameScore> selectGameScores(long gameId) {
        List<GameScore> gameScores = new ArrayList<GameScore>();
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SCORE_SELECT);
            statement.setLong(1, gameId);
            try {
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    String key = resultSet.getString(1);
                    String value = resultSet.getString(2);

                    gameScores.add(new GameScore(key, value));
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(GameScoreDAO.class, "Cannot select game scores from DB!", e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(GameScoreDAO.class, "Cannot select game scores from DB!", e);
        }
        return gameScores;
    }
}
