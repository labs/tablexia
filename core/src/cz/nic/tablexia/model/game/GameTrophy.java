/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.model.game;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.trophy.GameTrophyDefinition;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.util.Log;

/**
 * Created by frantisek on 3.11.15.
 */
public class GameTrophy {

    public interface GameTrophyResolver {
        boolean hasGameTrophy(User user, GameTrophyDefinition trophyDef);
    }

    public static class NumberOfTimesFinished implements GameTrophyResolver {

        public static final String GAME_PLAYED_SELECT = "SELECT COUNT(*) FROM game WHERE user_id = ? AND game_number = ? AND difficulty_number != ? AND end_time IS NOT NULL GROUP BY user_id, game_number";

        @Override
        public boolean hasGameTrophy(User user, GameTrophyDefinition trophyDef) {
            int playedCount = 0;
            try {
                PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_PLAYED_SELECT);
                statement.setLong(1, user.getId());
                statement.setInt(2, trophyDef.getGameDefinition().getGameNumber());
                statement.setInt(3, GameDifficulty.TUTORIAL.getDifficultyNumber());
                try {
                    ResultSet resultSet = statement.executeQuery();
                    if (resultSet.next()) {
                        playedCount = resultSet.getInt(1);
                    }
                    resultSet.close();
                } catch (SQLException e) {
                    Log.err(GameTrophy.class, "Cannot select number of games from DB!", e);
                }
                statement.close();
            } catch (SQLException e) {
                Log.err(GameTrophy.class, "Cannot select number of games from DB!", e);
            }
            return playedCount >= trophyDef.getLimit();
        }
    }

    public static class ThreeStarsReceived implements GameTrophyResolver {

    public static final String GAME_SELECT_FOR_GAME_AND_DIFFICULTY = "SELECT " + GameDAO.GAME_SELECT_COLUMNS + " FROM game WHERE game_number = ? AND difficulty_number = ? AND user_id = ? AND end_time IS NOT NULL";

        private static List<Game> getGamesForGameAndDifficulty(User user, GameTrophyDefinition trophyDef) {
            List<Game> games = new ArrayList<Game>();
            try {
                PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_FOR_GAME_AND_DIFFICULTY);
                statement.setInt(1, trophyDef.getGameDefinition().getGameNumber());
                statement.setInt(2, trophyDef.getLimit());
                statement.setLong(3, user.getId());
                try {
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        games.add(GameDAO.prepareGameObject(resultSet));
                    }
                    resultSet.close();
                } catch (SQLException e) {
                    Log.err(GameTrophy.class, "Cannot select game with user_id: " + user.getId() + ", game_number: " + trophyDef.getGameDefinition().getGameNumber() + ", difficulty: " + trophyDef.getLimit(), e);
                }
                statement.close();
            } catch (SQLException e) {
                Log.err(GameTrophy.class, "Cannot select game with user_id: " + user.getId() + ", game_number: " + trophyDef.getGameDefinition().getGameNumber() + ", difficulty: " + trophyDef.getLimit(), e);
            }
            return games;
        }

        @Override
        public boolean hasGameTrophy(User user, GameTrophyDefinition trophyDef) {
            List<Game> games = getGamesForGameAndDifficulty(user, trophyDef);

            if (games.isEmpty()) {
                return false;
            }

            for (Game game : games) {
                if (GameDAO.getGameResult(game).getStarCount() == AbstractTablexiaGame.GameResult.THREE_STAR.getStarCount()) {
                    return true;
                }
            }
            return false;
        }
    }
}
