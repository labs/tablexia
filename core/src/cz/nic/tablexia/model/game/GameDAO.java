/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.model.game;

import com.badlogic.gdx.utils.TimeUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.common.TablexiaRandom;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.ranksystem.RankProcessingScript;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GamePause;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.Log;

/**
 * Created by Matyáš Latner
 */
public class GameDAO {

    private static final Object         ATOMIC_LOCK                 = new Object();
    private static final String         STRING_ONLY_DIGITS_REGEX    = "[0-9]+";

    private static final AtomicInteger  gameScoreSetCounter         = new AtomicInteger(0);

//////////////////////////// Game API

    public static Game createGame(User user, GameDifficulty difficulty, GameDefinition gameDefinition, TablexiaRandom random, UserRankManager.UserRank userRank) {
        Game game = null;
        Long id = insertNewGame(user, difficulty, gameDefinition, random,userRank);
        if (id != null) {
            game = GameDAO.selectGameForId(id);
            game.setGamePauses(GamePauseDAO.selectGamePausesForGame(game));
        }
        return game;
    }

    public static Game getGameForId(long id) {
        return selectGameForId(id);
    }

	public static String getLastGameScoreValueForGameAndKey(int gameNumber, String key) {
		return selectLastGameScoreValue(TablexiaSettings.getInstance().getSelectedUser().getId(), gameNumber, key);
	}
	
	public static String getLastGameScoreValueForGameDifficultyAndKey(int difficultyNumber, int gameNumber, String key){
        return selectLastGameScoreValue(TablexiaSettings.getInstance().getSelectedUser().getId(), difficultyNumber, gameNumber, key);
    }

    public static List<Game> getGamesForUserAndDefinition(Long userId, GameDefinition definition) {
        return selectGamesForUserAndDefinition(userId, definition);
    }

    public static void startGame(Game game) {
        game.setStartTime(gameUpdateStart(game.getId(), TimeUtils.millis()));
    }

    public static void startGame(Game game, long startTime) {
        game.setStartTime(gameUpdateStart(game.getId(), startTime));
    }

    public static void endGame(Game game) {
        game.setEndTime(gameUpdateEnd(game.getId(), TimeUtils.millis()));
    }

    public static void endGame(Game game, long endTime) {
        game.setEndTime(gameUpdateEnd(game.getId(), endTime));
    }

    public static Long pauseGame(Game game) {
        if (!game.isFinished()) {
            return GamePauseDAO.createGamePause(game);
        }
        return null;
    }

    public static Long resumeGame(Game game) {
        if (!game.isFinished()) {
            return GamePauseDAO.updateGamePauseEnd(game);
        }
        return null;
    }

    public static Long getAllPausesDuration(Game game) {
        return getAllPausesDuration(game, true);
    }

	public static Long getAllPausesDuration(Game game, boolean useDatabase) {
		long pausesDuration = 0L;

        List<GamePause> gamePauses;

        if(useDatabase) gamePauses = GamePauseDAO.selectGamePausesForGame(game);
        else gamePauses = game.getGamePauses();

        if(gamePauses == null) return 0L;

		for (GamePause gamePause : gamePauses) {
			if (gamePause.hasStartTime() && gamePause.hasEndTime()) {
				pausesDuration = pausesDuration + (gamePause.getEndTime() - gamePause.getStartTime());
			}
		}
		return pausesDuration;
	}

    public static long getGameDuration(Game game) {
        return getGameDuration(game, true);
    }

	public static Long getGameDuration(Game game, boolean useDatabase) {
		if (game.getEndTime() == null || game.getGameStartTime() == null) {
			return null;
		}
		return game.getEndTime() - game.getGameStartTime() - getAllPausesDuration(game, useDatabase);
	}

    public static void setGameScores(final Game game, List<GameScore> scores) {
        if(scores == null) return;

        for(GameScore score : scores) {
            setGameScore(game, score.getKey(), score.getValue());
        }
    }

    public static void setGameScore(final Game game, final String key, final String value) {
        gameScoreSetCounter.getAndIncrement();
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (ATOMIC_LOCK) {
                    final GameScore storedScore = game.getGameScore(key);
                    if (storedScore == null) {
                        if (GameScoreDAO.insertGameScore(game.getId(), key, value)) {
                            game.getGameScoreMap().add(new GameScore(key, value));
                        }
                    } else {
                        if (!storedScore.getValue().equals(value) && GameScoreDAO.updateGameScore(game.getId(), key, value)) {
                            storedScore.setValue(value);
                        }
                    }
                    gameScoreSetCounter.getAndDecrement();
                }
            }
        }).start();
    }

    /**
     * Is there any background thread setting up game score values ?
     * @return
     */
    public static boolean isGameScoreActual() {
        return gameScoreSetCounter.get() == 0;
    }

    public static AbstractTablexiaGame.GameResult getGameResult(Game game) {
        return AbstractTablexiaGame.GameResult.getGameResultForGameResultDefinition(GameDefinition.getGameDefinitionForGameNumber(game.getGameNumber()).getGameResultResolver(game.getGameScoreResolverVersion()).getGameCupsResult(game));
    }

    public static Integer getNumberOfCompleteGamesForGameDefinitionAndDifficulty(GameDefinition gameDefinition, GameDifficulty gameDifficulty, User user) {
        return selectCompleteGameCountForGameNumberAndDifficultyNumber(gameDefinition.getGameNumber(), gameDifficulty.getDifficultyNumber(), user.getId());
    }

    public static Integer getNumberOfIncompleteAndCompleteGamesForGameDefinition(GameDefinition gameDefinition, User user) {
        return selectIncompleteAndCompleteGameCountForGameNumber(gameDefinition.getGameNumber(), user.getId());
    }


//////////////////////////// DB ACCESS

    public static final String GAME_SELECT_COLUMNS = "id, user_id, difficulty_number, game_number, random_seed, start_time, end_time, locale, application_version_name, application_version_code, model_version_name, model_version_code, build_type, platform, hw_serial_number, rank_system_version_code, game_score_resolver_version, user_rank, user_age";

    public static Game prepareGameObject(ResultSet resultSet) throws SQLException {
        Game game = new Game(
                resultSet.getLong(1),                                       //gameId
                UserDAO.selectUser(resultSet.getLong(2)),                   //user
                resultSet.getInt(3),                                        //difficultyNumber
                resultSet.getInt(4),                                        //gameNumber
                resultSet.getLong(5),                                       //randomSeed
                prepareLong(resultSet.getString(6)),                        //startTime
                prepareLong(resultSet.getString(7)),                        //endTime
                resultSet.getInt(8),                                        //locale
                resultSet.getString(9),                                     //applicationVersionName
                resultSet.getInt(10),                                       //applicationVersionCode
                resultSet.getString(11),                                    //modelVersionName
                resultSet.getInt(12),                                       //modelVersionCode
                resultSet.getInt(13),                                       //buildType
                resultSet.getInt(14),                                       //platform
                resultSet.getString(15),                                    //hwSerialNumber
                resultSet.getInt(16),                                       //rankSystemVersionCode
                resultSet.getInt(17),                                       //gameScoreResolverVersion
                resultSet.getInt(18),                                       //userRank
                resultSet.getInt(19)                                        //userAge
        );
        game.setGamePauses(GamePauseDAO.selectGamePausesForGame(game));
        game.getGameScoreMap().addAll(GameScoreDAO.selectGameScores(game.getId()));
        return game;
    }

    private static Long prepareLong(String longNumber) {
        if (longNumber != null && longNumber.matches(STRING_ONLY_DIGITS_REGEX)) {
            return Long.valueOf(longNumber);
        }
        return null;
    }

    // prepared statements
    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS game (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER NOT NULL, start_time INTEGER, end_time INTEGER, difficulty_number INTEGER NOT NULL, game_number INTEGER NOT NULL, random_seed INTEGER NOT NULL, sync_at INTEGER, FOREIGN KEY(user_id) REFERENCES user(id))";
    public static final String NEW_GAME_INSERT = "INSERT INTO game (user_id, difficulty_number, game_number, random_seed, locale, application_version_name, application_version_code, model_version_name, model_version_code, build_type, platform, hw_serial_number, rank_system_version_code, game_score_resolver_version, user_rank, user_age) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
    public static final String GAME_UPDATE_START = "UPDATE game SET start_time = ? WHERE id = ?";
    public static final String GAME_UPDATE_END = "UPDATE game SET end_time = ? WHERE id = ?";
    public static final String GAME_UPDATE_SYNC_AT = "UPDATE game SET sync_at = ? WHERE id = ?";
    public static final String GAME_SELECT_FOR_ID = "SELECT " + GAME_SELECT_COLUMNS + " FROM game WHERE id = ?";
    public static final String GAME_SELECT_LAST_ID = "SELECT max(id) FROM game";
    public static final String GAME_SELECT_COUNT_FOR_GAME_AND_DIFFICULTY = "SELECT count(id) FROM game WHERE game_number = ? AND difficulty_number = ? AND user_id = ? AND end_time IS NOT NULL";
    public static final String GAME_SELECT_FOR_USER_AND_DEFINITION = "SELECT " + GAME_SELECT_COLUMNS + " FROM game WHERE user_id = ? AND game_number = ? AND end_time IS NOT NULL AND end_time != 0 ORDER BY start_time ASC";
    public static final String GAME_SELECT_COUNT_FOR_GAME = "SELECT count(id) FROM game WHERE game_number = ? AND user_id = ?";
    public static final String GAME_SELECT_ALL_FOR_USER_SYNC = "SELECT " + GAME_SELECT_COLUMNS + " FROM game where user_id = ? AND sync_at is null";
    public static final String GAME_SELECT_BY_START_AND_END = "SELECT id FROM game WHERE user_id = ? AND start_time = ? AND end_time = ?";
    public static final String GAME_SELECT_BY_START_AND_NULL_END = "SELECT id FROM game WHERE user_id = ? AND start_time = ? AND end_time IS NULL";
    public static final String GAME_SELECT_BONUS_DIFFICULTY_UNLOCKED = "SELECT " + GAME_SELECT_COLUMNS + " FROM game WHERE user_id = ? AND difficulty_number = ? AND game_number = ? AND end_time IS NOT NULL";


    public static final String GAME_SELECT_LAST_SCORES_FOR_USER_AND_GAME = "SELECT value FROM game_score INNER JOIN game ON game_score.game_id=game.id AND game.user_id=? AND game.game_number=? WHERE game_score.key=? AND end_time IS NOT NULL ORDER BY game_id DESC LIMIT 1";
    public static final String GAME_SELECT_LAST_SCORES_FOR_USER_DIFFICULTY_AND_GAME = "SELECT value FROM game_score INNER JOIN game ON game_score.game_id=game.id AND game.user_id=? AND game.difficulty_number=? AND game.game_number=? WHERE game_score.key=? AND end_time IS NOT NULL ORDER BY game_id DESC LIMIT 1";
    public static final String GAME_SELECT_FOR_RANK_MANAGER = "SELECT " + GAME_SELECT_COLUMNS + " FROM game WHERE user_id = ? AND rank_system_version_code == ? AND start_time IS NOT NULL AND start_time != 0 AND end_time IS NOT NULL AND end_time != 0 ORDER BY end_time ASC";

    // classic statements
    public static final String IMPORT_GAME_INSERT = "INSERT INTO game (user_id, difficulty_number, game_number, random_seed, start_time, end_time, locale, application_version_name, application_version_code, model_version_name, model_version_code, build_type, platform, hw_serial_number, sync_at, rank_system_version_code, game_score_resolver_version, user_rank, user_age) VALUES (%d, %d, %d, %d, %s, %s, %d, '%s', %d, '%s', %d, %d, %d, '%s', %d, %d, %d, %d, %d)";


    private static Long insertNewGame(User user, GameDifficulty difficulty, GameDefinition gameDefinition, TablexiaRandom random, UserRankManager.UserRank userRank) {
        try {
            PreparedStatement insertStatement = TablexiaStorage.getInstance().prepareStatement(NEW_GAME_INSERT);
            insertStatement.setLong(1, user.getId());
            insertStatement.setInt(2, difficulty.getDifficultyNumber());
            insertStatement.setInt(3, gameDefinition.getGameNumber());
            insertStatement.setLong(4, random.getSeed());
            insertStatement.setInt(5, TablexiaSettings.getInstance().getLocaleDefinition().getLocaleNumber());
            insertStatement.setString(6, TablexiaSettings.getInstance().getApplicationVersionName());
            insertStatement.setInt(7, TablexiaSettings.getInstance().getApplicationVersionCode());
            insertStatement.setString(8, TablexiaSettings.getInstance().getModelVersionName());
            insertStatement.setInt(9, TablexiaSettings.getInstance().getModelVersionCode());
            insertStatement.setInt(10, TablexiaSettings.getInstance().getBuildType().getId());
            insertStatement.setInt(11, TablexiaSettings.getInstance().getPlatform().getId());
            insertStatement.setString(12, TablexiaSettings.getInstance().getHwSerialNumber());
            insertStatement.setInt(13, RankProcessingScript.getCurrentRankProcessingScript().getScriptVersion());
            insertStatement.setInt(14, gameDefinition.getGameScoreResolverVersion());
            insertStatement.setInt(15, userRank.getId());
            insertStatement.setInt(16, user.getAge());
            insertStatement.executeUpdate();
            insertStatement.close();
            // insertStatement.getGeneratedKeys().getLong(1) cannot be used -> getGeneratedKeys() is not implemented in SQLDroid
            return selectLastGameId();
        } catch (SQLException e) {
            Log.err(Game.class, "Cannot insert new game record into DB!", e);
        }
        return null;
    }

	private static String selectLastGameScoreValue(long userId, int gameNumber, String key) {
		String result = null;

		PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_LAST_SCORES_FOR_USER_AND_GAME);
		try {
			//Set userId, gameNumber and key
			statement.setLong(1, userId);
			statement.setLong(2, gameNumber);
			statement.setString(3, key);
			ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                result = resultSet.getString(1);
                resultSet.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    private static String selectLastGameScoreValue(long userId, int difficultyNumber, int gameNumber, String key) {
        String result = null;

        PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_LAST_SCORES_FOR_USER_DIFFICULTY_AND_GAME);
        try {
            //Set userId, difficultyNumber, gameNumber and key
            statement.setLong(1, userId);
            statement.setLong(2, difficultyNumber);
            statement.setLong(3, gameNumber);
            statement.setString(4, key);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                result = resultSet.getString(1);
                resultSet.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static Long importGame(User user, Game game) {
        try {
            Long gameId = null;
            Statement st = TablexiaStorage.getInstance().createStatement();
            Long startTime = game.getStartTime();
            Long endTime = game.getEndTime();
            // workaround for sync error -> serialized JSON from server contains 0 instead of NULL
            String sql = String.format(IMPORT_GAME_INSERT,
                    user.getId(),
                    game.getGameDifficulty(),
                    game.getGameNumber(),
                    game.getRandomSeed(),
                    startTime != 0 ? String.valueOf(startTime) : "NULL",
                    endTime != 0 ? String.valueOf(endTime) : "NULL",
                    game.getGameLocale(),
                    game.getApplicationVersionName(),
                    game.getApplicationVersionCode(),
                    game.getModelVersionName(),
                    game.getModelVersionCode(),
                    game.getBuildType(),
                    game.getPlatform(),
                    game.getHwSerialNumber(),
                    System.currentTimeMillis(),
                    game.getRankSystemVersionCode(),
                    game.getGameScoreResolverVersion(),
                    game.getUserRank(),
                    game.getUserAge());
            st.executeUpdate(sql);
            gameId = selectLastGameId();
            st.close();

            return gameId;
        } catch (SQLException ex) {
            Log.err(GameDAO.class, "Cannot import game record into DB!", ex);
        }

        return null;
    }

    public static Long selectGameByTimes(long userId, long startTime, Long endTime) throws SQLException {
        // workaround for sync error -> serialized JSON from server contains 0 instead of NULL
//        PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_BY_START_AND_END);
        PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement((endTime != null && endTime != 0) ? GAME_SELECT_BY_START_AND_END : GAME_SELECT_BY_START_AND_NULL_END);
        statement.setLong(1, userId);
        statement.setLong(2, startTime);

        // workaround for sync error -> serialized JSON from server contains 0 instead of NULL
//        statement.setLong(3, endTime);
        if (endTime != null && endTime != 0) {
            statement.setLong(3, endTime);
        }

        Long gameId = null;
        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            gameId = resultSet.getLong("id");
        }
        resultSet.close();
        statement.close();

        return gameId;
    }

    private static Game selectGameForId(long id) {
        Game selectedGame = null;
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_FOR_ID);
            statement.setLong(1, id);
            try {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    selectedGame = prepareGameObject(resultSet);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select game with id: " + id, e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select game with id: " + id, e);
        }
        return selectedGame;
    }

    public static List<Game> selectUsersGamesForSync(long userId) {
        List<Game> games = new ArrayList<Game>();
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_ALL_FOR_USER_SYNC);
            statement.setLong(1, userId);
            try {
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    games.add(prepareGameObject(resultSet));
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select games for user_id: " + userId, e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select games for user_id: " + userId, e);
        }
        return games;
    }

    public static List<Game> selectUserGamesForRankManager(long userId, int rankVersionCode) {
        List<Game> games = new ArrayList<Game>();
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_FOR_RANK_MANAGER);
            statement.setLong(1, userId);
            statement.setInt(2, rankVersionCode);
            try {

                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Game game = prepareGameObject(resultSet);
                    if(game.getStartTime() == null || game.getEndTime() == null) continue; //only games that were finished
                    games.add(game);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select games for user_id: " + userId, e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select games for user_id: " + userId, e);
        }
        return games;
    }

    private static List<Game> selectGamesForUserAndDefinition(long userId, GameDefinition definition) {

        List<Game> selectedGames = new ArrayList<Game>();

        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_FOR_USER_AND_DEFINITION);
            statement.setLong(1, userId);
            statement.setInt(2, definition.getGameNumber());

            try {
                ResultSet resultSet = statement.executeQuery();
                boolean hasNextGames = true;

                while (hasNextGames) {
                    if (resultSet.next()) {
                        selectedGames.add(prepareGameObject(resultSet));
                    } else {
                        hasNextGames = false;
                    }
                }

                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select game with user id : " + userId, e);
            }

            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select game with user id : " + userId, e);
        }

        return selectedGames;
    }

    private static Long selectLastGameId() {
        Long id = null;
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_LAST_ID);
            try {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    id = resultSet.getLong(1);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select game with id: " + id, e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select game with id: " + id, e);
        }
        return id;
    }

    private static int selectCompleteGameCountForGameNumberAndDifficultyNumber(int gameNumber, int difficultyNumber, long userId) {
        Integer count = null;
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_COUNT_FOR_GAME_AND_DIFFICULTY);
            try {
                statement.setInt(1, gameNumber);
                statement.setInt(2, difficultyNumber);
                statement.setLong(3, userId);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    count = resultSet.getInt(1);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select games count for game definition number: " + gameNumber + " and difficulty number: " + difficultyNumber, e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select games count for game definition number: " + gameNumber + " and difficulty number: " + difficultyNumber, e);
        }
        return count;
    }

    private static int selectIncompleteAndCompleteGameCountForGameNumber(int gameNumber, long userId) {
        Integer count = null;
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_COUNT_FOR_GAME);
            try {
                statement.setInt(1, gameNumber);
                statement.setLong(2, userId);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    count = resultSet.getInt(1);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(User.class, "Cannot select games count for game definition number: " + gameNumber, e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(User.class, "Cannot select games count for game definition number: " + gameNumber, e);
        }
        return count;
    }

    public static void markGamesAsSync(List<Game> games) {
        if (games == null) {
            return;
        }

        PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_UPDATE_SYNC_AT);
        try {
            try {
                TablexiaStorage.getInstance().setAutoCommit(false);
                for (Game game : games) {
                    statement.setLong(1, System.currentTimeMillis());
                    statement.setLong(2, game.getId());
                    statement.addBatch();
                }
                statement.executeBatch();
                statement.close();
                TablexiaStorage.getInstance().commit();
            } catch (SQLException ex) {
                TablexiaStorage.getInstance().rollback();
                Log.err(GameDAO.class, "Can't mark games as synchronized: " + ex.getMessage(), ex);
            } finally {
                TablexiaStorage.getInstance().setAutoCommit(true);
            }

        } catch (SQLException ex) {
            Log.err(GameDAO.class, "Can't mark games as synchronized: " + ex.getMessage(), ex);
        }
    }

    public static boolean isBonusDifficultyUnlocked(int gameNumber, long userId) {
        return requiredHardGamesCount(gameNumber, userId) >= GameDifficulty.BONUS_DIFFICULTY_UNLOCK_THRESHOLD;
    }

    public static int requiredHardGamesCount(int gameNumber, long userId) {
        List<Game> games = new ArrayList<>();
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_BONUS_DIFFICULTY_UNLOCKED);
            statement.setLong(1, userId);
            statement.setInt(2, GameDifficulty.HARD.getDifficultyNumber());
            statement.setInt(3, gameNumber);
            try {
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    games.add(GameDAO.prepareGameObject(resultSet));
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(GameTrophy.class, "Cannot select number of games from DB!", e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(GameTrophy.class, "Cannot select number of games from DB!", e);
        }

        int threeStarsHardGamesCount = 0;
        for (Game game : games) {
            if (GameDAO.getGameResult(game).getStarCount() == AbstractTablexiaGame.GameResult.THREE_STAR.getStarCount())
                threeStarsHardGamesCount++;
        }
        return threeStarsHardGamesCount;
    }

    private static Long gameUpdateStart(long id, long startTime) {
        try {
            PreparedStatement updateStatement = TablexiaStorage.getInstance().prepareStatement(GAME_UPDATE_START);
            updateStatement.setLong(1, startTime);
            updateStatement.setLong(2, id);
            float result = updateStatement.executeUpdate();
            updateStatement.close();
            if (result > 0) {
                return startTime;
            }
        } catch (SQLException e) {
            Log.err(Game.class, "Cannot update game start time in DB!", e);
        }
        return null;
    }

    private static Long gameUpdateEnd(long id, long endTime) {
        try {
            PreparedStatement updateStatement = TablexiaStorage.getInstance().prepareStatement(GAME_UPDATE_END);
            updateStatement.setLong(1, endTime);
            updateStatement.setLong(2, id);
            float result = updateStatement.executeUpdate();
            updateStatement.close();
            if (result > 0) {
                return endTime;
            }
        } catch (SQLException e) {
            Log.err(Game.class, "Cannot update game end time in DB!", e);
        }
        return null;
    }
}