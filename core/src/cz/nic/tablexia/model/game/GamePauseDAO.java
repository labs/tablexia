/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.model.game;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GamePause;
import cz.nic.tablexia.util.Log;

/**
 * Created by Matyáš Latner
 */
public class GamePauseDAO {

//////////////////////////// Pause API

    public static Long createGamePause(Game game) {
        long startTime = System.currentTimeMillis();
        if (insertGamePause(game.getId(), startTime)) {
            return startTime;
        }
        return null;
    }

    public static Long updateGamePauseEnd(Game game) {
        long endTime = System.currentTimeMillis();
        if (updateGamePauseEnd(game.getId(), endTime)) {
            return endTime;
        }
        return null;
    }

    public static List<GamePause> selectGamePausesForGame(Game game) {
        return selectGamePausesForGameId(game.getId());
    }


//////////////////////////// DB ACCESS

    // prepared statements
    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS game_pause (id INTEGER PRIMARY KEY AUTOINCREMENT, start_time INTEGER NOT NULL, end_time INTEGER)";
    public static final String CREATE_CONNECTION_TABLE = "CREATE TABLE IF NOT EXISTS game_pauses (id INTEGER PRIMARY KEY AUTOINCREMENT, game_id INTEGER NOT NULL, game_pause_id INTEGER NOT NULL, FOREIGN KEY(game_id) REFERENCES game(id), FOREIGN KEY(game_pause_id) REFERENCES game_pause(id))";

    public static final String PAUSE_INSERT = "INSERT INTO game_pause (start_time) VALUES (?)";
    public static final String PAUSE_SELECT_LAST_ID = "SELECT max(id) FROM game_pause";
    public static final String PAUSE_CONNECTION_INSERT = "INSERT INTO game_pauses (game_id, game_pause_id) VALUES (?, ?)";
    public static final String PAUSE_UPDATE_END_TIME = "UPDATE game_pause SET end_time = ? WHERE id IN (SELECT max(id) FROM game_pauses WHERE game_id = ?)";
    public static final String PAUSE_SELECT_FOR_GAME_ID = "SELECT id, start_time, end_time FROM game_pause WHERE id IN (SELECT game_pause_id FROM game_pauses WHERE game_id = ? ORDER BY id ASC)";

    // classic statements
    public static final String PAUSE_IMPORT = "INSERT INTO game_pause (start_time, end_time) VALUES (%d, %d)";

    private static boolean insertGamePause(long gameId, long startTime) {
        boolean result = false;
        try {
            TablexiaStorage.getInstance().setAutoCommit(false);
            PreparedStatement insertStatementPause = TablexiaStorage.getInstance().prepareStatement(PAUSE_INSERT);
            insertStatementPause.setLong(1, startTime);
            result = insertStatementPause.executeUpdate() > 0;
            insertStatementPause.close();
            // insertStatementPause.getGeneratedKeys().getLong(1) cannot be used -> getGeneratedKeys() is not implemented in SQLDroid
            long gamePauseId = selectGamePauseLastId();

            PreparedStatement insertStatementPauseConnection = TablexiaStorage.getInstance().prepareStatement(PAUSE_CONNECTION_INSERT);
            insertStatementPauseConnection.setLong(1, gameId);
            insertStatementPauseConnection.setLong(2, gamePauseId);
            result = result && insertStatementPauseConnection.executeUpdate() > 0;
            insertStatementPauseConnection.close();

            TablexiaStorage.getInstance().commit();
            TablexiaStorage.getInstance().setAutoCommit(true);
        } catch (SQLException e) {
            Log.err(GamePauseDAO.class, "Cannot insert game pause into DB!", e);
            try {
                TablexiaStorage.getInstance().rollback();
            } catch (SQLException e1) {
                Log.err(GamePauseDAO.class, "Cannot rollback game pause!", e);
            }
        }
        return result;
    }

    /**
     * Game pause import for server synchronization - don't use transaction inside. It is already managed by caller method
     *
     * @param gameId
     * @param startTime
     * @param endTime
     */
    public static void importPause(long gameId, long startTime, long endTime) {
        try {
            Long gamePauseId = null;
            Statement insertStatementPause = TablexiaStorage.getInstance().createStatement();
            insertStatementPause.executeUpdate(String.format(PAUSE_IMPORT, startTime, endTime));
            insertStatementPause.close();
            gamePauseId = selectGamePauseLastId();

            PreparedStatement insertStatementPauseConnection = TablexiaStorage.getInstance().prepareStatement(PAUSE_CONNECTION_INSERT);
            insertStatementPauseConnection.setLong(1, gameId);
            insertStatementPauseConnection.setLong(2, gamePauseId);
            insertStatementPauseConnection.executeUpdate();
            insertStatementPauseConnection.close();
        } catch (SQLException e) {
            Log.err(GamePauseDAO.class, "Cannot insert game pause into DB!", e);
            try {
                TablexiaStorage.getInstance().rollback();
            } catch (SQLException e1) {
                Log.err(GamePauseDAO.class, "Cannot rollback game pause!", e);
            }
        }
    }

    private static boolean updateGamePauseEnd(long gameId, long endTime) {
        boolean result = false;
        try {
            PreparedStatement updateStatementPause = TablexiaStorage.getInstance().prepareStatement(PAUSE_UPDATE_END_TIME);
            updateStatementPause.setLong(1, endTime);
            updateStatementPause.setLong(2, gameId);
            result = updateStatementPause.executeUpdate() > 0;
            updateStatementPause.close();
        } catch (SQLException e) {
            Log.err(GamePauseDAO.class, "Cannot update game pause in DB!", e);
            try {
                TablexiaStorage.getInstance().rollback();
            } catch (SQLException e1) {
                Log.err(GamePauseDAO.class, "Cannot update game pause in DB!", e);
            }
        }
        return result;
    }

    private static Long selectGamePauseLastId() {
        Long id = null;
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(PAUSE_SELECT_LAST_ID);
            try {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    id = resultSet.getLong(1);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(GamePauseDAO.class, "Cannot select game pause with id: " + id, e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(GamePauseDAO.class, "Cannot select game pause with id: " + id, e);
        }
        return id;
    }

    private static List<GamePause> selectGamePausesForGameId(long gameId) {
        List<GamePause> gamePauses = new ArrayList<GamePause>();
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(PAUSE_SELECT_FOR_GAME_ID);
            statement.setLong(1, gameId);
            try {
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Long id = resultSet.getLong(1);
                    Long startTime = resultSet.getLong(2);
                    Long endTime = resultSet.getLong(3);

                    gamePauses.add(new GamePause(id, startTime, endTime));
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(GameScoreDAO.class, "Cannot select game pauses from DB!", e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(GameScoreDAO.class, "Cannot select game pauses from DB!", e);
        }
        return gamePauses;
    }
}
