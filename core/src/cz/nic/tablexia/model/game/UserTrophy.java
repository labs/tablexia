/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.model.game;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.game.AbstractTablexiaGame;
import cz.nic.tablexia.game.GameDefinition;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.trophy.UserTrophyDefinition;
import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.Log;

/**
 * Created by frantisek on 4.11.15.
 * Edited by danilov on 22.6.16.
 */
public class UserTrophy {

    public interface UserTrophyResolver {
        boolean hasGameTrophy(User user, UserTrophyDefinition trophyDef);
    }

    public static class AllGamesInOneDay implements UserTrophyResolver {

        public static final String ALL_IN_ONE_DAY_SELECT = "SELECT game_number, end_time FROM game WHERE user_id = ? AND end_time IS NOT NULL AND difficulty_number != 0";

        @Override
        public boolean hasGameTrophy(User user, UserTrophyDefinition trophyDef) {
            HashMap<Calendar, Integer> games = new HashMap<Calendar, Integer>();
            try {
                PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(ALL_IN_ONE_DAY_SELECT);
                statement.setLong(1, user.getId());
                try {
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(resultSet.getLong(2));
                        games.put(calendar, resultSet.getInt(1));
                    }
                    resultSet.close();
                } catch (SQLException e) {
                    Log.err(UserTrophy.class, "Cannot select number of games from DB!", e);
                }
                statement.close();
            } catch (SQLException e) {
                Log.err(UserTrophy.class, "Cannot select number of games from DB!", e);
            }

            for(Calendar calendar1 : games.keySet()) {
                boolean[] allGamesPlayedArray = new boolean[GameDefinition.values().length];
                boolean allGamesPlayed = true;
                for (Calendar calendar2 : games.keySet()) {
                    if(calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR) && calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)) {
                        allGamesPlayedArray[games.get(calendar2) - 1] = true;
                    }
                }

                for(int i = 0; i < allGamesPlayedArray.length; i++) {
                    if(!allGamesPlayedArray[i]) {
                        allGamesPlayed = false;
                        break;
                    }
                }
                if(allGamesPlayed) return true;
            }
            return false;
        }
    }

    public static class ConsecutivelyGamePlayed implements UserTrophyResolver {

        public static final String ALL_FINISHED_GAMES_SELECT = "SELECT game_number, end_time" +
                " FROM game WHERE end_time IS NOT NULL AND user_id = ? ORDER BY end_time ASC";

        @Override
        public boolean hasGameTrophy(User user, UserTrophyDefinition trophyDef) {
            Map<String, Set<Integer>> gamesPlayed = getAllPlayedGames(user);

            for (String key : gamesPlayed.keySet()) {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date endDate = df.parse(key);
                    Calendar c = Calendar.getInstance();

                    Set<Integer> intersec = new HashSet<Integer>(gamesPlayed.get(key));
                    // check days after actual date if any already played game was played again
                    for (int i = 1; i < trophyDef.getLimit(); i++) {
                        c.setTime(endDate);
                        c.add(Calendar.DATE, i);
                        String newDate = df.format(c.getTime());
                        if (gamesPlayed.containsKey(newDate)) {
                            intersec.retainAll(gamesPlayed.get(newDate));
                        } else {
                            intersec.clear();
                            break;
                        }
                    }

                    // we have match for all days and at least one game
                    if (!intersec.isEmpty()) {
                        return true;
                    }

                } catch (ParseException e) {
                    Log.err(UserTrophy.class, e.getMessage(), e);
                }
            }
            return false;
        }

        private Map<String, Set<Integer>> getAllPlayedGames(User user) {
            Map<String, Set<Integer>> gamesPlayed = new HashMap<String, Set<Integer>>();
            try {
                PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(ALL_FINISHED_GAMES_SELECT);
                statement.setLong(1, user.getId());
                try {
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        int gameNumber = resultSet.getInt(1);
                        int endTime = resultSet.getInt(2);
                        Date endDate = new Date(endTime);
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        String dateTime = df.format(endDate).toString();
                        if (!gamesPlayed.containsKey(dateTime) || gamesPlayed.get(dateTime) == null) {
                            gamesPlayed.put(dateTime, new HashSet<Integer>());
                        }
                        gamesPlayed.get(dateTime).add(gameNumber);
                    }
                    resultSet.close();
                } catch (SQLException e) {
                    Log.err(UserTrophy.class, "Cannot select number of games from DB!", e);
                }
                statement.close();
            } catch (SQLException e) {
                Log.err(UserTrophy.class, "Cannot select number of games from DB!", e);
            }
            return gamesPlayed;
        }
    }

    public static class AllStarsGame implements UserTrophyResolver {

        public static final String GAME_SELECT_FOR_USER_AND_DIFFICULTY = "SELECT " + GameDAO.GAME_SELECT_COLUMNS + " FROM game WHERE difficulty_number = ? AND user_id = ? AND end_time IS NOT NULL";
        public static final String GAME_SELECT_FOR_USER = "SELECT " + GameDAO.GAME_SELECT_COLUMNS + " FROM game WHERE user_id = ? AND difficulty_number != ? AND start_time IS NOT NULL AND start_time != 0 AND end_time IS NOT NULL AND end_time != 0";

        private static List<Game> getGamesForUserAndDifficulty(User user, UserTrophyDefinition trophyDef) throws SQLException {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_FOR_USER_AND_DIFFICULTY);
            statement.setInt(1, trophyDef.getLimit());
            statement.setLong(2, user.getId());

            return getGamesByUserAndStatement(user, statement);
        }

        private static List<Game> getGamesForUser(User user) throws SQLException {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(GAME_SELECT_FOR_USER);
            statement.setLong(1, user.getId());
            statement.setInt(2, GameDifficulty.TUTORIAL.getDifficultyNumber());

            return getGamesByUserAndStatement(user, statement);
        }

        private static List<Game> getGamesByUserAndStatement(User user, PreparedStatement statement) {
            List<Game> games = new ArrayList<Game>();
            try {
                try {
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        games.add(GameDAO.prepareGameObject(resultSet));
                    }
                    resultSet.close();
                } catch (SQLException e) {
                    Log.err(UserTrophy.class, "Cannot select game with user_id: " + user.getId(), e);
                }
                statement.close();
            } catch (SQLException e) {
                Log.err(UserTrophy.class, "Cannot select game with user_id: " + user.getId(), e);
            }
            return games;
        }

        @Override
        public boolean hasGameTrophy(User user, UserTrophyDefinition trophyDef) {
            List<Game> games = new ArrayList<Game>();
            try {
                games = getGamesForUserAndDifficulty(user, trophyDef);
            } catch (SQLException e) {
                Log.err(UserTrophy.class, "Cannot select game with user_id: " + user.getId() + ", difficulty: " + trophyDef.getLimit(), e);
            }

            if(games.size() < GameDefinition.values().length) return false;
            int[] sixConsecutiveGames = new int[GameDefinition.values().length];

            for (Game game : games) {
                boolean duplicateFound = false;

                //shifting games in array
                for (int i = 0; i < sixConsecutiveGames.length - 1; i++) {
                    sixConsecutiveGames[i] = sixConsecutiveGames[i + 1];
                }
                sixConsecutiveGames[sixConsecutiveGames.length - 1] = game.getGameNumber();

                //if array sixConsecutiveGames has minimum number of games loaded, we can try achieve a trophy
                if(games.indexOf(game) + 1 >= GameDefinition.values().length) {
                    for (int i = 0; i < sixConsecutiveGames.length; i++) {
                        for (int j = i + 1; j < sixConsecutiveGames.length; j++) {
                            if (sixConsecutiveGames[i] == sixConsecutiveGames[j] ||
                                    GameDAO.getGameResult(game).getStarCount() != AbstractTablexiaGame.GameResult.THREE_STAR.getStarCount()) {
                                duplicateFound = true;
                                break;
                            }
                        }
                        if (duplicateFound) break;
                    }
                    if (!duplicateFound) return true;
                }
            }
            return false;
        }

        public static Map<GameDefinition, Integer> getNumberOfStarsResults(User user) {
            List<Game> games = new ArrayList<Game>();
            Map<GameDefinition, Integer> results = new HashMap<GameDefinition, Integer>();

            try {
                games = getGamesForUser(user);
            } catch (SQLException e) {
                Log.err(UserTrophy.class, "Cannot select game with user_id: " + user.getId(), e);
            }

            for (Game game : games) {
                GameDefinition gameDef = GameDefinition.getGameDefinitionForGameNumber(game.getGameNumber());
                if (!results.containsKey(gameDef)) {
                    results.put(gameDef, 0);
                }
                results.put(gameDef, results.get(gameDef) + GameDAO.getGameResult(game).getStarCount());
            }
            return results;
        }

        public static int getNumberOfAllStarsResults(User user) {
            Map<GameDefinition, Integer> results = getNumberOfStarsResults(user);

            int sum = 0;
            for (int star : results.values()) {
                sum += star;
            }

            return sum;
        }
    }
}
