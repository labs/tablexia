/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.model.screen;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.TablexiaStorage;
import cz.nic.tablexia.shared.model.Screen;
import cz.nic.tablexia.util.Log;

/**
 * Created by danilov on 1/27/16.
 */
public class ScreenDAO {

    public static final String SCREEN_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS screen (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INT NOT NULL, screen_name TEXT NOT NULL, time INT NOT NULL, sync_at INT, FOREIGN KEY(user_id) REFERENCES user(id))";
    public static final String SCREEN_INSERT = "INSERT INTO screen (user_id, screen_name, time) VALUES (?,?,?)";
    public static final String SCREEN_IMPORT = "INSERT INTO screen (user_id, screen_name, time, sync_at) SELECT ?, ?, ?, ? WHERE NOT EXISTS (SELECT 1 FROM screen WHERE user_id = ? AND time = ?)";
    public static final String SCREEN_SELECT = "SELECT time FROM screen WHERE user_id = ? AND screen_name = ?";
    public static final String SCREEN_SELECT_ALL_FOR_SYNC = "SELECT * FROM screen WHERE user_id = ? AND sync_at IS NULL";
    public static final String SCREEN_UPDATE_SYNC_AT = "UPDATE screen SET sync_at = ? WHERE id = ?";
    public static final String SCREEN_LAST_VISIT = "SELECT max(time) FROM screen WHERE user_id = ? AND screen_name = ?";


    /////API
    public static int getScreenViewCount(long userId, String screenName) {
        return selectScreenCount(userId, screenName);
    }

    public static void setScreenVisitTime(long userId, String screenName, long time) {
        insertScreen(userId, screenName, time);
    }

    public static long getScreenLastVisitTime(long userId, String screenName) {
        long result = 0;

        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SCREEN_LAST_VISIT);
            statement.setLong(1, userId);
            statement.setString(2, screenName);

            try {
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()) {
                    result = resultSet.getLong(1);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(ScreenDAO.class, "Cannot select time from DB!", e);
            }
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    ////DB ACCESS
    public static int selectScreenCount(long userId, String screenName) {
        int screenCount = 0;
        try {
            PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SCREEN_SELECT);
            statement.setLong(1, userId);
            statement.setString(2, screenName);
            try {
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    screenCount++;
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(ScreenDAO.class, "Cannot select count from DB!", e);
            }
            statement.close();
        } catch (SQLException e) {
            Log.err(ScreenDAO.class, "Cannot select count from DB!", e);
        }
        return screenCount;
    }

    public static List<Screen> selectAllScreenForSync(long userId) {
        List<Screen> screens = new ArrayList<Screen>();
        try {
            PreparedStatement selectStatement = TablexiaStorage.getInstance().prepareStatement(SCREEN_SELECT_ALL_FOR_SYNC);
            selectStatement.setLong(1, userId);
            try {
                ResultSet resultSet = selectStatement.executeQuery();
                while (resultSet.next()) {

                    String screenName = resultSet.getString("screen_name");
                    long time = resultSet.getLong("time");
                    long id = resultSet.getLong("id");

                    Screen screen = new Screen(id, userId, screenName, time);
                    screens.add(screen);
                }
                resultSet.close();
            } catch (SQLException e) {
                Log.err(ScreenDAO.class, "Cannot select screens for user_id: " + userId, e);
            }
            selectStatement.close();
        } catch (SQLException e) {
            Log.err(ScreenDAO.class, "Cannot select screens for user_id: " + userId, e);
        }
        return screens;
    }

    public static void insertScreen(long userId, String screenName, long time) {
        try {
            PreparedStatement insertStatement = TablexiaStorage.getInstance().prepareStatement(SCREEN_INSERT);
            insertStatement.setLong(1, userId);
            insertStatement.setString(2, screenName);
            insertStatement.setLong(3, time);
            insertStatement.executeUpdate();
            insertStatement.close();
        } catch (SQLException e) {
            Log.err(ScreenDAO.class, "Cannot insert screen row to DB!", e);
        }
    }

    public static boolean importScreen(long userId, String screenName, long time, Long sync_at) {
        int result = 0;
        try {
            PreparedStatement insertStatement = TablexiaStorage.getInstance().prepareStatement(SCREEN_IMPORT);
            insertStatement.setLong(1, userId);
            insertStatement.setString(2, screenName);
            insertStatement.setLong(3, time);
            insertStatement.setLong(4, sync_at);
            insertStatement.setLong(5, userId);
            insertStatement.setLong(6, time);
            result = insertStatement.executeUpdate();
            insertStatement.close();
            if (result == 1) Log.debug(ScreenDAO.class, "importing screen visit: " + screenName + ", " + time);
        } catch (SQLException e) {
            Log.err(ScreenDAO.class, "Cannot insert screen row to DB!", e);
        }
        return result == 1;
    }

    public static void markScreensAsSync(List<Screen> screens) {
        if (screens == null) {
            return;
        }

        PreparedStatement statement = TablexiaStorage.getInstance().prepareStatement(SCREEN_UPDATE_SYNC_AT);
        try {
            try {
                TablexiaStorage.getInstance().setAutoCommit(false);
                for (Screen screen : screens) {
                    statement.setLong(1, System.currentTimeMillis());
                    statement.setLong(2, screen.getId());
                    statement.addBatch();
                }
                statement.executeBatch();
                statement.close();
                TablexiaStorage.getInstance().commit();
            } catch (SQLException ex) {
                TablexiaStorage.getInstance().rollback();
                Log.err(ScreenDAO.class, "Can't mark screens as synchronized: " + ex.getMessage(), ex);
            } finally {
                TablexiaStorage.getInstance().setAutoCommit(true);
            }

        } catch (SQLException ex) {
            Log.err(ScreenDAO.class, "Can't mark screens as synchronized: " + ex.getMessage(), ex);
        }
    }

}