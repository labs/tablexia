/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;

import net.engio.mbassy.listener.Handler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.model.UserDifficultySettingsDAO;
import cz.nic.tablexia.model.game.GameDAO;
import cz.nic.tablexia.model.game.GamePauseDAO;
import cz.nic.tablexia.model.screen.ScreenDAO;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.model.game.GameScoreDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.Utility;

/**
 * Created by Matyáš Latner.
 */
public class TablexiaStorage {

    private static final Object mutex = new Object();
    public static final FileHandle  DATABASE_DIRECTORY  = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaAbstractFileManager.DataStorageType.EXTERNAL);
    public static final String      DATABASE_NAME       = "tablexia.db";

    private static volatile   TablexiaStorage instance = null;
    private         Connection      connection = null;

    private TablexiaStorage(Connection connection) {
        this.connection = connection;
        ApplicationBus.getInstance().subscribe(this);
    }

    static void init(Tablexia.SQLConnectionType connectionType, boolean reset) {
        if (instance == null) {
            if (!DATABASE_DIRECTORY.exists()) {
                DATABASE_DIRECTORY.mkdirs();
            }
            try {
                Class.forName(connectionType.getDriver());
                Connection connection = DriverManager.getConnection(connectionType.getConnectionString() + DATABASE_DIRECTORY.file().getAbsolutePath() + "/" + DATABASE_NAME);
                instance = new TablexiaStorage(connection);
                instance.initTables();
                instance.MigrateData();
                instance.reset(reset);
            } catch (ClassNotFoundException e) {
                Log.err(TablexiaStorage.class, "Cannot find class for database driver!", e);
            } catch (SQLException e) {
                Log.err(TablexiaStorage.class, "Cannot create database connection!", e);
            }
        } else {
            Log.err(TablexiaStorage.class, "Data storage has already been initialized! Skipping initialization...");
        }
    }

    void dispose() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                Log.err(getClass(), "Cannot close connection!", e);
                e.printStackTrace();
            }
        } else {
            throw new IllegalStateException("Data storage is not initialized or already closed!");
        }
        ApplicationBus.getInstance().unsubscribe(this);
        instance = null;
    }

    public static TablexiaStorage getInstance() {
        TablexiaStorage storage = instance;
        if (storage == null) {
            synchronized (mutex){
                storage = instance;
                if(storage == null){
                    throw new IllegalStateException("Data storage is not initialized!");
                }
            }
        }
        return storage;
    }

    public Statement createStatement() {
        try {
            return connection.createStatement();
        } catch (SQLException e) {
            Log.err(getClass(), "Cannot create statement!", e);
            return null;
        }
    }

    public PreparedStatement prepareStatement(String sql) {
        try {
            return connection.prepareStatement(sql);
        } catch (SQLException e) {
            Log.err(getClass(), "Cannot create prepared statement!", e);
            return null;
        }
    }

    public void setAutoCommit(boolean autoCommit) throws SQLException {
        connection.setAutoCommit(autoCommit);
    }

    public void commit() throws SQLException {
        connection.commit();
    }

    public void rollback() throws SQLException {
        connection.rollback();
    }


//////////////////////////// STORAGE

    private void initTables() {
        try {
            Statement statement = createStatement();
            statement.execute(UserDAO.CREATE_TABLE);
            statement.execute(GameDAO.CREATE_TABLE);
            statement.execute(GameScoreDAO.CREATE_TABLE);
            statement.execute(GamePauseDAO.CREATE_TABLE);
            statement.execute(GamePauseDAO.CREATE_CONNECTION_TABLE);
            statement.execute(SCREEN_STATE_CREATE_TABLE);
            statement.execute(ScreenDAO.SCREEN_CREATE_TABLE);
			statement.execute(UserDifficultySettingsDAO.CREATE_TABLE);
            statement.close();
        } catch (SQLException e) {
            Log.err(getClass(), "Cannot init Tablexia tables!", e);
        }
    }

    private void MigrateData() {
    }

    private void reset(boolean reset) {
        if (reset) {
            resetScreenState();
        }
    }


//////////////////////////// SCREEN STATE

    private static final    String  SCREEN_STATE_DROP_TABLE      = "DROP TABLE IF EXISTS screen_state";
    private static final    String  SCREEN_STATE_CREATE_TABLE    = "CREATE TABLE IF NOT EXISTS screen_state (key TEXT NOT NULL, value TEXT, screen TEXT NOT NULL, user_id INTEGER NOT NULL, PRIMARY KEY (key, screen, user_id), FOREIGN KEY(user_id) REFERENCES user(id))";
    private static final    String  SCREEN_STATE_INSERT          = "INSERT INTO screen_state(key, value, screen, user_id) VALUES (?, ?, ?, ?)";
    private static final    String  SCREEN_STATE_SELECT          = "SELECT key, value FROM screen_state WHERE screen = ? AND user_id = ?";
    private static final    String  SCREEN_CLASS_SELECT          = "SELECT screen FROM screen_state WHERE user_id = ? LIMIT 1";

    public static final     int     STATE_TABLE_KEY_INDEX       = 1;
    public static final     int     STATE_TABLE_VALUE_INDEX     = 2;
    public static final     int     STATE_TABLE_SCREEN_INDEX    = 3;
    public static final     int     STATE_TABLE_USERID_INDEX    = 4;

    public static final     int     STATE_QUERY_SCREEN_INDEX    = 1;
    public static final     int     STATE_QUERY_USERID_INDEX    = 2;

    private static final    int     SCREEN_QUERY_SCREEN_INDEX   = 1;
    private static final    int     SCREEN_QUERY_USERID_INDEX   = 1;


    public AbstractTablexiaScreen<?> getSavedScreen(User selectedUser) {
        AbstractTablexiaScreen<?> result = null;
        if (selectedUser != null) {
            try {
                PreparedStatement statement = prepareStatement(SCREEN_CLASS_SELECT);
                statement.setLong(SCREEN_QUERY_USERID_INDEX, selectedUser.getId());
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    String screenClassName = resultSet.getString(SCREEN_QUERY_SCREEN_INDEX);
                    if (screenClassName != null) {
                        try {
                            result = Utility.getScreenForScreenClass(ClassReflection.forName(screenClassName), null);
                        } catch (ReflectionException e) {
                            Log.err(getClass(), "Cannot load screen class: " + screenClassName, e);
                        }
                    }
                }
                resultSet.close();
                statement.close();
            } catch (SQLException e) {
                Log.err(getClass(), "Cannot load last screen from DB!", e);
            }
        }
        return result;
    }

    public void resetScreenState(AbstractTablexiaScreen screen) {
        if (screen.canResetState()) {
            resetScreenState();
        }
    }

    private void resetScreenState() {
        Statement statement = createStatement();
        try {
            statement.executeUpdate(SCREEN_STATE_DROP_TABLE);
            statement.executeUpdate(SCREEN_STATE_CREATE_TABLE);
            statement.close();
        } catch (SQLException e) {
            Log.err(getClass(), "Cannot reset screen state in DB!", e);
        }
    }

    public Map<String, String> loadScreenState(AbstractTablexiaScreen screen, User selectedUser) {
        Map<String, String> screenState = new HashMap<String, String>();
        if (selectedUser != null) {
            try {
                PreparedStatement statement = prepareStatement(SCREEN_STATE_SELECT);
                statement.setString(STATE_QUERY_SCREEN_INDEX, screen.getClass().getName());
                statement.setLong(STATE_QUERY_USERID_INDEX, selectedUser.getId());
                try {
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        screenState.put(resultSet.getString(STATE_TABLE_KEY_INDEX), resultSet.getString(STATE_TABLE_VALUE_INDEX));
                    }
                    resultSet.close();
                } catch (SQLException e) {
                    Log.err(getClass(), "Cannot select screen state from DB!", e);
                }
                statement.close();
            } catch (SQLException e) {
                Log.err(getClass(), "Cannot select screen state from DB!", e);
            }
        }
        return screenState;
    }

    public void saveScreenState(AbstractTablexiaScreen screen, Map<String, String> screenState, User selectedUser) {
        if (selectedUser != null) {
            try {
                connection.setAutoCommit(false);
                resetScreenState();

                try {
                    for (String key: screenState.keySet()) {
                        insertScreenState(key, screenState.get(key), screen.getClass().getName(), selectedUser);
                    }
                } catch (SQLException e) {
                    Log.err(getClass(), "Cannot insert screen state into DB!", e);
                }
                connection.commit();
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                Log.err(getClass(), "Cannot insert screen state into DB!", e);
            }
        }
    }

    private void insertScreenState(String key, String value, String screenClass, User selectedUser) throws SQLException {
        PreparedStatement insertStatement = prepareStatement(SCREEN_STATE_INSERT);
        insertStatement.setString(STATE_TABLE_KEY_INDEX, key);
        insertStatement.setString(STATE_TABLE_VALUE_INDEX, value);
        insertStatement.setString(STATE_TABLE_SCREEN_INDEX, screenClass);
        insertStatement.setLong(STATE_TABLE_USERID_INDEX, selectedUser.getId());
        insertStatement.executeUpdate();
        insertStatement.close();
    }

    @Handler
    public void handleSelectUserEvent(TablexiaSettings.SelectedUserEvent selectedUserEvent) {
        if (!selectedUserEvent.isUserSelected()) {
            resetScreenState();
        }
    }
}
