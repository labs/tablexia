/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.debug;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;

import net.engio.mbassy.listener.Handler;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import cz.nic.tablexia.TablexiaApplication.ScreenChangedEvent;
import cz.nic.tablexia.TablexiaBuildConfig;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.screen.AbstractTablexiaScreen.ScreenInfoEvent;
import cz.nic.tablexia.util.ui.TablexiaLabel;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;

/**
 * Debug info container
 *
 * @author Matyáš Latner
 */
public class DebugInfo extends Table implements Disposable {

    private static class DebugInfoComponent extends Table {

        private Map<String, TablexiaLabel> infoLabelMap;
        private TablexiaLabel.TablexiaLabelStyle labelStyle;

        public DebugInfoComponent() {
            infoLabelMap = new HashMap<String, TablexiaLabel>();
            setBackground(new TextureRegionDrawable(ApplicationAtlasManager.getInstance().getColorTextureRegion(BACKGROUND_COLOR)));
            addAction(alpha(BACKGROUND_ALPHA));
            labelStyle = new TablexiaLabel.TablexiaLabelStyle(ApplicationFontManager.FontType.REGULAR_12, FONT_COLOR);
        }

        public synchronized void setInfoValue(String infoKey, String infoValue) {
            TablexiaLabel label = infoLabelMap.get(infoKey);
            if (label == null) {
                createInfoLabel(infoKey, infoValue);
            } else {
                label.setText(infoKey + KEY_COLON + infoValue);
            }
        }

        private void clean() {
            clearChildren();
            infoLabelMap.clear();
        }

        private void createInfoLabel(String infoKey, String infoValue) {
            TablexiaLabel infoLabel = new TablexiaLabel(infoKey + KEY_COLON + infoValue, labelStyle);
            infoLabelMap.put(infoKey, infoLabel);
            clearChildren();

            for (TablexiaLabel label : infoLabelMap.values()) {
                Cell<TablexiaLabel> cell = add(label);
                if (getCells().size > 1) {
                    cell.pad(0, INFO_PADDING, 0, 0);
                }
            }
        }

        @Override
        public void setSize(float width, float height) {
            super.setSize(width, height);
        }
    }


    private static final String NATIVE_HEAP = "Native Heap";
    private static final String JAVA_HEAP = "Java Heap";
    private static final String FPS = "FPS";
    private static final String LOCALE = "Locale";
    private static final String VERSION = "Name";
    private static final String SERVER = "Server";
    private static final String USER = "User";
    private static final String NO_VALUE = "---";

    private static final String SCREEN_NAME = "Screen Name";

    private static final int INFO_PADDING = 20;
    private static final int MB_SIZE = 1000000;
    private static final String UNIT_MB = " MB";
    private static final String KEY_COLON = ": ";

    private static final Color FONT_COLOR = Color.WHITE;
    private static final Color BACKGROUND_COLOR = Color.BLACK;
    private static final float BACKGROUND_ALPHA = 0.5f;

    private DebugInfoComponent applicationDebugInfo;
    private DebugInfoComponent screenNameInfo;
    private DebugInfoComponent screenDebugInfo;

    private ConcurrentLinkedQueue<ScreenInfoEvent> screenDebugInfoQueue = new ConcurrentLinkedQueue<ScreenInfoEvent>();
    private ConcurrentLinkedQueue<ScreenChangedEvent> screenNameInfoQueue = new ConcurrentLinkedQueue<ScreenChangedEvent>();

    public static DebugInfo prepareDebugInfo(boolean applicationInfo, boolean screenInfo) {
        return applicationInfo || screenInfo ? new DebugInfo(applicationInfo, screenInfo) : null;
    }

    private DebugInfo(boolean applicationInfo, boolean screenInfo) {
        ApplicationBus.getInstance().subscribe(this);

        Table topTable = new Table();
        if (applicationInfo) {
            screenNameInfo = new DebugInfoComponent();
            topTable.add(screenNameInfo).expandX();
        }

        if (screenInfo) {
            screenDebugInfo = new DebugInfoComponent();
            topTable.add(screenDebugInfo).pad(0, INFO_PADDING, 0, 0).expandX();
            add(topTable);
        }

        row();
        add(new Container<Actor>()).expand();

        if (applicationInfo) {
            row();
            applicationDebugInfo = new DebugInfoComponent();
            add(applicationDebugInfo).expandX();
        }
    }

    public void update() {
        if (applicationDebugInfo != null && TablexiaSettings.getInstance() != null) {
            User selectedUser = TablexiaSettings.getInstance().getSelectedUser();
            applicationDebugInfo.setInfoValue(USER, selectedUser == null ? NO_VALUE : selectedUser.getName() + " (" + selectedUser.getId() + ")");
            applicationDebugInfo.setInfoValue(FPS, "" + Gdx.graphics.getFramesPerSecond());
            applicationDebugInfo.setInfoValue(JAVA_HEAP, ("" + Gdx.app.getJavaHeap() / MB_SIZE) + UNIT_MB);
            applicationDebugInfo.setInfoValue(NATIVE_HEAP, ("" + Gdx.app.getNativeHeap() / MB_SIZE) + UNIT_MB);
            applicationDebugInfo.setInfoValue(LOCALE, "" + TablexiaSettings.getInstance().getLocale());
            applicationDebugInfo.setInfoValue(VERSION, "" + TablexiaSettings.getInstance().getFullName());
            applicationDebugInfo.setInfoValue(SERVER, "" + TablexiaBuildConfig.TABLEXIA_SERVER_HOST);
        }

        if (screenDebugInfo != null) {
            while (!screenDebugInfoQueue.isEmpty()) {
                ScreenInfoEvent screenInfoEvent = screenDebugInfoQueue.poll();
                screenDebugInfo.setInfoValue(screenInfoEvent.getInfoKey(), screenInfoEvent.getInfoValue());
            }
        }

        if (screenNameInfo != null) {
            while (!screenNameInfoQueue.isEmpty()) {
                screenNameInfo.setInfoValue(SCREEN_NAME, "" + screenNameInfoQueue.poll().getScreenClass().getSimpleName());
            }
        }
    }

    public void onResize() {
        setBounds(0, getStage().getCamera().position.y - getStage().getHeight() / 2, getStage().getWidth(), getStage().getHeight());
    }

    @Handler
    public void handleScreenChangedEvent(ScreenChangedEvent screenChangedEvent) {
        screenDebugInfo.clean();
        screenNameInfoQueue.add(screenChangedEvent);
    }

    @Handler
    public void handleScreenInfoEvent(ScreenInfoEvent screenInfoEvent) {
        screenDebugInfoQueue.add(screenInfoEvent);
    }

    @Override
    public void dispose() {
        ApplicationBus.getInstance().unsubscribe(this);
    }
}
