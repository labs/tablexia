/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.ExternalFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.LocalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.util.Log;

public abstract class TablexiaAbstractFileManager extends AssetManager {

    private enum BuildTypeSuffix {

        DEBUG(TablexiaSettings.BuildType.DEBUG),
        ITEST(TablexiaSettings.BuildType.ITEST),
        DEVEL(TablexiaSettings.BuildType.DEVEL);

        private final TablexiaSettings.BuildType buildType;

        BuildTypeSuffix(TablexiaSettings.BuildType buildType) {
            this.buildType = buildType;
        }

        public String getSuffix() {
            return "_" + buildType.getKey() + "/";
        }

        public static String getSuffixForBuildType(TablexiaSettings.BuildType buildType) {
            for (BuildTypeSuffix buildTypeSuffix: BuildTypeSuffix.values()) {
                if (buildTypeSuffix.buildType == buildType) {
                    return buildTypeSuffix.getSuffix();
                }
            }
            return "/";
        }
    }

    public interface StorageType {
        String getStoragePath();
        FileHandleResolver getResolver();
    }

    private enum BuildTypeFileHandler {
        ITEST(TablexiaSettings.BuildType.ITEST, new ExternalFileHandleResolver()),
        INTERNAL(null, new InternalFileHandleResolver()),
        EXTERNAL(null, new ExternalFileHandleResolver()),
        LOCAL(null, new LocalFileHandleResolver());

        private TablexiaSettings.BuildType buildType;
        private FileHandleResolver fileHandlerResolver;

        BuildTypeFileHandler(TablexiaSettings.BuildType buildType, FileHandleResolver fileHandleResolver) {
            this.buildType = buildType;
            this.fileHandlerResolver = fileHandleResolver;
        }

        private static BuildTypeFileHandler getBuildTypeFileHandlerForBuildType(TablexiaSettings.BuildType buildType) {

            for (BuildTypeFileHandler buildTypeFileHandler: BuildTypeFileHandler.values()) {
                if (buildType.equals(buildTypeFileHandler.buildType) || buildType.equals(TablexiaSettings.BuildType.DEBUG)) {
                    return buildTypeFileHandler;
                }
            }
            // desktop version would save assets in user home directory, where mobile version would use application folder
            if (TablexiaSettings.getInstance().getPlatform().equals(TablexiaSettings.Platform.DESKTOP))
                return EXTERNAL;

            return LOCAL;
        }

        public FileHandleResolver getFileHandlerResolver() {
            return fileHandlerResolver;
        }
    }

    public enum RootStorageType implements StorageType {

        INTERNAL("", false, new InternalFileHandleResolver()),
        LOCAL(RootStorageType.TABLEXIA_DIRECTORY, true, BuildTypeFileHandler.getBuildTypeFileHandlerForBuildType(TablexiaSettings.getInstance().getBuildType()).getFileHandlerResolver()),
        LOCAL2(RootStorageType.TABLEXIA_DIRECTORY, true, new ExternalFileHandleResolver());

        public static final String TABLEXIA_DIRECTORY = ".tablexia";

        private String storagePath;
        private boolean useBuildTypeSuffix;
        private FileHandleResolver resolver;

        RootStorageType(String storagePath, boolean useBuildTypeSuffix, FileHandleResolver resolver) {
            this.storagePath = storagePath;
            this.useBuildTypeSuffix = useBuildTypeSuffix;
            this.resolver = resolver;
        }

        public String getStoragePath() {
            return storagePath + (useBuildTypeSuffix ? BuildTypeSuffix.getSuffixForBuildType(TablexiaSettings.getInstance().getBuildType()) : "");
        }

        public FileHandleResolver getResolver() {
            return resolver;
        }
    }

    public enum AssetsStorageType implements StorageType {

        INTERNAL(RootStorageType.INTERNAL, ""),
        EXTERNAL(RootStorageType.LOCAL, AssetsStorageType.ASSETS_DIRECTORY);

        public static final String ASSETS_DIRECTORY = "assets/";

        private String storagePath;
        private FileHandleResolver resolver;

        AssetsStorageType(RootStorageType rootStorageType, String assetsPath) {
            this.storagePath = rootStorageType.getStoragePath() + assetsPath;
            this.resolver = rootStorageType.getResolver();
        }

        public String getStoragePath() {
            return storagePath;
        }

        public FileHandleResolver getResolver() {
            return resolver;
        }
    }

    public enum DataStorageType implements StorageType {

        INTERNAL(RootStorageType.INTERNAL, DataStorageType.DATA_DIRECTORY),
        EXTERNAL(RootStorageType.LOCAL, DataStorageType.DATA_DIRECTORY);

        public static final String DATA_DIRECTORY = "data/";

        private String storagePath;
        private FileHandleResolver resolver;

        DataStorageType(RootStorageType rootStorageType, String assetsPath) {
            this.storagePath = rootStorageType.getStoragePath() + assetsPath;
            this.resolver = rootStorageType.getResolver();
        }

        public String getStoragePath() {
            return storagePath;
        }

        public FileHandleResolver getResolver() {
            return resolver;
        }
    }

    public enum DownloadStorageType implements StorageType {

        INTERNAL(RootStorageType.INTERNAL, DownloadStorageType.DOWNLOAD_DIRECTORY),
        EXTERNAL(RootStorageType.LOCAL2, DownloadStorageType.DOWNLOAD_DIRECTORY);

        public static final String DOWNLOAD_DIRECTORY = "download/";

        private String storagePath;
        private FileHandleResolver resolver;

        DownloadStorageType(RootStorageType rootStorageType, String assetsPath) {
            this.storagePath = rootStorageType.getStoragePath() + assetsPath;
            this.resolver = rootStorageType.getResolver();
        }

        public String getStoragePath() {
            return storagePath;
        }

        public FileHandleResolver getResolver() {
            return resolver;
        }
    }

    public enum KeystoreStorageType implements StorageType {

        INTERNAL(RootStorageType.INTERNAL, KeystoreStorageType.KEYSTORE_DIRECTORY),
        EXTERNAL(RootStorageType.LOCAL, KeystoreStorageType.KEYSTORE_DIRECTORY);

        public static final String KEYSTORE_DIRECTORY = "keystore/";

        private String storagePath;
        private FileHandleResolver resolver;

        KeystoreStorageType(RootStorageType rootStorageType, String assetsPath) {
            this.storagePath = rootStorageType.getStoragePath() + assetsPath;
            this.resolver = rootStorageType.getResolver();
        }

        public String getStoragePath() {
            return storagePath;
        }

        public FileHandleResolver getResolver() {
            return resolver;
        }
    }

    public enum ReportStorageType implements StorageType {

        INTERNAL(RootStorageType.INTERNAL, ReportStorageType.REPORTS_DIRECTORY),
        EXTERNAL(RootStorageType.LOCAL, ReportStorageType.REPORTS_DIRECTORY);

        public static final String REPORTS_DIRECTORY = "reports/";

        private String storagePath;
        private FileHandleResolver resolver;

        ReportStorageType(RootStorageType rootStorageType, String assetsPath) {
            this.storagePath = rootStorageType.getStoragePath() + assetsPath;
            this.resolver = rootStorageType.getResolver();
        }

        public String getStoragePath() {
            return storagePath;
        }

        public FileHandleResolver getResolver() {
            return resolver;
        }
    }

    public static FileHandle getFileStoragePathFileHandle(StorageType storageType, String additionalPath) {
        return storageType.getResolver().resolve(storageType.getStoragePath() + additionalPath);
    }

    public static FileHandle getFileStoragePathFileHandle(StorageType storageType) {
        return getFileStoragePathFileHandle(storageType, "");
    }

    public static String getFileStoragePath(StorageType storageType, String additionalPath) {
        return getFileStoragePathFileHandle(storageType, additionalPath).file().getAbsolutePath();
    }

    public static String getFileStoragePath(StorageType storageType) {
        return getFileStoragePathFileHandle(storageType).file().getAbsolutePath();
    }

    public static FileHandle getAssetsPackFileHandle(String additionalPath) {
        return getFileStoragePathFileHandle(TablexiaSettings.getInstance()
                .getPlatform().isAssetsBundled() ?
                TablexiaAbstractFileManager.DownloadStorageType.INTERNAL :
                TablexiaAbstractFileManager.DownloadStorageType.EXTERNAL,
                additionalPath);
    }

    public static String getAssetsPackStoragePath(){
        return getFileStoragePath(TablexiaSettings.getInstance().getPlatform().isAssetsBundled() ?
                DownloadStorageType.INTERNAL : DownloadStorageType.EXTERNAL);
    }

    private final StorageType storageType;

    public TablexiaAbstractFileManager(StorageType storageType) {
        super(storageType.getResolver());
        this.storageType = storageType;
    }

    public <T> void loadAsset(String assetFileName, Class<T> clazz) {
        loadAsset(assetFileName, clazz, null, true);
    }

    public <T> void loadAsset(String assetFileName, Class<T> clazz, boolean checkFileExistence) {
        loadAsset(assetFileName, clazz, null, checkFileExistence);
    }

    public <T> void loadAsset(String assetFileName, Class<T> clazz, AssetLoaderParameters<T> parameter, boolean checkFileExistence) {
        if (assetFileName != null) {
            String filePath = storageType.getStoragePath() + assetFileName;
            if (!checkFileExistence || storageType.getResolver().resolve(filePath).exists()) {
                load(filePath, clazz, parameter);
                return;
            }
        }
        Log.info(getClass(), "Asset file for name: " + assetFileName + " was not found!");
    }

    public <T> T getAsset(String assetName, Class<T> clazz) {
        return get(storageType.getStoragePath() + assetName, clazz);
    }

    @Override
    public synchronized boolean isLoaded (String fileName, Class type) {
        return super.isLoaded(storageType.getStoragePath() + fileName, type);
    }

    public StorageType getStorageType() {
        return storageType;
    }
}
