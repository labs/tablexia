/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;
import java.util.Map;

import cz.nic.tablexia.util.Log;

public class TablexiaAtlasManager extends TablexiaAbstractFileManager {

    public static final Color COLOR_OVERLAY        = new Color(0, 0, 0, 0.5f);

    private Map<Color, Texture> colorMap = new HashMap<Color, Texture>();

    public TablexiaAtlasManager() {
        super(AssetsStorageType.EXTERNAL);
    }

    public TablexiaAtlasManager(StorageType storageType) {
        super(storageType);
    }

    @Override
    public synchronized void dispose() {
        super.dispose();
        for (Texture texture : colorMap.values()) {
            texture.dispose();
        }
    }

    public void loadAtlas(String atlasName) {
        loadAtlas(atlasName, true);
    }

    public void loadAtlas(String atlasName, boolean checkAtlasExistence) {
        loadAsset(atlasName, TextureAtlas.class, checkAtlasExistence);
    }

    public TextureRegion getTextureRegionFromAtlas(String atlasName, String regionName, Integer index) {
        return getTextureRegionFromAtlas(atlasName, regionName, index, true);
    }

    public TextureRegion getTextureRegionFromAtlas(String atlasName, String regionName, Integer index, boolean printErrors) {
        if (!isLoaded(atlasName, TextureAtlas.class)) {
            if (printErrors) {
                Log.err(getClass(), "Cannot get region: " + regionName + " -> Atlas with name: " + atlasName + " not found!");
            }
            return null;
        }

        TextureAtlas atlas = getAsset(atlasName, TextureAtlas.class);
        TextureAtlas.AtlasRegion region;
        if (index == null) {
            region = atlas.findRegion(regionName);
        } else {
            region = atlas.findRegion(regionName, index);
        }
        if (region == null) {
            if (printErrors) {
                Log.err(getClass(), "Cannot get region: " + regionName + " -> Region with name: " + regionName + " not found!");
            }
            return null;
        }

        return region;
    }

    public Animation<TextureRegion> getAnimationFromAtlas(String atlasName, String regionName, int framesCount, float frameDuration) {
        TextureRegion[] textureRegions = new TextureRegion[framesCount];
        for (int i = 0; i < framesCount; i++) {
            textureRegions[i] = getTextureRegionFromAtlas(atlasName, regionName, i);
        }
        return new Animation<>(frameDuration, textureRegions);
    }

    public NinePatch getPatchFromAtlas(String atlasName, String patchName) {
        TextureAtlas atlas = getAsset(atlasName, TextureAtlas.class);
        if (atlas == null) {
            Log.err(getClass(), "Cannot get patch: " + patchName + " -> Atlas with name: " + atlasName + " not found!");
            return null;
        }

        NinePatch patch = atlas.createPatch(patchName);
        if (patch == null) {
            Log.err(getClass(), "Cannot get patch: " + patchName + " -> Patch with name: " + patchName + " not found!");
            return null;
        }

        return patch;
    }

    public Texture getColorTexture(Color color) {
        if (colorMap.get(color) == null) {
            Pixmap p = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
            p.setColor(color);
            p.drawPixel(0, 0);
            Texture texture = new Texture(p);
            p.dispose();
            colorMap.put(color, texture);
        }
        return colorMap.get(color);
    }

    public TextureRegion getColorTextureRegion(Color color) {
       return new TextureRegion(getColorTexture(color),1,1);
    }
}
