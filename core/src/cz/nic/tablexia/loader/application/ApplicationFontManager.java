/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.application;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.DistanceFieldFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

import cz.nic.tablexia.loader.IApplicationLoader;

/**
 * Font loader and manager class for application context
 * 
 * @author Matyáš Latner
 *
 */
public class ApplicationFontManager implements IApplicationLoader {

	public enum FontType {
		REGULAR_10  (10, false, false),
		REGULAR_12  (12, false, false),
		REGULAR_14  (14, false, false),
		REGULAR_16  (16, false, false),
		REGULAR_18  (18, false, false),
		REGULAR_20  (20, false, false),
		REGULAR_26  (26, false, false),
		REGULAR_30  (30, false, false),
		REGULAR_35  (35, false, false),
		REGULAR_40  (40, false, false),
		REGULAR_45  (45, false, false),
		REGULAR_50  (50, false, false),
		REGULAR_55  (55, false, false),

		BOLD_10  (10, true, false),
		BOLD_12  (12, true, false),
		BOLD_14  (14, true, false),
		BOLD_16  (16, true, false),
		BOLD_18  (18, true, false),
		BOLD_20  (20, true, false),
		BOLD_26  (26, true, false),
		BOLD_30  (30, true, false),
		BOLD_35  (35, true, false),
		BOLD_40  (40, true, false),
		BOLD_45  (45, true, false),
		BOLD_50  (50, true, false),
		BOLD_55  (55, true, false),
		BOLD_65  (65, true, false),

		ENCYCLOPEDIA_REGULAR_10  (10, false, true),
		ENCYCLOPEDIA_REGULAR_12  (12, false, true),
		ENCYCLOPEDIA_REGULAR_14  (14, false, true),
		ENCYCLOPEDIA_REGULAR_16  (16, false, true),
		ENCYCLOPEDIA_REGULAR_18  (18, false, true),
		ENCYCLOPEDIA_REGULAR_20  (20, false, true),
		ENCYCLOPEDIA_REGULAR_26  (26, false, true),
		ENCYCLOPEDIA_REGULAR_30  (30, false, true),
		ENCYCLOPEDIA_REGULAR_35  (35, false, true),
		ENCYCLOPEDIA_REGULAR_40  (40, false, true),
		ENCYCLOPEDIA_REGULAR_45  (45, false, true),
		ENCYCLOPEDIA_REGULAR_50  (50, false, true),
		ENCYCLOPEDIA_REGULAR_55  (55, false, true),

		ENCYCLOPEDIA_BOLD_10  (10, true, true),
		ENCYCLOPEDIA_BOLD_12  (12, true, true),
		ENCYCLOPEDIA_BOLD_14  (14, true, true),
		ENCYCLOPEDIA_BOLD_16  (16, true, true),
		ENCYCLOPEDIA_BOLD_18  (18, true, true),
		ENCYCLOPEDIA_BOLD_20  (20, true, true),
		ENCYCLOPEDIA_BOLD_26  (26, true, true),
		ENCYCLOPEDIA_BOLD_30  (30, true, true),
		ENCYCLOPEDIA_BOLD_35  (35, true, true),
		ENCYCLOPEDIA_BOLD_40  (40, true, true),
		ENCYCLOPEDIA_BOLD_45  (45, true, true),
		ENCYCLOPEDIA_BOLD_50  (50, true, true),
		ENCYCLOPEDIA_BOLD_55  (55, true, true),
		ENCYCLOPEDIA_BOLD_65  (65, true, true);

		int size;
		boolean bold;
		boolean encyclopediaFont;

		FontType(int size, boolean bold, boolean encyclopediaFont) {
			this.size = size;
			this.bold = bold;
			this.encyclopediaFont = encyclopediaFont;
		}

		public static FontType getProximateFontTypeForSize(int size, boolean bold) {
			FontType result = null;
			for (FontType fontType : FontType.values()) {
				if (result == null || (fontType.bold == bold && Math.abs(fontType.size - size) < Math.abs(result.size - size))) {
					result = fontType;
				}
			}
			return result;
		}

		public int getSize() {
			return size;
		}

		public boolean isBold() {
			return bold;
		}

		public boolean isEncyclopediaFont() {
			return encyclopediaFont;
		}

		public float getScale() {
			return this.size/DISTANCE_FIELD_FONT_SIZE;
		}
	}

	private static final String		ROBOTO_REGULAR_FONT_FILE = "font/roboto-regular";
	private static final String		ROBOTO_BOLD_FONT_FILE    = "font/roboto-bold";
	private static final String 	FONT_FILE_EXTENSION		 = ".fnt";
	private static final String 	FONT_BITMAP_EXTENSION	 = ".png";

	public static final float		DISTANCE_FIELD_FONT_SIZE = 32;
	private static final float  	ENCYCLOPEDIA_LINE_HEIGHT = 50;

	//According to (https://github.com/libgdx/libgdx/wiki/Distance-field-fonts) it should be 8/scale... I feel like breaking the rules a little bit :/
	public static final float		DISTANCE_FIELD_FONT_DEFAULT_SMOOTHING = 6;

	private static ApplicationFontManager instance;

	private DistanceFieldFont robotoRegularFont;
	private DistanceFieldFont robotoBoldFont;
	//encyklopedia font - extra spacing (lineHeight)
	private DistanceFieldFont encyclopediaRobotoRegularFont;
	private DistanceFieldFont encyclopediaRobotoBoldFont;

	private ShaderProgram distanceFieldShader;
	

	private ApplicationFontManager() {
	}
	
	public static ApplicationFontManager getInstance() {
		if (instance == null) {
			instance = new ApplicationFontManager();
		}
		return instance;
	}

	public ShaderProgram getDistanceFieldShader() {
		return distanceFieldShader;
	}

	public DistanceFieldFont getDistanceFieldFont(FontType fontType) {
		if(!isLoaded()) load();
		if(fontType.isEncyclopediaFont()){
			return fontType.isBold() ? encyclopediaRobotoBoldFont : encyclopediaRobotoRegularFont;
		}
		return fontType.isBold() ? robotoBoldFont : robotoRegularFont;
	}
	
	public void load() {
		if(robotoRegularFont != null) robotoRegularFont.dispose();
		Texture robotoRegular = new Texture(Gdx.files.internal(ROBOTO_REGULAR_FONT_FILE + FONT_BITMAP_EXTENSION), true);
		robotoRegular.setFilter(Texture.TextureFilter.MipMapLinearNearest, Texture.TextureFilter.MipMapLinearNearest);
		robotoRegularFont = createRobotoFont(robotoRegular, ROBOTO_REGULAR_FONT_FILE + FONT_FILE_EXTENSION);

		if(robotoBoldFont != null) robotoBoldFont.dispose();
		Texture robotoBold = new Texture(Gdx.files.internal(ROBOTO_BOLD_FONT_FILE + FONT_BITMAP_EXTENSION), true);
		robotoBold.setFilter(Texture.TextureFilter.MipMapLinearNearest, Texture.TextureFilter.MipMapLinearNearest);
		robotoBoldFont = createRobotoFont(robotoBold, ROBOTO_BOLD_FONT_FILE + FONT_FILE_EXTENSION);

		if(encyclopediaRobotoRegularFont != null) encyclopediaRobotoRegularFont.dispose();
		encyclopediaRobotoRegularFont = createEncyclopediaRobotoFont(robotoRegular, ROBOTO_REGULAR_FONT_FILE + FONT_FILE_EXTENSION);

		if(encyclopediaRobotoBoldFont != null) encyclopediaRobotoBoldFont.dispose();
		encyclopediaRobotoBoldFont = createEncyclopediaRobotoFont(robotoBold, ROBOTO_BOLD_FONT_FILE + FONT_FILE_EXTENSION);

		if(distanceFieldShader != null) distanceFieldShader.dispose();
		distanceFieldShader = DistanceFieldFont.createDistanceFieldShader();
//		pedantic flag indicating whether attributes & uniforms must be present at all times
//		distanceFieldShader.pedantic = false;
	}

	private DistanceFieldFont createEncyclopediaRobotoFont(Texture robotoRegular, String fontFile) {
		DistanceFieldFont encyclopediaRobotoFont = createRobotoFont(robotoRegular, fontFile);
		encyclopediaRobotoFont.getData().setLineHeight(ENCYCLOPEDIA_LINE_HEIGHT);
		return encyclopediaRobotoFont;
	}

	private DistanceFieldFont createRobotoFont(Texture robotoRegular, String fontFile) {
		DistanceFieldFont robotoFont = new DistanceFieldFont(Gdx.files.internal(fontFile), new TextureRegion(robotoRegular));
		robotoFont.setUseIntegerPositions(false);
		robotoFont.setDistanceFieldSmoothing(ApplicationFontManager.DISTANCE_FIELD_FONT_SIZE);
		robotoFont.getData().markupEnabled = false;
		return robotoFont;
	}


//////////////////////////// IApplicationLoader

	private boolean isLoaded() {
		return  robotoBoldFont != null &&
				distanceFieldShader != null &&
				encyclopediaRobotoRegularFont != null &&
				encyclopediaRobotoBoldFont != null;
	}

	@Override
	public boolean update() {
		if(!isLoaded()) load();
		return true;
	}

	@Override
	public synchronized void dispose() {
		instance = null;
		robotoRegularFont.dispose();
		robotoBoldFont.dispose();
		encyclopediaRobotoRegularFont.dispose();
		encyclopediaRobotoBoldFont.dispose();
		distanceFieldShader.dispose();
	}
}