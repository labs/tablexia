/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.application;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.menu.user.UserAvatarDefinition;
import cz.nic.tablexia.model.CustomAvatarDAO;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.shared.model.CustomAvatarDataPacket;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.util.Log;

public class ApplicationAvatarManager {
    private static final int                   AVATAR_HEIGHT                     = 300; //(Resolution of saved avatars)
    private static final int                   AVATAR_WIDTH                      = 250;

    private static final int                   PIXMAP_PACKER_PAGE_SIZE           = 1024;
    private static final Pixmap.Format         CUSTOM_AVATAR_FORMAT              = Pixmap.Format.RGB888; //2 bytes per pixel, to save some space...
    public  static final String[]              CUSTOM_AVATAR_ALLOWED_FILE_TYPES  = {".jpg", ".png"}; //Allowed FileTypes for "avatar chooser"

    private static final boolean               ATLAS_TEXTURE_MIPMAPPING          = true;
    private static final Texture.TextureFilter ATLAS_TEXTURE_MIN_FILTER          = Texture.TextureFilter.MipMapLinearNearest;
    private static final Texture.TextureFilter ATLAS_TEXTURE_MAG_FILTER          = Texture.TextureFilter.Linear;

    private static final String                FALLBACK_DEFAULT_AVATAR           = ApplicationAtlasManager.USERMENU_MENUITEM_AVATAR_0;

    private HashMap<Long, TextureRegion> avatarAtlasCache;

    private TextureAtlas avatarAtlas;

    private boolean finishedLoading = false;

    private static ApplicationAvatarManager instance;

    private ApplicationAvatarManager() {}

    public static ApplicationAvatarManager getInstance() {
        if(instance == null) {
            instance = new ApplicationAvatarManager();
        }

        return instance;
    }

    public boolean isCustomAvatarLoaded(User user) {
        return avatarAtlasCache != null && avatarAtlasCache.containsKey(user.getId());
    }

    public synchronized boolean update() {
        if(!finishedLoading) {
            this.avatarAtlas = new TextureAtlas();
            this.avatarAtlasCache = new HashMap<>();

            PixmapPacker pixmapPacker = new PixmapPacker(PIXMAP_PACKER_PAGE_SIZE, PIXMAP_PACKER_PAGE_SIZE, CUSTOM_AVATAR_FORMAT, 2, false);

            for(User user : UserDAO.selectActiveUsersWithCustomAvatars()) {
                byte[] avatarData = CustomAvatarDAO.getCustomAvatarForID(user.getId());

                if(avatarData == null) continue;

                Pixmap userAvatar = new Pixmap(avatarData, 0, avatarData.length);
                pixmapPacker.pack(String.valueOf(user.getId()), userAvatar);
                userAvatar.dispose();
            }

            this.avatarAtlas = pixmapPacker.generateTextureAtlas(ATLAS_TEXTURE_MIN_FILTER, ATLAS_TEXTURE_MAG_FILTER, ATLAS_TEXTURE_MIPMAPPING);

            pixmapPacker.dispose();
            finishedLoading = true;
        }
        return finishedLoading;
    }

    /**
     * Looking for already loaded and cached avatar for particular user
     * @param user - Actual user...
     * @return Returns TextureRegion of users avatar or default avatar if it fails...
     */
    public TextureRegion getAvatarTextureRegion(User user) {
        if(UserAvatarDefinition.isUserUsingCustomAvatar(user)) {
            TextureRegion result;

            if((result = getAvatarTextureRegion(user.getId())) != null) {
                return result;
            }
            else {
                return ApplicationAtlasManager.getInstance().getTextureRegion(FALLBACK_DEFAULT_AVATAR);
            }
        }
        else {
            return ApplicationAtlasManager.getInstance().getTextureRegion(UserAvatarDefinition.getAvatar(user.getAvatar()));
        }
    }

    /**
     * Returns texture region from cache, if it doesn't exist in cache
     * looks for it in avatarAtlas and caches it...
     * @param userId
     * @return TextureRegion or null if textureRegion doesn't exist...
     */
    private TextureRegion getAvatarTextureRegion(Long userId) {
        TextureRegion result = avatarAtlasCache.get(String.valueOf(userId));

        if(result == null) {
            if((result = avatarAtlas.findRegion(userId.toString())) != null) {
                avatarAtlasCache.put(userId, result);
            }
        }

        return result;
    }

    public Pixmap createAvatarPixmap(FileHandle textureFile) {
        return createAvatarPixmap(new Pixmap(textureFile));
    }

    /**
     * Creates preview avatar pixmap from FileHandle
     * Creating huge texture in case of high resolution textureFile doesn't really work on older devices
     * So this method loads texture into Pixmap and creates pixmap with resolution AVATAR_WIDTH x AVATAR_HEIGHT, which is cropped correctly
     * @return avatar pixmap (Don't forget to dispose pixmap)
     */
    public Pixmap createAvatarPixmap(Pixmap srcPixmap) {
        //Probably not gonna happen, but input image file may already be correct resolution
        if(srcPixmap.getWidth() == AVATAR_WIDTH && srcPixmap.getHeight() == AVATAR_HEIGHT) {
            return srcPixmap;
        }
        else {
            Pixmap destPixmap = new Pixmap(AVATAR_WIDTH, AVATAR_HEIGHT, CUSTOM_AVATAR_FORMAT);
            Rectangle cropRectangle = getCustomAvatarRegion(srcPixmap.getWidth(), srcPixmap.getHeight());

            destPixmap.setColor(Color.WHITE);
            destPixmap.setFilter(Pixmap.Filter.BiLinear);
            destPixmap.drawPixmap(
                    srcPixmap,
                    (int) cropRectangle.getX(),
                    (int) cropRectangle.getY(),
                    (int) cropRectangle.getWidth(),
                    (int) cropRectangle.getHeight(),
                    0,
                    0,
                    destPixmap.getWidth(),
                    destPixmap.getHeight()
            );

            //Destroy original image pixmap
            srcPixmap.dispose();
            return destPixmap;
        }
    }

    /**
     * Takes avatarPixmap and saves it to avatars folder. You can pass already created previewTextureRegion
     * and ApplicationAvatarManager adds this one into TextureAtlas instead...
     * @param user - User you wish to create the avatar file for
     * @param avatarPixmap - Pixmap created with createAvatarPixmap(FileHandle) method.
     */
    public void createCustomAvatar(User user, Pixmap avatarPixmap, TextureRegion previewTextureRegion) {
        CustomAvatarDAO.insertNewCustomAvatar(user.getId(), convertPixmapToByteArray(avatarPixmap));
        if(avatarAtlas.findRegion(String.valueOf(user.getId())) == null) {
            avatarAtlas.addRegion(String.valueOf(user.getId()), previewTextureRegion);
        } else {
            avatarAtlas.findRegion(String.valueOf(user.getId())).setRegion(previewTextureRegion);
        }
    }

    public void addCustomAvatarToAtlas(User user, byte[] avatar) {
        if(avatarAtlas == null) return;

        Pixmap pixmap = new Pixmap(avatar, 0, avatar.length);
        TextureRegion tr = new TextureRegion(new Texture(pixmap));
        pixmap.dispose();

        avatarAtlas.addRegion(String.valueOf(user.getId()), tr);
    }

    private byte[] convertPixmapToByteArray(Pixmap pixmap) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            PixmapIO.PNG png = new PixmapIO.PNG((int) (pixmap.getWidth() * pixmap.getHeight() * 1.5f));
            png.setFlipY(false);
            png.write(byteArrayOutputStream, pixmap);
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            Log.err(getClass(), "Cannot convert pixmap to Base64! Reason: " + e.getMessage());
        }

        return null;
    }

    /**
     * Calculates cropping rectangle according to texture's dimensions.
     * @param width - Width of the texture
     * @param height - Height of the texture
     * @return Cropped TextureRegion
     */
    private Rectangle getCustomAvatarRegion(int width, int height) {
        int textureWidth  = width;
        int textureHeight = height;

        Rectangle result = new Rectangle();

        if(textureWidth >= textureHeight) { //Image is wide, cropping edges
            int newWidth = MathUtils.ceil(textureHeight * (AVATAR_WIDTH / (float)(AVATAR_HEIGHT)));

            result.set(
                    (int) ((textureWidth - newWidth) / 2f),
                    0,
                    newWidth,
                    textureHeight
            );
        }
        else { //Image is narrow, cropping top and bottom edges
            int newHeight = MathUtils.ceil(textureWidth * (AVATAR_HEIGHT / (float) (AVATAR_WIDTH)));

            result.set(
                    0,
                    (int)((textureHeight - newHeight) / 2f),
                    textureWidth,
                    newHeight
            );
        }

        return result;
    }

    public synchronized void dispose() {
        if(avatarAtlas != null)
            avatarAtlas.dispose();

        instance = null;
    }

    public static class AvatarUpdatedEvent implements ApplicationBus.ApplicationEvent {}
}