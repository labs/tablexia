/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.application;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.async.AsyncTask;

import java.util.Locale;

import cz.nic.tablexia.loader.IApplicationLoader;
import cz.nic.tablexia.loader.TablexiaDataManager;

/**
 * Texture loader and manager for application context.
 * 
 * @author Matyáš Latner
 *
 */
public class ApplicationTextManager extends TablexiaDataManager<I18NBundle> implements IApplicationLoader {

    public static final class ApplicationTextsAssets {

        public static final String VICTORYSCREEN_BUTTON_REPLAY      = "victoryscreen_button_replay";
        public static final String VICTORYSCREEN_BUTTON_FINISH		= "victoryscreen_button_finish";
		public static final String VICTORYSCREEN_NEW_TROPHY  		= "victoryscreen_new_trophy";

		public static final String USERMENU_NEWUSER  				= "usermenu_newuser";
		public static final String USERMENU_DELETE_USER  			= "usermenu_deleteuser";

        public static final String USER_LOGOUT_MESSAGE              = "user_logout_message";

		public static final String PRELOADER_TITLE					= "preloader_title";

		public static final String SYSTEM_CONFIRM					= "system_confirm";
		public static final String SYSTEM_DECLINE					= "system_decline";
		public static final String SYSTEM_YES 						= "system_yes";
		public static final String SYSTEM_NO 						= "system_no";
		public static final String SYSTEM_EXIT 						= "system_exit";
		public static final String SYSTEM_RETRY						= "system_retry";
		public static final String SYSTEM_PROCEED					= "system_proceed";
		public static final String SYSTEM_BACK						= "system_back";
		public static final String SYSTEM_UNDERSTAND				= "system_understand";
		public static final String SYSTEM_CLOSE						= "system_close";
		public static final String SYSTEM_SETTINGS 					= "system_settings";

		public static final String ASSETS_MANAGER_ERROR            = "assets_manager_error";
		public static final String ASSETS_MANAGER_DOWNLOAD_REQUEST = "assets_manager_download_request";
		public static final String ASSETS_MANAGER_NOT_ENOUGH_SPACE = "assets_manager_not_enough_space";
		public static final String ASSETS_MANAGER_ROLLBACK_REQUEST = "assets_manager_rollback_request";
		public static final String ASSETS_MANAGER_NO_VALID_PACKAGE = "assets_manager_no_valid_package";
		public static final String ASSETS_MANAGER_OLD_DEVICE	   = "assets_manager_old_device";

		public static final String WARNING_USING_APP      			= "warning_info";

		public static final String LANGUAGE_SYSTEM 					= "language_system";
		public static final String LANGUAGE_CZECH  					= "language_czech";
		public static final String LANGUAGE_SLOVAK 					= "language_slovak";
		public static final String LANGUAGE_GERMAN 					= "language_german";

		public static final String MAINMENU_GAMES 					= "mainmenu_games";
		public static final String MAINMENU_OFFICE 					= "mainmenu_office";
		public static final String MAINMENU_HALLOFFAME 				= "mainmenu_halloffame";
		public static final String MAINMENU_STATISTICS 				= "mainmenu_statistics";
		public static final String MAINMENU_ENCYCLOPEDIA			= "mainmenu_encyclopedia";
		public static final String MAINMENU_ABOUT					= "mainmenu_about";
		public static final String MAINMENU_PROFILE					= "mainmenu_profile";
		public static final String MAINMENU_LOGOUT					= "mainmenu_logout";
		public static final String MAINMENU_DEBUG					= "mainmenu_screendebug";

		public static final String GAME_ROBBERY_TITLE 					= "game_robbery_title";
		public static final String GAME_ROBBERY_DESCRIPTION				= "game_robbery_description";
		public static final String GAME_ROBBERY_PRELOADER_TEXT			= "game_robbery_preloader";
		public static final String GAME_PURSUIT_TITLE 					= "game_pursuit_title";
		public static final String GAME_PURSUIT_DESCRIPTION				= "game_pursuit_description";
		public static final String GAME_PURSUIT_PRELOADER_TEXT			= "game_pursuit_preloader";
		public static final String GAME_PURSUIT_PRELOADER_DESKTOP_TEXT 	= "game_pursuit_preloader_desktop";
		public static final String GAME_KIDNAPPING_TITLE 				= "game_kidnapping_title";
		public static final String GAME_KIDNAPPING_DESCRIPTION			= "game_kidnapping_description";
		public static final String GAME_KIDNAPPING_PRELOADER_TEXT1		= "game_kidnapping_preloader1";
		public static final String GAME_KIDNAPPING_PRELOADER_TEXT2		= "game_kidnapping_preloader2";
		public static final String GAME_KIDNAPPING_PRELOADER_TEXT3		= "game_kidnapping_preloader3";
		public static final String GAME_NIGHT_WATCH_TITLE 				= "game_night_watch_title";
		public static final String GAME_NIGHT_WATCH_DESCRIPTION			= "game_night_watch_description";
		public static final String GAME_NIGHT_WATCH_PRELOADER_TEXT		= "game_night_watch_preloader";
		public static final String GAME_NIGHT_WATCH_PRELOADER_BONUS_TEXT	= "game_night_watch_preloader_bonus";
		public static final String GAME_SHOOTING_RANGE_TITLE 			= "game_shooting_range_title";
		public static final String GAME_SHOOTING_RANGE_DESCRIPTION		= "game_shooting_range_description";
		public static final String GAME_SHOOTING_RANGE_PRELOADER_TEXT1	= "game_shooting_range_preloader1";
		public static final String GAME_SHOOTING_RANGE_PRELOADER_TEXT2	= "game_shooting_range_preloader2";
		public static final String GAME_IN_THE_DARKNESS_TITLE 			= "game_in_the_darkness_title";
		public static final String GAME_IN_THE_DARKNESS_DESCRIPTION		= "game_in_the_darkness_description";
		public static final String GAME_IN_THE_DARKNESS_PRELOADER_TEXT1	= "game_in_the_darkness_preloader1";
		public static final String GAME_IN_THE_DARKNESS_PRELOADER_TEXT2	= "game_in_the_darkness_preloader2";
		public static final String GAME_IN_THE_DARKNESS_PRELOADER_DEKSTOP	= "game_in_the_darkness_preloader_desktop_1";

		public static final String GAME_CRIME_SCENE_TITLE                    = "game_crime_scene_title";
		public static final String GAME_CRIME_SCENE_DESCRIPTION              = "game_crime_scene_description";
		public static final String GAME_CRIME_SCENE_PRELOADER_TEXT_1         = "game_crime_scene_preloader_1";
		public static final String GAME_CRIME_SCENE_PRELOADER_TEXT_2         = "game_crime_scene_preloader_2";
		public static final String GAME_CRIME_SCENE_PRELOADER_TEXT_2_DESKTOP = "game_crime_scene_preloader_desktop_1";

		public static final String GAME_RUNES_DESCRIPTION		        = "game_runes_description";
		public static final String GAME_RUNES_TITLE 			        = "game_runes_title";
		public static final String GAME_RUNES_PRELOADER_TEXT1			= "game_runes_preloader1";
		public static final String GAME_RUNES_PRELOADER_TEXT2			= "game_runes_preloader2";
		public static final String GAME_RUNES_PRELOADER_TEXT3			= "game_runes_preloader3";

		public static final String GAME_PROTOCOL_TITLE					= "game_protocol_title";
		public static final String GAME_PROTOCOL_DESCRIPTION			= "game_protocol_description";
		public static final String GAME_PROTOCOL_PRELOADER				= "game_protocol_preloader";
	
		public static final String GAME_SAFE_TITLE 						= "game_safe_title";
		public static final String GAME_SAFE_DESCRIPTION				= "game_safe_description";
		public static final String GAME_SAFE_PRELOADER_TEXT				= "game_safe_preloader";
		public static final String GAME_SAFE_PRELOADER_TEXT_BONUS		= "game_safe_preloader_bonus";
		public static final String GAME_SAFE_PRELOADER_HEADPHONES		= "game_safe_preloader_headphones";

		public static final String GAME_ON_THE_TRAIL_TITLE 				= "game_on_the_trail_title";
		public static final String GAME_ON_THE_TRAIL_DESCRIPTION 		= "game_on_the_trail_description";
		public static final String GAME_ON_THE_TRAIL_PRELOADER_TEXT 	= "game_on_the_trail_preloader";

		public static final String MEMORY_GAME_TITLE 					= "memory_game_title";
		public static final String MEMORY_GAME_DESCRIPTION				= "memory_game_description";
		public static final String MEMORY_GAME_PRELOADER_EASY_TEXT		= "memory_game_preloader_easy";
		public static final String MEMORY_GAME_PRELOADER_TEXT			= "memory_game_preloader";
		public static final String MEMORY_GAME_PRELOADER_BONUS_TEXT		= "memory_game_preloader_bonus";

		public static final String ATTENTION_GAME_TITLE 					= "attention_game_title";
		public static final String ATTENTION_GAME_DESCRIPTION				= "attention_game_description";
		public static final String ATTENTION_GAME_PRELOADER_1				= "attention_game_preloader_1";
		public static final String ATTENTION_GAME_PRELOADER_1_BONUS			= "attention_game_preloader_1_bonus";

		public static final String GAME_DIFFICULTY_NAME					= "gamedifficulty_name";
		public static final String GAME_SCORE 							= "game_score";
		public static final String GAME_SCORE_ERROR 					= "game_score_error";
		public static final String GAME_SCORE_DURATION 					= "game_score_duration";
		public static final String GAME_AVERAGE_SCORE 					= "game_averagescore";
		public static final String GAME_AVERAGE_SCORE_ERROR 			= "game_averagescore_error";
		public static final String GAME_AVERAGE_SCORE_DURATION			= "game_averagescore_duration";
		public static final String GAME_QUIT_QUESTION					= "game_quit_question";

		public static final String GAME_STATUS_NEW						= "game_status_new";

		public static final String OFFICE_HELP_TROPHIES					= "office_help_trophies";
		public static final String OFFICE_HELP_DETECTIVES_CARD			= "office_help_detectives_card";

		public static final String SOUND_MUTED_QUESTION 				= "sound_muted_question";
		public static final String SOUND_MUTED_GAME_QUESTION 			= "sound_muted_game_question";

		public static final String SYNC_REQUEST_DIALOG_TEXT				= "sync_request_dialog_text";
		public static final String SYNC_REQUEST_QR_DIALOG_TEXT			= "sync_request_qr_dialog_text";
		public static final String SYNC_REQUEST_BUTTON					= "sync_request_button";
		public static final String SYNC_REQUEST_WRONG_ID				= "sync_request_wrong_id";
		public static final String SYNC_REQUEST_UUID_EXISTS				= "sync_request_uuid_exists";
		public static final String SYNC_REQUEST_ERROR					= "sync_request_error";

		public static final String EXIT_APP_BUTTON						= "exit_app_button";
		public static final String EXIT_MESSAGE							= "exit_app_message";

		public static final String USER_RANK_1							= "user_rank_1";
		public static final String USER_RANK_2							= "user_rank_2";
		public static final String USER_RANK_3							= "user_rank_3";
		public static final String USER_RANK_4							= "user_rank_4";
		public static final String USER_RANK_5							= "user_rank_5";
		public static final String USER_RANK_6							= "user_rank_6";
		public static final String USER_RANK_7							= "user_rank_7";
		public static final String USER_RANK_8							= "user_rank_8";
		public static final String USER_RANK_9							= "user_rank_9";
		public static final String USER_RANK_10							= "user_rank_10";
		public static final String USER_RANK_11							= "user_rank_11";

		public static final String DIFFICULTY_LOCKED_TITLE				= "gamedifficulty_bonus_locked_title";
		public static final String DIFFICULTY_LOCKED_DESCRIPTION		= "gamedifficulty_bonus_locked_description";
		public static final String DIFFICULTY_LOCKED_CURRENT			= "gamedifficulty_bonus_locked_current";

		public static final String CAMERA_ACCESS_NOTIFICATION           = "camera_access_notification";

		public static final String SYNC_QR_REQUEST_BUTTON				= "sync_qr_request_button";
		public static final String SYNC_QR_REQUEST_INFO_DIALOG_TEXT       = "sync_qr_request_info_dialog_text";
		public static final String SYNC_QR_REQUEST_ON_DECODED_DIALOG_TEXT = "sync_qr_request_on_decoded_dialog_text";
	}
	
	private static final String APPLICATION_TEXT_RESOURCE_FILE = "text/application/application";

	private static class ApplicationTextLoader implements AsyncTask<I18NBundle> {

		private String textResourceFileName;
		private Locale locale;

		public ApplicationTextLoader(String textResourceFileName, Locale locale) {
			this.textResourceFileName = textResourceFileName;
			this.locale = locale;
		}

		@Override
		public I18NBundle call() throws Exception {
			FileHandle baseFileHandle = Gdx.files.internal(textResourceFileName);
			return I18NBundle.createBundle(baseFileHandle, locale);
		}
	}

	private static ApplicationTextManager instance;
	
	private ApplicationTextManager() {}
	
	public static ApplicationTextManager getInstance() {
		if (instance == null) {
			instance = new ApplicationTextManager();
		}
		return instance;
	}
	
	public void load(Locale locale) {
        setAsyncTask(new ApplicationTextLoader(APPLICATION_TEXT_RESOURCE_FILE, locale));
	}

    public String getText(String textKey) {
        return getResult().get(textKey);
    }

    public String getFormattedText(String textKey, Object... args) {
        return getResult().format(textKey, args);
    }
	
	@Override
	public void dispose() {
		super.dispose();
		instance = null;
	}
	
}