/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.application;

import com.badlogic.gdx.audio.Sound;

import cz.nic.tablexia.loader.IApplicationLoader;
import cz.nic.tablexia.loader.TablexiaSoundManager;

/**
 * Application Sounds Manager, which is loading sounds from the external path...
 */
public class ApplicationInternalSoundManager extends TablexiaSoundManager implements IApplicationLoader {

	private static ApplicationInternalSoundManager instance;

	private ApplicationInternalSoundManager() {
		super(AssetsStorageType.INTERNAL);
	}
	
	public static ApplicationInternalSoundManager getInstance() {
		if (instance == null) {
			instance = new ApplicationInternalSoundManager();
		}
		return instance;
	}
	
	@Override
	public synchronized void dispose() {
		super.dispose();
		instance = null;
	}

	private static final String MP3_SUFFIX 			= ".mp3";
	private static final String BASE_PATH 			= "sfx/";
	public 	static final String BUTTON_CLICKED		= BASE_PATH + "button_clicked" + MP3_SUFFIX;
	public 	static final String ALERT				= BASE_PATH + "alert" + MP3_SUFFIX;

    public void load() {
		loadSound(BUTTON_CLICKED);
		loadSound(ALERT);
    }

	@Override
	public void loadSound(String soundName) {
		// don't check sound file -> all assets needn't be unzipped yet
		super.loadSound(soundName, false);
	}

	@Override
	public Sound getSound(String soundName) {
		return super.getSound(soundName);
	}
}