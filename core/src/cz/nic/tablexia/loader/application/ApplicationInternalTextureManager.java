/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.application;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;

import cz.nic.tablexia.loader.TablexiaTextureManager;

public class ApplicationInternalTextureManager extends TablexiaTextureManager {

    private static final Texture.TextureFilter  DEFAULT_TEXTURE_FILTER = Texture.TextureFilter.Linear;

    public static final String NINE_PATCH_SUFFIX        = ".9.png";
    public static final String PNG_SUFFIX               = ".png";

    public static final String GFX_PATH                 = "gfx/";
    public static final String BUTTON_PATH              = GFX_PATH + "button/";
    public static final String BUTTON_BLUE_PART         = "tablexiabutton_blank_blue";
    public static final String BUTTON_GREEN_PART        = "tablexiabutton_blank_green";
    public static final String BUTTON_RED_PART          = "tablexiabutton_blank_red";
    public static final String BUTTON_PRESSED_PART      = "_pressed";
    public static final String BUTTON_UNPRESSED_PART    = "_unpressed";
    public static final String BUTTON_SOLARIZED_PART    = "_solarized";
    public static final String BUTTON_BLUE_PRESSED      = BUTTON_PATH + BUTTON_BLUE_PART + BUTTON_PRESSED_PART + NINE_PATCH_SUFFIX;
    public static final String BUTTON_BLUE_UNPRESSED    = BUTTON_PATH + BUTTON_BLUE_PART + BUTTON_UNPRESSED_PART + NINE_PATCH_SUFFIX;
    public static final String BUTTON_BLUE_SOLARIZED    = BUTTON_PATH + BUTTON_BLUE_PART + BUTTON_SOLARIZED_PART + NINE_PATCH_SUFFIX;
    public static final String BUTTON_GREEN_PRESSED     = BUTTON_PATH + BUTTON_GREEN_PART + BUTTON_PRESSED_PART + NINE_PATCH_SUFFIX;
    public static final String BUTTON_GREEN_UNPRESSED   = BUTTON_PATH + BUTTON_GREEN_PART + BUTTON_UNPRESSED_PART + NINE_PATCH_SUFFIX;
    public static final String BUTTON_GREEN_SOLARIZED   = BUTTON_PATH + BUTTON_GREEN_PART + BUTTON_SOLARIZED_PART + NINE_PATCH_SUFFIX;
    public static final String BUTTON_RED_PRESSED       = BUTTON_PATH + BUTTON_RED_PART + BUTTON_PRESSED_PART + NINE_PATCH_SUFFIX;
    public static final String BUTTON_RED_UNPRESSED     = BUTTON_PATH + BUTTON_RED_PART + BUTTON_UNPRESSED_PART + NINE_PATCH_SUFFIX;
    public static final String BUTTON_RED_SOLARIZED     = BUTTON_PATH + BUTTON_RED_PART + BUTTON_SOLARIZED_PART + NINE_PATCH_SUFFIX;
    public static final String BUTTON_DISABLED          = BUTTON_PATH + "tablexiabutton_disabled" + NINE_PATCH_SUFFIX;
    public static final String BUTTON_CLOSE_PRESSED     = BUTTON_PATH + "closebutton_pressed" + PNG_SUFFIX;
    public static final String BUTTON_CLOSE_UNPRESSED   = BUTTON_PATH + "closebutton_unpressed" + PNG_SUFFIX;
    public static final String BUTTON_YES_ICON          = BUTTON_PATH + "yes_icon" + PNG_SUFFIX;
    public static final String BUTTON_NO_ICON           = BUTTON_PATH + "no_icon" + PNG_SUFFIX;
    public static final String BUTTON_REPEAT_ICON       = BUTTON_PATH + "repeat_icon" + PNG_SUFFIX;

    public static final String BUTTON_BIN               = GFX_PATH + "bin.png";
    public static final String WARNING_DIALOG_BG        = GFX_PATH + "warning_rectangle_bg" + NINE_PATCH_SUFFIX;

    public enum InternalNinePatch {

        BUTTON_BLUE_PRESSED_PATCH     (BUTTON_BLUE_PRESSED,     13, 21, 9, 28),
        BUTTON_BLUE_UNPRESSED_PATCH   (BUTTON_BLUE_UNPRESSED,   18, 19, 6, 32),
        BUTTON_BLUE_SOLARIZED_PATCH   (BUTTON_BLUE_SOLARIZED,   18, 19, 6, 32),
        BUTTON_GREEN_PRESSED_PATCH    (BUTTON_GREEN_PRESSED,    13, 21, 9, 28),
        BUTTON_GREEN_UNPRESSED_PATCH  (BUTTON_GREEN_UNPRESSED,  18, 19, 6, 32),
        BUTTON_GREEN_SOLARIZED_PATCH  (BUTTON_GREEN_SOLARIZED,  18, 19, 6, 32),
        BUTTON_RED_PRESSED_PATCH      (BUTTON_RED_PRESSED,      13, 21, 9, 28),
        BUTTON_RED_UNPRESSED_PATCH    (BUTTON_RED_UNPRESSED,    18, 19, 6, 32),
        BUTTON_RED_SOLARIZED_PATCH    (BUTTON_RED_SOLARIZED,    18, 19, 6, 32),
        BUTTON_DISABLED_PATCH         (BUTTON_DISABLED,         13, 21, 9, 28),
        WARNING_DIALOG_BG_PATCH       (WARNING_DIALOG_BG,       25, 25, 25, 25);

        private final String    ninePatchPath;
        private final int       leftPad;
        private final int       rightPad;
        private final int       topPad;
        private final int       botPad;

        InternalNinePatch(String ninePatchPath, int leftPad, int rightPad, int topPad, int botPad) {
            this.ninePatchPath  = ninePatchPath;
            this.leftPad        = leftPad;
            this.rightPad       = rightPad;
            this.topPad         = topPad;
            this.botPad         = botPad;
        }
    }

    private static ApplicationInternalTextureManager instance;

    public ApplicationInternalTextureManager() {
        super(AssetsStorageType.INTERNAL);
    }

    public static ApplicationInternalTextureManager getInstance() {
        if (instance == null) {
            instance = new ApplicationInternalTextureManager();
        }
        return instance;
    }

    @Override
    public synchronized void dispose() {
        super.dispose();
        instance = null;
    }
    @Override
    public Texture getTexture(String textureName) {
        Texture texture = super.getTexture(textureName);
        texture.setFilter(DEFAULT_TEXTURE_FILTER, DEFAULT_TEXTURE_FILTER);
        return texture;
    }


    public NinePatch getPatch(InternalNinePatch ninePatch) {
        return new NinePatch(getTexture(ninePatch.ninePatchPath), ninePatch.leftPad, ninePatch.rightPad, ninePatch.topPad, ninePatch.botPad);
    }

    public void load() {
        loadTexture(BUTTON_BLUE_PRESSED);
        loadTexture(BUTTON_BLUE_UNPRESSED);
        loadTexture(BUTTON_BLUE_SOLARIZED);
        loadTexture(BUTTON_GREEN_PRESSED);
        loadTexture(BUTTON_GREEN_UNPRESSED);
        loadTexture(BUTTON_GREEN_SOLARIZED);
        loadTexture(BUTTON_RED_PRESSED);
        loadTexture(BUTTON_RED_UNPRESSED);
        loadTexture(BUTTON_RED_SOLARIZED);
        loadTexture(BUTTON_DISABLED);
        loadTexture(BUTTON_BLUE_PRESSED);
        loadTexture(BUTTON_CLOSE_PRESSED);
        loadTexture(BUTTON_CLOSE_UNPRESSED);
        loadTexture(BUTTON_YES_ICON);
        loadTexture(BUTTON_NO_ICON);
        loadTexture(BUTTON_REPEAT_ICON);
        loadTexture(BUTTON_BIN);
        loadTexture(WARNING_DIALOG_BG);
    }
}
