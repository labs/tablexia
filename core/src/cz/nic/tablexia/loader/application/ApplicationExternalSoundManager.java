/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.application;

import com.badlogic.gdx.audio.Sound;

import cz.nic.tablexia.loader.IApplicationLoader;
import cz.nic.tablexia.loader.TablexiaSoundManager;
import cz.nic.tablexia.util.Utility;

/**
 * Application Sounds Manager, which is loading sounds from the internal path...
 * Useful for sounds that you need to play before game has downloaded all the assets
 */
public class ApplicationExternalSoundManager extends TablexiaSoundManager implements IApplicationLoader {

	private static ApplicationExternalSoundManager instance;

	private ApplicationExternalSoundManager() {
		super(AssetsStorageType.EXTERNAL);
	}
	
	public static ApplicationExternalSoundManager getInstance() {
		if (instance == null) {
			instance = new ApplicationExternalSoundManager();
		}
		return instance;
	}
	
	@Override
	public synchronized void dispose() {
		super.dispose();
		instance = null;
	}

	private static final String APPLICATION_PATH	= "_global/application/";
	private static final String MAINMENU_PATH		= "mainmenu/";

	public 	static final String MAINMENU_OPEN		= MAINMENU_PATH + "mainmenu_open.mp3";
    public 	static final String MAINMENU_CLOSE		= MAINMENU_PATH + "mainmenu_close.mp3";

	public  static final String RANKUP				= "badge/rankup_sound.mp3";

    public void load() {
        loadSound(MAINMENU_OPEN);
        loadSound(MAINMENU_CLOSE);
		loadSound(RANKUP);
    }

	private String createAssetPath(String fileName) {
		return APPLICATION_PATH + Utility.transformLocalAssetsPath(fileName);
	}

	@Override
	public void loadSound(String soundName) {
		// don't check sound file -> all assets needn't be unzipped yet
		super.loadSound(createAssetPath(soundName), false);
	}

	@Override
	public Sound getSound(String soundName) {
		return super.getSound(createAssetPath(soundName));
	}
}