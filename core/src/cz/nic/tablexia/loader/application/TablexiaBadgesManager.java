/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.application;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import net.engio.mbassy.listener.Handler;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.game.ranksystem.UserRankManager;
import cz.nic.tablexia.loader.TablexiaTextureManager;
import cz.nic.tablexia.loader.zip.TablexiaAssetsManager;
import cz.nic.tablexia.shared.model.User;

import cz.nic.tablexia.util.Log;

/**
 * Created by drahomir on 8/4/17.
 */

public class TablexiaBadgesManager extends TablexiaTextureManager {
    private static final String BADGES_ASSETS_PATH   = "badges/";
    private static final String BADGE_IMAGE_FILE_EXT = ".png";


    public enum FullRankBadges {
        BADGE_NONE  (UserRankManager.UserRank.RANK_NONE, BADGES_ASSETS_PATH + "badge_none" + BADGE_IMAGE_FILE_EXT),
        BADGE_1     (UserRankManager.UserRank.RANK_I,    BADGES_ASSETS_PATH + "badge1"     + BADGE_IMAGE_FILE_EXT),
        BADGE_2     (UserRankManager.UserRank.RANK_II,   BADGES_ASSETS_PATH + "badge2"     + BADGE_IMAGE_FILE_EXT),
        BADGE_3     (UserRankManager.UserRank.RANK_III,  BADGES_ASSETS_PATH + "badge3"     + BADGE_IMAGE_FILE_EXT),
        BADGE_4     (UserRankManager.UserRank.RANK_IV,   BADGES_ASSETS_PATH + "badge4"     + BADGE_IMAGE_FILE_EXT),
        BADGE_5     (UserRankManager.UserRank.RANK_V,    BADGES_ASSETS_PATH + "badge5"     + BADGE_IMAGE_FILE_EXT),
        BADGE_6     (UserRankManager.UserRank.RANK_VI,   BADGES_ASSETS_PATH + "badge6"     + BADGE_IMAGE_FILE_EXT),
        BADGE_7     (UserRankManager.UserRank.RANK_VII,  BADGES_ASSETS_PATH + "badge7"     + BADGE_IMAGE_FILE_EXT),
        BADGE_8     (UserRankManager.UserRank.RANK_VIII, BADGES_ASSETS_PATH + "badge8"     + BADGE_IMAGE_FILE_EXT),
        BADGE_9     (UserRankManager.UserRank.RANK_IX,   BADGES_ASSETS_PATH + "badge9"     + BADGE_IMAGE_FILE_EXT),
        BADGE_10    (UserRankManager.UserRank.RANK_X,    BADGES_ASSETS_PATH + "badge10"    + BADGE_IMAGE_FILE_EXT),
        BADGE_11    (UserRankManager.UserRank.RANK_XI,   BADGES_ASSETS_PATH + "badge11"    + BADGE_IMAGE_FILE_EXT);


        private UserRankManager.UserRank userRank;
        private String filePath;

        FullRankBadges(UserRankManager.UserRank userRank, String filePath) {
            this.userRank = userRank;
            this.filePath = filePath;
        }


        public String getFilePath() {
            return filePath;
        }

        public static String getFilePathForUserRank(UserRankManager.UserRank rank) {
            for(FullRankBadges badge : values()) {
                if(badge.userRank == rank) return badge.filePath;
            }
            return FullRankBadges.BADGE_NONE.filePath;
        }
    }

    private static TablexiaBadgesManager instance;

    private UserRankManager.UserRank loadedRank = null;
    private UserRankManager.UserRank nextRankCache;
    private String loadedRankPath, nextRankCachePath;


    private boolean loadingSuccessful = true;

    private TablexiaBadgesManager() {}

    public static void initialize() {
        if(instance == null) {
            instance = new TablexiaBadgesManager();
            ApplicationBus.getInstance().subscribe(instance);
        }
    }

    public static TablexiaBadgesManager getInstance() {
        return instance;
    }

    @Handler
    public void onUserSelectedEvent(TablexiaSettings.SelectedUserEvent selectedUserEvent) {
        refreshBadges();
    }

    private void loadAssetsForCurrentUser() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                loadTexture(loadedRankPath = FullRankBadges.getFilePathForUserRank(loadedRank));
                loadTexture(nextRankCachePath = FullRankBadges.getFilePathForUserRank(nextRankCache));
                finishLoading();


                boolean loaded = isLoaded(loadedRankPath, Texture.class);
                loaded &=  isLoaded(nextRankCachePath, Texture.class);

                TablexiaBadgesManager.this.onLoadingResult(loaded);
            }
        });
    }

    private void onLoadingResult(boolean result) {
        Log.info(getClass(), "On loading result! Loaded Rank: " + loadedRank + ". Result: " + result) ;
        loadingSuccessful = result;
    }

    public void refreshBadges() {
        User currentUser = TablexiaSettings.getInstance().getSelectedUser();

        UserRankManager.UserRank rank = UserRankManager.UserRank.RANK_NONE;
        UserRankManager rankManager = UserRankManager.getInstance();

        if(currentUser != null && rankManager != null) {
            rank = rankManager.getRank(currentUser);
        }

        if(rank == null) rank = UserRankManager.UserRank.RANK_NONE;

        Log.info(getClass(), "Refreshing badges.");
        if(!loadingSuccessful || rank != loadedRank) {
            disposeAllLoadedAssets();
            loadedRank = rank;
            nextRankCache = UserRankManager.UserRank.getNextRank(loadedRank);

            loadAssetsForCurrentUser();
        }
    }

    public Texture getCurrentRankBadgeTexture() {
        return getTexture(loadedRankPath);
    }

    public Texture getNextRankBadgeTexture() {
        return getTexture(nextRankCachePath);
    }

    @Override
    public Texture getTexture(String textureName) {
        Texture texture = super.getTexture(textureName);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        return texture;
    }

    private void disposeAllLoadedAssets() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                clear();
            }
        });
    }
}