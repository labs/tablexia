/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.application;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import cz.nic.tablexia.loader.IApplicationLoader;
import cz.nic.tablexia.loader.TablexiaAtlasManager;


public class ApplicationAtlasManager extends TablexiaAtlasManager implements IApplicationLoader {

    private static ApplicationAtlasManager instance;

    private ApplicationAtlasManager() {
    }

    public static ApplicationAtlasManager getInstance() {
        if (instance == null) {
            instance = new ApplicationAtlasManager();
        }
        return instance;
    }

    @Override
    public synchronized void dispose() {
        super.dispose();
        instance = null;
    }

    private static final String GLOBAL_PATH       = "_global/";
    private static final String APPLICATION_PATH  = GLOBAL_PATH + "application/";
    private static final String APPLICATION_ATLAS = APPLICATION_PATH + "application.atlas";

    public static final String MAINMENU_PATH                            = "mainmenu/";
    public static final String MAINMENU_BACKGROUND                      = MAINMENU_PATH + "background";
    public static final String MAINMENU_BORDER                          = MAINMENU_PATH + "background_border";
    public static final String MAINMENU_SWITCH                          = MAINMENU_PATH + "background_switch";
    public static final String MAINMENU_ICON_ABOUT_PRESSED              = MAINMENU_PATH + "icon_about_pressed";
    public static final String MAINMENU_ICON_ABOUT_UNPRESSED            = MAINMENU_PATH + "icon_about_unpressed";
    public static final String MAINMENU_ICON_ENCYCLOPEDIA_PRESSED       = MAINMENU_PATH + "icon_encyclopedia_pressed";
    public static final String MAINMENU_ICON_ENCYCLOPEDIA_UNPRESSED     = MAINMENU_PATH + "icon_encyclopedia_unpressed";
    public static final String MAINMENU_ICON_GAMES_PRESSED              = MAINMENU_PATH + "icon_games_pressed";
    public static final String MAINMENU_ICON_GAMES_UNPRESSED            = MAINMENU_PATH + "icon_games_unpressed";
    public static final String MAINMENU_ICON_HALLOFFAME_PRESSED         = MAINMENU_PATH + "icon_halloffame_pressed";
    public static final String MAINMENU_ICON_HALLOFFAME_UNPRESSED       = MAINMENU_PATH + "icon_halloffame_unpressed";
    public static final String MAINMENU_ICON_OFFICE_PRESSED             = MAINMENU_PATH + "icon_office_pressed";
    public static final String MAINMENU_ICON_OFFICE_UNPRESSED           = MAINMENU_PATH + "icon_office_unpressed";
    public static final String MAINMENU_ICON_PROFILE_PRESSED            = MAINMENU_PATH + "icon_profile_pressed";
    public static final String MAINMENU_ICON_PROFILE_UNPRESSED          = MAINMENU_PATH + "icon_profile_unpressed";
    public static final String MAINMENU_ICON_LOGOUT_PRESSED             = MAINMENU_PATH + "icon_logout_pressed";
    public static final String MAINMENU_ICON_LOGOUT_UNPRESSED           = MAINMENU_PATH + "icon_logout_unpressed";
    public static final String MAINMENU_ICON_EXIT_PRESSED               = MAINMENU_PATH + "icon_exit_pressed";
    public static final String MAINMENU_ICON_EXIT_UNPRESSED             = MAINMENU_PATH + "icon_exit_unpressed";
    public static final String MAINMENU_ICON_STATISTICS_PRESSED         = MAINMENU_PATH + "icon_statistics_pressed";
    public static final String MAINMENU_ICON_STATISTICS_UNPRESSED       = MAINMENU_PATH + "icon_statistics_unpressed";
    public static final String MAINMENU_CHILDITEM_BACKGROUND            = MAINMENU_PATH + "childitem_background";
    public static final String MAINMENU_CHILDITEM_ARROW                 = MAINMENU_PATH + "childitem_arrow";
    public static final String MAINMENU_CHILDITEM_RESTART               = MAINMENU_PATH + "childitem_restart";

    public static final String ALL_TROPHY_ANIMATION_PATH                = "alltrophyanimation/";
    public static final String TROPHY_ANIMATION                         = ALL_TROPHY_ANIMATION_PATH + "all_trophy_anim";

    public static final String MAINMENU_ICON_SOUNDS_PRESSED             = MAINMENU_PATH + "button_sound_pressed";
    public static final String MAINMENU_ICON_SOUNDS_UNPRESSED           = MAINMENU_PATH + "button_sound_unpressed";
    public static final String MAINMENU_ICON_SOUNDS_DISABLED            = MAINMENU_PATH + "button_sound_disabled_overlay";

    public static final String MAINMENU_VOLUME_BAR_BACKGROUND           = MAINMENU_PATH + "volume_bar_bg";
    public static final String MAINMENU_VOLUME_BAR_POINT                = MAINMENU_PATH + "volume_bar_point";

    public static final String USERMENU_PATH                            = "usermenu/";
    public static final String USERMENU_MENUITEM_BACKGROUND             = USERMENU_PATH + "menuitem_background";
    public static final String USERMENU_MENUITEM_TRIANGLE               = USERMENU_PATH + "menuitem_triangle";
    public static final String USERMENU_MENUBUTTON_BACKGROUND           = USERMENU_PATH + "menubutton_background";

    public static final String AVATAR_PATH                              = "avatar/";
    public static final String USERMENU_MENUITEM_AVATAR_0               = AVATAR_PATH+"0";
    public static final String USERMENU_MENUITEM_AVATAR_1               = AVATAR_PATH+"1";
    public static final String USERMENU_MENUITEM_AVATAR_2               = AVATAR_PATH+"2";
    public static final String USERMENU_MENUITEM_AVATAR_3               = AVATAR_PATH+"3";
    public static final String USERMENU_MENUITEM_AVATAR_4               = AVATAR_PATH+"4";
    public static final String USERMENU_MENUITEM_AVATAR_5               = AVATAR_PATH+"5";
    public static final String USERMENU_MENUITEM_AVATAR_6               = AVATAR_PATH+"6";
    public static final String USERMENU_MENUITEM_AVATAR_7               = AVATAR_PATH+"7";
    public static final String USERMENU_MENUITEM_AVATAR_8               = AVATAR_PATH+"8";
    public static final String USERMENU_MENUITEM_AVATAR_9               = AVATAR_PATH+"9";
    public static final String USERMENU_MENUITEM_AVATAR_10              = AVATAR_PATH+"10";
    public static final String USERMENU_MENUITEM_AVATAR_11              = AVATAR_PATH+"11";
    public static final String USERMENU_MENUITEM_AVATAR_12              = AVATAR_PATH+"12";
    public static final String USERMENU_MENUITEM_AVATAR_13              = AVATAR_PATH+"13";
    public static final String USERMENU_MENUITEM_AVATAR_14              = AVATAR_PATH+"14";

    public static final String USERMENU_MENUITEM_CUSTOM_AVATAR          = AVATAR_PATH+"custom_avatar";

    public static final String BADGE_PATH                               = "badge/";

    public static final String BADGE_NONE_ICON                          = BADGE_PATH + "badge_none_icon";
    public static final String BADGE_1_ICON                             = BADGE_PATH + "badge1_icon";
    public static final String BADGE_2_ICON                             = BADGE_PATH + "badge2_icon";
    public static final String BADGE_3_ICON                             = BADGE_PATH + "badge3_icon";
    public static final String BADGE_4_ICON                             = BADGE_PATH + "badge4_icon";
    public static final String BADGE_5_ICON                             = BADGE_PATH + "badge5_icon";
    public static final String BADGE_6_ICON                             = BADGE_PATH + "badge6_icon";
    public static final String BADGE_7_ICON                             = BADGE_PATH + "badge7_icon";
    public static final String BADGE_8_ICON                             = BADGE_PATH + "badge8_icon";
    public static final String BADGE_9_ICON                             = BADGE_PATH + "badge9_icon";
    public static final String BADGE_10_ICON                             = BADGE_PATH + "badge10_icon";
    public static final String BADGE_11_ICON                            = BADGE_PATH + "badge11_icon";

    public static final String SCREEN_LOADER_MOUSE                      = "screenloader/mouse";

    public static final String UI_PATH                               = "ui/";
    public static final String BACK_BUTTON                           = UI_PATH + "back_button";
    public static final String BACK_BUTTON_PRESSED                   = UI_PATH + "back_button_pressed";
    public static final String QR_MAGNIFIER_ICON                     = UI_PATH + "qr_magnifier_icon";

    public static final String DIALOG_PATH                           = "dialog/";
    public static final String DIALOG_BUBBLE_CLASSIC                 = DIALOG_PATH + "bubble_classic";
    public static final String DIALOG_BUBBLE_CLASSIC_ROUND           = DIALOG_PATH + "bubble_classic_round";
    public static final String DIALOG_BUBBLE_CLASSIC_SQUARE          = DIALOG_PATH + "bubble_classic_square";
    public static final String DIALOG_BUBBLE_ARROW_DOWN              = DIALOG_PATH + "bubble_arrow_down";
    public static final String DIALOG_BUBBLE_ARROW_BEND_LEFT         = DIALOG_PATH + "bubble_arrow_bend_left";
    public static final String DIALOG_BUBBLE_CLASSIC_CONTINUE_BUTTON = DIALOG_PATH + "bubble_classic_continue_button";
    public static final String DIALOG_RECTANGLE                      = DIALOG_PATH + "dialog_rectangle";
    public static final String DIALOG_SQUARE                         = DIALOG_PATH + "dialog_square";
    public static final String DIALOG_SQUARE_BORDER_LINES            = DIALOG_PATH + "dialog_square_borderlines";
    public static final String DIALOG_BADGE_BOARD                    = DIALOG_PATH + "dialog_badgeboard";
    public static final String DIALOG_THIN_BORDER                    = DIALOG_PATH + "thin_border";

    public static final String DIALOG_BUBBLE_ARROW_BOTTOM            = DIALOG_PATH + "bubble_arrow_bottom";
    public static final String DIALOG_BUBBLE_ARROW_TOP               = DIALOG_PATH + "bubble_arrow_top";
    public static final String DIALOG_BUBBLE_ARROW_LEFT              = DIALOG_PATH + "bubble_arrow_left";
    public static final String DIALOG_BUBBLE_ARROW_RIGHT             = DIALOG_PATH + "bubble_arrow_right";

    public static final String DIALOG_BUBBLE_ARROW_BEND_LEFT_BOTTOM  = DIALOG_PATH + "bubble_arrow_bend_left_bottom";
    public static final String DIALOG_BUBBLE_ARROW_BEND_LEFT_TOP     = DIALOG_PATH + "bubble_arrow_bend_left_top";
    public static final String DIALOG_BUBBLE_ARROW_BEND_LEFT_LEFT    = DIALOG_PATH + "bubble_arrow_bend_left_left";
    public static final String DIALOG_BUBBLE_ARROW_BEND_LEFT_RIGHT   = DIALOG_PATH + "bubble_arrow_bend_left_right";

    public static final String DIALOG_BUBBLE_ARROW_BEND_LEFT_NEW     = DIALOG_PATH + "bubble_arrow_bend_left_new";
    public static final String DIALOG_BUBBLE_ARROW_BEND_RIGHT_NEW    = DIALOG_PATH + "bubble_arrow_bend_right_new";

    public static final String DIALOG_BUBBLE_CLASSIC_BOTTOM          = DIALOG_PATH + "bubble_classic_bottom";
    public static final String DIALOG_BUBBLE_CLASSIC_TOP             = DIALOG_PATH + "bubble_classic_top";
    public static final String DIALOG_BUBBLE_CLASSIC_LEFT            = DIALOG_PATH + "bubble_classic_left";
    public static final String DIALOG_BUBBLE_CLASSIC_RIGHT           = DIALOG_PATH + "bubble_classic_right";

    public static final String DIALOG_BUBBLE_CLASSIC_LEFT_NEW        = DIALOG_PATH + "bubble_classic_left_new";
    public static final String DIALOG_BUBBLE_CLASSIC_RIGHT_NEW       = DIALOG_PATH + "bubble_classic_right_new";

    public static final String DIALOG_BUBBLE_CLASSIC_SQUARE_SHADOW   = DIALOG_PATH + "bubble_classic_square_shadow";
    public static final String DIALOG_CLASSIC_LEFT_SHADOW            = DIALOG_PATH + "bubble_classic_left_shadow";
    public static final String DIALOG_CLASSIC_RIGHT_SHADOW           = DIALOG_PATH + "bubble_classic_right_shadow";

    public static final String DIALOG_CLASSIC_BUTTON_SHADOW          = DIALOG_PATH + "dialog_button_shadow";

    public static final String DIALOG_WARNING                        = DIALOG_PATH + "quitdialog_warning";

    public static final String PROGRESS_BAR             = UI_PATH + "progressbar";
    public static final String PROGRESS_BAR_FILL        = UI_PATH + "progressbar_fill";
    public static final String RANK_UP_BACKGROUND       = UI_PATH + "rankup_bg";
    public static final String RANK_UP_PROMOTION        = UI_PATH + "promotion_label";
    public static final String RANK_UP_STAR             = UI_PATH + "rankup_star";
    public static final String DEBUG_CUP_BUTTON_RELEASED= UI_PATH + "debug_cup_released";
    public static final String DEBUG_CUP_BUTTON_PRESSED = UI_PATH + "debug_cup_pressed";
    public static final String PROGRESS_BAR_BACKGROUND_9PATCH = UI_PATH + "progressbar_background";

    public static final String UNIVERSAL_PATH    = "universal/";
    public static final String BACKGROUND_WOODEN = UNIVERSAL_PATH + "background_wooden";

    public void load() {
        // don't check atlas file -> all assets needn't be unzipped yet
        loadAtlas(APPLICATION_ATLAS, false);
    }

    public TextureRegion getTextureRegion(String textureName) {
        return getTextureRegionFromAtlas(APPLICATION_ATLAS, textureName, null);
    }

    public NinePatch getPatch(String patchName) {
        return getPatchFromAtlas(APPLICATION_ATLAS, patchName);
    }

    public Animation getAnimation(String name, int framesCount, float frameDuration) {
        return getAnimationFromAtlas(APPLICATION_ATLAS, name, framesCount, frameDuration);
    }

}