/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.zip;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.checksum.Checksum;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.util.Log;

/**
 * Created by drahomir on 1/25/17.
 */

public class TablexiaAssetsUtil {
    private static final Class  CLASS                               =  TablexiaAssetsUtil.class;

    private static final String SNAPSHOT_PACKAGE_NAME               = "SNAPSHOT";

    private static final String TABLEXIA_TRUST_KEYSTORE_NAME        = "tablexiaTrustKeystore";
    private static final String TABLEXIA_TRUST_KEYSTORE_PASSWORD    = "tablexia";

    public static boolean checkAssets(String buildChecksum, String extractDestinationDirectory) {
        File file = new File(extractDestinationDirectory);
        if (file.exists() && file.isDirectory()) {
            try {
                String runtimeChecksum = Checksum.getMd5OfDir(file);
                Log.debug(CLASS, "Comparing assets checksums: [BUILD: " + buildChecksum + "] - [RUNTIME: " + runtimeChecksum + "]");
                return runtimeChecksum.equals(buildChecksum);
            } catch (NoSuchAlgorithmException e) {
                Log.err(CLASS, "Cannot get checksum for assets!", e);
            } catch (IOException e) {
                Log.err(CLASS, "Cannot get checksum for assets!", e);
            }
        }
        return false;
    }

    public static boolean checkAssetsPackage(FileHandle downloadedChecksum, FileHandle downloadedFile) {
        if (downloadedChecksum.exists() && downloadedFile.exists() && !downloadedFile.isDirectory()) {
            try {
                String fileChecksum = Checksum.getMd5OfFile(downloadedFile.file());
                String controlChecksum = downloadedChecksum.readString();
                Log.debug(CLASS, "Comparing assets package checksum: [PACKAGE: " + fileChecksum + "] - [CONTROL: " + controlChecksum + "]");
                return fileChecksum.equals(controlChecksum);
            } catch (NoSuchAlgorithmException e) {
                Log.err(CLASS, "Cannot get checksum for assets package!", e);
            } catch (IOException e) {
                Log.err(CLASS, "Cannot get checksum for assets package!", e);
            }
        }
        return false;
    }

    public static void prepareKeyStore() {
        FileHandle keystoreExternal = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaAbstractFileManager.KeystoreStorageType.EXTERNAL, TABLEXIA_TRUST_KEYSTORE_NAME);
        FileHandle keystoreInternal = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaAbstractFileManager.KeystoreStorageType.INTERNAL, TABLEXIA_TRUST_KEYSTORE_NAME);
        keystoreExternal.write(keystoreInternal.read(), false);

        System.setProperty("javax.net.ssl.trustStore", keystoreExternal.file().getAbsolutePath());
        System.setProperty("javax.net.ssl.trustStorePassword", TABLEXIA_TRUST_KEYSTORE_PASSWORD);
    }

    public static void removeSnapshotsForLocale(Locale locale){
        removeUnnecessaryPackageFiles(locale, true);
    }

    public static void removeUnnecessaryPackageFiles(Locale locale, String assetFileName) {
        File dir = new File(TablexiaAbstractFileManager.getAssetsPackStoragePath());
        File[] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                boolean sameAssetPackType = TablexiaSettings.getInstance().isUseHdAssets() == name.endsWith(TablexiaSettings.AssetsPackDefinition.HD.getPackSuffix());
                return (name.startsWith(locale.getLanguage()) && !name.contains(assetFileName) && !name.contains(SNAPSHOT_PACKAGE_NAME) && sameAssetPackType);
            }
        });

        if (files == null) return;

        for (File assetFile : files) {
            Log.info(CLASS, "Removing unnecessary asset file: " + assetFile.getName());
            deleteFile(assetFile);
        }
    }

    public static void removeInvalidPackageFiles(Locale locale, String assetFileName) {
        File dir = new File(TablexiaAbstractFileManager.getAssetsPackStoragePath());
        File[] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return (name.startsWith(locale.getLanguage()) && name.contains(assetFileName) && !name.contains(SNAPSHOT_PACKAGE_NAME));
            }
        });

        if (files == null) return;

        for (File assetFile : files) {
            Log.info(CLASS, "Removing invalid asset file: " + assetFile.getName());
            deleteFile(assetFile);
        }
    }

    public static void removeUnnecessaryPackageFiles(Locale locale, boolean removeSnapshots) {
        File dir = new File(TablexiaAbstractFileManager.getAssetsPackStoragePath());
        File[] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return (name.startsWith(locale.getLanguage()) && (name.contains(SNAPSHOT_PACKAGE_NAME) && removeSnapshots));
            }
        });

        if (files == null) return;

        for (File assetFile : files) {
            Log.info(CLASS, "Removing unnecessary asset file: " + assetFile.getName());
            deleteFile(assetFile);
        }
    }

    public static boolean deleteDirectory(File directory) {
        if (directory.exists()) {
            File[] files = directory.listFiles();

            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    } else {
                        deleteFile(files[i]);
                    }
                }
            }
        }
        return deleteFile(directory);
    }

    public static boolean deleteFile(File file) {
        // workaround for android devices filesystem lock
        final File renamedFile = new File(file.getPath() + System.currentTimeMillis());
        file.renameTo(renamedFile);
        return renamedFile.delete();
    }

    public static void unzip(FileHandle zipFile, String extractDestinationDirectory) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(zipFile.read()));
        try {
            ZipEntry zipEntry;
            int count;
            byte[] buffer = new byte[8192];
            Log.debug("Safe unzipping check", "Comparing cannonical paths unzipped files with expected directories" );
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {

                if (zipEntry.isDirectory()) {
                    continue;
                }
                File file = new File(extractDestinationDirectory, zipEntry.getName());
                String canonicalPath = file.getCanonicalPath();
                String absolutePath = file.getAbsolutePath();
                //check if canonical paths to unzipped files are underneath an expected directory (Zip Path Traversal Vulnerability)
                if (canonicalPath!=null && !canonicalPath.startsWith(extractDestinationDirectory)) {
                    Log.info("TablexiaAssetsUtil", "Canonical path is not underneath an expected directory!" + canonicalPath + extractDestinationDirectory );
                    //make sure absolute path is OK - Android issue with symlink and external memory
                    if(Gdx.app.getType() != Application.ApplicationType.Android || !absolutePath.startsWith(extractDestinationDirectory)) {
                        throw new SecurityException("Canonical paths of unzipped files are not underneath an expected directory! \n" + "expected extract path: " + extractDestinationDirectory + ", cannonical path: " + canonicalPath + ", absolute path: " + absolutePath);
                    }
                }

                File dir = zipEntry.isDirectory() ? file : file.getParentFile();
                if(!dir.exists()) dir.mkdir();

                if (!dir.isDirectory() && !dir.mkdirs()) {
                    throw new FileNotFoundException("Failed to ensure directory: " + dir.getAbsolutePath());
                }
                if (zipEntry.isDirectory()) {
                    continue;
                }
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zipInputStream.read(buffer)) != -1) {
                        fout.write(buffer, 0, count);
                    }
                } finally {
                    fout.close();
                }
            }
            Log.debug("Safe unzipping check", "OK" );
        } finally {
            zipInputStream.close();
        }
    }
}