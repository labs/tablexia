/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.zip;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.async.AsyncTask;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.util.Log;

/**
 * Created by drahomir on 1/25/17.
 */

public class TablexiaAssetsAsyncTask implements AsyncTask<Void>, TablexiaAssetsManager.ILoaderDialogAcceptListener {
    private static final String ZIP_FILE_EXTENSION      = ".zip";
    private static final String CHECKSUM_FILE_EXTENSION = ".checksum";
    private static final String SNAPSHOT_FILE_NAME      = "SNAPSHOT";

    private static final long   ZIP_MAX_FILE_SIZE         = 220 * 1024 * 1024L;
    private static final long   EXTRACTED_ASSETS_MAX_SIZE = ZIP_MAX_FILE_SIZE;
    private static final Object DOWNLOAD_LOCK             = new Object();

    private              AtomicBoolean exit          = new AtomicBoolean(false);

    private final TablexiaAssetsDownloader tablexiaAssetsDownloader;

    private TablexiaAssetsManager manager;
    private final Locale locale;
    private final String checksum;

    public TablexiaAssetsAsyncTask(TablexiaAssetsManager manager, Locale locale, String checksum) {
        this.manager = manager;
        this.locale = locale;
        this.checksum = checksum;
        this.tablexiaAssetsDownloader = new TablexiaAssetsDownloader(manager);
    }

    @Override
    public Void call() throws Exception {
        //Check current .../assets/ folder
        ApplicationBus.getInstance().post(TablexiaAssetsManager.AssetsManagerEvent.assetsCheckStartedEvent()).asynchronously();
        if (TablexiaAssetsUtil.checkAssets(checksum, TablexiaAbstractFileManager.getFileStoragePath(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL))) {
            Log.info(getClass(), "Assets check has been successfully finished!");
            manager.onSuccess();
            return null;
        }

        //Delete ..assets/
        TablexiaAssetsUtil.deleteDirectory(TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL).file());

        String assetsPackageName = locale.getLanguage() + "_" + checksum + TablexiaSettings.AssetsPackDefinition.getAssetsPackSuffix() + ZIP_FILE_EXTENSION;
        String checksumPackageName = locale.getLanguage() + "_" + checksum + TablexiaSettings.AssetsPackDefinition.getAssetsPackSuffix() + CHECKSUM_FILE_EXTENSION;

        FileHandle checksumPackageFileHandle = TablexiaAbstractFileManager.getAssetsPackFileHandle(checksumPackageName);
        FileHandle assetsPackageFileHandle = TablexiaAbstractFileManager.getAssetsPackFileHandle(assetsPackageName);

        if(TablexiaSettings.getInstance().getPlatform().isAssetsBundled() && (!checksumPackageFileHandle.file().exists() || !assetsPackageFileHandle.file().exists())){
            Log.info(getClass(), "Bundled assets are not found");

            //fallback to external assets
            checksumPackageFileHandle = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaAbstractFileManager.DownloadStorageType.EXTERNAL, checksumPackageName);
            assetsPackageFileHandle = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaAbstractFileManager.DownloadStorageType.EXTERNAL, assetsPackageName);
        }

        //Remove packages with same locale but wrong checksum in their name...
        TablexiaAssetsUtil.removeUnnecessaryPackageFiles(locale, checksum);

        //Check if package in ..download/ folder is not corrupted
        Log.info(getClass(), "Checking local packages");
        ApplicationBus.getInstance().post(TablexiaAssetsManager.AssetsManagerEvent.assetsCheckStartedEvent()).asynchronously();
        if (!TablexiaAssetsUtil.checkAssetsPackage(checksumPackageFileHandle, assetsPackageFileHandle)) {
            Log.info(getClass(), "Local packages are not valid");
            TablexiaAssetsUtil.removeInvalidPackageFiles(locale, checksum);
            if (!isEnoughSpace(TablexiaAbstractFileManager.DataStorageType.EXTERNAL)) {
                manager.onNotEnoughSpace();
                return null;
            }

            TablexiaAssetsUtil.prepareKeyStore();

            // Check if user ins't using mobile internet
            if (Tablexia.getConnectionManager().isUsingMobileData()) {
                Log.info(getClass(), "Mobile data is in use -> notifying user");
                // If mobile internet is used -> notify user about additional data that will be downloaded
                stopDownload();
                if (exit.get()) {
                    Log.info(getClass(), "User declined request of using mobile data");
                    manager.onLoadUnsuccessful();
                    return null;
                }
                Log.info(getClass(), "User decided to use mobile data to download assets");
            }

          if (Tablexia.getLegacyManager().needLegacyCheck()){
              if(!Tablexia.getLegacyManager().isSupportedDevice()){
                  stopDownload();
                  manager.onLegacyError(ApplicationTextManager.ApplicationTextsAssets.ASSETS_MANAGER_OLD_DEVICE);
              }
          }

            //switch to external storage before download
            assetsPackageFileHandle = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaAbstractFileManager.DownloadStorageType.EXTERNAL, assetsPackageName);
            checksumPackageFileHandle = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaAbstractFileManager.DownloadStorageType.EXTERNAL, checksumPackageName);

            if (!downloadAssetsPackage(checksumPackageFileHandle, assetsPackageFileHandle)) {
                if (TablexiaSettings.getInstance().getBuildType().equals(TablexiaSettings.BuildType.RELEASE)){
                    manager.onLoadUnsuccessful();
                    return null;
                }else {
                    assetsPackageFileHandle = TablexiaAbstractFileManager.getAssetsPackFileHandle(
                            locale.getLanguage() + "_" + SNAPSHOT_FILE_NAME +
                                    TablexiaSettings.AssetsPackDefinition.getAssetsPackSuffix() +
                                    ZIP_FILE_EXTENSION);
                    checksumPackageFileHandle = TablexiaAbstractFileManager.getAssetsPackFileHandle(
                            locale.getLanguage() + "_" + SNAPSHOT_FILE_NAME +
                                    TablexiaSettings.AssetsPackDefinition.getAssetsPackSuffix() +
                                    CHECKSUM_FILE_EXTENSION);

                    if(TablexiaSettings.getInstance().getPlatform().isAssetsBundled() && (!checksumPackageFileHandle.file().exists() || !assetsPackageFileHandle.file().exists())){
                        Log.info(getClass(), "Bundled snapshots are not found");

                        //fallback to external assets
                        checksumPackageFileHandle = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaAbstractFileManager.DownloadStorageType.EXTERNAL,                             locale.getLanguage() + "_" + SNAPSHOT_FILE_NAME +
                                TablexiaSettings.AssetsPackDefinition.getAssetsPackSuffix() +
                                CHECKSUM_FILE_EXTENSION);
                        assetsPackageFileHandle = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaAbstractFileManager.DownloadStorageType.EXTERNAL,                             locale.getLanguage() + "_" + SNAPSHOT_FILE_NAME +
                                TablexiaSettings.AssetsPackDefinition.getAssetsPackSuffix() +
                                ZIP_FILE_EXTENSION);
                    }

                    if (!TablexiaAssetsUtil.checkAssetsPackage(checksumPackageFileHandle, assetsPackageFileHandle)) {
                        Log.info(getClass(), "Local snapshot isn't valid.");
                        TablexiaAssetsUtil.removeSnapshotsForLocale(locale);
                        if (!downloadAssetsPackage(checksumPackageFileHandle, assetsPackageFileHandle)) {
                            Log.info(getClass(),"Can't download snapshot");
                            manager.onLoadUnsuccessful();
                            return null;
                        }
                        Log.info(getClass(),"Snapshot was successfully downloaded");
                    }
                }
            }
            
            checkSpaceAndUnzip(assetsPackageFileHandle);
            return null;
        }

        //Check If there is enough space to extract the package
        checkSpaceAndUnzip(assetsPackageFileHandle);
        return null;
    }
    
    private boolean downloadAssetsPackage(FileHandle checksumFileHandle, FileHandle assetsPackageFileHandle){
        if (!tablexiaAssetsDownloader.downloadChecksumFile(checksumFileHandle)) return false;
        Log.info(getClass(),"Checksum file: " + checksumFileHandle.name() + " was downloaded.");
        if (!tablexiaAssetsDownloader.downloadAssetsPackage(assetsPackageFileHandle)) return false;
        Log.info(getClass(),"Assets package: " + assetsPackageFileHandle.name() + " was downloaded.");

        return true;
    }
    
    private void checkSpaceAndUnzip(FileHandle assetsPackFileHandle) throws IOException {
        if (isEnoughSpace(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL)) {
            //Extract package and exit
            ApplicationBus.getInstance().post(TablexiaAssetsManager.AssetsManagerEvent.extractingAssetsStartedEvent()).asynchronously();
            TablexiaAssetsUtil.unzip(assetsPackFileHandle, TablexiaAbstractFileManager.getFileStoragePath(TablexiaAbstractFileManager.AssetsStorageType.EXTERNAL));
            manager.onSuccess();
        } else {
            manager.onNotEnoughSpace();
        }
    }
    
    private void stopDownload(){
        manager.onUsingMobileData(this);
        synchronized (DOWNLOAD_LOCK) {
            try {
                DOWNLOAD_LOCK.wait();
            } catch (InterruptedException e) {
                Log.err(getClass(), e.toString());
            }
        }
    }
    
    private boolean isEnoughSpace(TablexiaAbstractFileManager.StorageType storageType){
        return Tablexia.getFileSystemManager().getSpaceAvailable(TablexiaAbstractFileManager.getFileStoragePath(storageType)) > EXTRACTED_ASSETS_MAX_SIZE;
    }
    
    private void notifyDownloadLock(){
        synchronized (DOWNLOAD_LOCK){
            try {
                DOWNLOAD_LOCK.notify();
            }catch (Exception e){
                Log.err(getClass(), e.toString());
            }
        }
    }
    
    @Override
    public void onAccept() {
        exit.set(false);
        notifyDownloadLock();
    }

    @Override
    public void onDecline() {
        exit.set(true);
        notifyDownloadLock();
    }
}
