/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.zip;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import net.engio.mbassy.listener.Handler;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaSettings;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.loader.IApplicationLoader;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaDataManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;

/**
 * Created by drahomir on 1/25/17.
 */

public class TablexiaAssetsManager extends TablexiaDataManager<Void> implements IApplicationLoader {
    public static final String DOWNLOAD_REQUEST_DIALOG_NAME = "downloadRequest";

    private Locale locale;
    private Map<String, String> buildChecksumMap;
    
    private boolean error = false;
    private boolean success = false;
    
    private volatile boolean applicationTerminated = false;
    
    public void load(Locale locale, Map<String, String> buildChecksumMap) {
        this.locale = locale;
        this.buildChecksumMap = buildChecksumMap;
        ApplicationBus.getInstance().subscribe(this);
        setAsyncTask(new TablexiaAssetsAsyncTask(this, locale, buildChecksumMap.get(locale.getLanguage())));
    }
    
    public ArrayList<TablexiaSettings.LocaleDefinition> checkAvailablePackages(){
        Log.info(getClass(),"Looking for local valid packages");
        ApplicationBus.getInstance().post(AssetsManagerEvent.assetsCheckStartedEvent()).asynchronously();
        ArrayList<TablexiaSettings.LocaleDefinition> availableLocale = new ArrayList<>();
        for (TablexiaSettings.LocaleDefinition localeDefinition : TablexiaSettings.LocaleDefinition.getSelectableLocaleDefinitions()){
            String localeChecksum = buildChecksumMap.get(localeDefinition.getLocale().getLanguage());
            FileHandle checksumFileHandle = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaSettings.getInstance()
                    .getPlatform().isAssetsBundled() ?
                    TablexiaAbstractFileManager.DownloadStorageType.INTERNAL : TablexiaAbstractFileManager.DownloadStorageType.EXTERNAL, localeDefinition.getLocale().getLanguage() + "_" + localeChecksum + TablexiaSettings.AssetsPackDefinition.getAssetsPackSuffix() + ".checksum");
            FileHandle packageFileHandle = TablexiaAbstractFileManager.getFileStoragePathFileHandle(TablexiaSettings.getInstance()
                    .getPlatform().isAssetsBundled() ?
                    TablexiaAbstractFileManager.DownloadStorageType.INTERNAL : TablexiaAbstractFileManager.DownloadStorageType.EXTERNAL, localeDefinition.getLocale().getLanguage() + "_" + localeChecksum + TablexiaSettings.AssetsPackDefinition.getAssetsPackSuffix() + ".zip");
            if (TablexiaAssetsUtil.checkAssetsPackage(checksumFileHandle, packageFileHandle)) {
                Log.info(getClass(),"Found valid package: " + localeDefinition.name());
                availableLocale.add(localeDefinition);
            }
        }
        return availableLocale;
    }

    public boolean isApplicationTerminated() {
        return applicationTerminated;
    }

    public void onApplicationTerminated() {
        this.applicationTerminated = true;
    }

    @Override
    public boolean update() {
        return super.update() && success;
    }

    protected void onSuccess() {
        ApplicationBus.getInstance().publishAsync(TablexiaAssetsManager.AssetsManagerEvent.loadFinished());
        success = true;
    }

    protected void onInconsistentConnectionError(ILoaderDialogAcceptListener acceptListener) {
        onDownloadError(acceptListener,
                        ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.ASSETS_MANAGER_ERROR),
                        ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_RETRY),
                        ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_DECLINE));
    }

    protected void onUsingMobileData(ILoaderDialogAcceptListener acceptListener) {
        onDownloadError(acceptListener, ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.ASSETS_MANAGER_DOWNLOAD_REQUEST));
    }

    protected void onLegacyError(String error){
        onFail(error);
    }
    
    protected void onDownloadError(ILoaderDialogAcceptListener acceptListener, String errorMessage){
        onDownloadError(acceptListener, 
                        errorMessage, 
                        ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_YES), 
                        ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_NO));
    }

    protected void onDownloadError(ILoaderDialogAcceptListener acceptListener, String errorMessage, String yesButtonText, String noButtonText) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                TablexiaComponentDialog dialog = TablexiaComponentDialogFactory.getInstance().createYesNoDialog(
                        null,
                        errorMessage,
                        yesButtonText,
                        noButtonText,
                        new ClickListener() {
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                acceptListener.onAccept();
                            }
                        },
                        new ClickListener() {
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                acceptListener.onDecline();
                            }
                        }
                , true);
                dialog.setName(DOWNLOAD_REQUEST_DIALOG_NAME);
                dialog.show(Tablexia.ERROR_DIALOG_WIDTH, Tablexia.ERROR_DIALOG_HEIGHT);
            }
        });
    }

    protected void onNotEnoughSpace() {
        onFail(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.ASSETS_MANAGER_NOT_ENOUGH_SPACE));
    }

    protected void onFail(String errorText){
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                TablexiaComponentDialogFactory.getInstance().createSingleButtonNotifyDialog(
                        errorText,
                        new ClickListener(){
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                super.clicked(event, x, y);
                                Gdx.app.exit();
                            }
                        }
                ).show(Tablexia.ERROR_DIALOG_WIDTH, Tablexia.ERROR_DIALOG_HEIGHT);
            }
        });
    }
    
    @Override
    public void dispose() {

    }
    
    protected void onLoadUnsuccessful(){
        ApplicationBus.getInstance().post(TablexiaAssetsManager.AssetsManagerEvent.downloadFailed()).asynchronously();
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                TablexiaComponentDialogFactory.getInstance().createYesNoDialog(
                        ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.ASSETS_MANAGER_ROLLBACK_REQUEST),
                        new ClickListener() {
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                ApplicationBus.getInstance().publishAsync(AssetsManagerEvent.assetsCheckStartedEvent());
                                ApplicationBus.getInstance().publishAsync(new Tablexia.ApplicationLocaleResetEvent());
                            }
                        },
                        new ClickListener() {
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                Gdx.app.exit();
                            }
                        }
                ).show(Tablexia.ERROR_DIALOG_WIDTH, Tablexia.ERROR_DIALOG_HEIGHT);
            }
        });
    }
    
    public static class AssetsManagerEvent implements ApplicationBus.ApplicationEvent {
        public enum AssetsManagerEventType {
            AssetCheckStarted,
            ExtractingAssetsStarted,
            LoadFinished,
            DownloadStarted,
            DownloadProgress,
            DownloadFinished,
            DownloadFailed,
            InconsistentConnection
        }

        private AssetsManagerEventType eventType;
        private float progress;

        private AssetsManagerEvent(AssetsManagerEventType eventType, float progress) {
            this.eventType = eventType;
            this.progress = progress;
        }
        
        private AssetsManagerEvent(AssetsManagerEventType eventType){
            this(eventType, 0);
        }

        public AssetsManagerEventType getEventType() {
            return eventType;
        }

        public float getProgress() {
            return progress;
        }

        public static AssetsManagerEvent assetsCheckStartedEvent() {
            return new AssetsManagerEvent(AssetsManagerEventType.AssetCheckStarted);
        }

        public static AssetsManagerEvent extractingAssetsStartedEvent() {
            return new AssetsManagerEvent(AssetsManagerEventType.ExtractingAssetsStarted);
        }

        public static AssetsManagerEvent downloadStarted() {
            return new AssetsManagerEvent(AssetsManagerEventType.DownloadStarted);
        }

        public static AssetsManagerEvent downloadFinished() {
            return new AssetsManagerEvent(AssetsManagerEventType.DownloadFinished, 1);
        }

        public static AssetsManagerEvent downloadProgress(float progress) {
            return new AssetsManagerEvent(AssetsManagerEventType.DownloadProgress, progress);
        }

        public static AssetsManagerEvent downloadFailed() {
            return new AssetsManagerEvent(AssetsManagerEventType.DownloadFailed);
        }

        public static AssetsManagerEvent downloadInconsistentConnection() {
            return new AssetsManagerEvent(AssetsManagerEventType.InconsistentConnection);
        }
        
        public static AssetsManagerEvent loadFinished(){
            return new AssetsManagerEvent(AssetsManagerEventType.LoadFinished);
        }
    }
    
/////////Application terminating event handling    

    public static class ApplicationTerminatedEvent implements ApplicationBus.ApplicationEvent {
        public ApplicationTerminatedEvent() {
        }
    }
    
    @Handler
    public void handleApplicationTerminatedEvent(ApplicationTerminatedEvent applicationTerminatedEvent) {
        Log.info(getClass(), "Application is terminated. Stopping download threads before closing.");
        onApplicationTerminated();
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                Gdx.app.exit();
            }
        });
        
    }

/////////Loader dialog listener interface

    public interface ILoaderDialogAcceptListener {
        void onAccept();
        void onDecline();
    }
}