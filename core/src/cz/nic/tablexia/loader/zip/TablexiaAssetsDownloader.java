/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader.zip;

import com.badlogic.gdx.Net;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Timer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.TablexiaBuildConfig;
import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.util.Log;

/**
 * Created by drahomir on 1/25/17.
 */

public class TablexiaAssetsDownloader implements TablexiaAssetsManager.ILoaderDialogAcceptListener {
    private static final Class  CLASS                               =  TablexiaAssetsDownloader.class;

    private static final String TABLEXIA_ASSETS_DOWNLOAD_BASE_PATH  = TablexiaBuildConfig.TABLEXIA_ASSETS_URL + "/";
    private static final int    TABLEXIA_ASSETS_DOWNLOAD_TIMEOUT    = 2500;
    private static final int    STATUS_CODE_SUCCESS                 = 206;
    private static final int    STATUS_CODE_OK                       = 200;
    private static final int    STATUS_CODE_NF                       = 404;

    private static final int    NUMBER_OF_TRIES                     = 3;
    private static final float  TRIES_DELAY_SECONDS                 = 1.5f;

    private static final String REQUEST_HEADER_RANGE                = "Range";
    private static final String REQUEST_HEADER_RANGE_FORMAT         = "bytes=%d-";

    private static final Object DOWNLOAD_LOCK                       = new Object();
    private static final Object TRY_DELAY_LOCK                      = new Object();
    private static final Object DIALOG_LOCK                         = new Object();

    private AtomicBoolean exit = new AtomicBoolean(false);

    private class TablexiaHttpResponseListener implements Net.HttpResponseListener {
        private final boolean[] downloadResult = new boolean[]{false};
        private final boolean[] hasDownloadResult = new boolean[]{false};
        private final boolean[] downloadInProcess = new boolean[]{false};
        private final boolean[] errorOccured = new boolean[]{false};
        
        private final byte[]    byteBuffer = new byte[1024];

        private FileHandle                     fileHandle;
        private OutputStream                   outputStream;

        private int                            tries = 0;

        private TablexiaHttpResponseListener(FileHandle fileHandle) {
            this.fileHandle = fileHandle;
            this.outputStream = fileHandle.write(true);
            reset();
        }

        public void reset(FileHandle fileHandle, long bytesDownloaded) {
            hasDownloadResult[0] = false;
            downloadInProcess[0] = false;
            this.fileHandle = fileHandle;
        }
        
        public void reset(){
            hasDownloadResult[0] = false;
            downloadInProcess[0] = false;
            errorOccured[0] = false;
        }

        @Override
        public void handleHttpResponse(Net.HttpResponse httpResponse) {
            int responseStatusCode = httpResponse.getStatus().getStatusCode();
            if (responseStatusCode != STATUS_CODE_SUCCESS && responseStatusCode != STATUS_CODE_OK && responseStatusCode != STATUS_CODE_NF) {
                Log.info(CLASS, String.format("Download failed. Returned status code: %d", httpResponse.getStatus().getStatusCode()));
                tryAgain();
                return;
            } else if (responseStatusCode == STATUS_CODE_NF){
                Log.info(getClass(), "File was not found on server");
                
                errorOccured[0] = true;
                hasDownloadResult[0] = true;
                downloadResult[0] = false;
                
                synchronized (DOWNLOAD_LOCK) {
                    DOWNLOAD_LOCK.notify();
                }
                
                return;
            }

            InputStream is = httpResponse.getResultAsStream();

            long length = Long.parseLong(httpResponse.getHeader("Content-Length"));
            long read = 0;

            //Add already downloaded part of file.
            long fileSize = fileHandle.length();
            length += fileSize;
            read   += fileSize;

            Log.info(getClass(), "Download in progress!");
            ApplicationBus.getInstance().post(TablexiaAssetsManager.AssetsManagerEvent.downloadStarted()).asynchronously();

            int count;
            
            downloadInProcess[0] = true;
            
            try {
                while ((count = is.read(byteBuffer, 0, byteBuffer.length)) != -1 && !manager.isApplicationTerminated()) {
                    outputStream.write(byteBuffer, 0, count);
                    read += count;

                    tries = 0;

                    final double done = read / (double) length;
                    final int progress = (int)(done * 100);

                    ApplicationBus.getInstance().post(TablexiaAssetsManager.AssetsManagerEvent.downloadProgress((float) done)).asynchronously();
                    ApplicationBus.getInstance().post(new AbstractTablexiaScreen.ScreenInfoEvent("Downloading: ", "Progress: [ " + progress + "% ]")).asynchronously();
                }

                Log.info(CLASS, "Download finished!");
                ApplicationBus.getInstance().post(TablexiaAssetsManager.AssetsManagerEvent.downloadFinished()).asynchronously();
                synchronized (DOWNLOAD_LOCK) {
                    downloadResult[0] = true;
                    hasDownloadResult[0] = true;
                    DOWNLOAD_LOCK.notify();
                }
            } catch (IOException e) {
                failed(e);
            }
        }

        public void tryAgain() {
            ApplicationBus.getInstance().post(TablexiaAssetsManager.AssetsManagerEvent.downloadInconsistentConnection()).asynchronously();

            Log.info(CLASS, "Download interrupted! Trying again!");
            
            downloadInProcess[0] = false;
            
            if(++tries >= NUMBER_OF_TRIES) {
                hasDownloadResult[0] = true;
            }
            else {
                hasDownloadResult[0] = false;
            }
            synchronized (DOWNLOAD_LOCK) {
                DOWNLOAD_LOCK.notify();
            }
        }

        @Override
        public void failed(Throwable t) {
            Log.err(getClass(), t.toString());
            tryAgain();
        }

        @Override
        public void cancelled() {
            tryAgain();
        }

        public boolean getHasDownloadResult() {
            return hasDownloadResult[0];
        }

        public boolean getDownloadResult() {
            return downloadResult[0];
        }
        
        public boolean isDownloadInProcess() {
            return downloadInProcess[0];
        }
        
        public boolean isErrorOccured(){
            return errorOccured[0];
        }
        
    }

    ///// TABLEXIA ASSETS DOWNLOADER CLASS /////

    private final TablexiaAssetsManager manager;

    public TablexiaAssetsDownloader(TablexiaAssetsManager manager) {
        this.manager = manager;
    }

    public boolean downloadChecksumFile(FileHandle checkSumFileHandle) {
        return downloadFile(checkSumFileHandle);
    }

    public boolean downloadAssetsPackage(FileHandle assetsPackageFileHandle) {
        return downloadFile(assetsPackageFileHandle);
    }
    
    public boolean downloadSnapshotPackage(FileHandle snapshotFileHandle){
        return downloadFile(snapshotFileHandle);
    }
    
    public boolean downloadFile(FileHandle fileHandle) {
        TablexiaHttpResponseListener httpResponseListener = new TablexiaHttpResponseListener(fileHandle);

        while(true) {
            sendHttpRequest(fileHandle, httpResponseListener);

            //Wait for download result
            synchronized (DOWNLOAD_LOCK) {
                if (httpResponseListener.getHasDownloadResult() == false) {
                    try {
                        DOWNLOAD_LOCK.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            
            if (manager.isApplicationTerminated()) break;

            if (!httpResponseListener.getHasDownloadResult()) {
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        synchronized (TRY_DELAY_LOCK) {
                            Log.info(CLASS, "Try successfully delayed!");
                            TRY_DELAY_LOCK.notify();
                        }
                    }
                }, TRIES_DELAY_SECONDS);

                synchronized (TRY_DELAY_LOCK) {
                    try {
                        Log.info(CLASS, "Delaying next try for " + TRIES_DELAY_SECONDS + " seconds!");
                        TRY_DELAY_LOCK.wait();
                    } catch (InterruptedException e) {
                        Log.err(getClass(), e.toString());
                    }
                }
            } else { //Has Result
                
                if (httpResponseListener.isErrorOccured()) break;

                if (!httpResponseListener.isDownloadInProcess()) {
                    //show dialog
                    httpResponseListener.reset();
                    
                    manager.onInconsistentConnectionError(this);

                    synchronized (DIALOG_LOCK) {
                        try {
                            DIALOG_LOCK.wait();
                        } catch (InterruptedException e) {
                            Log.err(getClass(), e.toString());
                        }
                    }

                }

                if (exit.get()) break;
                if (httpResponseListener.getDownloadResult()) break;
            }
        }

        return httpResponseListener.getDownloadResult();
    }
    
    private void notifyDialogLock(){
        synchronized (DIALOG_LOCK) {
            DIALOG_LOCK.notify();
        }
    }
    
    @Override
    public void onAccept() {
        exit.set(false);
        notifyDialogLock();
    }

    @Override
    public void onDecline() {
        exit.set(true);
        notifyDialogLock();
    }
    
    public void sendHttpRequest(FileHandle fileHandle, Net.HttpResponseListener httpResponseListener){
        Tablexia.getNet().sendHttpRequest(prepareRequest(fileHandle.name(), fileHandle.length()), httpResponseListener);
    }

    public Net.HttpRequest prepareRequest(String fileName, long bytesDownloaded) {
        Net.HttpRequest request = new Net.HttpRequest(Net.HttpMethods.GET);
        request.setTimeOut(TABLEXIA_ASSETS_DOWNLOAD_TIMEOUT);
        request.setUrl(TABLEXIA_ASSETS_DOWNLOAD_BASE_PATH + fileName);
        request.setHeader(REQUEST_HEADER_RANGE, String.format(REQUEST_HEADER_RANGE_FORMAT, bytesDownloaded));
        return request;
    }
}