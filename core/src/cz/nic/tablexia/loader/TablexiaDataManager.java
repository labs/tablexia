/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.loader;

import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.async.AsyncExecutor;
import com.badlogic.gdx.utils.async.AsyncResult;
import com.badlogic.gdx.utils.async.AsyncTask;
import com.badlogic.gdx.utils.async.ThreadUtils;

public class TablexiaDataManager<T> implements Disposable {
	
	private AsyncExecutor 	asyncExecutor;
	private AsyncTask<T>	asyncTask;
	private AsyncResult<T> 	asyncResult;
	
	private boolean 		started;
	private boolean 		complete;
	
	private T 				result;
	
	protected void setAsyncTask(AsyncTask<T> asyncTask) {
		this.asyncTask 	= asyncTask;
		result 			= null;
		asyncExecutor 	= new AsyncExecutor(1);
		started			= false;
		complete 		= false;
	}
	
	public boolean update() {
		if (started) {
			if (asyncResult == null || asyncResult.isDone()) {
				if (!complete) {
					complete = true;	
					result = asyncResult.get();
					asyncResult = null;
					asyncExecutor.dispose();
					asyncExecutor = null;
					asyncTask = null;
				}
				return true;
			}
		} else {
			if (asyncTask == null) {
				return true;
			}
			asyncResult = asyncExecutor.submit(asyncTask);
			started = true;
		}
		return false;
	}

    public void finishLoading () {
        while (!update()) {
            ThreadUtils.yield();
        }
    }
	
	public T getResult() {
		if (result == null) {
			throw new IllegalStateException("Loading not complete or didnt start!");
		}
		return result;
	}
	
	@Override
	public void dispose() {
		result = null;
	}
}