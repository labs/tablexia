/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.bus;

import net.engio.mbassy.bus.MBassador;
import net.engio.mbassy.bus.config.BusConfiguration;
import net.engio.mbassy.bus.config.Feature;
import net.engio.mbassy.bus.config.IBusConfiguration;
import cz.nic.tablexia.bus.ApplicationBus.ApplicationEvent;

public class ApplicationBus extends MBassador<ApplicationEvent> {
	
	public interface ApplicationEvent {};
	
	private static volatile ApplicationBus instance;
	private static final Object mutext = new Object();

	private ApplicationBus(IBusConfiguration configuration) {
		super(configuration);
	}
	
	public static ApplicationBus getInstance() {
		ApplicationBus bus = instance;

		if (bus == null) {
			synchronized (mutext) {
				bus = instance;
				if(bus == null) {
					IBusConfiguration defaultConfig = new BusConfiguration();
					defaultConfig.addFeature(Feature.SyncPubSub.Default());
					defaultConfig.addFeature(Feature.AsynchronousHandlerInvocation.Default());
					defaultConfig.addFeature(Feature.AsynchronousMessageDispatch.Default());
					instance = new ApplicationBus(defaultConfig);
					bus = instance;
				}
			}
		}
		return bus;
	}

}
