/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.bus.event;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.IMenuItem;

public class SubMenuControlEvent implements ApplicationBus.ApplicationEvent {

    public enum SubMenuAction {
        OPEN    (false),
        CLOSE   (true),
        TOGGLE  (null);

        private Boolean isOpen;

        SubMenuAction(Boolean isOpen) {
            this.isOpen = isOpen;
        }

        public boolean isOpen(boolean isMenuOpenNow) {
            return isOpen != null ? isOpen : !isMenuOpenNow;
        }
    }

    private Class<? extends AbstractMenu>   menu;
    private IMenuItem                       menuItem;
    private SubMenuAction subMenuAction;

    public SubMenuControlEvent(Class<? extends AbstractMenu> menu, IMenuItem menuItem, SubMenuAction subMenuAction) {
        this.menu           = menu;
        this.menuItem       = menuItem;
        this.subMenuAction = subMenuAction;
    }

    public Class<? extends AbstractMenu> getMenu() {
        return menu;
    }

    public IMenuItem getMenuItem() {
        return menuItem;
    }

    public SubMenuAction getSubMenuAction() {
        return subMenuAction;
    }
}
