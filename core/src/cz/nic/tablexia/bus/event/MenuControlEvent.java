/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.bus.event;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.menu.AbstractMenu;

public class MenuControlEvent implements ApplicationBus.ApplicationEvent {

    private Class<? extends AbstractMenu>   menu;
    private AbstractMenu.MenuAction         menuAction;
    private boolean                         animated;

    public MenuControlEvent(Class<? extends AbstractMenu> menu, AbstractMenu.MenuAction menuAction, boolean animated) {
        this.menu = menu;
        this.menuAction = menuAction;
        this.animated = animated;
    }

    public Class<? extends AbstractMenu> getMenu() {
        return menu;
    }

    public AbstractMenu.MenuAction getMenuAction() {
        return menuAction;
    }

    public boolean isAnimated() {
        return animated;
    }
}
