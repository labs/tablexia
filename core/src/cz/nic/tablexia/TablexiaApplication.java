/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import net.engio.mbassy.listener.Handler;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.bus.ApplicationBus;
import cz.nic.tablexia.bus.ApplicationBus.ApplicationEvent;
import cz.nic.tablexia.bus.event.DeleteUserSynchronizationEvent;
import cz.nic.tablexia.bus.event.DimmerControlEvent;
import cz.nic.tablexia.bus.event.DownloadUserSynchronizationEvent;
import cz.nic.tablexia.bus.event.StartFullSynchronizationEvent;
import cz.nic.tablexia.bus.event.StartIncrementalSynchronizationEvent;
import cz.nic.tablexia.debug.DebugInfo;
import cz.nic.tablexia.loader.TablexiaAbstractFileManager;
import cz.nic.tablexia.loader.TablexiaAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationAtlasManager;
import cz.nic.tablexia.loader.application.ApplicationFontManager;
import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.menu.MenuController;
import cz.nic.tablexia.model.UserDAO;
import cz.nic.tablexia.screen.AbstractTablexiaScreen;
import cz.nic.tablexia.screen.AbstractTablexiaScreen.ScreenLoadingListener;
import cz.nic.tablexia.shared.model.User;
import cz.nic.tablexia.sync.RestSynchronizationService;
import cz.nic.tablexia.sync.work.DeleteUser;
import cz.nic.tablexia.sync.work.PushDataToServer;
import cz.nic.tablexia.sync.work.SyncUser;
import cz.nic.tablexia.sync.work.UpdateAvatar;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.AnimatedImage;
import cz.nic.tablexia.util.ui.ComponentScaleUtil;
import cz.nic.tablexia.util.ui.XFillViewport;
import cz.nic.tablexia.util.ui.button.TablexiaButton;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialog;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;
import cz.nic.tablexia.util.ui.dialog.components.AdaptiveSizeDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.BackButtonHideComponent;
import cz.nic.tablexia.util.ui.dialog.components.CenterPositionDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.CloseButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.FixedSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ResizableSpaceContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.SingleButtonContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.TablexiaDialogComponentAdapter;
import cz.nic.tablexia.util.ui.dialog.components.TextContentDialogComponent;
import cz.nic.tablexia.util.ui.dialog.components.ViewportMaximumSizeComponent;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Abstract Tablexia libGDX application screenshotListener with screen transactions implementation.
 *
 * @author Matyáš Latner
 */
public abstract class TablexiaApplication implements ApplicationListener {
    public static final String PRELOADER_CLOSE_BUTTON = "preloader button";
    public static final String EVENT_PRELOADER_BUTTON_ENABLE = "preloader button is enable";

    private static final Color  BACKGROUND_COLOR              =  new Color(0f, 0f, 0f, 1f);
    private static final int    DIMMER_TRANSACTION_SPEED      =  1;

    private static final float  PRELOADER_DIALOG_HEIGHT       =  400;
    private static final float  PRELOADER_DIALOG_WIDTH        =  500;

    private static final float  SCREEN_LOADER_MOUSE_FRAMETIME =  0.15f;
    private static final int    SCREEN_LOADER_MOUSE_FRAMES    =  9;

    //Adding a little offset to the mouse, because it doesnt feel centered because of a space around the tail
    private static final float  SCREEN_LOADER_MOUSE_WIDTH     =  0.20f;
    private static final float  SCREEN_LOADER_MOUSE_OFFSET_X  = -0.13f;
    private static final float  SCREEN_LOADER_MOUSE_DELAY     =  1.0f;
    private static final float  SCREEN_LOADER_MOUSE_FADEIN    =  0.5f;
    private static final float  SCREEN_LOADER_MOUSE_FADEOUT   =  0.5f;

    private final ApplicationFontManager.FontType PRELOADER_TITLE_FONT    = ApplicationFontManager.FontType.BOLD_26;

    private PreloaderAssetsManager  preloaderAssetsManager;
    private ScreenTransaction       currentScreenTransaction;
    private boolean                 newScreenPreloaderAsync     = true;
    private boolean                 preloaderStart              = false;
    private boolean                 screenPrepared              = false;

    private static AbstractTablexiaScreen<?> screen;
    private AbstractTablexiaScreen<?> lastScreen;
    private InputMultiplexer inputMultiplexer;
    private DebugInfo debugInfo;
    private Actor screenDimmer;
    private Group backButtonLayer;
    private Group menuLayer;
    private static Stage stage;
    private boolean screenSizeUnderThreshold = false;

    private TablexiaComponentDialog             preloaderDialog;
    private SingleButtonContentDialogComponent  preloaderCloseButtonComponent;

    private AnimatedImage screenLoaderMouse;

    private boolean mouseVisible = false;
    private boolean loadingComplete = false;

//////////////////////////// TablexiaApplication API

    public static Stage getStage() {
        return stage;
    }

    public static Class<? extends AbstractTablexiaScreen> getActualScreenClass() {
        if (screen == null) {
            return null;
        }
        return screen.getClass();
    }


//////////////////////////// LIBGDX LIFECYCLE

    @Override
    public void dispose() {
        disposePreloader();
        ApplicationBus.getInstance().unsubscribe(screenDimmer);
        inputMultiplexer.removeProcessor(alternativeControlsInputProcessor);
        inputMultiplexer.removeProcessor(backButtonInputProcessor);
        inputMultiplexer.removeProcessor(stage);
        if (debugInfo != null) {
            debugInfo.dispose();
        }
        stage.dispose();
        if (lastScreen != null) {
            lastScreen.hide();
            lastScreen.dispose();
        }
        if (screen != null) {
            screen.hide();
            screen.dispose();
            screen = null;
        }
    }

    @Override
    public void pause() {
        if (lastScreen != null) lastScreen.pause();
        if (screen != null) screen.pause();
    }

    @Override
    public void resume() {
        if (lastScreen != null) lastScreen.resume();
        if (screen != null) screen.resume();
    }

    @Override
    public void create() {
        stage = new Stage(new XFillViewport());
        inputMultiplexer = new InputMultiplexer(stage);
        inputMultiplexer.addProcessor(backButtonInputProcessor);
        inputMultiplexer.addProcessor(alternativeControlsInputProcessor);
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setInputProcessor(inputMultiplexer);
        prepareBackButtonLayer();
        prepareMenuLayer();
        prepareScreenDimmer();
        prepareDebugInfo();

        ApplicationBus.getInstance().subscribe(this);
    }

    @Override
    public void render() {
        // clear screen every frame
        Gdx.gl.glClearColor(BACKGROUND_COLOR.r, BACKGROUND_COLOR.g, BACKGROUND_COLOR.b, BACKGROUND_COLOR.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (debugInfo != null) {
            debugInfo.update();
        }
        float deltaTime = Gdx.graphics.getDeltaTime();
        if (lastScreen != null) lastScreen.render(deltaTime);
        if (screen != null) screen.render(deltaTime);

        stage.act(deltaTime);
        stage.draw();

        if (preloaderStart && preloaderAssetsManager != null) {
            if (newScreenPreloaderAsync) {
                if (!preloaderAssetsManager.update()) return;
                preloaderComplete();
            } else {
                preloaderAssetsManager.finishLoading();
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        if (lastScreen != null) lastScreen.resize(width, height);
        if (screen != null) screen.resize(width, height);
        if (stage != null) {
            stage.getViewport().update(width, height, true);
            setScreenDimmerBounds();
        }

        if (screenSizeUnderThreshold != ComponentScaleUtil.isUnderThreshold()) {
            ApplicationBus.getInstance().publishAsync(new ScreenSizeThresholdChanged(ComponentScaleUtil.isUnderThreshold()));
            screenSizeUnderThreshold = ComponentScaleUtil.isUnderThreshold();
        }
        // keep debug info on edges of the screen
        if (debugInfo != null) {
            debugInfo.onResize();
        }
    }


//////////////////////////// BACK BUTTON HANDLER

    InputAdapter backButtonInputProcessor = new InputAdapter() {
        @Override
        public boolean keyDown(int keycode) {
            if (keycode == Input.Keys.BACK) {
                ApplicationBus.getInstance().post(new BackButtonPressed()).asynchronously();
            }
            return false;
        }
    };
    
//////////////////////////// ALTERNATIVE CONTROLS HANDLER

    InputAdapter alternativeControlsInputProcessor = new InputAdapter() {
        @Override
        public boolean keyDown(int keycode) {
            if (keycode == Input.Keys.ESCAPE) {
                ApplicationBus.getInstance().publishAsync(new EscButtonPressed());
            }
            return false;
        }
    };

//////////////////////////// SCREEN DIMMER

    public static class ScreenDimmerHiddenEvent implements ApplicationEvent {
        private ScreenDimmerHiddenEvent() {}
    }

    public static class ScreenDimmerShownEvent implements ApplicationEvent {
        private ScreenDimmerShownEvent() {}
    }

    private void prepareScreenDimmer() {
        screenDimmer = new Image(ApplicationAtlasManager.getInstance().getColorTexture(Color.BLACK)) {

            @Handler
            public void handleScreenDimmerControlEvent(DimmerControlEvent dimmerControlEvent) {
                if (dimmerControlEvent.isDim()) {
                    setTouchable(Touchable.enabled);
                    addAction(sequence(fadeIn(DIMMER_TRANSACTION_SPEED, Interpolation.pow4Out), run(new Runnable() {
                        @Override
                        public void run() {
                            ApplicationBus.getInstance().post(new ScreenDimmerShownEvent()).asynchronously();
                        }
                    })));
                } else {
                    addAction(sequence(fadeOut(DIMMER_TRANSACTION_SPEED, Interpolation.pow4In), run(new Runnable() {
                        @Override
                        public void run() {
                            setTouchable(Touchable.disabled);
                            ApplicationBus.getInstance().post(new ScreenDimmerHiddenEvent()).asynchronously();
                        }
                    })));
                }
            }
        };
        setScreenDimmerBounds();
        screenDimmer.setTouchable(Touchable.disabled);
        screenDimmer.addAction(Actions.alpha(0));
        // don't click through the dimmer
        screenDimmer.addCaptureListener(new ClickListener());
        stage.addActor(screenDimmer);
        ApplicationBus.getInstance().subscribe(screenDimmer);
    }

    private void setScreenDimmerBounds() {
        screenDimmer.setPosition(TablexiaSettings.getViewportLeftX(getStage()), TablexiaSettings.getViewportBottomY(getStage()));
        screenDimmer.setSize(TablexiaSettings.getViewportWidth(getStage()), TablexiaSettings.getViewportHeight(getStage()));
    }


//////////////////////////// BACK BUTTON LAYER

    private void prepareBackButtonLayer() {
        backButtonLayer = new Group();
        stage.addActor(backButtonLayer);
    }

    protected void setBackButton(TablexiaButton backButton) {
        backButtonLayer.addActor(backButton);
    }


//////////////////////////// MENU LAYER

    private void prepareMenuLayer() {
        menuLayer = new Group();
        stage.addActor(menuLayer);
    }

    protected void addMenuController(MenuController menuController) {
        menuLayer.addActor(menuController);
    }

    protected void setMenuDimmerColor(Color color){

    }


//////////////////////////// DEBUG INFO

    private void prepareDebugInfo() {
        debugInfo = DebugInfo.prepareDebugInfo(TablexiaSettings.getInstance().getBuildType().isApplicationInfo(), TablexiaSettings.getInstance().getBuildType().isScreenInfo()); // TODO probably deprecated creating specific size
        if (debugInfo != null) {
            stage.addActor(debugInfo);
        }
    }


//////////////////////////// SCREEN LOADER

    private void prepareScreenLoaderMouse() {
        screenLoaderMouse = new AnimatedImage(
                ApplicationAtlasManager.getInstance().getAnimation(
                        ApplicationAtlasManager.SCREEN_LOADER_MOUSE,
                        SCREEN_LOADER_MOUSE_FRAMES,
                        SCREEN_LOADER_MOUSE_FRAMETIME
                ),
                false
        );
    }

    private void showScreenLoaderMouse() {
        if(screenLoaderMouse == null)
            prepareScreenLoaderMouse();

        float ratio = screenLoaderMouse.getHeight()/screenLoaderMouse.getWidth();
        screenLoaderMouse.setSize(
                SCREEN_LOADER_MOUSE_WIDTH * getStage().getWidth(),
                SCREEN_LOADER_MOUSE_WIDTH * getStage().getWidth() * ratio
        );

        screenLoaderMouse.setPosition(
                TablexiaSettings.getViewportWidth(getStage()) / 2f - screenLoaderMouse.getWidth() / 2f + (screenLoaderMouse.getWidth() * SCREEN_LOADER_MOUSE_OFFSET_X),
				TablexiaSettings.getActorSceneVerticalCenter(getStage(), screenLoaderMouse)
        );

        screenLoaderMouse.addAction(Actions.alpha(0));
        screenLoaderMouse.addAction(Actions.fadeIn(SCREEN_LOADER_MOUSE_FADEIN));
        screenLoaderMouse.startAnimationLoop();
        getStage().addActor(screenLoaderMouse);
    }

    private void hideScreenLoaderMouse() {
        if(screenLoaderMouse != null && mouseVisible)
            screenLoaderMouse.addAction(new SequenceAction(Actions.fadeOut(SCREEN_LOADER_MOUSE_FADEOUT), Actions.removeActor()));

        mouseVisible = false;
    }


//////////////////////////// SCREEN PRELOADER

    public static class PreloaderAssetsManager {

        private TablexiaAbstractFileManager.AssetsStorageType   storageType;
        private TablexiaAtlasManager                            atlasManager;
        private Music                                           speech;

        private PreloaderAssetsManager(TablexiaAbstractFileManager.AssetsStorageType storageType) {
            this.storageType = storageType;
            atlasManager = new TablexiaAtlasManager(storageType);
        }

        private void loadAtlas(String atlasName) {
            atlasManager.loadAtlas(atlasName);
        }

        private void dispose() {
            atlasManager.dispose();
            if (speech != null) {
                speech.dispose();
            }
        }

        private synchronized boolean update () {
            return atlasManager.update();
        }

        private void finishLoading () {
            atlasManager.finishLoading();
        }

        public TextureRegion getTextureRegion(String regionName) {
            return atlasManager.getTextureRegionFromAtlas(screen.preparePreloaderAtlasPath(), regionName, null);
        }

        public Animation getAnimation(String regionName, int framesCount, float frameDuration) {
            return atlasManager.getAnimationFromAtlas(screen.preparePreloaderAtlasPath(), regionName, framesCount, frameDuration);
        }

        public String getText(String textKey) {
            return ApplicationTextManager.getInstance().getText(textKey);
        }

        public Music getSpeech() {
            speech = Gdx.audio.newMusic(TablexiaAbstractFileManager.getFileStoragePathFileHandle(storageType, screen.preparePreloaderSpeechPath()));
            return speech;
        }
    }

    public void startPreloader(AbstractTablexiaScreen newScreen, ScreenTransaction screenTransaction) {
        currentScreenTransaction = screenTransaction;
        newScreenPreloaderAsync = newScreen.isLoadAsync();
        preloaderAssetsManager = new PreloaderAssetsManager(newScreen.getStorageType());
        preloaderAssetsManager.loadAtlas(newScreen.preparePreloaderAtlasPath());
        preloaderStart = true;
    }

    public void stopPreloader() {
        preloaderStart = false;
        preloaderAssetsManager = null;
        currentScreenTransaction = null;
        newScreenPreloaderAsync = true;
    }

    public void disposePreloader() {
        if (preloaderAssetsManager != null) {
            preloaderAssetsManager.dispose();
            stopPreloader();
        }
    }

    private void preloaderComplete() {
        preloaderStart = false;
        prepareAndShowPreloaderDialog();
        playPreloaderSpeech();
    }

    private void prepareAndShowPreloaderDialog() {
        List<TablexiaDialogComponentAdapter> genericComponents = new ArrayList<TablexiaDialogComponentAdapter>();
        List<TablexiaDialogComponentAdapter> screenComponents = new ArrayList<TablexiaDialogComponentAdapter>();

        // loads screen specific dialog components
        screen.preparePreloaderContent(PRELOADER_DIALOG_WIDTH, PRELOADER_DIALOG_HEIGHT, preloaderAssetsManager, screenComponents);

        // build dialog from generic and screen specific components
        preloaderCloseButtonComponent = new CloseButtonContentDialogComponent(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.SYSTEM_UNDERSTAND))
                .setEnabled(false)
                .useOnce(true)
                .setInputListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        super.clicked(event, x, y);
                        continueWithTransaction();
                    }
                });
        preloaderCloseButtonComponent.getButton().setName(PRELOADER_CLOSE_BUTTON);
        genericComponents.add(new FixedSpaceContentDialogComponent());
        genericComponents.add(new TextContentDialogComponent(ApplicationTextManager.getInstance().getText(ApplicationTextManager.ApplicationTextsAssets.PRELOADER_TITLE), PRELOADER_TITLE_FONT));
        genericComponents.add(new ResizableSpaceContentDialogComponent());
        genericComponents.addAll(screenComponents);
        genericComponents.add(new ResizableSpaceContentDialogComponent());
        genericComponents.add(preloaderCloseButtonComponent);
        genericComponents.add(new FixedSpaceContentDialogComponent());
        genericComponents.add(new CenterPositionDialogComponent());
        genericComponents.add(new ViewportMaximumSizeComponent());
        genericComponents.add(new AdaptiveSizeDialogComponent());
        genericComponents.add(new BackButtonHideComponent() {
            @Override
            protected void hideDialog() {
                if (preloaderCloseButtonComponent.isEnabled()) {
                    super.hideDialog();
                    continueWithTransaction();
                }
            }
        });
        preloaderDialog = TablexiaComponentDialogFactory.getInstance().createDialog(genericComponents.toArray(new TablexiaDialogComponentAdapter[]{}));
        preloaderDialog.show(PRELOADER_DIALOG_WIDTH, PRELOADER_DIALOG_HEIGHT);
    }

    private void continueWithTransaction() {
        currentScreenTransaction.continueWithTransaction();
    }

    private void playPreloaderSpeech() {
        if (screen.preloaderHasSpeech()) {
            final Music speech = preloaderAssetsManager.getSpeech();
            screen.playPreloaderSpeech(speech);
        }
    }


//////////////////////////// SCREEN HANDLING

    public static class ScreenChangedEvent implements ApplicationEvent {

        private Class<? extends AbstractTablexiaScreen<?>> screenClass;

        public ScreenChangedEvent(Class<? extends AbstractTablexiaScreen<?>> screenClass) {
            this.screenClass = screenClass;
        }

        public Class<? extends AbstractTablexiaScreen<?>> getScreenClass() {
            return screenClass;
        }
    }

    /**
     * Change current screen.
     *
     * @param screen new screen to change for
     */
    public void setScreen(final AbstractTablexiaScreen<?> screen) {
        lastScreen = this.screen;
        this.screen = screen;

        processLastScreen(lastScreen);
        lastScreen = null;

        screen.setLoadingListener(new ScreenLoadingListener() {

            @Override
            public void loadingComplete() {
                screen.resetLoadingListener();
                processScreenVisible(screen);
            }
        });
        processNewScreen(screen);
    }

	/**
	 * Returns whether or not is screen prepared
	 * @return screenPrepared
	 */
	public boolean isScreenPrepared() {
		return screenPrepared;
	}

    /**
     * Change current screen to new screen with screen transaction.
     * If new screen is same as current screen no change is performed.
     * Unless it's LoaderScreen. That means, that language was changed.
     * If {@link ScreenTransaction} is <code>null</code> screen is changed without transaction.
     *
     * @param newScreen         new screen to change for
     * @param screenTransaction screen transaction type
     */
    public void setScreenIfIsDifferent(AbstractTablexiaScreen<?> newScreen, ScreenTransaction screenTransaction) {
        if (newScreen != null) {
            if (getActualScreen() == null || !getActualScreen().equals(newScreen)) {
                setScreen(newScreen, screenTransaction);
            }
        }
    }

    /**
     * Change current screen to new screen with screen transaction.
     * If {@link ScreenTransaction} is <code>null</code> screen is changed without transaction.
     *
     * @param newScreen         new screen to change for
     * @param screenTransaction screen transaction type
     */
    public void setScreen(final AbstractTablexiaScreen<?> newScreen, final ScreenTransaction screenTransaction) {
        Runnable newScreenAction = new Runnable() {
            @Override
            public void run() {
                screenPrepared = false;
                if (screenTransaction == null) {
                    setScreen(newScreen);
                } else {
                    // for case there is incomplete transaction
                    processLastScreen(lastScreen);

                    lastScreen = screen;
                    screen = newScreen;

                    // store screens in local variables for future use in runnable, fields can be changed by another transaction
                    final AbstractTablexiaScreen newScreenToProcess = newScreen;
                    final AbstractTablexiaScreen lastScreenToProcess = lastScreen;

                    screenTransaction.processTransaction(lastScreenToProcess, newScreenToProcess,

                            new Runnable() {

                                @Override
                                public void run() {
                                    processNewScreen(newScreenToProcess);
                                }
                            },

                            new Runnable() {
                                @Override
                                public void run() {
                                    processLastScreen(lastScreenToProcess);
                                    // reset last screen
                                    lastScreen = null;
                                }
                            },

                            new Runnable() {
                                @Override
                                public void run() {
                                    processScreenVisible(newScreenToProcess);
                                }
                            },

                            new Runnable() {
                                @Override
                                public void run() {
                                    processStartLoading(newScreenToProcess, screenTransaction);
                                }
                            },

                            new Runnable() {
                                @Override
                                public void run() {
                                    processLoadingComplete(screenTransaction);
                                }
                            });
                }
            }
        };
        newScreenAction.run();
    }

    /**
     * Return current screen
     *
     * @return current screen
     */
    public static AbstractTablexiaScreen<?> getActualScreen() {
        return screen;
    }

    @SuppressWarnings("unchecked")
    protected void processNewScreen(AbstractTablexiaScreen<?> newScreen) {
        if (newScreen != null) {
            try {
                inputMultiplexer.addProcessor(newScreen.getInputProcessor());
                setMenuDimmerColor(newScreen.getPauseColor());
                newScreen.show();
                newScreen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
                ApplicationBus.getInstance().post(new ScreenChangedEvent((Class<? extends AbstractTablexiaScreen<?>>) newScreen.getClass())).asynchronously();
            } catch (Throwable t) {
                performScreenLoadingErrorAction(newScreen, t);
            }
        }
    }

    private void processLastScreen(AbstractTablexiaScreen<?> lastScreen) {
        if (lastScreen != null) {
            inputMultiplexer.removeProcessor(lastScreen.getInputProcessor());
            lastScreen.hide();
            lastScreen.dispose();
            System.gc();
        }
    }

    private void processStartLoading(AbstractTablexiaScreen<?> newScreen, ScreenTransaction screenTransaction) {
        this.loadingComplete = false;

        if (newScreen.hasPreloader()) {
            startPreloader(newScreen, screenTransaction);
        } else {
            stopPreloader();
            preloaderDialog = null;

            getStage().addAction(new SequenceAction(Actions.delay(SCREEN_LOADER_MOUSE_DELAY), Actions.run(new Runnable() {
                @Override
                public void run() {
                    if(!loadingComplete) {
                        mouseVisible = true;
                        showScreenLoaderMouse();
                    }
                }
            })));
        }
    }

    private void processLoadingComplete(ScreenTransaction screenTransaction) {
        this.loadingComplete = true;

        if (preloaderDialog == null) {
            hideScreenLoaderMouse();
            screenTransaction.continueWithTransaction();
        } else {
            preloaderCloseButtonComponent.setEnabled(true);
            AbstractTablexiaScreen.triggerScenarioStepEvent(EVENT_PRELOADER_BUTTON_ENABLE);
        }
    }

    private void processScreenVisible(AbstractTablexiaScreen<?> newScreen) {
        screenPrepared = true;
        disposePreloader();
        try {
            newScreen.performScreenVisible();
        } catch (Throwable t) {
            performScreenLoadingErrorAction(newScreen, t);
        }
    }

    private void performScreenLoadingErrorAction(AbstractTablexiaScreen<?> screen, Throwable t) {
        Log.err(TablexiaApplication.this.getClass(), "Cannot load screen: " + screen.getClass(), t);
        ApplicationBus.getInstance().post(new Tablexia.ChangeScreenEvent(TablexiaSettings.INITIAL_SCREEN, ScreenTransaction.FADE)).asynchronously();
    }



//////////////////////////// SCREEN TRANSACTIONS

    public enum ScreenTransaction {

        MOVE_RIGHT(new MoveRightAnimation()),
        MOVE_LEFT(new MoveLeftAnimation()),
        MOVE_UP(new MoveUpAnimation()),
        FADE(new FadeAnimation());

        private interface ScreenTransactionImplementation {
            void processTransaction(AbstractTablexiaScreen<?> lastScreen, AbstractTablexiaScreen<?> newScreen, Runnable newScreenHandler, Runnable lastScreenHandler, Runnable finishHandler, Runnable loadingStartHandler, Runnable loadingCompleteHandler);
            void continueWithTransaction();
        }

        private ScreenTransactionImplementation screenTransactionImplementation;

        ScreenTransaction(ScreenTransactionImplementation screenTransactionImplementation) {
            this.screenTransactionImplementation = screenTransactionImplementation;
        }

        private void processTransaction(AbstractTablexiaScreen<?> lastScreen, AbstractTablexiaScreen<?> newScreen, Runnable newScreenHandler, Runnable lastScreenHandler, Runnable finishHandler, Runnable loaderHandler, Runnable loadingCompleteHandler) {
            screenTransactionImplementation.processTransaction(lastScreen, newScreen, newScreenHandler, lastScreenHandler, finishHandler, loaderHandler, loadingCompleteHandler);
        }

        private void continueWithTransaction() {
            screenTransactionImplementation.continueWithTransaction();
        }
    }

    private static class MoveRightAnimation implements cz.nic.tablexia.TablexiaApplication.ScreenTransaction.ScreenTransactionImplementation {

        protected float newScreenMoveFromX;
        protected float newScreenMoveToX;
        protected float lastScreenMoveToX;

        protected float newScreenMoveFromY;
        protected float newScreenMoveToY;
        protected float lastScreenMoveToY;

        protected void prepareTransactionPositions(Stage stage) {
            newScreenMoveFromX  = -TablexiaSettings.getSceneWidth(stage);
            newScreenMoveToX    = 0;
            lastScreenMoveToX   = TablexiaSettings.getSceneWidth(stage);

            newScreenMoveFromY  = 0;
            newScreenMoveToY    = 0;
            lastScreenMoveToY   = 0;
        }

        @Override
        public void processTransaction(final AbstractTablexiaScreen<?> lastScreen, final AbstractTablexiaScreen<?> newScreen, Runnable newScreenHandler, final Runnable lastScreenHandler, final Runnable finishHandler, Runnable loadingStartHandler, Runnable loadingCompleteHandler) {
            prepareTransactionPositions(lastScreen.getStage());
            final int transactionSpeed = 2;
            newScreen.setLoadingListener(new ScreenLoadingListener() {

                @Override
                public void loadingComplete() {
                    newScreen.resetLoadingListener();
                    if (lastScreen != null) {
                        lastScreen.addAction(sequence(moveTo(lastScreenMoveToX, lastScreenMoveToY, transactionSpeed, Interpolation.pow4Out), run(lastScreenHandler)));
                    }
                    if (newScreen != null) {
                        newScreen.setPosition(newScreenMoveFromX, newScreenMoveFromY);
                        newScreen.addAction(sequence(moveTo(newScreenMoveToX, newScreenMoveToY, transactionSpeed, Interpolation.pow4Out), run(finishHandler)));
                    }
                }
            });
            newScreenHandler.run();
        }

        public void continueWithTransaction() {}
    }

    private static class MoveLeftAnimation extends MoveRightAnimation {

        @Override
        protected void prepareTransactionPositions(Stage stage) {
            super.prepareTransactionPositions(stage);
            newScreenMoveFromX  = TablexiaSettings.getSceneWidth(stage);
            newScreenMoveToX    = 0;
            lastScreenMoveToX   = -TablexiaSettings.getSceneWidth(stage);
        }
    }

    private static class MoveUpAnimation extends MoveRightAnimation {

        @Override
        protected void prepareTransactionPositions(Stage stage) {
            super.prepareTransactionPositions(stage);
            newScreenMoveFromX  = 0;
            newScreenMoveToX    = 0;
            lastScreenMoveToX   = 0;

            newScreenMoveFromY  = -TablexiaSettings.getSceneOuterHeight(stage);
            newScreenMoveToY    = 0;
            lastScreenMoveToY   = TablexiaSettings.getSceneOuterHeight(stage);
        }
    }

    private static class FadeAnimation implements cz.nic.tablexia.TablexiaApplication.ScreenTransaction.ScreenTransactionImplementation {

        private Object screenDimmerHandler;

        @Override
        public void processTransaction(final AbstractTablexiaScreen<?> lastScreen, final AbstractTablexiaScreen<?> newScreen, final Runnable newScreenHandler, final Runnable lastScreenHandler, final Runnable finishHandler, final Runnable loadingStartHandler, final Runnable loadingCompleteHandler) {
            screenDimmerHandler = new Object() {

                @Handler
                public void handleScreenDimmerShownEvent(ScreenDimmerShownEvent screenDimmerShownEvent) {
                    // wait until screen is loaded and hide dimmer
                    newScreen.setLoadingListener(new ScreenLoadingListener() {

                        @Override
                        public void loadingComplete() {
                            newScreen.resetLoadingListener();
                            loadingCompleteHandler.run();
                        }
                    });

                    // process new screen and last screen handlers
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            lastScreenHandler.run();
                            loadingStartHandler.run();
                            newScreenHandler.run();
                        }
                    });
                }

                @Handler
                public void ScreenDimmerHiddenEvent(ScreenDimmerHiddenEvent screenDimmerHiddenEvent) {
                    ApplicationBus.getInstance().unsubscribe(screenDimmerHandler);
                    screenDimmerHandler = null;
                    // process finish handler
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            finishHandler.run();
                        }
                    });
                }

            };

            ApplicationBus.getInstance().subscribe(screenDimmerHandler);
            ApplicationBus.getInstance().post(new DimmerControlEvent(true)).asynchronously();
        }

        public void continueWithTransaction() {
            ApplicationBus.getInstance().post(new DimmerControlEvent(false)).asynchronously();
        }
    }


//////////////////////////// SERVER SYNCHRONIZATION

    @Handler
    public void handleSynchronizationEvent(StartFullSynchronizationEvent syncEvent) {
        Log.debug(this.getClass(), "Full sync request");
        for (User user: syncEvent.getUsers()) {
            RestSynchronizationService.doSyncWork(new PushDataToServer(user, syncEvent.getType()));

            //TODO - Refactor the condition. Make SyncWork proof for situation where users uuid is null or empty
            if(user.getUuid() != null && !user.getUuid().isEmpty()) RestSynchronizationService.doSyncWork(new UpdateAvatar(user));
        }
    }

    @Handler
    public void handleSynchronizationEvent(StartIncrementalSynchronizationEvent syncEvent) {
        Log.debug(this.getClass(), "Sync request for userId: " + syncEvent.getUserId());
        User user = UserDAO.selectUser(syncEvent.getUserId());
        if (user != null) {
            RestSynchronizationService.doSyncWork(new PushDataToServer(user, syncEvent.getType()));
        }
    }

    @Handler
    public void handleSynchronizationEvent(DeleteUserSynchronizationEvent syncEvent) {
        Log.debug(this.getClass(), "Delete user request for uuid: " + syncEvent.getUser().getUuid());
        if (syncEvent.getUser() != null && syncEvent.getUser().getUuid() != null) {
            RestSynchronizationService.doSyncWork(new DeleteUser(syncEvent.getUser()));
        }
    }

    @Handler
    public void handleSynchronizationEvent(DownloadUserSynchronizationEvent syncEvent) {
        if (syncEvent.getUser().getUuid() == null || syncEvent.getUser().getUuid().isEmpty()) {
            Log.err(this.getClass(), "Calling download user without uuid");
            return;
        }
        Log.debug(this.getClass(), "Download user request for uuid: " + syncEvent.getUser().getUuid());
        RestSynchronizationService.doSyncWork(new SyncUser(syncEvent.getUser().getUuid()));
    }

    @Handler
    public void updateUserUuid(final PushDataToServer.UserUuidUpdatedEvent event) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                User user = TablexiaSettings.getInstance().getSelectedUser();
                if (user != null && user.getId() == event.getUserId()) {
                    user.setUuid(event.getUserUuid());
                }
            }
        });
    }


//////////////////////////// SCREEN SIZE THRESHOLD CHANGE

    public static class ScreenSizeThresholdChanged implements ApplicationBus.ApplicationEvent {

        private boolean underThreshold;

        private ScreenSizeThresholdChanged(boolean underThreshold) {
            this.underThreshold = underThreshold;
        }

        public boolean isUnderThreshold() {
            return underThreshold;
        }
    }

//////////////////////////// BACK BUTTON PRESSED EVENT

    public static class BackButtonPressed implements ApplicationBus.ApplicationEvent {

        public static final int SCREEN_PRIORITY 	= 0;
		public static final int DIALOG_PRIORITY 	= 20;
		public static final int MENU_PRIORITY 		= 10;

		private int processWithPriority = 0;

		public BackButtonPressed() {

        }

		public boolean shouldProcess(int priority) {
			return processWithPriority <= priority;
		}

		public void setProcessWithPriority(int processWithPriority) {
			this.processWithPriority = processWithPriority;
		}
	}
	
	public static class EscButtonPressed implements ApplicationBus.ApplicationEvent {
        
        public EscButtonPressed(){
            
        }
        
    }
}
