/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.graphics.Pixmap;

import org.robovm.apple.coregraphics.CGPoint;
import org.robovm.apple.coregraphics.CGRect;
import org.robovm.apple.foundation.NSData;
import org.robovm.apple.uikit.UIButton;
import org.robovm.apple.uikit.UIColor;
import org.robovm.apple.uikit.UIControlEvents;
import org.robovm.apple.uikit.UIControlState;
import org.robovm.apple.uikit.UIGraphics;
import org.robovm.apple.uikit.UIImage;
import org.robovm.apple.uikit.UIImageOrientation;
import org.robovm.apple.uikit.UIImagePickerController;
import org.robovm.apple.uikit.UIImagePickerControllerCameraCaptureMode;
import org.robovm.apple.uikit.UIImagePickerControllerCameraDevice;
import org.robovm.apple.uikit.UIImagePickerControllerDelegate;
import org.robovm.apple.uikit.UIImagePickerControllerEditingInfo;
import org.robovm.apple.uikit.UIImagePickerControllerSourceType;
import org.robovm.apple.uikit.UIInterfaceOrientation;
import org.robovm.apple.uikit.UIInterfaceOrientationMask;
import org.robovm.apple.uikit.UINavigationController;
import org.robovm.apple.uikit.UINavigationControllerDelegateAdapter;
import org.robovm.apple.uikit.UIView;
import org.robovm.apple.uikit.UIViewController;
import org.robovm.objc.Selector;

import java.nio.ByteBuffer;

import cz.nic.tablexia.util.CameraOpener;

public class IOSCameraOpener extends CameraOpener {

    private class PickerControllerDelegate extends UINavigationControllerDelegateAdapter implements UIImagePickerControllerDelegate {
        @Override
        public void didFinishPickingMedia(UIImagePickerController picker, UIImagePickerControllerEditingInfo info) {
            UIImage uiImage = getProperlyOrientedImage(info.getOriginalImage());

            NSData nsData = uiImage.toPNGData();
            ByteBuffer buffer = nsData.asByteBuffer();
            byte[] imgData = new byte[buffer.capacity()];
            buffer.get(imgData);

            Pixmap pixmap = new Pixmap(imgData, 0, imgData.length);

            nsData.dispose();
            uiImage.dispose();
            closeCamera();

            onPhotoTaken(pixmap, true);
        }

        @Override
        public void didCancel(UIImagePickerController picker) {
            picker.dismissViewController(true, null);
        }

        @Override
        public UIInterfaceOrientation getPreferredInterfaceOrientation(UINavigationController navigationController) {
            return UIInterfaceOrientation.Portrait;
        }

        @Override
        public UIInterfaceOrientationMask getSupportedInterfaceOrientations(UINavigationController navigationController) {
            return UIInterfaceOrientationMask.All;
        }
    }

    private UIViewController appController;
    private PickerControllerDelegate delegate;
    private UIImagePickerController imagePickerController;

    @Override
    public void onCameraOpen() {
        appController = ((IOSApplication) Gdx.app).getUIViewController();
        imagePickerController = new UIImagePickerController() {
            @Override
            public boolean shouldAutorotate() {
                return true;
            }

            @Override
            public UIInterfaceOrientation getPreferredInterfaceOrientationForPresentation() {
                return UIInterfaceOrientation.Portrait;
            }

            @Override
            public UIInterfaceOrientationMask getSupportedInterfaceOrientations() {
                return UIInterfaceOrientationMask.Portrait;
            }
        };

        imagePickerController.getView().setBounds(appController.getView().getBounds());
        imagePickerController.setSourceType(UIImagePickerControllerSourceType.Camera);
        imagePickerController.setCameraCaptureMode(UIImagePickerControllerCameraCaptureMode.Photo);
        imagePickerController.setCameraDevice(UIImagePickerControllerCameraDevice.Front);

        delegate = new PickerControllerDelegate();
        imagePickerController.setDelegate(delegate);
        appController.presentViewController(imagePickerController, true, null);
    }

    /**
     * Gets correctly oriented UIImage and disposes the old on.
     */
    private UIImage getProperlyOrientedImage(UIImage image) {
        if(image.getOrientation() == UIImageOrientation.Up) return image;

        UIGraphics.beginImageContext(image.getSize(), false, image.getScale());
        image.draw(new CGRect(new CGPoint(0, 0), image.getSize()));
        UIImage normalizedImage = UIGraphics.getImageFromCurrentImageContext();
        UIGraphics.endImageContext();

        image.dispose();
        return normalizedImage;
    }

    @Override
    public void onCameraClose() {
        delegate.dispose();
        delegate = null;

        imagePickerController.dismissViewController(true, null);
        imagePickerController = null;
    }

    @Override
    public boolean cameraAccessible() {
        return true;
    }

    @Override
    public void deleteTemporaryFile() { /* No Temporary File Needed! */}
}
