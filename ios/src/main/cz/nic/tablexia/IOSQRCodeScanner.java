/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.iosrobovm.IOSApplication;

import org.robovm.apple.avfoundation.AVCaptureConnection;
import org.robovm.apple.avfoundation.AVCaptureDevice;
import org.robovm.apple.avfoundation.AVCaptureDeviceInput;
import org.robovm.apple.avfoundation.AVCaptureDevicePosition;
import org.robovm.apple.avfoundation.AVCaptureMetadataOutput;
import org.robovm.apple.avfoundation.AVCaptureMetadataOutputObjectsDelegateAdapter;
import org.robovm.apple.avfoundation.AVCaptureOutput;
import org.robovm.apple.avfoundation.AVCaptureSession;
import org.robovm.apple.avfoundation.AVCaptureVideoOrientation;
import org.robovm.apple.avfoundation.AVCaptureVideoPreviewLayer;
import org.robovm.apple.avfoundation.AVLayerVideoGravity;
import org.robovm.apple.avfoundation.AVMediaType;
import org.robovm.apple.avfoundation.AVMetadataObject;
import org.robovm.apple.avfoundation.AVMetadataObjectType;
import org.robovm.apple.coregraphics.CGRect;
import org.robovm.apple.dispatch.DispatchQueue;
import org.robovm.apple.dispatch.DispatchQueueAttr;
import org.robovm.apple.foundation.NSArray;
import org.robovm.apple.foundation.NSErrorException;
import org.robovm.apple.foundation.NSURL;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.apple.uikit.UIButton;
import org.robovm.apple.uikit.UIControlState;
import org.robovm.apple.uikit.UIImage;
import org.robovm.apple.uikit.UIInterfaceOrientation;
import org.robovm.apple.uikit.UIView;
import org.robovm.apple.uikit.UIViewController;
import org.robovm.objc.block.VoidBooleanBlock;

import java.util.Arrays;

import cz.nic.tablexia.loader.application.ApplicationTextManager;
import cz.nic.tablexia.util.Log;
import cz.nic.tablexia.util.ui.QRCodeScanner;
import cz.nic.tablexia.util.ui.dialog.TablexiaComponentDialogFactory;

public class IOSQRCodeScanner extends QRCodeScanner {
    private class CaptureMetadataOutputDelegate extends AVCaptureMetadataOutputObjectsDelegateAdapter{
        private IOSQRCodeScanner qrCodeScanner;
        
        public CaptureMetadataOutputDelegate() {
            this.qrCodeScanner = IOSQRCodeScanner.this;
        }
        
        @Override
        public void didOutputMetadataObjects(AVCaptureOutput captureOutput, NSArray<AVMetadataObject> metadataObjects, AVCaptureConnection connection) {
            if(metadataObjects != null && metadataObjects.size() > 0) {
                AVMetadataObject data = metadataObjects.get(0);
                if(data.getType() == AVMetadataObjectType.QRCode) {
                    //Found QR Code TODO - change data.toString()
                    qrCodeScanner.onCodeScanned(data.toString());
                }
                
                metadataObjects.dispose();
            }
        }
    }

    private static final String DISPATCH_QUEUE_NAME         = "qrCodeQueue";
    private static final String PERMISSION_MESSAGE_TEXT_KEY = "qr_scanner_permission_message_ios";

    private static final String IOS_CANCEL_BUTTON_PATH      = "button/ios_cancel.png";
    private static final int    IOS_CANCEL_BUTTON_PADDING   = 10;
    private static final int    IOS_CANCEL_BUTTON_SIZE      = 50;

    private AVCaptureDevice cameraDevice;
    
    private AVCaptureSession captureSession;
    private AVCaptureDeviceInput captureDeviceInput;
    private AVCaptureMetadataOutput captureMetadataOutput;
    
    private CaptureMetadataOutputDelegate captureDelegate;
    
    private UIViewController cameraPreviewViewController;
    private UIView cameraPreviewView;
    private AVCaptureVideoPreviewLayer videoPreviewLayer;
    
    private DispatchQueue dispatchQueue;
    
    private boolean initializeCameraDevice() {
        if(cameraDevice != null) return true;
        
        NSArray<AVCaptureDevice> captureDevices = AVCaptureDevice.getDevicesForMediaType(AVMediaType.Video);
        
        for(AVCaptureDevice captureDevice : captureDevices) {
            if(captureDevice.getPosition() == AVCaptureDevicePosition.Back) {
                //We found back camera!
                cameraDevice = captureDevice;
                return true;
            }
        }

        return false;
    }

    @Override
    protected CameraPermissionStatus refreshCameraPermission() {
        if(!initializeCameraDevice()) return CameraPermissionStatus.Denied;

        switch (cameraDevice.getAuthorizationStatusForMediaType(AVMediaType.Video)) {
            case Authorized:
                return CameraPermissionStatus.Allowed;

            case NotDetermined:
                return CameraPermissionStatus.NotDetermined;

            case Restricted:
            case Denied:
                return CameraPermissionStatus.Denied;
        }

        //Something went wrong...
        return CameraPermissionStatus.Denied;
    }

    @Override
    protected void requestPermissionSettingChange() {
        //First request...
        if(getCameraPermissionStatus() == CameraPermissionStatus.NotDetermined) {
            Log.info(getClass(), "Requesting access for media type: " + AVMediaType.Video);
            cameraDevice.requestAccessForMediaType(AVMediaType.Video, new VoidBooleanBlock() {
                @Override
                public void invoke(final boolean result) {
                    if (result) {
                        cameraPermissionStatus = CameraPermissionStatus.Allowed;

                        //Camera has to be launched from main thread!
                        Gdx.app.postRunnable(() -> startCameraPreview());
                    }
                    else cameraPermissionStatus = CameraPermissionStatus.Denied;
                }
            });
        }
        else { //First permission request has been denied. Open our own dialog...
            TablexiaComponentDialogFactory.getInstance().createPermissionDialog(
                    ApplicationTextManager.getInstance().getText(PERMISSION_MESSAGE_TEXT_KEY),
                    () -> {
                        //On success open settings url for this app
                        UIApplication.getSharedApplication().openURL(new NSURL(UIApplication.getOpenSettingsURLString()));
                    }
            ).show();
        }
    }

    private AVCaptureVideoOrientation getOrientationForCameraPreviewLayer() {
        switch (UIApplication.getSharedApplication().getStatusBarOrientation()) {
            case LandscapeLeft: return AVCaptureVideoOrientation.LandscapeLeft;
            case LandscapeRight: return AVCaptureVideoOrientation.LandscapeRight;
            case Portrait: return AVCaptureVideoOrientation.Portrait;
            case PortraitUpsideDown: return AVCaptureVideoOrientation.PortraitUpsideDown;
            default: return AVCaptureVideoOrientation.LandscapeLeft;
        }
    }
    
    private void updateViewPreviewLayerOrientation() {
        if(videoPreviewLayer != null ) videoPreviewLayer.getConnection().setVideoOrientation(getOrientationForCameraPreviewLayer());
    }
    
    @Override
    public void onCameraPreviewStarted() {
        captureSession = new AVCaptureSession();
        
        //INPUT DEVICE//
        try {
            captureDeviceInput = new AVCaptureDeviceInput(cameraDevice);
            if(captureSession.canAddInput(captureDeviceInput)) captureSession.addInput(captureDeviceInput);
        } catch (NSErrorException e) {
            Log.err(getClass(), "Cannot start camera preview!", e);
            
            if(captureDeviceInput != null) captureDeviceInput.dispose();
            captureSession.dispose();
            return;
        }
        
        dispatchQueue = DispatchQueue.create(DISPATCH_QUEUE_NAME, DispatchQueueAttr.Serial());
        
        captureMetadataOutput = new AVCaptureMetadataOutput();
        captureDelegate = new CaptureMetadataOutputDelegate();
        captureMetadataOutput.setMetadataObjectsDelegate(captureDelegate, dispatchQueue);
        
        if(captureSession.canAddOutput(captureMetadataOutput)) captureSession.addOutput(captureMetadataOutput);
        captureMetadataOutput.setMetadataObjectTypes(Arrays.asList(AVMetadataObjectType.QRCode));
        
        IOSApplication iosApplication = (IOSApplication) Gdx.app;
        
        cameraPreviewViewController = new UIViewController() {
            @Override
            public void viewWillLayoutSubviews() {
                super.viewWillLayoutSubviews();
                updateViewPreviewLayerOrientation();
            }
            
            @Override
            public void willAnimateRotation(UIInterfaceOrientation uiInterfaceOrientation, double v) {
                //Unfortunately this method is deprecated and there isn't binding in RoboVM for any of these replacements...
                // - willTransitionToTraitCollection:withTransitionCoordinator:
                // - viewWillTransitionToSize:withTransitionCoordinator:
                super.willAnimateRotation(uiInterfaceOrientation, v);
                updateViewPreviewLayerOrientation();
            }
        };
        
        cameraPreviewView = new UIView();
        cameraPreviewView.setBounds(iosApplication.getUIViewController().getView().getBounds());
        cameraPreviewView.setCenter(iosApplication.getUIViewController().getView().getCenter());

        cameraPreviewViewController.setView(cameraPreviewView);
        
        videoPreviewLayer = new AVCaptureVideoPreviewLayer(captureSession);
        videoPreviewLayer.setVideoGravity(AVLayerVideoGravity.ResizeAspectFill);
        videoPreviewLayer.getConnection().setVideoOrientation(getOrientationForCameraPreviewLayer());
        videoPreviewLayer.setFrame(cameraPreviewView.getBounds());

        cameraPreviewView.getLayer().addSublayer(videoPreviewLayer);
        cameraPreviewView.addSubview(getButtonBack(iosApplication.getUIViewController().getView().getBounds()));
        
        iosApplication.getUIViewController().addChildViewController(cameraPreviewViewController);
        iosApplication.getUIViewController().getView().addSubview(cameraPreviewView);
        
        if(!captureSession.isRunning()) captureSession.startRunning();
    }

    private UIButton getButtonBack(CGRect viewBounds){
       UIButton button = new UIButton(
                new CGRect(
                        viewBounds.getMaxX() - IOS_CANCEL_BUTTON_SIZE - IOS_CANCEL_BUTTON_PADDING,
                        IOS_CANCEL_BUTTON_PADDING,
                        IOS_CANCEL_BUTTON_SIZE,
                        IOS_CANCEL_BUTTON_SIZE));

        button.setImage(UIImage.getImage(IOS_CANCEL_BUTTON_PATH), UIControlState.Normal);
        button.addOnTouchUpInsideListener((uiControl, uiEvent) -> cancelScanner());

        return button;
    }

    
    @Override
    public void onCameraPreviewStopped() {
        captureSession.stopRunning();
        
        captureSession.removeInput(captureDeviceInput);
        captureSession.removeOutput(captureMetadataOutput);
        
        captureDeviceInput.dispose();
        captureDeviceInput = null;
        
        captureMetadataOutput.dispose();
        captureMetadataOutput = null;
        
        captureDelegate.dispose();
        captureDelegate = null;
        
        videoPreviewLayer.removeFromSuperlayer();
        videoPreviewLayer.dispose();
        videoPreviewLayer = null;
        
        cameraPreviewViewController.removeFromParentViewController();
        cameraPreviewViewController.dispose();
        cameraPreviewViewController = null;
        
        cameraPreviewView.removeFromSuperview();
        cameraPreviewView.dispose();
        cameraPreviewView = null;
        
        captureSession.dispose();
        captureSession = null;
        
        dispatchQueue = null;
    }
}