/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;
import com.badlogic.gdx.graphics.glutils.HdpiMode;

import org.robovm.apple.foundation.NSException;
import org.robovm.apple.foundation.NSErrorException;
import org.robovm.apple.foundation.NSFileManager;
import org.robovm.apple.foundation.NSFileSystemAttributes;
import org.robovm.objc.block.VoidBlock1;
import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.foundation.NSBundle;
import org.robovm.apple.foundation.NSDictionary;
import org.robovm.apple.foundation.NSString;
import org.robovm.apple.glkit.GLKViewDrawableMultisample;
import org.robovm.apple.systemconfiguration.SCNetworkReachability;
import org.robovm.apple.systemconfiguration.SCNetworkReachabilityFlags;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.apple.uikit.UIApplicationLaunchOptions;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import cz.nic.tablexia.screen.loader.IConnectionManager;
import cz.nic.tablexia.screen.loader.ILegacyManager;
import cz.nic.tablexia.util.IFileSystemManager;
import cz.nic.tablexia.util.Log;

public class IOSLauncher extends IOSApplication.Delegate {
    private static final String                      TABLEXIA_BUILD_TYPE_KEY            = "cz.nic.tablexia.BuildType";
    private static final String                      TABLEXIA_SENTRY_DSN_KEY            = "cz.nic.tablexia.SentryDSN";

    public  static final Tablexia.SQLConnectionType  SQL_CONNECTION_TYPE                = new Tablexia.SQLConnectionType("SQLite.JDBCDriver", "jdbc:sqlite:");
    private static final boolean                     HAS_SOFT_BACK_BUTTON               = true;
    private static final boolean                     HAS_ALTERNATIVE_CONTROLS           = false;

    private static final String                      FALLBACK_CONNECTION_CHECK_HOST     = "nic.cz";
    private static final Integer                     FALLBACK_CONNECTION_CHECK_PORT     = 80;

    private static final String                      CONNECTION_CHECK_HOST              = TablexiaBuildConfig.TABLEXIA_SERVER_HOST != null ? TablexiaBuildConfig.TABLEXIA_SERVER_HOST : FALLBACK_CONNECTION_CHECK_HOST;
    private static final Integer                     CONNECTION_CHECK_PORT              = TablexiaBuildConfig.TABLEXIA_SERVER_PORT != null ? TablexiaBuildConfig.TABLEXIA_SERVER_PORT : FALLBACK_CONNECTION_CHECK_PORT;

    private     TablexiaIOSFiles                     tablexiaIOSFiles;
    protected   Tablexia                             tablexia;

    private IOSApplication                           iosApplication;

    @Override
    protected IOSApplication createApplication() {
        IOSApplicationConfiguration config = new IOSApplicationConfiguration();
		config.multisample = GLKViewDrawableMultisample._4X;
        config.orientationLandscape = true;
        config.orientationPortrait = false;
        config.allowIpod = true;
        config.hdpiMode = HdpiMode.Pixels;

        NSDictionary infoDictionary = NSBundle.getMainBundle().getInfoDictionary();
        String buildType = infoDictionary.get(new NSString(TABLEXIA_BUILD_TYPE_KEY)).toString();
        String sentryDSN = infoDictionary.get(new NSString(TABLEXIA_SENTRY_DSN_KEY)).toString();
        
        tablexiaIOSFiles = new TablexiaIOSFiles();
        this.iosApplication = new IOSApplication(new Tablexia(buildType,
                Locale.getDefault(),
                SQL_CONNECTION_TYPE,
                new IOSConnectionManager(),
                new IOSLegacyManager(),
                new IOSCameraOpener(),
                new IOSFileSystemManager(),
                new IOSQRCodeScanner(),
                sentryDSN,
                HAS_SOFT_BACK_BUTTON,
                HAS_ALTERNATIVE_CONTROLS,
                false,
                null), config) {

            @Override
            public Files getFiles() {
                return tablexiaIOSFiles;
            }
        };

        return iosApplication;
    }

    @Override
    public boolean didFinishLaunching(UIApplication application, UIApplicationLaunchOptions launchOptions) {
        boolean result = super.didFinishLaunching(application, launchOptions);

        registerUncaughtExceptionHandler();

        Gdx.files = tablexiaIOSFiles;
        return result;
    }

    private void registerUncaughtExceptionHandler() {
        NSException.setUncaughtExceptionHandler(new VoidBlock1<NSException>() {
            @Override
            public void invoke(final NSException e) {
                String exceptionMsg = e.getName() + ": " + e.getReason();

                StackTraceElement[] elements = new StackTraceElement[e.getCallStackSymbols().size()];
                for(int i = 0; i < elements.length; i++) {
                    elements[i] = new StackTraceElement(e.getCallStackSymbols().get(i).toString(), "", "", 0);
                }

                TablexiaSentry.sendCustomException(TablexiaSentry.ExceptionType.IOSException, exceptionMsg, elements);
            }
        });
    }

    public static void main(String[] argv) {
        runIt(argv, IOSLauncher.class);
    }

    public static void runIt(String[] argv, Class launcher){
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(argv, null, launcher);
        pool.close();
    }

    public static class IOSConnectionManager implements IConnectionManager {
        @Override
        public boolean isUsingMobileData() {
            return getConnectionType() == ConnectionType.Mobile;
        }

        @Override
        public ConnectionType getConnectionType() {
            SCNetworkReachabilityFlags flags = new SCNetworkReachability(CONNECTION_CHECK_HOST).getFlags();

            if(flags.compareTo(SCNetworkReachabilityFlags.IsWWAN) == 1) {
                return ConnectionType.Mobile;
            } else {
                return ConnectionType.Wifi;
            }
        }
    }

    private static class IOSFileSystemManager implements IFileSystemManager {
        @Override
        public Long getSpaceAvailable(String path) {
            try {
                File file = new File(path);
                if(!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                NSFileSystemAttributes fileSystemAttributes = NSFileManager.getDefaultManager().getAttributesOfFileSystemAtPath(path);
                long result =  fileSystemAttributes.getFreeSize();
                file.delete();
                return result;
            } catch (NSErrorException e) {
                Log.err(getClass(), "Can't get free space for path: " + path + "!", e);
            }

            return null;
        }
    }

    private static class IOSLegacyManager implements ILegacyManager {

        @Override
        public boolean needLegacyCheck() {
            return false;
        }

        @Override
        public boolean isSupportedDevice() {
            return true;
        }

        @Override
        public void checkLegacy() {

        }
    }
}
