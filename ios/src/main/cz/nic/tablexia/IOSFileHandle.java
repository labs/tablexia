/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.io.File;

public class IOSFileHandle extends FileHandle {
    protected IOSFileHandle (String fileName, Files.FileType type) {
        super(fileName, type);
    }

    protected IOSFileHandle (File file, Files.FileType type) {
        super(file, type);
    }

    @Override
    public FileHandle child (String name) {
        if (file.getPath().length() == 0) return new IOSFileHandle(new File(name), type);
        return new IOSFileHandle(new File(file, name), type);
    }

    @Override
    public FileHandle parent () {
        File parent = file.getParentFile();
        if (parent == null) {
            if (type == Files.FileType.Absolute)
                parent = new File("/");
            else
                parent = new File("");
        }
        return new IOSFileHandle(parent, type);
    }

    @Override
    public FileHandle sibling (String name) {
        if (file.getPath().length() == 0) throw new GdxRuntimeException("Cannot get the sibling of the root.");
        return new IOSFileHandle(new File(file.getParent(), name), type);
    }

    @Override
    public File file () {
        if (type == Files.FileType.Internal) return new File(TablexiaIOSFiles.internalPath, file.getPath());
        if (type == Files.FileType.External) return new File(TablexiaIOSFiles.externalPath, file.getPath());
        if (type == Files.FileType.Local) return new File(TablexiaIOSFiles.localPath, file.getPath());
        return file;
    }

}
