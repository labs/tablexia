/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.files.FileHandle;

import org.robovm.apple.foundation.NSBundle;

public class TablexiaIOSFiles implements Files {

    static final String appDir = System.getenv("HOME");
    static final String externalPath = appDir + "/Documents/";
    static final String localPath = appDir + "/Library/Caches/";
    static final String internalPath = NSBundle.getMainBundle().getBundlePath();

    public TablexiaIOSFiles () {
        new FileHandle(externalPath).mkdirs();
        new FileHandle(localPath).mkdirs();
    }

    @Override
    public FileHandle getFileHandle (String fileName, FileType type) {
        return new IOSFileHandle(fileName, type);
    }

    @Override
    public FileHandle classpath (String path) {
        return new IOSFileHandle(path, FileType.Classpath);
    }

    @Override
    public FileHandle internal (String path) {
        return new IOSFileHandle(path, FileType.Internal);
    }

    @Override
    public FileHandle external (String path) {
        return new IOSFileHandle(path, FileType.External);
    }

    @Override
    public FileHandle absolute (String path) {
        return new IOSFileHandle(path, FileType.Absolute);
    }

    @Override
    public FileHandle local (String path) {
        return new IOSFileHandle(path, FileType.Local);
    }

    @Override
    public String getExternalStoragePath () {
        return externalPath;
    }

    @Override
    public boolean isExternalStorageAvailable () {
        return true;
    }

    @Override
    public String getLocalStoragePath () {
        return localPath;
    }

    @Override
    public boolean isLocalStorageAvailable () {
        return true;
    }
}
