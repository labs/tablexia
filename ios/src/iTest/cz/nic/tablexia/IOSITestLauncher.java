/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia;

import org.robovm.apple.uikit.UIApplication;
import org.robovm.apple.uikit.UIApplicationLaunchOptions;

import cz.nic.tablexia.testing.TestRunner;


public class IOSITestLauncher extends IOSLauncher {
    private static String testClassName = null;

    @Override
    public boolean didFinishLaunching(UIApplication application, UIApplicationLaunchOptions launchOptions) {
        boolean result = super.didFinishLaunching(application, launchOptions);

        TestRunner testRunner = new TestRunner(tablexia);
        testRunner.runTest(testClassName);
        return result;
    }

    public static void main(String[] argv) {
        if(argv.length > 0) {
            try {
                testClassName = argv[0].trim();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        runIt(argv, IOSITestLauncher.class);
    }
}